from openpyxl import load_workbook
import sys, os
import re


import csv

class Chapter():

    def __init__(self, ws, start_row):

        self.ws = ws
        self.chapter_start = self.ws.cell(start_row, 5)
        self.chapter_title_cell = self.ws.cell(start_row, 6)
    
    def chapter_name(self):

        return self.chapter_title_cell.value.split('-', 1)[0].strip()

    def chapter_title(self):
        return self.chapter_title_cell.value.split('-', 1)[1].split('(')[0].strip()
    
    def chapter_content(self):
        title = self.chapter_title_cell.value.split('-', 1)[1].strip()
        return re.search(r'\(([^)]+)', title).group(1) if '(' in title else ''

    def parsed_chapter(self):

        chapter_contents = []

        for i in range (1, 100):

            section_level = self.chapter_start.offset(row=i)
            if section_level.value is None:
                pass
            elif section_level.offset(column=1).value is None:
                pass
            else:
                if '1. FOUNDATIONAL' in section_level.value:
                    if not section_level.offset(column=1).value == 'No requirements':
                        section_num, section_title = [x.strip() for x in section_level.offset(column=-3).value.split('-')]
                        section_index = None
                        for index, section in enumerate(chapter_contents):
                            section_no = section.get('section_num', None)
                            if section_no == section_num:
                                section_index = index
                                break
                        
                        if section_index is None:
                            
                            chapter_contents.append({
                                'name': section_num,
                                'title': section_title,
                                'levels': {
                                    'E': section_level.offset(column=1).value.split('\n'),
                                    'D': [],
                                    'C': [],
                                    'B': [],
                                }
                            })
                        else:
                            chapter_contents[section_index]['levels']['E'].extend(section_level.offset(column=1).value.split('\n'))
                    

                elif 'BASIC' in section_level.value:
                    if not section_level.offset(column=1).value == 'No requirements':
                        section_num, section_title = [x.strip() for x in section_level.offset(column=-3).value.split('-')]
                        section_index = None
                        for index, section in enumerate(chapter_contents):
                            section_no = section.get('name', None)
                            if section_no == section_num:
                                section_index = index
                                break
                        if section_index is None:
                            chapter_contents.append({
                                'name': section_num,
                                'title': section_title,
                                'levels': {
                                    'E': [],
                                    'D': section_level.offset(column=1).value.split('\n'),
                                    'C': [],
                                    'B': [],
                                }
                            })
                        else:
                            chapter_contents[section_index]['levels']['D'].extend(section_level.offset(column=1).value.split('\n'))

                elif 'Progressive' in section_level.value:
                    if not section_level.offset(column=1).value == 'No requirements':
                        section_num, section_title = [x.strip() for x in section_level.offset(column=-3).value.split('-')]
                        section_index = None
                        for index, section in enumerate(chapter_contents):
                            section_no = section.get('name', None)
                            if section_no == section_num:
                                section_index = index
                                break
                        if section_index is None:
                            chapter_contents.append({
                                'name': section_num,
                                'title': section_title,
                                'levels': {
                                    'E': [],
                                    'D': [],
                                    'C': section_level.offset(column=1).value.split('\n'),
                                    'B': [],
                                }
                            })
                        else:
                            chapter_contents[section_index]['levels']['C'].extend(section_level.offset(column=1).value.split('\n'))
        
                elif 'Aspirational' in section_level.value:
                    if not section_level.offset(column=1).value == 'No requirements':
                        section_num, section_title = [x.strip() for x in section_level.offset(column=-3).value.split('-')]
                        section_index = None
                        for index, section in enumerate(chapter_contents):
                            section_no = section.get('name', None)
                            if section_no == section_num:
                                section_index = index
                                break
                        if section_index is None:
                            chapter_contents.append({
                                'name': section_num,
                                'title': section_title,
                                'levels': {
                                    'E': [],
                                    'D': [],
                                    'C': [],
                                    'B': section_level.offset(column=1).value.split('\n'),
                                }
                            })
                        else:
                            chapter_contents[section_index]['levels']['B'].extend(section_level.offset(column=1).value.split('\n'))

                elif 'REQUIREMENT' in section_level.value:
                    break

        return {
            'name': self.chapter_name(),
            'title': self.chapter_title(),
            'sections': chapter_contents,
        }


def tag_builder(prefix, chapter, section=None, level=None, point=None):

    builder = ''
    builder += prefix.upper()
    builder += 'CH' + str(chapter)
    if section is not None: builder += 'S' + str(section)
    if level is not None: builder += 'L' + str(level)
    if point is not None: builder += '-' + str(point)

    return builder


def create_chapter_fragment(prefix, ref_model, chapter_num, chapter_title, chapter_content):

    return {
        'ref_model': ref_model,
        'guideline_tag': tag_builder(prefix, chapter_num),
        'section_type': 'Chapter',
        'section_no': chapter_num,
        'section_name': chapter_num,
        'title': chapter_title,
        'content': chapter_content,
        'active': 'TRUE',
    }



def create_section_fragment(prefix, ref_model, grade_range, chapter_num, section_num, parsed_section):

    csv_frag = []

    print(f"STARTING: Section {parsed_section['name']}")

    csv_frag.append({
        'parent_tag': tag_builder(prefix, chapter_num),
        'ref_model': ref_model,
        'guideline_tag': tag_builder(prefix, chapter_num, section_num),
        'section_type': 'Section',
        'section_no': section_num,
        'section_name': f"{chapter_num}.{section_num}",
        'title': parsed_section['title'],
        'active': 'TRUE',
    })

    for j in range(len(grade_range)):

        level = parsed_section['levels'][grade_range[j]]

        csv_frag.append({
                'parent_tag': tag_builder(prefix, chapter_num, section_num),
                'ref_model': ref_model,
                'guideline_tag': tag_builder(prefix, chapter_num, section_num, j+1),
                'section_type': 'Level',
                'section_no': j+1,
                'section_name': grade_range[j],
                'title': 'Level ' + grade_range[j],
                'active': 'TRUE',
            })

        for k, point in enumerate(level):

            if point != '':
                csv_frag.append({
                    'parent_tag': tag_builder(prefix, chapter_num, section_num, j+1),
                    'ref_model': ref_model,
                    'guideline_tag': tag_builder(prefix, chapter_num, section_num, j+1, k+1),
                    'section_type': 'Point',
                    'section_no': k+1,
                    'section_name': f'{grade_range[j]}-{k+1}',
                    'title': f'{grade_range[j]}-{k+1}',
                    'content': point,
                    'active': 'TRUE',
                })
    
    return csv_frag


if (len(sys.argv) < 3):
    sys.exit(f'You must provide filenames after {sys.argv[0]} (ex. {sys.argv[0]} <input file> <output file>')


input_file = sys.argv[1]
output_file = sys.argv[2]


if not os.path.isfile(input_file):
    sys.exit(f'{input_file} does not exist.')

print("===== SCM CSV Maker =====")

looping = True

while looping:
    user_input = input('Are you ready? [y/n]: ')

    if user_input.upper() == 'Y':
        looping = False
    elif user_input.upper() == 'N':
        sys.exit('Exiting!')
        
# set parameters

user_input = input('What sheet is the SCM guidelines on? [1-MAXINT]: ')

if not user_input.isdigit():
    sys.exit("That's not an integer...")
elif int(user_input) == 0:
    sys.exit("Can't use 0.")

wb = load_workbook(input_file)
ws = wb[wb.sheetnames[int(user_input) - 1]]

prefix = 'SCM'
ref_model = 'scm_assessment'
grade_range = ['E', 'D', 'C', 'B']




chapter_num = 1
header = True
new_csv = []


for row in ws.rows:

    if "REQUIREMENT" in str(row[4].value):
        if header == True:
            # there's a header that has the same name as the section titles, skipping it with this
            header = False
            pass
        else:
            chapter = Chapter(ws, row[4].row)
            new_csv += [create_chapter_fragment(prefix, ref_model, chapter_num, chapter.chapter_title(), chapter.chapter_content())]
            for index, section in enumerate(chapter.parsed_chapter()['sections']):
                new_csv += create_section_fragment(prefix, ref_model, grade_range, chapter_num, index + 1, section)
            
            chapter_num += 1


if new_csv == []:
    sys.exit('There was a problem, nothing was parsed...')
else:
    print('finished building CSV file!')

with open(output_file, mode='w') as csv_file:
    fieldnames = [
        'parent_tag',
        'ref_model',
        'guideline_tag',
        'section_type',
        'section_no',
        'section_name',
        'title',
        'content',
        'active',
        'note_header',
        'note_bullets'
        ]
    writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
    print('Writing CSV file!')
    writer.writeheader()

    for x in new_csv:
        writer.writerow(x)

print(f'Write complete to {output_file}!')


# TODO: trim whitespace
# chapter_section_name, chapter_title = chapter_cell.value.split(':')
# section_section_name, section_title = section_cell.value.split(':')
# paragraphs1 = re.split('\n\n', e_cell.value)
# paragraphs2 = re.split('\n\n', d_cell.value)

# print('chapter_section_name:', chapter_section_name)
# print('chapter_title:', chapter_title)
# print('section_section_name:', section_section_name)
# print('section_title:', section_title)
# print('com cell1:', comment_cell1.value)
# print('com_cell2:', comment_cell2.value)
# print('e cell:', paragraphs1)
# print('d_cell:', paragraphs2)
# print('c_cell', c_cell.value)
# print('b_cell:', b_cell.value)







