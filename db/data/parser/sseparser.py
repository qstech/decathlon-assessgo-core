from openpyxl import load_workbook
import sys, os
import re


import csv

class Chapter():

    def __init__(self, ws, start_row):

        self.ws = ws
        self.chapter_cell = self.ws.cell(start_row, 1)

    def chapter_name(self):

        return self.chapter_cell.value.split('-')[0].strip()

    def chapter_title(self):

        return self.chapter_cell.value.split('-')[1].strip()

    def num_of_sections(self):
        # searching rows, hopefully no more than 25 sections exists.
        for i in range(1, 25):
            if ('CHAPTER' in str(self.chapter_cell.offset(row=i).value) or self.chapter_cell.offset(row=i).value is None):
                return i - 1

    def parsed_section(self, row_offset):

        section_start = self.chapter_cell.offset(row=row_offset)
        end_cell_split = re.split('\n\n', section_start.offset(column=10).value)

        if section_start.offset(column=2).value is not None:
            if '===' in str(section_start.offset(column=2).value):
                e_cell = []
            else:
                e_cell = re.split('\n\n', section_start.offset(column=2).value)
        else:
            e_cell = []
        if section_start.offset(column=4).value is not None:
            d_cell = re.split('\n\n', section_start.offset(column=4).value)
        else:
            d_cell = []
        if section_start.offset(column=6).value is not None:
            c_cell = re.split('\n\n', section_start.offset(column=6).value)
        else:
            c_cell = []
        if section_start.offset(column=8).value is not None:
            b_cell = re.split('\n\n', section_start.offset(column=8).value)
        else:
            b_cell = []

        # doing 'basic sense cell'

        level_content = {
            'E': '',
            'D': '',
            'C': '',
            'B': '',
        }

        for bs_point in end_cell_split:
            if 'E:' in bs_point:
                level_content['E'] = bs_point.split('E:')[1].strip()
            elif 'D:' in bs_point:
                level_content['D'] = bs_point.split('D:')[1].strip()
            elif 'C:' in bs_point:
                level_content['C'] = bs_point.split('C:')[1].strip()
            elif 'B:' in bs_point:
                level_content['B'] = bs_point.split('B:')[1].strip()

        return {
            'section': {
                'section_name': section_start.value.split(':')[0].strip(),
                'title': section_start.value.split(':')[1].strip(),
            },
            'levels': {
                'E': e_cell,
                'E_content': level_content['E'],
                'D': d_cell,
                'D_content': level_content['D'],
                'C': c_cell,
                'C_content': level_content['C'],
                'B': b_cell,
                'B_content': level_content['B'],
            }
        }

    def parsed_sections(self):
        end = self.num_of_sections() + 1
        sections = []
        for i in range(1, end):
            sections.append(self.parsed_section(i))

        return sections

    def parsed_chapter(self):

        return {
           'chapter': {
               'section_name': self.chapter_name(),
               'title': self.chapter_title(),
           },
           'sections': self.parsed_sections(),
        }


def tag_builder(prefix, chapter, section=None, level=None, point=None):

    builder = ''
    builder += prefix.upper()
    builder += 'CH' + str(chapter)
    if section is not None: builder += 'S' + str(section)
    if level is not None: builder += 'L' + str(level)
    if point is not None: builder += '-' + str(point)

    return builder

def create_chapter_csv_fragment(prefix, ref_model, grade_range, chapter_num, parsed_chapter):

    csv_frag = []
    chapter = parsed_chapter['chapter']
    sections = parsed_chapter['sections']

    print(f"STARTING: Chapter {chapter['section_name']}")

    csv_frag.append({
        'ref_model': ref_model,
        'guideline_tag': tag_builder(prefix, chapter_num),
        'section_type': 'Chapter',
        'section_no': chapter_num,
        'section_name': chapter['section_name'],
        'title': chapter['title'],
        'active': 'TRUE',
        })

    for i, section in enumerate(sections):
        print(f"STARTING: Section {section['section']['section_name']}")

        section_num = i + 1
        csv_frag.append({
            'parent_tag': tag_builder(prefix, chapter_num),
            'ref_model': ref_model,
            'guideline_tag': tag_builder(prefix, chapter_num, section_num),
            'section_type': 'Section',
            'section_no': section_num,
            'section_name': f"{chapter_num}.{section_num}",
            'title': section['section']['title'],
            'active': 'TRUE',
        })

        for j in range(len(grade_range)):
            level_num = j + 1

            level = section['levels'][grade_range[j]]

            if (level != []):
                csv_frag.append({
                    'parent_tag': tag_builder(prefix, chapter_num, section_num),
                    'ref_model': ref_model,
                    'guideline_tag': tag_builder(prefix, chapter_num, section_num, level_num),
                    'section_type': 'Level',
                    'section_no': level_num,
                    'section_name': grade_range[j],
                    'title': 'Level ' + grade_range[j],
                    'content': section['levels'][f'{grade_range[j]}_content'],
                    'active': 'TRUE',
                })

                for k, point in enumerate(level):
                    if len(point.split('- ')) > 1:
                        csv_frag.append({
                            'parent_tag': tag_builder(prefix, chapter_num, section_num, level_num),
                            'ref_model': ref_model,
                            'guideline_tag': tag_builder(prefix, chapter_num, section_num, level_num, k+1),
                            'section_type': 'Point',
                            'section_no': k+1,
                            'section_name': f'{grade_range[j]}-{k+1}',
                            'title': f'{grade_range[j]}-{k+1}',
                            'content': point.split('- ')[0].strip(),
                            'active': 'TRUE',
                        })
                        for bullet in point.split('- ')[1:]:
                            csv_frag.append({
                                'parent_tag': 'BULLET',
                                'ref_model': ref_model,
                                'guideline_tag': tag_builder(prefix, chapter_num, section_num, level_num, k+1),
                                'section_type': '',
                                'section_no': '',
                                'section_name': '',
                                'title': '',
                                'content': '',
                                'active': '',
                                'note_bullets': bullet.strip(),
                            })
                    else:
                        csv_frag.append({
                            'parent_tag': tag_builder(prefix, chapter_num, section_num, level_num),
                            'ref_model': ref_model,
                            'guideline_tag': tag_builder(prefix, chapter_num, section_num, level_num, k+1),
                            'section_type': 'Point',
                            'section_no': k+1,
                            'section_name': f'{grade_range[j]}-{k+1}',
                            'title': f'{grade_range[j]}-{k+1}',
                            'content': point,
                            'active': 'TRUE',
                        })

    return csv_frag


if (len(sys.argv) < 3):
    sys.exit(f'You must provide filenames after {sys.argv[0]} (ex. {sys.argv[0]} <input file> <output file>')


input_file = sys.argv[1]
output_file = sys.argv[2]

if not os.path.isfile(input_file):
    sys.exit(f'{input_file} does not exist.')

print("===== SSE CSV Maker =====")
print("NOTE 1: Each level point must be on it's own paragraph, the program is looking for double line breaks.")
print("NOTE 2: Program doesn't follow links on the spreadsheet, you must only provide values in each cell.")
print("NOTE 3: Each bullet point must be in this format ' -<content>', it's space sensitive!")

looping = True

while looping:
    user_input = input('Are you ready? [y/n]: ')

    if user_input.upper() == 'Y':
        looping = False
    elif user_input.upper() == 'N':
        sys.exit('Exiting!')

# set parameters

user_input = input('What sheet is the SSE guidelines on? [1-MAXINT]: ')

if not user_input.isdigit():
    sys.exit("That's not an integer...")
elif int(user_input) == 0:
    sys.exit("Can't use 0.")

wb = load_workbook(input_file, data_only=True)
ws = wb[wb.sheetnames[int(user_input) - 1]]

prefix = 'SSE'
ref_model = 'sse_assessment'
grade_range = ['E', 'D', 'C', 'B']
chapter_num = 1

new_csv = []

for row in ws.rows:
    if "CHAPTER" in str(row[0].value):
        chapter = Chapter(ws, row[0].row)

        new_csv += create_chapter_csv_fragment(
            prefix, ref_model, grade_range, chapter_num, chapter.parsed_chapter())

        chapter_num += 1

if new_csv == []:
    sys.exit('There was a problem, nothing was parsed...')
else:
    print('finished building CSV file!')

with open(output_file, mode='w') as csv_file:
    fieldnames = [
        'parent_tag',
        'ref_model',
        'guideline_tag',
        'section_type',
        'section_no',
        'section_name',
        'title',
        'content',
        'active',
        'note_header',
        'note_bullets'
        ]
    writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
    print('Writing CSV file!')
    writer.writeheader()

    for x in new_csv:
        writer.writerow(x)

print(f'Write complete to {output_file}!')


# TODO: trim whitespace
# chapter_section_name, chapter_title = chapter_cell.value.split(':')
# section_section_name, section_title = section_cell.value.split(':')
# paragraphs1 = re.split('\n\n', e_cell.value)
# paragraphs2 = re.split('\n\n', d_cell.value)

# print('chapter_section_name:', chapter_section_name)
# print('chapter_title:', chapter_title)
# print('section_section_name:', section_section_name)
# print('section_title:', section_title)
# print('com cell1:', comment_cell1.value)
# print('com_cell2:', comment_cell2.value)
# print('e cell:', paragraphs1)
# print('d_cell:', paragraphs2)
# print('c_cell', c_cell.value)
# print('b_cell:', b_cell.value)







