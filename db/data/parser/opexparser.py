from openpyxl import load_workbook
import sys, os
import re


import csv


class OPEXChapter():

    def __init__(self, ws, start_row):

        self.ws = ws
        self.chapter_cell = self.ws.cell(start_row, 1)
    
    def chapter_name(self):

        return self.chapter_cell.value.split(':')[0].strip()

    def chapter_title(self):

        return self.chapter_cell.value.split(':')[1].strip()

    def num_of_sections(self):
        # searching rows, hopefully not more than 25 sections exists.
        for i in range(2, 25):
            if (self.chapter_cell.offset(row=i).value is None):
                return i - 2
    
    def parsed_section(self, row_offset):
        
        section_start = self.chapter_cell.offset(row=row_offset)
        section_content = section_start.offset(column=2).value.strip().replace('\n',' ')
        section_note_header = section_start.offset(column=3).value.strip().replace('\n',' ')

        if section_start.offset(column=6).value is not None: 
            e_cell = re.split('\n\n', section_start.offset(column=6).value)
        else:
            e_cell = []
        if section_start.offset(column=8).value is not None:
            d_cell = re.split('\n\n', section_start.offset(column=8).value)
        else:
            d_cell = []
        if section_start.offset(column=10).value is not None: 
            c_cell = re.split('\n\n', section_start.offset(column=10).value)
        else:
            c_cell = []
        if section_start.offset(column=10).value is not None: 
            b_cell = re.split('\n\n', section_start.offset(column=12).value)
        else:
            b_cell = []

        return {
            'section': {
                'section_name': section_start.value.split(':')[0].strip(),
                'title': section_start.value.split(':')[1].strip(),
                'content': section_content,
                'note_header': section_note_header,
            },
            'levels': {
                'E': e_cell,
                'D': d_cell,
                'C': c_cell,
                'B': b_cell,
            }
        }
    
    def parsed_sections(self):
        end = 2 + self.num_of_sections()
        sections = []
        for i in range(2, end):
            sections.append(self.parsed_section(i))
        
        return sections
    
    def parsed_chapter(self):

        return {
           'chapter': {
               'section_name': self.chapter_name(),
               'title': self.chapter_title(),
           },
           'sections': self.parsed_sections(),
        }


def tag_builder(prefix, chapter, section=None, level=None, point=None):
    builder = ''
    builder += prefix.upper()
    builder += 'CH' + str(chapter)
    if section is not None: builder += 'S' + str(section)
    if level is not None: builder += 'L' + str(level)
    if point is not None: builder += '-' + str(point)

    return builder

def create_chapter_csv_fragment(prefix, ref_model, grade_range, chapter_num, parsed_chapter):

    csv_frag = []
    chapter = parsed_chapter['chapter']
    sections = parsed_chapter['sections']

    print(f"STARTING: Chapter {chapter['section_name']}")

    csv_frag.append({
        'ref_model': ref_model,
        'guideline_tag': tag_builder(prefix, chapter_num),
        'section_type': 'Chapter',
        'section_no': chapter_num,
        'section_name': chapter['section_name'],
        'title': chapter['title'],
        'active': 'TRUE',
        })

    for i, section in enumerate(sections):
        print(f"STARTING: Chapter {chapter['section_name']} - Section {section['section']['section_name']}")

        section_num = i + 1
        csv_frag.append({
            'parent_tag': tag_builder(prefix, chapter_num),
            'ref_model': ref_model,
            'guideline_tag': tag_builder(prefix, chapter_num, section_num),
            'section_type': 'Section',
            'section_no': section_num,
            'section_name': f"{chapter_num}.{section_num}",
            'title': section['section']['title'],
            'content': section['section']['content'],
            'note_header': section['section']['note_header'],
            'active': 'TRUE',
        })

        for j in range(len(grade_range)):
            level_num = j + 1

            level = section['levels'][grade_range[j]]

            csv_frag.append({
                'parent_tag': tag_builder(prefix, chapter_num, section_num),
                'ref_model': ref_model,
                'guideline_tag': tag_builder(prefix, chapter_num, section_num, level_num),
                'section_type': 'Level',
                'section_no': level_num,
                'section_name': grade_range[j],
                'title': 'Level ' + grade_range[j],
                'active': 'TRUE',
            })

            for k, point in enumerate(level):
                point_num = k + 1
                csv_frag.append({
                'parent_tag': tag_builder(prefix, chapter_num, section_num, level_num),
                'ref_model': ref_model,
                'guideline_tag': tag_builder(prefix, chapter_num, section_num, level_num, point_num),
                'section_type': 'Point',
                'section_no': point_num,
                'section_name': f'{grade_range[j]}-{point_num}',
                'title': f'{grade_range[j]}-{point_num}',
                'content': point,
                'active': 'TRUE',
            })
    
    return csv_frag


if (len(sys.argv) < 3):
    sys.exit(f'You must provide filenames after {sys.argv[0]} (ex. {sys.argv[0]} <input file> <output file>')


input_file = sys.argv[1]
output_file = sys.argv[2]

if not os.path.isfile(input_file):
    sys.exit(f'{input_file} does not exist.')


print("===== OPEX CSV Maker =====")
print("NOTE 1: Each level point must be on it's own paragraph, the program is looking for double line breaks.")
print("NOTE 2: Program doesn't follow links on the spreadsheet, you must only provide values in each cell.")

looping = True

while looping:
    user_input = input('Are you ready? [y/n]: ')

    if user_input.upper() == 'Y':
        looping = False
    elif user_input.upper() == 'N':
        sys.exit('Exiting!')
        
# set parameters

user_input = input('What sheet is the OPEX guidelines on? [1-MAXINT]: ')

if not user_input.isdigit():
    sys.exit("That's not an integer...")
elif int(user_input) == 0:
    sys.exit("Can't use 0.")

wb = load_workbook(input_file)
ws = wb[wb.sheetnames[int(user_input) - 1]]

prefix = 'OPEX'
ref_model = 'opex_assessment'
grade_range = ['E', 'D', 'C', 'B']
chapter_num = 1

new_csv = []

for row in ws.rows:
    if "Axis" in str(row[0].value):


        chapter = OPEXChapter(ws, row[0].row)

        new_csv += create_chapter_csv_fragment(
            prefix, ref_model, grade_range, chapter_num, chapter.parsed_chapter())
        
        chapter_num += 1

print('finished building CSV file!')

with open('opex-raw.csv', mode='w') as csv_file:
    fieldnames = [
        'parent_tag',
        'ref_model',
        'guideline_tag',
        'section_type',
        'section_no',
        'section_name',
        'title',
        'content',
        'active',
        'note_header',
        'note_bullets'
        ]
    writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
    print('Writing CSV file!')
    writer.writeheader()

    for x in new_csv:
        writer.writerow(x)



# TODO: trim whitespace
# chapter_section_name, chapter_title = chapter_cell.value.split(':')
# section_section_name, section_title = section_cell.value.split(':')
# paragraphs1 = re.split('\n\n', e_cell.value)
# paragraphs2 = re.split('\n\n', d_cell.value)

# print('chapter_section_name:', chapter_section_name)
# print('chapter_title:', chapter_title)
# print('section_section_name:', section_section_name)
# print('section_title:', section_title)
# print('com cell1:', comment_cell1.value)
# print('com_cell2:', comment_cell2.value)
# print('e cell:', paragraphs1)
# print('d_cell:', paragraphs2)
# print('c_cell', c_cell.value)
# print('b_cell:', b_cell.value)







