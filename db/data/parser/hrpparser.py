from openpyxl import load_workbook
import sys, os
import re


import csv

class Section():

    def __init__(self, ws, start_row):

        self.ws = ws
        self.section_start = self.ws.cell(start_row, 4)
        self.section_title_cell = self.ws.cell(start_row, 5)

    def section_name(self):

        return self.section_title_cell.value.split('-', 1)[0].strip()

    def section_title(self):
        print(self.section_title_cell.value)
        return self.section_title_cell.value.split('-', 1)[1].strip()

    def parsed_section(self):

        e_contents = []
        d_contents = []
        c_contents = []
        b_contents = []

        for i in range (1, 50):

            section_level = self.section_start.offset(row=i)

            if section_level.value is None:
                pass
            else:
                if '0.' in section_level.value:
                    e_contents.append(section_level.offset(column=1).value)
                elif '1.' in section_level.value:
                    d_contents.append(section_level.offset(column=1).value)
                elif '2.' in section_level.value:
                    c_contents.append(section_level.offset(column=1).value)
                elif '3.' in section_level.value:
                    b_contents.append(section_level.offset(column=1).value)
                elif 'REQUIREMENT' in section_level.value:
                    break

        return {
            'section': {
                'name': self.section_name(),
                'title': self.section_title(),
                'levels': {
                    'E': e_contents,
                    'D': d_contents,
                    'C': c_contents,
                    'B': b_contents,
                },
            }
        }


def tag_builder(prefix, chapter, section=None, level=None, point=None):

    builder = ''
    builder += prefix.upper()
    builder += 'CH' + str(chapter)
    if section is not None: builder += 'S' + str(section)
    if level is not None: builder += 'L' + str(level)
    if point is not None: builder += '-' + str(point)

    return builder


def create_chapter_fragment(prefix, ref_model, chapter_num, chapter_title):

    return {
        'ref_model': ref_model,
        'guideline_tag': tag_builder(prefix, chapter_num),
        'section_type': 'Chapter',
        'section_no': chapter_num,
        'section_name': chapter_num,
        'title': chapter_title,
        'active': 'TRUE',
    }



def create_section_fragment(prefix, ref_model, grade_range, chapter_num, section_num, parsed_section):

    csv_frag = []

    print(f"STARTING: Section {parsed_section['section']['name']}")

    csv_frag.append({
        'parent_tag': tag_builder(prefix, chapter_num),
        'ref_model': ref_model,
        'guideline_tag': tag_builder(prefix, chapter_num, section_num),
        'section_type': 'Section',
        'section_no': section_num,
        'section_name': f"{chapter_num}.{section_num}",
        'title': parsed_section['section']['title'],
        'active': 'TRUE',
    })

    for j in range(len(grade_range)):

        level = parsed_section['section']['levels'][grade_range[j]]

        csv_frag.append({
                'parent_tag': tag_builder(prefix, chapter_num, section_num),
                'ref_model': ref_model,
                'guideline_tag': tag_builder(prefix, chapter_num, section_num, j+1),
                'section_type': 'Level',
                'section_no': j+1,
                'section_name': grade_range[j],
                'title': 'Level ' + grade_range[j],
                'active': 'TRUE',
            })

        for k, point in enumerate(level):

            csv_frag.append({
                'parent_tag': tag_builder(prefix, chapter_num, section_num, j+1),
                'ref_model': ref_model,
                'guideline_tag': tag_builder(prefix, chapter_num, section_num, j+1, k+1),
                'section_type': 'Point',
                'section_no': k+1,
                'section_name': f'{grade_range[j]}-{k+1}',
                'title': f'{grade_range[j]}-{k+1}',
                'content': point,
                'active': 'TRUE',
            })

    return csv_frag


if (len(sys.argv) < 3):
    sys.exit(f'You must provide filenames after {sys.argv[0]} (ex. {sys.argv[0]} <input file> <output file>')


input_file = sys.argv[1]
output_file = sys.argv[2]

if not os.path.isfile(input_file):
    sys.exit(f'{input_file} does not exist.')

print("===== HRP CSV Maker =====")

looping = True

while looping:
    user_input = input('Are you ready? [y/n]: ')

    if user_input.upper() == 'Y':
        looping = False
    elif user_input.upper() == 'N':
        sys.exit('Exiting!')

# set parameters

user_input = input('What sheet is the HRP guidelines on? [1-MAXINT]: ')

if not user_input.isdigit():
    sys.exit("That's not an integer...")
elif int(user_input) == 0:
    sys.exit("Can't use 0.")

wb = load_workbook(input_file)
ws = wb[wb.sheetnames[int(user_input) - 1]]

prefix = 'HRP'
ref_model = 'hrp_assessment'
grade_range = ['E', 'D', 'C', 'B']




chapter_num = 1
section_num = 1
header = True
new_csv = []
for row in ws.rows:

    if "REQUIREMENT" in str(row[3].value):
        if header == True:
            # there's a header that has the same name as the section titles, skipping it with this
            header = False
            pass
        else:
            if section_num == 1:
                new_csv += [create_chapter_fragment(prefix, ref_model, chapter_num, 'Basic Responsibilities')]
            elif section_num == 4:
                chapter_num = 2
                new_csv += [create_chapter_fragment(prefix, ref_model, chapter_num, 'Health & Safety')]
            elif section_num == 9:
                chapter_num = 3
                new_csv += [create_chapter_fragment(prefix, ref_model, chapter_num, 'Operations')]

            section = Section(ws, row[3].row)
            if section_num < 4:
                new_csv += create_section_fragment(
                prefix, ref_model, grade_range, chapter_num, section_num, section.parsed_section())
            elif section_num < 9:
                new_csv += create_section_fragment(
                prefix, ref_model, grade_range, chapter_num, section_num - 3, section.parsed_section())
            else:
                new_csv += create_section_fragment(
                prefix, ref_model, grade_range, chapter_num, section_num - 8, section.parsed_section())

            section_num += 1


if new_csv == []:
    sys.exit('There was a problem, nothing was parsed...')
else:
    print('finished building CSV file!')

with open(output_file, mode='w') as csv_file:
    fieldnames = [
        'parent_tag',
        'ref_model',
        'guideline_tag',
        'section_type',
        'section_no',
        'section_name',
        'title',
        'content',
        'active',
        'note_header',
        'note_bullets'
        ]
    writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
    print('Writing CSV file!')
    writer.writeheader()

    for x in new_csv:
        writer.writerow(x)

print(f'Write complete to {output_file}!')


# TODO: trim whitespace
# chapter_section_name, chapter_title = chapter_cell.value.split(':')
# section_section_name, section_title = section_cell.value.split(':')
# paragraphs1 = re.split('\n\n', e_cell.value)
# paragraphs2 = re.split('\n\n', d_cell.value)

# print('chapter_section_name:', chapter_section_name)
# print('chapter_title:', chapter_title)
# print('section_section_name:', section_section_name)
# print('section_title:', section_title)
# print('com cell1:', comment_cell1.value)
# print('com_cell2:', comment_cell2.value)
# print('e cell:', paragraphs1)
# print('d_cell:', paragraphs2)
# print('c_cell', c_cell.value)
# print('b_cell:', b_cell.value)







