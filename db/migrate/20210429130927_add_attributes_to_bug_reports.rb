class AddAttributesToBugReports < ActiveRecord::Migration[5.2]
  def change
    add_column :bug_reports, :assignee, :integer, default: 0
    add_column :bug_reports, :priority, :integer, default: 0
    add_column :bug_reports, :resolved_at, :datetime
  end
end
