class AddResponseChoicesToGuidelines < ActiveRecord::Migration[5.2]
  def change
    add_column :guidelines, :response_choices, :text, default: ['OK', 'NOK', 'Not Applicable', 'Not Assessed'], array: true
    add_column :dpr_guidelines, :response_choices, :text, default: ['OK', 'NOK', 'Not Applicable', 'Not Assessed'], array: true
  end
end
