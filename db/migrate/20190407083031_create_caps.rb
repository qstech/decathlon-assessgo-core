class CreateCaps < ActiveRecord::Migration[5.2]
  def change
    create_table :caps do |t|
      t.string     :ref_model
      t.integer    :ref_id
      t.datetime   :cap_date
      t.string     :cap_type
      t.references :supplier_profile_id
      t.bigint     :pl_id
      t.text       :comments
      t.bigint     :follow_up_for

      t.timestamps
    end
  end
end
