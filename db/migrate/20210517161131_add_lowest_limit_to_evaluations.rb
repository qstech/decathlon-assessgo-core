class AddLowestLimitToEvaluations < ActiveRecord::Migration[5.2]
  def change
    add_column :evaluations, :lowest_limit, :string, default: "E"
  end
end
