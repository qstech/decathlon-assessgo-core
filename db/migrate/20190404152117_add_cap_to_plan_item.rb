class AddCapToPlanItem < ActiveRecord::Migration[5.2]
  def change
    add_column :plan_items, :cap, :boolean, default: false
  end
end
