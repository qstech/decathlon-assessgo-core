class AddDestroyedAtToProductFamilies < ActiveRecord::Migration[5.2]
  def change
    add_column :product_families, :destroyed_at, :datetime
  end
end
