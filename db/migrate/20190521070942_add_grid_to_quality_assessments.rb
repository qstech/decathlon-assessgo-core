class AddGridToQualityAssessments < ActiveRecord::Migration[5.2]
  def change
    add_column :quality_assessments, :grid, :string, default: 'quality_assessment'
  end
end
