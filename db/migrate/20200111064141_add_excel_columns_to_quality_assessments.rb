class AddExcelColumnsToQualityAssessments < ActiveRecord::Migration[5.2]
  def change
    add_column :quality_assessments, :excel_date, :datetime
    add_column :quality_assessments, :excel_state, :string
  end
end
