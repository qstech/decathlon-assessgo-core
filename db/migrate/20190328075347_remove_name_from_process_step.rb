class RemoveNameFromProcessStep < ActiveRecord::Migration[5.2]
  def change
    remove_column :process_steps, :name
  end
end
