class AddPeopleConcernedToQualityAssessment < ActiveRecord::Migration[5.2]
  def change
    add_column :quality_assessments, :people_concerned, :text, default: [], array: true
  end
end
