class AddFieldsToGuidelines < ActiveRecord::Migration[5.2]
  def change
    add_column :guidelines, :section_type, :string
    add_column :guidelines, :section_no, :integer
  end
end
