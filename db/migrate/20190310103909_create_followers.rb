class CreateFollowers < ActiveRecord::Migration[5.2]
  def change
    create_table :followers do |t|
      t.references :user, foreign_key: true
      t.references :follower, foreign_key: {to_table: :users}, index: true
      t.boolean :active

      t.timestamps
    end
  end
end
