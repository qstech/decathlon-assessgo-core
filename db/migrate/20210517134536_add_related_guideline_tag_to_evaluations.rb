class AddRelatedGuidelineTagToEvaluations < ActiveRecord::Migration[5.2]
  def change
    add_column :evaluations, :related_guideline_tag, :string
  end
end
