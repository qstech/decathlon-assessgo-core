class AddClosedToSupplierProfiles < ActiveRecord::Migration[5.2]
  def change
    add_column :supplier_profiles, :closed, :boolean, default: false
  end
end
