class AddTechnicalEvaluationToAssessorApplications < ActiveRecord::Migration[5.2]
  def change
    add_column :assessor_applications, :technical_eval, :text
  end
end
