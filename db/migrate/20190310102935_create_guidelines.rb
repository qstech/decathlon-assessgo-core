class CreateGuidelines < ActiveRecord::Migration[5.2]
  def change
    create_table :guidelines do |t|
      t.integer :parent_id
      t.string :ref_model
      t.string :guideline_tag
      t.string :title
      t.text :content
      t.boolean :active, default: true
      t.datetime :destroyed_at

      t.timestamps
    end
  end
end
