class CreateSupplierProfiles < ActiveRecord::Migration[5.2]
  def change
    create_table :supplier_profiles do |t|
      t.string :code
      t.string :name
      t.string :address
      t.string :contact_name
      t.string :contact_email
      t.string :target
      t.boolean :active
      t.string :status

      t.timestamps
    end
  end
end
