class AddSystemTagToGuidelines < ActiveRecord::Migration[5.2]
  def change
    add_column :guidelines, :system_tag, :string
  end
end
