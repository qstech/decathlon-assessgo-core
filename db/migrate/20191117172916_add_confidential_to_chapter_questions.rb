class AddConfidentialToChapterQuestions < ActiveRecord::Migration[5.2]
  def change
    add_column :chapter_questions, :confidential, :boolean, default: false
  end
end
