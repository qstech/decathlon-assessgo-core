class AddInviteDateToSupplierProfiles < ActiveRecord::Migration[5.2]
  def change
    add_column :supplier_profiles, :invite_sent_at, :datetime
  end
end
