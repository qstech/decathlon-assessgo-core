class AddCreatedByToControlPlans < ActiveRecord::Migration[5.2]
  def change
    add_column :control_plans, :created_by, :bigint
  end
end
