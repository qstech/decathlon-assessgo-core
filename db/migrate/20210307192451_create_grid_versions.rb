class CreateGridVersions < ActiveRecord::Migration[5.2]
  def change
    create_table :grid_versions do |t|
      t.string :grid
      t.integer :version_number
      t.string :version_name
      t.boolean :active, default: true
      t.string :short_name

      t.timestamps
    end

    add_reference :quality_assessments, :grid_version, foreign_key: true
    add_reference :guidelines, :grid_version, foreign_key: true
    add_reference :form_questions, :grid_version, foreign_key: true
  end
end
