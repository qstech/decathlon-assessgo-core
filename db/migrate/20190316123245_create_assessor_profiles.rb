class CreateAssessorProfiles < ActiveRecord::Migration[5.2]
  def change
    create_table :assessor_profiles do |t|
      t.bigint :user_id
      t.string :handle
      t.text :assessor_for, default: [], array: true
      t.text :candidate_for, default: [], array: true

      t.timestamps
    end
  end
end
