class AddPreviousScoreToQualityAssessments < ActiveRecord::Migration[5.2]
  def change
    add_column :quality_assessments, :previous_score, :string
  end
end
