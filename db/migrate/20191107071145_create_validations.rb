class CreateValidations < ActiveRecord::Migration[5.2]
  def change
    create_table :validations do |t|
      t.references :assessor_application, foreign_key: true, index: true
      t.references :quality_assessment, foreign_key: true, index: true
      t.references :control_plan, foreign_key: true, index: true
      t.string :result
      t.boolean :validated, default: false
      t.text :comments

      t.timestamps
    end
  end
end
