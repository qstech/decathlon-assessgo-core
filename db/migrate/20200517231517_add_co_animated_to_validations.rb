class AddCoAnimatedToValidations < ActiveRecord::Migration[5.2]
  def change
    add_column :validations, :co_animated, :boolean, default: false
  end
end
