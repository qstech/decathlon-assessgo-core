class AddFindingsProcessToPlanItems < ActiveRecord::Migration[5.2]
  def change
    add_column :plan_items, :findings_process, :text
  end
end
