class ChangeCcToArray < ActiveRecord::Migration[5.2]
  def change
    remove_column :mail_bags, :cc
    remove_column :mail_bags, :bcc
    remove_column :letters, :cc
    remove_column :letters, :bcc
    add_column :mail_bags, :cc, :text, default: [], array: true
    add_column :mail_bags, :bcc, :text, default: [], array: true
    add_column :letters, :cc, :text, default: [], array: true
    add_column :letters, :bcc, :text, default: [], array: true
  end
end
