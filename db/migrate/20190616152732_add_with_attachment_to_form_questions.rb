class AddWithAttachmentToFormQuestions < ActiveRecord::Migration[5.2]
  def change
    add_column :form_questions, :require_attachment, :boolean, default: false
  end
end
