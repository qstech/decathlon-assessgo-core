class AddProcessToQualityAssessments < ActiveRecord::Migration[5.2]
  def change
    add_column :quality_assessments, :process, :integer
  end
end
