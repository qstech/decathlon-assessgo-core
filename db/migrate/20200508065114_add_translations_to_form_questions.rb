class AddTranslationsToFormQuestions < ActiveRecord::Migration[5.2]
  def change
    add_column :form_questions, :label_translations, :jsonb
  end
end
