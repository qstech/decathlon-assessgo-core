class AddGuidanceAndDocumentsToRoutineQuestions < ActiveRecord::Migration[5.2]
  def change
    add_column :routine_questions, :guidance_definitions_translations, :jsonb
    add_column :routine_questions, :documents_to_check_translations, :jsonb
  end
end
