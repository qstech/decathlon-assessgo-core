class CreateControlPlans < ActiveRecord::Migration[5.2]
  def change
    create_table :control_plans do |t|
      t.references :product_family, foreign_key: true
      t.references :assessor_profile, foreign_key: true
      t.string :plan_type
      t.text :ref_note
      t.string :kpi_type

      t.timestamps
    end
  end
end
