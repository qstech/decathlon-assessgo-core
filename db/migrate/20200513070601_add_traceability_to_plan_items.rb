class AddTraceabilityToPlanItems < ActiveRecord::Migration[5.2]
  def change
    add_column :plan_items, :traceability, :text
  end
end
