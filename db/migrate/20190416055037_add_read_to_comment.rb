class AddReadToComment < ActiveRecord::Migration[5.2]
  def change
    add_column :comments, :receiver_read, :boolean, default: false
  end
end
