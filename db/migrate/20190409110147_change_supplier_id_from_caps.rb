class ChangeSupplierIdFromCaps < ActiveRecord::Migration[5.2]
  def change
    remove_reference :caps, :supplier_profile_id
    add_reference :caps, :supplier_profile, foreign_key: true
  end
end
