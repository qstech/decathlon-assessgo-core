class AddLastPopupToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :last_popup, :datetime
  end
end
