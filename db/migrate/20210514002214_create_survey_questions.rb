class CreateSurveyQuestions < ActiveRecord::Migration[5.2]
  def change
    create_table :survey_questions do |t|
      t.belongs_to :quality_assessment
      t.belongs_to :form_question
      t.string :question_tag
      t.string :response
      t.integer :result, default: 0
      t.text :remarks
      t.datetime :destroyed_at
      t.string :respondant

      t.timestamps
    end
  end
end
