class AddTranslationsToRoutineQuestions < ActiveRecord::Migration[5.2]
  def change
    add_column :routine_questions, :questions_translations, :jsonb
    add_column :routine_questions, :how_to_check_translations, :jsonb
  end
end
