class AddLoginNotifiedAtToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :login_notified_at, :datetime, default: -> { 'CURRENT_TIMESTAMP' }
  end
end
