class CreateBugReports < ActiveRecord::Migration[5.2]
  def change
    create_table :bug_reports do |t|
      t.string :bug_type
      t.string :message
      t.text :backtrace, default: [], array: true
      t.string :controller_action
      t.string :request_path
      t.jsonb :request_params
      t.jsonb :user_params
      t.integer :app, default: 0
      t.integer :crash_level, default: 0
      t.integer :status, default: 0
      t.integer :feature
      t.string :supplier_code
      t.text :user_description

      t.timestamps
    end
  end
end
