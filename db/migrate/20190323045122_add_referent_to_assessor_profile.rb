class AddReferentToAssessorProfile < ActiveRecord::Migration[5.2]
  def change
    add_column :assessor_profiles, :referent_for, :text, default: [], array: true
    add_column :assessor_profiles, :ref_candidate_for, :text, default: [], array: true
  end
end
