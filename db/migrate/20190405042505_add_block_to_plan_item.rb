class AddBlockToPlanItem < ActiveRecord::Migration[5.2]
  def change
    add_column :plan_items, :block, :boolean, default: false
  end
end
