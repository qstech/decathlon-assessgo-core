class AddAuthenticatedUntilToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :authenticated_until, :datetime, default: -> { 'CURRENT_TIMESTAMP' }
  end
end
