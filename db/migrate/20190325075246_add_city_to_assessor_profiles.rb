class AddCityToAssessorProfiles < ActiveRecord::Migration[5.2]
  def change
    add_column :assessor_profiles, :city, :string
  end
end
