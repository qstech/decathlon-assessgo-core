class ChangeForeignKeyFromCpPendingFrequency < ActiveRecord::Migration[5.2]
  def change
    remove_reference :cp_pending_frequencies, :process_steps
    add_reference :cp_pending_frequencies, :process_step, foreign_key: true, null: false
  end
end
