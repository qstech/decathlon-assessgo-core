class AddStatusToQualityAssessments < ActiveRecord::Migration[5.2]
  def change
    add_column :quality_assessments, :status, :string
  end
end
