class CreateCapFollowups < ActiveRecord::Migration[5.2]
  def change
    create_table :cap_followups do |t|
      t.references :cap_resolution, foreign_key: true
      t.string :result
      t.datetime :followup_date
      t.text :comments
      t.boolean :resolved

      t.timestamps
    end
  end
end
