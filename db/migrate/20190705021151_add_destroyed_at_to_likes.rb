class AddDestroyedAtToLikes < ActiveRecord::Migration[5.2]
  def change
    add_column :likes, :destroyed_at, :datetime
  end
end
