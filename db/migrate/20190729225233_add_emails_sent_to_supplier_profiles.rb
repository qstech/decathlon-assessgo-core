class AddEmailsSentToSupplierProfiles < ActiveRecord::Migration[5.2]
  def change
    add_column :supplier_profiles, :emails_sent, :text, default: [], array: true
  end
end
