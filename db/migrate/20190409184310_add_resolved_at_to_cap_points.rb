class AddResolvedAtToCapPoints < ActiveRecord::Migration[5.2]
  def change
    add_column :cap_points, :resolved_at, :datetime
  end
end
