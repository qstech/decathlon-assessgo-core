class CreateSearchHistories < ActiveRecord::Migration[5.2]
  def change
    create_table :search_histories do |t|
      t.references :user, foreign_key: true, null: false
      t.string :query, null: false
      t.text :options, default: [], array: true

      t.timestamps
    end
  end
end
