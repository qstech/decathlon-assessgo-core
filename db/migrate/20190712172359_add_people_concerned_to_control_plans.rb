class AddPeopleConcernedToControlPlans < ActiveRecord::Migration[5.2]
  def change
    add_column :control_plans, :people_concerned, :text, default: [], array: true
  end
end
