class AddFieldsToControlPlans < ActiveRecord::Migration[5.2]
  def change
    add_column :control_plans, :assess_date, :datetime, null: false, default: -> { 'CURRENT_TIMESTAMP' }
    add_column :control_plans, :completed, :boolean, default: false, null: false
    add_column :control_plans, :nonconformity_rate, :string, default: "0"
    add_column :control_plans, :date_confirmed, :datetime
    add_column :control_plans, :completed_at, :datetime
  end
end
