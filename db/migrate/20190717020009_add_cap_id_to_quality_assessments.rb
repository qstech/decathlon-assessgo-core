class AddCapIdToQualityAssessments < ActiveRecord::Migration[5.2]
  def change
    add_column :quality_assessments, :cap_id, :bigint
    add_column :control_plans, :cap_id, :bigint
  end
end
