class CreateChangeLogs < ActiveRecord::Migration[5.2]
  def change
    create_table :change_logs do |t|
      t.string :version
      t.text :change_list, default: [], array: true
      t.text :viewed_by, default: [], array: true

      t.timestamps
    end
  end
end
