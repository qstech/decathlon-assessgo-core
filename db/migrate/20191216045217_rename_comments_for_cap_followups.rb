class RenameCommentsForCapFollowups < ActiveRecord::Migration[5.2]
  def change
    rename_column :cap_followups, :comments, :notes
  end
end
