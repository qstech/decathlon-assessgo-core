class CreateGoesActivities < ActiveRecord::Migration[5.2]
  def change
    create_table :goes_activities do |t|
      t.references :user, foreign_key: true
      t.string :ref_model
      t.integer :ref_id
      t.string :ref_type
      t.string :ref_status
      t.text :content

      t.timestamps
    end
  end
end
