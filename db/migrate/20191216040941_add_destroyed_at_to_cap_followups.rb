class AddDestroyedAtToCapFollowups < ActiveRecord::Migration[5.2]
  def change
    add_column :cap_followups, :destroyed_at, :datetime
  end
end
