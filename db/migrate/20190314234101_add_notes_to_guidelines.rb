class AddNotesToGuidelines < ActiveRecord::Migration[5.2]
  def change
    add_column :guidelines, :note_header, :string
    add_column :guidelines, :note_bullets, :text, default: [], array: true
  end
end
