class ChangeActiveDefaultInProcessSteps < ActiveRecord::Migration[5.2]
  def change
    change_column :process_steps, :active, :boolean, default: true
  end
end
