class AddInvitedPlsToCaps < ActiveRecord::Migration[5.2]
  def change
    add_column :caps, :invited_pls, :text, default: [], array: true
  end
end
