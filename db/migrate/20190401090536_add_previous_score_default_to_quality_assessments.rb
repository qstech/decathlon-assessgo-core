class AddPreviousScoreDefaultToQualityAssessments < ActiveRecord::Migration[5.2]
  def change
    change_column :quality_assessments, :previous_score, :string, default: "N/A"
  end
end
