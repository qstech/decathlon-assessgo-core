class ReviseFieldsFromFormQuestions < ActiveRecord::Migration[5.2]
  def change
    remove_column :form_questions, :text
    add_column    :form_questions, :label, :string
  end
end
