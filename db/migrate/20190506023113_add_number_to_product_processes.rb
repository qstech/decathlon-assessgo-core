class AddNumberToProductProcesses < ActiveRecord::Migration[5.2]
  def change
    add_column :product_processes, :number, :string, default: "000"
  end
end
