class AddParamsToLetters < ActiveRecord::Migration[5.2]
  def change
    add_column :letters, :params, :jsonb, default: {}
  end
end
