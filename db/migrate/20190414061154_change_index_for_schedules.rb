class ChangeIndexForSchedules < ActiveRecord::Migration[5.2]
  def change
    remove_foreign_key :schedules, column: :recipient_id
    # remove_reference :schedules, :recipient
    add_foreign_key :schedules, :supplier_profiles, column: :recipient_id
  end
end
