class AddFieldsToSupplierProfiles < ActiveRecord::Migration[5.2]
  def change
    add_column :supplier_profiles, :industrial_process, :string
    add_column :supplier_profiles, :cpm, :string
    add_column :supplier_profiles, :finished_goods_supplier, :boolean, default: false
    add_column :supplier_profiles, :component_supplier, :boolean, default: false
    add_column :supplier_profiles, :country, :string
    add_column :supplier_profiles, :city, :string
    add_column :supplier_profiles, :partner, :boolean, default: false
  end
end
