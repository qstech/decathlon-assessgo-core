class CreateQualityAssessments < ActiveRecord::Migration[5.2]
  def change
    create_table :quality_assessments do |t|
      t.references :supplier, foreign_key: true
      t.string :supplier_code
      t.string :assess_type
      t.datetime :assess_date
      t.text :prepped_by, default: [], array: true
      t.text :assessed_by, default: [], array: true

      t.timestamps
    end
  end
end
