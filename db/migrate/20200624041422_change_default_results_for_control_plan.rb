class ChangeDefaultResultsForControlPlan < ActiveRecord::Migration[5.2]
  def change
    change_column_default :plan_items, :result, nil
  end
end
