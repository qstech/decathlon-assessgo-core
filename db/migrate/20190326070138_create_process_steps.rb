class CreateProcessSteps < ActiveRecord::Migration[5.2]
  def change
    create_table :process_steps do |t|
      t.string :name
      t.references :product_process, foreign_key: true
      t.string :step_number
      t.string :category
      t.string :control_item
      t.text :control_method
      t.string :requirement
      t.integer :criticity
      t.string :control_type
      t.string :supplier_control_freq
      t.string :supplier_controller
      t.string :decathlon_control_freq

      t.timestamps
    end
  end
end
