class CreateChapterAssessments < ActiveRecord::Migration[5.2]
  def change
    create_table :chapter_assessments do |t|
      t.references :quality_assessment, foreign_key: true
      t.string :level
      t.string :cotation

      t.timestamps
    end
  end
end
