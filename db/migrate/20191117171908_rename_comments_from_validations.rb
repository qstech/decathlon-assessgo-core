class RenameCommentsFromValidations < ActiveRecord::Migration[5.2]
  def change
    rename_column :validations, :comments, :reason
  end
end
