class CreateSchedules < ActiveRecord::Migration[5.2]
  def change
    create_table :schedules do |t|
      t.string :ref_model
      t.integer :ref_id
      t.references :sender, foreign_key: {to_table: :users}, index: true
      t.references :recipient, foreign_key: {to_table: :users}, index: true
      t.boolean :pending, default: true
      t.boolean :accepted, default: false
      t.datetime :schedule_date

      t.timestamps
    end
  end
end
