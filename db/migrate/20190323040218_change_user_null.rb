class ChangeUserNull < ActiveRecord::Migration[5.2]
  def change
    change_column_null :users, :email, true
    change_column_null :users, :encrypted_password, true
    remove_index :users, :email
    # remove_index :users, :reset_password_token
  end
end
