class ChangeTeamLeaderConfirmationInAssessorApplication < ActiveRecord::Migration[5.2]
  def change
    rename_column :assessor_applications, :team_leader_confirmation, :team_leader_confirmed
  end
end
