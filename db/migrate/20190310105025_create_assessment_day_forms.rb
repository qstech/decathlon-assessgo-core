class CreateAssessmentDayForms < ActiveRecord::Migration[5.2]
  def change
    create_table :assessment_day_forms do |t|
      t.references :quality_assessment, foreign_key: true
      t.integer :pl_id
      t.boolean :pl_present, default: false
      t.text :observers, default: [], array:true
      t.string :people_encountered
      t.text :steps_assessed
      t.string :contact_name
      t.string :contact_email
      t.string :supplier_status
      t.string :supplier_target

      t.timestamps
    end
  end
end
