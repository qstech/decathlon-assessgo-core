class AddCompletedToCaps < ActiveRecord::Migration[5.2]
  def change
    add_column :caps, :completed, :boolean, default: false
  end
end
