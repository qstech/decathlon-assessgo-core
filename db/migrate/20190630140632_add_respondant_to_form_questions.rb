class AddRespondantToFormQuestions < ActiveRecord::Migration[5.2]
  def change
    add_column :form_questions, :respondant, :string, default: "assessor"
    add_column :prep_questions, :respondant, :string, default: "assessor"
  end
end
