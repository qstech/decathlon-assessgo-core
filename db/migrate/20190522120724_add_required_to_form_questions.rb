class AddRequiredToFormQuestions < ActiveRecord::Migration[5.2]
  def change
    add_column :form_questions, :required, :boolean, default: false
  end
end
