class AddSectionTagToGuidelines < ActiveRecord::Migration[5.2]
  def change
    add_column :guidelines, :section_name, :string
  end
end
