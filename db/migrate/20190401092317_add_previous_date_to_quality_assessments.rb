class AddPreviousDateToQualityAssessments < ActiveRecord::Migration[5.2]
  def change
    add_column :quality_assessments, :previous_date, :datetime
  end
end
