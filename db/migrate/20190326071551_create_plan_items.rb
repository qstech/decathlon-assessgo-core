class CreatePlanItems < ActiveRecord::Migration[5.2]
  def change
    create_table :plan_items do |t|
      t.references :control_plan, foreign_key: true
      t.references :process_step, foreign_key: true
      t.text :findings
      t.text :actions
      t.boolean :result

      t.timestamps
    end
  end
end
