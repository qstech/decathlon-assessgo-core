class AddSamplingEtcToProcessSteps < ActiveRecord::Migration[5.2]
  def change
    add_column :process_steps, :sampling, :text
    add_column :process_steps, :acceptance_rules, :text
    add_column :process_steps, :reaction_mode, :text
  end
end
