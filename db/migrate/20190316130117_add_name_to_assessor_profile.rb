class AddNameToAssessorProfile < ActiveRecord::Migration[5.2]
  def change
    add_column :assessor_profiles, :name, :string
  end
end
