class CreateAssessorAssignments < ActiveRecord::Migration[5.2]
  def change
    create_table :assessor_assignments do |t|
      t.string :ref_model
      t.integer :ref_id
      t.references :user, foreign_key: true
      t.boolean :primary

      t.timestamps
    end
  end
end
