class AddCreatedByToQualityAssessments < ActiveRecord::Migration[5.2]
  def change
    add_column :quality_assessments, :created_by, :bigint
  end
end
