class AddAuthenticationStatusToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :authenticated_status, :string, default: ""
  end
end
