class AddPrepForToFormQuestion < ActiveRecord::Migration[5.2]
  def change
    add_column :form_questions, :prep_for, :string, default: ''
  end
end
