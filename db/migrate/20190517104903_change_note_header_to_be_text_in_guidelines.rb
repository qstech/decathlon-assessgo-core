class ChangeNoteHeaderToBeTextInGuidelines < ActiveRecord::Migration[5.2]
  def change
    change_column :guidelines, :note_header, :text
  end
end