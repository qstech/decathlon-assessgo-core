class AddOpmsdToQualityAssessments < ActiveRecord::Migration[5.2]
  def change
    add_column :quality_assessments, :opmsd, :integer
  end
end
