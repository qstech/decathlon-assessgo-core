class AddSlugToControlPlans < ActiveRecord::Migration[5.2]
  def change
    add_column :control_plans, :slug, :string
  end
end
