class AddShortNameToSupplierProfiles < ActiveRecord::Migration[5.2]
  def change
    add_column :supplier_profiles, :short_name, :string
  end
end
