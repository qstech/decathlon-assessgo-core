class AddCapDateToCapPoints < ActiveRecord::Migration[5.2]
  def change
    add_column :cap_points, :cap_date, :datetime
  end
end
