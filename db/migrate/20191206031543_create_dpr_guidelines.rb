class CreateDprGuidelines < ActiveRecord::Migration[5.2]
  def change
    create_table :dpr_guidelines do |t|
      t.string :guideline_tag
      t.string :grid
      t.string :code
      t.string :chapter_no # 7
      t.string :chapter_name # molding
      t.string :section_no # 100
      t.string :section_name # Print
      t.string :process_name # General Process
      t.string :step_no # 102
      t.string :step_name # Heat Transfer protocol check (i.e. step name)
      t.string :from_level
      t.string :to_level
      t.text :content

      t.timestamps
    end
  end
end
