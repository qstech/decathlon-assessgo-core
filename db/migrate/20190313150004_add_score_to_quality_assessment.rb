class AddScoreToQualityAssessment < ActiveRecord::Migration[5.2]
  def change
    add_column    :quality_assessments, :result, :string
    add_column    :quality_assessments, :cap_required, :boolean, default: false
    add_column    :quality_assessments, :cotation, :integer

    remove_column :chapter_assessments, :cotation
    add_column    :chapter_assessments, :cotation, :integer

    remove_column :assessment_day_forms, :contact_name
    remove_column :assessment_day_forms, :contact_email
  end
end
