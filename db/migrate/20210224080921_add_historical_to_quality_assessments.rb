class AddHistoricalToQualityAssessments < ActiveRecord::Migration[5.2]
  def change
    add_column :quality_assessments, :historical, :boolean, default: false
  end
end
