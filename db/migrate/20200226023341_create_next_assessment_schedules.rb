class CreateNextAssessmentSchedules < ActiveRecord::Migration[5.2]
  def change
    create_table :next_assessment_schedules do |t|
      t.string :grid
      t.string :assess_type
      t.integer :next_assessment
      t.string :time_unit
      t.string :after_assess_type
      t.string :supplier_type
      t.string :result_condition
      t.text :notify_in, default: [], array: true
      t.text :notify_what, default: [], array: true
      t.boolean :active, default: true
      t.datetime :destroyed_at

      t.timestamps
    end
  end
end
