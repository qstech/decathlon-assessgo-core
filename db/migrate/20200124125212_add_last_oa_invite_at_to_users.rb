class AddLastOaInviteAtToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :last_oa_invite, :datetime
  end
end
