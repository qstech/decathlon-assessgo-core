class AddFrequencyToRoutineQuestions < ActiveRecord::Migration[5.2]
  def change
    add_column :routine_questions, :frequency_translations, :jsonb
  end
end
