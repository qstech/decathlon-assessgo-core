class DropSuppliers < ActiveRecord::Migration[5.2]
  def change
    remove_reference :quality_assessments, :supplier
    drop_table :suppliers
    add_reference :quality_assessments, :supplier_profile
  end
end
