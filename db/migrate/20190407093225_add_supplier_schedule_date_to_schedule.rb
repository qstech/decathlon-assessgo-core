class AddSupplierScheduleDateToSchedule < ActiveRecord::Migration[5.2]
  def change
    add_column :schedules, :supplier_schedule_date, :datetime
  end
end
