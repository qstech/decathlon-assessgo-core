class AddGridToFormQuestions < ActiveRecord::Migration[5.2]
  def change
    add_column :form_questions, :grid, :string, default: 'quality_assessment'
  end
end
