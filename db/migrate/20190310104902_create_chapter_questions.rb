class CreateChapterQuestions < ActiveRecord::Migration[5.2]
  def change
    create_table :chapter_questions do |t|
      t.references :chapter_assessment, foreign_key: true
      t.string :chapter_tag
      t.string :chapter
      t.string :section
      t.string :level
      t.string :point
      t.string :result
      t.text :strong_points
      t.text :points_to_improve
      t.integer :score

      t.timestamps
    end
  end
end
