class CreateFormOptions < ActiveRecord::Migration[5.2]
  def change
    create_table :form_options do |t|
      t.references :form_question, foreign_key: true
      t.integer :priority
      t.string :value
      t.string :text

      t.timestamps
    end
  end
end
