class AddGridsAvailableToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :grids_available, :text, default: [], array: true
    add_column :supplier_profiles, :grids_available, :text, default: [], array: true
  end
end
