class CreateNextAssessmentLogs < ActiveRecord::Migration[5.2]
  def change
    create_table :next_assessment_logs do |t|
      t.integer :ref_id
      t.string :ref_grid
      t.string :ref_assess_type
      t.string :next_assess_type
      t.datetime :ref_assess_date
      t.datetime :next_assess_date
      t.datetime :notify_time
      t.text :recipients, default: [], array: true
      t.boolean :sent, default: false

      t.timestamps
    end
  end
end
