class CreateMailBags < ActiveRecord::Migration[5.2]
  def change
    create_table :mail_bags do |t|
      t.string :cc
      t.string :bcc
      t.string :subject
      t.text :body
      t.string :trigger
      t.string :tag

      t.timestamps
    end
  end
end
