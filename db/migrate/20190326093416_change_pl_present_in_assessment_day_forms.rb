class ChangePlPresentInAssessmentDayForms < ActiveRecord::Migration[5.2]
  def change
    change_column :assessment_day_forms, :pl_present, :string
  end
end
