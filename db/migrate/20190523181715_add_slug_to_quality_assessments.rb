class AddSlugToQualityAssessments < ActiveRecord::Migration[5.2]
  def change
    add_column :quality_assessments, :slug, :string
  end
end
