class AddAudienceToMailBags < ActiveRecord::Migration[5.2]
  def change
    add_column :mail_bags, :audience, :text, default: [], array: true
  end
end
