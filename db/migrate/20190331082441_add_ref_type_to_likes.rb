class AddRefTypeToLikes < ActiveRecord::Migration[5.2]
  def change
    add_column :likes, :ref_type, :string
  end
end
