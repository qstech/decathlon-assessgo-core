class AddNutritionToGuidelines < ActiveRecord::Migration[5.2]
  def change
    add_column :guidelines, :nutrition, :boolean, default: false
  end
end
