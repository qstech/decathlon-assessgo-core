class AddCategoryToProcess < ActiveRecord::Migration[5.2]
  def change
    add_column :product_processes, :category, :string, default: ""
    add_column :control_plans, :assess_type, :string, default: ""
  end
end
