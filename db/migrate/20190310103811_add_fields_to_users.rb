class AddFieldsToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :wx_openid, :string
    add_column :users, :supplier, :boolean
    add_column :users, :assessor, :boolean
  end
end
