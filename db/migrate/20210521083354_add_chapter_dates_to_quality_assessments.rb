class AddChapterDatesToQualityAssessments < ActiveRecord::Migration[5.2]
  def change
    add_column :quality_assessments, :chapter_dates, :jsonb, default: {}
  end
end
