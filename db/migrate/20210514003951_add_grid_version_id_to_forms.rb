class AddGridVersionIdToForms < ActiveRecord::Migration[5.2]
  def change
    add_reference :forms, :grid_version, foreign_key: true
  end
end
