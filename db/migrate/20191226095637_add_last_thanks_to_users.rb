class AddLastThanksToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :last_thanks, :datetime
  end
end
