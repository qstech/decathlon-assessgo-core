class AddRelatedAssessmentsGridToGuidelines < ActiveRecord::Migration[5.2]
  def change
    add_column :guidelines, :related_assessments_grid, :string
  end
end
