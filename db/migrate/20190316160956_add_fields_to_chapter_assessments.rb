class AddFieldsToChapterAssessments < ActiveRecord::Migration[5.2]
  def change
    add_column :chapter_assessments, :chapter_tag, :string
    add_column :chapter_assessments, :score, :string
  end
end
