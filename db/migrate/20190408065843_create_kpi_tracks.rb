class CreateKpiTracks < ActiveRecord::Migration[5.2]
  def change
    create_table :kpi_tracks do |t|
      t.references :product_family, foreign_key: true
      t.string :name
      t.string :frequency

      t.timestamps
    end
  end
end
