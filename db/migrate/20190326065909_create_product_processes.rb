class CreateProductProcesses < ActiveRecord::Migration[5.2]
  def change
    create_table :product_processes do |t|
      t.string :name
      t.references :product_family, foreign_key: true
      t.integer :process_number
      t.timestamps
    end
  end
end
