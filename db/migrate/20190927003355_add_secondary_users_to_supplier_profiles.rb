class AddSecondaryUsersToSupplierProfiles < ActiveRecord::Migration[5.2]
  def change
    add_column :supplier_profiles, :secondary_users, :text, default: [], array: true
  end
end
