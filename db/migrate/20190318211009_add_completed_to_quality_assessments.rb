class AddCompletedToQualityAssessments < ActiveRecord::Migration[5.2]
  def change
    add_column :quality_assessments, :completed, :boolean, default: false
  end
end
