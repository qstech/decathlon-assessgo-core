class CreateAssessorApplications < ActiveRecord::Migration[5.2]
  def change
    create_table :assessor_applications do |t|
      t.references :user, foreign_key: true, index: true
      t.string :grid
      t.text :reason
      t.references :opm, foreign_key: {to_table: :assessor_profiles}, index: true
      t.references :validator, foreign_key: {to_table: :assessor_profiles}, index: true
      t.boolean :opm_confirmed
      t.text :opm_reason
      t.datetime :trained_at
      t.string :status, default: "in_progress"
      t.boolean :completed, default: false
      t.string :result

      t.timestamps
    end
  end
end
