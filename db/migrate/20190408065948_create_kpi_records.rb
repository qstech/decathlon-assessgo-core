class CreateKpiRecords < ActiveRecord::Migration[5.2]
  def change
    create_table :kpi_records do |t|
      t.references :kpi_track, foreign_key: true
      t.float :value

      t.timestamps
    end
  end
end
