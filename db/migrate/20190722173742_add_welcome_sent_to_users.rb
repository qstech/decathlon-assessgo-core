class AddWelcomeSentToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :welcome_sent, :boolean
  end
end
