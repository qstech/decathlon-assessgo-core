class SoftDeleteAllTables < ActiveRecord::Migration[5.2]
  def change
    add_column :form_questions, :destroyed_at, :datetime
    add_column :form_options, :destroyed_at, :datetime
    add_column :form_sections, :destroyed_at, :datetime
    add_column :forms, :destroyed_at, :datetime
    add_column :sub_points, :destroyed_at, :datetime
    add_column :invitations, :destroyed_at, :datetime
    add_column :comments, :destroyed_at, :datetime
    add_column :users, :destroyed_at, :datetime
    add_column :goes_activities, :destroyed_at, :datetime
    add_column :assessment_day_forms, :destroyed_at, :datetime
    add_column :chapter_questions, :destroyed_at, :datetime
    add_column :prep_questions, :destroyed_at, :datetime
    add_column :chapter_assessments, :destroyed_at, :datetime
    add_column :assessor_profiles, :destroyed_at, :datetime
    add_column :supplier_profiles, :destroyed_at, :datetime
    add_column :quality_assessments, :destroyed_at, :datetime
    add_column :product_processes, :destroyed_at, :datetime
    add_column :control_plans, :destroyed_at, :datetime
    add_column :process_steps, :destroyed_at, :datetime
    add_column :plan_items, :destroyed_at, :datetime
    add_column :cap_points, :destroyed_at, :datetime
    add_column :cap_resolutions, :destroyed_at, :datetime
    add_column :kpi_tracks, :destroyed_at, :datetime
    add_column :cp_pending_frequencies, :destroyed_at, :datetime
    add_column :caps, :destroyed_at, :datetime
    add_column :kpi_records, :destroyed_at, :datetime
  end
end
