class AddCicaToAssessorAssignments < ActiveRecord::Migration[5.2]
  def change
    add_column :assessor_applications, :cica_at, :datetime
    add_column :assessor_applications, :cica_result, :text
  end
end
