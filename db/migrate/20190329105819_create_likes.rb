class CreateLikes < ActiveRecord::Migration[5.2]
  def change
    create_table :likes do |t|
      t.references :user, foreign_key: true
      t.string :ref_model
      t.bigint :ref_id

      t.timestamps
    end
  end
end
