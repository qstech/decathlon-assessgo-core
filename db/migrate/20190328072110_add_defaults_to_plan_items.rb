class AddDefaultsToPlanItems < ActiveRecord::Migration[5.2]
  def change
    change_column :plan_items, :findings, :text, default: ''
    change_column :plan_items, :actions, :text, default: ''
    change_column :plan_items, :result, :boolean, default: false
  end
end
