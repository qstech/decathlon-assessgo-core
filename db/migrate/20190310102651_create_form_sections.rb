class CreateFormSections < ActiveRecord::Migration[5.2]
  def change
    create_table :form_sections do |t|
      t.references :form, foreign_key: true
      t.integer :priority
      t.string :title

      t.timestamps
    end
  end
end
