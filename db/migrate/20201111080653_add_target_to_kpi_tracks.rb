class AddTargetToKpiTracks < ActiveRecord::Migration[5.2]
  def change
    add_column :kpi_tracks, :target, :float
    add_column :kpi_tracks, :active, :boolean
  end
end
