class AddPlSetToQualityAssessments < ActiveRecord::Migration[5.2]
  def change
    add_column :quality_assessments, :pl_set, :text, default: [], array: true
  end
end
