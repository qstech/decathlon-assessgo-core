class CreateLetters < ActiveRecord::Migration[5.2]
  def change
    create_table :letters do |t|
      t.string :to
      t.references :mail_bag, foreign_key: true
      t.string :subject
      t.string :cc
      t.string :bcc
      t.text :body
      t.datetime :opened_at
      t.datetime :delivered_at

      t.timestamps
    end
  end
end
