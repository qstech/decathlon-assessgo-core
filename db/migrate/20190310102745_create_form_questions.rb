class CreateFormQuestions < ActiveRecord::Migration[5.2]
  def change
    create_table :form_questions do |t|
      t.references :form_section, foreign_key: true
      t.integer :priority
      t.string :question_tag
      t.string :component
      t.string :name
      t.text :text
      t.text :hint
      t.boolean :active, default: true

      t.timestamps
    end
  end
end
