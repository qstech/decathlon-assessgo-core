class AddProcessToGuidelines < ActiveRecord::Migration[5.2]
  def change
    add_column :guidelines, :process, :integer
  end
end
