class AddSectionToChapterAssessment < ActiveRecord::Migration[5.2]
  def change
    add_column :chapter_assessments, :section, :string
  end
end
