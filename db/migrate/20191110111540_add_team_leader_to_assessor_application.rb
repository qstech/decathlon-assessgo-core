class AddTeamLeaderToAssessorApplication < ActiveRecord::Migration[5.2]
  def change
    add_reference :assessor_applications, :team_leader, foreign_key: {to_table: :assessor_profiles}, index: true
    add_column :assessor_applications, :team_leader_confirmation, :boolean
    add_column :assessor_applications, :team_leader_reason, :text
  end
end
