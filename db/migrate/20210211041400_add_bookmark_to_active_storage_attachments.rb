class AddBookmarkToActiveStorageAttachments < ActiveRecord::Migration[5.2]
  def change
    add_column :active_storage_attachments, :bookmark, :boolean, default: false
  end
end
