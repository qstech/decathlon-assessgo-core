class AddReportDateToControlPlans < ActiveRecord::Migration[5.2]
  def change
    add_column :control_plans, :report_date, :datetime
  end
end
