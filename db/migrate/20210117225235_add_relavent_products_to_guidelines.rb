class AddRelaventProductsToGuidelines < ActiveRecord::Migration[5.2]
  def change
    add_column :guidelines, :relavent_products, :text, default: [], array: true
  end
end
