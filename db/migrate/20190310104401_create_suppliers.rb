class CreateSuppliers < ActiveRecord::Migration[5.2]
  def change
    create_table :suppliers do |t|
      t.references :user, foreign_key: true
      t.string :supplier_code
      t.string :name
      t.string :address
      t.string :contact_name
      t.string :contact_email

      t.timestamps
    end
  end
end
