class RemoveColumnsFromCp < ActiveRecord::Migration[5.2]
  def change
    remove_column :process_steps, :category
    remove_column :control_plans, :kpi_type
    remove_column :product_families, :current_kpi
    remove_column :product_families, :code
    remove_column :product_processes, :process_number
  end
end
