class AddLevelsToQualityAssessments < ActiveRecord::Migration[5.2]
  def change
    add_column :quality_assessments, :from_level, :string
    add_column :quality_assessments, :to_level, :string
  end
end
