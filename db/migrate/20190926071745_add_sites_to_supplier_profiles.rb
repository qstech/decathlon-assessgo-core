class AddSitesToSupplierProfiles < ActiveRecord::Migration[5.2]
  def change
    add_column :supplier_profiles, :sites, :text, default: [], array: true
  end
end
