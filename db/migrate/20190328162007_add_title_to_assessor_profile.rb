class AddTitleToAssessorProfile < ActiveRecord::Migration[5.2]
  def change
    add_column :assessor_profiles, :title, :string, default: ""
  end
end
