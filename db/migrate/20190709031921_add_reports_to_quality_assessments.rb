class AddReportsToQualityAssessments < ActiveRecord::Migration[5.2]
  def change
    add_column :quality_assessments, :report_date, :datetime
    add_column :quality_assessments, :report_state, :string
  end
end
