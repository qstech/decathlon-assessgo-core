class AddRelatedAssessmentIdToChapterQuestions < ActiveRecord::Migration[5.2]
  def change
    add_column :chapter_questions, :related_assessment_id, :bigint
  end
end
