class AddPermissionsToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :admin, :boolean, default: false
    add_column :users, :access_levels, :text, default: [], array: true
  end
end
