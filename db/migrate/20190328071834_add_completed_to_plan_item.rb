class AddCompletedToPlanItem < ActiveRecord::Migration[5.2]
  def change
    add_column :plan_items, :completed, :boolean, default: false
  end
end
