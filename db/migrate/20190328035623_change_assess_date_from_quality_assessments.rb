class ChangeAssessDateFromQualityAssessments < ActiveRecord::Migration[5.2]
  def change
    add_column :quality_assessments, :assess_dates, :text, default: [], array: true
    change_column :quality_assessments, :assess_date, :datetime, null: false
    change_column :quality_assessments, :assess_type, :string, null: false
    change_column :quality_assessments, :supplier_code, :string, null: false
  end
end
