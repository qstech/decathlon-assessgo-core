class AddToolboxTranslationsToGuidelines < ActiveRecord::Migration[5.2]
  def change
    add_column :guidelines, :toolbox_slides_translations, :jsonb
  end
end
