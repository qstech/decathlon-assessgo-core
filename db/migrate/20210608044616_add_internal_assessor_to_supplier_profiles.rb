class AddInternalAssessorToSupplierProfiles < ActiveRecord::Migration[5.2]
  def change
    add_column :supplier_profiles, :internal_assessors, :text, default: [], array: true
  end
end
