class CreateCapResolutions < ActiveRecord::Migration[5.2]
  def change
    create_table :cap_resolutions do |t|
      t.references :cap_point, foreign_key: true
      t.string :action_type
      t.text :action
      t.datetime :deadline_date
      t.string :responsible

      t.timestamps
    end
  end
end
