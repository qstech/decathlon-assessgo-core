class AddConfirmedToAssessorAssignments < ActiveRecord::Migration[5.2]
  def change
    add_column :assessor_assignments, :confirmed, :boolean, default: false
  end
end
