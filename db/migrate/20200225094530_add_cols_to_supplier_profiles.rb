class AddColsToSupplierProfiles < ActiveRecord::Migration[5.2]
  def change
    add_column :supplier_profiles, :ib_emails, :text, array: true, default: []
    add_column :supplier_profiles, :ptm_set, :text, array: true, default: []
    add_column :supplier_profiles, :opm_set, :text, array: true, default: []
    add_column :supplier_profiles, :kas, :boolean, default: false
  end
end
