class CreateCpPendingFrequencies < ActiveRecord::Migration[5.2]
  def change
    create_table :cp_pending_frequencies do |t|
      t.references :process_steps, foreign_key: true
      t.datetime :expires_at
      t.datetime :resolved_at
      t.string :frequency_for

      t.timestamps
    end
  end
end
