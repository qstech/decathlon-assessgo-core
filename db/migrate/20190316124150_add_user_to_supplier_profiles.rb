class AddUserToSupplierProfiles < ActiveRecord::Migration[5.2]
  def change
    add_column :supplier_profiles, :user_id, :bigint
  end
end
