class AddSupplierProfileToUsers < ActiveRecord::Migration[5.2]
  def change
    add_reference :users, :supplier, foreign_key: {to_table: :supplier_profiles}, index: true
  end
end
