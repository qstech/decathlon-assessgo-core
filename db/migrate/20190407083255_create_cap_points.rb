class CreateCapPoints < ActiveRecord::Migration[5.2]
  def change
    create_table :cap_points do |t|
      t.references :cap, foreign_key: true
      t.string :ref_model
      t.integer :ref_id
      t.string :section
      t.string :level
      t.string :requirement
      t.string :result
      t.string :nonconformity
      t.text :root_cause_nc
      t.text :root_cause_nd

      t.timestamps
    end
  end
end
