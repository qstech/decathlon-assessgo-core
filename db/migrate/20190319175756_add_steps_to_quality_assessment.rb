class AddStepsToQualityAssessment < ActiveRecord::Migration[5.2]
  def change
    add_column :quality_assessments, :prep_completed, :boolean, default: false
    add_column :quality_assessments, :assessor_confirmed, :boolean, default: false
    add_column :quality_assessments, :day_form_completed, :boolean, default: false
    add_column :quality_assessments, :chapters_completed, :boolean, default: false
  end
end
