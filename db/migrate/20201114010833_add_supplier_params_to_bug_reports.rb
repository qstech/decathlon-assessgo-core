class AddSupplierParamsToBugReports < ActiveRecord::Migration[5.2]
  def change
    add_column :bug_reports, :supplier_params, :jsonb
  end
end
