class AddActiveToProcessStep < ActiveRecord::Migration[5.2]
  def change
    add_column :process_steps, :active, :boolean, default: false
  end
end
