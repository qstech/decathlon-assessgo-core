class AddAssessorProviderToSupplierProfiles < ActiveRecord::Migration[5.2]
  def change
    add_column :supplier_profiles, :assessor_provider, :boolean, default: false
  end
end
