class AddWithRemarksToFormQuestions < ActiveRecord::Migration[5.2]
  def change
    add_column :form_questions, :with_remarks, :boolean, default: false
  end
end
