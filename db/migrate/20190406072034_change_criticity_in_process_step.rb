class ChangeCriticityInProcessStep < ActiveRecord::Migration[5.2]
  def change
    change_column :process_steps, :criticity, :string, default: "unassigned criticity"
  end
end
