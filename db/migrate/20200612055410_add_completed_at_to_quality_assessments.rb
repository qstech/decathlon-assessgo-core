class AddCompletedAtToQualityAssessments < ActiveRecord::Migration[5.2]
  def change
    add_column :quality_assessments, :completed_at, :datetime
  end
end
