class AddTranslationsToGuidelines < ActiveRecord::Migration[5.2]
  def change
    add_column :guidelines, :title_translations, :jsonb
    add_column :guidelines, :content_translations, :jsonb
    add_column :guidelines, :note_header_translations, :jsonb
    add_column :guidelines, :note_bullets_translations, :jsonb
  end
end
