class CreatePrepQuestions < ActiveRecord::Migration[5.2]
  def change
    create_table :prep_questions do |t|
      t.references :quality_assessment, foreign_key: true
      t.string :question_tag
      t.string :response

      t.timestamps
    end
  end
end
