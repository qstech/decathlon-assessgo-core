class AddPeopleConcernedToCaps < ActiveRecord::Migration[5.2]
  def change
    add_column :caps, :people_concerned, :text, default: [], array: true
  end
end
