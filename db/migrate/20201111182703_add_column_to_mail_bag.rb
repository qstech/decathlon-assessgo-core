class AddColumnToMailBag < ActiveRecord::Migration[5.2]
  def change
    add_column :mail_bags, :name, :string
  end
end
