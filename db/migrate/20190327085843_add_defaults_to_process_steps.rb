class AddDefaultsToProcessSteps < ActiveRecord::Migration[5.2]
  def change
    change_column :process_steps, :name, :string, default: ''
    change_column :process_steps, :step_number, :string, default: ''
    change_column :process_steps, :category, :string, default: ''
    change_column :process_steps, :control_item, :string, default: ''
    change_column :process_steps, :control_method, :text, default: ''
    change_column :process_steps, :requirement, :string, default: ''
    change_column :process_steps, :criticity, :integer, default: 0 
    change_column :process_steps, :control_type, :string, default: ''
    change_column :process_steps, :supplier_control_freq, :string, default: ''
    change_column :process_steps, :supplier_controller, :string, default: ''
    change_column :process_steps, :decathlon_control_freq, :string, default: ''
  end
end
