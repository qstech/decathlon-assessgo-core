class ChangePlanItems < ActiveRecord::Migration[5.2]
  def change
    change_column :plan_items, :result, :string, defalut: ""
    add_column :plan_items, :cap_date, :datetime
    add_column :plan_items, :orders, :text, default: [], array: true
  end
end
