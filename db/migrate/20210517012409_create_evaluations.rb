class CreateEvaluations < ActiveRecord::Migration[5.2]
  def change
    create_table :evaluations do |t|
      t.integer :limit_a
      t.integer :limit_b
      t.integer :limit_c
      t.integer :limit_d
      t.string :description
      t.references :guideline, foreign_key: true

      t.timestamps
    end
  end
end
