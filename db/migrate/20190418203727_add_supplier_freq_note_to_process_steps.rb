class AddSupplierFreqNoteToProcessSteps < ActiveRecord::Migration[5.2]
  def change
    add_column :process_steps, :supplier_control_freq_note, :string
    add_column :process_steps, :decathlon_control_freq_note, :string
  end
end
