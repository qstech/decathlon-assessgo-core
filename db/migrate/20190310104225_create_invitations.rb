class CreateInvitations < ActiveRecord::Migration[5.2]
  def change
    create_table :invitations do |t|
      t.string :ref_model
      t.integer :ref_id
      t.references :sender, foreign_key: {to_table: :users}, index: true
      t.references :recipient, foreign_key: {to_table: :users}, index: true
      t.boolean :accepted, default: false
      t.boolean :rejected, default: false

      t.timestamps
    end
  end
end
