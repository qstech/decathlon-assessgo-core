class AddUnionIdToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :unionid, :string
    add_column :users, :oa_openid, :string
  end
end
