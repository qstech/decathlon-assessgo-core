class AddToolBoxSlidesToGuidelines < ActiveRecord::Migration[5.2]
  def change
    add_column :guidelines, :toolbox_slides, :text, default: [], array: true
    add_column :guidelines, :toolbox_mode, :integer, default: 0
  end
end
