class AddPlIdToProductFamily < ActiveRecord::Migration[5.2]
  def change
    add_column :product_families, :pl_id, :bigint
  end
end
