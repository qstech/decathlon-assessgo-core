class AddActionTakenToChapterQuestions < ActiveRecord::Migration[5.2]
  def change
    add_column :chapter_questions, :action_taken, :boolean
  end
end
