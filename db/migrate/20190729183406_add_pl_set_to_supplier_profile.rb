class AddPlSetToSupplierProfile < ActiveRecord::Migration[5.2]
  def change
    add_column :supplier_profiles, :pl_set, :text, default: [], array: true
  end
end
