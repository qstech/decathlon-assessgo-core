class CreateProductFamilies < ActiveRecord::Migration[5.2]
  def change
    create_table :product_families do |t|
      t.string :name
      t.integer :code
      t.string :current_kpi
      t.integer :current_level
      t.integer :target_level
      t.references :supplier_profile, foreign_key: true

      t.timestamps
    end
  end
end
