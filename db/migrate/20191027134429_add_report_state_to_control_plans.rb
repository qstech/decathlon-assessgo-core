class AddReportStateToControlPlans < ActiveRecord::Migration[5.2]
  def change
    add_column :control_plans, :report_state, :string
  end
end
