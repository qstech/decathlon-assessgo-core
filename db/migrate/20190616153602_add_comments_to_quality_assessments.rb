class AddCommentsToQualityAssessments < ActiveRecord::Migration[5.2]
  def change
    add_column :quality_assessments, :overall_comments, :text
    add_column :quality_assessments, :strong_points, :text
    add_column :quality_assessments, :points_to_improve, :text
  end
end
