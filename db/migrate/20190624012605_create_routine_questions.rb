class CreateRoutineQuestions < ActiveRecord::Migration[5.2]
  def change
    create_table :routine_questions do |t|
      t.references :guideline, foreign_key: true
      t.text :points_to_discuss
      t.text :how_to_check
      t.text :comments

      t.timestamps
    end
  end
end
