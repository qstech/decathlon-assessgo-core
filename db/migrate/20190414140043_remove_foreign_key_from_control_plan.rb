class RemoveForeignKeyFromControlPlan < ActiveRecord::Migration[5.2]
  def change
    remove_foreign_key :control_plans, column: :assessor_profile_id
    add_column :control_plans, :assessor_type,  :string, default: "assessor"
  end
end
