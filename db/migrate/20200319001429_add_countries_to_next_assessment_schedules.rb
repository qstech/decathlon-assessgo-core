class AddCountriesToNextAssessmentSchedules < ActiveRecord::Migration[5.2]
  def change
    add_column :next_assessment_schedules, :countries, :jsonb, default: {only: [], except: []}
  end
end
