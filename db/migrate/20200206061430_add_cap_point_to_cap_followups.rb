class AddCapPointToCapFollowups < ActiveRecord::Migration[5.2]
  def change
    add_reference :cap_followups, :cap_point, foreign_key: true
  end
end
