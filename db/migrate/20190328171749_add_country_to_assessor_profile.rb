class AddCountryToAssessorProfile < ActiveRecord::Migration[5.2]
  def change
    add_column :assessor_profiles, :country, :string, default: ""
  end
end
