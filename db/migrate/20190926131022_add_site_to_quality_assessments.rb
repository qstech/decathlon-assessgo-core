class AddSiteToQualityAssessments < ActiveRecord::Migration[5.2]
  def change
    add_column :quality_assessments, :site, :string
  end
end
