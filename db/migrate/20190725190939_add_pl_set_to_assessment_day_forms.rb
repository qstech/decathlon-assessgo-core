class AddPlSetToAssessmentDayForms < ActiveRecord::Migration[5.2]
  def change
    add_column :assessment_day_forms, :pl_set, :text, default: [], array: true
    remove_column :quality_assessments, :pl_set
  end
end
