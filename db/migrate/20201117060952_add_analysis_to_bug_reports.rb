class AddAnalysisToBugReports < ActiveRecord::Migration[5.2]
  def change
    add_column :bug_reports, :analysis, :text
  end
end
