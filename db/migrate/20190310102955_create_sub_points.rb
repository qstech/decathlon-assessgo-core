class CreateSubPoints < ActiveRecord::Migration[5.2]
  def change
    create_table :sub_points do |t|
      t.references :guideline, foreign_key: true
      t.text :content

      t.timestamps
    end
  end
end
