require 'csv'
DB ="#{Rails.root}/db/seed"

STATUS = {
  step: "initializing",
  perc: 0
}


def probable_result_for(level)
  cutoff = { "E" => 100,"D" => 83,"C" => 67,"B"=>45,"A"=>12}
  return "Not Applicable" if rand(99) < 12
  rand(99) < cutoff[level] ? "OK" : ["NOK", "Not Assessed"].sample
end







# =========================ITERATE w/ Percentages =================

def each_with_perc(array)
  total = array.length
  array.each_with_index do |item, i|
    update_status_perc(i, total)
    yield(item,i)
  end
end

def parse_csv_with_perc(path, col_sep = nil)
  total = CSV.read(path).count
  csv_options = {headers: :first_row, header_converters: :symbol }
  csv_options[:col_sep] = col_sep if col_sep.present?
  CSV.foreach(path, csv_options).with_index do |row, i|
    update_status_perc(i,total)
    yield(row, i)
  end
end

def update_status_perc(i, total)
  STATUS[:perc] = " #{i}/#{total}  #{(i.to_f / total * 100).to_i}"
end

# ================================================================
# ========================FUN STUFF LIKE==========================
# ==========================ANIMATIONS============================
# ================================================================

def display_status
  @timer = Time.now
  @canvas = ""

  Thread.new {
    loop do
      new_time = Time.now
      min = (new_time - @timer).to_i / 60
      sec = (new_time - @timer - (min * 60)).round(3)

      @canvas = "#{min}:#{sec}"

      puts "#{STATUS[:step]} (#{STATUS[:perc]}%)                  "
      puts @canvas
      print "\r\e[A" + "\r\e[A"
      sleep 0.01
    end
  }
end

def success_message
  puts "
                           ******************************************
                         *                __                         *
                         *    \\_/_    ._ (_  _  _  _|                *
                         *     |(_)|_||  __)(/_(/_(_|                *
                         *                _                          *
    *****     ***        *      |_| _. _ |_o._ o _|_  _  _||         *
   **     ****   ***     *      | |(_|_> | || ||_>| |(/_(_|o         *
  ***   Q         **    *                                            *
 *****            *    *                                             *
******    *******    *                                               *
******   *             *           Total Time: #{@canvas}             *
******   *       **     *                                            *
*****     ****  *  *     ********************************************
 ****        * *   *
  ** *    * * *  **
      *   *  *  *
      *     *  *
     *         *
     *          *
    *            *
    *  *          *
    *  *           *
    *  *           *
    *  *          *
"
end
