# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_06_08_044616) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "action_text_rich_texts", force: :cascade do |t|
    t.string "name", null: false
    t.text "body"
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["record_type", "record_id", "name"], name: "index_action_text_rich_texts_uniqueness", unique: true
  end

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.boolean "bookmark", default: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "assessment_day_forms", force: :cascade do |t|
    t.bigint "quality_assessment_id"
    t.integer "pl_id"
    t.string "pl_present", default: "false"
    t.text "observers", default: [], array: true
    t.string "people_encountered"
    t.text "steps_assessed"
    t.string "supplier_status"
    t.string "supplier_target"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "destroyed_at"
    t.text "pl_set", default: [], array: true
    t.index ["quality_assessment_id"], name: "index_assessment_day_forms_on_quality_assessment_id"
  end

  create_table "assessor_applications", force: :cascade do |t|
    t.bigint "user_id"
    t.string "grid"
    t.text "reason"
    t.bigint "opm_id"
    t.bigint "validator_id"
    t.boolean "opm_confirmed"
    t.text "opm_reason"
    t.datetime "trained_at"
    t.string "status", default: "in_progress"
    t.boolean "completed", default: false
    t.string "result"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "team_leader_id"
    t.boolean "team_leader_confirmed"
    t.text "team_leader_reason"
    t.text "technical_eval"
    t.datetime "cica_at"
    t.text "cica_result"
    t.index ["opm_id"], name: "index_assessor_applications_on_opm_id"
    t.index ["team_leader_id"], name: "index_assessor_applications_on_team_leader_id"
    t.index ["user_id"], name: "index_assessor_applications_on_user_id"
    t.index ["validator_id"], name: "index_assessor_applications_on_validator_id"
  end

  create_table "assessor_assignments", force: :cascade do |t|
    t.string "ref_model"
    t.integer "ref_id"
    t.bigint "user_id"
    t.boolean "primary"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "confirmed", default: false
    t.index ["user_id"], name: "index_assessor_assignments_on_user_id"
  end

  create_table "assessor_profiles", force: :cascade do |t|
    t.bigint "user_id"
    t.string "handle"
    t.text "assessor_for", default: [], array: true
    t.text "candidate_for", default: [], array: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name"
    t.text "referent_for", default: [], array: true
    t.text "ref_candidate_for", default: [], array: true
    t.string "city"
    t.string "title", default: ""
    t.string "country", default: ""
    t.datetime "destroyed_at"
  end

  create_table "authentication_tokens", force: :cascade do |t|
    t.string "body"
    t.bigint "user_id"
    t.datetime "last_used_at"
    t.integer "expires_in"
    t.string "ip_address"
    t.string "user_agent"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["body"], name: "index_authentication_tokens_on_body"
    t.index ["user_id"], name: "index_authentication_tokens_on_user_id"
  end

  create_table "bug_reports", force: :cascade do |t|
    t.string "bug_type"
    t.string "message"
    t.text "backtrace", default: [], array: true
    t.string "controller_action"
    t.string "request_path"
    t.jsonb "request_params"
    t.jsonb "user_params"
    t.integer "app", default: 0
    t.integer "crash_level", default: 0
    t.integer "status", default: 0
    t.integer "feature"
    t.string "supplier_code"
    t.text "user_description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.jsonb "supplier_params"
    t.text "analysis"
    t.integer "assignee", default: 0
    t.integer "priority", default: 0
    t.datetime "resolved_at"
  end

  create_table "cap_followups", force: :cascade do |t|
    t.bigint "cap_resolution_id"
    t.string "result"
    t.datetime "followup_date"
    t.text "notes"
    t.boolean "resolved"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "destroyed_at"
    t.bigint "cap_point_id"
    t.index ["cap_point_id"], name: "index_cap_followups_on_cap_point_id"
    t.index ["cap_resolution_id"], name: "index_cap_followups_on_cap_resolution_id"
  end

  create_table "cap_points", force: :cascade do |t|
    t.bigint "cap_id"
    t.string "ref_model"
    t.integer "ref_id"
    t.string "section"
    t.string "level"
    t.string "requirement"
    t.string "result"
    t.string "nonconformity"
    t.text "root_cause_nc"
    t.text "root_cause_nd"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "cap_date"
    t.datetime "resolved_at"
    t.datetime "destroyed_at"
    t.index ["cap_id"], name: "index_cap_points_on_cap_id"
  end

  create_table "cap_resolutions", force: :cascade do |t|
    t.bigint "cap_point_id"
    t.string "action_type"
    t.text "action"
    t.datetime "deadline_date"
    t.string "responsible"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "destroyed_at"
    t.index ["cap_point_id"], name: "index_cap_resolutions_on_cap_point_id"
  end

  create_table "caps", force: :cascade do |t|
    t.string "ref_model"
    t.integer "ref_id"
    t.datetime "cap_date"
    t.string "cap_type"
    t.bigint "pl_id"
    t.text "comments"
    t.bigint "follow_up_for"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "supplier_profile_id"
    t.datetime "destroyed_at"
    t.boolean "completed", default: false
    t.text "people_concerned", default: [], array: true
    t.text "invited_pls", default: [], array: true
    t.index ["supplier_profile_id"], name: "index_caps_on_supplier_profile_id"
  end

  create_table "change_logs", force: :cascade do |t|
    t.string "version"
    t.text "change_list", default: [], array: true
    t.text "viewed_by", default: [], array: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "chapter_assessments", force: :cascade do |t|
    t.bigint "quality_assessment_id"
    t.string "level"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "cotation"
    t.string "chapter_tag"
    t.string "score"
    t.string "section"
    t.datetime "destroyed_at"
    t.index ["quality_assessment_id"], name: "index_chapter_assessments_on_quality_assessment_id"
  end

  create_table "chapter_questions", force: :cascade do |t|
    t.bigint "chapter_assessment_id"
    t.string "chapter_tag"
    t.string "chapter"
    t.string "section"
    t.string "level"
    t.string "point"
    t.string "result"
    t.text "strong_points"
    t.text "points_to_improve"
    t.integer "score"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "destroyed_at"
    t.boolean "action_taken"
    t.boolean "confidential", default: false
    t.bigint "related_assessment_id"
    t.index ["chapter_assessment_id"], name: "index_chapter_questions_on_chapter_assessment_id"
  end

  create_table "comments", force: :cascade do |t|
    t.bigint "user_id"
    t.string "ref_model"
    t.integer "ref_id"
    t.text "content"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "destroyed_at"
    t.boolean "receiver_read", default: false
    t.bigint "reply_to"
    t.index ["user_id"], name: "index_comments_on_user_id"
  end

  create_table "control_plans", force: :cascade do |t|
    t.bigint "product_family_id"
    t.bigint "assessor_profile_id"
    t.string "plan_type"
    t.text "ref_note"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "assess_type", default: ""
    t.datetime "assess_date", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.boolean "completed", default: false, null: false
    t.string "nonconformity_rate", default: "0"
    t.datetime "date_confirmed"
    t.datetime "completed_at"
    t.datetime "destroyed_at"
    t.string "assessor_type", default: "assessor"
    t.text "people_concerned", default: [], array: true
    t.bigint "cap_id"
    t.string "slug"
    t.string "report_state"
    t.datetime "report_date"
    t.bigint "created_by"
    t.index ["assessor_profile_id"], name: "index_control_plans_on_assessor_profile_id"
    t.index ["product_family_id"], name: "index_control_plans_on_product_family_id"
  end

  create_table "cp_pending_frequencies", force: :cascade do |t|
    t.datetime "expires_at"
    t.datetime "resolved_at"
    t.string "frequency_for"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "process_step_id", null: false
    t.datetime "destroyed_at"
    t.index ["process_step_id"], name: "index_cp_pending_frequencies_on_process_step_id"
  end

  create_table "dpr_guidelines", force: :cascade do |t|
    t.string "guideline_tag"
    t.string "grid"
    t.string "code"
    t.string "chapter_no"
    t.string "chapter_name"
    t.string "section_no"
    t.string "section_name"
    t.string "process_name"
    t.string "step_no"
    t.string "step_name"
    t.string "from_level"
    t.string "to_level"
    t.text "content"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "response_choices", default: ["OK", "NOK", "Not Applicable", "Not Assessed"], array: true
  end

  create_table "evaluations", force: :cascade do |t|
    t.integer "limit_a"
    t.integer "limit_b"
    t.integer "limit_c"
    t.integer "limit_d"
    t.string "description"
    t.bigint "guideline_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "related_guideline_tag"
    t.string "lowest_limit", default: "E"
    t.index ["guideline_id"], name: "index_evaluations_on_guideline_id"
  end

  create_table "favorites", force: :cascade do |t|
    t.bigint "user_id"
    t.string "ref_model"
    t.bigint "ref_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "destroyed_at"
    t.index ["user_id"], name: "index_favorites_on_user_id"
  end

  create_table "followers", force: :cascade do |t|
    t.bigint "user_id"
    t.string "ref_model"
    t.bigint "ref_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_followers_on_user_id"
  end

  create_table "form_options", force: :cascade do |t|
    t.bigint "form_question_id"
    t.integer "priority"
    t.string "value"
    t.string "text"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "destroyed_at"
    t.index ["form_question_id"], name: "index_form_options_on_form_question_id"
  end

  create_table "form_questions", force: :cascade do |t|
    t.bigint "form_section_id"
    t.integer "priority"
    t.string "question_tag"
    t.string "component"
    t.string "name"
    t.text "hint"
    t.boolean "active", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "label"
    t.datetime "destroyed_at"
    t.boolean "required", default: false
    t.string "grid", default: "quality_assessment"
    t.string "prep_for", default: ""
    t.boolean "require_attachment", default: false
    t.string "respondant", default: "assessor"
    t.jsonb "label_translations"
    t.bigint "grid_version_id"
    t.boolean "with_remarks", default: false
    t.index ["form_section_id"], name: "index_form_questions_on_form_section_id"
    t.index ["grid_version_id"], name: "index_form_questions_on_grid_version_id"
  end

  create_table "form_sections", force: :cascade do |t|
    t.bigint "form_id"
    t.integer "priority"
    t.string "title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "destroyed_at"
    t.index ["form_id"], name: "index_form_sections_on_form_id"
  end

  create_table "forms", force: :cascade do |t|
    t.string "ref_model"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "title"
    t.datetime "destroyed_at"
    t.bigint "grid_version_id"
    t.index ["grid_version_id"], name: "index_forms_on_grid_version_id"
  end

  create_table "goes_activities", force: :cascade do |t|
    t.bigint "user_id"
    t.string "ref_model"
    t.integer "ref_id"
    t.string "ref_type"
    t.string "ref_status"
    t.text "content"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "destroyed_at"
    t.index ["user_id"], name: "index_goes_activities_on_user_id"
  end

  create_table "grid_versions", force: :cascade do |t|
    t.string "grid"
    t.integer "version_number"
    t.string "version_name"
    t.boolean "active", default: true
    t.string "short_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "guidelines", force: :cascade do |t|
    t.integer "parent_id"
    t.string "ref_model"
    t.string "guideline_tag"
    t.string "title"
    t.text "content"
    t.boolean "active", default: true
    t.datetime "destroyed_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "section_name"
    t.string "section_type"
    t.integer "section_no"
    t.text "note_header"
    t.text "note_bullets", default: [], array: true
    t.jsonb "title_translations"
    t.jsonb "content_translations"
    t.jsonb "note_header_translations"
    t.jsonb "note_bullets_translations"
    t.string "system_tag"
    t.text "toolbox_slides", default: [], array: true
    t.integer "toolbox_mode", default: 0
    t.text "relavent_products", default: [], array: true
    t.jsonb "toolbox_slides_translations"
    t.boolean "nutrition", default: false
    t.bigint "grid_version_id"
    t.text "response_choices", default: ["OK", "NOK", "Not Applicable", "Not Assessed"], array: true
    t.integer "process"
    t.string "related_assessments_grid"
    t.index ["grid_version_id"], name: "index_guidelines_on_grid_version_id"
  end

  create_table "invitations", force: :cascade do |t|
    t.string "ref_model"
    t.integer "ref_id"
    t.bigint "sender_id"
    t.bigint "recipient_id"
    t.boolean "accepted", default: false
    t.boolean "rejected", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "destroyed_at"
    t.index ["recipient_id"], name: "index_invitations_on_recipient_id"
    t.index ["sender_id"], name: "index_invitations_on_sender_id"
  end

  create_table "kpi_records", force: :cascade do |t|
    t.bigint "kpi_track_id"
    t.float "value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "destroyed_at"
    t.index ["kpi_track_id"], name: "index_kpi_records_on_kpi_track_id"
  end

  create_table "kpi_tracks", force: :cascade do |t|
    t.bigint "product_family_id"
    t.string "name"
    t.string "frequency"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "destroyed_at"
    t.float "target"
    t.boolean "active"
    t.index ["product_family_id"], name: "index_kpi_tracks_on_product_family_id"
  end

  create_table "letters", force: :cascade do |t|
    t.string "to"
    t.bigint "mail_bag_id"
    t.string "subject"
    t.text "body"
    t.datetime "opened_at"
    t.datetime "delivered_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "cc", default: [], array: true
    t.text "bcc", default: [], array: true
    t.jsonb "params", default: {}
    t.index ["mail_bag_id"], name: "index_letters_on_mail_bag_id"
  end

  create_table "likes", force: :cascade do |t|
    t.bigint "user_id"
    t.string "ref_model"
    t.bigint "ref_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "ref_type"
    t.datetime "destroyed_at"
    t.index ["user_id"], name: "index_likes_on_user_id"
  end

  create_table "mail_bags", force: :cascade do |t|
    t.string "subject"
    t.text "body"
    t.string "trigger"
    t.string "tag"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name"
    t.text "audience", default: [], array: true
    t.text "cc", default: [], array: true
    t.text "bcc", default: [], array: true
  end

  create_table "next_assessment_logs", force: :cascade do |t|
    t.integer "ref_id"
    t.string "ref_grid"
    t.string "ref_assess_type"
    t.string "next_assess_type"
    t.datetime "ref_assess_date"
    t.datetime "next_assess_date"
    t.datetime "notify_time"
    t.text "recipients", default: [], array: true
    t.boolean "sent", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "next_assessment_schedules", force: :cascade do |t|
    t.string "grid"
    t.string "assess_type"
    t.integer "next_assessment"
    t.string "time_unit"
    t.string "after_assess_type"
    t.string "supplier_type"
    t.string "result_condition"
    t.text "notify_in", default: [], array: true
    t.text "notify_what", default: [], array: true
    t.boolean "active", default: true
    t.datetime "destroyed_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.jsonb "countries", default: {"only"=>[], "except"=>[]}
  end

  create_table "plan_items", force: :cascade do |t|
    t.bigint "control_plan_id"
    t.bigint "process_step_id"
    t.text "findings", default: ""
    t.text "actions", default: ""
    t.string "result"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "completed", default: false
    t.datetime "cap_date"
    t.text "orders", default: [], array: true
    t.boolean "cap", default: false
    t.boolean "block", default: false
    t.datetime "destroyed_at"
    t.text "findings_process"
    t.text "traceability"
    t.index ["control_plan_id"], name: "index_plan_items_on_control_plan_id"
    t.index ["process_step_id"], name: "index_plan_items_on_process_step_id"
  end

  create_table "prep_questions", force: :cascade do |t|
    t.bigint "quality_assessment_id"
    t.string "question_tag"
    t.string "response"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "destroyed_at"
    t.string "respondant", default: "assessor"
    t.index ["quality_assessment_id"], name: "index_prep_questions_on_quality_assessment_id"
  end

  create_table "process_steps", force: :cascade do |t|
    t.bigint "product_process_id"
    t.string "step_number", default: ""
    t.string "control_item", default: ""
    t.text "control_method", default: ""
    t.string "requirement", default: ""
    t.string "criticity", default: "0"
    t.string "control_type", default: ""
    t.string "supplier_control_freq", default: ""
    t.string "supplier_controller", default: ""
    t.string "decathlon_control_freq", default: ""
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "active", default: true
    t.datetime "destroyed_at"
    t.string "supplier_control_freq_note"
    t.string "decathlon_control_freq_note"
    t.text "sampling"
    t.text "acceptance_rules"
    t.text "reaction_mode"
    t.index ["product_process_id"], name: "index_process_steps_on_product_process_id"
  end

  create_table "product_families", force: :cascade do |t|
    t.string "name"
    t.integer "current_level"
    t.integer "target_level"
    t.bigint "supplier_profile_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "destroyed_at"
    t.bigint "pl_id"
    t.index ["supplier_profile_id"], name: "index_product_families_on_supplier_profile_id"
  end

  create_table "product_processes", force: :cascade do |t|
    t.string "name"
    t.bigint "product_family_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "category", default: ""
    t.datetime "destroyed_at"
    t.string "number", default: "000"
    t.index ["product_family_id"], name: "index_product_processes_on_product_family_id"
  end

  create_table "quality_assessments", force: :cascade do |t|
    t.string "supplier_code", null: false
    t.string "assess_type", null: false
    t.datetime "assess_date", null: false
    t.text "prepped_by", default: [], array: true
    t.text "assessed_by", default: [], array: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "status"
    t.string "result"
    t.boolean "cap_required", default: false
    t.integer "cotation"
    t.bigint "supplier_profile_id"
    t.bigint "created_by"
    t.boolean "completed", default: false
    t.boolean "prep_completed", default: false
    t.boolean "assessor_confirmed", default: false
    t.boolean "day_form_completed", default: false
    t.boolean "chapters_completed", default: false
    t.text "assess_dates", default: [], array: true
    t.string "previous_score", default: "N/A"
    t.datetime "previous_date"
    t.datetime "destroyed_at"
    t.string "slug"
    t.string "grid", default: "quality_assessment"
    t.text "overall_comments"
    t.text "strong_points"
    t.text "points_to_improve"
    t.text "people_concerned", default: [], array: true
    t.datetime "report_date"
    t.string "report_state"
    t.bigint "cap_id"
    t.string "site"
    t.datetime "excel_date"
    t.string "excel_state"
    t.string "from_level"
    t.string "to_level"
    t.integer "opmsd"
    t.datetime "completed_at"
    t.boolean "nutrition", default: false
    t.boolean "historical", default: false
    t.bigint "grid_version_id"
    t.integer "process"
    t.jsonb "chapter_dates", default: {}
    t.index ["grid_version_id"], name: "index_quality_assessments_on_grid_version_id"
    t.index ["supplier_profile_id"], name: "index_quality_assessments_on_supplier_profile_id"
  end

  create_table "routine_questions", force: :cascade do |t|
    t.bigint "guideline_id"
    t.text "points_to_discuss"
    t.text "how_to_check"
    t.text "comments"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.jsonb "questions_translations"
    t.jsonb "how_to_check_translations"
    t.jsonb "frequency_translations"
    t.jsonb "guidance_definitions_translations"
    t.jsonb "documents_to_check_translations"
    t.index ["guideline_id"], name: "index_routine_questions_on_guideline_id"
  end

  create_table "schedules", force: :cascade do |t|
    t.string "ref_model"
    t.integer "ref_id"
    t.bigint "sender_id"
    t.bigint "recipient_id"
    t.boolean "pending", default: true
    t.boolean "accepted", default: false
    t.datetime "schedule_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "supplier_schedule_date"
    t.index ["recipient_id"], name: "index_schedules_on_recipient_id"
    t.index ["sender_id"], name: "index_schedules_on_sender_id"
  end

  create_table "search_histories", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.string "query", null: false
    t.text "options", default: [], array: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_search_histories_on_user_id"
  end

  create_table "sub_points", force: :cascade do |t|
    t.bigint "guideline_id"
    t.text "content"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "destroyed_at"
    t.index ["guideline_id"], name: "index_sub_points_on_guideline_id"
  end

  create_table "supplier_profiles", force: :cascade do |t|
    t.string "code"
    t.string "name"
    t.string "address"
    t.string "contact_name"
    t.string "contact_email"
    t.string "target"
    t.boolean "active"
    t.string "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "user_id"
    t.string "industrial_process"
    t.string "cpm"
    t.boolean "finished_goods_supplier", default: false
    t.boolean "component_supplier", default: false
    t.string "country"
    t.string "city"
    t.boolean "partner", default: false
    t.datetime "destroyed_at"
    t.datetime "invite_sent_at"
    t.text "pl_set", default: [], array: true
    t.text "emails_sent", default: [], array: true
    t.text "sites", default: [], array: true
    t.boolean "assessor_provider", default: false
    t.text "secondary_users", default: [], array: true
    t.string "short_name"
    t.text "grids_available", default: [], array: true
    t.string "rank"
    t.text "ib_emails", default: [], array: true
    t.text "ptm_set", default: [], array: true
    t.text "opm_set", default: [], array: true
    t.boolean "kas", default: false
    t.boolean "closed", default: false
    t.text "internal_assessors", default: [], array: true
  end

  create_table "survey_questions", force: :cascade do |t|
    t.bigint "quality_assessment_id"
    t.bigint "form_question_id"
    t.string "question_tag"
    t.string "response"
    t.integer "result", default: 0
    t.text "remarks"
    t.datetime "destroyed_at"
    t.string "respondant"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["form_question_id"], name: "index_survey_questions_on_form_question_id"
    t.index ["quality_assessment_id"], name: "index_survey_questions_on_quality_assessment_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: ""
    t.string "encrypted_password", default: ""
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.integer "failed_attempts", default: 0, null: false
    t.string "unlock_token"
    t.datetime "locked_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "wx_openid"
    t.boolean "supplier"
    t.boolean "assessor"
    t.string "username"
    t.string "wx_avatar"
    t.datetime "authenticated_until", default: -> { "CURRENT_TIMESTAMP" }
    t.string "authenticated_status", default: ""
    t.string "refresh_token"
    t.datetime "destroyed_at"
    t.boolean "admin", default: false
    t.text "access_levels", default: [], array: true
    t.boolean "welcome_sent"
    t.string "unionid"
    t.string "oa_openid"
    t.bigint "supplier_id"
    t.datetime "last_popup"
    t.text "grids_available", default: [], array: true
    t.string "locale", default: "en"
    t.datetime "last_thanks"
    t.datetime "login_notified_at", default: -> { "CURRENT_TIMESTAMP" }
    t.datetime "last_oa_invite"
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["supplier_id"], name: "index_users_on_supplier_id"
    t.index ["username"], name: "index_users_on_username"
  end

  create_table "validations", force: :cascade do |t|
    t.bigint "assessor_application_id"
    t.bigint "quality_assessment_id"
    t.bigint "control_plan_id"
    t.string "result"
    t.boolean "validated", default: false
    t.text "reason"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "co_animated", default: false
    t.index ["assessor_application_id"], name: "index_validations_on_assessor_application_id"
    t.index ["control_plan_id"], name: "index_validations_on_control_plan_id"
    t.index ["quality_assessment_id"], name: "index_validations_on_quality_assessment_id"
  end

  create_table "versions", force: :cascade do |t|
    t.string "item_type", null: false
    t.integer "item_id", null: false
    t.string "event", null: false
    t.string "whodunnit"
    t.text "object"
    t.datetime "created_at"
    t.text "object_changes"
    t.index ["item_type", "item_id"], name: "index_versions_on_item_type_and_item_id"
  end

  add_foreign_key "assessment_day_forms", "quality_assessments"
  add_foreign_key "assessor_applications", "assessor_profiles", column: "opm_id"
  add_foreign_key "assessor_applications", "assessor_profiles", column: "team_leader_id"
  add_foreign_key "assessor_applications", "assessor_profiles", column: "validator_id"
  add_foreign_key "assessor_applications", "users"
  add_foreign_key "assessor_assignments", "users"
  add_foreign_key "authentication_tokens", "users"
  add_foreign_key "cap_followups", "cap_points"
  add_foreign_key "cap_followups", "cap_resolutions"
  add_foreign_key "cap_points", "caps"
  add_foreign_key "cap_resolutions", "cap_points"
  add_foreign_key "caps", "supplier_profiles"
  add_foreign_key "chapter_assessments", "quality_assessments"
  add_foreign_key "chapter_questions", "chapter_assessments"
  add_foreign_key "comments", "users"
  add_foreign_key "control_plans", "product_families"
  add_foreign_key "cp_pending_frequencies", "process_steps"
  add_foreign_key "evaluations", "guidelines"
  add_foreign_key "favorites", "users"
  add_foreign_key "followers", "users"
  add_foreign_key "form_options", "form_questions"
  add_foreign_key "form_questions", "form_sections"
  add_foreign_key "form_questions", "grid_versions"
  add_foreign_key "form_sections", "forms"
  add_foreign_key "forms", "grid_versions"
  add_foreign_key "goes_activities", "users"
  add_foreign_key "guidelines", "grid_versions"
  add_foreign_key "invitations", "users", column: "recipient_id"
  add_foreign_key "invitations", "users", column: "sender_id"
  add_foreign_key "kpi_records", "kpi_tracks"
  add_foreign_key "kpi_tracks", "product_families"
  add_foreign_key "letters", "mail_bags"
  add_foreign_key "likes", "users"
  add_foreign_key "plan_items", "control_plans"
  add_foreign_key "plan_items", "process_steps"
  add_foreign_key "prep_questions", "quality_assessments"
  add_foreign_key "process_steps", "product_processes"
  add_foreign_key "product_families", "supplier_profiles"
  add_foreign_key "product_processes", "product_families"
  add_foreign_key "quality_assessments", "grid_versions"
  add_foreign_key "routine_questions", "guidelines"
  add_foreign_key "schedules", "supplier_profiles", column: "recipient_id"
  add_foreign_key "schedules", "users", column: "sender_id"
  add_foreign_key "search_histories", "users"
  add_foreign_key "sub_points", "guidelines"
  add_foreign_key "users", "supplier_profiles", column: "supplier_id"
  add_foreign_key "validations", "assessor_applications"
  add_foreign_key "validations", "control_plans"
  add_foreign_key "validations", "quality_assessments"
end
