require 'json'
require 'csv'
require_relative 'helpers'


terminal_arguements_set = ARGV.to_set
p "Arguments", terminal_arguements_set
help_set = ["help", "-h"].to_set
base_seed_args = ["base", "all", "production", "-b", "-a", "-p"].to_set
assessor_seed_args = ["assessors", "users", "all", "production", "-u", "-p", "-a"].to_set
supplier_seed_args = ["suppliers","users", "all", "production", "-u", "-p", "-a"].to_set
supplier_update_seed_args = ["s_update","users", "all", "production", "-u", "-p", "-a"].to_set
qa_seed_args = ["<testd>  </testd>ata", "all", "-t", "-a", "-qa"].to_set
cp_seed_args = ["testdata", "all", "-t", "-a", "-cp"].to_set
pf_seed_args = ["pf", "all", "production", "-pf", "-a", "-p"].to_set

# delete this later
indus_gl_args = ["ind_guidelines", "-ind"].to_set
prep_quest_args = ["prep_questions"].to_set

if terminal_arguements_set.intersect?(help_set) || ARGV == ["db:seed"]
  puts "Must Include Seed options!

  Ex: rails db:seed -a

  =====LIST OF OPTIONS======="
  puts " help (-h)   :  none"
  puts " base (-b)   :
    -> prep_questions
    -> qa_guidelines"
  # TODO: finish help messages
else
  print `clear`
  TH = display_status
end



if terminal_arguements_set.intersect?(indus_gl_args)
  # ===================================================================
  STATUS[:step] = "running INDUS guideline seed"
  load("#{DB}/indus_guidelines.rb")

  puts "FINISHED #{STATUS[:step]}              "
  # ===================================================================
  STATUS[:step] = "running Grid Version seed"
  load("#{DB}/grid_versions.rb")

  puts "FINISHED #{STATUS[:step]}              "
end

if terminal_arguements_set.intersect?(prep_quest_args)
  # ===================================================================
  STATUS[:step] = "running PREP Questions seed"
  load("#{DB}/prep_questions.rb")

  puts "FINISHED #{STATUS[:step]}"

  # ===================================================================
  STATUS[:step] = "running Grid Version seed"
  load("#{DB}/grid_versions.rb")

  puts "FINISHED #{STATUS[:step]}              "
end

# if terminal_arguements_set.intersect?(base_seed_args)
#   # ===================================================================
#   STATUS[:step] = "running next assessment schedules seed"
#   load("#{DB}/next_assessment_schedules.rb")

#   puts "FINISHED #{STATUS[:step]}              "

#   # ===================================================================
#   STATUS[:step] = "running prep questions form seed"
#   load("#{DB}/prep_questions.rb")

#   puts "FINISHED #{STATUS[:step]}              "
#   # ===================================================================
#   STATUS[:step] = "running QA guideline seed"
#   load("#{DB}/qa_guidelines.rb")

#   puts "FINISHED #{STATUS[:step]}              "
#   # ===================================================================
#   STATUS[:step] = "running OPEX guideline seed"
#   load("#{DB}/opex_guidelines.rb")

#   puts "FINISHED #{STATUS[:step]}              "
#   # ===================================================================
#   STATUS[:step] = "running SSE guideline seed"
#   load("#{DB}/sse_guidelines.rb")

#   puts "FINISHED #{STATUS[:step]}              "
#   # ===================================================================
#   STATUS[:step] = "running HRP guideline seed"
#   load("#{DB}/hrp_guidelines.rb")

#   puts "FINISHED #{STATUS[:step]}              "
#   # ===================================================================
#   STATUS[:step] = "running ENV guideline seed"
#   load("#{DB}/env_guidelines.rb")

#   puts "FINISHED #{STATUS[:step]}              "
#   # ===================================================================
#   STATUS[:step] = "running SCM guideline seed"
#   load("#{DB}/scm_guidelines.rb")

#   puts "FINISHED #{STATUS[:step]}              "
#   # ===================================================================
#   STATUS[:step] = "running PUR guideline seed"
#   load("#{DB}/pur_guidelines.rb")

#   puts "FINISHED #{STATUS[:step]}              "
#   # ===================================================================
#   STATUS[:step] = "running INDUS guideline seed"
#   load("#{DB}/indus_guidelines.rb")

#   puts "FINISHED #{STATUS[:step]}              "
# end

# if terminal_arguements_set.intersect?(assessor_seed_args)
#   # ===================================================================
#   STATUS[:step] = "running Assessor seed"
#   load("#{DB}/assessor_profiles.rb")

#   puts "FINISHED #{STATUS[:step]}              "
# end

# if terminal_arguements_set.intersect?(supplier_seed_args)
#   # ===================================================================
#   STATUS[:step] = "running Supplier seed"
#   load("#{DB}/supplier_profiles.rb")

#   puts "FINISHED #{STATUS[:step]}              "
# end

# if terminal_arguements_set.intersect?(supplier_update_seed_args)
#   # ===================================================================
#   STATUS[:step] = "running Supplier Update seed"
#   load("#{DB}/supplier_profiles_update.rb")

#   puts "FINISHED #{STATUS[:step]}              "
# end

# if terminal_arguements_set.intersect?(pf_seed_args)
#   # ===================================================================
#   STATUS[:step] = "running Product Family seed"
#   load("#{DB}/product_family_with_processes.rb")

#   puts "FINISHED #{STATUS[:step]}"
# end

# if terminal_arguements_set.intersect?(qa_seed_args)
#   # ===================================================================
#   STATUS[:step] = "running TEST CASE seed"
#   load("#{DB}/test_case.rb")

#   puts "FINISHED #{STATUS[:step]}              "
#   # ===================================================================
#   STATUS[:step] = "running GoesActivity seed"
#   load("#{DB}/goes_activity.rb")

#   puts "FINISHED #{STATUS[:step]}              "
#   # ===================================================================
#   STATUS[:step] = "running Comments seed"
#   load("#{DB}/comments.rb")

#   puts "FINISHED #{STATUS[:step]}              "
#   # ===================================================================
#   STATUS[:step] = "running Follower seed"
#   load("#{DB}/followers.rb")

#   puts "FINISHED #{STATUS[:step]}              "
# end

# if terminal_arguements_set.intersect?(cp_seed_args)
#   # ===================================================================
#   STATUS[:step] = "running Control Plan seed"
#   load("#{DB}/control_plan.rb")

#   puts "FINISHED #{STATUS[:step]}"
# end

unless terminal_arguements_set.intersect?(help_set) || ARGV == ["db:seed"]
  TH.kill

  success_message
end
