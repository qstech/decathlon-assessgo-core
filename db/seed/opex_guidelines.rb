require 'csv'

parse_csv_with_perc("db/data/opex_guidelines.csv") do |row,i|
  guideline = Guideline.find_or_create_by(guideline_tag: row[:guideline_tag])

  if row[:parent_tag].present? && row[:parent_tag] == "BULLET"
    bullets = (guideline.attributes["note_bullets"] || [])
    bullets << row[:note_bullets]
    guideline.note_bullets = bullets.uniq
    guideline.save(validate: false)
    next
  elsif row[:parent_tag].present?
    parent = Guideline.find_by(guideline_tag: row[:parent_tag])
    guideline.parent_id = parent.id
    guideline.save(validate: false)
  end

  params = row.to_h.except(:parent_tag, :note_bullets)
  guideline.assign_attributes(params)
  guideline.save(validate: false)
end

