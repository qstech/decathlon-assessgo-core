require_relative '../helpers'

def flevel(str)
  section, lev = str.split(' ')
  Guideline.joins(:parent).where(parents_guidelines: { section_name: section }).where(section_name: lev, ref_model: "quality_assessment").first
end

def point_add(lev, content)
  pt_no = lev.children.count + 1
  point = "#{lev.section_name}-#{pt_no}"
  attrs = {
    parent_id: lev.id, ref_model: lev.ref_model,
    guideline_tag: "#{lev.guideline_tag}-#{pt_no}",
    section_name: point, section_type: "Point", section_no: pt_no.to_i,
    title_en: point, title_zhcn: point, content_en: content, nutrition: true,
    relavent_products: lev.relavent_products, grid_version_id: lev.grid_version_id
  }

  lev.children << Guideline.new(attrs)
end

parse_csv_with_perc("db/data/nutrition.csv") do |row, i|
  level = flevel("#{row[:section]} #{row[:level]}")
  point_add(level, row[:content])
end
