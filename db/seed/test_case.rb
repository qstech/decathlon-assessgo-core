
ua = User.find_for_authentication(username: "testuser")
us = User.find_for_authentication(username: "test@supplier.com")

ua = User.create(username: "testuser", password: "123123") if ua.blank?
us = User.create(username: "test@supplier.com", password: "123123") if us.blank?

User.create(username: "test2@assessor.com", password: "123123") # to receive invitation

# sp.update(user_id: us.id)
ass  = AssessorProfile.new(user_id: ua.id, name: "TEST USER", handle: "testuser")
ua.assessor_profile = ass
# =========================================
# =========================================
# =========================================
puts "===== Creating Test Cases ======                "
STATUS[:step] = "Creating Blank New Assessment"
STATUS[:perc] = 0

sp = SupplierProfile.all.sample
qa_params = {
  assess_type: "Gemba",
  assess_date: "2019-04-01",
  created_by: ua.id,
  chapter_set: ["1.1","1.2","1.3","2.1","3.2","3.3"],
  grid: 'quality_assessment',
}

qa = sp.setup_new_qa(qa_params)

# =========================================
# =========================================
# =========================================
STATUS[:step] = "Creating In Progress Assessment"
STATUS[:perc] = 35

sp = SupplierProfile.all.sample
qa_params = {
  assess_type: "Annual",
  assess_date: Time.now.strftime("%Y-%m-%d"),
  created_by: ua.id,
  chapter_set: ["1.1","1.2","1.3", "1.4","2.1","3.1","3.2","3.3", "3.4", "3.5", "3.6", "3.7", "4.1"],
  grid: 'quality_assessment',
}

qa = sp.setup_new_qa(qa_params)

qa.prep_questions.each do |pq|
  pq.update(response: ["No", "Yes", "Not Applicable"].sample)
end
qa.update(status: "QA Ready!", prep_completed: true)
AssessorAssignment.create(ref_model: "quality_assessment", ref_id: qa.id,
  user_id: ua.id, primary: true, confirmed: true)
# =========================================
# =========================================
# =========================================
STATUS[:step] = "Creating Completed Assessment"
STATUS[:perc] = 67

sp = SupplierProfile.all.sample
qa_params = {
  assess_type: "CAP Review",
  assess_date: "2019-03-12",
  created_by: ua.id,
  chapter_set: ["1.3", "1.4","2.1","3.1","3.3", "3.4", "3.7"],
  grid: 'quality_assessment',
}

qa = sp.setup_new_qa(qa_params)

qa.prep_questions.each do |pq|
  pq.update(response: "Y")
end

qa.assessment_day_form.update(pl_id: ua.id, pl_present: true, observers:[],
  people_encountered: "Kathy Tan (manager)",
  steps_assessed: "belt production line > buckle fastening station")

AssessorAssignment.create(ref_model: "quality_assessment", ref_id: qa.id,
  user_id: ua.id, primary: true, confirmed: true)

qa.chapter_assessments.each do |ca|
  ca.chapter_questions.each do |cq|
    cq_params = {
      result: probable_result_for(cq.level),
      strong_points: "Vero, quisquam dolorum neque aut non magni saepe quas inventore modi sequi eos et ab molestias doloremque doloribus iste sed enim excepturi!",
      points_to_improve: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis impedit hic maiores incidunt rem numquam labore nihil optio molestiae."
    }
    cq.update(cq_params)
    cq.save
  end
  ca.update_status
end
qa.completed!

# =========================================
# =========================================
# =========================================







