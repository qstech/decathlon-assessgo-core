require_relative '../helpers'
require 'csv'
# in CSV file:
# separate items of array attributes (i.e. note_bullets, :response_choices) with commas (,)
# Avoid quotes("), new lines(\n), semicolons (;)
EVALUATION_PARAMS = %i[limit_a limit_b limit_c limit_d description lowest_limit]

def prepare_note_bullets(guideline, row)
  existing = (guideline.attributes["note_bullets"] || [])
  updated = existing + row[:note_bullets].split(',').map(&:strip)
  updated.uniq
end

parse_csv_with_perc("db/data/indus_guidelines.csv", ";") do |row, i|
  guideline = Guideline.find_or_create_by(guideline_tag: row[:guideline_tag])
  if row[:parent_tag].present? && row[:parent_tag] == "EVALUATION"
    evaluation = Evaluation.find_or_create_by(related_guideline_tag: row[:guideline_tag])
    params = row.to_h.select { |k, _v| EVALUATION_PARAMS.include?(k) }
    params[:guideline_id] = guideline&.id
    evaluation.assign_attributes(params)
    evaluation.save
    next
  elsif row[:parent_tag].present?
    parent = Guideline.find_by(guideline_tag: row[:parent_tag])
    guideline.parent_id = parent.id
    guideline.save(validate: false)
  end
  guideline.note_bullets = prepare_note_bullets(guideline, row) if row[:note_bullets].present?
  guideline.response_choices = row[:response_choices].split(',').map(&:strip) if row[:response_choices].present?
  guideline.save(validate: false)

  params = row.to_h.except(:parent_tag, :note_bullets, :response_choices, :related_guideline_tag, *EVALUATION_PARAMS)
  guideline.assign_attributes(params)
  guideline.save(validate: false)
end
