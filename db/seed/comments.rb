
contents = ["Great Job!", "I think your analysis was on point", "I think your detail can improve in your point by point analysis"]

goes_activities = GoesActivity.all
10.times do
  Comment.create(user_id: 1, ref_model: "quality_assessment", ref_id: goes_activities.sample.ref_id , content: contents.sample )
end
