require 'json'
require 'csv'
require_relative '../helpers'

# puts "SYNCING GUIDELINES"
# all_gl = JSON.parse(File.read("db/data/json/guidelines-all.json"))
# all_gl.each do |gl|
#   print "."
#   g = Guideline.find_or_create_by(guideline_tag: gl["guideline_tag"])
#   g.update(gl.except("id", "created_at", "updated_at"))
# end

# puts "Transferring stored values to EN"

# Guideline.all.each do |gl|
#   print "."
#   gl.title_en = gl.attributes["title"] if gl.attributes["title"].present?
#   gl.content_en = gl.attributes["content"] if gl.attributes["content"].present?
#   gl.note_header_en = gl.attributes["note_header"] if gl.attributes["note_header"].present?
#   gl.note_bullets_en = gl.attributes["note_bullets"]
#   gl.save
# end
term_args = ARGV
puts term_args
LOCALES = {
  "zh" => "Chinese",
  "fr" => "French",
  "vi" => "Vietnamese",
  "zh_env" => "Chinese ENV"
}


def gl_set(guideline, att, locale, row)
  guideline.send("#{att}_#{locale}=".to_sym, row["#{att}_#{locale}".to_sym].strip) if row["#{att}_#{locale}".to_sym].present?
end

def seed_for_language(locale)
  puts "Now seeding #{LOCALES[locale]}!"
  parse_csv_with_perc("db/data/#{locale}_guidelines.csv") do |row, i|
    locale = "zhcn" if locale.include?("zh")
    print "."
    guideline = Guideline.find_by(guideline_tag: row[:guideline_tag])
    if guideline.blank?
      puts "NOT FOUND! #{row[:guideline_tag]}"
      next
    end
    gl_set(guideline, "title", locale, row)
    gl_set(guideline, "content", locale, row)
    gl_set(guideline, "note_header", locale, row)

    if row["note_bullets_#{locale}".to_sym].present?
      bullets = row["note_bullets_#{locale}".to_sym].split("\n")
      bullets.delete("")

      guideline.send("note_bullets_#{locale}=".to_sym, bullets)
    end
    guideline.save
  end
end

seed_for_language("zh") if term_args.include?("zh")
seed_for_language("zh_env") if term_args.include?("zh_env")
seed_for_language("fr") if term_args.include?("fr")
seed_for_language("vi") if term_args.include?("vi")


