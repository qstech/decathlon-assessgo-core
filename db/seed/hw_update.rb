require 'json'

sets = JSON.parse(File.read("#{Rails.root}/db/data/hw_update.json"))
sets.each do |row|
  s = SupplierProfile.find_by_code(row[0])
  next if s.nil?
  s.update(country: row[1], industrial_process: row[2])
end
