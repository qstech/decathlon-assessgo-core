grids = {
  "quality_assessment"=> "QA",
  "opex_assessment"=> "OPEX",
  "sse_assessment"=> "SSE",
  "hrp_assessment"=>"HRP",
  "scm_assessment"=>"SCM",
  "env_assessment"=>"ENV",
  "pur_assessment"=>"PUR",
  "dpr_assessment"=> "DPR",
  "hfwb_assessment" => "HFWB",
  "indus_assessment" => "IND"
}

grids.each do |grid, short_name|
  gv = GridVersion.find_or_create_by(grid: grid,
                                     short_name: short_name, version_number: 1,
                                     version_name: "#{short_name} Assessment Grid (2020)")

  Guideline.where(ref_model: grid, grid_version_id: nil).update_all(grid_version_id: gv.id)
  FormQuestion.where(grid: grid, grid_version_id: nil).update_all(grid_version_id: gv.id)
  QualityAssessment.where(grid: grid, grid_version_id: nil).update_all(grid_version_id: gv.id)
  if short_name == "HFWB"
    QualityAssessment.where(grid_version_id: nil).where('grid ILIKE ?', 'hfwb:%').update_all(grid_version_id: gv.id)
  end
end
