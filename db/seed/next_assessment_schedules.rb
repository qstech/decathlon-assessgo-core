require 'csv'
parse_csv_with_perc("db/data/next_assessment_schedules.csv") do |row, i|

  params = row.to_h
  params[:next_assessment] = params[:next_assessment].to_i
  params[:notify_in] = params[:notify_in].split(",")
  params[:notify_what] = params[:notify_what].split(",")
  if params[:countries].blank?
    params[:countries] = {only: [], except: []}
  elsif params[:countries].include?("NOT: ")
    cnt = params[:countries][5..-1].split(", ")
    params[:countries] = {only: [], except: cnt}
  else
    cnt =  params[:countries].split(", ")
    params[:countries] = {only: cnt, except: []}
  end

  p params
  sched = NextAssessmentSchedule.find_or_create_by(params)
end
