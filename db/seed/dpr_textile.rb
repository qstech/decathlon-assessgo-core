require 'csv'
require_relative '../helpers'

parse_csv_with_perc("db/data/dpr_textile.csv") do |row,i|
  if row[:code].length < 6
    (6-row[:code].length).times{row[:code] += '0'}
  end

  row[:chapter_no] = row[:code].split('.').first
  row[:section_no] += "00" if row[:section_no].length == 1
  row[:step_no] = row[:code].split('.').last

  guideline_tag = "DPR_TEXTCH#{row[:chapter_no]}S#{row[:section_no]}L#{row[:code].split('.').last}"
  guideline = DprGuideline.find_or_create_by(guideline_tag: guideline_tag)
  params = row.to_h
  guideline.assign_attributes(params)
  guideline.save
end
