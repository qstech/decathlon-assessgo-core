DIR = "#{Rails.root}/public/toolbox"
FILES = Dir.entries(DIR).select{|x| x.include?("PNG") || x.include?("png")}.sort

FILES.each do |f|
  reg_match = f.match(/(.*assessment)_(\d.\d)\s?(\w)/)
  puts reg_match
  section = Guideline.find_by(ref_model: reg_match[1], section_name: reg_match[2])
  level = section.children.find_by(section_name: reg_match[3])
  level.toolbox_images.attach(io: File.open("#{DIR}/#{f}"), filename: f)
end

