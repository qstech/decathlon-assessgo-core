require "faker"

def create_letter(to, subject, body)
  admins = User.select(:id).where(username: ["kkang20", "wxu50"]).map(&:id)
  letter = Letter.create(to: to, subject: subject, body: body, cc: admins, mail_bag_id: MailBag.all.ids.sample)
end

batch_recipients = ["decathlon", "suppliers", "assessors", "grid_leaders", "all"]
subject = [ "Invitation to AssessGo Official Account Notifications",
            "Your New Password",
            "Email Changed",
            "Assess Go has created a Gemba assessment for you",
            "Thanks for Using Assess Go!",
            "user_followup_5_login",
            "user_followup_5_no_login",
            "Invitation to Decathlon's Assess Go"
          ]
body = Faker::TvShows::TheITCrowd.quote

10.times do
  create_letter(batch_recipients.sample, subject.sample, body)
end

