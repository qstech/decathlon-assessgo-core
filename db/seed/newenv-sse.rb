require 'csv'
require 'json'
require_relative '../helpers'

env_gv = GridVersion.find_or_create_by(grid: 'env_assessment',
    short_name: 'ENV', version_number: 2,
    version_name: "ENV V1.4")

sse_gv = GridVersion.find_or_create_by(grid: 'sse_assessment',
    short_name: 'SSE', version_number: 2,
    version_name: "SSE V8")


def seed_for(gv)
  parse_csv_with_perc("db/data/new#{gv.short_name.downcase}.csv") do |row,i|
    guideline = Guideline.find_or_create_by(guideline_tag: row[:guideline_tag], grid_version_id: gv.id)

    if row[:parent_tag].present? && row[:parent_tag] == "BULLET"
      bullets = (guideline.attributes["note_bullets"] || [])
      bullets << row[:note_bullets]
      guideline.note_bullets = bullets.uniq
      guideline.save(validate: false)
      next
    elsif row[:parent_tag].present?
      parent = Guideline.find_by(guideline_tag: row[:parent_tag])
      guideline.parent_id = parent.id
      guideline.save(validate: false)
    end

    params = row.to_h.except(:parent_tag, :note_bullets)
    guideline.assign_attributes(params)
    guideline.save(validate: false)
  end
end

def seed_prep_questions_for(gv)

  prep_questions = JSON.parse(File.read("db/data/new#{gv.short_name.downcase}_prep_questions.json"))

  f = Form.find_or_create_by(ref_model: "prep_question", title: "#{gv.short_name} Preparation Questions")
  s = FormSection.find_or_create_by(form_id: f.id, title: "Prep Questions")
  f.form_sections << s
  i = 1

  each_with_perc(prep_questions) do |quest|
    # next unless ['dpr_assessment'].include? quest['grid']

    q_params = {
      component: quest['component'],
      label: quest['question'],
      label_en: quest['question_en'] || quest['question'],
      active: true,
      required: quest['required']||false,
      grid: quest['grid'],
      grid_version_id: gv.id
    }
    q_params[:label_zhcn] = quest['question_zhcn'] if quest['question_zhcn']
    q_params[:respondant] = quest['respondant'] if quest['respondant'].present?
    q_params[:require_attachment] = quest['require_attachment'] if quest['require_attachment'].present?
    q_params[:prep_for] = quest['prep_for'] if quest['prep_for'].present?

    q = FormQuestion.find_or_create_by(question_tag: "#{gv.short_name}PREP#{i}", form_section_id: s.id, grid_version_id: gv.id)
    q_params[:priority] = i
    i += 1

    q_params[:name] = "prep_question[#{q.id}]"

    q.update(q_params)

    if quest['options'].present?
      quest['options'].each do |option|
        FormOption.find_or_create_by(form_question_id: q.id, value: option, text: option)
      end
    elsif quest['component'] == 'radio'
      ["Yes", "No", "Not Applicable"].each do |option|
        FormOption.find_or_create_by(form_question_id: q.id, value: option, text: option)
      end
    end
  end

end

seed_for(env_gv)
seed_for(sse_gv)
seed_prep_questions_for(sse_gv)
seed_prep_questions_for(env_gv)







