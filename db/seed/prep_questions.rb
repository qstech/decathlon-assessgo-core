require 'json'
require_relative '../helpers'

prep_questions = JSON.parse(File.read('db/data/prep_questions.json'))

f1 = Form.find_or_create_by(ref_model: "prep_question", title: "QA Preparation Questions")
s1 = FormSection.find_or_create_by(form_id: f1.id, title: "Prep Questions")
f1.form_sections << s1
i1 = 1

f2 = Form.find_or_create_by(ref_model: "prep_question", title: "SSE Preparation Questions")
s2 = FormSection.find_or_create_by(form_id: f2.id, title: "Prep Questions")
f2.form_sections << s2
i2 = 1

f3 = Form.find_or_create_by(ref_model: "prep_question", title: "HRP Preparation Questions")
s3 = FormSection.find_or_create_by(form_id: f3.id, title: "Prep Questions")
f3.form_sections << s3
i3 = 1

f4 = Form.find_or_create_by(ref_model: "prep_question", title: "OPEX Preparation Questions")
s4 = FormSection.find_or_create_by(form_id: f4.id, title: "Prep Questions")
f4.form_sections << s4
i4 = 1

f5 = Form.find_or_create_by(ref_model: "prep_question", title: "ENV Preparation Questions")
s5 = FormSection.find_or_create_by(form_id: f5.id, title: "Prep Questions")
f5.form_sections << s5
i5 = 1

f6 = Form.find_or_create_by(ref_model: "prep_question", title: "SCM Preparation Questions")
s6 = FormSection.find_or_create_by(form_id: f6.id, title: "Prep Questions")
f6.form_sections << s6
i6 = 1

f7 = Form.find_or_create_by(ref_model: "prep_question", title: "Purchasing Preparation Questions")
s7 = FormSection.find_or_create_by(form_id: f7.id, title: "Prep Questions")
f7.form_sections << s7
i7 = 1

f8 = Form.find_or_create_by(ref_model: "prep_question", title: "DPR Textile Preparation Questions")
s8 = FormSection.find_or_create_by(form_id: f8.id, title: "Prep Questions")
f8.form_sections << s8
i8 = 1

f9 = Form.find_or_create_by(ref_model: "prep_question", title: "INDUS Preparation Questions")
s9 = FormSection.find_or_create_by(form_id: f9.id, title: "Prep Questions")
f9.form_sections << s9
i9 = 1



each_with_perc(prep_questions) do |quest|
  # next unless ['dpr_assessment'].include? quest['grid']

  q_params = {
    component: quest['component'],
    label: quest['question'],
    label_en: quest['question_en'] || quest['question'],
    active: true,
    required: quest['required']||false,
    grid: quest['grid']
  }
  q_params[:label_zhcn] = quest['question_zhcn'] if quest['question_zhcn']
  q_params[:respondant] = quest['respondant'] if quest['respondant'].present?
  q_params[:require_attachment] = quest['require_attachment'] if quest['require_attachment'].present?
  q_params[:prep_for] = quest['prep_for'] if quest['prep_for'].present?

  case quest['grid']
  when 'quality_assessment'
    q = FormQuestion.find_or_create_by(question_tag: "QAPREP#{i1}", form_section_id: s1.id)
    q_params[:priority] = i1
    i1 += 1
  when 'sse_assessment'
    q = FormQuestion.find_or_create_by(question_tag: "SSEPREP#{i2}", form_section_id: s2.id)
    q_params[:priority] = i2
    i2 += 1
  when 'hrp_assessment'
    q = FormQuestion.find_or_create_by(question_tag: "HRPPREP#{i3}", form_section_id: s3.id)
    q_params[:priority] = i3
    i3 += 1
  when 'opex_assessment'
    q = FormQuestion.find_or_create_by(question_tag: "OPEXPREP#{i4}", form_section_id: s4.id)
    q_params[:priority] = i4
    i4 += 1
  when 'env_assessment'
    q = FormQuestion.find_or_create_by(question_tag: "ENVPREP#{i5}", form_section_id: s5.id)
    q_params[:priority] = i5
    i5 += 1
  when 'scm_assessment'
    q = FormQuestion.find_or_create_by(question_tag: "SCMPREP#{i6}", form_section_id: s6.id)
    q_params[:priority] = i6
    i6 += 1
  when 'pur_assessment'
    q = FormQuestion.find_or_create_by(question_tag: "PURPREP#{i7}", form_section_id: s7.id)
    q_params[:priority] = i7
    i7 += 1
  when 'dpr_assessment'
    q = FormQuestion.find_or_create_by(question_tag: "DPR_TEXTPREP#{i8}", form_section_id: s8.id)
    q_params[:priority] = i8
    i8 += 1
  when 'indus_assessment'
    q = FormQuestion.find_or_create_by(question_tag: "INDPREP#{i9}", form_section_id: s9.id)
    q_params[:priority] = i9
    i9 += 1
  end

  q_params[:name] = "prep_question[#{q.id}]"


  q.update(q_params)

  if quest['options'].present?
    quest['options'].each do |option|
      FormOption.find_or_create_by(form_question_id: q.id, value: option, text: option)
    end
  elsif quest['component'] == 'radio'
    ["Yes", "No", "Not Applicable"].each do |option|
      FormOption.find_or_create_by(form_question_id: q.id, value: option, text: option)
    end
  end
end
