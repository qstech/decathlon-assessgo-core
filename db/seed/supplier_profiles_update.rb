parse_csv_with_perc("db/data/p_supplier_update.csv") do |row, i|
  supplier_profile = SupplierProfile.find_or_create_by(code: row[:code])
  params = row.to_h.reject{|k,v| v.blank?}
  supplier_profile.update(params)
end
