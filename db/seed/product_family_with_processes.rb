parse_csv_with_perc("db/data/product_families.csv") do |row, i|
  next if row[:supplier_code].blank?

  supplier_profile = SupplierProfile.find_by(code: row[:supplier_code])
  product_family = ProductFamily.find_or_create_by(supplier_profile_id: supplier_profile.id, name: "#{row[:product]} #{row[:family]}")
  assessor_profile = AssessorProfile.find_by(name: row[:pl_name].titleize)
  product_family.update(pl_id: assessor_profile.id) if assessor_profile.present?
end
