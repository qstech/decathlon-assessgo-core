# NOTES ON ADDING GUIDELINES

# ===> Chapter
CHAPTER = {
  'parent_id' => nil, # always nil for chapter
  'ref_model' => 'quality_assessment', # Grid underscored-full name (ex: indus_assessment)
  'guideline_tag' => 'QACH1', # MUST BE IN FORMAT  '<SHORTNAME>CH<chapter_no>'
  'title' => 'QUALITY LEADERSHIP IN THE COMPANY - INTERNAL QUALITY MANAGEMENT',
  'content' => nil,
  'active' => true,
  'section_name' => 'Chapter 1', # ALWAYS BE 'Chapter <chapter_no>'
  'section_type' => 'Chapter', # ALWAYS 'Chapter'
  'section_no' => 1, # <chapter_no>
  'relavent_products' => [], # only used in HFWB at the moment
  'grid_version_id' => 1 # in seed, create new gridVersion for 'indus_assessment'
}
# ===> Section (belongs to Chapter)
SECTION = {
  'parent_id' => 1, # the id of the chapter from the seed.
  'ref_model' => 'quality_assessment', # same
  'guideline_tag' => 'QACH1S1', # ALWAYS in format '<SHORTNAME>CH<chapter_no>S<section_no>'
  'title' => 'Leadership of Quality', # Name of section
  'content' => 'The management create the conditions to delight customers', # Any additional text (ex: special descripiton/quote)
  'active' => true,
  'section_name' => '1.1', # ALWAYS in format '<chapter_no>.<section_no>'
  'section_type' => 'Section', #ALWAYS 'Section'
  'section_no' => 1, # <section_no>
  'grid_version_id'=>1,
}

# ===> Level (belongs to Section)
# FOR INDUS Chapters w/ no levels, use Level 'X'
LEVEL = {
  'parent_id' => 2,
  'ref_model' => 'quality_assessment',
  'guideline_tag' => 'QACH1S1L1', # ALWAYS in format '<SHORTNAME>CH<chapter_no>S<section_no>L<level_no>'
  'title' => 'Level D', # ALWAYS in format 'Level <A..E>'
  'active' => true,
  'section_name' => 'D', # <A..E>
  'section_type' => 'Level', # ALWAYS Level
  'section_no' => 1, # level_no
  'grid_version_id' => 1,
}

# ===> Point (belongs to Level)
POINT = {
  'parent_id' => 1490,
  'ref_model' => 'quality_assessment',
  'guideline_tag' => 'QACH1S1L1-1',
  'title' => 'E-1', #<level>-<point_no>
  'content' => 'Factory must be separated in different worspace : cutting area / assembling area / stock\n',
  'active' => true,
  'section_name' => 'E-1',
  'section_type' => 'Point',
  'section_no' => 1, # <point_no>
  'note_header' =>  'Some text',
  'note_bullets' => ['> 1st bullet', '> 2nd bullet'],
  'relavent_products' =>  ['trail_running_bag'],
  'grid_version_id' => 9,
  'response_choices' => ['OK', 'NOK', 'Not Applicable', 'Not Assessed']
}


