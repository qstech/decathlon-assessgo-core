# default OK, NOK, Not Applicable, Not Assessed
no_risk = "2.3/4.1/4.5/5.3/8.3/5.21"
no_assess = ["D","C","B"]
no_applicable = "1.5/1.8/2.3/2.5/2.6/2.12/2.13/2.14/2.15/2.16/2.17/2.18/2.19/2.20/2.22/3.1/3.2/4.3/4.4/4.7/4.8/4.9/5.2/5.4/5.5/5.6/5.9/5.11/5.12/5.16/5.17/5.21/5.22/5.28/6.1/6.2/6.3/6.4/6.5/6.6/6.7/6.8/6.9/6.10/6.11/6.12/6.13/6.14/6.15/6.16/6.17/6.18/6.19/6.20/6.21/6.22/6.23/6.24/6.25/6.26/6.27/6.28/6.29/6.30/6.31/6.32/6.33/6.34/6.35/6.36/6.37/7.4/7.16/7.25/7.26/7.31/8.3/8.4/8.5/8.6/8.7/8.11/8.13/8.14/8.15/8.16/8.17/8.18/11.6/11.7/11.10/11.12/11.13/11.20/12.9/12.10"



no_risk.split("/").map{|x| Guideline.find_by_hrp_tag(x)}.each do |gl|
  gl.response_choices << "NOK without risk"
  gl.response_choices.uniq!
  gl.response_choices.sort_by!{|x| Guideline::RESPONSE_VALUES.index(x)}
  gl.save
end

Guideline.active.where(ref_model: "hrp_assessment", section_type: "Level").where.not(section_name: no_assess ).each do |lev|
  lev.response_choices.delete("Not Assessed")
  lev.save
  lev.children.each{|x| x.response_choices.delete("Not Assessed");x.save;}
end

with_na = no_applicable.split("/").map{|x| Guideline.find_by_hrp_tag(x)}.map(&:id)
Guideline.active.where(ref_model: "hrp_assessment", section_type: "Point").where.not(id: with_na).each do |pt|
  pt.response_choices.delete("Not Applicable")
  pt.save
end

#ENV
no_assess = ["C","B"]
no_applicable = "0.3.8/0.3.9/1.1.1/1.1.2/1.2.1/1.2.2/1.2.3/1.2.4/1.2.5/1.2.6/1.2.7/1.2.8/1.2.9/1.2.10/1.4.1/1.4.2/1.4.4/1.4.5/1.4.6/2.3.4./3.3.1/3.3.2/3.3.3/3.3.4/3.3.5/3.3.6/3.3.7/3.3.8"



Guideline.active.where(ref_model: "env_assessment", section_type: "Level").where.not(section_name: no_assess ).each do |lev|
  lev.response_choices.delete("Not Assessed")
  lev.save
  lev.children.each{|x| x.response_choices.delete("Not Assessed");x.save;}
end

with_na = no_applicable.split("/").map{|x| Guideline.find_by_env_tag(x)}.reject(&:nil?).map(&:id)
Guideline.active.where(ref_model: "env_assessment", section_type: "Point").where.not(id: with_na).each do |pt|
  pt.response_choices.delete("Not Applicable")
  pt.save
end




