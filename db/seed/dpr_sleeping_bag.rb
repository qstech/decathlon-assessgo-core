require 'csv'
require_relative '../helpers'

@rows = []

parse_csv_with_perc("db/data/dpr_sleeping_bags.csv") do |row,i|
  # tag,process,title,requirement,level
  # 1.0.04A,SAMPLE ROOM,General Requirement,There is a sampling room,D
  @rows << row
end

@rows = @rows.group_by{|row| row[:process]}

@rows = @rows.map do |chapter_name, rows|
  [chapter_name, rows.group_by{ |row| row[:tag] }]
end.to_h

# {"SAMPLE ROOM"=> {"1.0.04A"=>[....], ...}

ref_model = "dpr_sleeping"
tag = "DPRSLP"
chapters = [1,2,3,4,7,9]

def lev_index(letter)
  ("A".."F").to_a.reverse.index(letter)
end

@rows.each_with_index do |(chapter_name, sections_h), i|
  ch_no = chapters[i]
  ch_params = {
    ref_model: ref_model, guideline_tag: "#{tag}CH#{ch_no}",
    title: chapter_name, section_name: "Chapter #{ch_no}",
    section_type: "Chapter", parent_id: nil, section_no: ch_no,
    active: true
  }
  ch = Guideline.find_or_create_by(guideline_tag: ch_params[:guideline_tag])
  ch.update(ch_params)


  sections_h.each_with_index do |(sec_tag, levs), j|
    no = j+1
    sec_params = {
      ref_model: ref_model, guideline_tag: "#{tag}CH#{ch_no}S#{no}",
      title: levs.first[:title], section_name: sec_tag,
      section_type: "Section", parent_id: ch.id, section_no: ch_no,
      active: true
    }
    sec = Guideline.find_or_create_by(guideline_tag: sec_params[:guideline_tag])
    sec.update(sec_params)

    lev_count = {"A"=> 0, "B"=> 0, "C"=>0, "D" => 0, "E"=> 0}

    levs.each do |lev|
      next unless lev[:level].in?(("A".."E").to_a)
      l = lev[:level].strip
      lev_params = {
        ref_model: ref_model, guideline_tag: "#{tag}CH#{ch_no}S#{no}L#{lev_index(l)}",
        title: "Level #{l}", section_name: l,
        section_type: "Level", parent_id: sec.id, section_no: lev_index(l),
        active: true
      }
      level = Guideline.find_or_create_by(guideline_tag: lev_params[:guideline_tag])
      level.update(lev_params)

      lev_count[l] += 1
      point_tag = "#{tag}CH#{ch_no}S#{no}L#{lev_index(l)}-#{lev_count[l]}"
      point_params = {
        ref_model: ref_model, guideline_tag: point_tag,
        title: "#{l}-#{lev_count[l]}", section_name: "#{l}-#{lev_count[l]}",
        section_type: "Point", parent_id: level.id, section_no: lev_count[l],
        active: true, content: lev[:requirement].strip
      }
      point = Guideline.find_or_create_by(guideline_tag: point_params[:guideline_tag])
      point.update(point_params)
    end
  end
end
