require 'csv'
require 'json'
require_relative '../helpers'

gv = GridVersion.find_or_create_by(grid: 'hrp_assessment',
    short_name: 'HRP', version_number: 2,
    version_name: "HRP V12")

parse_csv_with_perc("db/data/newhrp.csv") do |row,i|
  guideline = Guideline.find_or_create_by(guideline_tag: row[:guideline_tag], grid_version_id: gv.id)

  if row[:parent_tag].present? && row[:parent_tag] == "BULLET"
    bullets = (guideline.attributes["note_bullets"] || [])
    bullets << row[:note_bullets]
    guideline.note_bullets = bullets.uniq
    guideline.save(validate: false)
    next
  elsif row[:parent_tag].present?
    parent = Guideline.find_by(guideline_tag: row[:parent_tag], grid_version_id: gv.id)
    guideline.parent_id = parent.id
    guideline.save(validate: false)
  end

  params = row.to_h.except(:parent_tag, :note_bullets)
  guideline.assign_attributes(params)
  guideline.save(validate: false)
end






prep_questions = JSON.parse(File.read('db/data/newhrp_prep_questions.json'))

f3 = Form.find_or_create_by(ref_model: "prep_question", title: "HRP Preparation Questions")
s3 = FormSection.find_or_create_by(form_id: f3.id, title: "Prep Questions")
f3.form_sections << s3
i3 = 1

each_with_perc(prep_questions) do |quest|
  # next unless ['dpr_assessment'].include? quest['grid']

  q_params = {
    component: quest['component'],
    label: quest['question'],
    label_en: quest['question_en'] || quest['question'],
    active: true,
    required: quest['required']||false,
    grid: quest['grid'],
    grid_version_id: gv.id
  }
  q_params[:label_zhcn] = quest['question_zhcn'] if quest['question_zhcn']
  q_params[:respondant] = quest['respondant'] if quest['respondant'].present?
  q_params[:require_attachment] = quest['require_attachment'] if quest['require_attachment'].present?
  q_params[:prep_for] = quest['prep_for'] if quest['prep_for'].present?

  q = FormQuestion.find_or_create_by(question_tag: "HRPPREP#{i3}", form_section_id: s3.id, grid_version_id: gv.id)
  q_params[:priority] = i3
  i3 += 1

  q_params[:name] = "prep_question[#{q.id}]"

  q.update(q_params)

  if quest['options'].present?
    quest['options'].each do |option|
      FormOption.find_or_create_by(form_question_id: q.id, value: option, text: option)
    end
  elsif quest['component'] == 'radio'
    ["Yes", "No", "Not Applicable"].each do |option|
      FormOption.find_or_create_by(form_question_id: q.id, value: option, text: option)
    end
  end
end
