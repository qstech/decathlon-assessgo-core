require 'csv'
require_relative '../helpers'

@rows = []
# purpose,tag,process,title,requirement,level
PRODUCTS = ["Trail running bag","Bags","Welded Bags","PFD Foam","PFD Welding","Support",
  "Flexible protection","Boxing Gloves","Boxing bag","Horse Riding Textile",
  "Umbrella","FG Net","Kite & Sail","Webbing","Horse Riding Leather","Tents",
  "Sleeping bag","Stitched balls","Bladders","Rubber Moulded Balls",
  "PVC Rotomoulded balls","Laminated Balls","THB","Feather Shuttle","Gluing",
  "Leather","Welding"].map(&:downcase).map(&:parameterize).map(&:underscore)
GRID = "hfwb_assessment"
TAG = "HFWB"

def lev_index(letter)
  ("A".."F").to_a.reverse.index(letter)
end

def product_valid?(row, product)
  val = row[product.to_sym] || ""
  val.strip.downcase == "x"
end

def relavent_products(row)
  PRODUCTS.select{|x| product_valid?(row, x)}
end

parse_csv_with_perc("db/data/hfwb.csv") do |row,i|
  # purpose,tag,process,title,requirement,level
  # toxxx, 1.0.04A,SAMPLE ROOM,General Requirement,There is a sampling room,D
  row[:title] = "" if row[:title].nil?
  row[:requirement] = "" if row[:requirement].nil?
  next if row[:tag].blank? || row[:title].include?("DELETED") || row[:requirement].include?("DELETED")

  %w(purpose tag process title requirement level).each do |col|
    row[col.to_sym] = row[col.to_sym].strip unless row[col.to_sym].nil?
  end
  row[:tag] = row[:tag].gsub(/\s/, "")

  @rows << row
end

@rows = @rows.group_by{|row| row[:tag].split(".")[0]}

@rows = @rows.map do |ch_no, rows|
  [rows[0][:process], rows.group_by{ |row| row[:tag] }]
end.to_h

# {"SAMPLE ROOM"=> {"1.0.04A"=>[....], ...}

@rows.each_with_index do |(chapter_name, sections_h), i|
  ch_no = sections_h.keys.first.split(".").first.to_i
  ch_no = "0#{ch_no}" if ch_no < 10
  ch_params = {
    ref_model: GRID, guideline_tag: "#{TAG}CH#{ch_no}",
    title: chapter_name, section_name: "Chapter #{ch_no}",
    section_type: "Chapter", parent_id: nil, section_no: i,
    active: true
  }
  ch = Guideline.find_or_create_by(guideline_tag: ch_params[:guideline_tag])
  ch.update(ch_params)

  sections_h.each_with_index do |(sec_tag, levs), j|
    no = j+1
    sec_params = {
      ref_model: GRID, guideline_tag: "#{TAG}CH#{ch_no}S#{no}",
      title: levs.first[:title], content: levs.first[:purpose],
      section_name: sec_tag, section_type: "Section",
      parent_id: ch.id, section_no: i, active: true
    }
    sec = Guideline.find_or_create_by(guideline_tag: sec_params[:guideline_tag])
    sec.update(sec_params)

    lev_count = {"A"=> 0, "B"=> 0, "C"=>0, "D" => 0, "E"=> 0}

    levs.each do |lev|
      next unless lev[:level].in?(("A".."E").to_a)
      l = lev[:level].strip
      lev_params = {
        ref_model: GRID, guideline_tag: "#{TAG}CH#{ch_no}S#{no}L#{lev_index(l)}",
        title: "Level #{l}", section_name: l,section_type: "Level",
        parent_id: sec.id, section_no: lev_index(l), active: true
      }
      level = Guideline.find_or_create_by(guideline_tag: lev_params[:guideline_tag])
      level.update(lev_params)

      lev_count[l] += 1
      point_tag = "#{TAG}CH#{ch_no}S#{no}L#{lev_index(l)}-#{lev_count[l]}"
      point_params = {
        ref_model: GRID, guideline_tag: point_tag,
        title: "#{l}-#{lev_count[l]}", section_name: "#{l}-#{lev_count[l]}",
        section_type: "Point", parent_id: level.id, section_no: lev_count[l],
        active: true, content: lev[:requirement].strip
      }
      point = Guideline.find_or_create_by(guideline_tag: point_params[:guideline_tag])
      point.update(point_params)

      point.merge_relavent_products!(relavent_products(lev))
      level.merge_relavent_products!(relavent_products(lev))
      sec.merge_relavent_products!(relavent_products(lev))
      ch.merge_relavent_products!(relavent_products(lev))
    end
  end
end
