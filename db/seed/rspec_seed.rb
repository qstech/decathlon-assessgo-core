require 'csv'

path = "db/data/rspec_guidelines.csv"
csv_options = {headers: :first_row, header_converters: :symbol }

CSV.foreach(path, csv_options).with_index do |row, i|
    guideline = Guideline.find_or_create_by(guideline_tag: row[:guideline_tag])

    if row[:parent_tag].present? && row[:parent_tag] == "BULLET"
      guideline.note_bullets = (guideline.attributes["note_bullets"] || [])
      guideline.note_bullets << row[:note_bullets]
      guideline.note_bullets = guideline.note_bullets.uniq
      guideline.save(validate: false)
      next
    elsif row[:parent_tag].present?
      parent = Guideline.find_by(guideline_tag: row[:parent_tag])
      guideline.parent_id = parent.id
      guideline.save(validate: false)
    end

    params = row.to_h.except(:parent_tag, :note_bullets)
    guideline.assign_attributes(params)
    guideline.save(validate: false)
  end
