require 'csv'
parse_csv_with_perc("db/data/assessor_profiles.csv") do |row, i|
  assessor_profile = AssessorProfile.find_or_create_by(name: row[:name])
  params = row.to_h.except(:no)
  params[:assessor_for] = params[:assessor_for].blank? ? [] : params[:assessor_for].split(", ")
  params[:candidate_for] = params[:candidate_for].blank? ? [] : params[:candidate_for].split(", ")
  params[:referent_for] = params[:referent_for].blank? ? [] : params[:referent_for].split(", ")
  params[:ref_candidate_for] = params[:ref_candidate_for].blank? ? [] : params[:ref_candidate_for].split(", ")
  if params[:handle].present?
    params[:handle] = params[:handle].downcase
    u = User.find_by(username: params[:handle])
    u = User.create(username: params[:handle], password: "123123") if u.blank?
    params[:user_id] = u.id
  end
  assessor_profile.update(params)
end
