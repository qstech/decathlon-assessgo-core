require 'csv'
require 'json'
require_relative '../helpers'

gv = GridVersion.find_or_create_by(grid: 'hrp_assessment',
    short_name: 'HRP', version_number: 2,
    version_name: "HRP V12")

form = Form.find_or_create_by(ref_model: "hrp_assessment", title: "Dormitory Questions", grid_version_id: gv.id)
section = ""
sec = nil
sec_priority = 1

parse_csv_with_perc("db/data/dormitory.csv") do |row,i|
  if sec.nil? || (row[:section_name] != sec.title)
    sec = FormSection.find_or_create_by(form_id: form.id, title: row[:section_name])
    sec.update(priority: sec_priority)
    form.form_sections << sec
    sec.save
    sec_priority += 1
  end

  q_params = {
    component: sec.priority < 4 ? 'input' : 'radio',
    label: row[:question],
    label_en: row[:question],
    active: true,
    required: row[:required] == 'TRUE',
    priority: row[:priority],
    grid: 'hrp_assessment',
    grid_version_id: gv.id,
    respondant: 'dormitory',
    require_attachment: row[:require_attachment] == 'TRUE',
    with_remarks: row[:with_remarks]
  }

  q = FormQuestion.find_or_create_by(question_tag: "HRPDORMS#{sec.id}Q#{row[:priority]}", form_section_id: sec.id, grid_version_id: gv.id)
  q_params[:name] = "prep_question[#{q.id}]"
  q.update(q_params)
end
