# TRIGGERS
# "manual", "user_followup_5_login", "user_followup_5_no_login", "user_after_login",
# "comment_create", "control_plan_create", "control_plan_completed", "invitation_create",
# "invitation_accepted", "invitation_rejected", "assessment_create", "assessment_completed",
# "assessment_reminder", "assessment_destroyed", "bug_report_create", "bug_report_completed",
# "cap_followup_create", "cap_followup_reminder", "kpi_record_create", "product_family_create",
# "supplier_email_changed", "supplier_update", "supplier_gemba_creation", "assessor_application_create",
# "assessor_application_destroyed", "assessor_application_update", "assessor_application_completed",
# "custom_trigger"

ADMINS = User.select(:id).where(username: ["kkang20", "wxu50"]).map(&:id)
NOTIF_SET = JSON.parse(File.read('./db/data/mailbags/notif_mailbags.json'))


def create_mailbag(trigger,file,audience,subject)
  body   = File.read("./db/data/mailbags/#{file}.html")
  params = {subject: subject, body: body, trigger: trigger, tag: file,
    name: file.humanize, audience: audience, cc: ADMINS}
  MailBag.create(params)
end

create_mailbag("manual", "oa_invite", "manual", 'Invitation to AssessGo Official Account Notifications')
create_mailbag("custom_trigger", "reset_password", "user", "Your New Password")
create_mailbag("supplier_email_changed", "supplier_email_changed", "contact_email", "Email Changed")
create_mailbag("supplier_gemba_creation","supplier_gemba_creation", "user", "Assess Go has created a Gemba assessment for you")
create_mailbag("user_after_login", "thanks_followup", "user", "Thanks for Using Assess Go!")
create_mailbag("user_followup_5_login", "user_followup_5_login", "user", "user_followup_5_login")
create_mailbag("user_followup_5_no_login", "user_followup_5_no_login", "user", "user_followup_5_no_login")
create_mailbag("custom_trigger", "welcome", "user", "Invitation to Decathlon's Assess Go")

NOTIF_SET.values.flatten.each do |h|
  h["cc"] = ADMINS
  h["name"] = (h["tag"] || h["trigger"]).humanize
  h["tag"] = (h["tag"] || h["trigger"])

  MailBag.create(h)
end
