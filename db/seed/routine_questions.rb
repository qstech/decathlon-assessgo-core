require 'csv'
require_relative '../helpers.rb'

RoutineQuestion.destroy_all
xxx = %w(SSECH2S1 SSECH2S2 SSECH2S3 SSECH2S4 SSECH2S5)
parse_csv_with_perc("db/data/sse_routine_questionsv2.csv") do |row,i|

  next if row[:chapter].nil?
  puts "ATTEMPTING #{i+1}"
  p row
  tag  = "SSECH"
  ch   = row[:chapter].match(/(\d+)\.(\d+)/)
  tag += "#{ch[1]}S#{ch[2]}"

  sec  = row[:level].strip.match(/([ABCDE])(\d+)/)
  ptn = sec[2].to_i
  ptn += 1 if xxx.include?(tag) && sec[1] == "D"
  pt   = "#{sec[1]}-#{ptn}"
  gl = Guideline.where("guideline_tag LIKE ?", "#{tag}%")
  .where("section_name LIKE ?", "%#{pt}%").first

  if gl.blank?
    10.times{puts "GUIDELINE NOT FOUND!!! #{tag} #{pt}"}
  else
    rq = RoutineQuestion.find_or_create_by({guideline_id: gl.id})
    rq.how_to_check = row[:how_to_check].split("\n").map(&:strip).select(&:present?)
    rq.how_to_check_zhcn = row[:how_to_check_zhcn].split("\n").map(&:strip).select(&:present?)
    rq.questions = row[:questions].split("\n").map(&:strip).select(&:present?)
    rq.questions_zhcn = row[:questions_zhcn].split("\n").map(&:strip).select(&:present?)
    rq.frequency = row[:frequency]
    rq.save
  end
end
