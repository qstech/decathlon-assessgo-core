module RankHelper
  def render_partial(i)
    case i
    when 0
      'first.jpg'
    when 1
      'second.jpg'
    when 2
      'third.jpg'
    else
      '   '
    end
  end
end
