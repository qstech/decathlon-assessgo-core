require 'iso_country_codes'

module ApplicationHelper
  include Pagy::Frontend

  def country_name(abbr)
    names = { 'VN' => 'Vietnam' }
    return 'Not Set' if ['', nil].include? abbr

    names[abbr] || IsoCountryCodes.find(abbr).name
  rescue IsoCountryCodes::UnknownCodeError
    abbr
  end

  def access_levels_badge(i)
    case i
    when 'suppliers'
      '#edeff3'
    when 'guidelines'
      '#ed9f90'
    when 'my-reports'
      '#b0f566'
    when 'all-reports'
      '#4af2a1'
    else
      '#5cc9f5'
    end
  end

  def bug_report_status_badge(status)
    case status
    when 'unconfirmed'
      'unconfirmed-status'
    when 'pending'
      'pending-status'
    when 'debugged'
      'debugged-status'
    when 'published'
      'published-status'
    when 'closed'
      'closed-status'
    when 'rejected'
      'rejected-status'
    else
      ''
    end
  end

  def bug_report_priority_badge(priority)
    "#{priority}-priority"
  end

end
