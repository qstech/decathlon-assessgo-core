class GuidelinesController < BaseController
  def index
    @active = 'guidelines'
  end

  def show
    @active = 'guidelines'
    @guidelines = Guideline.fetch_for(params[:grid])
    @short_name = params[:grid] == 'quality_assessment' ? 'QA' : params[:grid].split('_').first.upcase
    @title = params[:grid].titleize
  end

  def edit
    @active = 'guidelines'
    @guideline = Guideline.find(params[:id])
    @guideline.build_routine_question if @guideline.routine_questions.count.zero? && (params[:new_routine] == 'true')
  end

  def update
    @guideline = Guideline.find(params[:id])
    gl_params = guideline_params
    gl_params[:process] = nil if gl_params[:process] == ''
    if @guideline.update(gl_params)
      render json: { code: 200, msg: 'success', object: @guideline.standard_hash }
    else
      redirect_to edit_guideline_path(params[:id])
    end
  end

  def prep
    @questions = FormQuestion.includes(form_section: :form)
                             .where(grid: params[:grid], destroyed_at: nil, respondant: (params[:form] || 'assessor'))
                             .order(:priority)
    @form = @questions.first.form_section.form
  end

  def frequency
    @index = NextAssessmentSchedule.where(grid: params[:grid], destroyed_at: nil)
    @frequency = NextAssessmentSchedule.find_by(id: params[:id]) || NextAssessmentSchedule.new
  end

  def frequency_update
    puts "frequency_params ==> #{frequency_params}"
    if params[:id].present?
      @frequency = NextAssessmentSchedule.find_by(id: params[:id])
      @frequency.update(frequency_params)
    else
      @frequency = NextAssessmentSchedule.create!(frequency_params)
    end
    redirect_to "/guidelines/frequency?grid=#{params[:grid]}"
  end

  def toolbox
    @guidelines = Guideline.fetch_for(params[:grid])
    @short_name = params[:grid] == 'quality_assessment' ? 'QA' : params[:grid].split('_').first.upcase
    @title = params[:grid].titleize
  end

  private

  def permission_scope
    current_user.can_manage?('guidelines')
  end

  def guideline_params
    params.require(:guideline).permit(
      :destroyed_at, :title, :content, :note_header, :title_en, :content_en, :note_header_en,
      :title_zhcn, :content_zhcn, :note_header_zhcn, :title_fr, :content_fr, :note_header_fr,
      :title_vi, :content_vi, :note_header_vi, :toolbox_mode, :process,
      response_choices: [], toolbox_images: [], toolbox_slides: [], note_bullets: [],
      note_bullets_en: [], note_bullets_zhcn: [], note_bullets_fr: [], note_bullets_vi: [],
      toolbox_slides_en: [], toolbox_slides_zhcn: [], toolbox_slides_fr: [], toolbox_slides_vi: [],
      routine_questions_attributes: routine_question_params
    )
  end

  def routine_question_params
    [:id] + RoutineQuestion::CONTENT_FIELDS.map do |x|
      %i[en zhcn fr vi].map { |locale| { "#{x}_#{locale}".to_sym => [] } }
    end.flatten
  end

  def frequency_params
    params.require(:next_assessment_schedule).permit(:grid, :assess_type, :next_assessment, :time_unit,
                                                     :after_assess_type, :supplier_type, :result_condition, :active, { notify_in: [] }, { notify_what: [] }, { countries: {} }, :destroyed_at)
  end
end
