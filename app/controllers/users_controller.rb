require 'creek'
class UsersController < BaseController
  def users
    @active = 'users'
  end

  def staff
    if params[:id].present?
      setup_qr(:user)
    else
      @active = 'users'
      users = User.includes(:assessor_profile, :authentication_tokens)
                  .where('username NOT LIKE ?', '%@%').where.not(assessor_profiles: { id: nil, name: nil })
                  .order(created_at: :desc).map(&:assessor_profile)
      @staff = users
      @qa_uids = AssessorAssignment.select(:user_id).all.distinct.map(&:user_id)
      @cps_uids = ControlPlan.select(:created_by).where(destroyed_at: nil).distinct.map(&:created_by)
    end
  end

  def email_user
    @assessors = User.includes(:assessor_profile, :authentication_tokens).where.not(email: '',
                                                                                    assessor_profiles: { id: nil,
                                                                                                         name: nil }).order(created_at: :desc).select do |x|
      x.assessor_profile.assessor_for.present?
    end
    @suppliers = SupplierProfile.includes(:user,
                                          user: :authentication_tokens).where.not(user_id: nil).order(created_at: :desc).map(&:user).select do |u|
      !u.email.empty?
    end
    @emails_filter = SupplierProfile.all.map { |x| { label: "#{x.name} Assessment", value: x.cpm } }
    @decathlonians = User.includes(:assessor_profile, :authentication_tokens).where('email ILIKE ?',
                                                                                    '%@decathlon%').where.not(assessor_profiles: {
                                                                                                                id: nil, name: nil
                                                                                                              })
    @grid_leaders = User.includes(:assessor_profile, :authentication_tokens).where('email ILIKE ?',
                                                                                   '%@decathlon%').where.not(assessor_profiles: {
                                                                                                               id: nil, name: nil
                                                                                                             }).select do |x|
      x.access_levels.include?('grid') || x.access_levels.include?('grids') || x.access_levels.include?('guidelines')
    end
  end

  def send_email
    users = params[:users]
    email_template = params[:email_template]
    users.each do |u|
      User.find(u).send_mail!(email_template)
    end
    redirect_to email_user_path
  end

  def permissions; end

  def post_permissions
    u_params = params.require(:user).permit(:file, access_levels: [])
    @file = u_params[:file]
    creek = Creek::Book.new(@file.tempfile)
    sheet = creek.sheets.first
    sheet.rows_with_meta_data.each do |row|
      full_cells = row['cells'].reject { |_k, v| v.nil? }
      next if full_cells.blank?

      profile = full_cells.values.first
      next if profile == 'profile'

      u = User.find_for_authentication(username: profile)
      next if u.blank?

      u.access_levels += u_params[:access_levels]
      u.access_levels.uniq!
      u.save
    end
    redirect_to staff_path
  end

  def permissions_all
    base = User.includes(:assessor_profile, :supplier_profile)
    @users = case params[:type]
             when 'supplier'
               base.where.not(supplier_id: nil).or(base.where.not({ supplier_profiles: { id: nil } }))
             when 'assessor'
               base.where.not(assessor_profiles: { assessor_for: [] })
             when 'grid_leader'
               base.where("'guidelines' = ANY (users.access_levels)")
             else
               base.all
             end
  end

  def patch_permissions
    u = User.find_by(id: params[:user_id])
    u_params = params.require(:user).permit(access_levels: [],
                                            assessor_profile_attributes: [:id, :user_id,
                                                                          { assessor_for: [], referent_for: [] }])
    p u_params
    u.update(u_params)
    redirect_to staff_path
  end

  def locked
    @active = 'users'
    @users = User.includes(:assessor_profile, :supplier_profile, :supplier).where.not(locked_at: nil)
  end

  # ===============================
  # SWAL CONFIRMED ACTIONS
  # ===============================
  def reset
    @user = User.find_by_username(params[:username])
    pass = 'Decathlon' # SecureRandom.urlsafe_base64(4).upcase
    @user.password = pass
    if @user.save
      render json: { msg: "New password for #{params[:username]} is <span style='color:red'>#{pass}</span>" }
    else
      render json: { msg: 'Password Update Failed' }
    end
  end

  def lock
    @user = User.find_by_username(params[:username])
    @user.locked_at = Time.now
    if @user.save
      render json: { msg: "<span style='color:red'>#{params[:username]}</span> Account Locked!" }
    else
      render json: { msg: 'Account Lock Failed' }
    end
  end

  def unlock
    @user = User.find_by_username(params[:username])
    @user.locked_at = nil
    if @user.save
      render json: { msg: "<span style='color:red'>#{params[:username]}</span> Account Unlocked!" }
    else
      render json: { msg: 'Account Unlock Failed' }
    end
  end

  def invite
    @user = User.find_by_username(params[:username])
    @supplier = @user.supplier || @user.supplier_profile

    if @supplier.present?
      @user.send_mail!('welcome')
      if @supplier.update invite_sent_at: Time.now
        render json: { msg: "Invitation sent to #{params[:username]} - #{@supplier.name}" }
      else
        render json: { msg: 'Sending Invitation Failed' }
      end
    end
  end

  def test_login
    puts "testing login\n\n\n\n#{params}"
    email_found = User.find_by(email: params['email'])
    puts "email found? #{email_found}"
    valid_password = email_found.valid_password?(params['password']) unless email_found.nil?
    puts "password valid? #{valid_password}"

    if email_found.nil?
      render json: { msg: 'User not found' }
    elsif !valid_password
      render json: { msg: 'Password incorrect' }
    else
      render json: { msg: 'This account is valid' }
    end
  end

  private

  def setup_qr(type)
    qr = {
      user: { page: 'members/show/show', scene: "id=#{params[:id]}" },
      supplier: { page: 'members/show/show', scene: "supplierid=#{params[:id]}" }
    }

    h = qr[type]
    render json: WxAuthentication.mp_qrcode(h)
  end

  def permission_scope
    current_user.can_manage?('users')
  end
end
