class BookmarksController < ApplicationController
  before_action :set_active

  def strong_points
    @pagy, @strong_points = pagy(ActiveStorage::Attachment.where(name: 'strong_points_images', bookmark: true),
                                 items: 8)
  end

  def points_to_improve
    @pagy, @points_to_improve = pagy(
      ActiveStorage::Attachment.where(name: 'points_to_improve_images', bookmark: true), items: 8
    )
  end

  private

  def set_active
    @active == 'bookmarks'
  end
end
