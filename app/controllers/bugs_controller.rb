require 'csv'
class BugsController < BaseController
  skip_before_action :authenticate_user!, only: [:bug_tracker_form]
  before_action :set_active

  layout 'native', only: %i[bug_tracker_form bug_submitted]

  def bug_tracker_form
    @bug_report = BugReport.find_by(id: params[:bug_id]) || BugReport.new
  end

  def bug_reports
    set_bug_reports
  end

  def bug_report
    @bug_report = BugReport.find_by(id: params[:id])
  end

  def generate_excel_bug_report
    set_bug_reports
    send_data generate_csv(@bug_reports), filename: "bugs-#{Date.today}.csv"
  end

  def bug_submitted; end

  def update_bug_status
    @bug_report = BugReport.find_by(id: params[:id])
    bug_report_params = params.require(:bug_report).permit(:status, :analysis, :assignee, :priority)
    @bug_report.update(bug_report_params)
    @bug_report.update_related! if @bug_report.backtrace.present?
    redirect_to bug_report_path(id: @bug_report.id)
  end

  private

  def set_bug_reports
    @bug_reports = BugReport.for_user(current_user)
    @bug_reports = @bug_reports.group_by(&:group_tag)
                               .map { |_, v| v.first.with_count(v.count) }
                               .flatten.sort_by(&:created_at)
  end

  def set_active
    @active = 'bug'
  end

  def permission_scope
    true
  end

  def generate_csv(bugs)
    CSV.generate(headers: true) do |csv|
      csv << %w[report_date submission supplier_code crash_level reported_by
                status priority asignee bug_description url]

      bugs.each { |report| csv << report.table_row }
    end
  end
end
