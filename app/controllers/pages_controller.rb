class PagesController < BaseController
  def dashboard
    @active = 'home'
    @comments_weekly_count = Comment.weekly_count
    @logins = User.daily_logins
    @reports_finished = QualityAssessment.where(completed: true,
                                                destroyed_at: nil).count + ControlPlan.where(completed: true,
                                                                                             destroyed_at: nil).count
    login_today

    @goes_feeds = GoesActivity.last(10).sort_by(&:created_at).reverse.map { |x| PageComponent.activity_card(x) }
    @comments = Comment.last(10).sort_by(&:created_at).reverse.map { |x| PageComponent.comment_card(x, current_user) }

    @more_activities = true if GoesActivity.count > 10
    @more_comments = true if Comment.count > 10

    days_and_weeks
  end

  def goes
    @active = 'goes'
    @goes_feeds = GoesActivity.includes(user: %i[assessor_profile supplier_profile supplier])
                              .order(created_at: :desc).first(12).map { |x| PageComponent.activity_card(x) }
    @supplier_feeds = []
    # all.select{|x| x.user.profile.class.name == "SupplierProfile"}.last(6).sort_by{|x| x.created_at}.reverse.map{|x| PageComponent.activity_card(x)}
    if params[:id].present?
      h = { page: 'members/activity/show', scene: "id=#{params[:id]}" }
      render json: WxAuthentication.mp_qrcode(h)
    end
  end

  def lock
    @user = User.find_by(username: params[:username])
    @user.lock_access!({ send_instructions: false })
    render json: { code: 200, msg: 'locked' }
  end

  def unlock
    @user = User.find_by(username: params[:username])
    @user.unlock_access!
    render json: { code: 200, msg: 'unlocked' }
  end

  def new_password
    @user = User.find_by(username: params[:username])
    new_password = 'Decathlon' # SecureRandom.urlsafe_base64(4).upcase
    @user.update(password: new_password)
    render json: { code: 200, msg: new_password }
  end

  def login; end

  def upload_cp
    @active = 'upload-cp'
    if request.method == 'POST'
      file = params.require(:control_plan).permit(:excel_file)['excel_file']
      excel = ExcelParser.new(file)
      @code = excel.code
      @results = excel.parse_excel
    elsif request.method == 'PUT'
      JSON.parse(params[:jsondata]).each do |item|
        ExcelParser.seed_product(item, params[:code])
      end
      render json: { code: 200, msg: 'FINISHED!' }
    end
  end

  def upload_historical_assessments
    @active = 'upload-assessment'
    if request.method == 'POST'
      file = params.require(:quality_assessment).permit(:excel_file)['excel_file']
      excel = ExcelParser::HistoricalAssessment.new(file)
      excel.parse_excel
      @done = true
    end
  end

  def control_plan
    @active = 'control-plan'
    @suppliers = SupplierProfile.includes(:product_families)
                                .where.not(code: [nil, ''], product_families: { id: nil })
                                .order(:code)
  end

  def new_change_log
    @active = 'change-log'
  end

  def help
    @active = 'help'
  end

  def version; end

  def google_slide
    parse_slide_url
    google = "#{@host}/#{@path}?id=0&presentation=#{@pres}&page=#{@page}"
    slide = JSON.parse(RestClient.get(google).body)

    if china_request?
      redirect_to slide['cn_url']
    else
      redirect_to slide['url']
    end
  end

  private

  def permission_scope
    true
  end

  def supplier_params
    params.require(:supplier_profile).permit(:code, :name, :address, :country, :city, :contact_name, :contact_email,
                                             :active, :partner)
  end

  def days_and_weeks
    @this_week = days_ago(0, 6).merge(login_count(0, 6)).values
    @last_week = days_ago(7, 13).merge(login_count(7, 13)).values
    @days = days_ago(0, 6).keys.sort.map { |x| x.strftime('%A') }

    @from = params[:from].present? ? Time.parse(params[:from]) : 1.months.ago
    @to = params[:to].present? ? Time.parse(params[:to]) : Time.now
  end

  def days_ago(start, finish)
    (start..finish).to_a.reverse.map { |x| [x.days.ago.to_date.to_time(:utc), 0] }.to_h
  end

  def login_count(start, finish)
    AuthenticationToken.where(created_at: (finish.days.ago..start.days.ago))
                       .group("DATE_TRUNC('day', created_at)").count
  end

  def login_today
    todays_users = User.includes(:authentication_tokens)
                       .where(authentication_tokens: { last_used_at: (1.days.ago..Time.now) }).map(&:id)
    todays_assessors = User.joins(:assessor_profile).where(id: todays_users).map(&:id)
    todays_suppliers = todays_users - todays_assessors

    @todays_logins = AssessorProfile.where(user_id: todays_users).map(&:name) +
                     User.where(id: todays_suppliers).map(&:username)
  end

  def parse_slide_url
    parsed = params[:slide_url].match(%r{/d/([^/]*)/.*slide=id\.(\w*)})

    @host = Rails.application.credentials.dig(:google, :api)
    @path = 'api/v1/google/slides_thumbnail'
    @pres = parsed[1]
    @page = parsed[2]
  end
end
