require 'rest-client'
require 'json'

class ApplicationController < ActionController::Base
  before_action :check_login
  before_action :authenticate_user!
  before_action :check_permission

  include Pagy::Backend
  # protect_from_forgery

  def check_login
    if params[:key] == NativeUrl::KEY # && !user_signed_in?
      sign_out current_user if user_signed_in?
      u = User.find_by(id: params[:id]) || User.find_by(username: params[:username])
      sign_in u
    end
  end

  def default_url_options
    { locale: I18n.locale }
  end

  private

  def check_permission
    p request.remote_ip
    redirect_to root_path if user_signed_in? && !(current_user.admin || permission_scope)
    p 'WOW'
  end

  def permission_scope
    false
  end

  def china_request?
    i2l = Ip2location.new.open("#{Rails.root}/lib/IP2LOCATION.BIN")
    i2l.get_country_short(request.remote_ip.to_s) == 'CN'
  end
end
