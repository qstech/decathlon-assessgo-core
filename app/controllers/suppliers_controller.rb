class SuppliersController < BaseController
  def suppliers
    puts "params..... ===> #{params}"
    status = params[:status] || 'all'

    @active = 'suppliers'
    @can_invite = current_user.can_manage?('invites')
    supplier_base = SupplierProfile.includes(user: :authentication_tokens, users: :authentication_tokens)

    users = supplier_base.where.not(user_id: nil).order(created_at: :desc)
    suppl = supplier_base.where(user_id: nil).order(created_at: :desc)
    @suppliers = users + suppl
    if status == 'active'
      @suppliers = @suppliers.select { |x| x.last_login.present? }
    elsif status == 'inactive'
      @suppliers = @suppliers.reject { |x| x.last_login.present? }
    elsif status == 'assessment'
      date = params[:date]
      grid = params[:grid]
      users = users.filter_by_grid_and_date(grid, date)
      suppl =  suppl.filter_by_grid_and_date(grid, date)
      @suppliers = (users + suppl)
    end

    @supplier_count = @suppliers.count
    setup_qr(:supplier)
  end

  def suppliers_new
    @active = 'users'
    @is_new = true
    @supplier = SupplierProfile.new
    @assessors = AssessorProfile.all
    @grids = QualityAssessment::ACTIVE_GRIDS + ['CP']
  end

  def suppliers_edit
    @is_new = false
    @supplier = SupplierProfile.find(params[:id])
    @assessors = AssessorProfile.all
    @grids = QualityAssessment::ACTIVE_GRIDS + ['CP']
    # raise
  end

  def supplier_create
    # @user = User.create(username: params[:supplier_profile][:contact_email], password: 123456, password_confirmation: 123456)
    @supplier = SupplierProfile.new(supplier_params)
    @supplier.user_id = @user.id
    redirect_to suppliers_path if @supplier.save
  end

  private

  def setup_qr(type)
    qr = {
      user: { path: 'members/show/show', scene: "id=#{params[:id]}" },
      supplier: { path: 'members/show/show', scene: "supplierid=#{params[:id]}" }
    }

    if params[:id].present?
      h = qr[type]
      render json: WxAuthentication.mp_qrcode(h)
    end
  end

  def permission_scope
    current_user.can_manage?('users') || current_user.can_manage?('suppliers')
  end
end
