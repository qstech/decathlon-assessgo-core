class OauthController < ActionController::Base
  def callback
    20.times { puts '' }
    puts 'OAUTH CALLBACK:'
    puts request.remote_ip

    render json: params
  end

  def callback_web
    # TODO
    code = params['code']
    if code
      auth = DecathAuthentication.fulfill_oauth_web(code)
      user_info = DecathAuthentication.format_response(auth)
      user = DecathAuthentication.assign_user(user_info[:username])
      user.update(access_levels: ['reports']) if user.access_levels.blank?
      sign_in user
    end
    redirect_to root_path
  end
end
