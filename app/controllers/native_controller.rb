class NativeController < BaseController
  layout 'native'
  skip_before_action :authenticate_user!, only: %i[new_supplier request_access]

  def survey_questions
    @no_toast = true
    @qa = QualityAssessment.find(params[:qa_id])
    @survey_questions = @qa.survey_questions.includes(form_question: :form_section)
                           .order('form_questions.priority ASC, form_sections.priority ASC')
    @sections = @survey_questions.group_by(&:form_section)
    @form = @sections.keys.first.form
  end

  def new_supplier; end

  def request_access
    # TODO: Add Email to Haidee Yu
  end

  def cp_index
    @control_plans = current_user.control_plans.where(completed: false, destroyed_at: nil).sort_by do |x|
      x.schedule ? x.schedule.supplier_schedule_date : Time.now
    end
    @control_plans.map! { |x| PageComponent.index_cp_card(x) }
  end

  def cp_search
    @cards = []
    if params[:input].present?
      pf_query = " \
        name ILIKE :query \
      "
      supplier_query = " \
        name ILIKE :query \
        OR code ILIKE :query \
      "

      supplier_results = SupplierProfile.where(supplier_query, query: "%#{params[:input]}%").limit(20)
      pf_results = ProductFamily.where(pf_query, query: "%#{params[:input]}%").where(destroyed_at: nil).limit(20)

      @cards = pf_results.map { |x| PageComponent.pf_search_card(x, current_user) } +
               supplier_results.map { |x| PageComponent.supplier_search_card(x) }
    end
  end

  def cp_product_list
    @supplier = SupplierProfile.find(params[:supplier_id].to_i)
    @productFamilies = @supplier.product_families.includes(:process_steps)
                                .where(destroyed_at: nil).map { |x| PageComponent.pf_search_card(x, current_user) }
  end

  def cp_new_product
    if params[:product_id].present?
      @product_family = ProductFamily.find_by(id: params[:product_id].to_i)
      @supplier = @product_family.supplier_profile
      @images = @product_family.process_flow_images_urls
    elsif !params[:edit].present?
      @supplier = SupplierProfile.find(params[:supplier_id].to_i)
      @product_family = ProductFamily.new(supplier_profile_id: @supplier.id)
      @images = @product_family.process_flow_images_urls
    else
      @product_family = ProductFamily.find_by(id: params[:id].to_i)
      @supplier = @product_family.supplier_profile
      @images = @product_family.process_flow_images_urls
    end
  end

  def cp_product
    @product_family = if params[:product_id]
                        ProductFamily.find(params[:product_id].to_i)
                      else
                        ProductFamily.find(params[:id].to_i)
                      end
    @supplier = @product_family.supplier_profile
    cps = @product_family.control_plans.order(:assess_date)
    @last_cp = cps.select { |x| x.assess_date < Time.now }.last
    @next_cp = cps.select { |x| x.assess_date > Time.now }.first
    @last_cp_review = @last_cp.present? ? @last_cp.assess_date.strftime('%Y-%m-%d') : nil
    @next_cp_review = @next_cp.present? ? @next_cp.assess_date.strftime('%Y-%m-%d') : nil

    @h = {
      productFamily: {
        id: @product_family.id,
        last_updated: @product_family.updated_at.strftime('%Y-%m-%d'),
        name: @product_family.name,
        images: @product_family.process_flow_images_urls,
        last_cp_review: @last_cp_review || 'N/A',
        next_cp_review: @next_cp_review || 'N/A',
        current_level: @product_family.current_level,
        target_level: @product_family.target_level
      },
      recentUpdates: @product_family.recent_changes(1),
      pcpSteps: {
        available: @product_family.available_count,
        reviewed: @product_family.reviewed_count,
        pending: @product_family.pending_count
      },
      kpiCard: {
        nonconformity_rate: @product_family.nonconformity_rate,
        frequency_respect_rate: @product_family.freq_respect_rate,
        supplier_frequency_respect_rate: @product_family.freq_respect_rate('supplier'),
        cap_close_rate: @product_family.cap_close_rate
      }
    }
    if @next_cp_review.present?
      schedule_hash = {
        scheduleReview: {
          lastReview: @last_cp_review,
          nextReview: @next_cp_review,
          nextSupplierReview: '',
          details: "#{@next_cp.plan_items.count} Steps Need Review Soon"
        }
      }
      @h.merge!(schedule_hash)
    end
  end

  def cp_schedule
    schedule_hash = {
      header: {
        bgColor: 'blue'
      },
      cp_type: ControlPlan::ASSESS_TYPE,
      process_categories: ProductProcess::CATEGORY,
      supplierSource: SupplierProfile.source_list
    }

    if params[:product_id].present?
      # if params[:product_id].present?
      @product_family = ProductFamily.find(params[:product_id])
      # else
      #   @product_family = ProductFamily.find(params[:id])
      # end
      @supplier = @product_family.supplier_profile
      process_map = @product_family.process_map

      schedule_hash.merge!({
                             product_id: @product_family.id,
                             productFamily: @product_family.standard_hash,
                             stepCount: @product_family.standard_hash[:stepCount],
                             supplier: @supplier,
                             productProcesses: process_map
                           })

    elsif params[:supplier_id].present?
      @supplier = SupplierProfile.find_by(id: params[:supplier_id])
      @product_family = @supplier.product_families.where(destroyed_at: nil).first || {}
      process_map = @product_family.blank? ? [] : @product_family.process_map

      schedule_hash.merge!({
                            product_id: @product_family.blank? ? 0 : @product_family.id,
                            productFamily: @product_family.blank? ? {} : @product_family.standard_hash,
                            stepCount: @product_family.blank? ? 0 : @product_family.standard_hash[:stepCount],
                            supplier: @supplier,
                            productProcesses: process_map
                          })
    else
      @product_family = ProductFamily.new
      @supplier = current_user.supplier || current_user.supplier_profile
      schedule_hash.merge!({
                             product_id: 0,
                             productFamily: {},
                             supplier: @supplier || {},
                             stepCount: 0,
                             productProcesses: {},
                             supplierInfo: {}
                           })
    end

    @h = schedule_hash
  end

  def cp_review
    @control_plan = ControlPlan.find(params[:cp_id])
    @product_family = @control_plan.product_family
    @supplier = @control_plan.product_family.supplier_profile
    plan_items = @control_plan.plan_items.order(:id)
    plan_items_array = plan_items.map { |x| x.process_step_id }
    product_processes = ProductProcess.where(product_family_id: @product_family.id).includes(:process_steps)
    pp_array = []

    product_processes.each do |process|
      p_hash = process.standard_hash.merge({ steps: [] })

      process.process_steps_active.each do |step|
        next unless plan_items_array.include?(step.id)

        plan_item = plan_items.select { |x| x.process_step_id == step.id }[0]
        p_hash[:steps].push(step.standard_hash.merge({
                                                       plan_item_id: plan_item.id,
                                                       plan_item_completed: plan_item.completed,
                                                       plan_item_result: plan_item.result
                                                     }))

        p_hash[:steps].sort_by! { |x| x[:step_number] }
        # else
        #   p_hash[:steps].push({
        #     id: step.id,
        #     step_number: step.step_number,
        #     control_item: step.control_item,
        #     criticity: step.criticity,
        #   })
      end
      pp_array.push(p_hash) if p_hash[:steps].length > 0
    end

    @active_steps = pp_array.map { |x| x[:steps].select { |s| !s[:plan_item_id].nil? } }.flatten
    @active_steps.sort_by! { |x| x[:step_number] }
    @processList = pp_array.group_by { |x| x[:category] }
    @categories = ProductProcess::CATEGORY
  end

  def cp_review_detail
    @plan_item = PlanItem.find(params[:plan_item_id])
    @process_step = @plan_item.process_step
    @control_plan = @plan_item.control_plan
    @plan_items = @control_plan.plan_items.order(:id)
    plan_items_array = @plan_items.map { |x| x.process_step_id }
    @product_family = @control_plan.product_family
    @supplier = @product_family.supplier_profile
    @product_processes = ProductProcess.where(product_family_id: @product_family.id).includes(:process_steps)
    @next_index = @plan_items.index(@plan_item) + 1
    @next = @plan_items[@next_index] || @plan_items[0]

    @t = {}
    @t.merge!(t('common'))
    @t.merge!(t('api.v1.control_plan.step_detail.step_detail_card'))
  end

  def cp_findings
    @cp = ControlPlan.find(params[:cp_id])
    @plan_items = @cp.plan_items.includes(:process_step).where(plan_items: { destroyed_at: nil }).order(:id)
                     .sort_by { |x| x.process_step.step_number }
  end

  def cp_history
    @product_family = ProductFamily.find(params[:product_id])
    @supplier = @product_family.supplier_profile
    @history_set = @product_family.recent_changes(20).map do |x|
      if x[:whodunit].present?
        prof = User.find_by(id: x[:whodunit]).profile
        x[:user] = prof.name
        x[:decathlon] = prof.instance_of?(AssessorProfile)
      end
      x
    end
  end

  def cp_cphistory
    cps = ControlPlan.where(product_family_id: params[:product_family_id])
    @cps = cps.map { |x| PageComponent.index_cp_card(x) }
  end

  def cp_kpi
    @product_family = ProductFamily.find_by(id: params[:product_id])
    @h = @product_family.kpi_details
  end

  def cp_list
    @product_family = if params[:product_id]
                        ProductFamily.find(params[:product_id])
                      else
                        ProductFamily.find(params[:id])
                      end
    @supplier = @product_family.supplier_profile
    @product_processes = ProductProcess.where(product_family_id: @product_family.id).includes(:process_steps).order(:number)
    @process_map = @product_processes.map { |x| PageComponent.product_process_to_list(x) }

    @processes = @product_processes.map { |x| x.name }
    @category = ProductProcess::CATEGORY

    @steps = @process_map.map { |x| x[:steps] }.flatten
    @counts = {
      total: @steps.count,
      reviewed: @steps.select { |x| x[:reviewed] }.count,
      pending: @steps.reject { |x| x[:reviewed] }.count,
      critical: @steps.select { |x| x[:criticity] == 'Critical' }.count,
      major: @steps.select { |x| x[:criticity] == 'Major' }.count,
      minor: @steps.select { |x| x[:criticity] == 'Minor' }.count
    }
  end

  def cp_new_step
    images = {}
    if params[:edit]
      ps = ProcessStep.find(params[:step_id])
      pr = ps.product_process
      pf = pr.product_family
      sup = pf.supplier_profile
      product_processes = pf.product_processes
      images[ps.id] = {
        acceptable_image: ps.acceptable_image_url,
        unacceptable_image: ps.unacceptable_image_url,
        requirement_images: ps.requirement_images_urls
      }
    else
      pf = ProductFamily.find(params[:product_family_id])
      sup = pf.supplier_profile
      product_processes = pf.product_processes
      ps = ProcessStep.new
      images[0] = {
        acceptable_image: nil,
        unacceptable_image: nil,
        requirement_images: []
      }
    end
    pp = {}
    ProductProcess::CATEGORY.each do |x|
      pp[x] = []
    end

    pp.merge!(product_processes.group_by do |x|
                x.category.to_s
              end.reject { |k, _v| !ProductProcess::CATEGORY.include?(k) }) # ProductProcess.find(params[:process_id])
    all_process_steps = ProcessStep.where(product_process_id: product_processes.map { |x| x.id }, destroyed_at: nil)
    processSteps = all_process_steps.map do |x|
                     { pid: x.product_process_id, step_number: x.step_number, control_item: x.control_item }
                   end.uniq.group_by { |x| x[:pid] }
    processSteps.each do |k, v|
      processSteps[k] = {
        items: v,
        names: v.map { |x| "#{x[:step_number]} - #{x[:control_item]}" }
      }
    end

    ppc = ProductProcess::CATEGORY
    psc = ProcessStep::CONTROL_TYPES
    new_step_hash = {
      header: {
        bgColor: 'blue'
      },
      supplier: {
        id: sup.id,
        name: sup.name
      },
      images: images,
      productFamily: {
        id: pf.id,
        name: pf.name
      },
      productProcess: pp,
      processStep: ps,
      product_process_types: ppc,
      process_step_control_types: psc,
      processSteps: processSteps
    }
    @pf = pf
    @pp = ProductProcess.new(product_family_id: pf.id)
    @ps = ps
    if params[:edit]
      pi = ppc.index(pr.category)
      ap = pp[pr.category]
      ai = ap.map { |x| "#{x.name} #{x.number}" }.index("#{pr.name} #{pr.number}")

      new_step_hash[:process_index] = pi
      new_step_hash[:active_processes] = ap
      new_step_hash[:active_processes_names] = ap.map { |x| x.name }
      new_step_hash[:active_processes_numbers] = ap.map { |x| x.number }
      new_step_hash[:active_process_index] = ai
      new_step_hash[:step_index] = psc.index(ps.control_type)
      new_step_hash[:active_step_index] =
        processSteps[pp[ppc[pi]][ai][:id]][:names].index("#{ps.step_number} - #{ps.control_item}")
    end

    @h = new_step_hash
    # new_process_button and new_step_button to be updated with tl
  end

  def cp_report
    @cp = ControlPlan.find(params[:cp_id])
  end

  def cap_index
    # model=quality_assessment&id=365(&followup=true)
    if params[:model] == 'quality_assessment'
      ref = QualityAssessment.find_by(id: params[:qa_id])
    elsif params[:model] == 'control_plan'
      ref = ControlPlan.find_by(id: params[:qa_id])
    end
    @qa = ref
    cap = ref.cap
    cap_points = cap.cap_points.includes(:cap_followup, :cap_resolutions).order(:id)
    points = cap_points.map do |x|
      h = x.serializable_hash
      h.symbolize_keys!
      h[:resolutions] = x.cap_resolutions
      h[:followup] = x.cap_followup
      h
    end
    can_schedule = false
    if params[:followup]
      resolved = cap_points.select { |x| x.cap_followup.present? && x.cap_followup.resolved }
      perc = resolved.count * 100 / cap_points.length
    else
      resolved = cap_points.select { |x| x.cap_resolutions.present? }
      perc = resolved.count * 100 / cap_points.length
      can_schedule = true if perc == 100
    end

    @h = { points: points, cap: cap.basic_h, perc: perc, can_schedule: can_schedule }
  end

  def cap_form
    @cap_index = params[:index].to_i || 0

    if params[:model] == 'quality_assessment'
      @ref = QualityAssessment.find_by(id: params[:id])
    elsif params[:model] == 'control_plan'
      @ref = ControlPlan.find_by(id: params[:id])
    end
    @qa = @ref
    @supplier = @ref.supplier_profile
    @points = @ref.cap_points.map { |x| x.standard_hash }
    @active_index = params[:index].to_i || 0
    @active_point = @points[@active_index]
  end

  def cap_followup
    @cap_index = params[:index].to_i || 0

    if params[:model] == 'quality_assessment'
      @ref = QualityAssessment.find_by(id: params[:id])
    elsif params[:model] == 'control_plan'
      @ref = ControlPlan.find_by(id: params[:id])
    end
    @qa = @ref
    @supplier = @ref.supplier_profile
    @points = @ref.cap_points.map { |x| x.standard_hash }
    @active_index = params[:index].to_i || 0
    @active_point = @points[@active_index]
    @cap_followup = @ref.cap_points[params[:index].to_i].cap_followup
  end

  def cap_followup_invite
    @qa = QualityAssessment.find(params[:qa_id])
    @cap = @qa.cap
  end

  def cap_confirmation; end

  def done; end

  def aa_application
    @grids = %w[QA CP OPEX SSE HRP SCM ENV PUR DPR]
    @opm_source = AssessorProfile.source_list
    @referent_source = AssessorProfile.referent_list
    @app = if params[:id] && params[:key].blank?
             AssessorApplication.find_by(id: params[:id]) || AssessorApplication.new
           else
             AssessorApplication.new
           end
  end

  def aa_application_selection
    @qas = @ass_app.user.quality_assessments
    @cards = @qas.map do |x|
               PageComponent.history_card(x.goes_activities.last, current_user.id)
             end.reject { |x| x.blank? }
  end

  def aa_application_status
    @applicant = AssessorProfile.find_by(user_id: params[:user_id])
    @app = AssessorApplication.where(user_id: params[:user_id]).order(:id).last
    @steps = @app.to_h(current_user)[:steps]
  end

  def aa_qualification
    @app = AssessorApplication.find_by(id: params[:id])
    role = params[:role] == 'opm' ? 'OPM' : 'Team Leader'
    @header = "#{role} Approval"
    @user = @app.user.assessor_profile
    @h = @app.to_h(current_user)
  end

  def aa_training_time
    @app = AssessorApplication.find_by(id: params[:id])
  end

  def aa_technical_eval
    # @app = AssessorApplication.find_by(id: params[:id])
  end

  def aa_validator
    @validation = Validation.find(params[:id])
    @ass_app = @validation.assessor_application
    @ass = @validation.assessment
    @h = { card: PageComponent.history_card(@ass.goes_activities.last, current_user.id) }
    @h.merge!({ application: @ass_app.to_h })
    @h.merge!({ validation: @validation.serial_hash })
  end

  def permission_scope
    true
  end
end
