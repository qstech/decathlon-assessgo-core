class BaseController < ApplicationController
  # include WebBugReporter
  around_action :set_locale

  def set_locale(&action)
    locale = params[:locale]
    locale = :en if locale.blank?
    I18n.with_locale(locale, &action)
  end
end
