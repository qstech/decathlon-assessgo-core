require 'iso_country_codes'
class StatisticsController < ApplicationController
  before_action :set_common_params,
                only: %i[country industrial_process result rank supplier supplier_qa supplier_history multiple_stats
                         assessment]
  before_action :set_available_selections,
                only: %i[country industrial_process result rank supplier supplier_qa supplier_history supplier_full multiple_stats
                         assessment]
  before_action :set_highcharts

  PIE_OPTS = { plot_options: { dataLabels: { enabled: true,
                                             format: '<b>{point.name}</b><br>{point.percentage:.1f} %' } } }

  OUTCOME = [
    { value: 'assessment', label: 'Statistic for Assessments' },
    { value: 'supplier', label: 'Statistic for Suppliers' },
    { value: 'assessor', label: 'Statistic for Assessors' },
    { value: 'control_plan', label: 'Statistic for Control Plans' }
  ]

  def statistics
    @active = 'statistics'
  end

  def excel_download
    @suppliers = SupplierProfile.all
  end

  def multiple_stats
    # @stats_outcome_choices = MULTI_STATS
    @stats_outcome_choices = OUTCOME
    stats_outcome = params[:stats_outcome] || nil
    puts "stats_outcome ===>>> #{stats_outcome}"
    # puts "params ===>> #{params.to_h}"
    # params = params.reject{|k,v| k == 'controller' || k == 'action'}

    if stats_outcome.present?
      if stats_outcome == 'all'
        render plain: 'Must choose outcome!'
      elsif stats_outcome == 'assessment'
        res = multi_stats_assessment(params)

      elsif stats_outcome == 'control_plan'
        res = multi_stats_control_plan(params)

      elsif stats_outcome == 'supplier'
        res = multi_stats_supplier(params)

      elsif stats_outcome == 'assessor'
        res = multi_stats_assessor(params)
      end

      puts "res ====>>>> #{res}"

      @group_by = []
      if res[:group_by].present?
        res[:group_by].each do |k, v|
          opts = PIE_OPTS
          opts = { x_title: k.to_s.humanize.to_s, x_labels: v[:data].map { |k, _v| k } } unless v[:chart_type] == :pie
          @group_by << create_plot(v[:data].to_a, v[:chart_type], "Grouped by #{k.to_s.humanize}", opts)
        end
      end

      @time_series = []
      puts ":time_series ====>>>>> #{res[:time_series]}"
      if res[:time_series].present?
        res[:time_series].each do |k, v|
          opts = PIE_OPTS
          opts = { x_title: k.to_s.humanize.to_s, x_labels: v[:data].map { |k, _v| k } } unless v[:chart_type] == :pie

          opts.merge!({ stacked: v[:stacked] }) if v[:stacked].present?

          plot = create_plot(v[:data].to_a, v[:chart_type], "#{k.to_s.humanize} Trend", opts)

          @time_series << plot
        end
        # create_plot(res[:time_series].to_a, :spline, "Assessment Count Over Time", {x_title: "Assessment Month", x_labels: res[:time_series].map{|k,v| k}}) if res[:time_series].present?
      end
      # puts "@time_series ==>> #{@time_series}"

      @top_ten = []
      if res[:top_ten].present?
        res[:top_ten].each do |k, v|
          opts = PIE_OPTS
          opts = { x_title: k.to_s.humanize.to_s, x_labels: v[:data].map { |k, _v| k } } unless v[:chart_type] == :pie
          @top_ten << create_plot(v[:data].to_a, v[:chart_type], "Top 10 #{k.to_s.humanize}", opts)
        end
      end
      # puts "@top_ten ==>> #{@top_ten}"
    end
  end

  def country
    @graphs = []

    cqa = country_qa(start_date: @start_date, end_date: @end_date, grids: @grids,
                     assess_types: @assess_types, completed: true, countries: @countries,
                     processes: @processes, prep_completed: @prep_completed, cap_finished: @cap_finished,
                     completed2: @completed2, title: 'Completed Assessments by Country')
    puts "cqa result ==>> #{cqa}"

    iqa = country_qa(start_date: @start_date, end_date: @end_date, grids: @grids,
                     assess_types: @assess_types, completed: false, countries: @countries,
                     processes: @processes, prep_completed: @prep_completed, cap_finished: @cap_finished,
                     completed2: @completed2, title: 'Open Assessments by Country')
    puts "iqa result ==>> #{iqa}"

    [cqa, iqa].each do |x|
      @graphs << {
        graph: Daru::View::Plot.new(x[:data], x[:opts]),
        data: x[:result].map(&:values),
        totals: [x[:result].count, x[:result].map { |x| x['count_assessments'].to_i }.sum, x[:result].map do |x|
                                                                                             x['count_supplier'].to_i
                                                                                           end.sum, x[:result].map do |x|
                                                                                                      x['count_plans'].to_i
                                                                                                    end.sum]
      }
    end
  end

  def industrial_process
    puts "params ==>> #{params}"
    base_params = {
      start_date: @start_date, end_date: @end_date, grids: @grids,
      assess_types: @assess_types, countries: @countries,
      prep_completed: @prep_completed, cap_finished: @cap_finished,
      completed2: @completed2, processes: @processes
    }

    c_result = process_query(base_params.merge({ completed: true }))
    @complete = Daru::View::Plot.new(c_result[:data], c_result[:opts])
    @complete_data = c_result[:result]

    i_result = process_query(base_params.merge({ completed: false }))
    @incomplete = Daru::View::Plot.new(i_result[:data], i_result[:opts])
    @incomplete_data = i_result[:result]
  end

  def result
    puts "@grids => #{@grids}"
    @qa_present = @grids.reject { |x| x == 'control_plan' }
    @cp_present = @grids.select { |x| x == 'control_plan' }

    results = %w[E EP D C B A]

    data_raw = QualityAssessment.joins(:supplier_profile, :assessment_day_form)
                                .where(destroyed_at: nil, completed: true, created_at: Time.parse(@start_date)..Time.parse(@end_date))
                                .where.not(result: nil)
    data_raw = data_raw.where(grid: @grids) if @grids.present?
    data_raw = data_raw.where(assess_type: @assess_types) if @assess_types.present?
    data_raw = data_raw.where(supplier_profiles: { country: @countries }) if @countries.present?
    data_raw = data_raw.where(supplier_profiles: { industrial_process: @processes }) if @processes.present?
    unless @prep_completed.nil? || @prep_completed == 'all'
      data_raw = data_raw.where(prep_completed: (@prep_completed == 'true'))
    end

    if @cap_finished.present? && @cap_finished != 'all'
      @cap_filter = @cap_finished
      data_raw = data_raw.where(id: calc_cap_interval)
    end

    data_raw = data_raw.where(id: calc_completed_interval) if @completed2.present? && @completed2 != 'all'

    data = data_raw.group(:result).count
    @graph = create_plot(data.to_a, :pie, 'Assessment Results', PIE_OPTS)

    @data = {}
    results.each { |x| @data[x] = data[x] || 0 }
    puts "DATA.....===>>> #{@data}"

    @total = @data.map { |_k, v| v }.sum

    # ABC Rate
    abc_grouping = "CASE WHEN quality_assessments.result in ('A', 'B', 'C') THEN 'ABC' ELSE 'Others' END"
    abc_data = data_raw.group(abc_grouping).count
    puts "ABC rate data.. ==>> #{abc_data}"

    @abc_graph = create_plot(abc_data.to_a, :pie, 'Result is A, B, or C', PIE_OPTS)

    # Target Reached Rate
    # Add => Target Rate [Rate of results that reached Target] to (Results) {select target: C,B,A}
    target_grouping = "CASE WHEN assessment_day_forms.supplier_target >= quality_assessments.result THEN 'Target reached' ELSE 'Target not reached' END"
    target_data = data_raw.group(target_grouping).count

    @target_graph = create_plot(target_data.to_a, :pie, 'Whether Target is Achieved', PIE_OPTS)

    if @grids.present?
      # Count D/E/EPs

      qa_ids = data_raw.map(&:id)
      @d_graph = level_counts(qa_ids, 'D', PIE_OPTS)
      @e_graph = level_counts(qa_ids, 'E', PIE_OPTS)
    end

    # ============ CONTROL PLAN ============ #
    if @grids.blank? || @cp_present.present?
      result_cp = %w[0-20 21-70 70-100]

      cp_base = ControlPlan.includes(plan_items: [:cap_points, { process_step: :cp_pending_frequencies }],
                                     product_family: :supplier_profile)
      data_cp = cp_base.where(destroyed_at: nil, completed: true,
                              created_at: Time.parse(@start_date)..Time.parse(@end_date))

      # country filter
      data_cp = data_cp.where(supplier_profiles: { country: @countries }) if @countries.present?
      data_cp = data_cp.where(supplier_profiles: { industrial_process: @processes }) if @processes.present?

      # plan type filter
      plan_types = @assess_types.select { |x| @plan_types_filter.include?(x) }
      data_cp = data_cp.where(plan_type: plan_types) if @cp_present.present? && plan_types.present?

      data_cp = data_cp.to_a

      # Nonconform
      data_cp_ncr = cp_rate_group_by(data_cp.map do |x|
        x.nonconformity_rate
      end).map { |k, v| [k, v.length] }.to_h
      # for table
      @data_cp_ncr = {}
      result_cp.each { |x| @data_cp_ncr[x] = data_cp_ncr.keys.include?(x) ? data_cp_ncr[x] : 0 }
      @graph_cp_ncr = create_plot(data_cp_ncr.to_a, :pie, 'Control Plan Nonconformity Rate', PIE_OPTS)
      @total_cp = data_cp_ncr.map { |_k, v| v }.sum

      # Freq resp. rate
      data_cp_freq = cp_rate_group_by(data_cp.map do |x|
        x.freq_respect_rate
      end).map { |k, v| [k, v.length] }.to_h
      @graph_cp_freq = create_plot(data_cp_freq.to_a, :pie, 'Control Plan Freq. Respected Rate', PIE_OPTS)

      # CAP Close rate
      data_cp_cap = cp_rate_group_by(data_cp.map do |x|
        x.cap_close_rate
      end).map { |k, v| [k, v.length] }.to_h
      @graph_cp_cap = create_plot(data_cp_cap.to_a, :pie, 'Control Plan Cap Close Rate', PIE_OPTS)
    end
  end

  def rank
    @roles = ['Decathlonian', 'Assessor Candidate', 'Assessor', 'Referent', 'Supplier']
    options = { start_date: @start_date, end_date: @end_date, grids: @grids,
                assess_types: @assess_types, countries: @countries, prep_completed: @prep_completed,
                cap_finished: @cap_finished, completed2: @completed2 }
    options[:role] = params[:role] if params[:role].present?
    complete = rank_query(options.merge!({ completed: true }))
    @complete = Daru::View::Plot.new(complete[:data], complete[:opts])

    @complete_data = complete[:result]
    puts "@complete_data ===>>>> #{@complete_data}"
    # @complete_data ===>>>> {"TEST USER"=>5, "Bruce GAO"=>2, "Kerry Kang"=>1}

    incomplete = rank_query(options.merge!({ completed: false }))
    @incomplete = Daru::View::Plot.new(incomplete[:data], incomplete[:opts])

    @incomplete_data = incomplete[:result]

    @complete_count = @complete_data.count
    @complete_sum = @complete_data.map { |_k, v| v.to_i }.sum
    @complete_cpsum = @complete_data.map { |_k, _v, x| x.to_i }.sum

    @incomplete_count = @incomplete_data.count
    @incomplete_sum = @incomplete_data.map { |_k, v| v.to_i }.sum
    @incomplete_cpsum = @incomplete_data.map { |_k, _v, x| x.to_i }.sum
  end

  def comment
    @graphs = []
    opts = { followers: { titles: ['Following Counts Rank', 'Following'] } }
    %i[comments likes favorites followers].each do |x|
      h = performance_query(x, opts[x])
      @graphs << {
        graph: Daru::View::Plot.new(h[:data], h[:opts]),
        data: h[:result].to_h,
        totals: [h[:result].count, h[:result].map(&:last).sum]
      }
    end
  end

  def supplier_search
    @suppliers = SupplierProfile.all
    puts "params...... #{params}"
    supplier = params[:suppliers]
    redirect_to "/statistics/supplier/#{supplier.join('+')}" if supplier.present?
  end

  def supplier
    @suppliers = SupplierProfile.all
    @suppliers_two = SupplierProfile.all.map(&:name)

    puts "params..... #{params}"
    set_supplier_options!

    puts "options...... #{@options}"
    @data_sets = []
    qa_present = params[:grids].blank? || params[:grids].reject { |x| x == 'control_plan' }.present?
    cp_present = params[:grids].blank? || params[:grids].select { |x| x == 'control_plan' }.present?

    if qa_present
      res = supplier_query(@options.merge({ completed: true }))
      @res = res[:data].values
      @qa_bar = supplier_bar(@options.merge({ completed: true }), res)

      @data_sets << {
        res: @res,
        title: 'Completed Assessments',
        total_suppliers: @res.map { |x| x[0] }.uniq.count,
        total_processess: @res.map { |x| x[6] }.uniq.count,
        bar: @qa_bar
      }

      res_open = supplier_query(@options.merge({ completed: false }))
      @res_open = res_open[:data].values
      @qa_bar_open = supplier_bar(@options.merge({ completed: false }), res_open)

      @data_sets << {
        res: @res_open,
        title: 'Open Assessments',
        total_suppliers: @res_open.map { |x| x[0] }.uniq.count,
        total_processess: @res_open.map { |x| x[6] }.uniq.count,
        bar: @qa_bar_open
      }
    end

    # # CONTROL PLAN
    @options.merge!({ start_date: @start_date, end_date: @end_date, plan_type: @plan_type })

    if cp_present
      @res_cp = supplier_cp_query(@options.merge({ completed: true })).values.group_by do |x|
                  x[0]
                end.map { |_k, v| v.first }
      @cp_bar = supplier_cp_bar(@options.merge({ completed: true }))

      @data_sets << {
        res: @res_cp,
        title: 'Completed Control Plans',
        total_suppliers: @res_cp.map { |x| x[0] }.uniq.count,
        total_processess: @res_cp.map { |x| x[5] }.uniq.count,
        bar: @cp_bar
      }

      @res_cp_open = supplier_cp_query(@options.merge({ completed: false })).values.group_by do |x|
                       x[0]
                     end.map { |_k, v| v.first }
      @cp_bar_open = supplier_cp_bar(@options.merge({ completed: false }))

      @data_sets << {
        res: @res_cp_open,
        title: 'Open Control Plans',
        total_suppliers: @res_cp_open.map { |x| x[0] }.uniq.count,
        total_processess: @res_cp_open.map { |x| x[5] }.uniq.count,
        bar: @cp_bar_open
      }
    end
  end

  def supplier_full
    # HRP STATS:  (# of Supplier Sites), supplier code, name, city, opm, category,
    #             ptm, pl, hrp target, last result, on target (did last result reach target?),
    #             last assess date, next assessed date (red: overdue, yellow: 3-months to overdue),
    #             assessed on time (OK, NOK, N/A, Late), CAP Uploaded (Y/N), ABC Rate, Target Rate
    #
    #
    # MISSING:  assessed on time(OK, NOK, N/A, Late), next assessment date(red: overdue, yellow: 3-months to overdue)

    # OPM PTM PL
    # ==>  just take opm_set, ptm_set, pl_set
    # and for each array of id's, AssessorProfile's name for that id.

    query_string = <<-SQL
    SELECT --combined.*,
      combined.code, combined.name, combined.city, combined.country, combined.sites,
      combined.assess_date, combined.grid, combined.result, combined.target,
      combined.prev_assess_date, combined.prev_result, combined.prev_target,
      combined.cap_review_created,

      CASE WHEN target IS NULL THEN 'No target set'
    WHEN target >= result THEN 'Yes' ELSE 'No' END as target_reached,
      'Need audit rule' as assessed_on_time,
      'Need audit rule' as next_assess_date,
      CASE WHEN abc.id IS NULL THEN 0.0 ELSE (CAST(abc.abc as FLOAT) / CAST(abc.total as FLOAT)) END as abc_rate,
      CASE WHEN t.id IS NULL THEN 0.0 ELSE (CAST(t.count_t as FLOAT) / CAST(t.total as FLOAT)) END as target_reached_rate,
      combined.pl, combined.opm, combined.ptm

    FROM
    -- start of combined sub-query
    (
      SELECT s.id, s.code, s.name, s.city, s.country, s.sites,
      qa.assess_date, qa.grid, qa.result, qa.supplier_target target,
      prev_qa.assess_date prev_assess_date, prev_qa.result prev_result, prev_qa.supplier_target prev_target,
      CASE WHEN cap.id IS NULL THEN 'No' ELSE 'Yes' END cap_review_created,
      pl.pl, opm.opm, ptm.ptm

      FROM supplier_profiles s
      JOIN
      (SELECT s.id supp_id,
       qa.id, qa.assess_date, qa.result, a.supplier_target, qa.grid,
       ROW_NUMBER() OVER(PARTITION BY s.id ORDER BY qa.assess_date DESC)
       FROM quality_assessments qa
       JOIN supplier_profiles s on s.id = qa.supplier_profile_id
       LEFT JOIN assessment_day_forms a ON a.quality_assessment_id = qa.id
       WHERE qa.completed = TRUE AND qa.result IS NOT NULL
       AND qa.destroyed_at IS NULL
       AND qa.assess_type = 'Annual'
       AND qa.grid IN (?)
       ) qa ON qa.supp_id = s.id AND qa.row_number = 1

      LEFT JOIN
      (SELECT s.id supp_id,
       qa.id, qa.assess_date, qa.result, a.supplier_target, qa.grid,
       ROW_NUMBER() OVER(PARTITION BY s.id ORDER BY qa.assess_date DESC)
       FROM quality_assessments qa
       JOIN supplier_profiles s on s.id = qa.supplier_profile_id
       LEFT JOIN assessment_day_forms a ON a.quality_assessment_id = qa.id
       WHERE qa.completed = TRUE AND qa.result IS NOT NULL
       AND qa.destroyed_at IS NULL
       AND qa.assess_type = 'Annual'
       AND qa.grid IN (?)
       ) prev_qa ON prev_qa.supp_id = s.id AND prev_qa.row_number = 2

      LEFT JOIN
      (SELECT qa.id, COUNT(c.id) count_cap
       FROM quality_assessments qa JOIN caps c ON qa.id = c.ref_id AND c.ref_model = 'quality_assessment'
       WHERE c.cap_type = 'CAP Review' and c.destroyed_at IS NULL
       AND qa.completed = TRUE AND qa.result IS NOT NULL
       AND qa.destroyed_at IS NULL
       AND qa.assess_type = 'Annual'
       AND qa.grid IN (?)
       GROUP BY qa.id
       ) cap ON cap.id = qa.id

      LEFT JOIN
      (SELECT pl.supp_id, string_agg(DISTINCT ap.name, ', ') as pl
       FROM assessor_profiles ap
       JOIN (SELECT sp.id supp_id, pl_id
             FROM supplier_profiles sp, unnest(sp.pl_set) pl_id
             WHERE pl_set <> '{}'
             AND pl_id <> ''
             ORDER BY 1) as pl ON cast(pl.pl_id as integer) = ap.id
       group by 1) pl ON pl.supp_id = s.id

      LEFT JOIN
      (SELECT opm.supp_id, string_agg(DISTINCT ap.name, ', ') as opm
       FROM assessor_profiles ap
       JOIN (SELECT sp.id supp_id, opm_id
             FROM supplier_profiles sp, unnest(sp.opm_set) opm_id
             WHERE opm_set <> '{}'
             AND opm_id <> ''
             ORDER BY 1) as opm ON cast(opm.opm_id as integer) = ap.id
       group by 1) opm ON opm.supp_id = s.id

      LEFT JOIN
      (SELECT ptm.supp_id, string_agg(DISTINCT ap.name, ', ') as ptm
       FROM assessor_profiles ap
       JOIN (SELECT sp.id supp_id, ptm_id
             FROM supplier_profiles sp, unnest(sp.ptm_set) ptm_id
             WHERE ptm_set <> '{}'
             AND ptm_id <> ''
             ORDER BY 1) as ptm ON cast(ptm.ptm_id as integer) = ap.id
       group by 1) ptm ON ptm.supp_id = s.id
    ) combined
    -- end of combined sub-query

    -- start of abc rate sub-query
    LEFT JOIN
    (SELECT s.id, abc.count_assessment abc, COUNT(qa.id) total
     FROM quality_assessments qa
     JOIN supplier_profiles s on s.id = qa.supplier_profile_id
     JOIN (SELECT s.id, COUNT(*) count_assessment
           FROM quality_assessments qa
           JOIN supplier_profiles s on s.id = qa.supplier_profile_id
           WHERE qa.completed = TRUE AND qa.result IS NOT NULL
           AND qa.destroyed_at IS NULL
           AND qa.assess_type = 'Annual'
           AND qa.grid IN (?)
           AND qa.result IN ('A', 'B', 'C')
           GROUP BY s.id) abc ON abc.id = s.id
     WHERE qa.completed = TRUE AND qa.result IS NOT NULL
     GROUP BY s.id, abc.count_assessment
     ) abc ON abc.id = combined.id
    -- end of abc rate sub-query

    -- start of target-reached rate sub-query
    LEFT JOIN
    (SELECT s.id, t.count_assessment count_t, COUNT(qa.id) total
     FROM quality_assessments qa
     JOIN supplier_profiles s on s.id = qa.supplier_profile_id
     JOIN (SELECT s.id, COUNT(*) count_assessment
           FROM quality_assessments qa
           JOIN supplier_profiles s on s.id = qa.supplier_profile_id
           JOIN assessment_day_forms a on a.quality_assessment_id = qa.id
           WHERE qa.completed = TRUE AND qa.result IS NOT NULL
           AND qa.destroyed_at IS NULL
           AND qa.assess_type = 'Annual'
           AND qa.grid IN (?)
           AND a.supplier_target >= qa.result
           GROUP BY s.id) t ON t.id = s.id
     WHERE qa.completed = TRUE AND qa.result IS NOT NULL
     GROUP BY s.id, t.count_assessment
     ) t ON t.id = combined.id
    -- end of target-reached rate sub-query
    SQL

    @grids = params[:grids]
    @grids = QualityAssessment.all.map { |x| x.grid }.uniq unless params[:grids].present?

    query = [query_string]
    # There are 5 ? variables in the SQL query
    5.times { query << @grids }

    result = exec_query(query)

    @headers = result.columns
    @rows = result.rows
  end

  def supplier_history
    @supplier_codes = params[:suppliers]
    @qas = QualityAssessment.joins(:supplier_profile).where(supplier_profiles: { code: @supplier_codes },
                                                            destroyed_at: nil, assess_date: Time.parse(@start_date)..Time.parse(@end_date))
    # .select("quality_assessments.id, supplier_profiles.code, supplier_profiles.name, quality_assessments.grid, quality_assessments.assess_type, quality_assessments.assess_date, quality_assessments.completed, quality_assessments.result")
    @cps = ControlPlan.joins(product_family: :supplier_profile).where(supplier_profiles: { code: @supplier_codes },
                                                                      destroyed_at: nil, assess_date: Time.parse(@start_date)..Time.parse(@end_date))
    # .select("control_plans.id, supplier_profiles.code, supplier_profiles.name, 'control_plan', control_plans.plan_type, control_plans.assess_date, control_plans.completed, control_plans.nonconformity_rate")

    params[:completed] = nil if params[:completed] == 'all'
    @completed = (params[:completed] == 'true') if params[:completed].present?

    puts "filtering @completed ==> #{@completed}"
    @qas = @qas.where(completed: @completed) if params[:completed].present?
    @cps = @cps.where(completed: @completed) if params[:completed].present?

    @qas = @qas.where(grid: @grids) if @grids.present?
    @cps = [] if @grids.present? && @grids.exclude?('control_plan')

    @qas = @qas.where(assess_type: @assess_types) if @assess_types.present?
    @cps = @cps.where(plan_type: @assess_types) if @cps.present? && @assess_types.present?

    @qa = @qas.group(:grid).count.merge({ 'control_plan' => @cps.count })

    @qas = @qas.order('quality_assessments.assess_date DESC, quality_assessments.grid ASC') if @qas.present?
    @cps = @cps.order('control_plans.assess_date DESC') if @cps.present?
    @assessments = @qas + @cps
    # raise

    qa_labels = @qa.map { |k, _v| QualityAssessment::SHORT_NAMES[k] || 'CP' }

    qa_data = [{
      name: 'Assessments',
      data: @qa.map { |_k, v| v }
    }]

    qa_opts = {
      chart: {
        type: :bar,
        styledMode: true
      },
      title: { text: 'Suppliers Assessments by Grid' },
      yAxis: { tickInterval: 1 },
      xAxis: { title: { text: 'Grid' }, categories: qa_labels }
    }
    @qa_bar = Daru::View::Plot.new(qa_data, qa_opts)
  end

  def supplier_qa
    puts "params... #{params}"
    puts "current_user...... #{current_user}"
    assess_type = params[:assess_type] || 'all'
    completed = params[:completed] || 'all'

    # @current_user_qas = current_user.quality_assessments.map{|x| x.id}
    # @current_user_cps = current_user.control_plans.map{|x| x.id}
    @suppliers = SupplierProfile.where(code: params[:supplier_code].split('+'))
    @charts = []

    @suppliers.each do |supplier|
      qas = supplier.quality_assessments.where(destroyed_at: nil,
                                               assess_date: Time.parse(@start_date)..Time.parse(@end_date))
                    .order(assess_date: :desc, grid: :asc)

      # @qa FOR QA BAR CHART
      qa = supplier.quality_assessments.where(destroyed_at: nil,
                                              assess_date: Time.parse(@start_date)..Time.parse(@end_date))

      if assess_type != 'all'
        qas = qas.where(assess_type: assess_type)
        qa = qa.where(assess_type: assess_type)
      end

      qas = qas.where(grid: @grid) if @grid != 'all'

      if completed != 'all'
        status = completed == 'completed'
        qas = qas.where(completed: status)
        qa = qa.where(completed: status)
      end

      # FOR QA BAR CHART
      qa = qa.group('quality_assessments.grid').count

      qa_labels = qa.map { |k, _v| QualityAssessment::SHORT_NAMES[k] }

      qa_data = [{
        name: 'Assessments',
        data: qa.map { |_k, v| v }
      }]

      qa_opts = {
        chart: {
          type: :bar,
          styledMode: true
        },
        title: { text: 'Supplier Assessments by Grid' },
        yAxis: { tickInterval: 1 },
        xAxis: { title: { text: 'Grid' }, categories: qa_labels }
      }
      qa_bar = Daru::View::Plot.new(qa_data, qa_opts)
      # raise
      # FOR CP
      plan_type = params[:plan_type] || 'all'
      cps = supplier.control_plans.where(destroyed_at: nil,
                                         assess_date: Time.parse(@start_date)..Time.parse(@end_date))
                    .order(assess_date: :desc)
      cp = supplier.control_plans.where(destroyed_at: nil,
                                        assess_date: Time.parse(@start_date)..Time.parse(@end_date))

      if completed != 'all'
        status = completed == 'completed'
        cps = cps.where(completed: status)
        cp = cp.where(completed: status)
      end

      cp = cp.group('control_plans.plan_type').count

      cp_labels = cp.map { |k, _v| k }

      cp_data = [{
        name: 'Control Plans',
        data: cp.map { |_k, v| v }
      }]

      cp_opts = {
        chart: {
          type: :bar,
          styledMode: true
        },
        title: { text: 'Control Plans Count by Type' },
        yAxis: { tickInterval: 1 },
        xAxis: { title: { text: 'Plan Type' }, categories: cp_labels }
      }
      cp_bar = Daru::View::Plot.new(cp_data, cp_opts)

      # form a chart
      # line_opts = {
      #   chart: {
      #     type: :line,
      #     styledMode: true
      #   },
      #   title: {text: "Line Chart for the year ending #{DateTime.now.end_of_month.strftime('%b %d, %Y')}"},
      #   xAxis: {
      #     categories: set_categories
      #   },
      #   legend: {
      #     layout: 'vertical',
      #     align: 'right',
      #     verticalAlign: 'middle'
      #   },

      # }

      # line_chart = Daru::View::Plot.new(line_chart_data(supplier.id), line_opts)
      # @charts << {supplier: supplier, qas: qas, qa_bar: qa_bar, cps: cps, cp_bar: cp_bar, line: line_chart}
      @charts << { supplier: supplier, qas: qas, qa_bar: qa_bar, cps: cps, cp_bar: cp_bar }
    end
  end

  def line_chart_data(id = nil)
    to = DateTime.now.end_of_month
    from = to - 1.year

    m = DateTime.now.month
    months = (1..12).to_a
    cats = (months.slice(m, months.length - m + 1) + months.slice(0, m))

    data = []
    # NCR
    # Plan Items for the past year
    pls = PlanItem.includes(control_plan: :product_family).where(product_families: { destroyed_at: nil }).where(control_plans: {
                                                                                                                  completed: true, assess_date: from..to
                                                                                                                })

    pls = pls.where(product_families: { supplier_profile_id: id }) if id.present?
    # Group by month
    grouped_pls = pls.group_by { |x| x.control_plan.assess_date.month }
    h = {}
    grouped_pls.each do |k, v|
      h[k] = v.count { |n| n.result == 'NOK' }.to_f * 100 / v.count do |e|
                                                              e.result.in? %w[OK NOK]
                                                            end
    end

    ncr_data = cats.map { |x| x.in?(h.keys) ? h[x] : 0 }

    data << { name: "NCR #{(ncr_data.sum / ncr_data.size).to_i} \% yearly", data: ncr_data }

    # CAP CLOSE RATE
    # resolved caps / all caps

    # Every supplier has many product product_families. Each product family has its own CCR (resolved caps / all caps). We need to calculate CCR for each product family per month
    caps = CapPoint.includes(:cap).where(caps: { ref_model: 'control_plan', created_at: from..to }) # querying and grouping done by cap.created_at

    # returns 100 for product_family with no control plans
    # count product families with no CP * 100

    # count others

    if id.present?
      # id is supplier_profile.id
      # find all the control_plans with supplier
      cps = ControlPlan.includes(:product_family).where(product_families: { supplier_profile_id: id }).pluck(:id)
      caps = caps.where(caps: { ref_id: cps })
    end

    grouped_caps = caps.group_by { |x| x.cap.created_at.month }
    h = {}
    grouped_caps.each { |k, v| h[k] = v.present? ? v.count { |n| n.resolved_at.present? }.to_f * 100 / v.count : 0 }

    cap_close_data = cats.map { |x| x.in?(h.keys) ? h[x] : 100 }
    data << { name: 'CAP Close rate', data: cap_close_data }

    data
  end

  def set_categories
    m = DateTime.now.month
    months = (1..12).to_a
    cats = (months.slice(m, months.length - m + 1) + months.slice(0, m))
    cats.map do |x|
      "#{Date::MONTHNAMES[x]} \'#{x - m > 0 ? (DateTime.now.year - 1).to_s.last(2) : DateTime.now.year.to_s.last(2)}"
    end
  end

  def xuwei_stats
    users = User.includes(:assessments, :cps, :assessor_profile, :supplier_profile, :supplier).where(destroyed_at: nil)
    h = Hash.new do |hash, key|
      hash[key] = {
        total_users: 0,
        total_dkn: 0,
        total_suppliers: 0,
        active_dkn: 0,
        active_suppliers: 0
      }
    end
    users.each do |user|
      prof = user.profile
      next if prof.nil?

      if params[:date].present?
        d = Time.parse(params[:date])
        next if user.created_at > d
      end

      country = prof.country_name

      h[country][:total_users] += 1
      if prof.is_a?(AssessorProfile)
        h[country][:total_dkn] += 1
        h[country][:active_dkn] += 1 if user.active?(params[:date])
      elsif prof.is_a?(SupplierProfile)
        h[country][:total_suppliers] += 1
        h[country][:active_suppliers] += 1 if user.active?(params[:date])
      end
    end

    h = h.sort_by { |_k, v| -v[:total_users] }.to_h

    render json: {
      methodology: 'Active means has scheduled a CP or QA. Total is number of accounts that exist (including logged in or not logged in).', data: h
    }
  end

  def assessment
    @headers = ['#', 'PL', 'Name of supplier', 'Supplier code (CNUF)', 'Which DPP', 'Industrial process',
                'Address of the factory', 'City', 'Country', 'Expected assessment date', 'Number of workers', 'Assessment type', 'Responsible OPM', 'Confirmed assessment date', 'Assessor', 'Report', 'Result', 'One-time ABC']
    assessments = QualityAssessment.valid.with_supplier.with_user_profiles
    assessments = assessments.where(filter_hash) if params.reject do |k, _v|
                                                      %w[action controller].include?(k)
                                                    end.present?
    assessor_index = AssessorProfile.select(:id, :user_id, :name).all
                                    .group_by(&:id).map { |id, ass_set| [id.to_s, ass_set[0]] }.to_h

    @totals = { all: assessments.size, done: assessments.where(completed: true).size,
                not_done: assessments.where(completed: false).size }
    @assessments = assessments.map do |x|
      s = x.supplier_profile

      [
        s.pl_set.present? ? assessor_names(s.pl_set, assessor_index) : 'NO PL',
        s.name,
        s.code,
        "#{s.city}/#{s.country}", # 'Which DDP'
        s.industrial_process,
        s.address, # this includes city and country but added city and country columns for sorting
        s.city,
        s.country,
        x.assess_date.strftime('%Y-%m-%d'),
        'TBA', # 'Number of workers'
        x.assess_type,
        s.opm_set.present? ? assessor_names(s.opm_set, assessor_index) : 'NO OPM',
        x.assessor_confirmed ? x.assess_date.strftime('%Y-%m-%d') : 'UNCONFIRMED', # confirmed assessment date
        x.assessor_name_noquery, # replaced x.assessor_name to get rid of n+1 query
        x.completed ? 'Done' : 'Not done',
        x.result.present? ? x.result : 'In progress',
        'TBA' # 'One-time ABC'
      ]
    end
  end

  def excel_by_code
    supp = SupplierProfile.find_by(code: params[:code])
    @xl = XlsxDump.new(supplier: supp)

    send_data @xl.dump!, filename: "#{supp.code}-#{supp.name}-#{Time.now}.xlsx",
                         disposition: 'attachment'
  end

  def excel_global
    par = params.except(:utf8, :controller, :action, :commit).merge({ type: :global })
    @xl = XlsxDump.new(par)

    send_data @xl.dump!, filename: "assessgo-global-stats-#{Time.now}.xlsx",
                         disposition: 'attachment'
  end

  private

  def assessor_names(ids, assessor_index)
    ids.map { |x| assessor_index[x].name }
  end

  def filter_hash
    clean_params = {}
    params.each { |k, v| clean_params[k] = (v.respond_to?(:reject) ? v.reject { |x| x == 'all' } : v) }
    filter_hash = {}
    supplier_filter = {}

    # Supplier
    supplier_filter[:country] = clean_params['countries'] if clean_params['countries'].present?
    supplier_filter[:industrial_process] = clean_params['processes'] if clean_params['industrial_processes'].present?

    # Assessment
    if clean_params['start_date'].present? && clean_params['end_date']
      filter_hash[:assess_date] =
        clean_params['start_date']..clean_params['end_date']
    end
    filter_hash[:grid] = clean_params['grids'] if clean_params['grids'].present?
    filter_hash[:assess_type] = clean_params['assess_types'] if clean_params['assess_types'].present?

    # Merge
    filter_hash[:supplier_profiles] = supplier_filter if supplier_filter.present?
    filter_hash
  end

  def bar_opts(hashy = {})
    { x_title: hashy[:x_title], x_labels: hashy[:x_labels] }
  end

  def multi_stats_assessment(params)
    params = params.reject { |k, _v| %w[controller action].include?(k) }
    puts "params .... ======>>>>>> #{params}"

    # Add 'all' as options in multi-selects
    params[:grids] = params[:grids].present? ? params[:grids].reject { |x| x == 'all' } : []
    params[:assess_types] = params[:assess_types].present? ? params[:assess_types].reject { |x| x == 'all' } : []
    params[:countries] = params[:countries].present? ? params[:countries].reject { |x| x == 'all' } : []
    params[:assessors] = params[:assessors].present? ? params[:assessors].reject { |x| x == 'all' } : []
    params[:processes] = params[:processes].present? ? params[:processes].reject { |x| x == 'all' } : []

    data = QualityAssessment.joins(:supplier_profile).where(destroyed_at: nil,
                                                            assess_date: params[:start_date]..params[:end_date])
    data = data.where(completed: params[:completed]) if params[:completed].present? && params[:completed] != 'all'
    data = data.where(supplier_profiles: { country: params[:countries] }) if params[:countries].present?
    data = data.where(supplier_profiles: { industrial_process: params[:processes] }) if params[:processes].present?
    data = data.where(grid: params[:grids]) if params[:grids].present?
    data = data.where(assess_type: params[:assess_types]) if params[:assess_types].present?
    data = data.where(result: params[:results]) if params[:results].present?

    # group by
    by_grid = data.group(:grid).count.map { |k, v| [k.gsub('_assessment', '').gsub('quality', 'QA').upcase, v] }
    by_country = data.group('supplier_profiles.country').count.map do |k, v|
                   [country_name(k), v]
                 end.sort_by { |x| x[1] }.reverse.to_h

    by_result = data.group(:result).count
    by_process = data.group('supplier_profiles.industrial_process').count.to_a.sort_by do |x|
      x[1]
    end.reverse.first(15).to_h
    group_by = { grid: { data: by_grid, chart_type: :pie }, country: { data: by_country, chart_type: :bar },
                 result: { data: by_result, chart_type: :pie }, process: { data: by_process, chart_type: :bar } }

    # time series
    time_series = { month: { chart_type: :spline, data: data.group_by_month(:assess_date, format: '%b %Y').count } }

    # top 10
    # top_ten = data.group(:name).order(count_all: :desc).count

    { group_by: group_by, time_series: time_series }
  end

  def multi_stats_control_plan(params)
    params = params.reject { |k, _v| %w[controller action].include?(k) }
    puts "params .... ======>>>>>> #{params}"

    # Add 'all' as options in multi-selects
    params[:grids] = params[:grids].present? ? params[:grids].reject { |x| x == 'all' } : []
    params[:assess_types] = params[:assess_types].present? ? params[:assess_types].reject { |x| x == 'all' } : []
    params[:countries] = params[:countries].present? ? params[:countries].reject { |x| x == 'all' } : []
    params[:assessors] = params[:assessors].present? ? params[:assessors].reject { |x| x == 'all' } : []
    params[:processes] = params[:processes].present? ? params[:processes].reject { |x| x == 'all' } : []

    data = ControlPlan.includes(plan_items: [:cap_points, process_step: :cp_pending_frequencies]).joins(product_family: :supplier_profile).where(control_plans: { destroyed_at: nil,
                                                                                       assess_date: params[:start_date]..params[:end_date] })
    data = data.where(completed: params[:completed]) if params[:completed].present? && params[:completed] != 'all'
    data = data.where(supplier_profiles: { country: params[:countries] }) if params[:countries].present?
    data = data.where(supplier_profiles: { industrial_process: params[:processes] }) if params[:processes].present?
    data = data.where(plan_type: params[:plan_types]) if params[:plan_types].present?

    # group by
    by_country = data.group('supplier_profiles.country').count.map do |k, v|
                   [country_name(k), v]
                 end.sort_by { |x| x[1] }.reverse.to_h

    by_process = data.group('supplier_profiles.industrial_process').count

    by_noncom = cp_rate_group_by(data.map { |x| x.nonconformity_rate }).map { |k, v| [k, v.length] }.to_h
    by_freq_rate = cp_rate_group_by(data.map { |x| x.freq_respect_rate('Decathlon', false) }).map { |k, v| [k, v.length] }.to_h
    by_cap_close_rate = cp_rate_group_by(data.map { |x| x.cap_close_rate }).map { |k, v| [k, v.length] }.to_h
    puts "by_noncom === >>>> #{by_noncom}"

    group_by = { country: { data: by_country, chart_type: :bar }, process: { data: by_process, chart_type: :bar },
                 nonconformity_rate: { data: by_noncom, chart_type: :pie }, frequency_respected_rate: { data: by_freq_rate, chart_type: :pie }, cap_close_rate: { data: by_cap_close_rate, chart_type: :pie } }

    # time series
    time_series = { month: { chart_type: :spline, data: data.group_by_month(:assess_date, format: '%b %Y').count } }

    { group_by: group_by, time_series: time_series }
  end

  def multi_stats_supplier(params)
    params = params.reject { |k, _v| %w[controller action].include?(k) }
    puts "params .... ======>>>>>> #{params}"

    # Add 'all' as options in multi-selects
    params[:grids] = params[:grids].present? ? params[:grids].reject { |x| x == 'all' } : []
    params[:assess_types] = params[:assess_types].present? ? params[:assess_types].reject { |x| x == 'all' } : []
    params[:countries] = params[:countries].present? ? params[:countries].reject { |x| x == 'all' } : []
    params[:assessors] = params[:assessors].present? ? params[:assessors].reject { |x| x == 'all' } : []
    params[:processes] = params[:processes].present? ? params[:processes].reject { |x| x == 'all' } : []

    qa_present = params[:grids].present? && params[:grids].reject { |x| x == 'control_plan' }
    cp_present = params[:grids].present? && params[:grids].select { |x| x == 'control_plan' }

    data = SupplierProfile.joins(:quality_assessments).where(quality_assessments: { destroyed_at: nil,
                                                                                    assess_date: params[:start_date]..params[:end_date] })
    if params[:completed].present? && params[:completed] != 'all'
      data = data.where(quality_assessments: { completed: params[:completed] })
    end
    data = data.where(supplier_profiles: { country: params[:countries] }) if params[:countries].present?
    data = data.where(supplier_profiles: { industrial_process: params[:processes] }) if params[:processes].present?

    data = data.where(supplier_profiles: { finished_goods_supplier: true }) if params[:finished_goods_supplier].present?
    data = data.where(supplier_profiles: { component_supplier: true }) if params[:component_supplier].present?
    data = data.where(supplier_profiles: { partner: true }) if params[:partner].present?

    data = data.where(quality_assessments: { grid: params[:grids] }) if params[:grids].present? && qa_present
    data = data.where(quality_assessments: { assess_type: params[:assess_types] }) if params[:assess_types].present? && params[:assess_types].reject do |x|
                                                                                        ['New Monitor',
                                                                                         'CAP Review'].include?(x)
                                                                                      end.present?
    data = data.where(quality_assessments: { result: params[:results] }) if params[:results].present?

    data_cp = data_cp = SupplierProfile.joins(product_families: :control_plans).where(control_plans: {
                                                                                        destroyed_at: nil, assess_date: params[:start_date]..params[:end_date]
                                                                                      })
    if params[:completed].present? && params[:completed] != 'all'
      data_cp = data_cp.where(control_plans: { completed: params[:completed] })
    end
    data_cp = data_cp.where(supplier_profiles: { country: params[:countries] }) if params[:countries].present?
    data_cp = data_cp.where(control_plans: { plan_type: params[:plan_types] }) if params[:assess_types].present? && params[:assess_types].select do |x|
                                                                                    ['New Monitor',
                                                                                     'CAP Review'].include?(x)
                                                                                  end.present?

    top_ten = {}
    if params[:grids].blank? || qa_present
      top_ten.merge!({ assessment: { data: data.group(:name).order(count_all: :desc).limit(10).count,
                                     chart_type: :bar } })
    end

    if params[:grids].blank? || cp_present
      top_ten.merge!({ control_plan: { data: data_cp.group(:name).order(count_all: :desc).limit(10).count,
                                       chart_type: :bar } })
    end

    # group by
    group_by = {}
    group_data = (data + data_cp).uniq

    # component vs finished goods
    supplier_type = []
    supplier_type << ['Finished Goods', group_data.select { |x| x.finished_goods_supplier }.count]
    supplier_type << ['Component', group_data.select { |x| x.component_supplier }.count]
    supplier_type << ['Partner', group_data.select { |x| x.partner }.count]

    group_by.merge!({ supplier_type: { data: supplier_type, chart_type: :bar } })
    puts "supplier_type ===>>> #{supplier_type}"

    # countries suppliers
    supplier_countries = group_data.group_by { |x| x.country }.map { |k, v| [k, v.count] }
    puts "supplier_countries ===>>> #{supplier_countries}"
    group_by.merge!({ supplier_countries: { data: supplier_countries, chart_type: :pie } })

    # time series
    # stacked column with active/inactive suppliers (supplier.last_login.present?)
    time_query = <<-SQL
    with tokens as (
      select t.user_id, t.created_at, row_number () over (partition by t.user_id order by t.created_at desc)
      from authentication_tokens t
      order by 1, 2 desc
    ),
    last_login as (
      select *
      from tokens t
      where t.row_number = 1
    ),
    combined as (
      select s.id, TO_CHAR(s.created_at :: DATE, 'yyyymm') supplier_created_month,
      ll.user_id, TO_CHAR(ll.created_at :: DATE, 'yyyymm') last_login_month
      from supplier_profiles s
      left join last_login ll ON ll.user_id = s.user_id
    ),
    unioned as (
      select id, supplier_created_month as month, 'created' as type
      from combined
      union all
      select id, last_login_month, 'first_login'
      from combined
      where user_id is not null
      union all
      select id, supplier_created_month, 'not_logged_in'
      from combined
      where user_id is null
    ),
    by_month as (
      select month
    ,sum(case when type = 'created' then 1 else 0 end) created
    ,sum(case when type = 'first_login' then 1 else 0 end) first_login
    ,sum(case when type = 'not_logged_in' then 1 else 0 end) not_logged_in
    from unioned
    group by 1
    order by 1),
    by_type as (
      select type, count(*) count_total
      from unioned
      group by 1
    order by 1)

    select m2.month
    , sum(m.first_login) count_active
    , sum(m.created) - sum(m.first_login) count_inactive

    from by_month m
    left join by_month m2 on m2.month >= m.month
    group by 1
    order by 1 asc
    SQL

    time_result = exec_query(time_query)
    # time_result = time_result.columns + time_result.rows
    time_result = time_result.rows # .map{|x| [x[0], [x[1], x[2]]]}.to_h
    puts "time series result... ===>>> #{time_result}"
    time_series = { supplier_account: { data: time_result, chart_type: :column, stacked: %w[active inactive] } }

    { top_ten: top_ten, time_series: time_series, group_by: group_by }
  end

  def multi_stats_assessor(params)
    # Requested STATS
    # completed assessment count (top 10)
    # count of likes
    # count of added to favorite
    # count of comments given
    # count of comments received >>>> to be clarified

    # Add 'all' as options in multi-selects
    params[:grids] = params[:grids].present? ? params[:grids].reject { |x| x == 'all' } : []
    params[:assess_types] = params[:assess_types].present? ? params[:assess_types].reject { |x| x == 'all' } : []
    params[:countries] = params[:countries].present? ? params[:countries].reject { |x| x == 'all' } : []
    params[:assessors] = params[:assessors].present? ? params[:assessors].reject { |x| x == 'all' } : []
    params[:processes] = params[:processes].present? ? params[:processes].reject { |x| x == 'all' } : []

    query_subs = <<-SQL
    WITH
    assessors AS (
      SELECT DISTINCT ref_id, user_id, ref_model, country
      FROM
      (SELECT qa.id ref_id, created_by user_id, 'quality_assessment' ref_model, ap.country
       FROM quality_assessments qa
       JOIN assessor_profiles ap ON ap.user_id = qa.created_by
       WHERE qa.destroyed_at IS NULL
       UNION ALL
       SELECT qa.id ref_id, aa.user_id, 'quality_assessment' ref_model, ap.country
       FROM quality_assessments qa
       LEFT JOIN assessor_assignments aa ON aa.ref_id = qa.id and ref_model = 'quality_assessment'
       JOIN assessor_profiles ap ON ap.user_id = aa.user_id
       WHERE qa.destroyed_at IS NULL AND aa.confirmed = TRUE
       UNION ALL
       SELECT cp.id ref_id, u.id user_id, 'control_plan' ref_model, ap.country
       FROM control_plans cp
       JOIN assessor_profiles ap ON ap.id = cp.assessor_profile_id AND cp.assessor_type = 'assessor'
       JOIN users u ON u.id = ap.user_id
       WHERE cp.destroyed_at IS NULL) qa
    ),
    count_countries AS (
      SELECT a.country, COUNT(*) count_countries
      FROM assessors a
    GROUP BY 1),
    count_assessments AS (
      SELECT a.user_id, COUNT(distinct qa.id) count_assessments
      FROM assessors a
      JOIN quality_assessments qa ON qa.id = a.ref_id
      WHERE qa.completed = TRUE AND qa.destroyed_at IS NULL
      AND a.ref_model = 'quality_assessment'
    GROUP BY 1),
    count_plans AS (
      SELECT a.user_id, COUNT(distinct cp.id) count_plans
      FROM assessors a
      JOIN control_plans cp ON cp.id = a.ref_id
      WHERE cp.completed = TRUE AND cp.destroyed_at IS NULL
      AND a.ref_model = 'control_plan'
    GROUP BY 1),
    count_comments AS (
      SELECT u.id user_id, COUNT(distinct c.id) count_comments
      FROM comments c JOIN users u ON u.id = c.user_id
    GROUP BY 1),

    count_comments_received AS (
      SELECT user_id, COUNT(distinct comment_id) count_comments_received
      FROM
      (SELECT a.user_id, g.ref_model, c.id comment_id
       FROM comments c
       JOIN goes_activities g ON g.id = c.ref_id AND c.ref_model = 'goes_activity'
       JOIN assessors a ON a.ref_id = g.ref_id
       WHERE c.ref_model = 'goes_activity'
       UNION ALL
       SELECT a.user_id, 'chapter_assessment', c.id comment_id
       FROM comments c
       JOIN chapter_assessments ca ON ca.id = c.ref_id AND c.ref_model = 'chapter_assessment'
       JOIN quality_assessments qa ON qa.id = ca.quality_assessment_id
       JOIN assessors a ON a.ref_id = qa.id and a.ref_model = 'quality_assessment'
       WHERE c.ref_model = 'chapter_assessment') c
      GROUP BY 1
    ),
    count_likes AS (
      SELECT u.id user_id, COUNT(distinct l.id) count_likes
      FROM likes l JOIN users u ON u.id = l.user_id
    GROUP BY 1),
    count_favorites AS (
      SELECT u.id user_id, COUNT(distinct l.id) count_favorites
      FROM favorites l JOIN users u ON u.id = l.user_id
    GROUP BY 1),
    count_favorited AS (
      SELECT user_id, COUNT(distinct fav_id) count_favorited
      FROM
      (SELECT a.user_id, f.id fav_id
       FROM favorites f
       JOIN assessors a ON f.ref_id = a.ref_id AND f.ref_model = a.ref_model
       WHERE f.ref_model = 'quality_assessment'
       UNION ALL
       SELECT a.user_id, f.id fav_id
       FROM favorites f
       JOIN assessors a ON f.ref_id = a.ref_id AND f.ref_model = a.ref_model
       WHERE f.ref_model = 'control_plan') f
    GROUP BY 1),
    count_followers AS (
      SELECT f.ref_id ref_id, COUNT(*) count_followers
      FROM followers f
      WHERE f.ref_model = 'assessor_profile'
    GROUP BY 1)
    SQL

    query_select = <<-SQL
    SELECT CASE WHEN ap.name IS NULL THEN u.username ELSE ap.name END as name,
      qa.count_assessments, cp.count_plans, f.count_favorited, fs.count_favorites,
      l.count_likes, c.count_comments, cr.count_comments_received, cf.count_followers
    FROM assessor_profiles ap
    JOIN users u ON u.id = ap.user_id
    LEFT JOIN count_assessments qa ON qa.user_id = u.id
    LEFT JOIN count_plans cp ON cp.user_id = u.id
    LEFT JOIN count_favorited f ON f.user_id = u.id
    LEFT JOIN count_favorites fs ON fs.user_id = u.id
    LEFT JOIN count_likes l ON l.user_id = u.id
    LEFT JOIN count_comments c ON c.user_id = u.id
    LEFT JOIN count_comments_received cr ON cr.user_id = u.id
    LEFT JOIN count_followers cf ON cf.ref_id = ap.id
    SQL

    assessor_countries = AssessorProfile.joins(:user)
                                        .joins('join quality_assessments ON quality_assessments.created_by = users.id')
                                        .where(quality_assessments: { destroyed_at: nil, completed: true })

    opts = []

    if params[:countries].present?
      assessor_countries = assessor_countries.where(country: params[:countries])
      query_select += "\t#{query_select.include?('WHERE') ? 'AND' : 'WHERE'} ap.country IN (?)"
      opts << params[:countries]
    end

    if params[:assessors].present?
      query_select += "\t#{query_select.include?('WHERE') ? 'AND' : 'WHERE'} ap.id IN (?)"
      opts << params[:assessors]
    end

    # query_string += ""
    query_select += "\nORDER BY 1"

    query = query_subs + query_select
    puts "opts.. ===>>> #{opts}"
    puts "query_select ===>>> #{query_select}"

    res = exec_query([query] + opts)
    puts "res header===>>>>>>>> #{res.columns}"
    # puts "res values===>>>>>>>> #{res.to_a}"

    values = res.to_a

    header = res.columns
    # ["name", "count_assessments", "count_plans", "count_favorited", "count_favorites", "count_likes", "count_comments", "count_comments_received"]

    top_ten = {}

    # assessment count
    completed_assessments = values.reject do |x|
                              x['count_assessments'].nil?
                            end.map { |x| [x['name'], x['count_assessments']] }.sort_by { |y| y[1] }.reverse.first(10)
    puts "completed_assessments ==>> #{completed_assessments}"
    top_ten.merge!({ completed_assessments: { data: completed_assessments, chart_type: :bar } })

    completed_control_plans = values.reject do |x|
                                x['count_plans'].nil?
                              end.map { |x| [x['name'], x['count_plans']] }.sort_by { |y| y[1] }.reverse.first(10)
    puts "completed_cp ==>> #{completed_control_plans}"
    top_ten.merge!({ completed_control_plans: { data: completed_control_plans, chart_type: :bar } })

    added_to_favorite = values.reject do |x|
                          x['count_favorited'].nil?
                        end.map { |x| [x['name'], x['count_favorited']] }.sort_by { |y| y[1] }.reverse.first(10)
    puts "added_to_favorite ==>> #{added_to_favorite}"
    top_ten.merge!({ added_to_favorite: { data: added_to_favorite, chart_type: :bar } })

    total_likes = values.reject do |x|
                    x['count_likes'].nil?
                  end.map { |x| [x['name'], x['count_likes']] }.sort_by { |y| y[1] }.reverse.first(10)
    puts "total_likes ==>> #{total_likes}"
    top_ten.merge!({ total_likes: { data: total_likes, chart_type: :bar } })

    comments_given = values.reject do |x|
                       x['count_comments'].nil?
                     end.map { |x| [x['name'], x['count_comments']] }.sort_by { |y| y[1] }.reverse.first(10)
    puts "comments_given ==>> #{comments_given}"
    top_ten.merge!({ comments_given: { data: comments_given, chart_type: :bar } })

    comments_received = values.reject do |x|
                          x['count_comments_received'].nil?
                        end.map do |x|
                          [x['name'],
                           x['count_comments_received']]
                        end.sort_by { |y| y[1] }.reverse.first(10)
    puts "comments_received ==>> #{comments_received}"
    top_ten.merge!({ comments_received: { data: comments_received, chart_type: :bar } })

    followers = values.reject do |x|
                  x['count_followers'].nil?
                end.map { |x| [x['name'], x['count_followers']] }.sort_by { |y| y[1] }.reverse.first(10)
    puts "followers ==>> #{followers}"
    top_ten.merge!({ followers: { data: followers, chart_type: :bar } })

    assessor_countries = assessor_countries.group(:country).count.map { |k, v| [country_name(k), v] }.to_h
    # countries = values.reject{|x| x["count_countries"].nil? }.map{|x| [x["name"], x["count_countries"]]}.sort_by{|y| y[1]}.reverse.first(10)
    # puts "countries ==>> #{countries}"
    top_ten.merge!({ countries: { data: assessor_countries, chart_type: :bar } })

    { top_ten: top_ten }
  end

  def set_supplier_options!
    @supplier = params[:supplier] || 'none'
    @assess_type = params[:assess_type] || 'all'
    @plan_type = params[:plan_type] || 'all'
    @status = params[:status] || 'all'

    @fgp = (params[:finished_goods_supplier].present? && params[:finished_goods_supplier] == 'true')
    @component = (params[:component_supplier].present? && params[:component_supplier] == 'true')
    @partner = (params[:partner].present? && params[:partner] == 'true')

    sup_options = { supplier: @supplier, status: @status, finished_goods_supplier: @fgp,
                    component_supplier: @component, partner: @partner }

    @options.merge!(sup_options)
  end

  def supplier_bar(options = {}, res)
    # FOR TOP 10 QA
    res_bar = SupplierProfile.joins(:quality_assessments).where(quality_assessments: { completed: options[:completed],
                                                                                       assess_date: Time.parse(@start_date)..Time.parse(@end_date) })

    res_bar = res_bar.where(supplier_profiles: { status: @status }) if @status != 'all'
    res_bar = res_bar.where(supplier_profiles: { finished_goods_supplier: true }) if @fgp
    res_bar = res_bar.where(supplier_profiles: { component_supplier: true }) if @component
    res_bar = res_bar.where(supplier_profiles: { partner: true }) if @partner
    res_bar = res_bar.where(supplier_profiles: { industrial_process: @processes }) if @processes.present?
    res_bar = res_bar.where(quality_assessments: { grid: @grid }) if @grid != 'all'
    res_bar = res_bar.where(quality_assessments: { assess_type: @assess_type }) if @assess_type != 'all'

    # puts "1. #{res_bar}"
    res_bar = res_bar.group_by { |x| x.name }.map { |key, items| [key, items.count] }.sort_by(&:last).reverse.first(10)

    Hash[res_bar]
    # puts "2. #{res_bar}"

    res_bar = res[:bar].values.first(10)

    qa_labels = res_bar.map { |x| x[0] }
    title_prefix = options[:completed] ? 'Completed' : 'Open'

    qa_data = [{
      name: 'Assessments',
      data: res_bar.map { |x| x[1] }
    }]

    qa_opts = {
      chart: { type: :bar, styledMode: true },
      title: { text: "#{title_prefix} Assessments - Top 10 Suppliers" },
      yAxis: { tickInterval: 1 },
      xAxis: { title: { text: 'Supplier' }, categories: qa_labels }
    }
    Daru::View::Plot.new(qa_data, qa_opts)
  end

  def supplier_cp_bar(options = {})
    # FOR TOP 10 CP
    res_cp_bar = SupplierProfile.joins(product_families: :control_plans).where(control_plans: {
                                                                                 completed: options[:completed], assess_date: Time.parse(@start_date)..Time.parse(@end_date)
                                                                               })
    res_cp_bar = res_cp_bar.where(supplier_profiles: { status: @status }) if @status != 'all'
    res_cp_bar = res_cp_bar.where(supplier_profiles: { finished_goods_supplier: true }) if @fgp
    res_cp_bar = res_cp_bar.where(supplier_profiles: { component_supplier: true }) if @component
    res_cp_bar = res_cp_bar.where(supplier_profiles: { partner: true }) if @partner
    res_cp_bar = res_cp_bar.where(supplier_profiles: { industrial_process: @processes }) if @processes.present?
    res_cp_bar = res_cp_bar.where(control_plans: { plan_type: @plan_type }) if @plan_type != 'all'

    puts "1. #{res_cp_bar}"
    res_cp_bar = res_cp_bar.group_by do |x|
                   x.name
                 end.map { |key, items| [key, items.count] }.sort_by(&:last).reverse.first(10)

    Hash[res_cp_bar]
    puts "2. #{res_cp_bar}"

    cp_labels = res_cp_bar.map { |k, _v| k }
    title_prefix = options[:completed] ? 'Completed' : 'Open'

    cp_data = [{
      name: 'Control Plans',
      data: res_cp_bar.map { |_k, v| v }
    }]

    cp_opts = {
      chart: { type: :bar, styledMode: true },
      title: { text: "#{title_prefix} Control Plans - Top 10 Suppliers" },
      yAxis: { tickInterval: 1 },
      xAxis: { title: { text: 'Supplier' }, categories: cp_labels }
    }
    Daru::View::Plot.new(cp_data, cp_opts)
  end

  def supplier_query(options = {})
    qa_present = options[:grids].reject { |x| x == 'control_plan' }.present? || options[:grids].blank?
    cp_present = options[:grids].select { |x| x == 'control_plan' }.present? || options[:grids].blank?

    query_string_qa = ''
    opts = []

    if qa_present
      opts = [options[:completed], options[:start_date], options[:end_date]]

      assessor_sub_query = "SELECT qa.id, ap.name
                FROM quality_assessments qa
                JOIN assessor_assignments aa ON aa.ref_id = qa.id AND aa.ref_model = 'quality_assessment'
                JOIN assessor_profiles ap ON ap.user_id = aa.user_id
                UNION ALL
                SELECT qa.id, CASE WHEN ap.name IS NULL THEN u.username ELSE ap.name END as assessor
                FROM quality_assessments qa
                JOIN users u ON u.id = qa.created_by
                LEFT JOIN assessor_profiles ap ON ap.user_id = u.id"

      query_string_qa += "SELECT DISTINCT sp.name, sp.code, sp.country,
          qa.assess_date, qa.grid, qa.assess_type, qa.result,
          sp.industrial_process, a.assessors
          FROM supplier_profiles sp
          JOIN quality_assessments qa ON qa.supplier_profile_id = sp.id
          JOIN (
              SELECT ap.id, string_agg(DISTINCT ap.name, ', ') as assessors
              FROM (#{assessor_sub_query}) as ap
              group by 1
          ) a ON a.id = qa.id
          WHERE qa.completed = ? AND qa.destroyed_at IS NULL
          AND (qa.assess_date BETWEEN ? AND ?)"

      # assess_types
      if options[:assess_types].present?
        query_string_qa += ' AND qa.assess_type IN (?)'
        opts << options[:assess_types]
      end

      # grids
      if options[:grids].present?
        query_string_qa += ' AND qa.grid IN (?)'
        opts << options[:grids]
      end

      # countries
      if options[:countries].present?
        query_string_qa += ' AND sp.country IN (?)'
        opts << options[:countries]
      end

      # processes
      if options[:processes].present?
        query_string_qa += ' AND sp.industrial_process IN (?)'
        opts << options[:processes]
      end

      # supplier
      if options[:supplier] != 'none'
        query_string_qa += ' AND CONCAT(sp.code, sp.name) ILIKE ?'
        opts << options[:supplier]
      end

      # status
      if options[:status] != 'all'
        query_string_qa += ' AND sp.status = ?'
        opts << options[:status]
      end

      # fg supplier
      if options[:finished_goods_supplier].present?
        query_string_qa += ' AND sp.finished_goods_supplier = ?'
        opts << true
      end

      # component
      if options[:component_supplier].present?
        query_string_qa += ' AND sp.component_supplier = ?'
        opts << true
      end

      # partner
      if options[:partner].present?
        query_string_qa += ' AND sp.partner = ?'
        opts << true
      end

      # prep_completed
      if options[:prep_completed] != 'all'
        query_string_qa += ' AND qa.prep_completed = ?'
        opts << (options[:prep_completed] == 'true')
      end

      # cap_finished
      if options[:cap_finished] != 'all'
        @cap_filter = options[:cap_finished]
        query_string_qa += ' AND qa.id IN (?)'
        # cap_opts = calc_cap_interval
        opts << calc_cap_interval
      end
    end

    p "qa opts ===>>>> #{opts}"

    query_string_cp = ''
    cp_opts = []
    if cp_present
      # cp_opts = [options[:start_date], options[:end_date]]
      # query_string_cp += "SELECT DISTINCT sp.name, sp.code, sp.country,
      #   cp.assess_date, 'control_plan' as grid, cp.plan_type as assess_type, cp.nonconformity_rate,
      #   sp.industrial_process,  ap.name as assessors
      #   FROM supplier_profiles sp
      #   JOIN product_families pf ON pf.supplier_profile_id = sp.id
      #   JOIN control_plans cp ON cp.product_family_id = pf.id
      #   JOIN assessor_profiles ap ON ap.id = cp.assessor_profile_id
      #   WHERE cp.completed = true AND cp.destroyed_at IS NULL
      #   AND (cp.assess_date BETWEEN ? AND ?)"

      # if options[:assess_types].select{|x| ['CAP Review', 'New Monitor'].include?(x) }.present?
      #   query_string_cp += " AND cp.plan_type IN (?)"
      #   cp_opts << options[:assess_types].select{|x| ['CAP Review', 'New Monitor'].include?(x) }
      # end
    end

    p "qa opts ===>>>> #{opts}"
    combined = query_string_qa

    query_string = "SELECT * FROM (SELECT *, ROW_NUMBER () OVER (PARTITION BY name ORDER BY assess_date DESC)
        FROM (#{combined}) combined
        ORDER BY name ASC, assess_date DESC) as res
        WHERE row_number = 1
        "

    query = [query_string] + opts
    # query << cap_opts if cap_opts.present?
    p "query ===>>>> #{query}"

    query_count = ["SELECT name, COUNT(*) count_assessments
            FROM (#{combined}) combined
            GROUP BY 1
            ORDER BY 2 DESC
            "] + opts

    { data: exec(query), bar: exec(query_count) }
  end

  def supplier_cp_query(options = {})
    puts "supplier_cp options ===>>>> #{options}"
    opts = [options[:completed], options[:start_date], options[:end_date]]

    query_string = "SELECT DISTINCT sp.name, sp.code, sp.country,
        cp.assess_date, cp.plan_type, cp.nonconformity_rate, sp.industrial_process,
        CASE WHEN cp.assessor_type = 'assessor' THEN ap.name ELSE users.username END as assessors

        FROM supplier_profiles sp
        JOIN product_families pf ON pf.supplier_profile_id = sp.id
        JOIN control_plans cp ON cp.product_family_id = pf.id
        JOIN assessor_profiles ap ON ap.id = cp.assessor_profile_id
        JOIN users on users.id = cp.created_by

        WHERE cp.completed = ? AND cp.destroyed_at IS NULL
        AND (cp.assess_date BETWEEN ? AND ?)"

    if options[:plan_type] != 'all'
      query_string += ' AND cp.plan_type = ?'
      opts << options[:plan_type]
    end

    # countries
    if options[:countries].present?
      query_string += ' AND sp.country IN (?)'
      opts << options[:countries]
    end

    # processes
    if options[:processes].present?
      query_string += ' AND sp.industrial_process IN (?)'
      opts << options[:processes]
    end

    # supplier
    if options[:supplier] != 'none'
      query_string += ' AND CONCAT(sp.code, sp.name) ILIKE ?'
      opts << options[:supplier]
    end

    # status
    if options[:status] != 'all'
      query_string += ' AND sp.status = ?'
      opts << options[:status]
    end

    # fg supplier
    if options[:finished_goods_supplier].present?
      query_string += ' AND sp.finished_goods_supplier = ?'
      opts << true
    end

    # component
    if options[:component_supplier].present?
      query_string += ' AND sp.component_supplier = ?'
      opts << true
    end

    # partner
    if options[:partner].present?
      query_string += ' AND sp.partner = ?'
      opts << true
    end

    puts "opts after filtering ===>>>> #{opts}"

    query = [query_string] + opts
    puts "query after filtering =====>>> #{query}"

    exec(query)
  end

  def rank_query(options = {})
    grid = options[:grid]
    grids = options[:grids]
    cp_present = grids.include?('control_plan') || grids.blank?
    assess_types = options[:assess_types]
    countries = options[:countries]
    prep_completed = options[:prep_completed]
    cap_finished = options[:cap_finished]
    completed2 = options[:completed2]
    prefix = options[:completed] ? 'Completed' : 'Open'
    title = "#{prefix} Assesments & Control Plans by Assessors"
    assessor_name_present = "assessor_profiles.name <> ''"
    name_with_username = "CONCAT(assessor_profiles.name, '(', users.username, ')')"

    data = User.joins("LEFT JOIN assessor_assignments ON assessor_assignments.user_id = users.id
        LEFT JOIN assessor_profiles ON assessor_profiles.user_id = users.id
        JOIN quality_assessments qa ON qa.id = assessor_assignments.ref_id
        JOIN supplier_profiles sp ON sp.id = qa.supplier_profile_id")
               .select("CASE WHEN #{assessor_name_present} THEN #{name_with_username} ELSE users.username END as prof_name,
          COUNT(DISTINCT assessor_assignments) as count_assessments, 0 as count_plans")
               .where('qa.destroyed_at IS NULL AND qa.completed = ? AND (qa.created_at BETWEEN ? AND ?)', options[:completed], options[:start_date], options[:end_date])

    # filtering
    data = data.where('qa.grid IN (?)', options[:grids].reject { |x| x == 'control_plan' }) if options[:grids].present?
    if options[:assess_types].present?
      data = data.where('qa.assess_type IN (?)', options[:assess_types].reject do |x|
                                                   x == 'New Monitor'
                                                 end)
    end
    data = data.where('sp.country IN (?)', options[:countries]) if options[:countries].present?
    if prep_completed.present? && prep_completed != 'all'
      data = data.where('qa.prep_completed = ?',
                        (prep_completed == 'true'))
    end

    data = filter_by_role(data, options[:role]) if options[:role]

    if cap_finished.present? && cap_finished != 'all'
      @cap_filter = cap_finished
      data = data.where('qa.id IN (?)', calc_cap_interval)
    end

    data_raw = data_raw.where(id: calc_completed_interval) if completed2.present? && completed2 != 'all'
    # final..

    qa_data = data.group('prof_name')

    # =================== CONTROL PLAN ================= #
    if cp_present
      cp_data = User.left_joins(:assessor_profile)
                    .joins('INNER JOIN control_plans cp ON cp.created_by = users.id')
                    .select("CASE WHEN #{assessor_name_present} THEN #{name_with_username} ELSE users.username END as prof_name, 0 as count_assessments,COUNT(*) as count_plans")
                    .where('cp.destroyed_at IS NULL AND cp.completed = ? AND (cp.created_at BETWEEN ? AND ?)', options[:completed], options[:start_date], options[:end_date])

      cp_types = options[:assess_types].select { |x| ['New Monitor', 'CAP Review'].include?(x) }
      cp_data = cp_data.where('cp.plan_type IN (?)', cp_types) if cp_types.present?
      cp_data = filter_by_role(cp_data, options[:role]) if options[:role]
      cp_data = cp_data.group('prof_name')
    end

    # ================ QA + CP ================== #
    query = "SELECT t.prof_name, SUM(t.count_assessments) count_assessments#{', SUM(t.count_plans)' if cp_present}
                FROM (#{qa_data.to_sql} #{cp_present ? 'UNION ALL ' + cp_data.to_sql : ''}) as t
                GROUP BY t.prof_name
                ORDER BY count_assessments DESC"

    result = ActiveRecord::Base.connection.execute(query).values
    puts "result... ==>>> #{result}"

    labels = result.map { |x| x[0] }
    puts "labels... ==>>> #{labels}"

    data = [{
      name: 'Assessments',
      data: result.map { |x| x[1].to_i }
    }]
    data << { name: 'Control Plans', data: result.map { |x| x[2].to_i } } if cp_present
    puts "data ==> #{data}"

    opts = {
      chart: {
        type: :bar,
        styledMode: true
      },
      title: { text: title },
      yAxis: { tickInterval: 1 },
      xAxis: { title: { text: 'Assessors' }, categories: labels }
    }
    { data: data, opts: opts, result: result }
  end

  def process_query(options)
    p "options ===>>>> #{options}"

    qa_opts = [options[:completed], options[:start_date], options[:end_date]]
    cp_opts = [options[:completed], options[:start_date], options[:end_date]]

    qa_sub_query = "SELECT
              CASE WHEN COALESCE(industrial_process, '') = '' THEN 'Not Selected'
                ELSE industrial_process END as industrial_process,
              sp.id AS s_id, COUNT(qa.id) AS assessments, 0 AS plans
                  FROM supplier_profiles sp
                  JOIN quality_assessments qa ON qa.supplier_profile_id = sp.id
                  WHERE qa.destroyed_at IS NULL AND qa.completed = ?
                  AND (qa.created_at BETWEEN ? AND ?)"

    cp_sub_query = "SELECT
            CASE WHEN COALESCE(industrial_process, '') = '' THEN 'Not Selected'
                ELSE industrial_process END as industrial_process,
                sp.id AS s_id, 0 AS assessments, COUNT(cp.id) AS plans
                  FROM supplier_profiles sp
                  JOIN product_families pf ON sp.id = pf.supplier_profile_id
                  JOIN control_plans cp ON pf.id = cp.product_family_id
                  WHERE cp.destroyed_at IS NULL AND cp.completed = ?
                  AND (cp.created_at BETWEEN ? AND ?)"

    # grids
    if options[:grids].present?
      qa_sub_query += ' AND qa.grid IN (?)'
      qa_opts << options[:grids]
    end

    # assess types
    assess_types = options[:assess_types].reject { |x| x == 'New Monitor' } || []
    if assess_types.present?
      qa_sub_query += ' AND qa.assess_type IN (?)'
      qa_opts << assess_types
    end

    # countries qa
    if options[:countries].present?
      qa_sub_query += ' AND sp.country IN (?)'
      qa_opts << options[:countries]
    end

    # processes qa
    if options[:processes].present?
      qa_sub_query += ' AND sp.industrial_process IN (?)'
      qa_opts << options[:processes]
    end

    # prep_completed
    if options[:prep_completed] != 'all'
      qa_sub_query += ' AND qa.prep_completed = ?'
      qa_opts << (options[:prep_completed] == 'true')
    end

    # cap_finished
    cap_finished = (options[:cap_finished] != 'all')
    if cap_finished
      @cap_filter = options[:cap_finished]
      qa_sub_query += ' AND qa.id IN (?)'
      qa_opts << calc_cap_interval
    end

    # completed2
    if options[:completed2] != 'all'
      @completed2 = options[:completed2]
      query_string_qa += ' AND qa.id IN (?)'
      # cap_opts = calc_cap_interval
      opts << calc_completed_interval
    end

    # countries cp
    if options[:countries].present?
      cp_sub_query += ' AND sp.country IN (?)'
      cp_opts << options[:countries]
    end

    # processes qa
    if options[:processes].present?
      cp_sub_query += ' AND sp.industrial_process IN (?)'
      cp_opts << options[:processes]
    end

    # plan_type cp
    plan_types = options[:assess_types].select { |x| ['New Monitor', 'CAP Review'].include?(x) } || []
    if plan_types.present?
      cp_sub_query += ' AND cp.plan_type in (?)'
      cp_opts << plan_types
    end

    # connect all query
    qa_sub_query += " GROUP BY CASE WHEN COALESCE(industrial_process, '') = '' THEN 'Not Selected' ELSE industrial_process END, s_id"
    cp_sub_query += " GROUP BY CASE WHEN COALESCE(industrial_process, '') = '' THEN 'Not Selected' ELSE industrial_process END, s_id"
    # query_string = "#{query_start}
    #                 FROM (#{qa_sub_query}) as qa
    #                 FULL OUTER JOIN (#{cp_sub_query}) as cp ON cp.industrial_process = qa.industrial_process"

    qa_grids = options[:grids].reject { |x| x == 'control_plan' }
    cp_grids = options[:grids].select { |x| x == 'control_plan' }
    puts "qa_grids ===>>>> #{qa_grids}"

    query_string = "SELECT industrial_process,
                        COUNT(DISTINCT s_id) as assessed_suppliers,
                        SUM(assessments) as assessments,
                        SUM(plans) as plans
                        FROM (#{qa_sub_query} UNION ALL #{cp_sub_query}) as qa
                        GROUP BY industrial_process"
    query = [query_string] + qa_opts + cp_opts

    puts "query ====>>>>> #{query}"
    result = exec(query).values
    puts "result ====>>>>> #{result}"

    result.sort_by! { |x| x[0] == 'Not Selected' ? 999 : x[2].to_i }

    { data: process_data(result), opts: process_opts(result, options[:completed]), result: result }
  end

  def process_data(result)
    # [{ name: 'Assessed Suppliers', data: result.map{|x| x[1]} },
    # { name: 'Assessments', data: result.map{|x| x[2]} },
    # { name: 'Monitored Suppliers', data: result.map{|x| x[3]} },
    [{ name: 'Assessed Suppliers', data: result.map { |x| x[1].to_i } },
     { name: 'Assessments', data: result.map { |x| x[2].to_i } }]
    # { name: 'Control Plans', data: result.map{|x| x[3].to_i} }]
  end

  def process_opts(result, completed = true)
    labels = result.map { |x| x[0] }

    { chart: { type: :column, styledMode: true },
      title: { text: "#{completed ? 'Completed' : 'Open'} Assessments by Process" },
      yAxis: { tickInterval: 1 },
      xAxis: { title: { text: 'Process' }, categories: labels } }
  end

  # ==============COUNTRY ANALYSIS===========
  def country_qa(options = {})
    result = country_qa_query(options).to_a
    puts "QUERY RESULT QA ====> #{result}"
    labels = result.map do |x|
               x.select do |k, _v|
                 k == 'country'
               end.map { |_k, v| v }
             end.flatten.map { |x| country_name(x) }
    puts "labels.. ==>>> #{labels}"

    data = [{
      name: 'Assessment Count',
      # data: result.map{|x| x[1]}
      data: result.map { |x| x.select { |k, _v| k == 'count_assessment' }.map { |_k, v| v } }.flatten
    }, {
      name: 'Supplier Count',
      # data: result.map{|x| x[2]}
      data: result.map { |x| x.select { |k, _v| k == 'count_supplier' }.map { |_k, v| v } }.flatten
    }]

    opts = {
      chart: {
        type: :bar,
        styledMode: true
      },
      title: { text: options[:title] },
      yAxis: { tickInterval: 1 },
      xAxis: { title: { text: 'Countries' },
               categories: labels }
    }

    { data: data, opts: opts, result: result }
  end

  def country_qa_query(options = {})
    opts = [options[:completed], options[:start_date], options[:end_date]]

    query_string_qa = "SELECT
            CASE WHEN COALESCE(country, '') = '' THEN 'Not Set' ELSE country END as country,
            COUNT(qa.id) AS count_assessments, s.id AS s_id, 0 AS count_plans
            FROM supplier_profiles s
            JOIN quality_assessments qa ON qa.supplier_profile_id = s.id
            WHERE qa.destroyed_at IS NULL AND qa.completed = ?
            AND (qa.created_at BETWEEN ? AND ?)"

    if options[:grids].present?
      query_string_qa += ' AND qa.grid IN (?)'
      puts options[:grids]
      grids = options[:grids].reject { |x| x == 'control_plan' }
      opts << grids
    end

    puts "opts now ==>> #{opts}"
    if options[:assess_types].present?
      query_string_qa += ' AND qa.assess_type IN (?)'
      assess_types = options[:assess_types].reject { |x| x == 'New Monitor' }
      opts << assess_types
    end

    puts "opts now ==>> #{opts}"
    if options[:countries].present?
      query_string_qa += ' AND country IN (?)'
      opts << options[:countries]
    end

    puts "opts now ==>> #{opts}"
    if options[:processes].present?
      query_string_qa += ' AND industrial_process IN (?)'
      opts << options[:processes]
    end

    puts "opts now ==>> #{opts}"
    if options[:prep_completed] != 'all'
      query_string_qa += ' AND qa.prep_completed = ?'
      opts << (options[:prep_completed] == 'true')
    end

    puts "opts now ==>> #{opts}"
    if options[:cap_finished] != 'all'
      cap_filter = calc_cap_interval
      query_string_qa += ' AND qa.id IN (?)'
      opts << cap_filter
    end

    # completed2
    if options[:completed2] != 'all'
      @completed2 = options[:completed2]
      query_string_qa += ' AND qa.id IN (?)'
      # cap_opts = calc_cap_interval
      opts << calc_completed_interval
    end

    puts "opts now ==>> #{opts}"
    query_string_qa += ' GROUP BY country, s_id'

    query_string_cp = ' '
    is_cp = options[:grids].blank? || options[:grids].include?('control_plan')
    if is_cp
      query_string_cp += country_cp_query(options)
      opts << options[:completed]
      opts << options[:start_date]
      opts << options[:end_date]
      puts "opts now ==>> #{opts}"
      opts << options[:countries] if options[:countries].present?
      opts << options[:processes] if options[:processes].present?
      puts "opts now ==>> #{opts}"
    end

    query_string = 'SELECT t.country, SUM(t.count_assessments) count_assessments, COUNT(DISTINCT t.s_id) count_supplier, SUM(t.count_plans) count_plans FROM '
    query_string += "(#{query_string_qa} #{is_cp ? 'UNION ALL ' + query_string_cp : ''}) as t "
    query_string += 'GROUP BY t.country ORDER BY count_assessments DESC'

    query = [query_string] + opts

    puts "\n\nQUERY.... \n\n\n#{query}"
    exec(query)
  end

  def country_cp_query(options = {})
    query_string = "SELECT
            CASE WHEN COALESCE(country, '') = '' THEN 'Not Set' ELSE country END as country,
            0 AS count_assessments, s.id AS s_id, COUNT(cp.id) AS count_plans
            FROM supplier_profiles s
            JOIN product_families pf ON s.id = pf.supplier_profile_id
            JOIN control_plans cp ON pf.id = cp.product_family_id
            WHERE cp.destroyed_at IS NULL AND cp.completed = ?
            AND (cp.created_at BETWEEN ? AND ?)"

    query_string += ' AND country in (?)' if options[:countries].present?

    query_string += ' AND industrial_process in (?)' if options[:processes].present?

    query_string += ' GROUP BY country, s_id'

    query_string
  end

  # =============PERFORMANCE ANALYSIS==========
  def performance_primary(table_name, start_date, end_date, ref_model)
    if ref_model == 'all' || ref_model.nil?
      dataset = User.joins("LEFT JOIN #{table_name} ON #{table_name}.user_id = users.id LEFT JOIN assessor_profiles ON assessor_profiles.user_id = users.id")
                    .select("CASE WHEN assessor_profiles.name IS NULL THEN users.username ELSE assessor_profiles.name END as prof_name, COUNT(#{table_name}) as count_stats")
                    .where("#{table_name}.created_at BETWEEN ? AND ?", start_date, end_date)
                    .group('prof_name').order('count_stats DESC')
    else
      dataset = User.joins("LEFT JOIN #{table_name} ON #{table_name}.user_id = users.id LEFT JOIN assessor_profiles ON assessor_profiles.user_id = users.id")
                    .select("CASE WHEN assessor_profiles.name IS NULL THEN users.username ELSE assessor_profiles.name END as prof_name, COUNT(#{table_name}) as count_stats")
                    .where("#{table_name}.created_at BETWEEN ? AND ? AND #{table_name}.ref_model = ?", start_date, end_date, ref_model)
                    .group('prof_name').order('count_stats DESC')
    end
  end

  # def performance_reverse(table_name, start_date, end_date, ref_model)
  #   case table_name
  #   when :comments  then reverse_comments(start_date, end_date, ref_model);
  #   when :likes     then reverse_likes(start_date, end_date, ref_model);
  #   when :favorites then reverse_favorites(start_date, end_date, ref_model);
  #   when :followers then reverse_followers(start_date, end_date);
  #   end
  # end

  # def reverse_comments(start_date, end_date, ref_model)
  #   # => {"goes_activity"=>72, "chapter_assessment"=>178}

  # end

  # def reverse_likes(start_date, end_date)
  #   # => {"quality_assessment"=>34, "control_plan"=>12, "chapter_question"=>65}
  #   # User.joins(:assessor_assignments, assessor_profile: :control_plans)
  #   #   .joins("LEFT JOIN quality_assessments ON assessor_assignments.ref_id = quality_assessments.id")
  #   #   .joins("LEFT JOIN chapter_assessments ON chapter_assessments.quality_assessment_id = quality_assessments.id")
  #   #   .joins("LEFT JOIN chapter_questions ON chapter_questions.chapter_assessment_id = chapter_assessments.id")
  #   #   .joins("LEFT JOIN likes ON likes.ref_id = quality_assessments.id AND likes.ref_model = 'quality_assessment'
  #   #     OR likes.ref_id = control_plans.id AND likes.ref_model = 'control_plan'
  #   #     OR likes.ref_id = chapter_questions.id AND likes.ref_model = 'chapter_question'")
  #   #   .select("assessor_profiles.name as prof_name, COUNT(likes) as count_stats")
  #   #   .where(likes: {created_at: (start_date..end_date)})
  #   #   .where.not(assessor_profiles: {name: nil})
  #   #   .group("prof_name").order("count_stats DESC")
  # end

  # def reverse_favorites(start_date, end_date)
  #   # => {"quality_assessment"=>28, "control_plan"=>10}
  #     # User.joins(:assessor_assignments, assessor_profile: :control_plans)
  #     # .joins("INNER JOIN quality_assessments ON assessor_assignments.ref_id = quality_assessments.id")
  #     # .joins("INNER JOIN favorites ON favorites.ref_id = quality_assessments.id AND favorites.ref_model = 'quality_assessment'
  #     #   OR favorites.ref_id = control_plans.id AND favorites.ref_model = 'control_plan'")
  #     # .select("assessor_profiles.name as prof_name, COUNT(DISTINCT favorites.id) as count_stats")
  #     # .where(favorites: {created_at: (start_date..end_date)})
  #     # .where(assessor_assignments: {primary: true})
  #     # .where.not(assessor_profiles: {name: nil})
  #     # .group("prof_name").order("count_stats DESC")
  # end

  # def reverse_followers(start_date, end_date)
  #   # => {"supplier_profile"=>251, "assessor_profile"=>154}
  #   AssessorProfile.joins("LEFT JOIN followers ON assessor_profiles.id = followers.ref_id")
  #     .select("assessor_profiles.name as prof_name, COUNT(followers) as count_stats")
  #     .where(followers: {created_at: (start_date..end_date), ref_model: "assessor_profile"})
  #     .where.not(assessor_profiles: {name: nil})
  #     .group("prof_name").order("count_stats DESC")
  # end

  def performance_query(table_name, options = {})
    start_date = params[:start_date] || 1.years.ago
    end_date = params[:end_date] || Time.now
    ref_model = params[table_name].present? ? (params[table_name][:ref_model] || 'all') : 'all'

    dataset = performance_primary(table_name, start_date, end_date, ref_model)
    # dataset2 = performance_reverse(table_name, start_date, end_date, ref_model)

    # hash w/ default value as array w/ first value same as key, second as 0, third as 0
    res = Hash.new { |h, k| h[k] = [k, 0] }
    # res = Hash.new{|h, k| h[k] = [k, 0, 0]}

    dataset.each { |x| res[x.prof_name][1] = x.count_stats }
    # dataset2.each{|x| res[x.prof_name][2] = x.count_stats}

    # CHANGE BELOW TO SUPPORT 2 SETS
    result = res.values

    labels = res.keys

    data = [{
      name: options.blank? ? table_name.to_s.capitalize : options[:titles][1],
      data: res.values.map { |x| x[1] }
    }]
    # , {
    #   name: options.blank? ? table_name.to_s.capitalize : options[:titles][2],
    #   data: res.values.map{|x| x[2]}
    # }

    opts = {
      chart: {
        type: :bar,
        styledMode: true
      },
      title: { text: options.blank? ? "#{table_name.to_s.capitalize} Rank" : options[:titles][0] },
      yAxis: { tickInterval: 1 },
      xAxis: { title: { text: 'Users' }, categories: labels }
    }
    { data: data, opts: opts, result: result }
  end

  def set_available_selections
    @grids_filter = QualityAssessment.all.map { |x| { label: "#{x.short_name} Assessment", value: x.grid } }.uniq
    puts "@grids ===> #{@grids}"
    @assess_types_filter = QualityAssessment.all.map(&:assess_type).uniq
    @plan_types_filter = ControlPlan.all.map(&:plan_type).uniq
    @assess_plan_types_filter = (@assess_types_filter + @plan_types_filter).uniq
    @supplier_statuses_filter = SupplierProfile.all.map { |x| x.status }.uniq.reject { |x| x.nil? }
    @results_filter = QualityAssessment.where.not(result: nil, completed: false).map(&:result).uniq.sort
    @processes_filter = SupplierProfile.all.map { |x| x.industrial_process }.uniq
    # @assessors_filter = AssessorProfile.all.select{|x| x.name.present? && x.user.present? }.map{|x| {label: x.name, value: x.id} }
    # @suppliers = SupplierProfile.all.map{|x| {label: "#{x.code} #{x.name}", value: x.id} }
  end

  def set_common_params
    p "params.... ====> #{params}"
    @start_date = params[:start_date] || '2000-01-01' # 1.years.ago.strftime('%Y-%m-%d')
    @end_date = params[:end_date] || (Time.now + 10.years).strftime('%Y-%m-%d')
    @grid = params[:grid] || 'all'
    @assess_type = params[:assess_type] || 'all'
    @prep_completed = params[:prep_completed] || 'all'
    @cap_finished = params[:cap_finished] || 'all'
    @completed2 = params[:completed2] || 'all'

    @grids = params[:grids].present? ? params[:grids].reject { |x| x == 'all' } : []
    @assess_types = params[:assess_types].present? ? params[:assess_types].reject { |x| x == 'all' } : []
    @countries = params[:countries].present? ? params[:countries].reject { |x| x == 'all' } : []
    @processes = params[:processes].present? ? params[:processes].reject { |x| x == 'all' } : []

    @options = { start_date: @start_date, end_date: @end_date, grids: @grids,
                 assess_types: @assess_types, prep_completed: @prep_completed,
                 cap_finished: @cap_finished, completed2: @completed2, countries: @countries,
                 processes: @processes }
  end

  def create_plot(data, chart_type, title, other_opts = {})
    chart_data = data

    opts = {
      chart: {
        type: chart_type,
        styledMode: true
      },
      title: { text: title }
    }
    if other_opts.present?
      if other_opts[:x_labels].present? || other_opts[:x_title].present?
        opts.merge!({
                      yAxis: { tickInterval: (other_opts[:tick_interval] || 1) },
                      xAxis: { title: { text: (other_opts[:x_title] || '') }, categories: other_opts[:x_labels] }
                    })

        chart_data = [{
          name: other_opts[:x_title],
          data: data
        }]

        if other_opts[:stacked].present?
          chart_data = []
          other_opts[:stacked].each_with_index do |s, i|
            chart_data << { name: s, data: data.map { |x| [x[0], x[i + 1].to_i] } }
            opts.merge!({ plotOptions: { chart_type => { stacking: 'normal' } } })
          end
        end
      end

      opts.merge!({ plotOptions: { chart_type => other_opts[:plot_options] } }) if other_opts[:plot_options]
    end
    puts "chart opts ===>>>> #{opts}"
    puts "chart data ====>>> #{chart_data}"

    Daru::View::Plot.new(chart_data, opts)
  end

  def calc_completed_interval
    compl_query = 'SELECT qa.id FROM quality_assessments qa'

    compl_query += if @completed2 == 'true'
                     " WHERE (qa.completed_at - qa.assess_date) <= INTERVAL '14 days'"
                   else
                     " WHERE (qa.completed_at - qa.assess_date) > INTERVAL '14 days'"
                   end

    filter = exec([compl_query]).values.flatten
    puts "completed 2-weeks filter.... =>>>>>> #{filter}"
    filter
  end

  def calc_cap_interval
    cap_query = "SELECT qa.id
                    FROM quality_assessments qa
                    JOIN caps c ON c.id = qa.cap_id
                      AND qa.assess_type = 'CAP Review'
                    JOIN quality_assessments qa2 ON qa2.id = c.ref_id
                      AND c.ref_model = 'quality_assessment'
                    "

    cap_query += if @cap_filter == 'true'
                   " WHERE (qa.assess_date - qa2.assess_date) <= INTERVAL '14 days'"
                 else
                   " WHERE (qa.assess_date - qa2.assess_date) > INTERVAL '14 days'"
                 end

    cap_filter = exec([cap_query]).values.flatten
    puts "cap_filter.... =>>>>>> #{cap_filter}"
    cap_filter
  end

  def cp_rate_group_by(data)
    data.group_by do |x|
      if x <= 20
        '0-20'
      elsif x > 20 && x <= 70
        '21-70'
      else
        '71-100'
      end
    end
  end

  def filter_by_role(data, role)
    case role
    when 'Decathlonian'
      data.where('users.username NOT LIKE ?', '%@%')
          .where(assessor_profiles: { candidate_for: [], assessor_for: [], referent_for: [] })
    when 'Assessor Candidate'
      data.where.not(assessor_profiles: { candidate_for: [] })
          .where(assessor_profiles: { assessor_for: [] })
    when 'Assessor'
      data.where.not(assessor_profiles: { assessor_for: [] })
          .where(assessor_profiles: { referent_for: [] })
    when 'Referent'
      data.where.not(assessor_profiles: { referent_for: [] })
    when 'Supplier'
      data.where('assessor_profiles.id IS NULL')
    end
  end

  def set_highcharts
    Daru::View.plotting_library = :highcharts
  end

  def country_name(abbr)
    return 'Not Set' if ['', nil].include? abbr

    IsoCountryCodes.find(abbr).name
  rescue IsoCountryCodes::UnknownCodeError
    abbr
  end

  def exec(query)
    sql = ActiveRecord::Base.send(:sanitize_sql_array, query)
    ActiveRecord::Base.connection.execute(sql)
  end

  def exec_query(query)
    sql = ActiveRecord::Base.send(:sanitize_sql_array, query)
    ActiveRecord::Base.connection.exec_query(sql)
  end

  def permission_scope
    true
  end

  def level_counts(qa_ids, level, pie_options)
    supplier_latest_levels = ChapterAssessment
                             .select('section,
        supplier_profiles.id as supplier_profile_id,
        max(chapter_assessments.id) as last_id')
                             .joins(quality_assessment: :supplier_profile)
                             .where(quality_assessment_id: qa_ids, level: [level])
                             .where.not(score: nil).group(:section, 'supplier_profiles.id')

    counts = SupplierProfile.joins(quality_assessments: :chapter_assessments)
                            .joins("INNER JOIN (#{supplier_latest_levels.to_sql}) as unique_levels ON chapter_assessments.id = unique_levels.last_id")
                            .where.not(chapter_assessments: { score: ['OK', 'Not Applicable'] })
                            .group('unique_levels.section').count

    section_renamed = counts.map { |k, v| [ChapterAssessment.new(section: k).section_name, v] }

    create_plot(section_renamed, :pie, "Sections with #{level}", pie_options)
  end
end
