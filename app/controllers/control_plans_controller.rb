class ControlPlansController < BaseController
  before_action :set_supplier
  before_action :set_pf

  def product_families
    @product_families = @supplier.product_families.where(destroyed_at: nil)
  end

  def update_product_family; end

  def process_steps
    @steps = @pf.process_steps.where(destroyed_at: nil).order(:step_number)
  end

  def update_process_step
    @steps = @pf.process_steps.where(destroyed_at: nil)
    @step = @steps.find(params[:ps_id])
  end

  private

  def set_supplier
    @supplier = SupplierProfile.find_by(code: params[:supplier_code])
  end

  def set_pf
    @pf = ProductFamily.find_by(id: params[:pf_id])
  end

  def permission_scope
    true
  end
end
