# frozen_string_literal: false

# Native URL: Module to generate native url's
module NativeUrl
  KEY = '37057b62a375b99abaf0f73d381cb5'.freeze

  class << self
    def generate(path, user = User.new, params = {})
      "#{ApplicationRecord::DOMAIN}/native/#{path}?key=#{KEY}&id=#{user.id}&#{params.to_query}"
    end
  end
end
