class Search
  attr_accessor :params, :s_params, :qa_params, :label, :results, :current_user

  ASSESSOR_PARAMS = %w[name handle city].freeze
  SUPPLIER_PARAMS = %w[name contact_name city code].freeze
  COUNTRY_QUERY = 'OR country ILIKE :country'.freeze
  ASSESSOR_QUERY = ASSESSOR_PARAMS.map { |x| "#{x} ILIKE :query" }.join(' OR ')
  SUPPLIER_QUERY = SUPPLIER_PARAMS.map { |x| "#{x} ILIKE :query" }.join(' OR ')
  QUERIES = { 'AssessorProfile' => "#{ASSESSOR_QUERY} #{COUNTRY_QUERY}",
              'SupplierProfile' => "#{SUPPLIER_QUERY} #{COUNTRY_QUERY}" }.freeze

  def initialize(params)
    @params = params
    @current_user = params[:current_user]
    @s_params = params[:supplier].blank? ? {} : supplier_params
    @qa_params = params[:assessment].blank? ? {} : assessment_params
    @cp_search = (@qa_params[:grid] || []).delete('control_plan')
    @query = params[:input]
    @label = ''
    @results = []
  end

  def search_by_parameters!
    reduce_supplier_params! unless s_params.blank?
    add_supplier_label! unless s_params.blank?
    reduce_assessment_params! unless qa_params.blank?
    add_assessment_label! unless qa_params.blank?
    search_suppliers!
    @results
  end

  def search_by_input!
    @country_value = fetch_country_value
    search_permutations!
    @results
  end

  private

  def search_permutations!
    permutations = params[:input].split.permutation.map { |x| x.join(' ') }

    [AssessorProfile, SupplierProfile].each do |klass|
      @results += permutations.inject(klass) do |res, permutation| # evaluate each query, and chain result
        permutation = permutation.split.join('%') # fuzzy search boundaries and anything in between
        if res == klass # 1st iteration
          new_res = res.where(QUERIES[klass.to_s], query: "%#{permutation}%", country: @country_value)
        elsif res.class.to_s == "#{klass}::ActiveRecord_Relation" # all other iterations
          new_res = res.or(klass.where(QUERIES[klass.to_s], query: "%#{permutation}%", country: @country_value))
        end
        new_res
      end
    end
  end

  def search_suppliers!
    # puts @s_params, @qa_params, @cp_search
    @results = SupplierProfile.includes(:quality_assessments, product_families: :control_plans)
    @results = @results.all
    @results = @results.where(supplier_profiles: @s_params) unless @s_params.blank?
    @results = @results.where(quality_assessments: @qa_params) unless @qa_params.blank?
    @results = @results.where.not(control_plans: { id: nil }) unless @cp_search.blank?
  end

  def add_supplier_label!
    types = []
    supp_types = []

    types << 'Active ' if s_params[:status].present? && s_params[:status].include?('Active Supplier')
    types << 'Opening ' if s_params[:status].present? && s_params[:status].include?('Opening Supplier')
    supp_types << 'Finished Goods ' if s_params[:finished_goods_supplier].present?
    supp_types << 'Component ' if s_params[:component_supplier].present?
    supp_types << 'Partner ' if s_params[:partner].present?

    @label += types.join('& ') if types.length > 0
    @label += 'Suppliers'
    @label += " working on #{s_params[:industrial_process]}" if s_params[:industrial_process].present?

    @label += ' who are ' if supp_types.present?
    @label += supp_types.join('& ') if supp_types.length > 0
    @label += ' Suppliers' if s_params[:finished_goods_supplier] || s_params[:component_supplier] || s_params[:partner]
    @label += " from #{s_params[:country]}" if s_params[:country].present?
    @label += ' from All Countries,' if s_params[:country].blank?
  end

  def add_assessment_label!
    grids = (qa_params[:grid] || []).map { |x| QualityAssessment::SHORT_NAMES[x] }
    @label += ' with ' unless [qa_params[:result], qa_params[:assess_type]] == [nil, nil]
    @label += join_if_present(qa_params[:result], suffix: ' result, ')
    @label += join_if_present(qa_params[:assess_type], suffix: ' type ')
    @label += join_if_present(grids, joiner: ',', prefix: 'in ', suffix: ' ')
    @label += 'assessments'
  end

  def reduce_supplier_params!
    s_params[:status] = split_if_string(s_params[:status])
    s_params[:finished_goods_supplier] = true_or_nil(s_params[:finished_goods_supplier])
    s_params[:component_supplier] = true_or_nil(s_params[:component_supplier])
    s_params[:partner] = true_or_nil(s_params[:partner])
    s_params[:partner] = current_user.supplier? || s_params[:partner]

    s_params.reject! { |_, v| v.blank? }
  end

  def reduce_assessment_params!
    qa_params[:assess_type] = split_if_string(qa_params[:assess_type])
    qa_params[:grid] = split_if_string(qa_params[:grid])
    qa_params[:result] = split_if_string(qa_params[:result])

    qa_params.reject! { |_, v| v.blank? }
  end

  def supplier_params
    return params[:supplier] if params.is_a?(Hash)
    params.require(:supplier).permit(:country, :industrial_process, :status,
                                     :finished_goods_supplier, :component_supplier,
                                     :partner).to_h
  end

  def assessment_params
    return params[:assessment] if params.is_a?(Hash)
    params.require(:assessment).permit(:result, :grid, :assess_type,
                                       result: [], grid: [], assess_type: []).to_h
  end

  def true_or_nil(value = nil)
    value == 'true' ? true : nil
  end

  def split_if_string(value = nil)
    value.is_a?(String) ? value.split(',') : value
  end

  def join_if_present(values = [], joiner: '/', prefix: '', suffix: '')
    values.present? ? "#{prefix}#{values.join(joiner)}#{suffix}" : ''
  end

  def fetch_country_value
    IsoCountryCodes.search_by_name(@params[:input]).first.alpha2
  rescue StandardError
    @params[:input]
  end
end
