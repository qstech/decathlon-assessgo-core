module ApiBugReporter
  extend ActiveSupport::Concern

  def not_found(exception)
    render json: { code: 500, error: exception.message }, status: :not_found
  end

  def internal_server_error(exception)
    url = NativeUrl.generate('bug-tracker/form', current_user || User.new)

    if exception_valid(exception)
      generate_bug(exception)
      url += "&bug_id=#{@bug.id}"
      response = { code: 500, error: 'Internal Server Error', msg: 'Internal Server Error', webview: url }
      render json: response, status: :internal_server_error
    else
      response = { code: 200, msg: 'OK', webview: '' }
      render json: response
    end
  end

  def self.included(base)
    super

    base.send :rescue_from, StandardError, with: :internal_server_error
    base.send :rescue_from, ActiveRecord::RecordNotFound, with: :not_found
  end

  private

  def exception_valid(exception)
    response = true
    BugReport.invalid_messages.each do |msg|
      response = false if exception.message.to_s.include?(msg) || exception.class.to_s.include?(msg)
    end
    response
  end

  def generate_bug(exception)
    @bug = BugReport.create_from_exception(exception)
    @bug.app = request.headers['app'] || :unknown_app
    @bug.request_path = request.url if request.present?
    @bug.controller_action = [params[:controller], params[:action]].join('#')
    @bug.request_params = params.to_unsafe_h if params.present?
    if current_user
      @bug.user_params = { user_id: current_user.id, username: current_user.username,
                           email: current_user.email }
    end
    @bug.feature = detect_feature
    @bug.supplier_code = detect_supplier
    @bug.save
  end

  def detect_feature
    if params[:controller].include?('control_plan') || params[:name] == 'control_plans'
      :CP
    elsif params[:name] == 'quality_assessments' && @object.present?
      @object.feature_name.to_sym
    elsif params[:controller].include?('quality_assessment') && params[:ref_model]
      QualityAssessment.new(grid: params[:ref_model]).feature_name.to_sym
    elsif params[:controller].include?('quality_assessment') && !params[:action].in?(%w[single_chapter
                                                                                        historical_level invitations supplier_info])
      QualityAssessment.find_by(id: params[:id]).feature_name.to_sym
    elsif params[:action] == 'single_chapter'
      QualityAssessment.joins(:chapter_assessments).find_by(chapter_assessments: { id: params[:id] }).feature_name.to_sym
    else
      :OTHER
    end
  end

  def detect_supplier
    if params[:id] && params[:controller].include?('quality_assessment') && !params[:action].in?(%w[single_chapter
                                                                                                    historical_level invitations supplier_info])
      QualityAssessment.find_by(id: params[:id]).supplier_profile.code
    elsif params[:controller].include?('control_plan') && params[:cp_id]
      ControlPlan.find_by(id: params[:cp_id]).supplier_profile.code
    end
  rescue StandardError
    nil
  end
end
