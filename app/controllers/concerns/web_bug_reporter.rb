module WebBugReporter
  extend ActiveSupport::Concern

  def internal_server_error(exception)
    generate_bug(exception)
    url = "#{ApplicationRecord::DOMAIN}/bug-reports/form?&bug_id=#{@bug.id}"

    redirect_to url
  end

  def self.included(base)
    super
    base.send :rescue_from, StandardError, with: :internal_server_error
  end

  private

  def generate_bug(exception)
    @bug = BugReport.create_from_exception(exception)
    @bug.app = :web
    @bug.request_path = request.url if request.present?
    @bug.controller_action = [params[:controller], params[:action]].join('#')
    @bug.request_params = params.to_unsafe_h if params.present?
    if current_user
      @bug.user_params = { user_id: current_user.id, username: current_user.username,
                           email: current_user.email }
    end
    @bug.feature = detect_feature
    @bug.supplier_code = detect_supplier
    @bug.save
  end

  def detect_feature
    if params[:controller].include?('statistics')
      :STATS
    elsif params[:controller].include?('control_plan')
      :CP
    elsif params[:controller].include?('reports') && @qa.present?
      @qa.short_name.to_sym
    elsif params[:controller].include?('reports') && @cp.present?
      :CP
    else
      :OTHER
    end
  end

  def detect_supplier
    if @qa.present?
      @qa.supplier_profile.code
    elsif @cp.present?
      @cp.supplier_profile.code
    end
  rescue StandardError
    nil
  end
end
