class SessionsController < Devise::SessionsController
  def new
    super
    @no_sidebar = true
  end

  def create
    user = User.find_for_authentication(username: params[:user][:username])
    if user.blank?
      params[:external] = 'true'
      flash[:alert] = 'Not Assess Go User!'
      render :new
    elsif Rails.env == 'development'
      sign_in user
      redirect_to root_path
    elsif user.valid_password?(params[:user][:password])
      params[:external] = 'true'
      sign_in user
      redirect_to root_path
    elsif user.present? && !user.username.include?('@')
      params[:external] = 'true'
      flash[:alert] = 'Decathlon Users remember to login w/ profile'
      render :new
    else
      params[:external] = 'true'
      flash[:alert] = 'Password is incorrect.'
      render :new
    end
  end

  def permission_scope
    true
  end
end
