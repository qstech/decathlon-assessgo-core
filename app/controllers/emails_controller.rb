class EmailsController < BaseController
  before_action :set_active

  def email_system
    @letters = Letter.includes(:mail_bag).where(delivered_at: (30.days.ago..Time.now))
  end

  def email_send_out
    @letters = Letter.all
  end

  def letter_send_out_new
    @letter = Letter.new
    @mail_bags = MailBag.all
    @users = User.all
    if params[:template]
      @mail_bag = MailBag.find_by(id: params[:template])
      @template_subject = @mail_bag.subject
      @template_body = @mail_bag.body
    end
  end

  def letter_send_out_create
    @letter = Letter.create(letter_params)
    @recipients = @letter.to
    @recipients.each do |user|
      UserMailer.send_out_letter(user, @letter).deliver_later
    end
    redrect_to email_system_path if @letter.save
  end

  def email_send_out_show
    @letter = Letter.find_by(id: params[:id])
  end

  def manage_templates
    @mail_bags = MailBag.all
  end

  def mail_bag_new
    @mail_bag = MailBag.new
  end

  def mail_bag_create
    @mail_bag = MailBag.create(mail_bag_params)
    redirect_to manage_templates_path if @mail_bag.save
  end

  def mail_bag_edit
    @mail_bag = MailBag.find_by(id: params[:id])
  end

  def mail_bag_update
    @mail_bag = MailBag.find_by(id: params[:id])
    p @mail_bag
    @mail_bag.update(mail_bag_params)
    redirect_to manage_templates_path if @mail_bag.save
  end

  def view_email; end

  def statistics
    @letters = Letter.all.order(created_at: :desc)
    week_base = (0..6).to_a.reverse.map { |x| [x.days.ago.to_date.to_time(:utc), 0] }.to_h
    @this_week = week_base.merge(AuthenticationToken.where(created_at: (6.days.ago..Time.now)).group("DATE_TRUNC('day', created_at)").count)
    last_base = (7..13).to_a.reverse.map { |x| [x.days.ago.to_date.to_time(:utc), 0] }.to_h
    @last_week = last_base.merge(AuthenticationToken.where(created_at: (13.days.ago..7.days.ago)).group("DATE_TRUNC('day', created_at)").count)
    @days = week_base.keys.sort.map { |x| x.strftime('%A') }
    @this_week = @this_week.values
    @last_week = @last_week.values
  end

  def statistics_spy
    @letter = Letter.find_by(id: params[:id])
  end

  private

  def permission_scope
    current_user.can_manage?('emails')
  end

  def mail_bag_params
    params.require(:mail_bag).permit(:name, :subject, :body, :trigger, :tag, audience: [], cc: [], bcc: [])
  end

  def letter_params
    params.require(:letter).permit(:to, :mail_bag_id, :subject, :cc, :body, :bcc)
  end

  def set_active
    @active = 'email'
  end
end
