class ReportsController < BaseController
  before_action :set_qa,
                only: %i[overall_comments finish_qa update_report update_ap update_go update_ca update_chapter download
                         download_excel download_cap_review update_cap upload_cap_review update_dormitory]
  before_action :set_cp, only: %i[update_cp_report update_plan_item download_cp]
  skip_before_action :check_permission, only: %i[reports my_reports]

  def reports
    @active = 'reports'
  end


  def my_reports
    report_for = current_user.assessor? ? current_user : current_user.profile

    @quality_assessments = report_for.quality_assessments.where(destroyed_at: nil)
                                     .order(updated_at: :desc).order(:completed)
    @control_plans = report_for.control_plans.where(destroyed_at: nil)
                               .order(updated_at: :desc).order(:completed)

    @reports = @quality_assessments + @control_plans
  end

  def update_dormitory
    @survey_questions = @qa.survey_questions.includes(form_question: :form_section)
                           .order('form_questions.priority ASC, form_sections.priority ASC')
    @sections = @survey_questions.group_by(&:form_section)
    @form = @sections.keys.first.form
  end


  def update_report
    @related = @qa.cap.related_activity if @qa.has_related?
  end

  def update_cp_report
    @plan_items = @cp.plan_items.order(:id)
  end

  def update_plan_item
    @pi = @cp.plan_items.find(params[:pi_id])
    @other_pis = @cp.plan_items.order(:id).reject { |x| x == @pi }
    @ps = @pi.process_step
    @next_pi = @other_pis.reject { |x| x.id < @pi.id }.first
    @prev_pi = @other_pis.reject { |x| x.id > @pi.id }.last
    # raise
  end

  def finish_qa; end

  def overall_comments; end

  def update_cap
    @cap_index = params[:cap_index].to_i || 0
  end

  def update_ap
    @prep_questions = @qa.prep_questions_form(params[:respondant] || 'assessor')
  end

  def update_go
    @go = @qa.assessment_day_form
    @suppliers = @qa.supplier_profile.users
    @assessors = AssessorProfile.all
  end

  def update_ca
    @chapters = PageComponent.qa_chapter_set(@qa)

    if @qa.assess_type == 'CAP Close'
      ref = @qa.cap.ref
      @ref_chapters = PageComponent.qa_chapter_set(ref)
      @ref_chapters.each do |chapter|
        chapter[:children].each do |x|
          x[:disable_editing] = true
          x[:cap_id] = @qa.cap.id
          x[:cap_qa_id] = @qa.id
          x[:annual_chapter] = true
        end
      end

      @chapters.each_with_index do |chapter, _idx|
        # Re-assigning ref_chapter's chapter to CAP's chapter if present
        ref_ch = @ref_chapters.find { |x| chapter[:guideline_tag] == x[:guideline_tag] }
        next if ref_ch.nil?

        ref_idy = @ref_chapters.index(ref_ch)
        chapter[:children].each do |sec|
          ref_sec = ref_ch[:children].find { |x| sec[:guideline_tag] == x[:guideline_tag] }
          next if ref_sec.nil?

          ref_idx = ref_ch[:children].index(ref_sec)
          sec[:children].each_with_index do |lev, _i|
            ref_lev = ref_sec[:children].find { |x| lev[:guideline_tag] == x[:guideline_tag] }
            next if ref_lev.nil?

            ref_id = ref_sec[:children].index(ref_lev)

            ref_lev[:disable_editing] = true
            next unless lev[:result].in?(%w[OK NOK])

            lev[:disable_editing] = false
            @ref_chapters[ref_idy][:children][ref_idx][:children][ref_id] = lev
          end
        end
      end
    end

    @perc = (@qa.chapter_questions.where.not(result: nil).count.to_f / @qa.chapter_questions.count * 100).ceil(0).to_i
    @perc += 1 if @perc == 99
    # raise
  end

  def update_chapter
    @last_attempt = @qa.last_attempt
    @ca = @qa.chapter_assessments.where(chapter_tag: params[:chapter]).first
    if @ca.nil?
      @cap = @qa.cap.ref
      @ca = @cap.chapter_assessments.where(chapter_tag: params[:chapter]).first
    end

    @gl = @ca.guideline
    @gl_h = @qa.dpr? ? @gl.level_h : @gl.to_h

    if @qa.dpr?
      @cqs = @qa.chapter_questions.with_images.where(chapter_tag: params[:chapter])
      other_chapters = []
    else
      @cqs = @ca.chapter_questions.with_images
      @section = Guideline.find_by(guideline_tag: @ca.section, grid_version_id: @qa.grid_version_id)
      other_chapters = @ca.siblings.where.not(id: @ca.id).order(:chapter_tag)
    end

    @next_ca = other_chapters.reject { |x| x.chapter_tag < @ca.chapter_tag }.first
    @prev_ca = other_chapters.reject { |x| x.chapter_tag > @ca.chapter_tag }.last

    ChapterQuestion.find(params[:cq_id]).update(related_assessment_id: params[:related_assessment_id]) if params[:cq_id]
  end

  def upload_cap_review; end

  def inprogress
    @active = 'reports'

    if params[:id].present?
      if params[:type] == 'Control Plan'
        h = { scene: "cp_id=#{params[:id]}" }
        h[:page] = 'members/control_plan/review'
      else
        h = { scene: "id=#{params[:id]}" }
        h[:page] = 'members/assessment/pre-confirmation'
      end
      render json: WxAuthentication.mp_qrcode(h)
    else
      fetch_reports(completed: false)
    end
  end

  def completed
    @active = 'reports'

    if params[:id].present?
      h = { scene: "id=#{params[:id]}" }
      h[:page] = if params[:type] == 'Control Plan'
                   'members/control_plan/report'
                 else
                   'members/assessment/report'
                 end
      render json: WxAuthentication.mp_qrcode(h)
    else
      fetch_reports(completed: true)
    end
  end

  def download
    if @qa.report_state == 'PROCESSING' && params[:again].blank?
      render json: { msg: 'Received & Processing PDF' }
    elsif @qa.report.attachment.blank? || params[:again] == 'true' || params[:full] == 'false'
      ReportsWorker.set(queue: :reports).perform_async(@qa.id, 'QualityAssessment', 'pdf', params[:full] == 'true')
      render json: { msg: 'Processing PDF' }
    else
      render json: { available: true, url: rails_blob_path(@qa.report, disposition: 'attachment'),
                     report_date: @qa.report_date.strftime('%Y-%m-%d'), updated_at: @qa.updated_at.strftime('%Y-%m-%d') }
    end
  end

  def download_excel
    if @qa.excel_state == 'PROCESSING' && params[:again].blank?
      render json: { msg: 'Received & Processing XLSX' }
    elsif @qa.excel_report.attachment.blank? || params[:again] == 'true'
      ReportsWorker.set(queue: :reports).perform_async(@qa.id, 'QualityAssessment', 'xls')
      render json: { msg: 'Processing Excel' }
    else
      render json: { available: true, url: rails_blob_path(@qa.excel_report, disposition: 'attachment'),
                     report_date: @qa.excel_date.strftime('%Y-%m-%d'), updated_at: @qa.updated_at.strftime('%Y-%m-%d') }
    end
  end

  def download_cap_review
    if @qa.excel_state == 'PROCESSING' && params[:again].blank?
      render json: { msg: 'Received & Processing XLSX' }
    elsif @qa.excel_cap_review_empty.attachment.blank? || params[:again] == 'true'
      ReportsWorker.set(queue: :reports).perform_async(@qa.id, 'QualityAssessment', 'cap')
      render json: { msg: 'Processing Excel' }
    else
      render json: { available: true, url: rails_blob_path(@qa.excel_cap_review_empty, disposition: 'attachment'),
                     report_date: @qa.excel_date, updated_at: @qa.updated_at.strftime('%Y-%m-%d') }
    end
  end

  def download_cp
    if @cp.report_state == 'PROCESSING' && params[:again].blank?
      render json: { msg: 'Received & Processing PDF' }
    elsif @cp.report.attachment.blank? || params[:again] == 'true'
      ReportsWorker.set(queue: :reports).perform_async(@cp.id, 'ControlPlan', 'pdf')
      render json: { msg: 'Processing PDF' }
    else
      render json: { available: true, url: rails_blob_path(@cp.report, disposition: 'attachment'),
                     report_date: @cp.report_date.strftime('%Y-%m-%d'), updated_at: @cp.updated_at.strftime('%Y-%m-%d') }
    end
  end

  def generate_cap_review
    qa = QualityAssessment.find_by(slug: params[:slug])
    Cap.create_from_xls(qa)
    render json: { code: 200, msg: 'SUCCESS' }
  end

  private

  def fetch_reports(completed: nil)
    quality_assessments = QualityAssessment.includes(:supplier_profile,
                                                     assessor_assignments: { user: %i[assessor_profile
                                                                                      supplier_profile supplier] })
                                           .where(completed: completed, destroyed_at: nil).map do |x|
      primary_assessor = x.assessor_assignments.sort_by { |x| x.primary ? 0 : 1 }.map(&:user).map(&:username).first
      { id: x.id, code: x.supplier_profile.code, supplier: x.supplier_profile.name, name: primary_assessor,
        date: x.updated_at.strftime('%Y-%m-%d'), color: x.color, label: "#{x.short_name || 'QA'} - #{x.assess_type}",
        type: (x.grid.present? ? x.grid.titleize : 'Quality Assessment'), slug: x.slug, object: x }
    end
    control_plans = ControlPlan.include_supplier.includes(:assessor_profile)
                               .where(completed: completed, destroyed_at: nil).map do |x|
      { id: x.id, code: x.supplier_profile.code,
        supplier: x.supplier_profile.name, name: x.username,
        date: x.updated_at.strftime('%Y-%m-%d'), type: 'Control Plan',
        color: '#A241C8', label: "CP - #{x.plan_type}", slug: x.slug, object: x }
    end
    @reports = (quality_assessments + control_plans).sort_by { |x| x[:date] }.reverse
  end

  def permission_scope
    current_user.can_manage?('my-reports') || current_user.can_manage?('reports') ||
      current_user.can_manage?('own-reports') || current_user.can_manage?('view-reports')
  end

  def set_qa
    @qa = QualityAssessment.includes(:supplier_profile).find_by(slug: params[:slug])
  end

  def set_cp
    @cp = ControlPlan.includes(product_family: :supplier_profile).find_by(slug: params[:slug])
  end
end
