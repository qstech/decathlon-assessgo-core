class Api::V1::PagesController < Api::V1::BaseController
  # ONLY PUBLIC INFO
  before_action :set_model, only: %i[form guidelines]
  before_action :authenticate_user!
  skip_before_action :authenticate_user!, only: %i[sign_in login forget_password coming_soon loading]

  def form
    render json: @model.form
  end

  def guidelines
    render json: @model.fetch_guidelines(params)
  end

  def logs
    render json: @response
  end

  def sign_in
    render json: @response
  end

  def forget_password
    render json: @response
  end

  def components
    render json: @response
  end

  def coming_soon
    render json: @response
  end

  def loading
    render json: @response
  end

  def supplier_info
    render json: @response
  end
end
