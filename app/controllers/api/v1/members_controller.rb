class Api::V1::MembersController < Api::V1::BaseController
  def home
    if Rails.env != 'development'
      last_change = ChangeLog.last
      recent_changes = last_change.standard_hash
      recent_changes[:title] = 'New Version'
      view_tag = current_user.id.to_s
      if last_change.viewed_by.include?(view_tag)
        recent_changes[:show] = false
      else
        recent_changes[:show] = true
        last_change.viewed_by << view_tag
        last_change.save
      end
    end

    @badge_number = current_user.qa_counts
    @cp_counts = current_user.cp_counts
    set_grids!

    hashy = {
      recentChanges: recent_changes,
      badges: @response[:active_icons].map { |h| h[:badgeNumber] },
      pendingApplication: current_user.pending_application,
      locales: %w[en zh-CN fr it vi],
      locale_names: ['English', '中文', 'Française', 'Italiano', 'Tiếng Việt'],
      localeSet: {
        values: %w[en zh-CN fr it vi],
        labels: {
          'en' => 'English',
          'zh-CN' => '中文',
          'fr' => 'Française',
          'it' => 'Italiano',
          'vi' => 'Tiếng Việt'
        }
      }
    }

    hashy[:cp_web] = NativeUrl.generate('control_plan/index', current_user)
    hashy[:bug_form_url] = NativeUrl.generate('bug-tracker/form', current_user)
    @response.merge!(hashy)
    render json: @response
  end

  def manual
    hashy = {}
    dir = '/images/directions'
    hashy[:modal] = {
      assess: ["#{dir}/Assess1.jpg", "#{dir}/Assess2.jpg", "#{dir}/Assess3.jpg"],
      goes: ["#{dir}/Goes1.jpg", "#{dir}/Goes2.jpg"],
      for: ["#{dir}/For1.jpg", "#{dir}/For2.jpg"],
      performance: ["#{dir}/Performance1.jpg", "#{dir}/Performance2.jpg", "#{dir}/Performance3.jpg"]
    }
    @response.merge!(hashy)
    render json: @response
  end

  def activity
    # need to create GoesActivity seeds
    # Decathlon global: all not completed QA assessments, CP ...
    # My network: all activities created by me or inviting me or by someone in my network, need an Algorithm
    network = current_user.following_ids
    goes = GoesActivity.all unless current_user.supplier?
    goes = GoesActivity.for_suppliers if current_user.supplier?

    world_wide = goes.where(destroyed_at: nil).order(created_at: :desc).limit(10).map do |x|
      PageComponent.activity_card(x)
    end
    hashy = {
      header: {
        bgColor: 'blue',
        icon: '/images/tabbar/goes_sel.png',
        headerText: 'Go For Excellence'
      },
      worldWide: world_wide,
      activity: world_wide,
      myNetwork: goes.where(user_id: network, destroyed_at: nil).order(created_at: :desc).limit(10).map do |x|
                   PageComponent.activity_card(x)
                 end
    }
    @response.merge!(hashy)
    render json: @response
  end

  def activity_more
    goes = GoesActivity
    goes = GoesActivity.for_suppliers if current_user.supplier?

    if params[:type] == 'network'
      network = current_user.following_ids
      cards = goes.where(user_id: network,
                         destroyed_at: nil).order(created_at: :desc).limit(10).offset(params[:last_record]).map do |x|
        PageComponent.activity_card(x)
      end
    elsif params[:type] == 'world_wide'
      cards = goes.where(destroyed_at: nil).order(created_at: :desc).limit(10).offset(params[:last_record]).map do |x|
        PageComponent.activity_card(x)
      end
    end

    render json: { activities: cards }
  end

  def activity_show
    activity = GoesActivity.find(params[:id].to_i)
    comments = Comment.where(ref_model: 'goes_activity', ref_id: params[:id].to_i).order(created_at: :desc)
    hashy = {
      activity: PageComponent.activity_show_card(activity, current_user),
      comments: comments.map { |x| PageComponent.comment_card(x, current_user) },
      web_url: ''
    }
    if activity.ref_model == 'control_plan'
      hashy[:web_url] =
        NativeUrl.generate('control_plan/findings', current_user, cp_id: activity.ref_id)
    end
    ref = activity.ref

    hashy[:caps] = []

    if ref.cap_id.present?
      last = ref.cap.ref
      hashy[:caps] << {
        url: last.mp_path(:review),
        type: last.assess_type,
        date: last.assess_date.strftime('%Y-%m-%d')
      }
    end

    hashy[:caps] += ref.related_caps.map do |cap|
      next(nil) if cap.assessment.nil?

      {
        url: cap.mp_path,
        type: cap.cap_type,
        date: cap.cap_date.strftime('%Y-%m-%d')
      }
    end
    hashy[:caps].delete(nil)
    hashy[:caps].uniq!

    @response.merge!(hashy)
    render json: @response
  end

  def search_form
    recent_searches = current_user.search_histories.recent.map { |x| x.query }
    assess_types = QualityAssessment.group(:assess_type).count.keys.map { |x| { name: x, value: x } }
    res = {
      recent_searches: recent_searches,
      country_index: 0,
      country_names: ['All'] + SupplierProfile.country_names,
      country_values: [nil] + SupplierProfile.country_values,
      industrial_processes: ['All'] + SupplierProfile.industrial_processes,
      industrial_process_index: 0,
      assess_grids: [
        { name: 'QA', value: 'quality_assessment' },
        { name: 'OPEX', value: 'opex_assessment' },
        { name: 'SSE', value: 'sse_assessment' },
        { name: 'HRP', value: 'hrp_assessment' },
        { name: 'ENV', value: 'env_assessment' },
        { name: 'SCM', value: 'scm_assessment' },
        { name: 'PUR', value: 'pur_assessment' },
        { name: 'DPR', value: 'dpr_assessment' },
        { name: 'CP', value: 'control_plan' }
      ],
      assess_types: assess_types
    }
    @response.merge!(res)
    render json: @response
  end

  def me
    @me = true
    @response.merge!({ country_names: User.country_names, country_values: User.country_values })
    if current_user.assessor?
      params[:id] = current_user.profile.id
    else
      params[:supplierid] = current_user.profile.id
    end
    @response.merge!({ t: t('api.v1.members.members_show') })
    members_show
  end

  def members_show
    @me ||= false
    skip_grids = ['x']
    skip_grids = ['hrp_assessment'] if current_user.supplier? && !@me

    if params[:id]
      member = AssessorProfile.find(params[:id])
      user = member.user

      header = PageComponent.user_performance_header(current_user, member)
      user_data = PageComponent.user_performance_detail(current_user, member)
      followed = current_user.following?(member)
      if user.present?
        qa_activities = user.quality_assessments.includes(:goes_activities).valid.complete
                            .where.not(grid: skip_grids).order(created_at: :desc)
                            .map { |x| x.goes_activities.min_by(&:created_at) }.reject(&:nil?)
        cp_activities = user.control_plans.includes(:goes_activities)
                            .active.complete.order(created_at: :desc)
                            .map { |x| x.goes_activities.min_by(&:created_at) }.reject(&:nil?)
        activities = qa_activities + cp_activities
        favs = user.latest_activity_for_favorites
        if current_user.supplier? && ((params[:supplierid] || 0) != current_user.profile.id)
          favs.reject! do |x|
            x.ref.grid == 'hrp_assessment'
          end
        end
        following = user.following
        validations = user.validations
      else
        qa_activities = []
        cp_activities = []
        activities = []
        favs = []
        following = []
        validations = []
      end
    elsif params[:supplierid]
      member = SupplierProfile.find(params[:supplierid])
      is_supplier = member.all_users.map(&:id).include?(current_user.id)

      header = PageComponent.supplier_performance_header(member)
      user_data = PageComponent.supplier_performance_detail(current_user, member)

      active_member = current_user.profile == member ? current_user : member
      qa_activities = active_member.quality_assessments
      cp_activities = active_member.control_plans

      qa_activities = member.quality_assessments.includes(:goes_activities).valid.complete
                            .where.not(grid: skip_grids)
                            .order(created_at: :desc)
                            .map { |x| x.goes_activities.min_by(&:created_at) }.reject(&:nil?)
      cp_activities = member.control_plans.includes(:goes_activities).active.complete
                            .order(created_at: :desc)
                            .map { |x| x.goes_activities.min_by(&:created_at) }.reject(&:nil?)

      activities = member.goes_activities.limit(10).order(created_at: :desc)
      followed = current_user.following?(member)
      validations = []
      if is_supplier
        favs = current_user.latest_activity_for_favorites
        following = current_user.following
      elsif member.user_id.present?
        favs = member.user.latest_activity_for_favorites
        favs.reject! { |x| x.ref.grid == 'hrp_assessment' } if current_user.supplier?
        following = member.user.following
      else
        favs = []
        following = []
      end
    end

    member_hash = {
      historyLoading: false,
      header: header,
      profile: {
        me_id: current_user.id,
        profile_id: member.id,
        profile_type: member.model_name.singular,
        user: user_data,
        # replace with default wx avatar
        following: following.map { |x| x.to_h } || [],
        history: qa_activities.map { |x| PageComponent.history_card(x, current_user.id) }.uniq.reject(&:blank?),
        control_plans: cp_activities.map { |x| PageComponent.history_card(x, current_user.id) }.uniq.reject(&:blank?),
        favorites: favs.map { |x| PageComponent.history_card(x, current_user.id).except(:created) }
      }
    }
    if current_user.supplier?
      member_hash[:profile][:favorites].reject! do |x|
        x[:extraDetails][:grid] == 'hrp_assessment'
      end
    end
    member_hash[:profile][:validations] = validations

    if current_user != member
      trans = 'api.v1.members.members_show'
      member_hash[:followButtonText]  = (followed ? "#{t("#{trans}.unfollow")} -" : "#{t("#{trans}.follow")} +")
      member_hash[:followButtonColor] = (followed ? 'red' : 'rgba(50, 162, 237, 1.00)')
      member_hash[:updatedName] = member_hash[:profile][:user][:name]
      member_hash[:updatedCity] = member_hash[:profile][:user][:city]
    else
      member_hash[:followButtonText]  = ''
      member_hash[:followButtonColor] = ''
      member_hash[:updatedName] = ''
      member_hash[:updatedCity] = ''
    end
    path = 'assessor_application/application'
    path = 'assessor_application/application_status' if member.is_a?(AssessorProfile) && member.candidate_for.present?
    member_hash[:web_url] = NativeUrl.generate(path, current_user, user_id: member.user_id)

    @response.merge!(member_hash)
    clean_response!
    render json: @response
  end

  def members_show_history
    goes = GoesActivity
    goes = GoesActivity.no_hrp if current_user.supplier?
    activities = goes.where(user_id: params[:id]).limit(5).offset(params[:last_record]).order('created_at DESC')
    history = activities.map { |x| PageComponent.history_card(x, current_user.id) }.uniq
    render json: history
  end

  def members_detail
    skip_grids = ['x']
    skip_grids = ['hrp_assessment'] if current_user.supplier? && (params[:supplierid] != current_user.profile.id.to_s)
    if params[:id]
      supplier = false
      profile = AssessorProfile.find(params[:id])
      member = profile.user
      @self = member == current_user
      header = PageComponent.user_performance_header(current_user, profile)
      assessments = member.quality_assessments.valid.where.not(grid: skip_grids)
      suppliers = member.suppliers
    elsif params[:supplierid]
      supplier = true
      profile = SupplierProfile.find(params[:supplierid])
      member = profile.user_id.nil? ? nil : profile.user
      @self = member == current_user

      assessments = profile.quality_assessments.valid.where.not(grid: skip_grids)
      assessors = profile.assessors
      header = PageComponent.supplier_performance_header(profile)
    end
    is_assessor = profile.model_name.singular == 'assessor_profile'
    followers = profile.followers
    hashy = {
      header: header,
      self: @self,
      canClick: true,
      detailData: [],
      type: 'search_cards'
    }

    hashy[:canClick] = false if current_user.supplier? && !@self

    case params[:type]
    when 'assessments'
      hashy[:type] = 'assessments'
      hashy[:header][:headerText] = current_user != member ? "#{profile.name}'s Assessments" : 'My Assessments'
      hashy[:detailData] = assessments.map { |x| PageComponent.future_qa_card(x) }
    when 'suppliers'
      hashy[:header][:headerText] = current_user != member ? "#{profile.name}'s Suppliers" : 'My Suppliers'
      hashy[:detailData] = suppliers.map { |x| PageComponent.search_results_card(x, current_user) }
    when 'assessors'
      hashy[:header][:headerText] = current_user != member ? "#{profile.name}'s Assessors" : 'My Assessors'
      hashy[:detailData] = assessors.map { |x| PageComponent.search_results_card(x, current_user) }
    when 'followers'
      hashy[:header][:headerText] = current_user != member ? "#{profile.name}'s Followers" : 'My Followers'
      hashy[:detailData] = followers.map { |x| PageComponent.search_results_card(x.follower, current_user) }
    when 'comments'
      hashy[:type] = 'comments'
      hashy[:header][:headerText] = current_user != member ? "#{profile.name}'s Comments" : 'My Comments'
      hashy[:detailData] = member.nil? ? [] : member.all_comments.map { |x| PageComponent.comment_card(x, member) }
    when 'product_families'
      hashy[:header][:headerText] =
        current_user != member ? "#{profile.name}'s Product Families" : 'My Product Families'
      pfs = if supplier
              profile.product_families.where(destroyed_at: nil)
            else
              ProductFamily.where(
                supplier_profile_id: member.suppliers.map(&:id), destroyed_at: nil
              )
            end
      hashy[:detailData] = pfs.map { |x| PageComponent.search_results_card(x, current_user) }
    when 'control_plans'
      hashy[:header][:headerText] = current_user != member ? "#{profile.name}'s CP Conform Rate" : 'CP Conform Rate'
      hashy[:detailData] = if supplier
                             []
                           else
                             profile.product_families.includes(:plan_items)
                                    .where(destroyed_at: nil).where.not(plan_items: { id: nil }).where(plan_items: { destroyed_at: nil })
                                    .map { |x| PageComponent.search_results_card(x, current_user) }
                           end
    when 'cp_freq'
      hashy[:header][:headerText] = current_user != member ? "#{profile.name}'s CP Conform Rate" : 'CP Conform Rate'
      hashy[:detailData] = if supplier
                             []
                           else
                             profile.product_families.includes(:plan_items)
                                    .where(destroyed_at: nil).where.not(plan_items: { id: nil }).where(plan_items: { destroyed_at: nil })
                                    .map { |x| PageComponent.search_results_card(x, current_user) }
                           end
    end
    puts 'HERE'
    @response.merge!(hashy)
    render json: @response
  end

  def account_settings; end

  def search_results
    # 1) Supplier can search all user types.
    # 2) Supplier cannot see HRP Goes reports.
    # 3) Supplier cannot see searched users HRP history cards
    # 4) Supplier cannot see searched users HRP favorites
    params[:current_user] = current_user
    @search = Search.new(params)
    if params[:supplier].present? || params[:assessment].present?
      results = @search.search_by_parameters!
    elsif params[:input].present?
      unless current_user.search_histories.recent.queries.include?(params[:input])
        SearchHistory.create(query: params[:input], user_id: current_user.id)
      end
      results = @search.search_by_input!
    else
      results = []
    end
    results = results.reject { |x| x.name.nil? || (x == current_user.profile) }

    hashy = {
      label: @search.label,
      searchCards: results.map { |x| PageComponent.search_results_card(x, current_user) },
      searched: params[:input]
    }
    @response.merge!(hashy)
    render json: @response
  end

  private

  def set_grids!
    active_icons = if params[:grid] == 'hfwb_assessment'
                     hfwb_grids.values
                   elsif params[:dpr]
                     dpr_grids.values
                   elsif current_user.grids_available.blank?
                     grids.values
                   else
                     current_user.grids_available.map { |x| grids[x] }.reject(&:nil?)
                   end
    @response.merge!({ active_icons: active_icons })
  end

  def grids
    {
      'QA' => icon_for('quality_assessment'),
      'CP' => icon_for('control_plan'),
      'OPEX' => icon_for('opex_assessment'),
      'SSE' => icon_for('sse_assessment'),
      'HRP' => icon_for('hrp_assessment'),
      'SCM' => icon_for('scm_assessment'),
      'ENV' => icon_for('env_assessment'),
      'PUR' => icon_for('pur_assessment'),
      'DPR' => icon_for('dpr_assessment', :home),
      'IND' => icon_for('indus_assessment')
    }
  end

  def dpr_grids
    h = {}
    h['DPR_TEXT'] = icon_for('dpr_textile')
    h['HFWB'] = icon_for('hfwb_assessment', :home)
    h
  end

  def hfwb_grids
    h = {}
    AssessmentHelper::Constants::HFWB_PRODUCTS.each do |k|
      h[k] = icon_for("hfwb:#{k}")
    end
    h
  end

  def icon_for(grid, path = :index)
    trans = 'api.v1.members.home'

    if grid == 'control_plan'
      icon = ControlPlan.new.icon
      icon[:desc] = t("#{trans}.cp")
      icon[:badgeNumber] = @cp_counts
    elsif grid.include?('hfwb:')
      prod = grid.split(':').last
      icon = QualityAssessment.icon_for('hfwb_assessment', path)
      icon[:image] = QualityAssessment.new(grid: grid).logo
      icon[:image] = icon[:image].gsub('/images/logos/', 'https://assessgo.decathlon.cn/') unless native_app?
      icon[:url] = "/members/assessment/index?ref_model=#{grid}"
      icon[:desc] = t("#{trans}.#{prod}")
      icon[:badgeNumber] = @badge_number[grid]
    else
      short_name = AssessmentHelper::Constants::SHORT_NAMES[grid]
      icon = QualityAssessment.icon_for(grid, path)
      icon[:url].gsub!('dpr_textile', 'dpr_assessment') if grid == 'dpr_textile'
      icon[:desc] = t("#{trans}.#{short_name.downcase}")
      icon[:badgeNumber] = @badge_number[grid]
    end

    icon
  end

  def fetch_country_value
    IsoCountryCodes.search_by_name(params[:input]).first.alpha2
  rescue StandardError
    params[:input]
  end
end
