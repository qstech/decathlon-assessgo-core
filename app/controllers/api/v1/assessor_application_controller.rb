class Api::V1::AssessorApplicationController < Api::V1::BaseController
  before_action :set_assessor_application,
                only: %i[qualification application_selection training_time application_status]
  def application
    ass_app = params[:id] ? AssessorApplication.find_by(id: params[:id]) : AssessorApplication.new
    h = {
      opmSource: AssessorProfile.source_list,
      referentSource: AssessorProfile.referent_list,
      object: ass_app.serial_hash
    }

    h[:teamLeader] = {}
    h[:opm] = {}
    h[:activeReferent] = {}
    h[:grid] = %w[QA CP OPEX SSE HRP SCM ENV PUR DPR]

    if params[:id]
      h[:teamLeader] = { id: ass_app.team_leader_id, name: ass_app.team_leader.name } if ass_app.team_leader_id
      h[:opm] = { id: ass_app.opm_id, name: ass_app.opm.name } if ass_app.opm_id
      h[:activeReferent] = { id: ass_app.validator_id, name: ass_app.validator.name } if ass_app.validator_id
    end

    render json: @response.merge!(h)
  end

  def qualification
    role = params[:role] == 'opm' ? 'OPM' : 'Team Leader'
    h = {
      header: {
        bgColor: 'blue',
        headerText: "#{role} Approval"
      },
      role: params[:role],
      selection: [
        { option: 'Yes', value: true },
        { option: 'No', value: false }
      ],
      name: @ass_app.user.assessor_profile.name
    }
    h.merge!(@ass_app.to_h(current_user))

    render json: @response.merge!(h)
  end

  def application_selection
    @qas = @ass_app.user.quality_assessments
    @qas = @qas.select { |x| x.short_name == @ass_app.grid }
    h = {
      cards: @qas.map do |x|
               PageComponent.history_card(x.goes_activities.last, current_user.id)
             end.reject { |x| x.blank? },
      active_index: nil
    }
    h.merge!(@ass_app.to_h)
    render json: @response.merge!(h)
  end

  def training_time
    render json: @response.merge!(@ass_app.to_h)
  end

  def validator
    @validation = Validation.find(params[:id])
    @ass_app = @validation.assessor_application

    @ass = @validation.assessment
    h = { card: PageComponent.history_card(@ass.goes_activities.last, current_user.id) }
    h.merge!({ application: @ass_app.to_h })
    h.merge!({ validation: @validation.serial_hash })
    render json: @response.merge!(h)
  end

  def application_status
    h = @ass_app.to_h(current_user)
    render json: @response.merge!(h)
  end

  private

  def set_assessor_application
    @ass_app = if params[:id]
                 AssessorApplication.find(params[:id])
               elsif params[:user_id]
                 AssessorApplication.where(user_id: params[:user_id])
                                    .order(:id).last
               else
                 current_user.assessor_applications.last
               end
  end
end
