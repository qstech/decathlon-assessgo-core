class Api::V1::ControlPlanController < Api::V1::BaseController
  # remove except for production
  # before_action :authenticate_user!, except: [:review, :review_detail, :product, :list, :step_detail, :step_edit, :new_step, :history]

  def report
    @response.merge!({ completed: true })
    cp = ControlPlan.find(params[:id])
    stats = cp.stats
    supplier = cp.supplier_profile
    assessor = cp.assessor # nil sometimes
    puts 'OH WOW'

    assessor_header = if !assessor.present?
                        'Not Confirmed'
                      else
                        "Monitored By: #{assessor.name}"
                      end

    h = {
      header: {
        supplier: supplier.name,
        assessor: assessor_header,
        type: cp.assess_type,
        projectDate: {
          month: Date::ABBR_MONTHNAMES[cp.updated_at.month],
          day: cp.updated_at.day
        }
      },
      major_stats: stats,
      radars: cp.radar_set
    }
    @response.merge!(h)

    cp_floating_home
    render json: @response
  end

  def kpi
    @product_family = ProductFamily.find_by(id: params[:id])
    @response.merge!(@product_family.kpi_details)
    @response.merge!({ optionsKpi: KpiTrack::OPTIONS })
    cp_floating_home
    render json: @response
  end

  def index
    control_plans = current_user.control_plans.where(completed: false, destroyed_at: nil).sort_by do |x|
      x.schedule ? x.schedule.supplier_schedule_date : Time.now
    end
    index_hash = {
      header: {
        bgColor: 'purple',
        tableLogo: '/images/logos/cp_logo.jpg',
        tableDesc: 'Master your process, you will master your product',
        buttonText: 'Schedule CP ➜',
        toPage: 'newQa',
        url: '/members/control_plan/schedule'
      },
      controlPlans: control_plans.map { |x| PageComponent.index_cp_card(x) }
    }
    index_hash[:web_url] = NativeUrl.generate('control_plan/schedule', current_user)
    @response.merge!(index_hash)
    # WxAuthentication.mp_qrcode
    render json: @response
  end

  def search
    if params[:input].present?
      pf_query = " \
        name ILIKE :query \
      "
      supplier_query = " \
        name ILIKE :query \
        OR code ILIKE :query \
      "

      supplier_results = SupplierProfile.where(supplier_query, query: "%#{params[:input]}%").limit(20)

      pf_results = ProductFamily.where(pf_query, query: "%#{params[:input]}%").where(destroyed_at: nil).limit(20)

      cards = pf_results.map { |x| PageComponent.pf_search_card(x, current_user) } +
              supplier_results.map { |x| PageComponent.supplier_search_card(x) }

      search_hash = {
        header: {
          bgColor: 'blue'
        },
        cards: cards
      }
    else
      search_hash = {
        header: {
          bgColor: 'blue'
        },
        loading: false
      }
    end

    @response.merge!(search_hash)
    render json: @response
  end

  def product_list
    supplier = SupplierProfile.find(params[:supplier_id].to_i)
    web_url = NativeUrl.generate('control_plan/new-product', current_user, supplier_id: supplier.id)
    list_hash = {
      supplier: {
        name: supplier.name,
        id: supplier.id
      },
      web_url: web_url,
      productFamilies: supplier.product_families.includes(:process_steps)
                               .where(destroyed_at: nil).map { |x| PageComponent.pf_search_card(x, current_user) }
    }
    cp_floating_home
    @response.merge!(list_hash)
    # p @response
    render json: @response
  end

  def new_product
    p params
    if !params[:edit].present?
      supplier = SupplierProfile.find((params[:supplier_id] || params[:id]).to_i)
      product_family = ProductFamily.new(supplier_profile_id: supplier.id)
      h = {
        supplier: supplier,
        product_family: product_family.to_h,
        images: { product_family.id => { process_flow_images: product_family.process_flow_images_urls } }
      }
    else
      product_family = ProductFamily.find_by(id: params[:id].to_i)
      supplier = product_family.supplier_profile
      h = {
        supplier: supplier,
        edit: true,
        product_family: product_family.to_h,
        images: { product_family.id => { process_flow_images: product_family.process_flow_images_urls } }
      }
    end
    render json: @response.merge!(h)
  end

  def product
    product_family = ProductFamily.find(params[:id].to_i)
    supplier = product_family.supplier_profile
    cps = product_family.control_plans.order(:assess_date)
    last_cp = cps.select { |x| x.assess_date < Time.now }.last
    next_cp = cps.select { |x| x.assess_date > Time.now }.first
    last_cp_review = last_cp.present? ? last_cp.assess_date.strftime('%Y-%m-%d') : nil
    next_cp_review = next_cp.present? ? next_cp.assess_date.strftime('%Y-%m-%d') : nil
    product_hash = {
      header: {
        bgColor: 'blue'
      },
      supplier: {
        id: supplier.id,
        name: supplier.name,
        address: supplier.address,
        contact_name: supplier.contact_name,
        contact_email: supplier.contact_email
      },
      productFamily: {
        id: product_family.id,
        last_updated: product_family.updated_at.strftime('%Y-%m-%d'),
        name: product_family.name,
        images: product_family.process_flow_images_urls,
        last_cp_review: last_cp_review || 'N/A',
        next_cp_review: next_cp_review || 'N/A',
        current_level: product_family.current_level,
        target_level: product_family.target_level
      },
      recentUpdates: product_family.recent_changes(1),
      pcpSteps: {
        available: product_family.available_count,
        reviewed: product_family.reviewed_count,
        pending: product_family.pending_count
      },
      kpiCard: {
        nonconformity_rate: product_family.nonconformity_rate,
        frequency_respect_rate: product_family.freq_respect_rate,
        supplier_frequency_respect_rate: product_family.freq_respect_rate('supplier'),
        cap_close_rate: product_family.cap_close_rate,
        other_kpis: product_family.other_kpis
      },
      web: product_family.web_views(current_user)
    }
    if next_cp_review.present?
      schedule_hash = {
        scheduleReview: {
          lastReview: last_cp_review,
          nextReview: next_cp_review,
          nextSupplierReview: '',
          details: "#{next_cp.plan_items.count} Steps Need Review Soon"
        }
      }
      @response.merge!(schedule_hash)
    end

    cp_floating_home
    @response.merge!(product_hash)
    render json: @response
  end

  def history
    product_family = ProductFamily.find(params[:product_id])
    supplier = product_family.supplier_profile
    history_set = product_family.recent_changes(20).map do |x|
      if x[:whodunit].present?
        prof = User.find_by(id: x[:whodunit]).profile
        x[:user] = prof.name
        x[:decathlon] = prof.instance_of?(AssessorProfile)
      end
      x
    end
    history_hash = {
      header: {
        bgColor: 'blue'
      },
      supplier: supplier,
      productFamily: {
        id: product_family.id,
        name: product_family.name
      },
      history: history_set
    }

    cp_floating_home
    @response.merge!(history_hash)
    render json: @response
  end

  def cp_history
    cps = ControlPlan.where(product_family_id: params[:product_family_id])
    cps = cps.map { |x| PageComponent.index_cp_card(x) }
    @response.merge!({ control_plans: cps })
    render json: @response
  end

  def schedule
    schedule_hash = {
      header: { bgColor: 'blue' },
      cp_type: ControlPlan::ASSESS_TYPE,
      process_categories: ProductProcess::CATEGORY,
      supplierSource: SupplierProfile.source_list
    }

    if params[:id].present?
      product_family = ProductFamily.find(params[:id])
      supplier = product_family.supplier_profile
      process_map = product_family.process_map

      schedule_hash.merge!({
                             product_id: params[:id],
                             productFamily: product_family.standard_hash,
                             stepCount: product_family.standard_hash[:stepCount],
                             supplier: supplier,
                             productProcesses: process_map
                           })

    elsif params[:supplier_id].present?
      supplier = SupplierProfile.find_by(id: params[:supplier_id])
      product_family = supplier.product_families.where(destroyed_at: nil).first || {}
      process_map = product_family.nil? ? [] : product_family.process_map

      schedule_hash.merge!({
                             product_id: params[:id],
                             productFamily: product_family.standard_hash,
                             stepCount: product_family.standard_hash[:stepCount],
                             supplier: supplier,
                             productProcesses: process_map
                           })
    else
      process_map = {}
      supplier = current_user.supplier || current_user.supplier_profile
      pfid = 0
      if supplier.present?
        pf = supplier.product_families.first || ProductFamily.new
        pfid = pf.id || 0
      end

      schedule_hash.merge!({
                             product_id: pfid,
                             productFamily: {},
                             supplier: supplier || {},
                             stepCount: 0,
                             productProcesses: {},
                             supplierInfo: {}
                           })
    end

    process_map.each do |_cat, proc_set|
      proc_set.each do |process|
        process[:steps].each do |step|
          step[:respected] = current_user.supplier? ? step[:supplier_freq_respected] : step[:decathlon_freq_respected]
        end
      end
    end
    @response.merge!(schedule_hash)
    render json: @response
  end

  def schedule_create
    p "Schedule time: #{params[:schedule_date]}"
    pf = ProductFamily.find(params[:product_family_id])
    supplier = pf.supplier_profile
    # current_user = User.find(1) if current_user.is_blank?
    cp = ControlPlan.create(product_family_id: pf.id,
                            created_by: current_user.id,
                            assessor_profile_id: current_user.profile.id,
                            assessor_type: current_user.assessor? ? 'assessor' : 'supplier',
                            assess_date: params[:supplier_schedule_date],
                            plan_type: params[:cp_type])

    sch = Schedule.create(
      ref_id: cp.id,
      ref_model: 'control_plan',
      sender_id: current_user.id,
      recipient_id: supplier.id,
      supplier_schedule_date: Date.strptime(params[:supplier_schedule_date], '%Y-%m-%d')
    )
    p sch.errors

    cp.render_activity(current_user)

    item_ids = params[:item_ids].flatten.map { |x| x['id'] }.uniq
    item_ids.each do |x|
      PlanItem.create(control_plan_id: cp.id, process_step_id: x)
    end

    render json: { code: 200, msg: 'success', control_plan_id: cp.id }
  end

  def list
    product_family = ProductFamily.find(params[:id])
    supplier = product_family.supplier_profile
    product_processes = ProductProcess.where(product_family_id: product_family.id).includes(:process_steps).order(:number)
    process_map = product_processes.map { |x| PageComponent.product_process_to_list(x) }
    list_hash = {
      header: {
        bgColor: 'blue'
      },
      productFamily: {
        name: product_family.name,
        id: product_family.id
      },
      supplier: {
        name: supplier.name,
        address: supplier.address,
        contact_name: supplier.contact_name,
        contact_email: supplier.contact_email
      },
      processList: product_processes.map { |x| x.name },
      productProcesses: process_map,
      category: ProductProcess::CATEGORY
    }

    cp_floating_home
    @response.merge!(list_hash)
    render json: @response
  end

  def step_edit
    @response.merge!({ edit: true })
    new_step
  end

  def new_step
    images = {}
    if @response[:edit]
      ps = ProcessStep.find(params[:step_id])
      pr = ps.product_process
      pf = pr.product_family
      sup = pf.supplier_profile
      product_processes = pf.product_processes
      images[ps.id] = {
        acceptable_image: ps.acceptable_image_url,
        unacceptable_image: ps.unacceptable_image_url,
        requirement_images: ps.requirement_images_urls
      }
    else
      pf = ProductFamily.find(params[:product_family_id])
      sup = pf.supplier_profile
      product_processes = pf.product_processes
      ps = ProcessStep.new
      images[0] = {
        acceptable_image: nil,
        unacceptable_image: nil,
        requirement_images: []
      }
    end
    pp = {}
    ProductProcess::CATEGORY.each do |x|
      pp[x] = []
    end

    pp.merge!(product_processes.group_by do |x|
                x.category.to_s
              end.reject { |k, _v| !ProductProcess::CATEGORY.include?(k) }) # ProductProcess.find(params[:process_id])
    all_process_steps = ProcessStep.where(product_process_id: product_processes.map { |x| x.id }, destroyed_at: nil)
    processSteps = all_process_steps.map do |x|
                     { pid: x.product_process_id, step_number: x.step_number, control_item: x.control_item }
                   end.uniq.group_by { |x| x[:pid] }
    processSteps.each do |k, v|
      processSteps[k] = {
        items: v,
        names: v.map { |x| "#{x[:step_number]} - #{x[:control_item]}" }
      }
    end

    ppc = ProductProcess::CATEGORY
    psc = ProcessStep::CONTROL_TYPES
    new_step_hash = {
      header: {
        bgColor: 'blue'
      },
      supplier: {
        id: sup.id,
        name: sup.name
      },
      images: images,
      productFamily: {
        id: pf.id,
        name: pf.name
      },
      productProcess: pp,
      processStep: ps,
      product_process_types: ppc,
      process_step_control_types: psc,
      processSteps: processSteps
    }

    if @response[:edit]
      pi = ppc.index(pr.category)
      ap = pp[pr.category]
      ai = ap.map { |x| "#{x.name} #{x.number}" }.index("#{pr.name} #{pr.number}")

      new_step_hash[:process_index] = pi
      new_step_hash[:active_processes] = ap
      new_step_hash[:active_processes_names] = ap.map { |x| x.name }
      new_step_hash[:active_processes_numbers] = ap.map { |x| x.number }
      new_step_hash[:active_process_index] = ai
      new_step_hash[:step_index] = psc.index(ps.control_type)
      new_step_hash[:active_step_index] =
        processSteps[pp[ppc[pi]][ai][:id]][:names].index("#{ps.step_number} - #{ps.control_item}")
      new_step_hash[:choosed_step] = processSteps[pp[ppc[pi]][ai][:id]][:items][new_step_hash[:active_step_index]]
    end

    @response.merge!(new_step_hash)
    # new_process_button and new_step_button to be updated with tl
    render json: @response
  end

  def step_detail
    process_step = ProcessStep.find(params[:step_id])
    product_process = process_step.product_process
    product_family = product_process.product_family
    supplier = product_family.supplier_profile
    steps = if params[:edit].present?
              [process_step]
            else
              product_process.process_steps.where(destroyed_at: nil)
            end
    images = {}

    steps.map do |x|
      images[x.id] = {
        acceptable_image: x.acceptable_image_url,
        unacceptable_image: x.unacceptable_image_url
      }
    end

    steps_hash = steps.map { |x| x.standard_hash }
    steps_detail_hash = { steps: steps_hash }

    current_step = steps_hash.select { |x| x[:id] == params[:step_id].to_i }.first

    common_hash = {
      productProcess: {
        name: product_process.name,
        category: product_process.category
      },
      productFamily: {
        id: product_family.id,
        name: product_family.name
      },
      supplier: {
        id: supplier.id,
        name: supplier.name
      },
      control_types: ProcessStep::CONTROL_TYPES
    }

    cp_floating_home
    @response.merge!(steps_detail_hash)
    @response.merge!({ images: images })
    @response.merge!(common_hash)
    @response.merge!({ current_step: current_step })
    render json: @response
  end

  def review
    control_plan = ControlPlan.find(params[:cp_id])
    product_family = control_plan.product_family
    edit_product_web_url = NativeUrl.generate('control_plan/new-product', current_user, product_id: product_family.id)
    supplier = control_plan.product_family.supplier_profile
    plan_items = control_plan.plan_items.order(:id)
    plan_items_array = plan_items.map(&:process_step_id)
    product_processes = ProductProcess.includes(:process_steps).where(process_steps: { id: plan_items_array }).order(:id)
    pp_array = []

    product_processes.each do |process|
      p_hash = process.standard_hash.merge({ steps: [] })

      process.process_steps_active.each do |step|
        next unless plan_items_array.include?(step.id)

        plan_item = plan_items.select { |x| x.process_step_id == step.id }[0]
        p_hash[:steps].push(step.standard_hash.merge({
                                                       plan_item_id: plan_item.id,
                                                       plan_item_completed: plan_item.completed,
                                                       plan_item_result: plan_item.result,
                                                       result_color: plan_item.result_color
                                                     }))

        p_hash[:steps].sort_by! { |x| x[:step_number].to_f }
      end
      pp_array.push(p_hash) if p_hash[:steps].length > 0
    end

    active_steps = pp_array.map { |x| x[:steps].select { |s| !s[:plan_item_id].nil? } }.flatten
    active_steps.sort_by! { |x| x[:step_number].to_f }
    active_steps.sort_by! do |x|
      h = Hash.new(2)
      h['NOK'] = 1
      h['OK'] = 3
      h[x[:plan_item_result]]
    end

    review_hash = {
      header: { bgColor: 'blue' },
      controlPlan: control_plan.to_h,
      supplier: { name: supplier.name },
      productFamily: {
        id: product_family.id,
        name: product_family.name,
        web_url: edit_product_web_url
      },
      processCategories: ProductProcess::CATEGORY,
      processList: pp_array.group_by { |x| x[:category] },
      active_steps: active_steps,
      completed_web_url: NativeUrl.generate('cap/confirmation', current_user, cp_id: control_plan.id)
    }

    cp_floating_home
    @response.merge!(review_hash)
    render json: @response
  end

  def review_detail
    plan_item = PlanItem.find(params[:plan_item_id])
    process_step = plan_item.process_step
    process_step_hash = process_step.standard_hash.merge!({ category: process_step.product_process.category })
    control_plan = plan_item.control_plan
    plan_items = control_plan.plan_items.order(:id)
    plan_items_array = plan_items.map(&:process_step_id)
    product_family = control_plan.product_family
    supplier = product_family.supplier_profile
    product_processes = ProductProcess.includes(:process_steps).where(process_steps: { id: plan_items_array }).order(:id)
    images = {}
    images[process_step.id] = {
      acceptable_image: process_step.acceptable_image_url,
      unacceptable_image: process_step.unacceptable_image_url
    }

    active_plan_items = []
    active_process_steps = []
    product_processes.each do |process|
      process.process_steps_active.each do |step|
        next unless plan_items_array.include?(step.id)

        s = plan_items.select { |x| x.process_step_id == step.id }.min_by(&:id)
        active_plan_items << s unless s.nil?
        active_process_steps << step
      end
    end

    sorted_ids = active_plan_items.sort_by { |x| x.process_step.step_number.to_f }.map(&:id)
    current_index = sorted_ids.index(params[:plan_item_id].to_i)
    next_id = sorted_ids[current_index + 1] || 0
    last_id = sorted_ids[current_index - 1]
    last_id = 0 if current_index == 0

    detail_hash = {
      header: { bgColor: 'blue' },
      supplier: { name: supplier.name },
      productFamily: {
        id: product_family.id,
        name: product_family.name,
        web_url: NativeUrl.generate('control_plan/new-product', current_user, product_id: product_family.id)
      },
      controlPlan: { id: control_plan.id },
      planItem: plan_item.to_h,
      processStep: process_step_hash,
      images: images,
      active_plan_items: active_plan_items.map(&:to_h),
      active_process_steps: active_process_steps,
      next_id: next_id,
      last_id: last_id,
      result_options: PlanItem::RESULTS
    }
    @response[:t].merge!(t('common'))
    @response[:t].merge!(t('api.v1.control_plan.step_detail.step_detail_card'))
    @response.merge!(detail_hash)
    render json: @response
  end

  def cap_confirmation
    render json: @response
  end

  def findings
    cp = ControlPlan.find(params[:id])
    pls = cp.plan_items.includes(:process_step).where(destroyed_at: nil).order(:id)
    sorted = pls.sort_by { |x| (x.process_step.step_number || '0').to_i }
                .sort_by { |x| ['NOK', 'DER', 'OK', 'N/A', nil, 'false'].index(x.result) || 100 }.uniq
    h = {
      plan_items: sorted.map { |x| PageComponent.cp_findings_card(x) },
      cp_id: cp.id
    }
    cp_floating_home
    @response.merge!(h)
    render json: @response
  end
end
