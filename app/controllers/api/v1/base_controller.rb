require 'rest-client'
require 'jwt'
require 'mechanize'

class Api::V1::BaseController < ActionController::Base
  # protect_from_forgery with: :exception
  skip_before_action :verify_authenticity_token
  before_action :verify_headers
  # before_action :validate_permissions
  before_action :authenticate_user!
  around_action :set_locale
  before_action :prep_response, except: [:check_version]
  before_action :set_paper_trail_whodunnit
  # before_action :set_user
  include ApiBugReporter

  def validate_permissions
    raise StandardError.new('INVALID ENVIRONMENT') if request.get? && (request.headers['app'] == 'web')
  end

  def verify_headers
    request.headers['app'] = 'web' if request.headers['X-USER-USERNAME'].blank?
  end

  def check_version
    ok_versions = { 'ios' => '5.0.6', 'android' => '6.0.0' }
    urls = { 'ios' => 'http://d.zqapps.com/q8g9', 'android' => 'http://d.zqapps.com/dgr4' }

    app = if params[:app].present?
            params[:app]
          else
            request.headers['app'].strip
          end

    puts "CHECKING VERSION: #{app} #{params[:current]}"

    ok_version = ok_versions[app] || '6.0.0'

    if params[:current].present? &&  (params[:current] < ok_version)
      MailBag.trigger_by_tag!('outdated_version', current_user, { name: current_user.name })

      res = {
        code: 666,
        msg: 'There is a new version available! Please download now.',
        url: urls[app]
      }
    else
      res = { code: 200, msg: 'OK' }
    end
    render json: res
  end

  def set_locale(&action)
    I18n.locale = :en
    locale = params[:locale]
    locale = request.headers['locale'] if locale.blank?
    locale = :en if locale.blank?
    I18n.with_locale(locale, &action)
  end

  def set_avatar
    @user = User.find_by_username(params[:data][:username])
    @user.update(wx_avatar: params[:data][:avatar])
  end

  private

  def wx_auth(code)
    app_id = 'wx74bd28be58496d80'
    app_secret = '1a48bc2e78699aa734efe0d6dd5fa3f9'
    url = "https://api.weixin.qq.com/sns/jscode2session?appid=#{app_id}&secret=#{app_secret}&js_code=#{code}&grant_type=authorization_code"
    JSON.parse(RestClient.get(url))
    # res.key?("openid") ? res["openid"] : res["errmsg"]
  end

  def check_headers
    puts response.headers
  end

  def clean_params
    new_params = {}
    if params[:scene]
      new_params = decode_params(params[:scene])
      new_params.each { |k, v| params[k] = v }
    end
    new_params
  end

  def decode_params(encoded_params)
    new_params = {}
    first_decode = URI.decode_www_form(encoded_params)

    query = if first_decode.is_a?(Array)
              first_decode.join
            else
              URI.decode_www_form(first_decode)
            end

    query.split('&').each do |param|
      set = param.split('=')
      new_params[set[0].to_sym] = set[1]
    end
    new_params
  end

  def prep_response
    assessments = current_user.qa_counts.values.sum || 0
    cps = current_user.cp_counts || 0
    all = assessments + cps
    activity = 0 # GoesActivity.activities_count(current_user)
    received_comments_count = current_user.received_unread_comments_count

    path = params[:controller].split('/').last + '#' + params[:action]

    if path.include?('quality_assessments') || path.include?('home') || path.include?('control_plan')
      active_tab = 0
    elsif path.include?('activity')
      active_tab = 1
    elsif path.include?('search')
      active_tab = 2
    elsif path.include?('me')
      active_tab = 3
    end

    # LOCALE MERGE EN Backup
    translation_path = "#{controller_path}/#{action_name}".gsub('/', '.')
    t = t(translation_path)
    if params[:locale] != 'en'
      t = t.is_a?(String) ? t : t(translation_path, locale: :en).merge(t)
    end

    @response = {
      code: 200,
      status: 'success',
      hide_preload: true,
      today: Time.now.strftime('%Y-%m-%d'),
      load: true,
      t: t,
      navData: {
        active_tab: active_tab,
        tab_list: tab_list
      },
      navBadge: [all, activity, 0, received_comments_count] # the badge number, should be dynaimic
    }
    @response.merge!(clean_params)
  end

  def with_floating_home
    @response.merge!({ floating: { icon: 'home', bindtap: 'home',
                                   url: '/members/assessment/index' } })
  end

  def cp_floating_home
    @response.merge!({ floating: { icon: 'home', bindtap: 'home',
                                   url: '/members/control_plan/index' } })
  end

  def tab_list
    [
      {
        image: '/images/tabbar/home.png',
        image_sel: '/images/tabbar/home_sel.png',
        name: t('menu.assess'),
        nav_tab: '/members/home/home'
      },
      {
        image: '/images/tabbar/goes.png',
        image_sel: '/images/tabbar/goes_sel.png',
        name: t('menu.goes'),
        nav_tab: '/members/activity/activity'
      },
      {
        image: '/images/tabbar/search.png',
        image_sel: '/images/tabbar/search_sel.png',
        name: t('menu.for'),
        nav_tab: '/members/search/form'
      },
      {
        image: '/images/tabbar/user.png',
        image_sel: '/images/tabbar/user_sel.png',
        name: t('menu.performance'),
        nav_tab: '/members/me/me'
      }
    ]
  end

  def clean_response!
    @response = clean(@response)
  end

  def clean(v)
    if v.nil?
      ''
    elsif v.is_a? Array
      v.map { |x| clean(x) }
    elsif v.is_a? Hash
      v.each { |k, y| v[k] = clean(y) }
      v
    else
      v
    end
  end

  def china_request?
    i2l = Ip2location.new.open("#{Rails.root}/lib/IP2LOCATION.BIN")
    i2l.get_country_short(request.remote_ip.to_s) == 'CN'
  end

  def native_app?
    request.headers['app'].in?(%w[ios android]) || params[:app].in?(%w[ios android])
  end
end
