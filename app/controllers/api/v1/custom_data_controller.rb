require 'rest-client'
class Api::V1::CustomDataController < Api::V1::DataController
  skip_before_action :authenticate_user!, only: %i[pdf_finished get_wx_token get_oa_token]
  skip_before_action :prep_response, only: %i[pdf_finished get_wx_token get_oa_token offline_data]

  def offline_data
    # raise StandardError.new("NOT VALID MODEL") unless params[:model].in?(["quality_assessment", "control_plan"])
    if params[:model] == 'control_plan'
      ref = ControlPlan.find_by(id: params[:id])
      pp = PagePrepper.new(current_user, {}, ref, request.headers['app'] || 'wx')
      res = pp.prepare!(:control_plan)
    else
      ref = QualityAssessment.find_by(id: params[:id])
      pp = PagePrepper.new(current_user, {}, ref, request.headers['app'] || 'wx')
      res = pp.prepare!(:assessment)
    end

    if request.headers['app'] == 'ios'
      render json: res
    else
      render json: { code: 200, offline: res }
    end
  end

  def quality_assessments
    @model = QualityAssessment
    params[:process] = nil if params[:process] == 'Your Selection:'

    if params[:id].present? && params[:id].to_i > 0
      puts "params.. #{params}"
      @object = QualityAssessment.find(params[:id])
      old_chapter_set = @object.chapter_set

      # (1) find the chapter to delete
      if params[:chapter_set].present?
        old_chapter_set.each do |ch|
          ch_split = ch.split('.')

          next if params[:chapter_set].include?(ch)

          tag = 'QA'
          tag = 'OPEX' if @object.grid == 'opex_assessment'
          tag = 'SSE'  if @object.grid == 'sse_assessment'
          tag = 'HRP'  if @object.grid == 'hrp_assessment'
          tag = 'ENV'  if @object.grid == 'env_assessment'
          tag = 'SCM'  if @object.grid == 'scm_assessment'
          tag = 'PUR'  if @object.grid == 'pur_assessment'
          tag = 'DPR_TEXT'  if @object.grid == 'dpr_assessment'
          tag = 'IND'  if @object.grid == 'indus_assessment'
          tag += "CH#{ch_split[0]}S#{ch_split[1]}"

          cas = ChapterAssessment.where(quality_assessment_id: @object.id).where('chapter_tag LIKE ?', "#{tag}%")
          cas.each { |x| x.update destroyed_at: Time.now }
        end

        # (2) setup chapters for the additional ones
        new_chapter_set = params[:chapter_set].select { |x| !old_chapter_set.include?(x) }
        @object.setup_chapters(new_chapter_set, (params[:processes] || []))
      end

      m = model_params.to_h.reject { |_k, v| v.blank? }
      puts "model params... => #{m}"
      @object.update(m)
    else
      ch_params = { chapter_set: params[:chapter_set] }
      ch_params[:processes] = params[:processes] if params[:processes].present?
      m_params = model_params
      m_params[:assess_date] = 5.days.from_now if m_params[:assess_date].blank?
      m_params.delete(:process) if m_params[:process] == 'Your Selection:'
      puts "mmmmm => Process: #{m_params[:process]}"
      p m_params

      @object = QualityAssessment.create_and_setup(m_params, ch_params)
      @object.render_activity(current_user)
    end

    # here needs some modification for HRP
    # if this is QA create, and grid is not hrp
    must_invite = false
    must_invite = true if params[:id].nil? && !current_user.assessor?
    must_invite = true if params[:id].nil? && @object.grid != 'hrp_assessment'
    must_invite = true if params[:id].present?
    send_invites if must_invite

    @object.update_status
    render json: { code: 200, msg: 'success', object: @object.serializable_hash }
  end

  def duplicate_assessment
    ref = QualityAssessment.find(params[:id])
    # model_params ==>> {"supplier_code"=>"2701 AVERY DENNISON SOUTH CHINA", "assess_type"=>"Gemba", "assess_date"=>"2020-5-20", "supplier_profile_id"=>"1", "created_by"=>"277", "grid"=>"quality_assessment"}
    # ch_params ==>> {:chapter_set=>["1.1", "1.2", "1.3", "1.4"]}

    # cap_params =====>>>> {"ref_model"=>"quality_assessment", "ref_id"=>"8", "cap_date"=>"2020-05-07", "cap_type"=>"CAP Review", "supplier_profile_id"=>"38", "people_concerned"=>[], "invited_pls"=>[]}

    # cap_points =====>>>> [<ActionController::Parameters {"ref_model"=>"chapter_question", "ref_id"=>580, "section"=>"1.1", "level"=>"D-2", "requirement"=>"The General management is responsible of quality: The general management (or a representive) is committed toward quality and is able to explain the organisation and main axis and give adapted means to achieve the targets (ex: Human ressources). \n ", "result"=>"NOK"} permitted: true>]

    if ['CAP Close', 'CAP Review'].include?(ref.assess_type)
      puts 'duplicating cap...'
      model_params = ref.cap.serializable_hash(only: %w[ref_model ref_id cap_type supplier_profile_id
                                                        people_concerned invited_pls]).merge({ cap_date: Time.now })

      cap_points = ref.cap.cap_points.map do |x|
        x.serializable_hash(only: %i[ref_model ref_id section level requirement result])
      end
      cap = Cap.create_and_setup(model_params, cap_points, current_user.id)
      new_ref = QualityAssessment.find_by(cap: cap)
    else
      puts 'duplicating qa...'
      model_params = ref.serializable_hash(only: %w[supplier_code assess_type supplier_profile_id created_by
                                                    grid]).merge({ 'assess_date' => Time.now })
      sections = ref.chapter_assessments.map(&:section).uniq
      ch_params = { chapter_set: Guideline.active.where(guideline_tag: sections).map(&:section_name).sort }
      new_ref = QualityAssessment.create_and_setup(model_params, ch_params)
    end

    new_ref.assessment_day_form.update(ref.assessment_day_form.serializable_hash(except: %w[id quality_assessment_id created_at updated_at destroyed_at]))
    # assessor assignments?

    render json: { code: 200, msg: 'success', object: new_ref }
  end

  def section_na
    ch   = ChapterAssessment.find_by(id: params[:id].to_i)
    chs  = ChapterAssessment.where(section: ch.section, quality_assessment_id: ch.quality_assessment_id)
    cqs  = ChapterQuestion.where(chapter_assessment_id: chs.map { |x| x.id }, result: nil)
    puts "FOUND #{cqs.count} To Set"
    cqs.update_all(result: 'Not Applicable', strong_points: 'N/A', points_to_improve: 'N/A') unless cqs.blank?
    chs.each { |x| x.update_status }
    ch.quality_assessment.render_activity(current_user)

    render json: { code: 200, msg: 'Successfully set!' }
  end

  def invitation
    @invitation = Invitation.find(params[:id])
    @invitation.update(accepted: true) if params[:invitation] == 'accept'
    @invitation.update(rejected: true) if params[:invitation] == 'reject'
    @invitation.render_activity(current_user)

    if @invitation.ref_model == 'quality_assessment'
      puts 'Updating status..'
      assessment = QualityAssessment.find(@invitation.ref_id)
      assessment.update_status
    end

    render json: { code: 200, msg: 'success' }
  end

  def destroy_assessor_assignments
    qa = QualityAssessment.find(params[:id])
    qa.assessor_assignments.destroy_all
    qa.update(assessor_confirmed: false)
    qa.update_status
  end

  def cap_confirmation
    prep_response
    @response[:t].merge!(t('common'))
    if params[:cp_id].present?
      assessment = ControlPlan.find(params[:cp_id])
    elsif params[:qa_id].present?
      assessment = QualityAssessment.find(params[:qa_id])
    end
    puts '1'
    h = {
      # optionsCap: ['CAP Review', 'CAP Close'],
      prep: assessment.generate_cap_prep
    }

    h[:optionsCap] = if current_user.supplier? || assessment.assess_type == 'Gemba' || params[:cp_id].present?
      ['CAP Review']
    else
      ['CAP Review', 'CAP Close']
    end

    puts '2'
    if h[:prep][:points].blank? && h[:prep][:recheck].blank? && params[:cp_id].present?
      puts '3'
      assessment.update(completed: true)
      assessment.render_activity(current_user)
      render json: { code: 200, redirect: "/members/control_plan/report?id=#{params[:cp_id]}" }
    elsif  h[:prep][:points].blank? && params[:qa_id].present?
      puts '4'

      assessment.update(completed: true)
      assessment.render_activity(current_user)
      render json: { code: 200, redirect: "/members/assessment/report?id=#{params[:qa_id]}" }
    else
      puts '5'
      assessment.update(completed: true)
      puts '5'
      assessment.render_activity(current_user)
      puts '6'
      h[:adapterSource] = AssessorProfile.source_list + assessment.supplier_users.map do |x|
        x.merge!({ candidate: User.find(x[:id][1..-1].to_i).candidate_for?(assessment.grid) })
      end
      h[:assessor_names] = []
      h[:bindSource] = []
      h[:redirect] = nil
      @response.merge!(h)
      render json: @response
    end
  end

  def caps
    @model = Cap
    if params[:id].present?
      @object = Cap.find(params[:id])
    else
      cap_params = model_params
      puts "cap_params =====>>>> #{cap_params}"
      cap_points = params[:cap][:cap_points].map do |pt|
        pt.permit(:ref_model, :ref_id, :section, :level, :requirement, :result)
      end
      puts "cap_points =====>>>> #{cap_points}"

      if cap_params[:supplier_profile_id].blank?
        ass = params[:cap][:ref_model].classify.constantize.find_by(id: params[:cap][:ref_id])
        cap_params[:supplier_profile_id] = ass.supplier_profile.id
      end
      @object = Cap.create_and_setup(model_params, cap_points, current_user.id)
    end
    render json: { code: 200, msg: 'CAP Created' }
  end

  def cap_form
    if params[:model] == 'control_plan'
      ref = ControlPlan.find_by(id: params[:id])
    else
      ref = QualityAssessment.find_by(id: params[:id])
    end

    points = ref.cap_points.map { |x| x.standard_hash }
    i = (params[:index] || '0').to_i
    @response.merge!({ points: points, active_index: i, active_point: points[i], qa_id: params[:id] })
    render json: @response
  end

  def cap_index
    ref = if params[:model] == 'control_plan'
      ControlPlan.find_by(id: params[:id])
    else
      QualityAssessment.find_by(id: params[:id])
    end

    web_url = NativeUrl.generate('cap/followup_invite', current_user, qa_id:params[:id])
    cap = ref.cap
    cap_points = cap.cap_points.includes(:cap_followup, :cap_resolutions).order(:id)
    points = cap_points.map do |x|
      h = x.serializable_hash
      h.symbolize_keys!
      h[:resolutions] = x.cap_resolutions
      h[:followup] = x.cap_followup
      h
    end
    can_schedule = false
    if params[:followup] == 'true'
      resolved = cap_points.select { |x| x.cap_followup.present? && x.cap_followup.resolved }
      perc = resolved.count * 100 / cap_points.length
    else
      resolved = cap_points.select { |x| x.cap_resolutions.present? }
      perc = resolved.count * 100 / cap_points.length
      can_schedule = true if (perc == 100) && cap.cap_followups.blank?
    end

    @response.merge!({ points: points, cap: cap.basic_h, perc: perc, can_schedule: can_schedule, qa_id: params[:id],
                       web_url: web_url })
    render json: @response
  end

  def cap_followup
    if params[:model] == 'quality_assessment'
      ref = QualityAssessment.find_by(id: params[:id])
    elsif params[:model] == 'control_plan'
      ref = ControlPlan.find_by(id: params[:id])
    end

    cap_followup = ref.cap_points[params[:index].to_i].cap_followup
    cap_point = cap_followup.cap_point
    resolutions = cap_point.cap_resolutions

    # cap_followup = CapFollowup.find(params[:id])
    # resolution = cap_followup.cap_resolution
    date = cap_followup.followup_date.strftime('%Y-%m-%d')
    ass = cap_point.cap.assessment
    url = ass.mp_path(:review)
    point_url = "/members/assessment/historical-level?id=#{cap_point.ref_id}"

    hashy = {
      date: date,
      resolutions: resolutions.map(&:standard_hash),
      point: cap_point.to_h,
      followup: cap_followup.to_h,
      disable_editing: ref.primary_assessor != current_user
    }

    @response.merge!(hashy)
    @response.merge!({ point_url: point_url, cap_url: url, qa_id: params[:id] }) if ass.is_a?(QualityAssessment)
    @response[:t].merge!(t('api.v1.custom_data.cap_form'))

    render json: @response
  end

  def cap_followup_create
    @model = CapFollowup
    cap = Cap.find(params[:cap_id])
    cap.cap_points.each do |pt|
      CapFollowup.create(cap_point_id: pt.id, followup_date: model_params[:followup_date])
    end
    @object = cap.quality_assessment
    send_invites if @object

    render json: { msg: 'success' }
  end

  def cap_followup_invite
    h = {
      assessorSource: AssessorProfile.source_list
    }
    @response.merge!(h)
    render json: @response
  end

  def cap_related
    @cap = Cap.find(params[:id])
    h = { caps: @cap.related_activity.map do |x|
            { url: x.mp_path(:review), type: x.assess_type,
              date: x.assess_date.strftime('%Y-%m-%d') }
    end }
    @response.merge!(h)
    render json: @response
  end

  def kpi_create
    kpi_track_params = params.require(:kpi_track).permit(:name, :frequency, :product_family_id, :target, :active)
    kpi_record = params.require(:kpi_record).permit(:value)
    track = KpiTrack.find_or_create_by(kpi_track_params.except(:frequency, :target, :active))
    track.update(kpi_track_params)

    kpi = KpiRecord.create(kpi_track_id: track.id, value: kpi_record[:value])

    render json: { code: 200, msg: 'success', object: kpi.standard_hash }
  end

  def pdf_finished
    ActionCable.server.broadcast "PdfChannel_#{params[:slug]}", { message: 'done', url: params[:new_url] }
    render json: { code: 200, msg: 'success' }
  end

  def get_wx_token
    # MP TOKEN
    token = WxAuthentication.get_token
    render json: { access_token: token }
  end

  def get_oa_token
    # OA TOKEN
    token = WxNotifier.get_token
    render json: { access_token: token }
  end

  def blackouts
    begin
      @assessor = AssessorProfile.find(params[:id])
      user = User.joins(:assessor_profile).find_by(assessor_profiles: { id: params[:id] })
      if user.supplier?
        days = []
        dstr = []
      else
        url = Rails.application.credentials.dig(:google, :api)
        res = RestClient.get("#{url}/api/v1/google/fetch_events?email=#{user.email}&id=0")
        json = JSON.parse(res.body)

        days = json['json_days']
        dstr = json['string_days']
      end
    rescue RestClient::InternalServerError
      days = []
      dstr = []
    ensure
      render json: { code: 200, msg: 'success', disabledDays: days, disabledStr: dstr }
    end
  end

  private

  def send_invites(model = 'quality_assessment')
    ids = if params[:invited_assessors].nil?
      nil
    elsif params[:invited_assessors][0].is_a? String
      params[:invited_assessors]
    else
      params[:invited_assessors].map { |x| x[:id] }.uniq
    end

    puts "send_invites... invited_assessors ids ==>> #{ids}"

    if ids.present?
      ids.each do |id|
        if current_user.assessor_profile.present? && (id == current_user.assessor_profile.id) && (model == 'quality_assessment')
          assign_current_user

        else
          # Check if user is a supplier and find a user to send invitation
          u = id[0] === 's' ? User.find(id[1..-1].to_i) : AssessorProfile.find_by(id: id.to_i).user
          if u.present?
            Invitation.create(
              sender_id: current_user.id,
              recipient_id: u.id,
              ref_model: model,
              ref_id: @object.id
            )
          end
        end
      end
    elsif params[:id].blank? || params[:id].to_i == 0
      assign_current_user(model)
    end
  end

  def assign_current_user(model = 'quality_assessment')
    ass = AssessorAssignment.new(user_id: current_user.id, ref_model: model, ref_id: @object.id, primary: true,
                                 confirmed: true)
    ass.save
  end
end
