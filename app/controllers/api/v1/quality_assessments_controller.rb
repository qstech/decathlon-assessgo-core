require 'rest-client'
class Api::V1::QualityAssessmentsController < Api::V1::BaseController
  def overview
    @qa = QualityAssessment.find_by(id: params[:id])
    @response.merge!({ assessment: @qa.serializable_hash })
    render json: @response
  end

  def assessment_prep
    @qa = QualityAssessment.find_by(id: params[:id])
    respondant = params[:respondant] || 'assessor'
    fq = @qa.prep_questions_form(respondant)

    @response.merge!(fq)
    @response.merge!({ short_name: @qa.short_name, assess_type: @qa.assess_type })
    clean_response!
    render json: @response
  end

  def index
    # Set Vars
    grid = set_ref_model
    @qa = QualityAssessment.new(grid: grid)
    @user = params[:id].present? ? User.find(params[:id]) : current_user

    # Retrieve Cards
    index_hash = @qa.index_cards(@user, is_self: @user == current_user)

    # Merge to JSON Response
    @response.merge!(index_hash)
    render json: @response
  end

  def new
    ref_model = set_ref_model
    @qa = QualityAssessment.new(grid: ref_model, supplier_profile_id: params[:supplier_id])
    @qa.supplier_profile_id = current_user.profile.id if current_user.supplier?
    hashy = @qa.new_form(current_user, native: native_app?)

    if params[:id].present?
      @qa = QualityAssessment.find(params[:id])
      @qa.edit_form(hashy)
    end

    @response.merge!(hashy)
    render json: @response
  end

  def review_assessment
    # @response.merge!({completed: true})
    @response[:t] = t('api.v1.quality_assessments.pre_confirmation')
    @review = true
    pre_confirmation
  end

  def report
    @response.merge!({ completed: true })
    qa = QualityAssessment.find(params['id'].to_i)
    supplier = qa.supplier_profile

    result = qa.completed? ? qa.result : ''
    cotation = qa.completed? ? qa.cotation : ''
    hashy = {
      result: result,
      resultColor: PageComponent::EXTRA_CLASSES[result] || 'black',
      cotation: cotation,
      prep_completed: qa.prep_completed,
      assessor_confirmed: qa.assessor_confirmed,
      assess_today: qa.assess_today?,
      radars: qa.radar_set,
      annual_assessment: qa.annual?,
      preloading: { type: 'Boolean', value: 'false' },
      supplier: supplier.to_h,
      header: {
        supplier: supplier.name,
        assessor: qa.assessor_header,
        type: qa.assess_type,
        color: PageComponent::EXTRA_CLASSES[qa.status],
        projectDate: {
          month: Date::ABBR_MONTHNAMES[qa.assess_date.month],
          day: qa.assess_date.day
        },
        status: qa.status
      },
      generate_cap: current_user.decathlon? ? qa.generate_cap? : false

    }

    hashy[:supplier][:qa_id] = qa.id
    # set Radar chart data here
    puts 'OR HERE'
    with_floating_home
    @response.merge!(hashy)
    render json: @response
  end

  def cap_index
    puts "PARAMS \n\n\n\n#{params}"
    supplier = SupplierProfile.find(params[:supplier_id])
    cap_type = params[:cap_type]
    ref_model = params[:ref_model]

    qas = supplier.quality_assessments.where(destroyed_at: nil, completed: true,
                                             grid: ref_model)

    qas = qas.where(created_by: current_user.id) if current_user.supplier?

    qas = if cap_type == 'CAP Close'
      qas.where(assess_type: ['Annual', 'CAP Close'])
    else
      qas.where.not(assess_type: ['CAP Close', 'CAP Review'])
    end

    qa_cards = qas.select { |x| x.generate_cap? }.map { |qa| PageComponent.completed_qa_card(qa) }

    h = {
      supplier: supplier.to_h,
      qa_cards: qa_cards,
      cap_type: cap_type || 'CAP'
    }

    render json: @response.merge!(h)
  end

  def pre_confirmation
    qa = QualityAssessment.find(params[:id])
    supplier = qa.supplier_profile
    active_links = PageComponent.qa_active_links(qa, current_user, @review == true)

    result = qa.completed? ? qa.result : ''
    cotation = qa.completed? ? qa.cotation : ''
    days_left = (qa.assess_date.to_date - Time.now.to_date).to_i

    day_form_target = AssessmentDayForm.find_by(quality_assessment_id: qa.id).supplier_target
    trans = 'api.v1.quality_assessments.pre_confirmation'
    qa_h = {
      qa_id: qa.id, completed: qa.completed?, day_form_target: day_form_target,
      target: day_form_target || supplier.target || 'N/A'
    }
    supp = supplier.to_h.merge(qa_h)
    last_qa = qa.last_assessment

    hashy = {
      completed: qa.completed?,
      result: result,
      resultColor: PageComponent::EXTRA_CLASSES[result] || 'black',
      cotation: cotation,
      prep_completed: qa.prep_completed,
      assessor_confirmed: qa.assessor_confirmed,
      assess_today: qa.assess_today?,
      annual_assessment: qa.annual?,
      preloading: { type: 'Boolean', value: 'false' },
      supplier: supp,
      header: {
        supplier: supplier.name,
        assessor: qa.assessor_header,
        type: qa.assess_type,
        color: PageComponent::EXTRA_CLASSES[qa.status],
        projectDate: {
          month: Date::ABBR_MONTHNAMES[qa.assess_date.month],
          day: qa.assess_date.day
        },
        status: qa.status
      },
      active_links: active_links,
      gradientNotice: {
        in: days_left,
        in_label: t("#{trans}.days_count", count: days_left),
        left: days_left - 7,
        left_label: t("#{trans}.days_count", count: days_left - 7)
      },
      qa: qa.to_h,
      generate_cap: current_user.decathlon? ? qa.generate_cap? : false
    }
    # to overwrite supplier's to_h method

    if last_qa.nil?
      hashy[:supplier][:lastAssessed] = 'N/A'
      hashy[:supplier][:lastResult] = 'N/A'
    else
      hashy[:supplier][:lastAssessed] = (last_qa.assess_date.strftime('%Y-%m-%d') || 'N/A')
      hashy[:supplier][:lastResult] = (last_qa.result || 'N/A')
      hashy[:supplier][:resultColor] = (last_qa.level_color[last_qa.result] || 'black')
    end

    if qa.completed?
      cap = QualityAssessment.includes(:cap)
      .where(caps: { ref_model: 'quality_assessment', ref_id: qa.id, destroyed_at: nil },
             quality_assessments: { assess_type: 'CAP Close' }).first

      if cap.present? && cap.completed?
        hashy[:cap_path] = cap.mp_path(:review)
      elsif cap.present?
        hashy[:cap_path] = cap.mp_path(:assess)
      end
    end

    with_floating_home
    @response.merge!(hashy)
    render json: @response
  end

  def go_for_assessment_review
    @response[:t] = t('api.v1.quality_assessments.go_for_assessment')
    go_for_assessment
  end

  def go_for_assessment
    @qa = QualityAssessment.find_by(id: params[:id])
    adf = @qa.assessment_day_form
    disable_editing = @qa.official_assessment? && current_user.supplier?
    adapter_source = @qa.adapter_source
    supplier = @qa.supplier_profile.to_h(grid: @qa.grid)
    supplier[:day_form_target] = adf.supplier_target

    @response.merge!({ supplier: supplier, qaType: @qa.official_assessment?,
                       type: @qa.assess_type, grid: @qa.grid, day_form: adf.form,
                       adapterSource: adapter_source, inputValue: '', bindSource: [],
                       selection: AssessmentDayForm::PL_PRESENT_CHOICES,
                       targetOptions: AssessmentDayForm::TARGET_CHOICES,
                       disable_editing: disable_editing, list: adf.responses })

    render json: @response
  end

  def copy_last_go_for_assessment
    current = AssessmentDayForm.find(params[:id])

    last = current.quality_assessment.cap.ref.assessment_day_form

    if last.present?
      atts = last.serializable_hash(except: %i[id quality_assessment_id created_at updated_at])
      current.update(atts)
      code = 200
      msg = 'success'
    else
      code = 404
      msg = 'not found'
    end

    render json: { code: code, msg: msg }
  end

  def chapter_assessment_review
    qa = QualityAssessment.find_by(id: params[:id])
    chapters = PageComponent.qa_chapter_set(qa)
    supplier = qa.supplier_profile

    res = {
      qa: qa,
      short_name: qa.short_name,
      assess_type: qa.assess_type,
      color: qa.color,
      chapters: chapters,
      supplier: supplier.name,
      review: true
    }
    if qa.cap_close?
      ref = qa.cap.ref
      ref_chapters = PageComponent.qa_chapter_set(ref)
      ref_chapters.each do |chapter|
        chapter[:children].each do |x|
          x[:disable_editing] = true
          x[:cap_id] = qa.cap.id
          x[:annual_chapter] = true
        end
      end
      chapters.each_with_index do |chapter, idx|
        # Re-assigning ref_chapter's chapter to CAP's chapter if present
        chapter[:children].each do |sec|
          ref_chapters[idx][:children].each_with_index do |x, i|
            ref_chapters[idx][:children][i] = sec if sec[:guideline_tag] == x[:guideline_tag]
          end
        end
      end
      res[:ref_chapters] = ref_chapters
    end

    @response.merge!(res)

    # @response.merge!({chapters: chapters, supplier: supplier.name, qa: qa, short_name: qa.short_name, review: true})
    with_floating_home
    render json: @response
  end

  def chapter_assessment
    qa = QualityAssessment.includes(:chapter_questions).find_by(id: params[:id])
    chapters = PageComponent.qa_chapter_set(qa)
    supplier = qa.supplier_profile

    res = {
      short_name: qa.short_name,
      assess_type: qa.assess_type,
      color: qa.color,
      chapters: chapters,
      supplier: supplier.name,
      perc: qa.chapter_percent,
      chapter_dates: qa.chapter_dates,
      assess_date: qa.assess_date.strftime("%Y-%m-%d"),
      id: params[:id]
    }

    res[:perc] += 1 if res[:perc] == 99

    if qa.cap_close?
      refs = QualityAssessment.where(id: qa.parent_assessment_ids).order(:id)
      merged_refs = []
      sections_indexed = []
      chapters.each { |ch| ch[:children].each { |sec| sections_indexed << [sec[:guideline_tag], sec] } }
      sections_indexed = sections_indexed.to_h
      refs.each do |ref|
        ref_chapters = PageComponent.qa_chapter_set(ref)
        ref_indexed = []
        ref_chapters.each { |ch| ch[:children].each { |sec| ref_indexed << [sec[:guideline_tag], sec] } }
        ref_indexed = ref_indexed.to_h

        ref_chapters.each do |chapter|
          chapter[:children].each do |sec|
            sec.merge!({ disable_editing: true, annual_chapter: true, cap_id: qa.cap.id, cap_qa_id: qa.id })
          end
          tags = merged_refs.map { |chapter| chapter[:guideline_tag] }

          if tags.include?(chapter[:guideline_tag])
            idx = tags.index(chapter[:guideline_tag])
            merged_refs[idx][:children].each_with_index do |section, i|
              ref_section = ref_indexed[section[:guideline_tag]]
              merged_refs[idx][:children][i] = ref_section if ref_section.present?
            end

            sec_tags = merged_refs[idx][:children].map { |section| section[:guideline_tag] }

            chapter[:children].each do |section|
              merged_refs[idx][:children] << section unless sec_tags.include?(section[:guideline_tag])
            end
          else
            merged_refs << chapter
          end
        end

        merged_refs.each_with_index do |chapter, idx|
          chapter[:children].each_with_index do |sec, i|
            merged_refs[idx][:children][i] = sections_indexed[sec[:guideline_tag]] if sections_indexed[sec[:guideline_tag]].present?
          end
        end
      end
      res[:ref_chapters] = merged_refs
    end

    @response.merge!(res)
    render json: @response
  end

  def single_chapter
    # NOTE: Must refactor code in MP to send QA id
    # PREVIOUS LOGIC:  SEND ALL INFO for ALL LEVELS of a QA Chapter.
    # ===> would only load API call once per page, then change tabs.
    # ===> e.g. Section 1.2 (all levels)

    # CURRENT LOGIC:  No longer using multiple Tabs. 1 API call per level.
    # ===> e.g. Section 1.2 D

    # Code should be refactored to do much less intensive queries, and
    # send a smaller JSON as a result.

    # Params to toggle show all/show cap points only for CAP Close
    puts 'START'
    @level = params[:level].nil? ? 1 : params[:level].to_i
    ch   = ChapterAssessment.find_by(id: params[:id])
    qa   = ch.quality_assessment
    chs  = ch.siblings.with_questions
    cqs  = ChapterQuestion.with_images.with_likes.where(chapter_assessment_id: chs.map(&:id))
    tags = cqs.map(&:chapter_tag)
    last_attempt = qa.last_attempt(tags)
    images = cqs.map { |cq| [cq.id, cq.img_h] }.to_h
    puts 'SET VARS'
    if qa.dpr?
      gl = DprGuideline.find_by(guideline_tag: ch.chapter_tag, grid: 'dpr_textile')
      cq = cqs.first
      gl_h = gl.to_h.merge(cq.single_chapter_form(current_user, with_comments: true))
      response_set = [gl_h]
      active_level = gl
    else
      gl = Guideline.with_grandchildren.find_by(grid_version_id: qa.grid_version_id, guideline_tag: ch.section)
      response_set = gl.children.sort_by(&:guideline_tag)
      .select { |x| chs.map(&:chapter_tag).include?(x.guideline_tag) }
      .map do |lev|
        vars = {
          chs: chs, cqs: cqs, gl: gl, qa: qa, last_attempt: last_attempt,
          user: current_user, native: native_app?, china: china_request?, tags: tags
        }
        lev.single_chapter_form(vars)
      end
      # removing nil values
      response_set = response_set.reject{|x| x.blank?}
      active_level = gl.children.find{|x| x.guideline_tag == ch.chapter_tag} || gl.children.sort_by(&:guideline_tag)[@level - 1] || gl.children.sort_by(&:guideline_tag).first
    end
    selection = active_level.response_set
    evaluation = ch&.evaluation&.to_a || []
    puts 'SINGLE CHAPTER FORM END'

    # if (qa.grid == "hrp_assessment") && (@level > 0)
    #   selection << {name: "NOK without risk", value: "NOK without risk"}
    #   selection.sort_by!{|x| x[:name]}.reverse!.uniq!
    # end

    disable_editing = (params[:review].present? || (qa.official_assessment? && !current_user.can_edit?(qa)))
    survey_url = NativeUrl.generate('survey-questions', current_user, { qa_id: qa.id })
    wx_url = { url: survey_url }.to_query
    survey_url = "/pages/bugs/bugs?#{wx_url}" unless native_app?

    hashy = { disable_editing: disable_editing, grid: qa.grid,
              assess_type: qa.assess_type, levels: response_set, selection: selection,
              qa_id: ch.quality_assessment_id, images: images, completed: qa.completed,
              score: qa.section_score(guideline: gl.to_h(with_children: true), chapters: chs),
              can_bookmark: current_user.decathlon?, survey_url: survey_url,
              active_level: response_set[@level - 1], next_offline: '', last_offline: '',
              evaluation: evaluation, evaluationShown: false }

    puts 'HASH SETUP'
    unless qa.dpr?
      ai = response_set.index(hashy[:active_level]) || (response_set.length - 1)
      next_i = ai + 1
      hashy[:next_offline] = offline_link(response_set[next_i][:lev_obj]) if next_i < (response_set.length - 1)
      hashy[:last_offline] = offline_link(response_set[ai - 1][:lev_obj]) if ai > 0
    end


    puts 'RESOLVED'
    @response.merge!(hashy)
    clean_response!
    render json: @response
  end

  def historical_level
    pt = {}
    cq = ChapterQuestion.find_by(id: params[:id]) || ChapterQuestion.new(chapter_tag: params[:tag])
    qa = cq.quality_assessment if cq.id

    pt[:show_fields] = params[:id].present?
    pt[:form_fields] = cq.standard_hash
    pt[:form_fields][:date] = cq.updated_at.strftime('%Y-%m-%d') if cq.updated_at

    if cq.id
      gl = Guideline.find_by(guideline_tag: cq.chapter_tag, grid_version_id: qa.grid_version_id)
    else
      gl = Guideline.active.find_by(guideline_tag: cq.chapter_tag) || Guideline.find_by(guideline_tag: cq.chapter_tag)
    end

    lev = Guideline.find_by(id: gl.parent_id)
    sec = Guideline.find_by(id: lev.parent_id)
    pt[:guidelines] = gl.to_h
    pt[:section] = sec.serial_hash
    pt[:grid] = cq.chapter_tag.gsub(/CH.*/, '')
    render json: @response.merge!(pt)
  end

  def assessment_prep_review
    qa = QualityAssessment.find(params['id'].to_i)
    respondant = params[:respondant] || 'assessor'
    qs = PrepQuestion.form_questions.where(respondant: respondant).order(priority: :asc).group_by { |x| x.question_tag }

    h = { 'Yes' => 'green', 'No' => 'red' }

    prep = qa.prep_questions.order(:id).map do |pq|
      if qs[pq.question_tag].nil?
        nil
      else
        {
          chapter: qs[pq.question_tag].first.label,
          status: pq.response,
          statusColor: h[pq.response] || 'black'
        }
      end
    end.reject(&:nil?)

    supplier = qa.supplier_profile
    assessor = qa.assessor_name

    @response.merge!({
                       header: {
                         supplier: supplier.name,
                         assessor: assessor, # invited assessor or myself
                         status: qa.status,
                         statusColor: qa.status_color[qa.status],
                         preparedBy: assessor # created by
                       },
                       prepList: prep
    })
    with_floating_home
    clean_response!
    render json: @response
  end

  def single_chapter_review
    @response[:t].merge!(t('api.v1.quality_assessments.single_chapter'))
    single_chapter
  end

  def survey_questions
    @qa = QualityAssessment.find(params[:qa_id])
    @survey_questions = @qa.survey_questions.includes(form_question: :form_section)
                           .order('form_questions.priority ASC, form_sections.priority ASC')
    @sections = @survey_questions.group_by(&:form_section)
    @form = @sections.keys.first.form
    @sections.map! { |sec, questions| { title: sec.title, questions: questions.map(&:to_h) } }
    h = {title: @form.title, sections: @sections}
  end

  def redo_chapter
    qa = QualityAssessment.find(params[:id].to_i)
    tag = ChapterAssessment.find(params[:chapter_id].to_i).section
    gl = Guideline.find_by(guideline_tag: tag, grid_version_id: qa.grid_version_id)
    # new_chapter = qa.setup_redo_chapter(tag)
    new_chapter = qa.setup_redo_chapter(tag).first
    render json: {
      id: new_chapter.id,
      section: gl.section_name,
      level: 1
    }
  end

  def invitations
    if params[:set_assessor].present?
      # hashy =
      hashy = {
        set_assessor: true,
        invitations: [],
        invitations_to_send: current_user.invitation_cards_to_send
      }
    else
      grid = params[:grid]
      hashy = {
        set_assessor: false,
        invitations: current_user.invitation_cards(grid),
        invitations_to_send: []
      }
    end
    @response.merge!(hashy)

    render json: @response
  end

  def supplier_info
    supplier = SupplierProfile.find(params[:id].to_i)

    base_options = {
      country_names: IsoCountryCodes.all.map(&:name),
      country_values: IsoCountryCodes.all.map(&:alpha2),
      country_index: supplier.country_index,
      processes: SupplierProfile::INDUSTRIAL_PROCESSES,
      process_index: SupplierProfile::INDUSTRIAL_PROCESSES.index(supplier.industrial_process) || 0,
      statusOptions: SupplierProfile::STATUS_CHOICES,
      rankOptions: SupplierProfile::RANK_CHOICES,
      status: supplier.status,
      targetOptions: SupplierProfile::TARGET_CHOICES
    }

    h = supplier.to_h.merge!(base_options)

    assessors = {
      assessors: supplier.pl_set,
      assessor_names: supplier.pl_names,
      opms: supplier.opm_set,
      opm_names: supplier.opm_names,
      ptms: supplier.ptm_set,
      ptm_names: supplier.ptm_names
    }

    @response.merge!(assessors)
    grids_available = (QualityAssessment::ACTIVE_GRIDS + ['CP']).map do |x|
      { value: x, checked: supplier.grids_available.include?(x) }
    end
    @response.merge!({
                       supplier: h,
                       assessorSource: AssessorProfile.source_list,
                       countrySource: User.all_countries.map do |x|
                         { value: x[:value], label: "#{x[:value]} - #{x[:label]}" }
                       end,
                       gridsAvailable: grids_available
    })
    render json: @response
  end

  private

  def set_qa_chapters(gls)
    gls.map do |ch|
      # For some reason, can't use attribute calling, so I switched it to symbol.
      formatted = { title: "#{ch[:section_name]} #{ch[:title]}", button: t('api.v1.quality_assessments.new.check_all'),
                    buttonShow: true }
      formatted[:subchapters] = ch[:children].sort_by { |sc| sc[:section_name] }.map do |sc|
        { chapter_id: sc[:section_name], value: sc[:title], checked: false, disabled: false }
      end
      formatted
    end
  end

  def set_ref_model
    if params[:id].present?
      QualityAssessment.find(params[:id]).grid
    else
      params[:ref_model] || params[:grid] || 'quality_assessment'
    end
  end

  def offline_link(obj)
    "members/assessment/single-chapter?id=#{obj.id}&section=#{params[:section]}&level=#{obj.level}"
  end
  # def inviting_user
  #   index
  # end
end
