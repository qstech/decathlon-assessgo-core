class Api::V1::UserSessionsController < Devise::SessionsController
  skip_before_action :verify_authenticity_token
  before_action :authenticate_user!
  before_action :clean_username!
  before_action :destroy_duplicates!
  skip_before_action :authenticate_user!, only: %i[login create login_status set_mode]
  skip_before_action :check_permission
  include ApiBugReporter
  # ===========CORE LOGIN ROUTES=====
  def login
    # Auto Login via openid post
    res = {}
    if params[:openid]
      res = { 'openid' => params[:openid] }
      res['unionid'] = params[:unionid] if params[:unionid]
      user = User.find_by(wx_openid: res['openid'])
    elsif params[:code]
      res = wx_auth(params[:code])
      puts 'WX AUTH RESULT:'
      puts res
      user = User.find_by(wx_openid: res['openid'])
    end

    if user_signed_in?
      user.update(wx_openid: nil) if (current_user != user) && !user.nil?
      current_user.update(wx_openid: params[:openid])
      current_user.update(unionid: params['unionid']) if params['unionid'].present? && current_user.unionid.blank?
      user = current_user

      user.send_mail!('thanks')
      token = Tiddle.create_and_return_token(user, request, expires_in: 1.days)
      render json: { code: 200, launch: 'production', object: user.user_hash,
                     user: { username: user.username, token: token }, hide_preload: true, change_password: user.change_password? }
    elsif user.blank?
      render json: { code: 300, launch: 'production', msg: 'no user associated with this wechat account',
                     user: {} }.merge(res)
    elsif Time.now < user.authenticated_until
      user.update(unionid: res['unionid']) if res['unionid'].present? && user.unionid.blank?
      user.send_mail!('thanks')

      token = Tiddle.create_and_return_token(user, request, expires_in: 1.days)

      render json: { code: 200, launch: 'production', object: user.user_hash,
                     user: { username: user.username, token: token }, hide_preload: true, change_password: user.change_password? }
    else
      puts user.authenticated_until
      render json: { code: 300, launch: 'production', msg: 'Authentication Period Expired!', user: {} }.merge(res)
    end
  end

  def logout
    puts 'logging out'
    user = User.find(params[:user])
    if user.update(authenticated_until: Time.now, wx_openid: nil)
      render json: { code: 200, msg: 'User signed out' }
    else
      render json: { code: 300, msg: 'Failed to log out' }
    end
  end

  def create
    # SIGN IN via Username/Password
    user = User.find_for_authentication(username: params[:data][:username])
    puts params

    if request.base_url != 'https://assessgo.decathlon.cn'
      fake_auth
    elsif user.present? && user.username.include?('@')
      supplier_auth(user)
    elsif !params[:data][:username].include?('@')
      decath_auth
    else
      not_found!
    end

    render json: @response
  end

  # ===========OTHER STUFF==========
  def set_mode
    # SETTING MODE IF DEV, TEST, or PROD
    puts ENV['LIMVERSION']
    puts params[:current]
    if params[:force_login]
      force_login
    elsif request.base_url == 'https://assessgo.decathlon.cn'
      login
    elsif ENV['LIMVERSION'].present? && ENV['LIMVERSION'] == params[:current]
      # login
      if params[:code]
        res = wx_auth(params[:code])
        puts 'WX AUTH RESULT:'
        puts res
      end
      fake_login
      # fake_supplier
      render json: @response
    else
      @response = 'prod'
      render json: @response
    end
  end

  def force_login
    user = User.find(params[:force_login])
    token = Tiddle.create_and_return_token(user, request, expires_in: 1.days)
    render json: { code: 200, launch: 'production--FORCED', object: user.user_hash,
                   user: { username: user.username, token: token }, hide_preload: true, change_password: user.change_password? }
  end

  def login_status
    # CHECKING ON DECATH LOGIN RESULT
    30.times do
      puts ''
    end

    puts 'DEBUGGING'
    puts params[:data][:username]
    user = User.find_for_authentication(username: params[:data][:username])
    wxu = User.find_by_wx_openid(params[:openid]) if params[:openid].present?
    status = (user || User.new(authenticated_status: 'ERR')).authenticated_status

    if status == 'success!' || (status == '' && Time.now < user.authenticated_until)
      puts 'update status'
      user.update(authenticated_status: '', authenticated_until: Time.now + 1.months)
      wxu.update(wx_openid: nil) if wxu.present? && (wxu != user)

      user.update(wx_openid: params[:openid]) unless params[:openid].blank?
      user.update(wx_openid: SecureRandom.urlsafe_base64(25)) if user.wx_openid.blank?
      token = Tiddle.create_and_return_token(user, request, expires_in: 1.days)
      headers = { 'X-USER-USERNAME' => user.username, 'X-USER-TOKEN' => token }
      user.send_mail!('thanks')
      render json: { code: 200, login: true, object: user.user_hash, headers: headers,
                     user: { username: user.username, token: token }, hide_preload: true, change_password: user.change_password? }
    elsif status == 'ERR'
      puts 'ERR!'
      msg = "Could not login. If your username and password is correct, you might be required by Decathlon's internal system to change your password. (Please visit https://password.decathlon.com/ with your computer browser to change your password)"
      msg = 'Invalid Username or Password!' if user.supplier?
      render json: { code: 402, login: false, msg: msg }
    else
      puts 'Continue!'
      render json: { code: 888, login: false, login_status: status }
    end
  end

  def destroy
    Tiddle.expire_token(current_user, request) if current_user
    render json: {}
  end

  def change_password
    p "params =======> #{params}"
    user = User.find_by(username: params[:username]) || current_user
    p "current_user ====> #{user}"
    if params[:current_password].nil? || user.valid_password?(params[:current_password])
      if user.update!(password: params[:password], authenticated_until: 3.months.from_now,
                      authenticated_status: 'success!')
        render json: { code: 200, msg: 'Success!' }
      else
        render json: { code: 300, msg: t('api.v1.members.members_show.password_not_updated') }
      end
    else
      render json: { code: 400, msg: t('api.v1.members.members_show.incorrect_password') }
    end
  end

  private

  # =============CORE LOGIN SCRIPTS=====================

  def supplier_auth(user)
    p params[:openid]
    set = { wx_openid: params[:openid] }
    set[:unionid] = params[:unionid] if params[:unionid].present?

    if user.valid_password?(params[:data][:password])
      update_condition1 = set[:wx_openid].present? && (user.wx_openid != set[:wx_openid])
      update_condition2 = set[:unionid].present? && (user.unionid != set[:unionid])
      user.update(set) if update_condition1 || update_condition2
      user.update(authenticated_status: 'success!')
      @response = { code: 200, login: false, login_status: 'Intializing connection...' }
    else
      user.update(authenticated_status: 'ERR')
      @response = { code: 402, login: false, msg: 'Username or Password incorrect!' }
    end
  end

  def decath_auth
    puts 'FINDING or CREATING USER'
    @user = DecathAuthentication.assign_user(params[:data][:username])
    p @user

    username = Encryptor.encrypt(params[:data][:username])
    password = Encryptor.encrypt(params[:data][:password])
    p params
    p params[:openid]
    set = { 'openid' => params[:openid] }
    set['unionid'] = params[:unionid] if params[:unionid].present?

    DecathAuthWorker.set(queue: :login).perform_async(username, password, set)
    @response = { code: 200, login: false, login_status: 'Intializing connection...' }
  end

  # =============WECHAT AUTH SCRIPTS====================
  def wx_setup
    if params[:openid]
      p params
      res = { 'openid' => params[:openid], 'unionid' => params[:unionid] }
      p res
    elsif params[:code]
      res = wx_auth(params[:code])
      puts "WX AUTH RESULT: #{res}"
    end
    res
  end

  def wx_auth(code)
    app_id = 'wx74bd28be58496d80'
    app_secret = '1a48bc2e78699aa734efe0d6dd5fa3f9'
    url = "https://api.weixin.qq.com/sns/jscode2session?appid=#{app_id}&secret=#{app_secret}&js_code=#{code}&grant_type=authorization_code"
    JSON.parse(RestClient.get(url))
  end

  # ==========FAKE LOGIN SCRIPTS FOR STAGING============

  def fake_supplier
    user = retrieve_fake_supplier
    @response = set_auth_tokens(user)
  end

  def fake_login
    user = retrieve_fake_user
    @response = set_auth_tokens(user)
  end

  def fake_auth
    if ['testuser', 'testsupplier@supplier.com', 'testsupplier'].include?(params[:data][:username])
      user = retrieve_fake_user if params[:data][:username] == 'testuser'
      user = retrieve_fake_supplier if params[:data][:username] == 'testsupplier'
      @response = set_auth_tokens(user)
    else
      @response = { code: 666, login: false, hide_preload: true,
                    error: "Username not valid! Please try either 'testuser' or 'testsupplier'" }
    end
  end

  def not_found!
    @response = { code: 666, login: false, hide_preload: true, error: 'User not valid!' }
  end

  def retrieve_fake_user
    user = User.find_by(username: 'testuser')
    # user = User.all.sample
    if user
      puts 'FOUND USER!'
    else
      puts 'Creating User....'
      user = User.create(username: 'testuser', password: '123123')
      ass  = AssessorProfile.new(name: 'TEST USER', handle: 'testuser')
      user.assessor_profile = ass
    end
    user
  end

  def retrieve_fake_supplier
    user = User.find_by(username: 'testsupplier@supplier.com')
    if user
      puts 'FOUND USER!'
    else
      puts 'Creating User....'
      user = User.create(username: 'testsupplier@supplier.com', password: '123123')
      supp = SupplierProfile.new(name: 'TEST SUPPLIER')
      user.supplier_profile = supp
    end
    user
  end

  # this is invoked before destroy and we have to override it
  def verify_signed_out_user; end

  # ===============

  def set_auth_tokens(user, _env = 'debug')
    token = Tiddle.create_and_return_token(user, request, expires_in: 1.days)
    headers = { username: user.username, token: token }
    head = { 'X-USER-USERNAME' => user.username, 'X-USER-TOKEN' => token }
    user.send_mail!('thanks')
    { code: 200, login: true, launch: 'debug', object: user.user_hash, user: headers, headers: head,
      hide_preload: true, change_password: user.change_password? }
  end

  def clean_username!
    params[:data][:username].gsub!(/\s/, '') if params[:data] && params[:data][:username]
  end

  def destroy_duplicates!
    User.destroy_duplicates!
  end
end
