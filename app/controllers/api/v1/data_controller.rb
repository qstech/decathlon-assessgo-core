# require 'rest-client'
require 'json'
class Api::V1::DataController < Api::V1::BaseController
  # protect_from_forgery with: :exception
  skip_before_action :prep_response, only: %i[index show create update destroy delete_image rotate_image]
  before_action :authenticate_user!
  before_action :set_model, only: %i[index show create update destroy delete_image rotate_image bookmark_image]
  before_action :set_object, only: %i[show update destroy delete_image rotate_image bookmark_image]
  after_action :update_chapters, only: [:update]

  def index
    p_hash = index_params.to_h

    @objects = @model.all
    @objects = @objects.where(destroyed_at: nil) if @model.attribute_names.include?('destroyed_at')
    @objects = @objects.where(p_hash) if p_hash.present?
    @objects = @objects.in_groups_of(100)
    i = if params[:page].blank? || (params[:page].to_i < 1)
          0
        else
          params[:page].to_i - 1
        end

    if @objects.empty?
      render json: []
    elsif [SupplierProfile, QualityAssessment, ControlPlan, ProductFamily, Comment, GoesActivity].include?(@model)
      render json: @objects[i].reject(&:nil?).map(&:standard_hash)
    else
      render json: []
    end
  end

  def show
    render json: @object.standard_hash
  end

  def create
    @object = @model.new(model_params)

    if @object.save
      attach_files
      @object.render_activity(current_user)
      ActionCable.server.broadcast "UserChannel_#{current_user.id}", { message: 'saved' }
      if params[:product_process].present?
        p '======productProcess'
        render json: { code: 200, msg: 'success', object: @object.standard_hash_with_steps }
      else
        render json: { code: 200, msg: 'success', object: @object.standard_hash }
      end
    else
      render json: { code: 300, msg: 'FAILED!' }
    end
  end

  def update
    if current_user.can_edit?(@object)
      attach_files
      if @object.update!(model_params)
        @object.render_activity(current_user)
        ActionCable.server.broadcast "UserChannel_#{current_user.id}", { message: 'saved' }
        render json: { code: 200, msg: 'success', object: @object.standard_hash }
      else
        puts @object.errors.messages
        render json: { code: 300, msg: 'FAILED!' }
      end
    else
      puts 'USER UNAUTHORIZED TO EDIT'
      render json: { code: 200, msg: 'success', object: @object.standard_hash }
    end
  end

  def destroy
    if current_user.can_edit?(@object)
      if @object.attributes.keys.include?('destroyed_at')
        @object.update(destroyed_at: Time.now)
        render json: { code: 200, msg: 'soft delete' }
      elsif @object.destroy
        render json: { code: 200, msg: 'success' }
      else
        render json: { code: 300, msg: 'FAILED!' }
      end
    else
      render json: { code: 402, msg: 'Unauthorized!' }
    end
  end

  def bookmark_image
    if current_user.assessor?
      bookmark!

      render json: { code: 200, msg: 'success', object: @object.standard_hash } if @object.save
    else
      render json: { code: 200, msg: 'success', object: @object.standard_hash }
    end
  end

  def delete_image
    if current_user.can_edit?(@object)
      purge!

      render json: { code: 200, msg: 'success', object: @object.standard_hash } if @object.save
    else
      render json: { code: 200, msg: 'success', object: @object.standard_hash }
    end
  end

  def rotate_image
    set = @object.public_send(params[:option])

    if set.instance_of?(ActiveStorage::Attached::Many)
      # attachment = set[params[:index].to_i]
      attachments = set.attachments
    elsif set.instance_of?(ActiveStorage::Attached::One)
      attachment = set.attachment
    end
    images = []
    thumbs = []
    rotations = params[:rotation].map { |r| r >= 0 ? r % 360 : r % -360 }
    rotations.each_with_index do |r, i|
      img = MiniMagick::Image.open(set[i].service_url)
      img.rotate(r)
      set.attach(io: File.open(img.path), filename: SecureRandom.urlsafe_base64)
      images[i] = set.last.service_url
      thumbs[i] = @object.variant_link(set.last)
      set[i].purge
    end

    render json: { code: 200, msg: 'success', img_urls: images, img_thumb_urls: thumbs }
  end

  def read_comments
    # no supplier jouney now
    # p "============= read comments"
    profile = params[:profile_type].constantize.find_by(id: params[:id].to_i)
    profile.user.received_comments_mark_read
    render json: { code: 200, msg: 'success' }
  rescue NoMethodError
    render json: { code: 200, msg: 'success' }
  end

  private

  def purge!
    set = @object.public_send(params[:option])

    if set.instance_of?(ActiveStorage::Attached::Many)
      set[params[:index].to_i].purge
    elsif set.instance_of?(ActiveStorage::Attached::One)
      set.purge
    end
  rescue StandardError
    true
  end

  def bookmark!
    set = @object.public_send(params[:option])
    if set.instance_of?(ActiveStorage::Attached::Many)
      set[params[:index].to_i].update(bookmark: params[:bookmark])
    elsif set.instance_of?(ActiveStorage::Attached::One)
      set.update(bookmark: true)
    end
  rescue StandardError
    true
  end

  def update_chapters
    if @model == QualityAssessment
      incomplete = @object.chapter_questions.where(result: nil)
      if @object.completed == true && @object.chapters_completed == false && incomplete.count > 0
        incomplete.each { |x| x.update result: 'Not Applicable' }
        @object.update chapters_completed: true
      end
    end
  end

  def set_model
    # /api/v1/:name
    # /api/v1/users
    # => User
    # Make an array of all DB table_names
    db_index = Hash[ActiveRecord::Base.connection.tables.collect do |c|
                      [c, c.classify]
    end ].except('schema_migrations', 'ar_internal_metadata')
    # Take the tablename, and make the Model of the relative table_name
    @model = db_index[params[:name]].constantize # "users" => User
  end

  def set_object
    # Take model from "set_model"
    # User.find(1)
    @object = if params[:id] == '0'
      # special case for process step, to delete images before created
      @model.new
    else
      @model.find(params[:id])
    end
  end

  def index_params
    mn = @model.to_s.underscore.to_sym
    # {user: {name: "something", other_column_name: "somevalue"}}
    return {} if params[mn].blank?

    # MAKE LIST OF THINGS TO PERMIT:
    arr = @model.attribute_names.map { |e| e.to_sym }
    arr.delete(:created_at)
    arr.delete(:updated_at)
    arr += arr.map do |attr|
      { attr => [] }
    end
    params.require(mn).permit(arr)
  end

  def model_params
    mn = @model.to_s.underscore.to_sym
    # {user: {name: "something", other_column_name: "somevalue"}}
    return {} if params[mn].blank?

    # MAKE LIST OF THINGS TO PERMIT:
    arr = @model.attribute_names.map(&:to_sym)
    arr.delete(:id)
    arr.delete(:created_at)
    arr.delete(:updated_at)
    arr.map! { |x| new_val(x) }

    file_set = @model.methods.map(&:to_s).select { |x| x.include?('with_attached') }.map { |x| x[14..-1].to_sym }
    file_set.map! do |attr|
      new_val = params[mn][attr].instance_of?(Array) ? { attr => [] } : attr
      new_val
    end

    file_set.reject! { |x| params[mn][x].blank? }

    stored_attributes = @model.stored_attributes.values.flatten

    arr = arr + file_set + stored_attributes
    p arr

    # Now, permit it!
    params.require(mn).permit(arr)
    # params.require(:user).permit(:name, :column_name, etc., array_type: [], array_type2: [])
  end

  def attach_files
    # @object = ChapterQuestion.find(params[:id])
    # @object.stong_points_images.attach(params[:file])

    # BASED ON ACTIVE STORAGE
    mn = @model.to_s.underscore.to_sym # /users => user
    #
    file_set = @model.methods.map(&:to_s).select { |x| x.include?('with_attached') }.map { |x| x[14..-1].to_sym }
    file_set.each do |fs|
      # For WeChat:
      if params[fs].present?
        attach_file(fs)
        next true
      end

      next unless params[mn].present? && params[mn][fs].present?

      # For WEB or anything NOT WECHAT:
      if params[mn][fs].is_a?(Array) && params[mn][fs].first.is_a?(ActionDispatch::Http::UploadedFile)
        # @object.public_send(fs).attach(params[mn][fs])
      elsif params[mn][fs].is_a?(Array)
        params[mn][fs] = params[mn][fs].reject { |x| x.include?('https') || x.include?('//tmp/') }
      elsif params[mn][fs] == ''
        params[mn].delete(fs)
      end
    end
  end

  def attach_file(key, attempts=3)
    @object.public_send(key).attach(params[key])
  rescue Faraday::ConnectionFailed
    attempts -= 1
    attempts > 0 ? attach_file(key, attempts) : raise(Faraday::ConnectionFailed)
  end

  def new_val(attr)
    val = @model.new.send(attr)
    return { attr => [] } if val == []

    return { attr => {} } if val == {}

    attr
  end
end
