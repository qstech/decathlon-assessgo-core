class PdfChannel < ApplicationCable::Channel
  def subscribed
    stream_from "PdfChannel_#{params[:id]}"
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
  end
end
