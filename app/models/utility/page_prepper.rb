class PagePrepper
  attr_reader :current_user, :params

  def initialize(user, params = {}, object, app_type)
    @current_user = user
    @params = params
    @obj = object
    @app = app_type
    prep_response
  end

  def prepare!(mod)
    if mod == :assessment
      ass_gen = AssessmentPage.new(current_user, @params, @obj, @response, @app)
      cap_gen = CapPage.new(current_user, @params, @obj, @response, @app)
      res = Hash.new { |h, k| h[k] = {} }

      res["members/assessment/index?ref_model=#{@obj.grid}"] = ass_gen.render('index')
      ass_gen.params[:id] = @obj.id
      res["members/assessment/pre-confirmation?id=#{@obj.id}"]  = ass_gen.render('pre_confirmation')
      res["members/assessment/go-for-assessment?id=#{@obj.id}"] = ass_gen.render('go_for_assessment')
      res["members/assessment/overview?id=#{@obj.id}"]          = ass_gen.render('overview')

      if @obj.assess_type != 'CAP Review'
        res["members/assessment/assessment-prep?id=#{@obj.id}"] = ass_gen.render('assessment_prep')
        res["members/assessment/chapter-assessment?id=#{@obj.id}"] = ass_gen.render('chapter_assessment')
        @obj.chapter_assessments.each do |x|
          10.times{puts "IN HERE:  CA ID - #{x.id}"}
          sec = x.section.gsub(/[A-Z]+(\d+)[A-Z]+(\d+)/, '\1.\2')
          lev = x.chapter_tag.split('L').last
          ass_gen.params[:id] = x.id
          ass_gen.params[:section] = sec
          ass_gen.params[:level] = lev
          res["members/assessment/single-chapter?id=#{x.id}&section=#{sec}&level=#{lev}"] =
            ass_gen.render('single_chapter')
        end
      else
        res["members/cap/index?model=quality_assessment&id=#{@obj.id}"] = cap_gen.render('cap_index')
        (0...@obj.cap_points.count).each do |i|
          cap_gen.params[:index] = i
          res["members/cap/form?model=quality_assessment&id=#{@obj.id}&index=#{i}&review=false"]      =
            cap_gen.render('cap_form')
          res["members/cap/followup?model=quality_assessment&id=#{@obj.id}&index=#{i}&review=false"]  =
            cap_gen.render('cap_followup')
        end
      end

      res
    elsif mod == :control_plan
      cp_gen = ControlPlanPage.new(current_user, @params, @obj, @response, @app)
      res = Hash.new { |h, k| h[k] = {} }

      res['members/control_plan/index']                     = cp_gen.render('index')
      res["members/control_plan/review?cp_id=#{@obj.id}"]   = cp_gen.render('review')
      @obj.plan_items.each do |x|
        cp_gen.params[:plan_item_id] = x.id
        res["members/control_plan/review-detail?plan_item_id=#{x.id}"] = cp_gen.render('review_detail')
      end
      res
    end
  end

  private

  def clean_params
    new_params = {}
    if params[:scene]
      query = URI.decode(URI.decode(params[:scene]))
      query.split('&').each do |param|
        set = param.split('=')
        p 'MODIFYING PARAMS'
        params[set[0].to_sym] = set[1]
        new_params[set[0].to_sym] = set[1]
      end
    end
    new_params
  end

  def prep_response
    @response = {
      code: 200,
      status: 'success',
      hide_preload: true,
      today: Time.now.strftime('%Y-%m-%d'),
      load: true,
      navData: {
        active_tab: 0,
        tab_list: tab_list
      },
      navBadge: [0, 0, 0, 0] # the badge number, should be dynaimic
    }
    @response.merge!(clean_params)
  end

  def tab_list
    [
      {
        image: '/images/tabbar/home.png',
        image_sel: '/images/tabbar/home_sel.png',
        name: I18n.t('menu.assess'),
        nav_tab: '/members/home/home'
      },
      {
        image: '/images/tabbar/goes.png',
        image_sel: '/images/tabbar/goes_sel.png',
        name: I18n.t('menu.goes'),
        nav_tab: '/members/activity/activity'
      },
      {
        image: '/images/tabbar/search.png',
        image_sel: '/images/tabbar/search_sel.png',
        name: I18n.t('menu.for'),
        nav_tab: '/members/search/form'
      },
      {
        image: '/images/tabbar/user.png',
        image_sel: '/images/tabbar/user_sel.png',
        name: I18n.t('menu.performance'),
        nav_tab: '/members/me/me'
      }
    ]
  end
end
