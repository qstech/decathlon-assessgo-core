require 'creek'
class ExcelParser
  attr_reader :code

  def initialize(excel_file)
    @file = excel_file.original_filename
    @creek = Creek::Book.new excel_file.path, check_file_extension: false
    @code = @file.match(/\d{2}\d+/).to_s
    @supplier = SupplierProfile.find_by(code: @code)
    @results = {}
    @finals = []
  end

  def parse_excel
    analyze_excel
  rescue NoMethodError
    { code: 200, msg: "FAILED TO PARSE EXCEL. Check if formatting of the
      excel conforms to the 2.3 CP standard." }
  rescue Zip::Error
    { code: 200, msg: 'FAILED TO PARSE EXCEL. Is the file empty?' }
  rescue RuntimeError
    { code: 200, msg: "FAILED TO PARSE EXCEL. Are you sure this is an XLSX
      file type?" }
  end

  def analyze_excel
    puts "Analyzing #{@file}"

    return { code: 200, msg: "Supplier not found for the code: #{@code}" } if @supplier.blank?

    puts "SUPPLIER FOUND! #{@code}"
    puts "PARSING EXCEL FOR #{@supplier.name}"
    i = 0
    @creek.sheets.each do |sheet|
      test_set = sheet.rows_with_meta_data.find { |x| x['cells'].values.join.include?('WHAT IS THE STANDARD') }
      next if test_set.blank?

      puts 'PASSED TEST SET'

      i += 1
      # INITIALIZE
      @tmp_process = {}
      @category = ''
      @key = ''
      @process = ProductProcess.new
      @step = ProcessStep.new
      @step_params = {}
      puts 'STARTING META DATA PARSE'
      sheet.rows_with_meta_data.each do |row|
        row['cells'] = row['cells'].reject { |k, _v| k > "T#{row['r']}" || k.size > (row['r'].to_i + 1) }
        full_cells = row['cells'].reject { |_k, v| v.nil? }
        next if full_cells.blank?

        puts "ROW: #{row['r']}"
        p row
        merged = full_cells.values.join.gsub(/(\W+\s*)/, ' ').strip.capitalize
        merged = 'Incoming control' if merged == 'In coming control'
        merged = 'Final control' if merged == 'FQC'

        if full_cells.values.join.include?('PRODUCT designation') && @key.blank?
          @key = full_cells.reject { |_k, v| !v.include?('PRODUCT designation') }.keys.first
          @key = "#{@key[0]}#{@key[1].to_i + 1}"
        end

        if full_cells.keys.include?(@key)
          pf_name = full_cells[@key].gsub(/(\s*)/, ' ').strip
          @pf = ProductFamily.find_or_create_by(name: pf_name, supplier_profile_id: @supplier.id)

          15.times { puts '' }
          puts "SUPPLIER: #{@supplier.code}"
          puts "PRODUCT FAMILY: #{@pf.name}"

          @results[@supplier.code] = {} if @results[@supplier.code].nil?
          @results[@supplier.code]['product_families'] = {} if @results[@supplier.code]['product_families'].nil?
          @results[@supplier.code]['product_families'][@pf.name] = { 'processes' => [] }
        end

        if ProductProcess::CATEGORY.include? merged
          @category = merged
          puts "CATEGORY: #{@category}"
          @process_name = @category
          @step_params = {}
        elsif full_cells.keys.count == 1
          p 'PROCESS'
          @process_name = full_cells.values.join.gsub(/(\s*)/, ' ').strip.capitalize
          @process_name = full_cells.values.join.strip if @process_name.blank?
          @step_params[:step_number] = '000'
        else
          next if @category.blank?

          unless @process.name == @process_name
            # p @pf
            @results[@supplier.code]['product_families'][@pf.name]['processes'] << @tmp_process if @tmp_process.present?
            @process = ProductProcess.find_or_create_by(product_family_id: @pf.id, name: @process_name,
                                                        category: @category)
            # p "HERE"
            puts "PROCESS: #{@process.name}"
            @tmp_process = { name: @process.name, category: @process.category, steps: [] }
          end
          set = row['cells'].values.map { |x| x.to_s.strip if x.present? }

          @step_params[:product_process_id] = @process.id
          @step_params[:step_number] = set[0] if set[0].present?
          @step_params[:control_item] = set[1] if set[1].present?
          next if @step_params[:control_item].blank?

          puts "#{row['r']} STEP: #{set[0]} - #{set[1]}" if set[0].present?
          criticity = 'Critical' if set[6].present?
          criticity = 'Major' if set[7].present?
          criticity = 'Minor' if set[8].present?
          # p criticity
          # p@step_params
          p set

          @item = {
            control_method: set[2],
            requirement: set[3],
            criticity: criticity,
            control_type: set[9],
            supplier_control_freq_note: set[10],
            supplier_controller: set[11],
            decathlon_control_freq_note: set[12],
            sampling: set[15],
            acceptance_rules: set[16],
            reaction_mode: set[17],
            active: true
          }
          p @item

          ps = ProcessStep.find_or_create_by(@step_params.merge(@item))
          puts "ITEM: #{ps.control_method}"
          @tmp_process[:steps] << @step_params.merge(@item).except(:product_process_id)
        end
      end

      # filepath = "#{Rails.root}/db/data/product_families"
      @results[@supplier.code]['product_families'][@pf.name]['processes'] << @tmp_process if @tmp_process.present?

      @supp_json = { 'filename' => @file,
                     'count' => @results[@supplier.code]['product_families'][@pf.name]['processes'].count }
      @supp_json.merge!({ 'product_family' => { 'name' => @pf.name,
                                                'processes' => @results[@supplier.code]['product_families'][@pf.name]['processes'] } })
      @finals << @supp_json
    end
    @finals
  end

  def self.seed_product(content, code)
    supp = SupplierProfile.find_by(code: code)
    if supp.blank?
      puts "SUPPLIER NOT FOUND! #{code}"
      return false
    end

    puts "SEEDING: #{code}"

    pf_hash = content['product_family']

    if pfs = ProductFamily.where(supplier_profile_id: supp.id, name: pf_hash['name'].strip, destroyed_at: nil)
      pfs.update_all(destroyed_at: Time.now)
    end
    pf = ProductFamily.create(supplier_profile_id: supp.id, name: pf_hash['name'].strip)
    pf_hash['processes'].each do |process|
      process['name'] = process['category'] if process['name'].blank?
      pr = ProductProcess.find_or_create_by(product_family_id: pf.id, name: process['name'],
                                            category: process['category'])

      process['steps'].each do |step|
        fh = {
          step_number: step['step_number'],
          control_item: step['control_item'],
          control_method: step['control_method'],
          product_process_id: pr.id
        }
        ps = ProcessStep.find_or_create_by(fh)
        step['updated_at'] = Time.now
        ps.update(step)
      end
    end
  end
end
