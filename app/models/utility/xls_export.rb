# frozen_string_literal: true

require 'rubyXL'
require 'rubyXL/convenience_methods'

class XlsExport
  ACTIVE_GRIDS = %w[QA SSE HRP ENV SCM PUR OPEX HFWB DPR].freeze
  attr_accessor :to_excel, :file_type, :filename
  attr_reader :qa, :result

  def initialize(assessment, cap = false)
    @cap = cap
    @qa = assessment
    @pls = AssessorProfile.where(id: @qa.assessment_day_form.pl_set.push(@qa.assessment_day_form.pl_id).reject(&:nil?))
    @result = @qa.cap_needed_label

    return if @cap

    @file_type = set_file_type
    @filename = "#{Rails.root}/lib/grids/#{assessment.product_grid || assessment.grid}"
    @to_excel = {}
    prep_for_excel
    set_workbook
    @workbook.update_sheets(@to_excel)
  end

  def cap_review
    @to_excel = {}
    @workbook = RubyXL::Parser.parse("#{Rails.root}/lib/grids/cap_review.xlsx")
    XlsExport::CapReview.prepare!(self)
  end

  def set_workbook
    case @file_type
    when :xlsx
      @workbook = XlsExport::XlsxAdapter.new(@filename)
    when :xls
      @workbook = XlsExport::XlsAdapter.new(@filename)
    end
  end

  def prep_for_excel
    # THIS METHOD PROVIDES A MAP OF WHAT TO CHANGE
    case (@qa.product_grid || @qa.grid)
    when 'quality_assessment'
      XlsExport::Qa.prepare!(self)
    when 'sse_assessment'
      XlsExport::Sse.prepare!(self)
    when 'hrp_assessment'
      XlsExport::Hrp.prepare!(self)
    when 'pur_assessment'
      XlsExport::Pur.prepare!(self)
    when 'env_assessment'
      XlsExport::Env.prepare!(self)
    when 'opex_assessment'
      XlsExport::Opex.prepare!(self)
    when 'scm_assessment'
      XlsExport::Scm.prepare!(self)
    when 'dpr_assessment'
      XlsExport::DprTextile.prepare!(self)
    when 'hfwb_assessment'
      XlsExport::Hfwb.prepare!(self)
    end
  end

  def export!
    @workbook.stream.string
  end

  private

  def set_file_type
    if @qa.hrp? && (@qa.grid_version.version_number == 1)
      :xls
    else
      :xlsx
    end
  end
end
