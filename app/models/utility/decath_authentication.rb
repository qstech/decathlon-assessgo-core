require 'rest-client'
require 'jwt'
require 'mechanize'

class DecathAuthentication < ApplicationRecord
  self.abstract_class = true
  # THIS CLASS is made up of helper methods that can create
  AUTH_URL = Rails.application.credentials.dig(:decath, :auth_uri)
  TOKEN_URL = Rails.application.credentials.dig(:decath, :token_uri)

  CLIENT_ID = Rails.application.credentials.dig(:decath, :client_id)
  CLIENT_SECRET = Rails.application.credentials.dig(:decath, :client_secret)
  REDIRECT = Rails.application.credentials.dig(:decath, :redirect_uri)

  WEB_ID = Rails.application.credentials.dig(:decath_web, :client_id)
  WEB_SECRET = Rails.application.credentials.dig(:decath_web, :client_secret)
  WEB_REDIRECT = Rails.application.credentials.dig(:decath_web, :redirect_uri)

  def self.timer
    start_time = Time.now
    yield
    total = Time.now - start_time
    puts "TIME: #{total} seconds"
  end

  def self.oauth_login(username, password, *openid)
    openid = openid.first
    puts "OPENID SET: #{openid}"
    agent = Mechanize.new
    code = retrieve_code(agent, username, password)
    if code == 'SKIP'
      @user = assign_user(username)
      @user.update(wx_openid: openid['openid']) if openid['openid'].present?
      @user.update(unionid: openid['unionid']) if openid['unionid'].present?
      @user.update(access_levels: ['reports']) if @user.access_levels.blank?
      @user.update(authenticated_status: 'success!')
    elsif code
      @user = assign_user(username)
      @user.update(wx_openid: openid['openid']) if openid['openid'].present?
      @user.update(unionid: openid['unionid']) if openid['unionid'].present?
      @user.update(access_levels: ['reports']) if @user.access_levels.blank?
      @user.update(authenticated_status: 'Syncing profile...')

      puts code
      auth = fulfill_oauth(code)
      puts auth
      user_info = format_response(auth)
      ass = @user.assessor_profile
      ass.update(name: user_info[:name], city: user_info[:city], country: user_info[:country], title: user_info[:title])
      user_params = {
        email: user_info[:email], authenticated_status: 'Profile Synced!',
        refresh_token: auth['refresh_token'], authenticated_until: (Time.now + 1.months)
      }
      @user.update(user_params)
      @user.update(authenticated_status: 'success!')
    else
      @user = assign_user(username)
      @user.update(authenticated_status: 'ERR')
    end
  end

  def self.verify_only(username, password)
    agent = Mechanize.new
    code = retrieve_code(agent, username, password)
    if code
      p code
      @user = User.find_for_authentication(username: username)
      @user.update(authenticated_status: '')
      true
    else
      false
    end
  end

  def self.assign_user(username)
    @user = User.find_for_authentication(username: username)
    if @user.blank?
      random_pass = SecureRandom.urlsafe_base64
      @user = User.create(username: username, password: random_pass)
    end

    if @user.assessor_profile.blank?
      ass_params = { user_id: @user.id, handle: username }
      ass = AssessorProfile.new(ass_params)
      @user.assessor_profile = ass
    end
    @user
  end

  def self.retrieve_code(agent, username, password)
    params = {
      client_id: CLIENT_ID,
      response_type: 'code',
      redirect_uri: REDIRECT,
      scope: 'openid profile email',
      state: 'AssessGo'
    }
    @user = User.find_for_authentication(username: username)
    @user.update(authenticated_status: 'Connecting to Decathlon Login System...') if @user.present?

    @redirpage = verify_credentials(@user, agent, username, password, params)
    @redirpage = handle_html(agent, @redirpage, @user) if @redirpage.body.include?('html')

    return false if @redirpage == false

    if @redirpage.body.include?('html')
      'SKIP'
    else
      puts JSON.parse(@redirpage.body)
      JSON.parse(@redirpage.body)['code']
    end
  end

  def self.verify_credentials(user, agent, username, password, params)
    timer do
      puts 'Loading Login Page...'
      @login_page = agent.get(AUTH_URL, params)
    end

    user.update(authenticated_status: 'Verifying username & password...') if user.present?

    timer do
      puts 'Verifying Credentials...'
      login_form = @login_page.form
      login_form.field_with(name: 'pf.username').value = username
      login_form.field_with(name: 'pf.pass').value = password
      @redirpage = agent.submit(login_form)
    end
    @redirpage
  end

  def self.handle_html(agent, page, user)
    if page.body.include?('<noscript><input type="submit" value="Resume"/></noscript>')
      form = page.form
      agent.submit(form)
    elsif page.body.include?('<span class="sr-only">Error:</span>')
      puts 'FAILED -- '
      puts 'FAILED -- '
      puts 'FAILED -- '
      puts 'FAILED -- '
      puts page.body
      user.update(authenticated_status: 'Username or Password Incorrect!') if user.present?
      false
    else
      puts 'INVESTIGATE -- '
      puts 'INVESTIGATE -- '
      puts 'INVESTIGATE -- '
      puts 'INVESTIGATE -- '
      puts page.body
      user.update(authenticated_status: 'Username or Password Incorrect!') if user.present?
      false
    end
  end

  def self.fulfill_oauth(code)
    params = {
      grant_type: 'authorization_code',
      client_id: CLIENT_ID,
      client_secret: CLIENT_SECRET,
      code: code,
      redirect_uri: REDIRECT,
      state: 'AssessGo'
    }
    timer do
      puts 'Sending Code...'
      post_res = RestClient.post(TOKEN_URL, params)
      @auth = JSON.parse(post_res.body)
    end
    @auth
  end

  def self.refresh_token(token)
    params = {
      grant_type: 'refresh_token',
      client_id: CLIENT_ID,
      client_secret: CLIENT_SECRET,
      refresh_token: token,
      redirect_uri: REDIRECT,
      state: 'AssessGo'
    }
    timer do
      puts 'Sending Code...'
      post_res = RestClient.post(TOKEN_URL, params)
      @auth = JSON.parse(post_res.body)
    end
    @auth
  end

  def self.format_response(auth)
    id = JWT.decode auth['id_token'], nil, false
    puts JWT.decode auth['id_token'], nil, false
    puts JWT.decode auth['access_token'], nil, false

    id = id.first
    {
      code: 200,
      refresh_token: auth['refresh_token'],
      username: id['uid'].downcase,
      name: id['displayName'].titleize,
      email: id['mail'].downcase,
      country: id['c'],
      title: id['title'],
      login: true
    }
  end

  def self.oauth_path
    "#{AUTH_URL}?client_id=#{CLIENT_ID}&response_type=code&redirect_uri=#{REDIRECT}&scope=openid%20profile%20email&state=AssessGo"
  end

  def self.web_oauth_path
    "#{AUTH_URL}?client_id=#{WEB_ID}&response_type=code&redirect_uri=#{WEB_REDIRECT}&scope=openid%20profile%20email&state=AssessGo"
  end

  def self.fulfill_oauth_web(code)
    params = {
      grant_type: 'authorization_code',
      client_id: WEB_ID,
      client_secret: WEB_SECRET,
      code: code,
      redirect_uri: WEB_REDIRECT,
      state: 'AssessGo'
    }
    timer do
      puts 'Sending Code...'
      post_res = RestClient.post(TOKEN_URL, params)
      @auth = JSON.parse(post_res.body)
    end
    @auth
  end
end
