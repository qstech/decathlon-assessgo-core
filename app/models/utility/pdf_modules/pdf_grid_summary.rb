class PdfGenerator
  # ============================================================================
  # ============================================================================
  #                         GRID SUMMARY
  # ============================================================================
  # ============================================================================

  def render_report_summary(pdf)
    report_summary_legend(pdf)
    render_summary_grid(pdf)
  end

  def report_summary_legend(pdf)
    pdf.move_down RHYTHM

    legend = if @qa.grid == 'hrp_assessment'
               { 'NOK' => RED, 'OK' => GREEN, 'EP' => EP_YELLOW, 'N/A or Not Assessed' => EP_YELLOW,
                 'Not Existant For Section' => MED_GRAY }
             else
               { 'NOK' => RED, 'OK' => GREEN, 'N/A or Not Assessed' => EP_YELLOW,
                 'Not Existant For Section' => MED_GRAY }
             end
    adjustment = 0
    # 10.times{puts ""}
    # puts "LEFT: #{pdf.bounds.left}; RIGHT: #{pdf.bounds.right}"
    # puts "ABS LEFT: #{pdf.bounds.absolute_left}; ABS RIGHT: #{pdf.bounds.absolute_right}"
    legend.each_with_index do |(k, v), _i|
      left = adjustment + 30
      width = 26 + 15 * k.count(('A'..'Z').to_a.join) + 8 * k.count(('a'..'z').to_a.join + '/')
      if left + width > (pdf.bounds.right + pdf.bounds.left)
        pdf.move_down 20
        adjustment = width
        left = 30
      else
        adjustment += width
      end
      # puts "left--#{left}; width--#{width}"
      top = pdf.cursor
      start = [left, top]
      pdf.fill_color = v
      pdf.fill_rectangle start, 18, 18
      pdf.fill_color DARK_GRAY
      pdf.text_box k, at: [left + 26, top], width: 15 * k.length, height: 18, color: DARK_GRAY, align: :left,
                      valign: :center, size: 16
    end
    pdf.move_down(RHYTHM * 4)
  end

  def render_summary_grid(pdf)
    grid_set = summary_grid
    summary_chapter_labels(pdf)
    pdf.move_down RHYTHM / 2
    grid_set.each do |chapter|
      summary_chapter_title(pdf, chapter)
      # summary_chapter_labels(pdf)
      summary_chapter(pdf, chapter)
      pdf.move_down RHYTHM * 2
    end
  end

  def summary_chapter_title(pdf, chapter)
    pdf.fill_color DARK_GRAY
    pdf.text chapter[:title].upcase, size: 14, align: :left, style: :bold
    pdf.move_down 5
  end

  def summary_chapter_labels(pdf)
    doc_width = pdf.bounds.absolute_right - pdf.bounds.absolute_left
    title_width = 200
    grid_height = 27
    grid_width  = (doc_width - title_width) / 5

    top = pdf.cursor
    labels = %w[E D C B Result]
    labels[-1] = 'A' if @qa.dpr?

    labels.each_with_index do |ltr, i|
      pt = [title_width + (grid_width * i), top]
      pdf.fill_color(LEVEL_COLORS[ltr] || BLACK)
      pdf.text_box(ltr, at: pt,
                        width: grid_width, height: grid_height,
                        align: :center, valign: :center, size: 16)
    end
    pdf.move_down grid_height
  end

  def summary_chapter(pdf, chapter)
    doc_width = pdf.bounds.absolute_right - pdf.bounds.absolute_left
    title_width = 200
    grid_height = 27
    grid_width  = (doc_width - title_width) / 5
    pad_h = 12 # padding for colored squares
    pad_v = 4 # padding for colored squares

    chapter[:set].each do |item|
      if pdf.cursor < (grid_height)
        pdf.start_new_page
        summary_chapter_labels(pdf)
      end
      pt = [pdf.bounds.left, pdf.cursor]
      pdf.fill_color DARK_GRAY
      pdf.text_box item[:title].split(' (').first, at: pt, width: title_width, height: grid_height,
                                                   align: :left, valign: :center, size: 12, style: :normal

      pdf.bounding_box([pdf.bounds.left + title_width, pdf.cursor],
                       width: (doc_width - title_width), height: grid_height) do
        pdf.stroke_color MED_GRAY
        pdf.stroke_horizontal_line pdf.bounds.left, pdf.bounds.right, at: pdf.bounds.top
        pdf.stroke_horizontal_line pdf.bounds.left, pdf.bounds.right, at: pdf.bounds.bottom

        item[:results].each_with_index do |x, i|
          case x
          when 'OK'               then pdf.fill_color(GREEN)
          when 'NOK'              then pdf.fill_color(RED)
          when 'NOK without risk' then pdf.fill_color(MED_DARK_GRAY)
          when 'NOK (EP)'         then pdf.fill_color(MED_DARK_GRAY)
          when 'Not Assessed'     then pdf.fill_color(EP_YELLOW)
          when nil                then pdf.fill_color(MED_GRAY)
          else                         pdf.fill_color(EP_YELLOW)
          end

          pt_x = pdf.bounds.left + (i * grid_width) + pad_h
          pt_y = pdf.bounds.top - pad_v
          pt = [pt_x, pt_y]
          pdf.fill_rectangle(pt, grid_width - (2 * pad_h), grid_height - (2 * pad_v))

          next if x.nil?
        end

        unless @qa.dpr?
          x = pdf.bounds.left + (4 * grid_width) + pad_h
          y = pdf.bounds.top - pad_v

          pdf.fill_color(LEVEL_COLORS[item[:level]] || MED_GRAY)
          pdf.text_box item[:level], at: [x, y], size: grid_height - (2 * pad_v), style: :bold,
                                     align: :center, valign: :center
        end
        pdf.move_down grid_height
      end
    end
  end

  def summary_grid
    to_grid = { 'E' => 0, 'D' => 1, 'C' => 2, 'B' => 3, 'A' => 4 }
    grid_set = []
    @scores[:chapters].each do |ch|
      chapter = {
        title: "#{ch[:section_name]} #{ch[:title].split(' - ').first}",
        set: []
      }
      sections = ch[:sections]
      first_sec = ch[:sections].first
      sections = first_sec[:subscores] if @qa.dpr?
      sections.each do |sec|
        if @qa.dpr?
          results = [nil, nil, nil, nil, nil]
          pt = sec[:points].first
          title = "#{pt[:section_name]} #{pt[:title]}"
          (sec[:to]..sec[:from]).to_a.each do |lev|
            i = to_grid[lev]
            results[i] = sec[:score]
          end
        else
          results = [nil, nil, nil, nil]
          title = "#{sec[:section_name]} #{sec[:title]}"
          sec[:subscores].each do |lev|
            next if lev[:points].blank?

            i = to_grid[lev[:level]]
            results[i] = lev[:score]
          end
        end
        chapter[:set] << {
          title: title, cotation: sec[:cotation],
          level: sec[:level], results: results
        }
      end
      grid_set << chapter
    end
    grid_set.sort_by { |x| x[:title] }
  end
end
