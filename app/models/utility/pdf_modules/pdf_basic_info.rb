class PdfGenerator
  # ============================================================================
  # ============================================================================
  #                   RENDER ASSESSMENT BASIC INFO
  # ============================================================================
  # ============================================================================
  def render_prep_questions(pdf)
    render_day_form(pdf)
    pdf.move_down 25

    # box_width = ((pdf.bounds.absolute_right-pdf.bounds.absolute_left)*0.6).to_i
    form = @qa.prep_questions_form
    full_length = false
    form[:form][:form_questions].each_with_index do |fq, i|
      last = (i + 1) == form[:form][:form_questions].length
      response = fq[:response]
      response = [response.join(', ')] if response.is_a? Array
      response = [response] if response.is_a? String
      response = ['N/A'] if response.nil?

      offset = (response.count * 17) + 12
      # full_length = true if pdf.cursor < bottom && !full_length
      pdf.start_new_page if (pdf.cursor + MARGIN_BOTTOM) < (pdf.bounds.absolute_bottom + offset)

      # if full_length
      form_response(pdf: pdf, question: "#{i + 1}. #{fq[:label]}", response: response, last: last, full_page: true,
                    attachments: fq[:attachments])
      # else
      #   pdf.bounding_box([pdf.bounds.left, pdf.cursor], width: box_width) do
      #     form_response(pdf: pdf, question: "#{i+1}. #{fq[:label]}", response: response, last: last, full_page: true, attachments: fq[:attachments])
      #   end
      # end
    end
  end

  def render_day_form(pdf)
    form = @qa.go_for_assessment_form
    lines = form[:form][:form_questions].map do |x|
      [20, 12 * calc_lines(x[:label], 60), 12 * calc_lines(x[:response], 60), 20]
    end.flatten.sum * 6 / 10
    yellow_height = lines
    render_small_yellow_box(pdf, yellow_height) do
      pdf.indent(10) { pdf.text('Go For Assessment', style: :bold, size: 14) }
      pdf.move_down 5
      ref_point = pdf.cursor
      next_ref  = pdf.cursor
      form[:form][:form_questions].each_with_index do |fq, i|
        last = (i + 1) == form[:form][:form_questions].length
        width = (pdf.bounds.right - pdf.bounds.left)

        if i.even?
          pt = [pdf.bounds.left, ref_point]
          pdf.bounding_box pt, width: width / 2 do
            pdf.indent(5) do
              form_response(pdf: pdf, question: "#{i + 1}. #{fq[:label]}", response: fq[:response], last: last,
                            full_page: false)
            end
          end
          ref_point = pdf.cursor
        else
          pt = [pdf.bounds.left + (width / 2), next_ref]
          pdf.bounding_box pt, width: width / 2 do
            form_response(pdf: pdf, question: "#{i + 1}. #{fq[:label]}", response: fq[:response], last: last,
                          full_page: false)
          end
          next_ref = pdf.cursor
        end
      end
    end
  end

  def form_response(pdf:, question:, response:, last: false, full_page: true, attachments: [])
    response = [response.join(', ')] if response.is_a? Array
    response = [response] if response.is_a? String
    response = ['N/A'] if response.nil? || response == ''

    pdf.font('Helvetica', size: 12, style: :italic)
    pdf.text(question, color: DARK_GRAY)

    response.each do |item|
      pdf.move_down 5
      pdf.font('Microhei', size: 12, style: :bold)
      pdf.indent(15) { pdf.text(item, color: BLACK) } if full_page
      pdf.indent(5) { pdf.text(item, color: BLACK) } unless full_page
    end
    if attachments.present?
      pdf.text('Attachments', color: DARK_GRAY)
      attachments.each_with_index do |attachment, i|
        pdf.indent(5) do
          pdf.text("<u>#{i + 1} <link href='#{attachment}'>Attachment ##{i + 1}</link></u>", color: BLUE,
                                                                                             inline_format: true)
        end
      end
    end

    pdf.move_down RHYTHM unless last
  end
end
