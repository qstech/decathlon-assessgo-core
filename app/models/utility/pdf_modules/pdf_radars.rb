class PdfGenerator
  # ============================================================================
  # ============================================================================
  #             RADAR CHART METHODS
  # ============================================================================
  # ============================================================================

  def render_overall_radar(pdf)
    pdf.text 'Total Result', size: 14, style: :bold, align: :center
    pdf.move_down RHYTHM * 2
    create_qa_radar_chart(pdf, @radars[:total])
  end

  def render_cp_radar(pdf, i)
    case i
    when 0 then text = 'Process'
    when 1 then text = 'Control Type'
    when 2 then text = 'Category'
    end
    pdf.text "Nonconformity By #{text}", size: 14, style: :bold, align: :center
    pdf.move_down RHYTHM * 2
    create_cp_radar_chart(pdf, @radars[i])
  end

  def render_chapter_radar(pdf, chapter)
    pdf.text "#{chapter[:section_name]} Result", size: 14, style: :bold, align: :center
    pdf.move_down RHYTHM * 2
    ch_radar = @qa.radars_for_chapter(chapter)
    create_qa_radar_chart(pdf, ch_radar)
  end

  def create_qa_radar_chart(pdf, radars)
    count = radars[:points].count
    padding = 20
    page_width = (pdf.bounds.right - pdf.bounds.left)
    radar_width = page_width - (2 * padding)
    radius = radar_width / 2
    origin = [pdf.bounds.left + radius + padding] # x-value
    origin << (pdf.cursor - radius)
    coordinate_series = get_radar_coordinate_series(count)

    radar_points = coordinate_series.map { |angle| radar_point_for(origin, radius, angle) }

    opt = {
      origin: origin,
      radius: radius,
      coordinate_series: coordinate_series,
      radar_points: radar_points,
      radars: radars,
      txt_size: radius / 10
    }
    opt[:axis_markers] = get_axis_points(pdf, opt)

    draw_radar_axis(pdf, opt)
    if @qa.assess_type == 'CAP Review'
      draw_radar(pdf, radars[:points], :blue, opt)
    else
      draw_radar(pdf, radars[:cotations], :blue, opt)
      draw_radar(pdf, radars[:points], :red, opt)
    end

    pdf.move_down radar_width
    pdf.move_down RHYTHM * 4
    draw_radar_legend(pdf, opt)
    # pdf.fill_and_stroke_polygon [297, 672], [375, 635], [394, 551], [340, 484], [254, 484], [200, 551], [220, 635]
  end

  def create_cp_radar_chart(pdf, radars)
    count = radars[:data].count
    padding = 20
    page_width = (pdf.bounds.right - pdf.bounds.left)
    radar_width = page_width - (2 * padding)
    radius = radar_width / 2
    origin = [pdf.bounds.left + radius + padding] # x-value
    origin << (pdf.cursor - radius)
    coordinate_series = get_radar_coordinate_series(count)

    radar_points = coordinate_series.map { |angle| radar_point_for(origin, radius, angle) }

    opt = {
      origin: origin,
      radius: radius,
      coordinate_series: coordinate_series,
      radar_points: radar_points,
      radars: radars,
      txt_size: radius / 10
    }
    opt[:axis_markers] = get_axis_points(pdf, opt)

    draw_radar_axis(pdf, opt)
    draw_radar(pdf, radars[:data], :red, opt)
  end

  def draw_radar(pdf, radar, color, opt)
    colors = { blue: '0198F1', green: '568D77', yellow: 'FECC45', aqua: '54C4C0', red: 'F2735D' }
    radar_points = opt[:coordinate_series].map.with_index do |angle, i|
      radar_point_for(opt[:origin], (opt[:radius] * radar[i] / 100), angle)
    end

    pdf.fill_color colors[color]
    pdf.stroke_color colors[color]
    pdf.undash
    pdf.line_width = opt[:radius] * 0.02
    if radar_points.length > 2
      pdf.transparent(0.5) { pdf.fill_polygon(*radar_points) }
      pdf.stroke_polygon(*radar_points)
    else
      radar_points.each do |pt|
        pt[0] = pt[0] - 2
        pt[1] = pt[1] + 2
        pdf.transparent(0.5) { pdf.fill_rectangle(pt, 4, 4) }
        pdf.stroke_rectangle(pt, 4, 4)
      end
    end
  end

  def draw_radar_legend(pdf, opt)
    w = opt[:radius] * 2
    h = opt[:radius] / 2

    o = [pdf.bounds.left + (INNER_MARGIN / 2), pdf.cursor - (opt[:radius] + INNER_MARGIN)]
    pdf.bounding_box(o, width: w, height: h) do
      pdf.stroke_color DARK_GRAY
      pdf.stroke_rounded_rectangle o, w, h, 5
      p_x = h
      p_y = h / 6
      inner_o = [o[0] + p_x, o[1] - p_y]
      inner_w = w - (2 * p_x)
      inner_h = h - (2 * p_y)
      pdf.bounding_box(inner_o, width: inner_w, height: inner_h) do
        txt_size = inner_h / 4

        bx_w = inner_h
        bx_h = txt_size / 2
        bx_o = [pdf.bounds.left, pdf.bounds.top - (bx_h / 2)]

        pdf.fill_color '0198F1'
        pdf.fill_rectangle bx_o, bx_w, bx_h

        bx_o = [bx_o[0], bx_o[1] - (inner_h * 2 / 3)]

        pdf.fill_color 'F2735D'
        pdf.fill_rectangle bx_o, bx_w, bx_h

        pdf.fill_color DARK_GRAY
        txt_o = [pdf.bounds.left + bx_w + txt_size, pdf.bounds.top]

        pdf.text_box 'Cotation', at: txt_o, size: txt_size, style: :normal

        txt_o = [txt_o[0], txt_o[1] - (inner_h * 2 / 3)]

        pdf.text_box 'Level', at: txt_o, size: txt_size, style: :normal
      end
    end
  end

  def draw_radar_axis(pdf, opt)
    pdf.stroke_color DARK_GRAY
    pdf.fill_color DARK_GRAY

    pdf.line_width = opt[:radius] * 0.01

    pdf.transparent(0.5) do
      # dashes from center out
      opt[:radar_points].each do |pt|
        pdf.dash(5, space: 2.5, phase: 0)
        pdf.stroke_line(opt[:origin], pt)
      end

      # Set Axis Dash Level
      pdf.dash(5, space: 2.5, phase: 0)

      # Stroke Axis Levels
      axis_bounds = get_axis_boundaries(pdf, opt)
      axis_bounds.each { |set| pdf.stroke_polygon(*set) }
      # pdf.stroke_polygon(*opt[:radar_points]) # OUTER BOUNDARY ONLY
    end

    pdf.transparent(0.8) do
      draw_axis_labels(pdf, opt)

      # CHAPTER LABELS
      pdf.font('Microhei', size: opt[:txt_size], style: :normal)
      label_points = opt[:coordinate_series].map { |angle| radar_point_for(opt[:origin], opt[:radius] * 1.1, angle) }
      label_points.each_with_index do |pt, i|
        adjust_x = opt[:radius] * 0.07
        adjust_y = opt[:radius] * 0.035
        pdf.text_box(opt[:radars][:labels][i], at: [pt[0] - adjust_x, pt[1] + adjust_y], size: opt[:txt_size],
                                               style: :normal)
      end
    end
  end

  def get_axis_points(_pdf, opt)
    axis_markers = []
    4.times do |i|
      axis_markers << radar_point_for(opt[:origin], (opt[:radius] * i) / 4, opt[:coordinate_series].first)
    end
    axis_markers << opt[:radar_points].first
    axis_markers
  end

  def get_axis_boundaries(_pdf, opt)
    axis_bounds = []
    4.times do |i|
      axis_bounds << opt[:coordinate_series]
                     .map { |x| radar_point_for(opt[:origin], (opt[:radius] * (i + 1)) / 4, x) }
    end
    axis_bounds
  end

  def draw_axis_labels(pdf, opt)
    axis_labels = ['0 (E)', '25 (D)', '50 (C)', '75 (B)', '100 (A)'] if @qa.present?
    axis_labels = %w[0 25 50 75 100] if @cp.present?

    opt[:axis_markers].each_with_index do |pt, i|
      # pdf.line_width = 1
      # pdf.stroke_line([pt[0]-3, pt[1]], [pt[0]+3, pt[1]]) # Dash up first vertical
      pdf.text_box(axis_labels[i], at: [pt[0] + 3, pt[1] + 4], size: opt[:txt_size], style: :normal)
    end
  end

  def get_radar_coordinate_series(length)
    each_angle = 2 * Math::PI / length
    coordinate_series = []
    length.times do |i|
      coordinate_series << each_angle * i
    end

    coordinate_series.map do |item|
      -1 * item + Math::PI / 2
    end
  end

  def radar_point_for(origin, radius, angle)
    x = origin[0] + (radius * Math.cos(angle))
    y = origin[1] + (radius * Math.sin(angle))
    [x.to_i, y.to_i]
  end
end
