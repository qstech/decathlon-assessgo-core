class PdfGenerator
  # ============================================================================
  # ============================================================================
  #                   RENDER ASSESSMENT DETAILS
  # ============================================================================
  # ============================================================================

  def render_chapter_assessments(pdf)
    scores = @scores
    first = true
    scores[:chapters].each do |ch|
      next if ch[:sections].blank?

      unless first
        pdf.start_new_page
        header_for(pdf, 'Chapter Assessments')
        content(pdf, 'Chatper Assessments', @qa.assess_type) { p pdf.cursor }
      end
      pdf.move_down RHYTHM
      radar_result_page(pdf, ch)
      first = false
      pdf.start_new_page if @qa.dpr?
      ch[:sections].each do |sec|
        sec[:subscores].each_with_index do |lev, i|
          next if lev[:cotation].nil? || ['N/A', 'Not Applicable', 'Not Assessed', nil].include?(lev[:score])

          pdf.start_new_page unless @qa.dpr?

          chapter_level(pdf, ch, sec, lev) if !@qa.dpr? || i.zero?
          lev[:points].each do |pt|
            pt[:level] = lev[:level]
            pdf.start_new_page if (pdf.cursor + MARGIN_BOTTOM) < (pdf.bounds.absolute_bottom + section_height(pt))
            trans = 1
            trans = 0.65 if pt[:last_assessment]

            pdf.transparent(trans) do
              point_header(pdf, pt)
              chapter_point(pdf, pt)
            end
          end
        end
      end
    end
  end

  def chapter_level(pdf, _ch, sec, lev)
    # pdf.move_down 10
    box_width = ((pdf.bounds.absolute_right + pdf.bounds.absolute_left)).to_i
    pdf.bounding_box([-pdf.bounds.absolute_left, pdf.bounds.top + PAGE_MARGIN], width: box_width) do
      pdf.fill_color OFFWHITE
      pdf.fill_rectangle [pdf.bounds.left, pdf.bounds.top], pdf.bounds.right, 30
      pdf.fill_color DARK_GRAY
      pdf.font('Helvetica', size: 16, style: :bold)

      unless @qa.dpr?
        pdf.text_box lev[:level], at: [pdf.bounds.right - 30, pdf.bounds.top - 5], width: 18, height: 18,
                                  color: DARK_GRAY, align: :right, valign: :center
      end

      pdf.pad(6) do
        pdf.indent(20) do
          pdf.font('Helvetica', size: 14, style: :normal)
          pdf.formatted_text [
            { color: DARK_GRAY, text: 'Section: ' },
            { styles: [:bold], color: DARK_GRAY, text: "#{sec[:section_name]} #{sec[:title].truncate(32)}      " }
          ]

          pdf.move_down 5
          pdf.formatted_text []
        end
      end
      pdf.move_cursor_to pdf.bounds.bottom
    end
    # pdf.move_up 6
  end

  def offwhite_top(pdf, label, txt = '')
    box_width = ((pdf.bounds.absolute_right + pdf.bounds.absolute_left)).to_i
    pdf.bounding_box([-pdf.bounds.absolute_left, pdf.bounds.top + PAGE_MARGIN], width: box_width) do
      pdf.fill_color OFFWHITE
      pdf.fill_rectangle [pdf.bounds.left, pdf.bounds.top], pdf.bounds.right, 30
      pdf.fill_color DARK_GRAY

      pdf.pad(6) do
        pdf.indent(20) do
          pdf.font('Helvetica', size: 14, style: :normal)
          pdf.formatted_text [
            { color: DARK_GRAY, text: label },
            { styles: [:bold], color: DARK_GRAY, text: txt }
          ]

          pdf.move_down 5
          pdf.formatted_text []
        end
      end
      pdf.move_cursor_to pdf.bounds.bottom
    end
  end

  def chapter_point(pdf, pt)
    pt = pt[:data]
    line_count = 0
    if pt[:strong_points].present? && (pt[:strong_points].strip != 'N/A')
      line_count += calc_lines(pt[:strong_points],
                               90)
    end
    if pt[:points_to_improve].present? && (pt[:points_to_improve].strip != 'N/A')
      line_count += calc_lines(pt[:points_to_improve],
                               90)
    end

    gray_height = (12 * line_count) + 30

    pdf.bounding_box([-pdf.bounds.left, pdf.cursor],
                     width: pdf.bounds.right + pdf.bounds.left,
                     height: gray_height) do
      pdf.fill_color MED_GRAY
      pdf.fill_rectangle([pdf.bounds.left, pdf.bounds.top],
                         pdf.bounds.right,
                         pdf.bounds.top - pdf.bounds.bottom)

      pdf.fill_color DARK_GRAY
      pdf.move_down 5
      pdf.indent(20) do
        none = true
        pdf.font 'Microhei'
        if pt[:strong_points].present? && (pt[:strong_points].strip != 'N/A')
          pdf.formatted_text [{ text: '•', color: GREEN, font: 'Helvetica' },
                              { text: '  Strong Points: ', styles: [:bold] }, { text: pt[:strong_points] }]
          none = false
        end

        if pt[:points_to_improve].present? && (pt[:points_to_improve].strip != 'N/A')
          pdf.formatted_text [{ text: '•', color: RED, font: 'Helvetica' },
                              { text: '  Points to Improve: ', styles: [:bold] }, { text: pt[:points_to_improve] }]
          none = false
          # form_response(pdf: pdf, question: "Points To Improve", response: pt[:points_to_improve])
        end

        pdf.formatted_text [{ text: 'No Details Submitted', font: 'Helvetica', styles: [:italic] }] if none
      end
    end

    if pt[:strong_points_images].present? || pt[:points_to_improve_images].present?
      images = pt[:strong_points_images].map { |x| { type: :strong, img: x } } +
               pt[:points_to_improve_images].map { |x| { type: :improve, img: x } }
      grey_box_imgs(pdf, images)
    end
    pdf.move_down 10
  end

  def render_qa_cap_review(pdf)
    @cap = @qa.cap
    @last_qa = @cap.ref
    @cap_points = @cap.cap_points.order(:id)
    ch = nil
    first = true

    @cap_points.each do |cap_point|
      cq = cap_point.ref
      ch = cq.chapter

      pt = cap_point.calc_point
      pt[:title] = "#{ch}.#{cq.section} #{pt[:title]}"
      pt[:title_size] = :long

      pdf.start_new_page if (pdf.cursor + MARGIN_BOTTOM) < (pdf.bounds.absolute_bottom + section_height(pt))

      point_header(pdf, pt)
      chapter_point(pdf, pt)
      cap_result(pdf, cap_point)
    end
  end

  def cap_result(pdf, cap_point)
    pdf.pad(5) do
      pdf.font('Helvetica', size: 12, style: :bold)
      pdf.indent(5) { pdf.text 'CAP Point', style: :bold }
    end
    render_if_present(pdf, cap_point.root_cause_nc, 'Root Cause for Non Conformity')
    render_if_present(pdf, cap_point.root_cause_nd, 'Root Cause for Non Detection')
    if cap_point.cap_resolutions.present?

      pdf.move_down 10
      pdf.pad(5) do
        pdf.font('Helvetica', size: 12, style: :bold)
        pdf.indent(5) { pdf.text 'CAP Resolutions', color: DARK_GRAY, style: :bold }
      end

      cap_point.cap_resolutions.each do |cap_res|
        render_if_present(pdf, cap_res.action_type, 'Action Type')
        render_if_present(pdf, cap_res.action, 'Action')
        render_if_present(pdf, cap_res.deadline_date, 'Deadline')
        render_if_present(pdf, cap_res.responsible, "Who's responsible?")
        pdf.move_down 10
      end
    end
  end

  # ===============UTILITY METHODS===========

  def section_height(pt, txt_size = 14)
    lines = []
    lines << txt_size
    if pt[:content].present?
      lines += pt[:content].split("\n").reject do |x|
                 x.nil?
               end.map { |x| (x.length / 90.0).ceil * txt_size }.flatten
    end
    if pt[:note_header].present?
      lines += pt[:note_header].split("\n").reject do |x|
                 x.nil?
               end.map { |x| (x.length / 90.0).ceil * txt_size }.flatten
    end
    if pt[:bullets].present?
      lines += pt[:bullets].reject do |x|
                 x.nil?
               end.map { |x| (x.length / 90.0).ceil * txt_size }.flatten
    end
    lines << 30
    lines << 10
    lines.sum
  end

  # Gray box for Chapter Section Point Descriptions
  # e.g. D-1, D-2, etc.
  def point_header_content(pdf, pt)
    w = 40
    w = 55 if pt[:title_size] == :long
    pdf.bounding_box([pdf.bounds.left, pdf.bounds.top],
                     width: pdf.bounds.right + pdf.bounds.left - w) do
      pdf.pad(7) do
        pdf.indent(w + 5) do
          pdf.fill_color DARK_GRAY
          # pdf.font('Helvetica', size: 14, style: :bold)
          # pdf.text(pt[:title], color: DARK_GRAY)
          # pdf.move_down 10

          pdf.font('Microhei', size: 12)
          pdf.text(pt[:content] || pt[:title], color: DARK_GRAY)
          pdf.text(pt[:note_header], color: DARK_GRAY) if pt[:note_header].present?
          pt[:bullets].each do |bullet|
            pdf.indent(10) { pdf.text(bullet, color: DARK_GRAY) }
          end
        end
      end
    end
  end

  def point_header(pdf, pt)
    pdf.move_down 20
    # pdf.start_new_page if (pdf.cursor + MARGIN_BOTTOM) < (pdf.bounds.absolute_bottom + 220)
    pdf.bounding_box([-pdf.bounds.left, pdf.cursor],
                     width: pdf.bounds.right + pdf.bounds.left) do
      content_start = pdf.cursor

      point_header_content(pdf, pt)

      pdf.fill_color MED_GRAY
      pdf.fill_rectangle([pdf.bounds.left, pdf.bounds.top],
                         pdf.bounds.right,
                         pdf.bounds.top - pdf.bounds.bottom)

      pdf.fill_color BLUE
      blue_width = 40
      blue_width = 55 if pt[:title_size] == :long

      pdf.fill_rectangle [pdf.bounds.left, pdf.bounds.top], blue_width, 25

      pdf.fill_color WHITE
      if @qa.dpr?
        pdf.text_box pt[:section_name], at: [pdf.bounds.left + 5, pdf.bounds.top - 7]
      else
        pdf.text_box pt[:title], at: [pdf.bounds.left + 5, pdf.bounds.top - 7]
      end

      if pt[:last_assessment]
        pdf.image "#{Rails.root}/public/clock-white.png", fit: [10, 10],
                                                          at: [pdf.bounds.left + blue_width - 12, pdf.bounds.top - 13]
      end

      case pt[:data][:result]
      when 'OK' then pdf.fill_color GREEN
      when 'NOK' then pdf.fill_color RED
      when 'NOK without risk' then pdf.fill_color RED
      when 'Not Assessed' then pdf.fill_color DARK_YELLOW
      when 'Not Applicable' then pdf.fill_color DARK_YELLOW
      else pdf.fill_color DARK_YELLOW
      end

      pdf.fill_rectangle [pdf.bounds.right - 40, pdf.bounds.top], 40, 25

      pdf.fill_color WHITE
      res = pt[:data][:result] || 'N/A'
      res = 'N/A' if ['Not Assessed', 'Not Applicable'].include?(res)
      res = 'NOK' if ['NOK without risk'].include?(res)
      res = pt[:level] if @qa.dpr?

      pdf.text_box res, at: [pdf.bounds.right - 33, pdf.bounds.top - 7]

      pdf.move_cursor_to pdf.bounds.top
      point_header_content(pdf, pt)
    end
  end
end
