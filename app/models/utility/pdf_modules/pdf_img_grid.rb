require 'mini_magick'
class PdfGenerator
  def grey_box_imgs(pdf, images)
    # img_height = images.count / 4
    # img_height += 1 if images.count % 4 > 0
    bottom = pdf.cursor
    current_page = pdf.page_count
    box_height = 120

    rows = images.each_slice(4).to_a

    rows.each do |row|
      if pdf.cursor < 120
        pdf.start_new_page
        puts '-----NEXT PAGE------'
      end

      puts "NEW ROW: #{pdf.cursor}"
      pdf.bounding_box([-pdf.bounds.left, pdf.cursor],
                       width: pdf.bounds.right + pdf.bounds.left,
                       height: box_height) do
        pdf.fill_color MED_GRAY
        pdf.fill_rectangle([pdf.bounds.left, pdf.bounds.top],
                           pdf.bounds.right,
                           pdf.bounds.top - pdf.bounds.bottom)
        pdf.move_cursor_to pdf.bounds.top
        pdf.move_down 5

        new_image_grid(pdf, row)

        pdf.move_cursor_to bottom if pdf.page_count == current_page && pdf.cursor > bottom
      end
    end
  end

  def new_image_grid(pdf, image_set)
    ref = pdf.cursor
    offset = 0
    pc = pdf.page_count
    pdf.undash

    image_set.each_with_index do |img, i|
      row = (i + offset) / 4
      col = (i + offset) % 4
      width = (pdf.bounds.absolute_right - pdf.bounds.absolute_left) / 4.0
      img_size = 110
      # ref = new_ref(pdf, col, row, img_size, ref)
      new_draw_image(pdf, col, width, ref, img_size, img)
    end
    # pdf.move_down 25
  end

  def new_draw_image(pdf, col, width, ref, img_size, img)
    # return true if @offline
    pdf.bounding_box([(col * width) + 7, ref], width: img_size, height: img_size) do
      if @offline
        pdf.fill_rectangle [pdf.bounds.left, pdf.bounds.top], img_size, img_size
      else # if ['png','jpeg','jpg'].include?(img[:img].downcase.split('.').last)
        draw_it!(pdf, img_size, img)
      end

      image_bounds(pdf, img[:type])
      pdf.move_cursor_to pdf.bounds.top if col < 3
    end
  end

  def draw_it!(pdf, img_size, img)
    image = MiniMagick::Image.open(img[:img])
    image.resize("#{img_size * 5}x#{img_size * 5}")
    image.quality '95'
    image.format 'png'
    image.write 'tmp/output.png'

    pdf.image 'tmp/output.png', fit: [img_size, img_size]
  rescue MiniMagick::Error
    image = MiniMagick::Image.open(img[:img])
    image.format 'png'
    image.write 'tmp/output.png'

    pdf.image 'tmp/output.png', fit: [img_size, img_size]
  rescue MiniMagick::Invalid
    pdf.fill_rectangle [pdf.bounds.left, pdf.bounds.top], img_size, img_size

    extension = img[:img].split('.').last.upcase
    pdf.indent(5) do
      pdf.text("<u> <link href='#{img[:img]}'>DOWNLOAD ATTACHED FILE</link></u>", color: BLUE, inline_format: true)
    end
  end

  def image_bounds(pdf, type)
    case type
    when :strong then pdf.stroke_color GREEN
    when :improve then pdf.stroke_color RED
    else pdf.stroke_color BLUE
    end

    pdf.line_width = 2
    pdf.stroke_bounds
  end

  # def image_grid(pdf, image_set, bottom)
  #   return true if @offline

  #   ref = pdf.cursor
  #   offset = 0
  #   no_box = false
  #   pc = pdf.page_count
  #   image_set.each_with_index do |img, i|
  #     no_box = true if pdf.page_count > pc
  #     row = (i+offset) / 3
  #     col = (i+offset) % 3
  #     width = (pdf.bounds.absolute_right - pdf.bounds.absolute_left) / 3.0
  #     img_size = 154
  #     ref = new_ref(pdf, col, row, img_size, ref)

  #     if !no_box && (col < 2)
  #       draw_image(pdf, col, width, ref, img_size, img)
  #       no_box = true if (ref < bottom)
  #     elsif !no_box
  #       offset += 1
  #       row = row +=1
  #       col = 0
  #       ref = new_ref(pdf, col, row, img_size, ref)
  #       draw_image(pdf, col, width, ref, img_size, img)
  #     else
  #       draw_image(pdf, col, width, ref, img_size, img)
  #     end
  #   end
  #   pdf.move_down 25
  # end

  def new_ref(_pdf, col, row, img_size, ref)
    p col, row, img_size, ref
    h = img_size + 10
    next_row = (col == 0 && row > 0)
    next_page = (ref - h) < h

    # if next_row && next_page
    #   pdf.start_new_page
    #   return pdf.bounds.absolute_top - MARGIN_BOTTOM
    if next_row
      ref - h
      # elsif col == 0 && (pdf.cursor + MARGIN_BOTTOM) < (pdf.bounds.absolute_bottom + img_size)
      # p "CURSOR: #{pdf.cursor}"
      # pdf.start_new_page
      # return pdf.bounds.absolute_top - MARGIN_BOTTOM
    else
      ref
    end
  end

  # def draw_image(pdf, col, width, ref, img_size, img)
  #   return true if @offline
  #   pdf.bounding_box([col * width, ref], width: img_size, height: img_size) do
  #     image = MiniMagick::Image.open(img)
  #     image.resize("#{img_size*5}x#{img_size*5}")
  #     image.quality "95"
  #     image.format "png"
  #     image.write "tmp/output.png"

  #     pdf.image "tmp/output.png", fit: [img_size, img_size]
  #     pdf.fill_color DARK_GRAY
  #     pdf.stroke_bounds
  #     pdf.move_cursor_to pdf.bounds.top if col < 2
  #   end
  # end
end
