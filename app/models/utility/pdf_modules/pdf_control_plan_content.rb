class PdfGenerator
  # ============================================================================
  # ============================================================================
  #                   RENDER CP DETAILS
  # ============================================================================
  # ============================================================================
  def render_cp_monitoring(pdf)
    @cp.plan_items.includes(process_step: :product_process).each do |pi|
      p pdf.cursor
      pdf.start_new_page if pdf.cursor < 50.0
      plan_item(pdf, pi)
    end
  end

  def plan_item(pdf, pi)
    process_step_info(pdf, pi)
    plan_item_info(pdf, pi)
  end

  def process_step_info(pdf, pi)
    process_step_header(pdf, pi)
    ps = pi.process_step

    process_step_content(pdf, ps)
  end

  def process_step_content(pdf, ps)
    pdf.pad(5) do
      pdf.indent(5) { pdf.text 'Process Step Standard' }
    end

    render_if_present(pdf, ps.step_number, 'Step # ')
    render_if_present(pdf, ps.control_item, 'What To Control ')
    render_if_present(pdf, ps.control_method, 'Control Method ')
    render_if_present(pdf, ps.requirement, 'Requirement ')

    if ps.requirement_images.present?
      image_set = ps.requirement_images.map { |x| { img: x.blob.service_url, type: :normal } }
      grey_box_imgs(pdf, image_set)
    end
    if ps.acceptable_image.attachment.present? || ps.unacceptable_image.attachment.present?
      image_set = []
      if ps.acceptable_image.attachment.present?
        image_set << { img: ps.acceptable_image.attachment.blob.service_url,
                       type: :strong }
      end
      if ps.unacceptable_image.attachment.present?
        image_set << { img: ps.unacceptable_image.attachment.blob.service_url,
                       type: :improve }
      end

      grey_box_text(pdf, 18) do
        pdf.pad(2) do
          pdf.formatted_text [{ color: DARK_GRAY, text: 'How To Judge?', font: 'Helvetica', styles: [:bold] }]
        end
      end
      grey_box_imgs(pdf, image_set)
    end

    render_if_present(pdf, ps.criticity, 'Criticity ')
    render_if_present(pdf, ps.control_type, 'Type of Control ')
    render_if_present(pdf, ps.sampling, 'Sampling ')
    render_if_present(pdf, ps.acceptance_rules, 'Acceptance Rules ')
    render_if_present(pdf, ps.reaction_mode, 'Reaction Mode ')
    if ps.supplier_control_freq.present?
      freq = "Every #{ps.supplier_control_freq.gsub('.', ' ')};"
      freq += " checked by: #{ps.supplier_controller}" if ps.supplier_controller.present?
      render_if_present(pdf, freq,  'Supplier Control Frequency ')
    end
    if ps.decathlon_control_freq.present?
      render_if_present(pdf, "Every #{ps.decathlon_control_freq.gsub('.', ' ')}", 'Decathlon Control Frequency ')
    end
  end

  def render_if_present(pdf, item, label)
    item = item.strftime('%Y-%m-%d') if item.is_a? ActiveSupport::TimeWithZone

    pdf.fill_color DARK_GRAY
    pdf.font('Microhei', size: 12)
    pdf.start_new_page if (pdf.cursor + MARGIN_BOTTOM) < (pdf.bounds.absolute_bottom + 30 + calc_lines(item, 90))

    if item.present?
      lines = (calc_lines(label + item, 90) * 12) + 6
      grey_box_text(pdf, lines) do
        pdf.pad(2) do
          pdf.formatted_text [
            { color: DARK_GRAY, text: label.strip, font: 'Helvetica', styles: [:bold] },
            { color: DARK_GRAY, text: '  ', font: 'Helvetica' },
            { color: DARK_GRAY, text: item.gsub("\n", ' / ') }
          ]
        end
      end
    else
      lines = (calc_lines(label + 'N/A', 90) * 12) + 6
      grey_box_text(pdf, lines) do
        pdf.pad(2) do
          pdf.formatted_text [
            { color: DARK_GRAY, text: label.strip, font: 'Helvetica', styles: [:bold] },
            { color: DARK_GRAY, text: '  ', font: 'Helvetica' },
            { color: MED_DARK_GRAY, text: 'N/A', font: 'Helvetica', styles: [:italic] }
          ]
        end
      end
    end
  end

  def imgs_if_present(pdf, imgs, label = nil)
    if imgs.present?
      if label.present?
        grey_box_text(pdf) do
          pdf.formatted_text [{ color: DARK_GRAY, text: label, styles: [:bold], font: 'Helvetica' }]
        end
      end

      images = imgs.map do |x|
        x = x.attachment unless x.instance_of?(ActiveStorage::Attachment)
        { img: x.blob.service_url, type: :normal }
      end
      grey_box_imgs(pdf, images)
    end
  end

  def plan_item_info(pdf, pi)
    pdf.pad(5) do
      pdf.indent(5) { pdf.text 'Control Plan Findings', color: DARK_GRAY }
    end

    # grey_box_text(pdf) do
    #   pdf.move_down RHYTHM/2
    # end

    render_if_present(pdf, pi.findings, "Findings of Supplier's records and methods? ")
    imgs_if_present(pdf, pi.findings_images)

    render_if_present(pdf, pi.findings_process, 'Findings of process and product? ')
    imgs_if_present(pdf, pi.findings_process_images)

    render_if_present(pdf, pi.traceability, 'Traceability ')
    render_if_present(pdf, pi.actions, 'Immediate Actions/Protections? ')
  end

  # Gray box for Chapter Section Point Descriptions
  # e.g. D-1, D-2, etc.
  def process_step_header_content(pdf, ps)
    cat = ps.product_process.category
    cat_length = ((cat.length * 8) + 5)

    pdf.bounding_box([pdf.bounds.left, pdf.bounds.top],
                     width: pdf.bounds.right + pdf.bounds.left - 40) do
      pdf.pad(7) do
        pdf.indent cat_length + 10 do
          pdf.fill_color DARK_GRAY
          pdf.font('Microhei', size: 12, style: :bold)
          pdf.text(ps.product_process.name, color: DARK_GRAY)
        end
      end
    end
  end

  def process_step_header(pdf, pi)
    pdf.move_down 20
    # pdf.start_new_page if (pdf.cursor + MARGIN_BOTTOM) < (pdf.bounds.absolute_bottom + 220)
    pdf.bounding_box([-pdf.bounds.left, pdf.cursor],
                     width: pdf.bounds.right + pdf.bounds.left) do
      process_step_header_content(pdf, pi.process_step)

      pdf.fill_color MED_GRAY
      pdf.fill_rectangle([pdf.bounds.left, pdf.bounds.top],
                         pdf.bounds.right,
                         pdf.bounds.top - pdf.bounds.bottom)
      cat = pi.process_step.product_process.category
      cat_length = ((cat.length * 8) + 5)

      pdf.fill_color BLUE
      pdf.fill_rectangle [pdf.bounds.left, pdf.bounds.top], cat_length, (pdf.bounds.top - pdf.bounds.bottom)

      pdf.fill_color WHITE
      pdf.text_box cat.upcase, at: [pdf.bounds.left + 7, pdf.bounds.top - 7]

      plan_item_result(pdf, pi)

      pdf.move_cursor_to pdf.bounds.top
      process_step_header_content(pdf, pi.process_step)
    end
  end

  def plan_item_result(pdf, pi)
    set_plan_item_result_color(pdf, pi)

    pdf.fill_rectangle [pdf.bounds.right - 40, pdf.bounds.top], 40, (pdf.bounds.top - pdf.bounds.bottom)
    pdf.fill_color WHITE
    res = pi.result || 'N/A'
    res = 'N/A' if ['Not Assessed', 'Not Applicable', 'false'].include?(res)
    pdf.text_box res, at: [pdf.bounds.right - 33, pdf.bounds.top - 7]
  end

  def set_plan_item_result_color(pdf, pi)
    case pi.result
    when 'OK' then pdf.fill_color GREEN
    when 'NOK' then pdf.fill_color RED
    when 'Not Assessed' then pdf.fill_color DARK_YELLOW
    when 'Not Applicable' then pdf.fill_color DARK_YELLOW
    else pdf.fill_color DARK_YELLOW
    end
  end
end
