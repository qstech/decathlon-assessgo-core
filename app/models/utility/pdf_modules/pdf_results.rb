class PdfGenerator
  # ============================================================================
  # ============================================================================
  #                        OVERALL RESULTS
  # ============================================================================
  # ============================================================================

  def radar_result_page(pdf, chapter = nil)
    top = pdf.cursor
    doc_width = pdf.bounds.right - pdf.bounds.left
    l_start = [pdf.bounds.left, top]
    l_width = doc_width * 6 / 10
    r_start = [pdf.bounds.left + l_width + INNER_MARGIN, top - INNER_MARGIN]
    r_width = doc_width - l_width - INNER_MARGIN
    height  = pdf.cursor - pdf.bounds.bottom

    pdf.bounding_box(r_start, width: r_width, height: height) do
      pdf.fill_color DARK_GRAY
      if chapter.nil?
        render_overall_radar(pdf)
      else
        render_chapter_radar(pdf, chapter)
      end
    end

    pdf.move_cursor_to top

    pdf.bounding_box(l_start, width: l_width, height: height) do
      pdf.fill_color MED_GRAY
      pdf.fill_rectangle [pdf.bounds.left, pdf.bounds.top], l_width, height
      txt_start = [pdf.bounds.left + INNER_MARGIN, top - INNER_MARGIN]
      pdf.bounding_box(txt_start, width: (l_width - (INNER_MARGIN * 2)),
                                  height: height - (INNER_MARGIN * 2)) do
        if chapter.nil? && (@qa.assess_type != 'CAP Review')
          render_overall_text(pdf)
        elsif chapter.nil?
          pdf.fill_color DARK_GRAY
          pdf.text "The radar chart to the right represents overall performance in CAP Review. \n\nScore is 0 if no resolutions have been proposed; 50 if resolutions have been proposed, but the CAP Point has not yet been resolved; and 100 if the CAP Resolution has been resolved."
        else
          render_chapter_text(pdf, chapter)
        end
      end
    end
  end

  def render_cp_results(pdf)
    pdf.move_down RHYTHM
    top = pdf.cursor
    doc_width = pdf.bounds.right - pdf.bounds.left
    width = doc_width * 3 / 10
    start1 = [pdf.bounds.left, top]
    start2 = [start1[0] + width + INNER_MARGIN, top]
    start3 = [start2[0] + width + INNER_MARGIN, top]
    height = width * 2
    [start1, start2, start3].each_with_index do |start, i|
      pdf.bounding_box(start, width: width, height: height) do
        pdf.fill_color DARK_GRAY
        render_cp_radar(pdf, i)
      end
    end
  end

  def render_overall_results(pdf)
    pdf.move_down RHYTHM
    radar_result_page(pdf)
    return true if @qa.assess_type == 'CAP Review'

    pdf.start_new_page
    render_instructions(pdf)

    if @qa.overall_comments || @qa.strong_points || @qa.points_to_improve
      pdf.start_new_page
      content(pdf, 'Overall Comments', @qa.assess_type) do
        render_comments_page(pdf)
      end
    end

    pdf.start_new_page
    content(pdf, 'Grid Summary', @qa.assess_type) do
      render_report_summary(pdf)
    end
  end

  def render_chapter_text(pdf, chapter)
    pdf.fill_color DARK_GRAY
    pdf.font 'Helvetica', size: 14
    # pdf.move_down RHYTHM
    pdf.formatted_text [{ text: chapter[:section_name], styles: [:bold] },
                        { text: '           Level:  ', styles: [:bold] },
                        { text: chapter[:level], styles: [:normal] },
                        { text: '      Cotation:  ', styles: [:bold] },
                        { text: chapter[:cotation].to_i.to_s, styles: [:normal] }]
    pdf.move_down RHYTHM
    pdf.text chapter[:title].titleize, style: :italic, align: :left
    pdf.move_down RHYTHM
    sections = chapter[:sections]
    sections = chapter[:sections].first[:subscores] if @qa.dpr?

    sections.each do |sec|
      if @qa.dpr?
        pt = sec[:points].first
        title = "#{pt[:section_name]} #{pt[:title].humanize.truncate(35)}"
      else
        title = "#{sec[:section_name]} #{sec[:title].humanize.truncate(35)}"
      end

      pdf.text title, align: :left
      pdf.move_down RHYTHM * 1.5

      bar_width = pdf.bounds.right - pdf.bounds.left
      pdf.stroke_color MED_DARK_GRAY
      pdf.stroke_rounded_rectangle [pdf.bounds.left, pdf.cursor], bar_width, 15, 2
      break_it = false
      %w[E D C B A].each_with_index do |ltr, i|
        spot = bar_width / 4
        spot = (pdf.bounds.left + (spot * i))
        pdf.stroke_vertical_line (pdf.cursor - 15), (pdf.cursor + 5), at: spot
        pdf.fill_color DARK_GRAY
        pdf.text_box ltr, at: [spot + 1, pdf.cursor + 12], width: 18, height: 18, style: :italic

        next if ltr == 'A'

        if @qa.dpr?
          level = sec
          levs = (sec[:to]..sec[:from]).to_a
        else
          level = sec[:subscores].find { |x| x[:level] == ltr }
          levs = sec[:subscores].map { |lev| lev[:level] }
        end
        if !levs.include?(ltr)
          pdf.fill_color DARK_GRAY
          status_bar(pdf, spot, 100, bar_width, i)
        elsif ['Not Applicable', 'Not Assessed', 'N/A'].include?(level[:score])
          pdf.fill_color DARK_YELLOW
          status_bar(pdf, spot, 100, bar_width, i)
        elsif break_it
          pdf.fill_color RED
          status_bar(pdf, spot, 100, bar_width, i)
        elsif level[:questions][:count] == 0
          pdf.fill_color DARK_GRAY
          status_bar(pdf, spot, 100, bar_width, i)
        else
          pdf.fill_color RED
          status_bar(pdf, spot, 100, bar_width, i)

          pdf.fill_color GREEN
          fill = (level[:questions][:ok_count] * 100 / level[:questions][:count])
          status_bar(pdf, spot, fill, bar_width, i)
          break_it = true if fill < 100
        end
      end

      pdf.move_down RHYTHM * 2
    end
    # render_analysis(pdf)
    # pdf.move_down RHYTHM

    pdf.move_down RHYTHM
  end

  def status_bar(pdf, spot, fill, bar_width, i)
    spot_bar = bar_width / 4 * fill / 100
    pdf.fill_rounded_rectangle [spot, pdf.cursor], spot_bar, 15, 2
    if i.zero?
      pdf.fill_rectangle [spot + 2, pdf.cursor], spot_bar - 2, 15
    elsif i == 3 && fill == 100
      pdf.fill_rectangle [spot, pdf.cursor], spot_bar - 2, 15
    else
      pdf.fill_rectangle [spot, pdf.cursor], spot_bar, 15
    end
  end

  def render_overall_text(pdf)
    pdf.fill_color DARK_GRAY
    pdf.font 'Helvetica'
    # pdf.text "Results", size: 14, style: :bold, align: :left
    pdf.move_down RHYTHM
    pdf.font 'Helvetica', size: 14
    pdf.formatted_text [{ text: 'Level:  ', styles: [:bold] },
                        { text: @scores[:level], styles: [:normal] },
                        { text: '      Cotation:  ', styles: [:bold] },
                        { text: @scores[:cotation].to_s, styles: [:normal] }]
    pdf.move_down RHYTHM
    render_analysis(pdf)
    # pdf.move_down RHYTHM

    pdf.move_down RHYTHM
  end

  def render_analysis(pdf)
    result = JSON.parse(File.read("#{Rails.root}/lib/radar_results.json"))[@scores[:level]]
                 .gsub('SUPPLIER_NAME', @supplier.name)
    conversion = { 'E' => 20, 'EP' => 20, 'D' => 40, 'C' => 60, 'B' => 80, 'A' => 100 }

    pdf.font 'Microhei', size: 12, style: :normal
    pdf.text result

    pdf.move_down RHYTHM
    pdf.font 'Helvetica', size: 12, style: :normal
    append = if @scores[:level] == 'A'
               JSON.parse(File.read("#{Rails.root}/lib/radar_results.json"))['cot_perfect']
             elsif @scores[:cotation] < (conversion[@scores[:level]] - 10)
               JSON.parse(File.read("#{Rails.root}/lib/radar_results.json"))['cot_under']
             elsif @scores[:cotation] < (conversion[@scores[:level]] + 10)
               JSON.parse(File.read("#{Rails.root}/lib/radar_results.json"))['cot_ok']
             else
               JSON.parse(File.read("#{Rails.root}/lib/radar_results.json"))['cot_over']
             end
    # pdf.text append

    lowest_level = @scores[:chapters].map { |x| x[:sections] }.flatten
                                     .select { |x| x[:level] == @scores[:level] }.map { |sec| sec[:section_name] }.join(', ')

    lowest_cot = @scores[:chapters].map { |x| x[:sections] }.flatten
                                   .select { |x| x[:cotation].nil? ? false : x[:cotation] < @scores[:cotation] }.map { |sec| sec[:section_name] }.join(', ')

    if @scores[:level] != 'A'
      pdf.move_down RHYTHM
      pdf.formatted_text [{ text: 'Section(s) primarily influencing ', styles: [:normal] },
                          { text: @scores[:level], styles: [:bold] },
                          { text: ' result:', styles: [:normal] }]
      pdf.move_down RHYTHM / 2
      pdf.indent(10) { pdf.text lowest_level, style: :bold }
    end

    if lowest_cot.present?
      pdf.move_down RHYTHM
      pdf.formatted_text [{ text: 'Section(s) primarily influencing cotation result:', styles: [:normal] }]
      pdf.move_down RHYTHM / 2
      pdf.indent(10) { pdf.text lowest_cot, style: :bold }
    end
  end

  def render_comments_page(pdf)
    render_overall_comments(pdf)
  end

  def render_overall_comments(pdf)
    pdf.font 'Microhei', size: 12, style: :normal

    if @qa.overall_comments.present?
      pdf.move_down RHYTHM
      pdf.formatted_text [{ text: "Assessor's Overall Comments:", styles: [:bold], font: 'Helvetica' },
                          { text: @qa.overall_comments, styles: [:bold] }]
    end

    if @qa.strong_points.present?
      pdf.move_down RHYTHM
      pdf.formatted_text [{ text: 'Strong Points:', styles: [:bold], font: 'Helvetica' },
                          { text: @qa.strong_points, styles: [:normal] }]
    end

    if @qa.points_to_improve.present?
      pdf.move_down RHYTHM
      pdf.formatted_text [{ text: 'Points to Improve:', styles: [:bold], font: 'Helvetica' },
                          { text: @qa.points_to_improve, styles: [:bold] }]
    end
  end

  def render_instructions(pdf)
    pdf.font 'Helvetica', size: 11, style: :normal

    pdf.text 'How To Read This Report', size: 12, style: :bold, align: :left
    File.readlines("#{Rails.root}/lib/radar_instructions.txt").each do |line|
      if line[0] == '#'
        pdf.text line[2..-1], size: 11, style: :bold
      elsif line[0] == '*'
        pdf.text line[2..-1], size: 11, style: :italic
      else
        pdf.text line, size: 11, style: :normal
      end
    end
  end
end
