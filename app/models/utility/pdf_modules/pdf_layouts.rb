class PdfGenerator
  # ============================================================================
  # ============================================================================
  #                         COVER PAGE
  # ============================================================================
  # ============================================================================
  def cover_page(pdf)
    cover_page_color(pdf)

    pdf.bounding_box([-pdf.bounds.absolute_left, pdf.bounds.absolute_top - ADJUSTMENT],
                     width: @doc_width,
                     height: @doc_height) do
      pdf.fill_color @color
      pdf.fill_rectangle([pdf.bounds.left, pdf.bounds.top],
                         pdf.bounds.right,
                         pdf.bounds.top - pdf.bounds.bottom)
      pdf.pad(@doc_height / 4) do
        indentation = @doc_width / 2 - 170
        pdf.indent(indentation) do
          pdf.image "#{Rails.root}/public/Decathlon_Logo.png", width: 340
          pdf.move_down 30
          pdf.image "#{Rails.root}/public/ag_logo.png", width: 340
        end

        pdf.move_down 50
        pdf.font('Helvetica', size: 32, style: :bold, align: :center)
        pdf.fill_color WHITE
        pdf.text(@text, align: :center)
      end
    end
  end

  def cover_page_color(_pdf)
    if @qa.present?
      case @qa.assess_type
      when 'Annual'
        @text = 'OFFICIAL ASSESSMENT REPORT'
        @color = LIGHT_BLUE
      when 'Gemba'
        @text = 'GEMBA REPORT'
        @color = LIGHT_GREEN
      when 'CAP Review'
        @text = 'CAP REVIEW'
        @color = BROWN
      else
        @text = 'ASSESSGO REPORT'
        @color = LIGHT_BLUE
      end
    elsif @cp.present?
      case @cp.plan_type
      when 'CAP Review'
        @text = 'CONTROL PLAN CAP REVIEW'
        @color = BROWN
      else
        @text = 'CONTROL PLAN REPORT'
        @color = PURPLE
      end
    end
  end

  # ============================================================================
  # ============================================================================
  #                   LAYOUTING (HEADERS/ETC)
  # ============================================================================
  # ============================================================================

  def header_for(pdf, _title)
    # BLUE HEADER
    if @qa.present?
      header_box(pdf, @qa.short_name.downcase, @qa.assessor_name, @qa.referent_name)
    elsif @cp.present?
      assessor = @cp.assessor
      header_box(pdf, 'cp', assessor.name)
    end

    # OFF-GRAY HEADER
    supplier = @supplier
    supplier_info(pdf, supplier)
  end

  def section(pdf, title)
    header_for(pdf, title)

    # FORM CONTENT
    assess_type = @qa.assess_type if @qa.present?
    assess_type = @cp.plan_type if @cp.present?

    content(pdf, title, assess_type) do
      route_content(pdf, title)
    end
  end

  def fetch_qr(h)
    @qr64 ||= WxAuthentication.mp_qrcode(h)[22..-1]
    f = Tempfile.new(['image', '.png'], encoding: 'ascii-8bit')
    f.write Base64.decode64(@qr64)
    f.flush
    qr = StringIO.new(File.read(f.path))
    f.close!
    f.unlink
    qr
  end

  def mp_qr(pdf)
    return true if @offline

    if @qa.present?
      h = { scene: "id=#{@qa.id}", page: 'members/assessment/report' }
    elsif @cp.present?
      h = { scene: "id=#{@cp.id}", page: 'members/control_plan/report' }
    end
    @qr = fetch_qr(h)
    pdf.image @qr, at: [504, pdf.bounds.top - 21], width: 76, height: 76
  end

  def header_box(pdf, assess_type, assessor_name, referent_name = nil)
    color = @color
    header_start = [-pdf.bounds.absolute_left, pdf.bounds.absolute_top - ADJUSTMENT]

    pdf.bounding_box(header_start, width: @doc_width, height: HEADER_HEIGHT) do
      pdf.fill_color color
      pdf.fill_rectangle([pdf.bounds.left, pdf.bounds.top],
                         pdf.bounds.right,
                         pdf.bounds.top - pdf.bounds.bottom)

      pdf.fill_color WHITE
      pdf.fill_rectangle([504, pdf.bounds.top - 21], 76, 76)
      # stamp the qr
      mp_qr(pdf)

      pdf.move_cursor_to(pdf.bounds.top - 105)
      pdf.font('Helvetica', size: 14, style: :normal)
      pdf.indent(504) { pdf.text('View Report', color: WHITE) }
      pdf.move_cursor_to pdf.bounds.top

      pdf.pad INNER_MARGIN do
        pdf.indent INNER_MARGIN do
          pdf.image "#{Rails.root}/public/Decathlon_Logo.png", height: 18
          pdf.move_down 4
          pdf.font('Helvetica', size: 24, style: :bold)
          pdf.text('Assess Go Report', color: WHITE)
          pdf.move_down 4
          pdf.bounding_box([0, pdf.cursor], width: 68, height: 44) do
            pdf.fill_color WHITE
            pdf.fill_rectangle([pdf.bounds.left, pdf.bounds.top],
                               pdf.bounds.right,
                               pdf.bounds.top - pdf.bounds.bottom)
            pdf.image "#{Rails.root}/public/#{assess_type.parameterize.underscore}_logo.jpg", fit: [68, 44]
          end
          pdf.move_down 15
          pdf.font('Helvetica', size: 14, style: :normal)

          if referent_name.present?
            pdf.formatted_text  [{ text: 'Candidate: ', styles: [:normal] },
                                 { text: "#{assessor_name}; ", styles: %i[bold italic] },
                                 { text: 'Referent: ', styles: [:normal] },
                                 { text: "#{referent_name}; ", styles: %i[bold italic] }]
          else
            pdf.formatted_text  [{ text: 'Assessed By:  ', styles: [:normal] },
                                 { text: "#{assessor_name} ", styles: %i[bold italic] }]
          end
        end
      end
    end
  end

  def supplier_info(pdf, supplier)
    pdf.bounding_box([-pdf.bounds.absolute_left, (pdf.bounds.absolute_top - ADJUSTMENT - HEADER_HEIGHT)],
                     width: @doc_width,
                     height: 80) do
      pdf.fill_color LIGHT_GRAY
      pdf.fill_rectangle([pdf.bounds.left, pdf.bounds.top],
                         pdf.bounds.right,
                         pdf.bounds.top - pdf.bounds.bottom)
      pdf.pad 10 do
        pdf.indent INNER_MARGIN do
          pdf.font('Microhei', size: 14, style: :normal, color: DARK_GRAY)
          formatted_row = [{ text: 'Supplier Name:  ', styles: [:normal], color: DARK_GRAY },
                           { text: supplier.name, styles: [:bold], color: DARK_GRAY }]
          if supplier.sites.present? && @qa.present?
            formatted_row << { text: ', Site: ', styles: [:normal], color: DARK_GRAY }
            formatted_row << { text: @qa.site || 'Not Set', styles: [:bold], color: DARK_GRAY }
          end

          pdf.formatted_text formatted_row
          pdf.move_down 6

          # pdf.font('Helvetica', size: 14, style: :normal, color: DARK_GRAY)
          pdf.formatted_text [{ text: 'Supplier Code:  ', styles: [:normal], color: DARK_GRAY },
                              { text: supplier.code, styles: [:bold], color: DARK_GRAY },
                              { text: '            ' },
                              { text: 'Supplier Status: ', styles: [:normal], color: DARK_GRAY },
                              { text: supplier.status || 'Not Set', styles: [:bold], color: DARK_GRAY }]
          pdf.move_down 6

          formatted_row = [{ text: 'Supplier Location:  ', styles: [:normal], color: DARK_GRAY },
                           { text: supplier.location, styles: [:bold], color: DARK_GRAY }]

          if @qa.present?
            target = @qa.assessment_day_form.supplier_target
            formatted_row << { text: '            ' }
            formatted_row << { text: 'Supplier Target:  ', styles: [:normal], color: DARK_GRAY }
            formatted_row << { text: target || supplier.target || 'Not Set', styles: [:bold], color: DARK_GRAY }
          end

          # pdf.font('Microhei', size: 14, style: :normal, color: DARK_GRAY)
          pdf.formatted_text formatted_row
        end
      end
    end
  end

  def footer(pdf)
    date = Time.now.strftime('%Y-%m-%d')

    pdf.repeat(->(pg) { pg != 1 }) do
      footer_base(pdf, date)
    end

    string = 'page <page> of <total>'
    options = { at: [pdf.bounds.right - 150, -28],
                width: 150,
                align: :right,
                page_filter: ->(pg) { pg > 1 },
                start_count_at: 1,
                color: DARK_GRAY }
    pdf.number_pages string, options
  end

  def footer_base(pdf, date)
    pdf.bounding_box([-pdf.bounds.absolute_left, 0],
                     width: @doc_width,
                     height: 50) do
      pdf.move_cursor_to pdf.bounds.top - 28

      pdf.font('Helvetica', size: 13, style: :normal)
      pdf.indent INNER_MARGIN do
        pdf.formatted_text [{ text: 'Report Created Date:  ', styles: [:normal], color: DARK_GRAY },
                            { text: date, styles: [:bold], color: DARK_GRAY }]
      end
      pdf.transparent(0.5) do
        pdf.image "#{Rails.root}/public/ag_spinner.png", fit: [36, 36],
                                                         at: [pdf.bounds.absolute_right - 51, pdf.bounds.top - 5]
      end
    end
  end

  def content(pdf, header, assessment_type)
    pdf.move_down 15
    pdf.font('Helvetica', size: 24, style: :bold)
    pdf.text(header, color: BLUE)

    pdf.move_up 24

    pdf.font('Helvetica', size: 14, style: :normal, align: :right)
    pdf.formatted_text [{ text: 'Assessment Type:  ', styles: [:normal], color: DARK_GRAY, align: :right },
                        { text: assessment_type, styles: [:bold], color: DARK_GRAY, align: :right }], align: :right
    pdf.move_down 10
    yield
  end

  def grey_box_text(pdf, lines = nil, &block)
    pdf.bounding_box([-pdf.bounds.left, pdf.cursor],
                     width: pdf.bounds.right + pdf.bounds.left) do
      if lines.nil?
        pdf.indent(10, &block)
        lines = pdf.bounds.top - pdf.bounds.bottom
      end

      pdf.fill_color MED_GRAY
      pdf.fill_rectangle([pdf.bounds.left, pdf.bounds.top],
                         pdf.bounds.right,
                         lines)

      pdf.indent(10, &block)
    end
  end

  def render_small_yellow_box(pdf, height, &block)
    box_width = (pdf.bounds.right - pdf.bounds.left).to_i
    try_again = false
    pdf.start_new_page if pdf.cursor < height + 20
    top = pdf.cursor
    pdf.bounding_box([pdf.bounds.left, pdf.cursor], width: box_width, height: height + 20) do
      pdf.fill_color YELLOW
      pdf.fill_rounded_rectangle [pdf.bounds.left, pdf.bounds.top], pdf.bounds.right, pdf.bounds.top, 6
      pdf.stroke_color DARK_GRAY
      pdf.dash(5, space: 2.5, phase: 0)
      pdf.stroke_rounded_rectangle [pdf.bounds.left, pdf.bounds.top], pdf.bounds.right, pdf.bounds.top, 6
      pdf.fill_color DARK_GRAY
      pdf.move_cursor_to pdf.bounds.top
      pdf.pad(10, &block)
    end
    # render_small_yellow_box(pdf){yield} if try_again
    pdf.cursor
  end
end
