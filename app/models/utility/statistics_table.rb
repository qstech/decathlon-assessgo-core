class StatisticsTable
  include StatisticsTable::Utility
  include StatisticsTable::Query

  def initialize(params = {})
    @suppliers_counted = []
    @start_date = params[:start_date] || '2000-01-01' # 1.years.ago.strftime('%Y-%m-%d')
    @end_date = params[:end_date] || (Time.now + 10.years).strftime('%Y-%m-%d')
    @created_at = (@start_date.to_date..@end_date.to_date)

    @common_options = { created_at: @created_at, destroyed_at: nil }
    extend_if_present(@common_options, :completed, params[:completed], boolean: true)

    # qa_options
    @qa_options = @common_options
    extend_if_present(@qa_options, :prep_completed, params[:prep_completed], boolean: true)
    extend_if_present(@qa_options, :grid, params[:grids], except: ['control_plan'])
    extend_if_present(@qa_options, :assess_type, params[:assess_types])

    # cp options
    @cp_options = @common_options

    # supplier_options
    @supplier_options = {}
    extend_if_present(@supplier_options, :country, params[:countries])
    extend_if_present(@supplier_options, :industrial_process, params[:processes])

    # @cap_finished = params[:cap_finished] || 'all'

    # @options = {start_date: @start_date, end_date: @end_date, grids: @grids,
    #   assess_types: @assess_types, prep_completed: @prep_completed,
    #   cap_finished: @cap_finished, completed: @completed, countries: @countries,
    #   processes: @processes}
  end

  def country_query(params = {})
    @active_query = :country_query
    count_data_by_suppliers(:country_name, :country, params)
  end

  def process_query(params = {})
    @active_query = :process_query
    count_data_by_suppliers(:process_name, :industrial_process, params)
  end

  def result_query(params = {})
    @active_query = :result_query
    result_qa_query(params)
    result_cp_query(params.merge({ merging: true }))
    self
  end

  def result_qa_query(_params = {})
    @active_query = :result_qa_query
    count_data_by_assessments(:result, "COALESCE(result, 'NO RESULT') as result", :result)
  end

  def result_cp_query(_params = {})
    @active_query = :result_cp_query
    count_data_by_control_plan_rate(:nonconformity_rate)
  end

  def usage_query(date: nil)
    @active_query = :usage_query
    count_usage_data(date: date)
  end

  def mega_query(params = {})
    @active_query = :mega_query
    count_mega_data(params)
  end

  private

  def count_mega_data(qa_params: {}, cp_params: {}, prefix: '', merging: false, common: {})
    qa_keys = %i[country industrial_process result supplier_count]
    cp_keys = %i[country industrial_process nonconformity_rate supplier_count]

    qa_data = qa_mega_query(qa_params.merge(common))
    cp_data = cp_counts(cp_params.merge(common))
    structured_h = structured_data_for(:industrial_process, :suppliers_assessed,
                                       :open_assessments, :completed_assessments, 'A', 'B', 'C', 'D', 'E', 'EP',
                                       'Invalid Assessments', :open_control_plans, :completed_control_plans, :suppliers_monitored,
                                       :avg_ncr, :avg_freq_respect_rate, :avg_cap_close_rate)

    h = {}
    @query_data = qa_data.group_by(&:country_name)
                         .map do |country, c_set|
      [country, c_set.group_by(&:industrial_process)
                     .map do |process, p_set|
                  [process, p_set.group_by(&:result)
                                 .map { |result, r_set| [result, r_set.select(&:completed).map(&:assessments_count).sum] }
                                 .to_h.merge({
                                               supplier_count: p_set.uniq(&:id).count,
                                               open_assessments: p_set.reject(&:completed).map(&:assessments_count).sum,
                                               completed_assessments: p_set.select(&:completed).map(&:assessments_count).sum
                                             })]
                end.to_h]
    end.to_h

    @query_data.each do |a, b|
      h[a] = structured_h.dup
      b.each do |process, ph|
        h[a][process][:industrial_process] = process
        h[a][process][:suppliers_assessed] = ph.delete(:supplier_count)
        h[a][process][:open_assessments] = ph.delete(:open_assessments)
        h[a][process][:completed_assessments] = ph.delete(:completed_assessments)
        res = %w[A B C D E EP].map { |i| [i, (ph[i] || 0)] }.to_h
        res['Invalid Assessments'] = ph[nil] || 0

        h[a][process].merge!(res)
      end
    end

    cp = cp_data.group_by { |cp| SupplierProfile.new(country: cp.country).country_name }
                .map do |country, c_set|
      [country, c_set.group_by(&:industrial_process)
                     .map do |process, p_set|
                  completed_cps = p_set.select(&:completed)
                  comp_cnt = completed_cps.length

                  [process, {
                    open_control_plans: p_set.reject(&:completed).length,
                    completed_control_plans: comp_cnt,
                    suppliers_monitored: p_set.uniq(&:supplier_profile_id).length,
                    avg_ncr: comp_cnt.zero? ? '' : (completed_cps.map(&:nonconformity_rate).sum / comp_cnt),
                    avg_freq_respect_rate: comp_cnt.zero? ? '' : (completed_cps.map(&:freq_respect_rate).sum / comp_cnt),
                    avg_cap_close_rate: comp_cnt.zero? ? '' : (completed_cps.map(&:cap_close_rate).sum / comp_cnt)
                  }]
                end]
    end.to_h

    cp.each do |a, b|
      h[a] = structured_h.dup if h[a].nil?
      b.each do |process, ph|
        h[a][process].merge!(ph)
      end
    end

    @query_data = h
    @result_data = h

    self
  end

  def count_data_by_suppliers(grouping, column, qa_params: {}, cp_params: {}, prefix: '', merging: false, common: {})
    count_data(grouping, :assessments_count, prefix: prefix, keys: [:supplier_count]) do |_k|
      qa_counts_by_supplier(column, column, qa_params.merge(common))
    end

    count_data(grouping, :control_plans_count, prefix: prefix, keys: [:supplier_count], merging: true) do |_k|
      cp_counts_by_supplier(column, column, cp_params.merge(common))
    end

    merge_data!

    self
  end

  def count_data_by_assessments(grouping, column, col, qa_params: {}, prefix: '', merging: false, common: {})
    count_data(grouping, :assessments_count, prefix: prefix, supplier: :supplier_profile_id) do |_k|
      qa_counts(column, col, qa_params.merge(common))
    end

    self
  end

  def count_data_by_control_plan_rate(rate, cp_params: {}, prefix: '', merging: false, common: {})
    count_data(rate, :control_plans_count, prefix: prefix, supplier: :supplier_profile_id) do |_k|
      cp_counts(:plan_type, :plan_type, cp_params.merge(common))
    end
    key = standardize_prefix(prefix) + 'control_plans_count'
    regroup_cp_data(rate: rate, key: key.to_sym, merging: merging)

    self
  end

  def regroup_cp_data(rate:, key:, merging: false)
    fixed = {}
    @query_data.values.each do |h|
      h[rate] = rate_group(h[rate])
      fixed[h[rate]] = { rate => h[rate] } if fixed[h[rate]].nil?
      fixed[h[rate]][key] = 0 if fixed[h[rate]][key].nil?
      fixed[h[rate]][key] += h[key]
    end
    @query_data = fixed
    @result_data = @query_data unless merging
  end

  def count_usage_data(date: nil)
    users = User.includes(:assessments, :cps, :assessor_profile,
                          :supplier_profile, :supplier).where(destroyed_at: nil)

    @query_data = Hash.new do |h, key|
      h[key] = { country_name: key, total_users: 0, total_dkn: 0,
                 total_suppliers: 0, active_dkn: 0, active_suppliers: 0 }
    end

    users.each do |user|
      prof = user.profile
      next if prof.nil?
      next if date.present? && (user.created_at > Time.parse(date))

      count_user(user, prof, date)
    end

    @result_data = @query_data.sort_by { |_k, v| -v[:total_users] }.to_h

    self
  end

  def count_user(user, prof, date)
    country = prof.country_name
    @query_data[country][:total_users] += 1

    if prof.is_a?(AssessorProfile)
      @query_data[country][:total_dkn] += 1
      @query_data[country][:active_dkn] += 1 if user.active?(date)
    elsif prof.is_a?(SupplierProfile)
      @query_data[country][:total_suppliers] += 1
      @query_data[country][:active_suppliers] += 1 if user.active?(date)
    end
  end
end

# cap finished in 2 weeks
# if options[:cap_finished] != 'all'
#   cap_filter = calc_cap_interval
#   query_string_qa += " AND qa.id IN (?)"
#   opts << cap_filter
# end

# completed in 2-weeks
# if options[:completed2] != 'all'
#   @completed2 = options[:completed2]
#   query_string_qa += " AND qa.id IN (?)"
#   # cap_opts = calc_cap_interval
#   opts << calc_completed_interval
# end
