# frozen_string_literal: true

class XFile
  XLSX = '.xlsx'
  EXCEL = '.xls,.xlsx'
  WORD = '.doc,.docx'
  POWERPOINT = '.ppt,.pptx'
  IMAGES = 'image/*'
  VIDEOS = 'video/*'
  PDF = '.pdf'

  COMMON = [IMAGES, VIDEOS, PDF, EXCEL, WORD, POWERPOINT].join(',')
end
