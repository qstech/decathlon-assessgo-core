require 'openssl'
require 'base64'

class Encryptor
  PUBLIC_KEY  = Rails.application.credentials.dig(:encryptor, :public_key)
  PRIVATE_KEY = Rails.application.credentials.dig(:encryptor, :private_key)
  WX_AES_KEY  = Rails.application.credentials.dig(:wechat, :oa_aeskey)

  def self.encrypt(msg)
    public_key = Encryption::PublicKey.new(PUBLIC_KEY)
    enc_msg = public_key.encrypt(msg)
    Base64.encode64(enc_msg).encode('utf-8')
  rescue Exception => e
    puts("Message for the encryption log file for message #{msg} = #{e.message}")
  end

  def self.decrypt(msg)
    private_key = Encryption::PrivateKey.new(PRIVATE_KEY)
    enc_msg = Base64.decode64(msg.encode('ascii-8bit'))
    private_key.decrypt(enc_msg)
  rescue Exception => e
    puts("Message for the decryption log file for message #{msg} = #{e.message}")
  end
end

# Xiaowei621
