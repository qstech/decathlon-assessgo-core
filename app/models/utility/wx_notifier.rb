require 'rest-client'

class WxNotifier < ApplicationRecord
  mattr_accessor :token
  mattr_accessor :token_time

  self.abstract_class = true

  COLORS     = { header: '#0198F1', body: '#4A4A4A', footer: '#568D77' }
  OA_ID      = Rails.application.credentials.dig(:wechat, :oa_id)
  OA_SECRET  = Rails.application.credentials.dig(:wechat, :oa_secret)
  OA_TOKEN   = Rails.application.credentials.dig(:wechat, :oa_token)
  OA_AESKEY  = Rails.application.credentials.dig(:wechat, :oa_aeskey)

  # =====================================================
  #             TEMPLATES FOR NOTIFICATIONS
  # =====================================================

  # TO DO:
  # - Add Comment received notificaiton
  # - ADD Like received notification
  # - ADD Favorite notification

  def self.templates
    {
      'assessment_result' => assessment_result,
      'invite_accepted,' => invite_accepted,
      'scheduled_event,' => scheduled_event,
      'invite_rejected,' => invite_rejected,
      'comment_posted' => comment_posted
    }
  end

  def self.assessment_not_scheduled(params = {})
    {
      'template_id' => 'TBC', # TBC
      'receiver' => (params[:openid] || 'OPENID'),
      'mp_redirect' => (params[:redirect] || 'REDIRECT_URL'),
      'header_color' => COLORS[:header],
      'body_color' => COLORS[:body],
      'footer_color' => COLORS[:footer],
      'data' => {
        # CUSTOM MESSAGES SENT TO USER
        # Available==> params[:title], params[:header], params[:creator], params[:comment_date]
        'first' => params[:title], #
        'keyword1' => 'TBC', # Report Topic
        'keyword2' => 'TBC', # Feedback content
        'keyword3' => 'TBC', # Feedback user
        'keyword4' => 'TBC', # Feedback timestamp
        'remark' => 'Click to View Comment Details'
      }
    }
  end

  def self.assessor_not_assigned(params = {})
    {
      'template_id' => 'TBC', # TBC
      'receiver' => (params[:openid] || 'OPENID'),
      'mp_redirect' => (params[:redirect] || 'REDIRECT_URL'),
      'header_color' => COLORS[:header],
      'body_color' => COLORS[:body],
      'footer_color' => COLORS[:footer],
      'data' => {
        # CUSTOM MESSAGES SENT TO USER
        # Available==> params[:title], params[:header], params[:creator], params[:comment_date]
        'first' => params[:title], #
        'keyword1' => 'TBC', # Report Topic
        'keyword2' => 'TBC', # Feedback content
        'keyword3' => 'TBC', # Feedback user
        'keyword4' => 'TBC', # Feedback timestamp
        'remark' => 'Click to View Comment Details'
      }
    }
  end

  def self.prep_questions_not_finished(params = {})
    {
      'template_id' => 'TBC', # TBC
      'receiver' => (params[:openid] || 'OPENID'),
      'mp_redirect' => (params[:redirect] || 'REDIRECT_URL'),
      'header_color' => COLORS[:header],
      'body_color' => COLORS[:body],
      'footer_color' => COLORS[:footer],
      'data' => {
        # CUSTOM MESSAGES SENT TO USER
        # Available==> params[:title], params[:header], params[:creator], params[:comment_date]
        'first' => params[:title], #
        'keyword1' => 'TBC', # Report Topic
        'keyword2' => 'TBC', # Feedback content
        'keyword3' => 'TBC', # Feedback user
        'keyword4' => 'TBC', # Feedback timestamp
        'remark' => 'Click to View Comment Details'
      }
    }
  end

  def self.assessment_is_approaching(params = {})
    {
      'template_id' => 'TBC', # TBC
      'receiver' => (params[:openid] || 'OPENID'),
      'mp_redirect' => (params[:redirect] || 'REDIRECT_URL'),
      'header_color' => COLORS[:header],
      'body_color' => COLORS[:body],
      'footer_color' => COLORS[:footer],
      'data' => {
        # CUSTOM MESSAGES SENT TO USER
        # Available==> params[:title], params[:header], params[:creator], params[:comment_date]
        'first' => params[:title], #
        'keyword1' => 'TBC', # Report Topic
        'keyword2' => 'TBC', # Feedback content
        'keyword3' => 'TBC', # Feedback user
        'keyword4' => 'TBC', # Feedback timestamp
        'remark' => 'Click to View Comment Details'
      }
    }
  end

  def self.comment_posted(params = {})
    #   报告标题：{{keyword1.DATA}}
    #   反馈内容：{{keyword2.DATA}}
    #   反馈人员：{{keyword3.DATA}}
    #   反馈时间：{{keyword4.DATA}}
    {
      'template_id' => '9wnHX0yts94ebXm1E0mYbKOEkLJTzOxyJ0O3nAJAk_g', # TBC
      'receiver' => (params[:openid] || 'OPENID'),
      'mp_redirect' => (params[:redirect] || 'REDIRECT_URL'),
      'header_color' => COLORS[:header],
      'body_color' => COLORS[:body],
      'footer_color' => COLORS[:footer],
      'data' => {
        # CUSTOM MESSAGES SENT TO USER
        # Available==> params[:title], params[:header], params[:creator], params[:comment_date]
        'first' => params[:title],
        'keyword1' => params[:assessment], # Report Topic
        'keyword2' => params[:content].truncate(50), # Feedback content
        'keyword3' => params[:creator], # Feedback user
        'keyword4' => params[:comment_date], # Feedback timestamp
        'remark' => 'Click to View Comment Details'
      }
    }
  end

  def self.assessment_result(params = {})
    {
      'template_id' => '3gAS97BFNx2GS3keJmrwZeploMWdqnJD2AbQZXEHzJA',
      'receiver' => (params[:openid] || 'OPENID'),
      'mp_redirect' => (params[:redirect] || 'REDIRECT_URL'),
      'header_color' => COLORS[:header],
      'body_color' => COLORS[:body],
      'footer_color' => COLORS[:footer],
      'data' => {

        # CUSTOM MESSAGES SENT TO USER
        'first' => "#{params[:title]} Result", # ok?
        'keyword1' => params[:assess_date], # ok?
        'keyword2' => params[:result],
        'remark' => 'Click to view details'
      }
    }
  end

  def self.invite_accepted(params = {})
    {
      'template_id' => 'Bb3LnUXCo-TksNXrStTZiDJv8sjUl0UI70USFHKlbHA',
      'receiver' => (params[:openid] || 'OPENID'),
      'mp_redirect' => (params[:redirect] || 'REDIRECT_URL'),
      'header_color' => COLORS[:header],
      'body_color' => COLORS[:body],
      'footer_color' => COLORS[:footer],
      'data' => {

        # CUSTOM MESSAGES SENT TO USER
        'first' => 'Asessment Invitation Accepted', # ok?
        'keyword1' => params[:assessor_name], # invitee's name
        'keyword2' => params[:accepted_at], # invite accepted time
        'remark' => 'Click to View Assessment Details'
      }
    }
  end

  def self.scheduled_event(params = {})
    {
      'template_id' => 'MV3BWfrGriVvAxOtjc1JUGwyEkO_Ao9Fdke926mZCJg',
      'receiver' => (params[:openid] || 'OPENID'),
      'mp_redirect' => (params[:redirect] || 'REDIRECT_URL'),
      'header_color' => COLORS[:header],
      'body_color' => COLORS[:body],
      'footer_color' => COLORS[:footer],
      'data' => {

        # CUSTOM MESSAGES SENT TO USER
        'first' => params[:header], # ok?
        'keyword1' => params[:title], # what the event is? Company name - type of assessment?
        'keyword2' => params[:creator], # contact person (invitation sender?)
        'keyword3' => params[:assess_date], # scheduled time
        'remark' => 'Click to view details'
      }
    }
  end

  def self.invite_rejected(params = {})
    {
      'template_id' => 'qZ_1qte3llouBwa2gm6WZ5LbSkIzCFl3bdYs2kNdewA',
      'receiver' => (params[:openid] || 'OPENID'),
      'mp_redirect' => (params[:redirect] || 'REDIRECT_URL'),
      'header_color' => COLORS[:header],
      'body_color' => COLORS[:body],
      'footer_color' => COLORS[:footer],
      'data' => {

        # CUSTOM MESSAGES SENT TO USER
        'first' => params[:header], # ok?
        'keyword1' => params[:assessor_name], # invitee's username,
        'keyword2' => params[:reason], # reason of rejection, we have no reason of rejection in db..
        'keyword3' => params[:rejected_at], # invite accepted time
        'remark' => 'Invite another assessor.'
      }
    }
  end

  def self.invite_sent(params = {})
    {
      'template_id' => 'MV3BWfrGriVvAxOtjc1JUGwyEkO_Ao9Fdke926mZCJg',
      'receiver' => (params[:openid] || 'OPENID'),
      'mp_redirect' => (params[:redirect] || 'REDIRECT_URL'),
      'header_color' => COLORS[:header],
      'body_color' => COLORS[:body],
      'footer_color' => COLORS[:footer],
      'data' => {

        # CUSTOM MESSAGES SENT TO USER
        'first' => 'Invitation Received!', # ok?
        'keyword1' => params[:title], # what the event is? Company name - type of assessment?
        'keyword2' => params[:creator], # contact person (invitation sender?)
        'keyword3' => params[:assess_date], # scheduled time
        'remark' => 'Click to view details'
      }
    }
  end

  def self.login_success(params = {})
    {
      'template_id' => 'OevZIDjMzl-rPWaIV_xKdvUorX7ZjGZcMlCRhJCkWBo',
      'receiver' => (params[:openid] || 'OPENID'),
      'mp_redirect' => nil,
      'header_color' => COLORS[:header],
      'body_color' => COLORS[:body],
      'footer_color' => COLORS[:footer],
      'data' => {

        # CUSTOM MESSAGES SENT TO USER
        'first' => 'Login Successful!',
        'keyword1' => params[:username],
        'keyword2' => params[:timestamp],
        'remark' => 'Thanks for using AssessGo!'
      }
    }
  end

  def self.logout_success(params = {})
    {
      'template_id' => 'qk3tEXQdjf-9ECoOJr1GK1CBuowHQvLIL_2O8F-V7ZA',
      'receiver' => (params[:openid] || 'OPENID'),
      'mp_redirect' => nil,
      'header_color' => COLORS[:header],
      'body_color' => COLORS[:body],
      'footer_color' => COLORS[:footer],
      'data' => {

        # CUSTOM MESSAGES SENT TO USER
        'first' => 'Logout Successful',
        'keyword1' => params[:username],
        'keyword2' => params[:timestamp],
        'remark' => 'Thanks for using AssessGo!'
      }
    }
  end

  # ======================================================
  #                 API CONNECTORS
  # ======================================================

  def self.token_expired?
    return true if token.nil?

    expiration = token_time + 100.minutes
    return false if expiration > Time.now

    true
  end

  # "26_BPp61C0QVl413M5jbWIPHELiwuS4IGGnUVCawsqhgjtd9iqylet3agjH3oGVTddAB627XmpXVV14D3d07M6BkLKJZxhUp3fKElLQckMQ7fimuidFsVJ0Amz4WhvsGYPCNiC9z767lPynwC54CWAgAIAUOS"
  def self.get_token
    if token_expired?
      p '=======token expired, get new one'
      return set_token
    end
    token
  end

  def self.set_token
    token_url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=#{OA_ID}&secret=#{OA_SECRET}"
    result = RestClient.get(token_url)
    # p "===============set token"
    r = JSON.parse(result)
    # p r
    if !r['access_token'].nil?
      # p "============get token========"
      self.token_time = Time.now
      self.token = r['access_token']
    else
      p 'get token failed'
      p r
    end
  end

  def self.fetch_users
    token = get_token
    url = "https://api.weixin.qq.com/cgi-bin/user/get?access_token=#{token}"
    result = RestClient.get(url)
    JSON.parse(result)
  end

  def self.fetch_unionid(openid)
    r = Rails.cache.fetch("wx/#{openid}") do
      token = get_token
      url = "https://api.weixin.qq.com/cgi-bin/user/info?access_token=#{token}&openid=#{openid}&lang=en"
      result = RestClient.get(url)
      JSON.parse(result)
    end

    p r
    r['unionid']
  end

  def self.notify!(params = {})
    get_token = RestClient.get('https://assessgo.decathlon.cn/api/v1/oa_token')
    token = JSON.parse(get_token)['access_token']
    puts 'TOKEN RETRIEVED!'
    puts "NOW NOTIFYING #{params['receiver']}"
    post_msg = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=#{token}"
    data = {}
    params['data'].each do |k, v|
      color = params['body_color']
      color = params['header_color'] if k == 'first'
      color = params['footer_color'] if k == 'remark'
      data[k] = { 'value' => v, 'color' => color }
    end
    msg = {
      'touser' => params['receiver'],
      'template_id' => params['template_id'],
      'miniprogram' => {
        'appid' => 'wx74bd28be58496d80'
      },
      'data' => data
    }
    msg['miniprogram']['pagepath'] = params['mp_redirect'] if params['mp_redirect'].present?

    p '------start send msg--------'
    result = RestClient.post(post_msg, msg.to_json)
    p JSON.parse(result)
    errcode  = JSON.parse(result)['errcode']

    if ['0', 0].include? errcode
      p 'sent out!'
      true
    elsif params['second_attempt'].blank?
      params['second_attempt'] = true
      set_token
      notify!(params)
    else
      false
    end
  end

  def self.template_samples
    {
      # Assessment Result
      '3gAS97BFNx2GS3keJmrwZeploMWdqnJD2AbQZXEHzJA' => "{{first.DATA}}
        时间：{{keyword1.DATA}}
        评估结果：{{keyword2.DATA}}
        {{remark.DATA}}",

      # Invitation Accepted
      'Bb3LnUXCo-TksNXrStTZiDJv8sjUl0UI70USFHKlbHA' => "{{first.DATA}}
        邀请人昵称：{{keyword1.DATA}}
        接受时间：{{keyword2.DATA}}
        {{remark.DATA}}",

      # Report Result Ready
      'CSQ4pvFpSus-GFhoNM_biJXGCmIuGclrQzf0K-Ze_TE' => "{{first.DATA}}
        机构名称：{{keyword1.DATA}}
        评估项目：{{keyword2.DATA}}
        评估时长：{{keyword3.DATA}}
        完成时间：{{keyword4.DATA}}
        {{remark.DATA}}",

      # New Scheduled Plan
      'MV3BWfrGriVvAxOtjc1JUGwyEkO_Ao9Fdke926mZCJg' => "{{first.DATA}}
        计划：{{keyword1.DATA}}
        关联联系人：{{keyword2.DATA}}
        计划时间：{{keyword3.DATA}}
        {{remark.DATA}}",

      # reject invitation
      'qZ_1qte3llouBwa2gm6WZ5LbSkIzCFl3bdYs2kNdewA' => "{{first.DATA}}
        昵称：{{keyword1.DATA}}
        拒绝理由：{{keyword2.DATA}}
        时间：{{keyword3.DATA}}
        {{remark.DATA}}",

      # Cancel Appointment (i.e. delete assessment/CP)
      'L4_BBbqdrm8O-DsKrDmT8CaR0zTgp3T8Z-JbYpGdESI' => "
        {{first.DATA}}
        预约项目：{{keyword1.DATA}}
        预约时间：{{keyword2.DATA}}
        取消原因：{{keyword3.DATA}}
        {{remark.DATA}}",

      # Login
      'OevZIDjMzl-rPWaIV_xKdvUorX7ZjGZcMlCRhJCkWBo' => "
      {{first.DATA}}
      登陆账号：{{keyword1.DATA}}
      登陆时间：{{keyword2.DATA}}
      {{remark.DATA}}",

      # Logout
      'qk3tEXQdjf-9ECoOJr1GK1CBuowHQvLIL_2O8F-V7ZA' => "
      {{first.DATA}}
      成员名称：{{keyword1.DATA}}
      退出时间：{{keyword2.DATA}}
      {{remark.DATA}}",

      # Report Feedback Notification
      '9wnHX0yts94ebXm1E0mYbKOEkLJTzOxyJ0O3nAJAk_g' => "
      {{first.DATA}}
      报告标题：{{keyword1.DATA}}
      反馈内容：{{keyword2.DATA}}
      反馈人员：{{keyword3.DATA}}
      反馈时间：{{keyword4.DATA}}
      {{remark.DATA}}"
    }
  end
end
