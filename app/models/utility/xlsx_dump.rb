require 'rubyXL'
require 'rubyXL/convenience_methods'

class XlsxDump
  def initialize(params = {})
    if params[:supplier].present?
      @supplier = params[:supplier]
      @type = :supplier
    else
      @type = params[:type]
      @params = params.except(:type)
    end

    @workbook = RubyXL::Workbook.new
  end

  def test!
    prepare
    @workbook.write('./test.xlsx')
  end

  def dump!
    prepare
    @workbook.stream.string
  end

  private

  def prepare
    case @type
    when :supplier
      XlsxDump::Supplier.new(@workbook, @supplier).append!
    when :global
      XlsxDump::Global.new(@workbook, @params).append!
    end
  end
end
