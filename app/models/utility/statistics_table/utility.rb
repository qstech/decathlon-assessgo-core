module StatisticsTable::Utility
  extend ActiveSupport::Concern

  def standardize_prefix(str)
    str += '_' if (str.length > 0) && (str.last != '_')
    str
  end

  def rate_group(x)
    if x <= 20
      '0-20'
    elsif x > 20 && x <= 70
      '21-70'
    else
      '71-100'
    end
  end

  def merge_data!
    raise StandardError, 'Cannot merge before running a query!' unless @active_query
    raise StandardError, 'Cannot merge without second query!' unless @query_data.is_a?(Hash)

    all_keys  = @result_data.first[1].keys
    all_keys += @query_data.first[1].keys if @query_data.present?
    all_keys.uniq!

    @result_data.each do |k, v|
      @result_data[k] = v.merge(@query_data.delete(k)) if @query_data[k]
    end

    # Iterate over remaining items not in result_data
    @query_data.each do |k, v|
      @result_data[k] = all_keys.map { |x| [x, 0] }.to_h
      @result_data[k] = @result_data[k].merge(v)
    end

    @result_data
  end

  def count_data(grouping, counter, prefix: '', supplier: :id, merging: false, keys: [])
    prefix = standardize_prefix(prefix)
    key = :"#{prefix}#{counter}"

    count_suppliers = keys.include?(:supplier_count)
    @query_data = structured_data_for(grouping, *keys)

    data = yield(key)
    append_response(data, grouping, key, counter)
    append_supplier_counts(data, grouping, supplier: supplier) if count_suppliers

    @result_data = @query_data unless merging

    self
  end

  def structured_data_for(identifier, *keys)
    Hash.new do |h, k|
      base = { "#{identifier}": k }

      if keys.include?(:supplier_count)
        count = @result_data[k][:supplier_count] if @result_data && @result_data.keys.include?(k)
        base[:supplier_count] = count || 0
      end

      keys.each { |y| base[y] = 0 }
      h[k] = base
    end
  end

  def extend_if_present(options, key, val, only: [], except: [], boolean: false)
    except << 'all'
    only = ['true', 'false', true, false] if boolean

    return nil if only.present? && !only.include?(val)
    return nil if except.present? && except.include?(val)
    return nil if val.in?([[], nil])

    if val.is_a?(Array)
      val = val.select { |x| only.include?(x) } if only.present?
      val = val.reject { |x| except.include?(x) } if except.present?
      options[key] = val if val.present?
    else
      options[key] = val
    end
  end

  def append_response(data, col, key, val)
    data.each do |obj|
      group = obj.public_send(col)
      @query_data[group][key] = 0 if @query_data[group][key].nil?
      @query_data[group][key] += obj.public_send(val)
    end
  end

  def append_supplier_counts(data, col, supplier: :id)
    data.each do |obj|
      count_supplier(obj, col, supplier)
    end
  end

  def count_supplier(obj, col, supplier)
    unless @suppliers_counted.include?(obj.public_send(supplier))
      @query_data[obj.public_send(col)][:supplier_count] += 1
      @suppliers_counted << obj.public_send(supplier)
    end
  end

  def data
    raise StandardError, 'Must run query first' unless @result_data

    @result_data
  end

  def keys
    raise StandardError, 'Must run query first' unless @result_data

    @result_data && @result_data.keys
  end

  def values
    raise StandardError, 'Must run query first' unless @result_data

    @result_data && @result_data.values
  end

  def columns
    values.first.keys
  end

  def merge(params = {})
    raise StandardError, 'Cannot merge before running a query!' unless @active_query

    params[:merging] = true
    public_send(@active_query, params)
    merge_data!

    self
  end
end
