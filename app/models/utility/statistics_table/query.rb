module StatisticsTable::Query
  extend ActiveSupport::Concern

  def cp_counts(params = {})
    supplier = 'supplier_profiles.id as supplier_profile_id'
    s_keys = ['supplier_profiles.country', 'supplier_profiles.industrial_process']

    select_keys = [:id, :completed, supplier] + s_keys

    ControlPlan.joins(product_family: :supplier_profile).includes(plan_items: [:cap_points,
                                                                               { process_step: :cp_pending_frequencies }])
               .select(select_keys).where(@cp_options.merge(params))
  end

  def qa_counts(col, grouping, params = {})
    QualityAssessment.joins(:supplier_profile)
                     .select(:id, :supplier_profile_id, col, 'COUNT(quality_assessments.*) as assessments_count')
                     .where(@qa_options.merge(params)).group(:id, :supplier_profile_id, grouping)
  end

  def qa_counts_by_supplier(col, grouping, params, *_keys)
    SupplierProfile.joins(:quality_assessments)
                   .select(:id, col, 'COUNT(quality_assessments.*) as assessments_count')
                   .where(quality_assessments: @qa_options.merge(params)).group(:id, grouping)
  end

  def cp_counts_by_supplier(col, _grouping, params)
    SupplierProfile.joins(product_families: :control_plans)
                   .select(:id, col, 'COUNT(control_plans.*) as control_plans_count')
                   .where(control_plans: @cp_options.merge(params)).group(:id, col)
  end

  def qa_mega_query(params)
    SupplierProfile.joins(:quality_assessments)
                   .select(:id, :country, :industrial_process,
                           'quality_assessments.result as result',
                           'quality_assessments.completed as completed',
                           'COUNT(DISTINCT quality_assessments.id) as assessments_count')
                   .where(quality_assessments: @qa_options.merge(params))
                   .group(:id, :country, :industrial_process, 'quality_assessments.result',
                          'quality_assessments.completed')
  end

  def cp_mega_query(params)
    SupplierProfile.joins(product_families: :control_plans)
                   .select(:id, :country, :industrial_process,
                           'COUNT(control_plans.*) as control_plans_count')
                   .where(control_plans: @cp_options.merge(params)).group(:id, :country, :industrial_process)
  end
end
