require 'date'
class PageComponent < ApplicationRecord
  self.abstract_class = true
  # THIS CLASS is made up of helper methods that can create

  EXTRA_CLASSES = {
    'Annual' => 'blue',
    'CAP Close' => 'blue',
    'Self Assessment Training' => 'red',
    'Prep Required' => 'red',
    'Awaiting Assessor' => 'yellow',
    'Assessment Ready!' => 'aqua',
    'Supplier Not Ready!' => 'red',
    'Assessment In Progress' => 'green',
    'Chapters Completed' => 'purple',
    'Complete' => 'blue',
    'Completed' => 'blue',
    'E' => 'black',
    'D' => 'red',
    'C' => 'yellow',
    'B' => 'green',
    'A' => 'blue',
    'OK' => 'green',
    'NOK' => 'red',
    'DER' => 'yellow',
    'quality_assessment' => 'blue',
    'opex_assessment' => 'green',
    'sse_assessment' => 'yellow',
    'hrp_assessment' => 'aqua',
    'scm_assessment' => 'red',
    'env_assessment' => 'aqua',
    'pur_assessment' => 'red',
    'dpr_assessment' => 'black',
    'control_plan' => 'purple'
  }

  AVATAR = {
    user: 'https://assessgo.blob.core.chinacloudapi.cn/data/avatar-default.jpg',
    supplier: 'https://fast.tuitu-tech.cn/assessgo-images/supplier.png'
  }

  def self.completed_qa_card(quality_assessment)
    colors = { 'opex_assessment' => 'green', 'sse_assessment' => 'yellow', 'quality_assessment' => 'blue',
               'hrp_assessment' => 'aqua', 'scm_assessment' => 'red', 'env_assessment' => 'aqua', 'pur_assessment' => 'red' }
    hashy = {
      id: quality_assessment.id,
      assess_date: (quality_assessment.assess_date.strftime('%Y-%m-%d') || 'N/A'),
      result: quality_assessment.completed? ? quality_assessment.result : nil,
      assess_type: quality_assessment.assess_type,
      grid: quality_assessment.grid,
      short_name: quality_assessment.short_name
    }
    hashy[:qaType] = {
      desc: quality_assessment.assess_type,
      borderColor: colors[quality_assessment.grid || 'quality_assessment'],
      pillBg: if ['Annual', 'CAP Close',
                  'Self Assessment Training'].include?(quality_assessment.assess_type)
      colors[quality_assessment.grid || 'quality_assessment']
      else
        'white'
      end,
      fontColor: EXTRA_CLASSES.keys.include?(quality_assessment.assess_type) ? 'white' : 'black'
    }

    hashy[:projectDate] = if quality_assessment.assess_date.present?
      {
        month: Date::ABBR_MONTHNAMES[quality_assessment.assess_date.month],
        day: quality_assessment.assess_date.day
      }
    else
      {
        month: 'N/A',
        day: 'N/A'
      }
    end

    hashy
  end

  def self.future_qa_card(quality_assessment)
    colors = { 'opex_assessment' => 'green', 'sse_assessment' => 'yellow', 'quality_assessment' => 'blue',
               'hrp_assessment' => 'aqua', 'scm_assessment' => 'red', 'env_assessment' => 'aqua', 'pur_assessment' => 'red' }
    puts "Retrieving card for QA: #{quality_assessment.id}"
    supp = quality_assessment.supplier_profile
    hashy = {}
    hashy[:qaType] = {
      desc: quality_assessment.assess_type,
      borderColor: colors[quality_assessment.grid || 'quality_assessment'],
      pillBg: if ['Annual', 'CAP Close',
                  'Self Assessment Training'].include?(quality_assessment.assess_type)
      colors[quality_assessment.grid || 'quality_assessment']
      else
        'white'
      end,
      fontColor: EXTRA_CLASSES.keys.include?(quality_assessment.assess_type) ? 'white' : 'black'
    }

    hashy[:projectDate] = if quality_assessment.assess_date.present?
      {
        month: Date::ABBR_MONTHNAMES[quality_assessment.assess_date.month],
        day: quality_assessment.assess_date.day
      }
    else
      {
        month: 'N/A',
        day: 'N/A'
      }
    end

    hashy[:supplier] = {
      code: supp.code,
      name: supp.name
    }

    # Adding new logic for last_qa
    last_qa = if ['CAP Close', 'CAP Review'].include?(quality_assessment.assess_type) && quality_assessment.cap.present?
      quality_assessment.cap.ref
    else
      supp.quality_assessments.where(completed: true, assess_type: quality_assessment.assess_type,
                                     grid: quality_assessment.grid).order(assess_date: :desc).first
    end

    hashy[:lastResult] = {
      date: last_qa.present? ? last_qa.assess_date.strftime('%Y-%m-%d') : "hasn't been assessed",
      color: last_qa.present? ? EXTRA_CLASSES[last_qa.result] : 'black',
      desc: last_qa.present? ? last_qa.result : 'N/A'
    }
    hashy[:status] = {
      color: EXTRA_CLASSES[quality_assessment.status],
      desc: quality_assessment.status
    }
    hashy[:id] = quality_assessment.id
    hashy[:grid] = quality_assessment.grid

    hashy
  end

  def self.qa_active_links(qa, user, review = false)
    active_links = []

    by = qa.assessor.nil? ? I18n.t('active_links.not_confirmed') : qa.assessor_name

    if qa.prep_questions.where(respondant: 'assessor').present?
      h = {
        web_url: '',
        grid: qa.grid,
        title: I18n.t('active_links.assessment_prep'),
        status: "#{qa.prep_status_all[:icon]} #{I18n.t("active_links.#{qa.prep_status_all[:tag]}")}",
        color: qa.prep_status_all[:color],
        bindtap: 'goPrepare',
        url: "/members/assessment/assessment-prep#{'-review' if review}?id=#{qa.id}",
        buttonText: "#{I18n.t('common.go')} ➜",
        id: qa.id,
        hint: qa.prep_completed ? "Prepped on #{qa.prep_finished_at} by #{by}" : '',
        review: qa.completed? ? true : false
      }
      # CAP REVIEW doesn't have prep questions
      active_links << h
    end

    # ==========SUPPLIER PREP============

    if qa.prep_questions.where(respondant: 'supplier').present?
      h = {
        web_url: '',
        grid: qa.grid,
        title: I18n.t('active_links.supplier_prep'),
        status: "#{qa.prep_status_supplier[:icon]} #{I18n.t("active_links.#{qa.prep_status_supplier[:tag]}")}",
        color: qa.prep_status_supplier[:color],
        bindtap: 'supplierPrepare',
        url: "/members/assessment/assessment-prep#{'-review' if review}?id=#{qa.id}&respondant=supplier",
        buttonText: "#{I18n.t('common.go')} ➜",
        id: qa.id,
        review: qa.completed? ? true : false
      }

      active_links << h
    end

    # ============================
    if qa.assessment_day_form.present?
      qa_day_form = qa.assessment_day_form

      h = {
        web_url: '',
        grid: qa.grid,
        title: I18n.t('active_links.go_for'),
        status: qa_day_form.go_for_assessment_status ? "✓ #{I18n.t('active_links.basic_complete')}" : "✖︎ #{I18n.t('active_links.basic_incomplete')}",
        color: qa_day_form.go_for_assessment_status ? 'green' : 'red',
        bindtap: 'goForAssess',
        url: "/members/assessment/go-for-assessment?id=#{qa.id}",
        id: qa.id,
        buttonText: "#{I18n.t('common.go')} ➜",
        review: qa.completed? ? true : false
      }

      if qa.assessor_confirmed
        ass = qa.assessor_assignments.where(confirmed: true)

        h[:date] = ass.first.updated_at.strftime('%Y-%m-%d') unless ass.first.nil?
      end
      active_links << h
    end

    if qa.assess_type.include?('CAP')
      h = {
        web_url: '',
        grid: qa.grid,
        title: I18n.t('active_links.related'),
        status: '',
        color: 'green',
        bindtap: 'capRelated',
        url: "/members/cap/related?id=#{qa.cap_id}",
        buttonText: "#{I18n.t('common.go')} ➜",
        review: true
      }
      active_links << h
    end

    if qa.chapter_assessments.present?
      label = qa.assess_type == 'CAP Close' ? 'CAP' : qa.short_name
      h = {
        web_url: '',
        grid: qa.grid,
        title: I18n.t('active_links.chapters', label: label),
        status: qa.chapter_assessments_finished? ? I18n.t('active_links.all_finished') : "✖︎ #{I18n.t('active_links.not_finished')}",
        color: qa.chapter_assessments_finished? ? 'green' : 'red',
        bindtap: 'chapterAssess',
        url: "/members/assessment/chapter-assessment?id=#{qa.id}",
        buttonText: "#{I18n.t('common.go')} ➜",
        review: qa.completed? ? true : false
      }
      active_links << h
    end

    if ['CAP Review'].include? qa.assess_type
      cap_points = qa.cap_points
      all_with_resolutions = cap_points.select { |x| x.cap_resolutions.present? }.length == cap_points.length
      h = {
        web_url: NativeUrl.generate('cap/index', user, model: 'quality_assessment', qa_id: qa.id),
        grid: qa.grid,
        title: I18n.t('active_links.cap'),
        status: all_with_resolutions ? I18n.t('active_links.all_finished') : "✖︎ #{I18n.t('active_links.not_finished')}",
        color: all_with_resolutions ? 'green' : 'red',
        bindtap: 'capIndex',
        url: "/members/cap/index?model=quality_assessment&id=#{qa.id}",
        buttonText: "#{I18n.t('common.go')} ➜",
        review: qa.completed? ? true : false
      }
      active_links << h

      if all_with_resolutions && (cap_points.length > 0) && cap_points.first.cap_followup.present?
        all_resolved = cap_points.select do |x|
          x.cap_followup.present? && x.cap_followup.resolved
        end.length == cap_points.length
        h = {
          web_url: NativeUrl.generate('cap/index', user, model: 'quality_assessment', qa_id: qa.id, followup: true),
          grid: qa.grid,
          title: I18n.t('active_links.cap_followup'),
          status: all_resolved ? I18n.t('active_links.all_finished') : "✖︎ #{I18n.t('active_links.not_finished')}",
          color: all_resolved ? 'green' : 'red',
          bindtap: 'capFollowup',
          url: "/members/cap/index?model=quality_assessment&id=#{qa.id}&followup=true",
          buttonText: "#{I18n.t('common.go')} ➜",
          review: qa.completed? ? true : false
        }
        active_links << h
      end
    end

    if qa.completed && (qa.assess_type != 'CAP Review')
      h = {
        web_url: '',
        grid: qa.grid,
        title: I18n.t('active_links.overall_comments'),
        status: I18n.t('active_links.all_finished'),
        color: 'green',
        bindtap: 'overallComments',
        url: "/members/assessment/overview?id=#{qa.id}&review=true",
        buttonText: "#{I18n.t('common.go')} ➜",
        review: qa.completed? ? true : false
      }
      active_links << h
    end

    active_links
  end

  def self.assert_standard_level(lev)
    return nil if lev.nil?
    return true if ('A'..'Z').include?(lev[:section_name])

    # if ('X'..'Z').include?(lev[:section_name])

    # else
      j = lev[:section_name].split('.').first.to_i - 1
      lev[:section_name] = ('A'..'E').to_a.reverse[j]
    # end
  end

  def self.qa_chapter_set(qa)
    ch_set = qa.chapter_assessments.includes(:chapter_questions)
    sec_tags = ch_set.map(&:section).uniq
    levels = ch_set.group_by(&:chapter_tag)
    guidelines = qa.guidelines

    next_one = { 'E' => 'D', 'D' => 'C', 'C' => 'B', 'B' => 'A' }
    # TODO: REPLACE ALGORITHM W/SCORE CALC
    if qa.dpr?
      highest_lev = 'E'
      guidelines.map! do |gl|
        gl[:children].map! do |sec|
          sec[:children].map! do |lev|
            next(nil) unless ch_set.map { |x| x.chapter_tag }.include?(lev[:guideline_tag])

            highest_lev = lev[:to] if lev[:to] < highest_lev
            ca = levels[lev[:guideline_tag]].first
            cq = ca.chapter_questions.first
            lev[:chapter_id] = ca.id
            lev[:result] = cq.result
            lev[:level] = lev[:to] if cq.result == 'OK'
            lev[:level] = lev[:from] if cq.result == 'NOK'
            lev
          end

          sec[:children].delete(nil)
          next(nil) if sec[:children].blank?

          sec[:children].sort_by { |lev| lev[:level] || 'Z' }.each do |lev|
            next if lev[:level].nil?

            sec[:level] = lev[:level] if sec[:level].blank? || (lev[:from] == lev[:level])
            puts 'RES:'
            p sec[:level]
          end

          # SKIP Chapter Level update if section level is higher score
          next(sec) if sec[:level].blank? || (gl[:level].present? && gl[:level] > sec[:level])

          puts "updating chapter from #{gl[:level]} to #{sec[:level]}"
          # Update the level only if there's a Level there.
          gl[:level] = sec[:level]
          # gl_score[:break] = true if sec[:cotation].nil? || sec[:cotation] == 0
          puts "Chapter level updated! Now: #{gl[:level]}"
          sec
        end
        gl[:children].delete(nil)
        next(nil) if gl[:children].blank?

        gl[:level] = '-' if gl[:level].blank?
        gl
      end
      guidelines.delete(nil)
    else
      guidelines.each do |gl|

        # chapters
        gl[:assess_date] = qa.chapter_dates[gl[:section_name]] || qa.assess_date.strftime('%Y-%m-%d')
        gl[:children] = gl[:children].select { |x| sec_tags.include?(x[:guideline_tag]) }
                                     .sort { |a, b| a[:section_split] <=> b[:section_split] }

        gl[:children].each do |sec|
          # sections
          sec[:cotation] = 0
          score = { break: false, level: nil }
          sec[:children].each_with_index do |lev, i|
            next_lev = sec[:children][i + 1]
            assert_standard_level(lev)
            assert_standard_level(next_lev)
            # levels
            ca = levels[lev[:guideline_tag]].first if levels[lev[:guideline_tag]]
            if ca.blank?
              lev[:empty] = true
              next
            end
            lev[:color] = EXTRA_CLASSES[lev[:section_name]]
            lev[:chapter_id] = ca.id
            lev[:result] = ca.score
            lev[:cotation] = ca.cotation

            if qa.cap?
              cap_refs = Cap.parent_tree_for(qa).map(&:ref_id)
              scores = ChapterAssessment.where(quality_assessment_id: cap_refs,
                                               chapter_tag: ca.chapter_tag)
              last_result = scores.select(&:countable?).last || scores.last

              lev[:lastResult] = last_result&.score
              lev[:borderColor] = last_result&.border_color
              lev[:disable_editing] = false
            end

            sec[:action_taken] = ca.action_taken? if lev[:section_name] == 'E' && qa.hrp?

            if ca.chapter_questions.blank?
              lev[:empty] = true
              next
            end
            sec[:cotation] += lev[:cotation] if lev[:cotation].present?
            next if score[:break]

            puts "lev cot: #{lev[:cotation]}"
            if lev[:result].in?(('A'..'E'))
              sec[:level] = lev[:result]
            elsif lev[:cotation].present? && lev[:result] == 'OK'
              sec[:level] = (next_lev.nil? ? 'A' : next_lev[:section_name])
            elsif lev[:cotation].present? && sec[:level].blank?
              sec[:level] = lev[:section_name]
            end
            score[:break] = true if (lev[:cotation] || 100) < 100
          end
          sec[:children].reject! { |lev| lev[:empty] }
          sec[:skip_levels] = qa.grid == "indus_assessment"
          # sec[:children] = [] if qa.grid == "indus_assessment"

          sec[:level] = 'A' if sec[:level].blank? && sec[:children].map do |x|
            x[:cotation]
          end.reject(&:nil?).present?
          sec[:level] = 'EP' if sec[:level] == 'E' && sec[:action_taken]
          # nil sec[:level] breaks this, adding a check and conversion
          # sec[:level] = '' if sec[:level].nil?

          # SKIP Chapter Level update if section level is higher score
          next if sec[:level].blank? || (gl[:level].present? && gl[:level] > sec[:level])

          # Update the level only if there's a Level there.
          gl[:level] = nil if sec[:cotation].nil? && gl[:level].blank?
          gl[:level] = sec[:level] if sec[:cotation].present? && sec[:level].present?
          # gl_score[:break] = true if sec[:cotation].nil? || sec[:cotation] == 0
          puts "Chapter level updated! Now: #{gl[:level]}"
        end
        gl[:level] = '-' if gl[:level].blank?
        gl = {} if gl[:children].blank?
      end
    end
    guidelines.reject { |x| x[:children].length == 0 }
  end

  def self.invitation_card(invitation)
    ref_model = invitation.ref
    id = ref_model.id

    if ref_model.instance_of?(ControlPlan)
      project_type = 'Control Plan'
    elsif ref_model.instance_of?(QualityAssessment)
      project_type = ref_model.short_name
    elsif ref_model.instance_of?(CapFollowup)
      project_type = 'CAP Followup'
      cap = ref_model.cap_resolution.cap_point.cap
      id = cap.ref_id
    end

    source = "/members/assessment/review-assessment?id=#{cap.ref_id}" if invitation.ref_model == 'cap_followup'
    source = "/members/assessment/pre-confirmation?id=#{ref_model.id}" if invitation.ref_model == 'quality_assessment'
    h = {
      id: invitation.id,
      ref_id: id,
      projectType: project_type,
      projectSource: source,
      bindtap: 'checkDetail',
      cap_followup: ref_model.instance_of?(CapFollowup),
      projectDate: ref_model.project_date,
      avatar: (invitation.sender.wx_avatar || AVATAR[:user]), # sender avatar
      name: invitation.sender.profile.name, # sender name
      inviteDate: invitation.invited_date,
      accept: 'acceptpath', # fixed string, js function name
      reject: 'rejectpath' # fixed string, js function name
    }
    h[:path] = "/members/cap/followup?id=#{ref_model.id}" if invitation.ref_model == 'cap_followup'
    h
  end

  def self.activity_card(activity)
    # goes_activity => ref_model
    colors = { 'opex_assessment' => 'green', 'sse_assessment' => 'yellow', 'quality_assessment' => 'blue',
               'hrp_assessment' => 'aqua', 'scm_assessment' => 'red', 'env_assessment' => 'aqua', 'pur_assessment' => 'red' }

    likes = Like.where(ref_model: activity.ref_model, ref_id: activity.ref_id).count
    comments = activity.comments.count
    user = activity.user.profile
    ref_model = activity.ref
    supplier = activity.supplier_profile

    # related_caps = ref_model.related_caps
    result = {
      id: activity.id,
      type: activity.ref_model,
      like_count: likes,
      comment_count: comments,
      goTo: "/members/assessment/pre-confirmation?id=#{ref_model.id}",
      borderColor: activity.ref_model == 'quality_assessment' ? colors[ref_model.grid] : 'purple',
      goesType: activity.grid_assess_type(ref_model.short_name)

    }

    result[:user] = if user.present?
      {
        avatar: activity.user.wx_avatar || AVATAR[:user], # avatar
        firstName: user.name || user.username,
        lastName: ''
      }
    else
      {
        avatar: AVATAR[:user], # avatar
        firstName: 'Not available',
        lastName: ''
      }
    end

    result[:updateDetail] = {
      year: activity.updated_at.strftime('%Y'),
      month: activity.updated_at.strftime('%m'),
      day: activity.updated_at.strftime('%d'),
      desc: activity.content
    }

    result[:projectDate] = if ref_model.assess_date.present?
      {
        month: ref_model.assess_date.strftime('%B'),
        day: ref_model.assess_date.strftime('%d')
      }
    else
      {
        month: 'N/A',
        day: 'N/A'
      }
    end

    if activity.ref_model == 'quality_assessment'
      result[:pillBg] =
        if ['Annual', 'CAP Close',
            'Self Assessment Training'].include?(activity.ref_type)
          colors[ref_model.grid]
        else
          'white'
        end

      if ref_model.assess_type.include?('CAP') && ref_model.cap_closed?
        result[:cap_status] = 'closed'
      elsif ref_model.caps.present?
        result[:cap_status] = 'CAP in progress'
        result[:cap_status] = 'CAP closed' if ref_model.cap_closed?
      end

      result[:extraDetails] = {
        type: activity.grid_assess_type(ref_model.short_name),
        supplier: supplier.present? ? supplier.name : '',
        result: ref_model.result || 'N/A',
        resultColor: EXTRA_CLASSES[ref_model.result] || 'black',
        status: ref_model.status,
        statusColor: EXTRA_CLASSES[ref_model.status]
      }

    elsif activity.ref_model == 'control_plan'
      result[:pillBg] = ['Annual', 'CAP Close'].include?(activity.ref_type) ? 'purple' : 'white'
      result[:extraDetails] = {
        type: activity.grid_assess_type(ref_model.short_name),
        supplier: supplier.present? ? supplier.name : '',
        result: ref_model.result || 'N/A',
        respect: ref_model.result == 'All OK' ? '✓' : 'x',
        nok: ref_model.plan_items.where(result: 'NOK').count
      }
      result[:goesType] = ref_model.plan_type
    end

    result
  end

  def self.comment_card(comment, member)
    profile = comment.profile
    reply_user = comment.reply_user
    page = comment.page_type
    {
      avatar: comment.user.wx_avatar || AVATAR[:user], # user avatar
      uid: comment.user_id,
      reply_to_self: comment.reply_to == member.id,
      reply_user: (reply_user.nil? ? nil : reply_user.profile),
      name: profile.name,
      date: comment.created_at.strftime('%Y-%m-%d'),
      content: comment.content,
      page: page,
      link: comment.link,
      read: comment.receiver_read,
      from_user: member == comment.user
    }
  end

  def self.activity_show_card(activity, current_user)
    user = activity.user.profile
    ref_model = activity.ref
    grid = ref_model.grid
    supplier = activity.supplier_profile

    if ref_model.instance_of?(ControlPlan)
      p ref_model
      edit_url = "/#{ref_model.mp_path(:assess)}"
      url = "/#{ref_model.mp_path(:review)}"
      m_name = 'Control Plan'
    else
      m_name = "#{ref_model.short_name} Assessment"
      edit_url = "/#{ref_model.mp_path(:assess)}"
      url = if current_user.quality_assessments.include?(ref_model) && ref_model.incomplete?
        "/#{ref_model.mp_path(:assess)}"
      else
        "/#{ref_model.mp_path(:review)}"
      end
    end

    puts 'here'
    result = {}

    result[:updateHeader] = {
      name: user.blank? ? 'Not Confirmed' : user.name,
      date: ref_model.updated_at.strftime('%Y-%m-%d'),
      desc: "has updated a #{m_name}"
    }
    month = ref_model.assess_date.present? ? ref_model.assess_date.strftime('%B') : ''
    day = ref_model.assess_date.present? ? ref_model.assess_date.strftime('%d') : ''
    fav = current_user.favorited?(ref_model)
    favobj = current_user.fav_for(ref_model)
    fav_count = Favorite.where(ref_model: activity.ref_model, ref_id: activity.ref_id).count

    like = current_user.liked?(ref_model)
    likeobj = current_user.like_for(ref_model)
    like_count = Like.where(ref_model: activity.ref_model, ref_id: activity.ref_id).count

    # result: ref_model.previous_score || "N/A",
    border_color = EXTRA_CLASSES[grid] || 'blue'
    p "GRID..... #{grid}"
    p "BORDER COLOR.... #{border_color}"

    res = ref_model.result || 'N/A'

    res = ref_model.cap_review_status if ref_model.assess_type == 'CAP Review' && ref_model.is_a?(QualityAssessment)

    result[:updateCard] = {
      borderColor: border_color,
      edit_url: edit_url,
      favorited: fav,
      favorite_id: favobj.present? ? favobj.id.to_s : '',
      favorite_count: fav_count,
      liked: like,
      like_count: like_count,
      like_id: likeobj.present? ? likeobj.id.to_s : '',
      ref_model: activity.ref_model,
      grid: ref_model.grid,
      ref_id: activity.ref_id,
      projectDate: {
        month: month,
        day: day
      },
      extraDetails: {
        type: activity.grid_assess_type(ref_model.short_name),
        supplier: supplier.name,
        supplierCode: supplier.code,
        result: res,
        resultColor: EXTRA_CLASSES[ref_model.result],
        status: ref_model.status,
        statusColor: EXTRA_CLASSES[ref_model.status]
      },
      button: {
        text: 'Go To Report ➜',
        url: url,
        goTo: 'goTo'
      }
    }

    result[:updateCard][:self] = current_user == activity.user
    result
  end

  def self.search_results_card(result, user)
    # ensure result is a profile model
    web = ''
    result = result.profile if result.instance_of?(User)

    if result.instance_of?(AssessorProfile)
      # Some assessor profiles have no user_id, can't find assignments without a user.
      if result.user_id.nil? || User.find_by(id: result.user_id).blank?
        assignments = 0
        completed = 0
        avatar = AVATAR[:user]
      else
        user = User.find(result.user_id)
        assignments = user.assessor_assignments.all
        completed = assignments.select { |x| x.parent_form.completed }.length
        assignments = assignments.length
        avatar = user.wx_avatar || AVATAR[:user]
      end
      {
        type: 'Name',
        name: result.name,
        web_url: web,
        id: result.id,
        url: "/members/show/show?id=#{result.id}",
        avatar: avatar,
        pillColor: 'blue',
        pillDesc: result.assessor_for.present? ? 'Validated Assessor' : 'User',
        borderColor: 'blue',
        content: {
          key1: 'Prepared',
          value1: assignments,
          key2: 'Assessed',
          value2: completed
        }
      }
    elsif result.instance_of?(SupplierProfile)
      if result.last_qa.present?
        last_qa_time = result.last_qa.created_at.strftime('%F')
        primary = result.last_qa.primary_assessor
        last_qa_assessor_name = primary.present? ? primary.profile.name : ''
      else
        last_qa_time = 'Never'
        last_qa_assessor_name = 'No one'
      end

      h = {
        type: 'Supplier Name',
        name: result.name,
        web_url: web,
        id: result.id,
        url: "/members/show/show?supplierid=#{result.id}", # result.user_id.present? ? "/members/show/show?id=#{result.user_id}" : "/members/show/show?supplierid=#{result.id}",
        avatar: AVATAR[:supplier],
        pillColor: 'green',
        pillDesc: 'Supplier',
        borderColor: 'blue',
        content: {
          key1: 'Last Assessed',
          value1: last_qa_time,
          key2: 'Assessed by',
          value2: last_qa_assessor_name
        }
      }
      if result.opm_set.present? && result.pl_set.present? && result.ptm_set.present?
        h[:edit_subtext] = 'Assess Go PL/OPM/PTM information current'
        h[:edit_subtext_color] = 'green'
        h[:edit_txt] = ''
        h[:edit_url] = ''
      else
        h[:edit_subtext] = 'Assess Go PL/OPM/PTM information missing, please help edit this supplier'
        h[:edit_subtext_color] = 'red'
        h[:edit_txt] = 'Edit'
        h[:edit_url] = "/members/supplier/edit-info?id=#{result.id}"
      end
      h
    elsif result.instance_of?(ProductFamily)
      web = NativeUrl.generate('control_plan/product', user, product_id: result.id)
      if !result.last_cp.nil?
        last_cp = result.last_cp
        last_cp_time = last_cp.created_at.strftime('%F')
        last_cp_assessor_name = last_cp.assessor_profile.name
      else
        last_cp_time = 'Never'
        last_cp_assessor_name = 'No one'
      end
      {
        type: 'Product Family',
        name: result.name,
        web_url: web,
        id: result.id,
        url: "/members/control_plan/product?id=#{result.id}", # result.user_id.present? ? "/members/show/show?id=#{result.user_id}" : "/members/show/show?supplierid=#{result.id}",
        avatar: AVATAR[:supplier],
        pillColor: 'green',
        pillDesc: 'ProductFamily',
        borderColor: 'purple',
        content: {
          key1: 'Last Assessed',
          value1: last_cp_time,
          key2: 'Assessed by',
          value2: last_cp_assessor_name
        }
      }
    end
  end

  def self.history_card(entry, uid)
    return {} if entry.blank?

    entry_ref = entry.ref
    grid = entry_ref.grid
    border_color = EXTRA_CLASSES[grid] || 'blue'
    res = entry_ref.completed ? entry_ref.result : entry_ref.status
    nam = entry.supplier_profile.name
    nam += " (#{entry_ref.product_family.name})" if entry.ref_model == 'control_plan'
    res = entry_ref.cap_review_status if entry_ref.assess_type == 'CAP Review' && entry_ref.is_a?(QualityAssessment)

    {
      web_url: '',
      id: entry.id,
      created: {
        date: entry.created_at.strftime('%F')
      },
      projectDate: {
        month: entry_ref.assess_date.strftime('%b'),
        day: entry_ref.assess_date.strftime('%d')
      },
      extraDetails: {
        creator: entry.user_id == uid ? 'You' : entry.user.profile.name,
        self: entry_ref.can_edit?(uid) || (entry.user_id == uid),
        duplicatable: (entry.ref_model == 'quality_assessment') && ['Annual', 'CAP Close',
                                                                    'CAP Review'].exclude?(entry_ref.assess_type) && (entry.user_id == uid),
        type: entry.grid_assess_type(entry_ref.short_name),
        ref_model: entry.ref_model,
        ref_id: entry_ref.id,
        supplier: nam,
        result: res,
        resultColor: entry_ref.completed ? entry_ref.level_color[entry_ref.result] : entry_ref.status_color[entry_ref.status],
        completedDate: entry_ref.updated_at.strftime('%F'),
        borderColor: border_color,
        grid: grid,
        show_sections: entry_ref.show_sections?,
        sections_concerned: entry_ref.sections
      }
    }
  end

  def self.product_process_to_list(process)
    process_steps = process.process_steps.to_a.sort_by { |x| (x.step_number || '0').to_i }
    {
      id: process.id,
      name: process.name,
      steps: process_steps.map { |y| process_item_card(y, process.name) },
      steps_count: process_steps.count
    }
  end

  def self.process_item_card(item, name)
    item.standard_hash.merge({
                               last_review: item.last_review[:reviewed_at],
                               last_assessor: item.last_review[:assessor],
                               last_review_result: item.last_review[:result],
                               category: item.product_process.category,
                               reviewed: item.reviewed?,
                               productProcess: name
    })
  end

  def self.pf_search_card(product_family, current_user)
    supplier = product_family.supplier_profile
    web_url = NativeUrl.generate('control_plan/new-product', current_user, product_id: product_family.id)
    {
      type: 'productFamily',
      supplier: {
        code: supplier.code,
        name: supplier.name
      },
      productFamily: {
        id: product_family.id,
        name: product_family.name,
        supplier_profile_id: product_family.supplier_profile_id,
        web_url: web_url
      },
      controlPlan: {
        next_review: product_family.next_review || 'n/a',
        steps_available: product_family.available_count,
        steps_reviewed: product_family.reviewed_count,
        steps_pending: product_family.pending_count
      }
    }
  end

  def self.supplier_search_card(supplier)
    {
      type: 'supplier',
      details: {
        id: supplier.id,
        code: supplier.code,
        name: supplier.name,
        address: supplier.address,
        contact_name: supplier.contact_name,
        contact_email: supplier.contact_email,
        next_review: '2020-01-01'
      }
    }
  end

  def self.index_cp_card(control_plan)
    product_family = control_plan.product_family
    supplier = product_family.supplier_profile
    schedule_date = control_plan.assess_date.nil? ? Time.now : control_plan.assess_date
    status = if product_family.current_level.nil?
      'N/A'
    else
      product_family.current_level > 4 ? 'Advanced' : 'Fundamental'
    end
    pill = { font: 'purple', bg: 'white', border: 'purple' }
    pill = { font: 'white', bg: 'purple', border: 'purple' } if control_plan.plan_type != 'New Monitor'
    colors = { 'Advanced' => 'purple', 'Fundamental' => 'blue', 'N/A' => 'red' }
    {
      id: control_plan.id,
      cpType: {
        desc: control_plan.plan_type,
        fontColor: pill[:font],
        pillBg: pill[:bg],
        borderColor: pill[:border]

      },
      finished: { text: control_plan.completed ? 'Finished' : 'Needs Review',
                  bgColor: control_plan.completed ? 'green' : 'red' },
      status: { text: status, bgColor: colors[status] },
      review_month: schedule_date.nil? ? 'NA' : schedule_date.strftime('%b'),
      review_day: schedule_date.nil? ? 'NA' : schedule_date.strftime('%d'),
      supplier: {
        id: supplier.id,
        code: supplier.code,
        name: supplier.name
      },
      productFamily: {
        id: product_family.id,
        name: product_family.name
      }
    }
  end

  def self.user_performance_header(current_user, assessor_profile)
    profile = assessor_profile
    member = profile.user
    assessments = member.present? ? member.quality_assessments.valid : []
    suppliers = member.present? ? member.suppliers : []
    comments = member.present? ? member.all_comments : []

    followed = current_user.following?(profile)
    c_is_m = current_user == member
    trans = 'api.v1.members.members_show.header'

    p '============user_performance_header======='
    {
      bgColor: 'blue',
      icon: '/images/tabbar/user_sel.png',
      headerText: c_is_m ? I18n.t("#{trans}.me_title") : I18n.t("#{trans}.ass_title"),
      number1: {
        total: assessments.count,
        label: I18n.t("#{trans}.assessments_performed"),
        url: "/members/show/detail?id=#{profile.id}&type=assessments",
        type: 'assessments'
      },
      number2: {
        total: suppliers.count,
        label: I18n.t("#{trans}.suppliers_assessed"),
        url: "/members/show/detail?id=#{profile.id}&type=suppliers",
        type: 'suppliers'
      },
      number3: {
        total: profile.followers.count,
        label: I18n.t("#{trans}.people_following"),
        url: "/members/show/detail?id=#{profile.id}&type=followers",
        type: 'followers'
      },
      number4: {
        total: comments.count,
        label: I18n.t("#{trans}.comments_given_taken"),
        url: "/members/show/detail?id=#{profile.id}&type=comments&profile_type=#{current_user.profile.class.name}",
        type: 'comments'
      },
      # Frequency Respect Rate, CAP Close Rate
      number5: {
        total: profile.product_families.where(destroyed_at: nil).count,
        label: I18n.t("#{trans}.products"),
        url: "/members/show/detail?id=#{profile.id}&type=product_families",
        type: 'product_families'
      },
      number6: {
        total: profile.nonconformity_rate,
        label: I18n.t("#{trans}.nonconformity_rate"),
        url: "/members/show/detail?id=#{profile.id}&type=control_plans",
        type: 'control_plans'
      },
      number7: {
        total: profile.frequency_respect_rate,
        label: I18n.t("#{trans}.freq_respect_rate"),
        url: "/members/show/detail?id=#{profile.id}&type=cp_freq",
        type: 'cp_freq'
      },
      number8: {
        total: profile.cap_close_rate,
        label: I18n.t("#{trans}.cap_close_rate"),
        url: "/members/show/detail?id=#{profile.id}&type=cap",
        type: 'cap'
      }
    }
  end

  def self.user_performance_detail(current_user, assessor_profile)
    profile = assessor_profile
    member  = profile.user
    assessments = member.present? ? member.quality_assessments.where(destroyed_at: nil) : []
    followed = current_user.following?(profile)
    follow_obj = Follower.find_by(ref_model: profile.model_name.singular, ref_id: profile.id, user_id: current_user.id)
    p '============user_performance_detail======='
    {
      avatar: member.present? ? member.avatar_url || AVATAR[:user] : AVATAR[:user],
      name: profile.name,
      # temp fix, change once city field is available
      city: profile.city.present? ? profile.city : 'Not Set',
      country: profile.country_name,
      country_index: profile.country_index,
      title: profile.title.length > 0 ? profile.title : 'Not Set',
      supplierType: '',
      assessorTypes: profile.assessor_for,
      candidateFor: profile.candidate_for,
      candidate: profile.candidate_for.present?,
      assessor: profile.assessor_for.present?,
      referent: profile.referent_for.present?,
      supplier: false,
      id: member.present? ? member.id : nil,
      profile_id: profile.id,
      followed: followed,
      follow_id: follow_obj.present? ? follow_obj.id : 0
    }
  end

  def self.supplier_performance_header(supplier_profile)
    p '========supplier performance header==========='
    member = supplier_profile.user_id.nil? ? nil : supplier_profile.user
    assessments = supplier_profile.quality_assessments.valid
    trans = 'api.v1.members.members_show.header'

    {
      bgColor: 'blue',
      icon: '/images/tabbar/user_sel.png',
      headerText: I18n.t("#{trans}.sup_title"),
      number1: {
        total: assessments.count,
        label: I18n.t("#{trans}.assessments"),
        url: "/members/show/detail?supplierid=#{supplier_profile.id}&type=assessments",
        type: 'assessments'
      },
      number2: {
        total: supplier_profile.assessors.count,
        label: I18n.t("#{trans}.assessors"),
        url: "/members/show/detail?supplierid=#{supplier_profile.id}&type=assessors",
        type: 'assessors'
      },
      number3: {
        total: supplier_profile.followers.count,
        label: I18n.t("#{trans}.people_following"),
        url: "/members/show/detail?supplierid=#{supplier_profile.id}&type=followers",
        type: 'followers'
      },
      number4: {
        total: member.nil? ? 0 : member.all_comments.count,
        label: I18n.t("#{trans}.comments_given_taken"),
        url: "/members/show/detail?supplierid=#{supplier_profile.id}&type=comments",
        type: 'comments'
      },
      # Frequency Respect Rate, CAP Close Rate
      number5: {
        total: supplier_profile.product_families.where(destroyed_at: nil).count,
        label: I18n.t("#{trans}.products"),
        url: "/members/show/detail?supplierid=#{supplier_profile.id}&type=product_families",
        type: 'product_families'
      },
      number6: {
        total: supplier_profile.nonconformity_rate,
        label: I18n.t("#{trans}.nonconformity_rate"),
        url: "/members/show/detail?supplierid=#{supplier_profile.id}&type=control_plans",
        type: 'control_plans'
      },
      number7: {
        total: supplier_profile.frequency_respect_rate,
        label: I18n.t("#{trans}.freq_respect_rate"),
        url: "/members/show/detail?supplierid=#{supplier_profile.id}&type=cp_freq",
        type: 'cp_freq'
      },
      number8: {
        total: supplier_profile.cap_close_rate,
        label: I18n.t("#{trans}.cap_close_rate"),
        url: "/members/show/detail?supplierid=#{supplier_profile.id}&type=cap",
        type: 'cap'
      }
    }
  end

  def self.supplier_performance_detail(current_user, supplier_profile)
    profile = supplier_profile
    member = supplier_profile.user
    assessments = profile.quality_assessments
    followed = current_user.following?(profile)
    follow_obj = Follower.find_by(ref_model: profile.model_name.singular, ref_id: profile.id, user_id: current_user.id)
    types = [profile.industrial_process]
    types << 'FG' if profile.finished_goods_supplier
    types << 'CPT' if profile.component_supplier
    {
      avatar: AVATAR[:supplier],
      name: profile.name,
      city: profile.city.present? ? profile.city : 'Shanghai',
      country: profile.country_name,
      country_index: profile.country_index,
      supplierType: types.join(', '),
      assessor: false,
      candidate: false,
      referent: false,
      supplier: true,
      supplierid: profile.id,
      profile_id: profile.id,
      followed: followed,
      follow_id: follow_obj.present? ? follow_obj.id : 0
    }
  end

  def self.cp_findings_card(plan_item)
    pf = plan_item.control_plan.product_family
    supplier = pf.supplier_profile
    process_step = plan_item.process_step
    h = {
      completed_at: plan_item.updated_at.strftime('%Y-%m-%d'),
      product_family: pf.name,
      supplier: supplier.name,
      step_number: process_step.step_number
    }
    h.merge!(plan_item.standard_hash)
    h[:findings] = "Record/Method: #{plan_item.findings}; \n\nProcess/Product: #{plan_item.findings_process}"
    h
  end
end
