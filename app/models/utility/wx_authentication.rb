require 'rest-client'

class WxAuthentication < ApplicationRecord
  mattr_accessor :token
  mattr_accessor :token_time

  self.abstract_class = true
  # THIS CLASS is made up of helper methods that can create

  APP_ID     = Rails.application.credentials.dig(:wechat, :mp_id)
  APP_SECRET = Rails.application.credentials.dig(:wechat, :mp_secret)

  def self.mp_qrcode(params = { auto_color: true, width: 430 })
    # MUST PROVIDE PAGE & SCENE
    # params[:page] = "members/show/show",
    # params[:scene] = "supplierid=159" # "id=5"
    5.times { puts '' }
    puts 'GETTING QR...'
    # qr_token = self.get_token
    get_token = RestClient.get('https://assessgo.decathlon.cn/api/v1/wx_token')
    qr_token = JSON.parse(get_token)['access_token']

    qr_url = "https://api.weixin.qq.com/wxa/getwxacodeunlimit?access_token=#{qr_token}"
    puts qr_url
    puts params
    res = RestClient.post(qr_url, params.to_json)

    prefix = 'data:image/png;base64,'
    qr_code_64 = Base64.strict_encode64(res.body)
    # p prefix + qr_code_64
    prefix + qr_code_64
  end

  def self.get_token
    Rails.cache.fetch('wx_token', expires_in: 100.minutes) do
      set_token
    end
  end

  def self.set_token
    app_id = ENV['WX_APP_ID']
    app_secret = ENV['WX_SECRET']
    token_url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=#{APP_ID}&secret=#{APP_SECRET}"
    result = RestClient.get(token_url)
    # p "===============set token"
    r = JSON.parse(result)
    # p r
    if !r['access_token'].nil?
      # p "============get token========"
      self.token_time = Time.now
      self.token = r['access_token']
    else
      p 'get token failed'
      p r
    end
  end

  def self.fetch_errors(start_time: 15.days.ago, end_time: Time.now, keyword: '', start: 1, limit: 10)
    url = "https://api.weixin.qq.com/wxaapi/log/jserr_search?access_token=#{get_token}"
    params = { errmsg_keyword: keyword, type: 1, client_version:  '',
               start_time: start_time.to_i, end_time:  end_time.to_i, start:  start,
               limit: limit, sceneDesc:  '全部' }

    res = RestClient.post(url, params.to_json)

    JSON.parse(res)
  end
end
