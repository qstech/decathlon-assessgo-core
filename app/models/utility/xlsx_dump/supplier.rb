require 'rubyXL'
require 'rubyXL/convenience_methods'

class XlsxDump::Supplier < XlsxDump::Base
  def initialize(workbook, supplier)
    @workbook = workbook
    @data_included = [:supplier_info]
    @supplier = supplier
    @assessments = @supplier.quality_assessments
                            .includes(:supplier_profile, :user, :assessor_assignments, :assessment_day_form)
                            .where(destroyed_at: nil)
                            .map(&:basic_info)
    @data_included << :assessments if @assessments.present?

    @control_plans = @supplier.control_plans
                              .include_supplier.includes(:assessor_profile, :user)
                              .where(destroyed_at: nil)
                              .map(&:basic_info)
    @data_included << :control_plans if @assessments.present?
  end

  def append!
    cover_sheet
    fill_basic_info
    fill_assessments_info
    fill_control_plans_info
  end

  private

  def fill_basic_info
    sheet_name = "#{@supplier.code} #{@supplier.name} (#{Date.today})"
    worksheet = @workbook.add_worksheet(sheet_name)

    r = 0
    @supplier.basic_info.each_with_index do |(k, v), i|
      c = (i % 2) * 2

      worksheet.add_cell(r, c, k.to_s.titleize)
      autofit(worksheet, r, c, k.to_s.titleize)
      worksheet.add_cell(r, c + 1, v)
      autofit(worksheet, r, c + 1, v)
      worksheet.sheet_data[r][c].change_font_bold(true)
      worksheet.sheet_data[r][c + 1].change_text_wrap(true)

      r += 1 if c == 2
    end
  end

  def fill_assessments_info
    return nil if @assessments.blank?

    worksheet = @workbook.add_worksheet('Assessments')
    fill_data(worksheet, @assessments)
  end

  def fill_control_plans_info
    return nil if @control_plans.blank?

    worksheet = @workbook.add_worksheet('Control Plans')
    fill_data(worksheet, @control_plans)
  end
end
