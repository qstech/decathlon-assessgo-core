require 'rubyXL'
require 'rubyXL/convenience_methods'

class XlsxDump::Base
  def initialize(workbook)
    @workbook = workbook
  end

  private

  def cover_sheet
    worksheet = @workbook.worksheets[0]
    worksheet.change_column_font_name(0, 'Courier')
    r0_txt = 'This is an automatically generated Excel sheet.'
    r1_txt = "Data Type:  #{self.class.name.split('::').last}"
    r2_txt = "Date Generated:  #{Date.today}"
    r3_txt = "Includes: #{@data_included.map(&:to_s).join(', ').titleize}"

    worksheet.add_cell(0, 0, r0_txt)
    worksheet.sheet_data[0][0].change_font_bold(true)

    worksheet.add_cell(2, 0, r1_txt)
    worksheet.add_cell(3, 0, r2_txt)
    worksheet.add_cell(4, 0, r3_txt)

    autofit(worksheet, 0, 0, r0_txt)
  end

  def fill_data(worksheet, dataset)
    # Header row
    worksheet.add_cell(0, 0, '#')
    dataset.first.keys.each_with_index do |val, i|
      worksheet.add_cell(0, i + 1, val.to_s.titleize)
      worksheet.sheet_data[0][i + 1].change_font_bold(true)
      autofit(worksheet, 0, i + 1, val)
    end

    # Data Rows
    dataset.each_with_index do |h, i|
      worksheet.add_cell(i + 1, 0, i + 1)
      h.values.each_with_index do |val, j|
        worksheet.add_cell(i + 1, j + 1, val)
        autofit(worksheet, i + 1, j + 1, val)
      end
    end
  end

  def autofit(worksheet, row, col, val)
    w = val.to_s.size * 1.1

    if w > 100
      worksheet.change_column_width(col, 100)
    elsif w > worksheet.get_column_width(col)
      worksheet.change_column_width(col, w)
    end

    worksheet.sheet_data[row][col].change_text_wrap(true)
  end
end
