require 'rubyXL'
require 'rubyXL/convenience_methods'

class XlsxDump::Global < XlsxDump::Base
  def initialize(workbook, params = {})
    @workbook = workbook
    @data_included = params.keys
    @usage    = params[:usage_stats].present?    || params[:all]
    @country  = params[:country_stats].present?  || params[:all]
    @process  = params[:process_stats].present?  || params[:all]
    @result   = params[:result_stats].present?   || params[:all]
    @assessor = params[:assessor_stats].present? || params[:all]
    @params   = params.except(:country_stats, :process_stats, :result_stats, :assessor_stats, :all)
  end

  def append!
    cover_sheet
    fill_mega_data
    # fill_usage_data     if @usage
    # fill_country_data   if @country
    # fill_process_data   if @process
    # fill_result_data    if @result
    # fill_assessor_data  if @assessor
  end

  private

  def fill_mega_data
    StatisticsTable.new(@params)
                   .mega_query.data.each do |country, data|
      worksheet = @workbook.add_worksheet(country)

      fill_data(worksheet, data.values)
    end
  end

  def fill_usage_data
    worksheet = @workbook.add_worksheet('Usage Data')

    data = StatisticsTable.new.usage_query.values

    fill_data(worksheet, data)
  end

  def fill_country_data
    worksheet = @workbook.add_worksheet('Country Data')

    data = StatisticsTable.new(@params)
                          .country_query(common: { completed: true }, prefix: 'completed')
                          .merge(common: { completed: false }, prefix: 'open').values

    fill_data(worksheet, data)
  end

  def fill_process_data
    worksheet = @workbook.add_worksheet('Process Data')

    data = StatisticsTable.new(@params)
                          .process_query(common: { completed: true }, prefix: 'completed')
                          .merge(common: { completed: false }, prefix: 'open').values

    fill_data(worksheet, data)
  end

  def fill_result_data
    worksheet = @workbook.add_worksheet('Assessment Result Data')

    qa_data = StatisticsTable.new(@params)
                             .result_qa_query(qa_params: { completed: true }, prefix: 'completed').values

    fill_data(worksheet, qa_data)

    worksheet = @workbook.add_worksheet('Control Plan Result Data')
    cp_data = StatisticsTable.new(@params)
                             .result_cp_query(cp_params: { completed: true }, prefix: 'completed').values

    fill_data(worksheet, cp_data)
  end

  def fill_assessor_data
    worksheet = @workbook.add_worksheet('Assessor Data')

    # fill_data(worksheet, @assessments)
  end
end
