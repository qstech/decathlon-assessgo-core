class XlsExport::Hfwb
  class << self
    def prepare!(xls)
      qa = xls.qa
      s  = qa.supplier_profile
      ad = qa.assessment_day_form
      puts 'BEGIN'
      xls.to_excel[0] = {
        'D2' => qa.short_name,
        'D4' => ad.supplier_target,
        'D6' => qa.assess_date.strftime('%Y-%m-%d'),
        'I3' => "#{s.code} #{s.name}",
        'I4' => "#{s.address} (#{qa.site || 'Main Site'})",
        'I5' => ad.pls.map(&:name).join(', '),
        'I6' => qa.assessors.map(&:name).join(', ')
      }
      map_questions(xls, qa)
      puts 'FIN:HFWB'
    end

    def map_questions(xls, qa)
      sections = qa.all_points.map(&:chapter_tag)
      cq_map = qa.all_points.map { |x| [x.chapter_tag, x] }.to_h
      gls = Guideline.where(grid_version_id: qa.grid_version_id, guideline_tag: sections)
                     .order(:guideline_tag).group_by { |x| x.parent.parent.parent.title }

      r = 9
      gls.each do |chapter, questions|
        xls.to_excel[0]["A#{r}"] = chapter
        r += 1
        questions.each do |gl|
          cq = cq_map[gl.guideline_tag]
          map_question(xls, gl, cq, chapter, r)
          r += 1
        end
      end
    end

    def map_question(xls, guideline, question, chapter, row)
      xls.to_excel[0]["A#{row}"] = guideline.parent.parent.section_name
      xls.to_excel[0]["B#{row}"] = chapter
      xls.to_excel[0]["C#{row}"] = guideline.parent.parent.title
      xls.to_excel[0]["D#{row}"] = guideline.content
      xls.to_excel[0]["E#{row}"] = guideline.parent.section_name
      xls.to_excel[0]["F#{row}"] = hfwb_result(question)
      xls.to_excel[0]["G#{row}"] = question.strong_points
      xls.to_excel[0]["H#{row}"] = question.points_to_improve
    end

    def hfwb_result(question)
      case question.result
      when 'Not Applicable'
        question.result.upcase
      when 'Not Assessed'
        'NOT AUDITED'
      else
        question.result
      end
    end
  end
end
