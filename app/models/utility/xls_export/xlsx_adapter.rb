# frozen_string_literal: true

require 'rubyXL'
require 'rubyXL/convenience_methods'

class XlsExport::XlsxAdapter
  include XlsExport::Utils

  def initialize(filename)
    @workbook = RubyXL::Parser.parse("#{filename}.xlsx")
  end

  def update_sheets(to_excel = {})
    to_excel.each do |i, set|
      sheet = @workbook.worksheets[i]
      update_sheet(sheet, set)
    end
  end

  def stream
    @workbook.stream
  end

  private

  def update_sheet(sheet, set)
    set.each do |cell_name, value|
      coor = cell_to_coordinates(cell_name)
      cell = sheet[coor[0]][coor[1]]
      update_cell(cell, value)
      next unless cell.formula && cell.formula.expression.present?
      next if cell.formula.expression.split('!').length == 1

      cel = ref_for_cell(cell)
      update_cell(cel, value)
    end
  end

  def ref_for_cell(cell)
    ref_sheet = cell.formula.expression.split('!').first[1..-2]
    ref_cell = cell.formula.expression.split('!').last
    sht = @workbook[ref_sheet]
    ref_coor = cell_to_coordinates(ref_cell)
    sht[ref_coor[0]][ref_coor[1]]
  end

  def update_cell(cell, value)
    cell.change_contents(value, nil)
  end
end
