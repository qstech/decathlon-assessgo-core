# frozen_string_literal: true

# HRP Excel Export
# supports V11 and V12 grids
class XlsExport::Hrp
  class << self
    def prepare!(xls)
      puts xls.qa.grid_version.version_number
      case xls.qa.grid_version.version_number
      when 1
        XlsExport::Hrp::V11.prepare!(xls)
      when 2
        XlsExport::Hrp::V12.prepare!(xls)
      end
    end
  end
end
