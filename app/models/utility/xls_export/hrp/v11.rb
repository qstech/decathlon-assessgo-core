# frozen_string_literal: true

class XlsExport::Hrp::V11
  class << self
    EXCEL_MAP = {
      'HRPCH1S1L1-1' => '20', 'HRPCH1S1L1-2' => '21', 'HRPCH1S1L2-1' => '22',
      'HRPCH1S1L2-2' => '23', 'HRPCH1S1L2-3' => '24', 'HRPCH1S1L3-1' => '25',
      'HRPCH1S1L3-2' => '26', 'HRPCH1S1L3-3' => '27', 'HRPCH1S1L4-1' => '28',
      'HRPCH1S1L4-2' => '29', 'HRPCH1S1L4-3' => '30', 'HRPCH1S2L1-1' => '35',
      'HRPCH1S2L1-2' => '36', 'HRPCH1S2L2-1' => '37', 'HRPCH1S2L2-2' => '38',
      'HRPCH1S2L2-3' => '39', 'HRPCH1S2L2-4' => '40', 'HRPCH1S2L2-5' => '41',
      'HRPCH1S2L2-6' => '42', 'HRPCH1S2L2-7' => '43', 'HRPCH1S2L3-1' => '44',
      'HRPCH1S2L4-1' => '45', 'HRPCH1S2L4-2' => '46', 'HRPCH1S2L4-3' => '47',
      'HRPCH1S3L2-1' => '55', 'HRPCH1S3L2-2' => '56', 'HRPCH1S3L2-3' => '57',
      'HRPCH1S3L3-1' => '58', 'HRPCH1S3L3-2' => '59', 'HRPCH1S3L3-3' => '60',
      'HRPCH1S3L3-4' => '61', 'HRPCH1S3L4-1' => '62', 'HRPCH2S1L2-1' => '67',
      'HRPCH2S1L2-2' => '68', 'HRPCH2S1L2-3' => '69', 'HRPCH2S1L2-4' => '70',
      'HRPCH2S1L2-5' => '71', 'HRPCH2S1L3-1' => '72', 'HRPCH2S1L3-2' => '73',
      'HRPCH2S1L3-3' => '74', 'HRPCH2S1L3-4' => '75', 'HRPCH2S1L3-5' => '76',
      'HRPCH2S1L3-6' => '77', 'HRPCH2S1L4-1' => '78', 'HRPCH2S2L1-1' => '81',
      'HRPCH2S2L1-2' => '82', 'HRPCH2S2L2-1' => '83', 'HRPCH2S2L2-2' => '84',
      'HRPCH2S2L2-3' => '85', 'HRPCH2S2L2-4' => '86', 'HRPCH2S2L2-5' => '87',
      'HRPCH2S2L2-6' => '88', 'HRPCH2S2L2-7' => '89', 'HRPCH2S2L3-1' => '90',
      'HRPCH2S2L3-2' => '91', 'HRPCH2S2L3-3' => '92', 'HRPCH2S2L3-4' => '93',
      'HRPCH2S2L3-5' => '94', 'HRPCH2S2L3-6' => '95', 'HRPCH2S2L3-7' => '96',
      'HRPCH2S2L3-8' => '97', 'HRPCH2S2L3-9' => '98', 'HRPCH2S2L3-10' => '99',
      'HRPCH2S2L3-11' => '100', 'HRPCH2S2L3-12' => '101', 'HRPCH2S2L4-1' => '102',
      'HRPCH2S2L4-2' => '103', 'HRPCH2S2L4-3' => '104', 'HRPCH2S2L4-4' => '105',
      'HRPCH2S3L1-1' => '110', 'HRPCH2S3L1-2' => '111', 'HRPCH2S3L2-1' => '112',
      'HRPCH2S3L2-2' => '113', 'HRPCH2S3L2-3' => '114', 'HRPCH2S3L2-4' => '115',
      'HRPCH2S3L2-5' => '116', 'HRPCH2S3L2-6' => '117', 'HRPCH2S3L2-7' => '118',
      'HRPCH2S3L2-8' => '119', 'HRPCH2S3L2-9' => '120', 'HRPCH2S3L3-1' => '121',
      'HRPCH2S3L3-2' => '122', 'HRPCH2S3L3-3' => '123', 'HRPCH2S3L3-4' => '124',
      'HRPCH2S3L3-5' => '125', 'HRPCH2S3L3-6' => '126', 'HRPCH2S3L3-7' => '127',
      'HRPCH2S3L3-8' => '128', 'HRPCH2S3L3-9' => '129', 'HRPCH2S3L3-10' => '130',
      'HRPCH2S3L4-1' => '131', 'HRPCH2S4L1-1' => '136', 'HRPCH2S4L1-2' => '137',
      'HRPCH2S4L1-3' => '138', 'HRPCH2S4L1-4' => '139', 'HRPCH2S4L1-5' => '140',
      'HRPCH2S4L1-6' => '141', 'HRPCH2S4L1-7' => '142', 'HRPCH2S4L1-8' => '143',
      'HRPCH2S4L1-9' => '144', 'HRPCH2S4L1-10' => '145', 'HRPCH2S4L1-11' => '146',
      'HRPCH2S4L1-12' => '147', 'HRPCH2S4L1-13' => '148', 'HRPCH2S4L1-14' => '149',
      'HRPCH2S4L1-15' => '150', 'HRPCH2S4L2-1' => '151', 'HRPCH2S4L2-2' => '152',
      'HRPCH2S4L2-3' => '153', 'HRPCH2S4L2-4' => '154', 'HRPCH2S4L2-5' => '155',
      'HRPCH2S4L2-6' => '156', 'HRPCH2S4L2-7' => '157', 'HRPCH2S4L2-8' => '158',
      'HRPCH2S4L2-9' => '159', 'HRPCH2S4L3-1' => '160', 'HRPCH2S4L3-2' => '161',
      'HRPCH2S4L3-3' => '162', 'HRPCH2S4L3-4' => '163', 'HRPCH2S4L3-5' => '164',
      'HRPCH2S4L3-6' => '165', 'HRPCH2S4L4-1' => '166', 'HRPCH2S4L4-2' => '167',
      'HRPCH2S4L4-3' => '168', 'HRPCH2S4L4-4' => '169', 'HRPCH2S5L2-1' => '178',
      'HRPCH2S5L2-2' => '179', 'HRPCH2S5L2-3' => '180', 'HRPCH2S5L2-4' => '181',
      'HRPCH2S5L2-5' => '182', 'HRPCH2S5L2-6' => '183', 'HRPCH2S5L2-7' => '184',
      'HRPCH2S5L2-8' => '185', 'HRPCH2S5L2-9' => '186', 'HRPCH2S5L2-10' => '187',
      'HRPCH2S5L3-1' => '188', 'HRPCH2S5L3-2' => '189', 'HRPCH2S5L3-3' => '190',
      'HRPCH2S5L3-4' => '191', 'HRPCH2S5L4-1' => '192', 'HRPCH2S5L4-2' => '193',
      'HRPCH2S5L4-3' => '194', 'HRPCH2S5L4-4' => '195', 'HRPCH2S5L4-5' => '196',
      'HRPCH2S5L4-6' => '197', 'HRPCH2S5L4-7' => '198', 'HRPCH2S5L4-8' => '199',
      'HRPCH3S1L2-1' => '208', 'HRPCH3S1L2-2' => '209', 'HRPCH3S1L2-3' => '210',
      'HRPCH3S1L2-4' => '211', 'HRPCH3S1L2-5' => '212', 'HRPCH3S1L3-1' => '213',
      'HRPCH3S1L3-2' => '214', 'HRPCH3S1L3-3' => '215', 'HRPCH3S1L4-1' => '216',
      'HRPCH3S1L4-2' => '217', 'HRPCH3S2L1-1' => '224', 'HRPCH3S2L2-1' => '225',
      'HRPCH3S2L2-2' => '226', 'HRPCH3S2L2-3' => '227', 'HRPCH3S2L2-4' => '228',
      'HRPCH3S2L2-5' => '229', 'HRPCH3S2L2-6' => '230', 'HRPCH3S2L2-7' => '231',
      'HRPCH3S2L3-1' => '232', 'HRPCH3S2L3-2' => '233', 'HRPCH3S2L3-3' => '234',
      'HRPCH3S2L3-4' => '235', 'HRPCH3S2L4-1' => '236', 'HRPCH3S2L4-2' => '237',
      'HRPCH3S3L1-1' => '242', 'HRPCH3S3L1-2' => '243', 'HRPCH3S3L2-1' => '244',
      'HRPCH3S3L2-2' => '245', 'HRPCH3S3L2-3' => '246', 'HRPCH3S3L2-4' => '247',
      'HRPCH3S3L2-5' => '248', 'HRPCH3S3L2-6' => '249', 'HRPCH3S3L2-7' => '250',
      'HRPCH3S3L3-1' => '251', 'HRPCH3S3L3-2' => '252', 'HRPCH3S3L3-3' => '253',
      'HRPCH3S3L3-4' => '254', 'HRPCH3S3L3-5' => '255', 'HRPCH3S3L3-6' => '256',
      'HRPCH3S3L3-7' => '257', 'HRPCH3S3L3-8' => '258', 'HRPCH3S3L4-1' => '259',
      'HRPCH3S3L4-2' => '260', 'HRPCH3S4L2-1' => '264', 'HRPCH3S4L2-2' => '265',
      'HRPCH3S4L2-3' => '266', 'HRPCH3S4L3-1' => '267', 'HRPCH3S4L3-2' => '268',
      'HRPCH3S4L3-3' => '269', 'HRPCH3S4L3-4' => '270', 'HRPCH3S4L3-5' => '271',
      'HRPCH3S4L4-1' => '272', 'HRPCH3S4L4-2' => '273'
    }

    def prepare!(xls)
      qa = xls.qa
      s  = qa.supplier_profile
      ad = qa.assessment_day_form
      ass_type = 'Full' if qa.assess_type == 'Annual'
      ass_type = 'CAP Follow-up' if qa.assess_type == 'CAP Close'

      xls.to_excel[0] = {
        'E3' => "#{s.code} #{s.name}",
        'E4' => "#{s.address} (#{qa.site || 'Main Site'})",
        'E5' => ad.steps_assessed,
        'E7' => s.supplier_type,
        'E8' => s.supplier_status,
        # "E9"=> ad.employee_count,
        'I3' => qa.last_assess_date,
        'I4' => qa.assess_date.strftime('%Y-%m-%d'),
        'I5' => ass_type,
        # "I6"=> ad.man_days,
        'I7' => qa.assessors.map(&:name).join(', '),
        'I8' => ad.pls.map(&:name).join(', '),
        'I9' => ad.pl_present ? 'Yes' : 'No'
      }

      qa.all_points.each do |cq|
        r = EXCEL_MAP[cq.chapter_tag]
        xls.to_excel[0]["F#{r}"] = cq.result || ''
        xls.to_excel[0]["G#{r}"] = cq.result || ''
        (xls.to_excel[0]["H#{r}"] = cq.action_taken ? 'Yes' : 'No') if cq.result == 'NOK' && cq.level == 'E'
        xls.to_excel[0]["I#{r}"] = "Strong Points: #{cq.strong_points} \n\nPoints To Improve: #{cq.points_to_improve}"
      end
    end
  end
end
