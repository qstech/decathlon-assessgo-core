class XlsExport::Hrp::V12
  class << self
    FILE = "#{Rails.root}/lib/grids/HRP-v12"
    EXCEL_MAP = {
      'HRPCH1S1L1-1' => 20, 'HRPCH1S1L1-2' => 21, 'HRPCH1S1L2-1' => 22, 'HRPCH1S1L2-2' => 23,
      'HRPCH1S1L2-3' => 24, 'HRPCH1S1L3-1' => 25, 'HRPCH1S1L4-1' => 26, 'HRPCH1S1L4-2' => 27,
      'HRPCH1S1L4-3' => 28, 'HRPCH1S2L1-1' => 36, 'HRPCH1S2L1-2' => 37, 'HRPCH1S2L2-1' => 38,
      'HRPCH1S2L2-2' => 39, 'HRPCH1S2L2-3' => 40, 'HRPCH1S2L2-4' => 41, 'HRPCH1S2L2-5' => 42,
      'HRPCH1S2L2-6' => 43, 'HRPCH1S2L2-7' => 44, 'HRPCH1S2L2-8' => 45, 'HRPCH1S2L3-1' => 46,
      'HRPCH1S2L3-2' => 47, 'HRPCH1S2L3-3' => 48, 'HRPCH1S2L3-4' => 49, 'HRPCH1S2L3-5' => 50,
      'HRPCH1S2L3-6' => 51, 'HRPCH1S2L3-7' => 52, 'HRPCH1S2L3-8' => 53, 'HRPCH1S2L3-9' => 54,
      'HRPCH1S2L3-10' => 55, 'HRPCH1S2L3-11' => 56, 'HRPCH1S2L4-1' => 57, 'HRPCH1S2L4-2' => 58,
      'HRPCH1S2L4-3' => 59, 'HRPCH1S3L2-1' => 69, 'HRPCH1S3L2-2' => 70, 'HRPCH1S3L2-3' => 71,
      'HRPCH1S3L2-4' => 72, 'HRPCH1S3L2-5' => 73, 'HRPCH1S3L3-1' => 74, 'HRPCH1S3L3-2' => 75,
      'HRPCH1S3L3-3' => 76, 'HRPCH1S3L3-4' => 77, 'HRPCH1S3L4-1' => 78, 'HRPCH2S1L2-1' => 85,
      'HRPCH2S1L2-2' => 86, 'HRPCH2S1L2-3' => 87, 'HRPCH2S1L2-4' => 88, 'HRPCH2S1L2-5' => 89,
      'HRPCH2S1L3-1' => 90, 'HRPCH2S1L3-2' => 91, 'HRPCH2S1L3-3' => 92, 'HRPCH2S1L3-4' => 93,
      'HRPCH2S1L3-5' => 94, 'HRPCH2S1L3-6' => 95, 'HRPCH2S1L4-1' => 96, 'HRPCH2S2L1-1' => 100,
      'HRPCH2S2L1-2' => 101, 'HRPCH2S2L2-1' => 102, 'HRPCH2S2L2-2' => 103, 'HRPCH2S2L2-3' => 104,
      'HRPCH2S2L2-4' => 105, 'HRPCH2S2L2-5' => 106, 'HRPCH2S2L2-6' => 107, 'HRPCH2S2L2-7' => 108,
      'HRPCH2S2L2-8' => 109, 'HRPCH2S2L2-9' => 110, 'HRPCH2S2L2-10' => 111, 'HRPCH2S2L2-11' => 112,
      'HRPCH2S2L3-1' => 113, 'HRPCH2S2L3-2' => 114, 'HRPCH2S2L3-3' => 115, 'HRPCH2S2L3-4' => 116,
      'HRPCH2S2L3-5' => 117, 'HRPCH2S2L3-6' => 118, 'HRPCH2S2L3-7' => 119, 'HRPCH2S2L3-8' => 120,
      'HRPCH2S2L3-9' => 121, 'HRPCH2S2L3-10' => 122, 'HRPCH2S2L3-11' => 123, 'HRPCH2S2L3-12' => 124,
      'HRPCH2S2L3-13' => 125, 'HRPCH2S2L3-14' => 126, 'HRPCH2S2L3-15' => 127, 'HRPCH2S2L3-16' => 128,
      'HRPCH2S2L4-1' => 129, 'HRPCH2S2L4-2' => 130, 'HRPCH2S2L4-3' => 131, 'HRPCH2S3L1-1' => 137,
      'HRPCH2S3L1-2' => 139, 'HRPCH2S3L1-3' => 140, 'HRPCH2S3L1-4' => 141, 'HRPCH2S3L2-1' => 142,
      'HRPCH2S3L2-2' => 143, 'HRPCH2S3L2-3' => 144, 'HRPCH2S3L2-4' => 145, 'HRPCH2S3L2-5' => 147,
      'HRPCH2S3L2-6' => 148, 'HRPCH2S3L2-7' => 149, 'HRPCH2S3L2-8' => 150, 'HRPCH2S3L2-9' => 151,
      'HRPCH2S3L2-10' => 152, 'HRPCH2S3L2-11' => 153, 'HRPCH2S3L2-12' => 154, 'HRPCH2S3L3-1' => 155,
      'HRPCH2S3L3-2' => 156, 'HRPCH2S3L3-3' => 157, 'HRPCH2S3L3-4' => 158, 'HRPCH2S3L3-5' => 159,
      'HRPCH2S3L3-6' => 160, 'HRPCH2S3L3-7' => 161, 'HRPCH2S3L3-8' => 162, 'HRPCH2S3L3-9' => 163,
      'HRPCH2S3L3-10' => 164, 'HRPCH2S3L3-11' => 165, 'HRPCH2S3L3-12' => 166, 'HRPCH2S3L4-1' => 167,
      'HRPCH2S3L4-2' => 168, 'HRPCH2S3L4-3' => 169, 'HRPCH2S3L4-4' => 170, 'HRPCH2S3L4-5' => 171,
      'HRPCH2S3L4-6' => 172, 'HRPCH2S3L4-7' => 173, 'HRPCH2S3L4-8' => 174, 'HRPCH2S3L4-9' => 175,
      'HRPCH2S3L4-10' => 176, 'HRPCH2S4L1-1' => 181, 'HRPCH2S4L1-2' => 182, 'HRPCH2S4L1-3' => 183,
      'HRPCH2S4L1-4' => 184, 'HRPCH2S4L1-5' => 185, 'HRPCH2S4L1-6' => 186, 'HRPCH2S4L1-7' => 187,
      'HRPCH2S4L1-8' => 188, 'HRPCH2S4L1-9' => 189, 'HRPCH2S4L1-10' => 190, 'HRPCH2S4L1-11' => 191,
      'HRPCH2S4L1-12' => 192, 'HRPCH2S4L1-13' => 193, 'HRPCH2S4L1-14' => 194, 'HRPCH2S4L1-15' => 195,
      'HRPCH2S4L2-1' => 196, 'HRPCH2S4L2-2' => 197, 'HRPCH2S4L2-3' => 198, 'HRPCH2S4L2-4' => 199,
      'HRPCH2S4L2-5' => 200, 'HRPCH2S4L2-6' => 201, 'HRPCH2S4L2-7' => 202, 'HRPCH2S4L2-8' => 203,
      'HRPCH2S4L2-9' => 204, 'HRPCH2S4L3-1' => 205, 'HRPCH2S4L3-2' => 206, 'HRPCH2S4L3-3' => 207,
      'HRPCH2S4L3-4' => 208, 'HRPCH2S4L3-5' => 209, 'HRPCH2S4L3-6' => 210, 'HRPCH2S4L3-7' => 211,
      'HRPCH2S4L3-8' => 212, 'HRPCH2S4L4-1' => 213, 'HRPCH2S4L4-2' => 214, 'HRPCH2S5L2-1' => 224,
      'HRPCH2S5L2-2' => 225, 'HRPCH2S5L2-3' => 226, 'HRPCH2S5L2-4' => 227, 'HRPCH2S5L2-5' => 228,
      'HRPCH2S5L2-6' => 229, 'HRPCH2S5L2-7' => 230, 'HRPCH2S5L2-8' => 231, 'HRPCH2S5L2-9' => 232,
      'HRPCH2S5L2-10' => 233, 'HRPCH2S5L3-1' => 234, 'HRPCH2S5L3-2' => 235, 'HRPCH2S5L3-3' => 236,
      'HRPCH2S5L3-4' => 237, 'HRPCH2S5L3-5' => 238, 'HRPCH2S5L4-1' => 239, 'HRPCH2S5L4-2' => 240,
      'HRPCH2S5L4-3' => 241, 'HRPCH2S5L4-4' => 242, 'HRPCH2S5L4-5' => 243, 'HRPCH2S5L4-6' => 244,
      'HRPCH2S5L4-7' => 245, 'HRPCH2S5L4-8' => 246, 'HRPCH3S1L2-1' => 258, 'HRPCH3S1L2-2' => 259,
      'HRPCH3S1L2-3' => 260, 'HRPCH3S1L2-4' => 261, 'HRPCH3S1L2-5' => 262, 'HRPCH3S1L3-1' => 263,
      'HRPCH3S1L3-2' => 264, 'HRPCH3S1L3-3' => 265, 'HRPCH3S1L4-1' => 266, 'HRPCH3S1L4-2' => 267,
      'HRPCH3S1L4-3' => 268, 'HRPCH3S2L1-1' => 275, 'HRPCH3S2L1-2' => 276, 'HRPCH3S2L2-1' => 277,
      'HRPCH3S2L2-2' => 278, 'HRPCH3S2L2-3' => 279, 'HRPCH3S2L2-4' => 280, 'HRPCH3S2L2-5' => 281,
      'HRPCH3S2L2-6' => 282, 'HRPCH3S2L2-7' => 283, 'HRPCH3S2L2-8' => 284, 'HRPCH3S2L3-1' => 285,
      'HRPCH3S2L3-2' => 286, 'HRPCH3S2L4-1' => 287, 'HRPCH3S3L1-1' => 292, 'HRPCH3S3L1-2' => 293,
      'HRPCH3S3L2-1' => 294, 'HRPCH3S3L2-2' => 295, 'HRPCH3S3L2-3' => 296, 'HRPCH3S3L2-4' => 297,
      'HRPCH3S3L2-5' => 298, 'HRPCH3S3L2-6' => 299, 'HRPCH3S3L2-7' => 300, 'HRPCH3S3L3-1' => 301,
      'HRPCH3S3L3-2' => 302, 'HRPCH3S3L3-3' => 303, 'HRPCH3S3L3-4' => 304, 'HRPCH3S3L3-5' => 305,
      'HRPCH3S3L3-6' => 306, 'HRPCH3S3L3-7' => 307, 'HRPCH3S3L3-8' => 308, 'HRPCH3S3L3-9' => 309,
      'HRPCH3S3L3-10' => 310, 'HRPCH3S3L3-11' => 311, 'HRPCH3S3L4-1' => 312, 'HRPCH3S3L4-2' => 313,
      'HRPCH3S3L4-3' => 314, 'HRPCH3S3L4-4' => 315, 'HRPCH3S4L1-1' => 321, 'HRPCH3S4L2-1' => 322,
      'HRPCH3S4L2-2' => 323, 'HRPCH3S4L2-3' => 324, 'HRPCH3S4L2-4' => 325, 'HRPCH3S4L3-1' => 326,
      'HRPCH3S4L3-2' => 327, 'HRPCH3S4L3-3' => 328, 'HRPCH3S4L3-4' => 329, 'HRPCH3S4L3-5' => 330,
      'HRPCH3S4L3-6' => 331, 'HRPCH3S4L3-7' => 332, 'HRPCH3S4L4-1' => 333, 'HRPCH3S4L4-2' => 334,
      'HRPCH3S4L4-3' => 335, 'HRPCH3S4L4-4' => 336, 'HRPCH3S4L4-5' => 337
    }

    def prepare!(xls)
      xls.filename = FILE
      qa = xls.qa
      s  = qa.supplier_profile
      ad = qa.assessment_day_form
      ass_type = 'Full' if qa.assess_type == 'Annual'
      ass_type = 'CAP Follow-up' if qa.assess_type == 'CAP Close'

      xls.to_excel[0] = {
        'E3' => "#{s.code} #{s.name}",
        'E4' => "#{s.address} (#{qa.site || 'Main Site'})",
        'E5' => ad.steps_assessed,
        'E7' => s.supplier_type,
        'E8' => s.supplier_status,
        # "E9"=> ad.employee_count,
        'I3' => qa.last_assess_date,
        'I4' => qa.assess_date.strftime('%Y-%m-%d'),
        'I5' => ass_type,
        # "I6"=> ad.man_days,
        'I7' => qa.assessors.map(&:name).join(', '),
        'I8' => ad.pls.map(&:name).join(', '),
        'I9' => ad.pl_present ? 'Yes' : 'No'
      }

      qa.all_points.each do |cq|
        r = EXCEL_MAP[cq.chapter_tag]
        result = cq.result || ''
        result = 'Not applicable' if result == 'Not Applicable'
        result = 'No but no risk' if result == 'NOK without risk'

        xls.to_excel[0]["F#{r}"] = result
        xls.to_excel[0]["G#{r}"] = cq.nok_type? ? 'NOK' : result
        (xls.to_excel[0]["H#{r}"] = cq.action_taken ? 'Yes' : 'No') if cq.result == 'NOK' && cq.level == 'E'
        xls.to_excel[0]["J#{r}"] = "Strong Points: #{cq.strong_points} \n\nPoints To Improve: #{cq.points_to_improve}"
      end
    end
  end
end
