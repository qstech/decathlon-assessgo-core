class XlsExport::Sse
  class << self
    def prepare!(xls)
      case xls.qa.grid_version.version_number
      when 1
        XlsExport::Sse::V6.prepare!(xls)
      when 2
        XlsExport::Sse::V8.prepare!(xls)
      end
    end
  end
end
