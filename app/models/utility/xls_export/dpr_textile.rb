require 'json'
class XlsExport::DprTextile
  class << self
    EXCEL_MAP = JSON.parse(File.read("#{Rails.root}/app/models/utility/xls_export/dpr_textile.json"))

    def prepare!(xls)
      qa = xls.qa
      s  = qa.supplier_profile
      ad = qa.assessment_day_form
      tags = qa.chapter_assessments.map(&:chapter_tag)
      processes = DprGuideline.where(guideline_tag: tags).map(&:process_name).uniq
      process_rows = processes.map { |x| EXCEL_MAP['process'][x] }
      steps = qa.chapter_assessments.map(&:section).uniq
      step_rows = steps.map { |x| EXCEL_MAP['steps'][x] }

      xls.to_excel[0] = {
        'B2' => "#{s.name} (#{qa.site || 'Main Site'})",
        'B3' => s.code,
        'B5' => qa.primary_assessor.assessor_profile.name,
        'B12' => qa.to_level,
        'D2' => ad.pls.map(&:name).join(', '),
        'D3' => qa.assess_date.strftime('%Y-%m-%d')
      }

      (15..26).to_a.each do |i|
        xls.to_excel[0]["B#{i}"] = process_rows.include?(i) ? 'Y' : 'N'
      end

      (15..39).to_a.each do |i|
        xls.to_excel[0]["D#{i}"] = step_rows.include?(i) ? 'Y' : 'N'
      end

      xls.to_excel[1] = {}

      qa.all_points.each do |cq|
        r = EXCEL_MAP['chapters'][cq.chapter_tag]
        score = cq.result
        score = 'Not_Audited' if cq.result == 'Not Assessed'
        score = 'Not_Applicable' if cq.result == 'Not Applicable'

        xls.to_excel[1]["F#{r}"] = score
        xls.to_excel[1]["G#{r}"] = ''
        xls.to_excel[1]["G#{r}"] += "Strong points: #{cq.strong_points};  " unless cq.strong_points.blank?
        xls.to_excel[1]["G#{r}"] += "Points to improve: #{cq.points_to_improve}" unless cq.points_to_improve.blank?
      end
    end
  end
end
