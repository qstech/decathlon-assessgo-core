class XlsExport::Scm
  class << self
    # POINT LEVEL
    # EXCEL_MAP = {
    #   "SCMCH1S1L1-1"=>"22", "SCMCH1S2L1-1"=>"23", "SCMCH1S2L1-2"=>"23", "SCMCH1S3L1-1"=>"24", "SCMCH1S3L1-2"=>"24",
    #   "SCMCH1S1L2-1"=>"25", "SCMCH1S1L2-2"=>"25", "SCMCH1S1L2-3"=>"25", "SCMCH1S2L2-1"=>"26", "SCMCH1S3L2-1"=>"27", "SCMCH1S3L2-2"=>"27",
    #   "SCMCH1S1L3-1"=>"28", "SCMCH1S2L3-1"=>"29", "SCMCH1S2L3-2"=>"29", "SCMCH1S3L3-1"=>"30",
    #   "SCMCH1S1L4-1"=>"31", "SCMCH1S2L4-1"=>"32", "SCMCH1S3L4-1"=>"33",
    #   ######################
    #   "SCMCH2S1L1-1"=>"39",
    #   "SCMCH2S1L2-1"=>"41", "SCMCH2S1L2-2"=>"41", "SCMCH2S1L2-3"=>"41",
    #   "SCMCH2S2L2-1"=>"42", "SCMCH2S2L2-2"=>"42", "SCMCH2S2L2-3"=>"42",
    #   "SCMCH2S1L3-1"=>"43", "SCMCH2S2L3-1"=>"44", "SCMCH2S2L3-2"=>"44",
    #   "SCMCH2S1L4-1"=>"45", "SCMCH2S2L4-1"=>"46", "SCMCH2S2L4-2"=>"46",
    #   ######################
    #   "SCMCH3S2L1-1"=>"54", "SCMCH3S3L1-1"=>"55", "SCMCH3S3L1-2"=>"55", "SCMCH3S4L1-1"=>"56",
    #   "SCMCH3S1L2-1"=>"57", "SCMCH3S1L2-2"=>"57", "SCMCH3S2L2-1"=>"58", "SCMCH3S3L2-1"=>"59", "SCMCH3S3L2-2"=>"59", "SCMCH3S3L2-3"=>"59", "SCMCH3S4L2-1"=>"60",
    #   "SCMCH3S1L3-1"=>"61", "SCMCH3S2L3-1"=>"62", "SCMCH3S3L3-1"=>"63", "SCMCH3S3L3-2"=>"63", "SCMCH3S3L3-3"=>"63", "SCMCH3S4L3-1"=>"64",
    #   "SCMCH3S1L4-1"=>"65", "SCMCH3S1L4-2"=>"65", "SCMCH3S2L4-1"=>"66", "SCMCH3S3L4-1"=>"67", "SCMCH3S4L4-1"=>"68",
    #   ######################
    #   "SCMCH4S1L1-1"=>"72", "SCMCH4S1L1-2"=>"72", "SCMCH4S1L1-3"=>"72", "SCMCH4S1L1-4"=>"72", "SCMCH4S1L1-5"=>"72",
    #   "SCMCH4S1L2-1"=>"73", "SCMCH4S1L2-2"=>"73", "SCMCH4S1L2-3"=>"73", "SCMCH4S1L2-4"=>"73",
    #   "SCMCH4S1L3-1"=>"74", "SCMCH4S1L3-2"=>"74",
    #   "SCMCH4S1L4-1"=>"75", "SCMCH4S1L4-2"=>"75", "SCMCH4S1L4-3"=>"75",
    #   ######################
    #   "SCMCH5S1L2-1"=>"82", "SCMCH5S1L2-2"=>"82",
    #   "SCMCH5S2L2-1"=>"83", "SCMCH5S2L2-2"=>"83",
    #   "SCMCH5S2L3-1"=>"85",
    #   "SCMCH5S1L4-1"=>"86", "SCMCH5S1L4-2"=>"86", "SCMCH5S2L4-1"=>"87",
    #   ######################
    #   "SCMCH6S1L1-1"=>"91",
    #   "SCMCH6S1L2-1"=>"92", "SCMCH6S1L2-2"=>"92",
    #   "SCMCH6S1L3-1"=>"93", "SCMCH6S1L3-2"=>"93",
    #   "SCMCH6S1L4-1"=>"94",
    #   ######################
    #   "SCMCH7S1L1-1"=>"102", "SCMCH7S1L1-2"=>"102", "SCMCH7S2L1-1"=>"103", "SCMCH7S2L1-2"=>"103", "SCMCH7S3L1-1"=>"104", "SCMCH7S3L1-2"=>"104",
    #   "SCMCH7S1L2-1"=>"105", "SCMCH7S1L2-2"=>"105", "SCMCH7S1L2-3"=>"105", "SCMCH7S2L2-1"=>"106", "SCMCH7S2L2-2"=>"106", "SCMCH7S3L2-1"=>"107", "SCMCH7S3L2-2"=>"107", "SCMCH7S3L2-3"=>"107",
    #   "SCMCH7S1L3-1"=>"108", "SCMCH7S1L3-2"=>"108", "SCMCH7S2L3-1"=>"109", "SCMCH7S3L3-1"=>"110", "SCMCH7S3L3-2"=>"110",
    #   "SCMCH7S1L4-1"=>"111", "SCMCH7S2L4-1"=>"112", "SCMCH7S3L4-1"=>"113"
    # }

    EXCEL_MAP = {
      'SCMCH1S1L1' => '22', 'SCMCH1S2L1' => '23', 'SCMCH1S3L1' => '24',
      'SCMCH1S1L2' => '25', 'SCMCH1S2L2' => '26', 'SCMCH1S3L2' => '27',
      'SCMCH1S1L3' => '28', 'SCMCH1S2L3' => '29', 'SCMCH1S3L3' => '30',
      'SCMCH1S1L4' => '31', 'SCMCH1S2L4' => '32', 'SCMCH1S3L4' => '33',
      ######################
      'SCMCH2S1L1' => '39',
      'SCMCH2S1L2' => '41',
      'SCMCH2S2L2' => '42',
      'SCMCH2S1L3' => '43', 'SCMCH2S2L3' => '44',
      'SCMCH2S1L4' => '45', 'SCMCH2S2L4' => '46',
      ######################
      'SCMCH3S2L1' => '54', 'SCMCH3S3L1' => '55', 'SCMCH3S4L1' => '56',
      'SCMCH3S1L2' => '57', 'SCMCH3S2L2' => '58', 'SCMCH3S3L2' => '59', 'SCMCH3S4L2' => '60',
      'SCMCH3S1L3' => '61', 'SCMCH3S2L3' => '62', 'SCMCH3S3L3' => '63', 'SCMCH3S4L3' => '64',
      'SCMCH3S1L4' => '65', 'SCMCH3S2L4' => '66', 'SCMCH3S3L4' => '67', 'SCMCH3S4L4' => '68',
      ######################
      'SCMCH4S1L1' => '72',
      'SCMCH4S1L2' => '73',
      'SCMCH4S1L3' => '74',
      'SCMCH4S1L4' => '75',
      ######################
      'SCMCH5S1L2' => '82',
      'SCMCH5S2L2' => '83',
      'SCMCH5S2L3' => '85',
      'SCMCH5S1L4' => '86', 'SCMCH5S2L4' => '87',
      ######################
      'SCMCH6S1L1' => '91',
      'SCMCH6S1L2' => '92',
      'SCMCH6S1L3' => '93',
      'SCMCH6S1L4' => '94',
      ######################
      'SCMCH7S1L1' => '102', 'SCMCH7S2L1' => '103', 'SCMCH7S3L1' => '104',
      'SCMCH7S1L2' => '105', 'SCMCH7S2L2' => '106', 'SCMCH7S3L2' => '107',
      'SCMCH7S1L3' => '108', 'SCMCH7S2L3' => '109', 'SCMCH7S3L3' => '110',
      'SCMCH7S1L4' => '111', 'SCMCH7S2L4' => '112', 'SCMCH7S3L4' => '113'
    }

    def prepare!(xls)
      qa = xls.qa
      s  = qa.supplier_profile
      ad = qa.assessment_day_form
      ass_type = qa.assess_type == 'Self Assessment' ? 'FULL' : 'CAP FOLLOW-UP'

      xls.to_excel[0] = {
        'D3' => "#{s.code} #{s.name}",
        'D4' => "#{s.address} (#{qa.site || 'Main Site'})",
        'D5' => ad.steps_assessed,
        'D7' => s.supplier_type,
        'D8' => s.supplier_status,
        # "E9"=> ad.employee_count,
        # "H3"=> qa.last_assess_date,
        'H4' => qa.assess_date.strftime('%Y-%m-%d'),
        'H5' => ass_type,
        # "H6"=> ad.man_days,
        'H7' => qa.assessors.map { |x| x.name }.join(', '),
        'H8' => ad.pls.map { |x| x.name }.join(', '),
        'H9' => ad.pl_present ? 'Yes' : 'No'
      }

      xls.to_excel[0] = {}
      qa.all_levels.each do |ca|
        r = EXCEL_MAP[ca.chapter_tag]
        next if r.blank?

        score = 'Yes' if ca.score == 'OK'
        score = 'No' if ca.score == 'NOK'
        score = 'N/A' if ca.score == 'Not Applicable'

        xls.to_excel[0]["G#{r}"] = score

        cqs = ca.chapter_questions
        xls.to_excel[0]["I#{r}"] =
          "Strong Points: #{cqs.map(&:strong_points).join("\n")} \n\nPoints To Improve: #{cqs.map(&:points_to_improve).join("\n")}"
      end
    end
  end
end
