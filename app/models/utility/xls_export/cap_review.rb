require 'rubyXL'
require 'rubyXL/convenience_methods'

class XlsExport::CapReview
  include ActiveStorage::Downloading

  attr_reader :blob

  def initialize(blob)
    @blob = blob
  end

  def extract!
    download_blob_to_tempfile do |tmp|
      xls = tmp.read
      @workbook = RubyXL::Parser.parse_buffer(StringIO.new(xls))
      sht = @workbook[0]
      rows = sht[11..150]
      pts = []
      pt = {}
      i = 0
      rows.each do |r|
        next if r.cells.reject { |x| x.nil? || x.value.nil? }.count == 0

        i += 1
        puts "ROW w/Data: #{i}"
        if !r[25].nil? && !r[25].value.blank?
          pts << pt if pt.present?
          pt = {}
          pt[:ref_model] = 'chapter_question'
          pt[:ref_id] = r[25].value       unless r[25].nil?
          pt[:section] = r[0].value       unless r[0].nil?
          pt[:level] = r[1].value         unless r[1].nil?
          pt[:requirement] = r[2].value   unless r[2].nil?
          pt[:result] = r[3].value        unless r[3].nil?
          pt[:nonconformity] = r[4].value unless r[4].nil?
          pt[:root_cause_nc] = r[5].value unless r[5].nil?
          pt[:root_cause_nd] = r[6].value unless r[6].nil?

          pt[:cap_resolutions] = []
        end
        p pt
        next unless !r[8].nil? && r[8].value.present?

        res = {}
        res[:action_type] = r[7].value    unless r[7].nil?
        res[:action] = r[8].value         unless r[8].nil?
        res[:deadline_date] = r[9].value  unless r[9].nil?
        res[:responsible] = r[10].value unless r[10].nil?
        p pt
        p res
        pt[:cap_resolutions] << res
      end
      pts << pt unless pt.blank?
      pts
    end
  end

  class << self
    def prepare!(xls)
      qa = xls.qa
      s  = qa.supplier_profile
      ad = qa.assessment_day_form

      xls.to_excel[0] = {
        'D4' => 'CAP Review',
        'D5' => Time.now.strftime('%Y-%m-%d'),
        'D6' => qa.supplier_code,
        'D7' => ad.pls.map(&:name).join(', '),
        'D8' => "#{qa.short_name} Assessment"
      }

      r = 12
      qa.generate_cap_prep[:points].each do |pt|
        xls.to_excel[0]["A#{r}"] = pt[:section]
        xls.to_excel[0]["B#{r}"] = pt[:level]
        xls.to_excel[0]["C#{r}"] = pt[:requirement]
        xls.to_excel[0]["D#{r}"] = pt[:result]
        xls.to_excel[0]["E#{r}"] = pt[:nonconformity]
        xls.to_excel[0]["Z#{r}"] = pt[:ref_id]
        r += 2
      end
    end

    def extract!(xls); end
  end
end
