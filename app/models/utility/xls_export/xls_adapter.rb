require 'spreadsheet'
class XlsExport::XlsAdapter
  include XlsExport::Utils

  def initialize(filename)
    @workbook = Spreadsheet.open("#{filename}.xls")
  end

  def update_sheets(to_excel = {})
    to_excel.each do |i, set|
      sheet = @workbook.worksheet i
      update_sheet(sheet, set)
      # update_test(sheet)
    end
  end

  def stream
    result = StringIO.new
    @workbook.write result
    result
  end

  private

  def update_sheet(sheet, set)
    set.each do |cell_name, value|
      update_cell(sheet, cell_name, value)
    end
  end

  def update_test(sheet)
    set = { 'E3' => '18889' }
    update_sheet(sheet, set)
  end

  def update_cell(sheet, cell_name, value)
    puts("#{cell_name} -> #{value}")
    coor = cell_to_coordinates(cell_name)
    sheet.rows[coor[0]][coor[1]] = value
  end
end
