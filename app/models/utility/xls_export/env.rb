class XlsExport::Env
  class << self
    EXCEL_MAP = {
      'ENVCH1S1L1-1' => '26', 'ENVCH1S1L1-2' => '27',
      'ENVCH1S1L2-1' => '29', 'ENVCH1S1L2-2' => '30', 'ENVCH1S1L2-3' => '31',
      'ENVCH1S1L2-4' => '32', 'ENVCH1S1L2-5' => '33', 'ENVCH1S1L2-6' => '34',
      'ENVCH1S1L2-7' => '36', 'ENVCH1S1L2-8' => '37', 'ENVCH1S1L2-9' => '38', 'ENVCH1S1L2-10' => '39',
      'ENVCH1S1L3-1' => '40', 'ENVCH1S1L3-2' => '41', 'ENVCH1S1L3-3' => %w[43 44 45],
      'ENVCH1S1L3-4' => '46', 'ENVCH1S1L3-5' => '47', 'ENVCH1S1L3-6' => '48',
      'ENVCH1S1L3-7' => %w[50 51 52], 'ENVCH1S1L3-8' => '53', 'ENVCH1S1L3-9' => '54', 'ENVCH1S1L3-10' => '55',
      'ENVCH1S1L4-1' => '56', 'ENVCH1S1L4-2' => '57', 'ENVCH1S1L4-3' => '58', 'ENVCH1S1L4-4' => '59',
      'ENVCH1S2L1-1' => '65', 'ENVCH1S2L1-2' => '66', 'ENVCH1S2L1-3' => '67',
      'ENVCH1S2L2-1' => '69', 'ENVCH1S2L2-2' => '70', 'ENVCH1S2L2-3' => '71',
      'ENVCH1S2L2-4' => '72', 'ENVCH1S2L2-5' => '73',
      'ENVCH1S2L3-1' => '74', 'ENVCH1S2L3-2' => '75', 'ENVCH1S2L3-3' => '76',
      'ENVCH1S2L3-4' => '77', 'ENVCH1S2L3-5' => %w[79 80 81], 'ENVCH1S2L3-6' => '82',
      'ENVCH1S2L4-1' => '83',
      'ENVCH1S3L1-1' => '92',
      'ENVCH1S3L2-1' => '93', 'ENVCH1S3L2-2' => '94',
      'ENVCH1S3L2-3' => '95', 'ENVCH1S3L2-4' => '96', 'ENVCH1S3L2-5' => '97',
      'ENVCH1S3L3-1' => '98', 'ENVCH1S3L3-2' => '99', 'ENVCH1S3L3-3' => '100',
      'ENVCH1S3L3-4' => '101', 'ENVCH1S3L3-5' => '102', 'ENVCH1S3L3-6' => '103',
      'ENVCH1S3L4-1' => '104', 'ENVCH1S3L4-2' => '105', 'ENVCH1S3L4-3' => '106',
      'ENVCH1S3L4-4' => '107', 'ENVCH1S3L4-5' => '108'
    }

    def prepare!(xls)
      qa = xls.qa
      s  = qa.supplier_profile
      ad = qa.assessment_day_form

      ass_type = 'Full' if qa.assess_type == 'Annual'
      ass_type = 'CAP Follow-up' if qa.assess_type == 'CAP Close'

      xls.to_excel[0] = {
        'E2' => "#{s.code} #{s.name}",
        'E3' => qa.site || 'Main Site',
        'E4' => s.address,
        'E5' => 'To modify: Person in charge',
        'E6' => ad.steps_assessed,
        'E8' => s.supplier_type,
        'E9' => s.supplier_status,
        'E10' => 'To modify: Industrial Process on sites',
        # "E11"=> ad.employee_count,
        'E12' => 'To modify: Peak Season of production (Month)',

        'I2' => qa.last_assess_date,
        'I3' => qa.assess_date.strftime('%Y-%m-%d'),
        'I4' => ass_type,
        'I5' => 'To modify: Contact person in charge',
        'I6' => qa.assessors.map { |x| x.name }.join(', '),
        'I7' => ad.pls.map { |x| x.name }.join(', '),
        'I8' => ad.pl_present ? 'Yes' : 'No',
        'I8' => 'To modify: Quantity of water used for industrial',
        'I10' => 'To modify: Quantity of electricity and/or steam',
        'I11' => 'To modify: Type(s) of Fuel used to produce',
        'I12' => 'To modify: Certification Obtained'
      }

      qa.all_points.each do |cq|
        r = EXCEL_MAP[cq.chapter_tag]
        r.is_a?(Array) ? r.each { |row| insert_to_grid(xls, row, cq) } : insert_to_grid(xls, r, cq)
      end
    end

    def insert_to_grid(xls, r, cq)
      xls.to_excel[0]["F#{r}"] = cq.result || ''
      xls.to_excel[0]["G#{r}"] = cq.result || ''
      (xls.to_excel[0]["H#{r}"] = cq.action_taken ? 'Yes' : 'No') if cq.result == 'NOK' && cq.level == 'E'
      xls.to_excel[0]["I#{r}"] = "Strong Points: #{cq.strong_points} \n\nPoints To Improve: #{cq.points_to_improve}"
    end
  end
end
