class XlsExport::Pur
  class << self
    EXCEL_MAP = {
      'PURCH1S3L2' => '3', 'PURCH1S3L3' => '4', 'PURCH1S3L4' => '5', 'PURCH1S3L5' => '6', # BOM
      'PURCH1S1L2' => '9', 'PURCH1S1L3' => '10', 'PURCH1S1L4' => '11', 'PURCH1S1L5' => '12', # RAW MATERIALS
      'PURCH1S2L2' => '15', 'PURCH1S2L3' => '16', 'PURCH1S2L4' => '17', 'PURCH1S2L5' => '18', # COMPONENTS
      'PURCH1S4L2' => '21', 'PURCH1S4L3' => '22', 'PURCH1S4L4' => '23', 'PURCH1S4L5' => '24', # CBD
      'PURCH1S5L2' => '27', 'PURCH1S5L3' => '28', 'PURCH1S5L4' => '29', 'PURCH1S5L5' => '30', # PPM
      'PURCH1S6L2' => '33', 'PURCH1S6L3' => '34', 'PURCH1S6L4' => '35', 'PURCH1S6L5' => '36', # CONSUMPTION
      'PURCH1S7L2' => '39', 'PURCH1S7L3' => '40', 'PURCH1S7L4' => '41', 'PURCH1S7L5' => '42', # STANDARD TIME
      'PURCH1S9L2' => '45', 'PURCH1S9L3' => '46', 'PURCH1S9L4' => '47', 'PURCH1S9L5' => '48', # BENCHMARKING
      'PURCH1S8L2' => '51', 'PURCH1S8L3' => '52', 'PURCH1S8L4' => '53', 'PURCH1S8L5' => '54', # NEGOTIATION
      'PURCH1S10L2' => '57', 'PURCH1S10L3' => '58', 'PURCH1S10L4' => '59', 'PURCH1S10L5' => '60' # COSTING DATABASE
    }

    def prepare!(xls)
      qa = xls.qa
      s  = qa.supplier_profile
      ad = qa.assessment_day_form
      xls.to_excel[1] = {
        'B2' => "#{s.code} - #{s.name} (#{qa.site || 'Main Site'})",
        'B3' => s.status,
        'B4' => qa.assess_date.strftime('%Y-%m-%d'),
        'B5' => qa.primary_assessor.assessor_profile.name,
        'B6' => ad.pls.map(&:name).join(', '),
        'B7' => ad.pls.map(&:name).join(', '),
        'B8' => ad.observer_names.join(','),
        'B9' => ad.people_encountered
      }

      xls.to_excel[3] = {}

      qa.all_levels.each do |ca|
        r = EXCEL_MAP[ca.chapter_tag]
        next if r.blank?

        # score = 'Not audited' if ca.score == "Not Applicable"
        xls.to_excel[3]["D#{r}"] = ca.score
        xls.to_excel[3]["E#{r}"] = ca.chapter_questions.map(&:strong_points)
                                     .reject { |x| x.blank? || x == 'N/A' }.join("\n\n")
        xls.to_excel[3]["F#{r}"] = ca.chapter_questions.map(&:points_to_improve)
                                     .reject { |x| x.blank? || x == 'N/A' }.join("\n\n")
      end

      xls.to_excel[4] = {
        'E6' => qa.strong_points,
        'J6' => qa.points_to_improve
      }
      # puts xls.to_excel.values.map(&:keys)
    end
  end
end
