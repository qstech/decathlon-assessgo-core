class XlsExport::Qa
  class << self
    EXCEL_MAP = {
      'QACH1S1L1' => '23', 'QACH1S1L2' => '24', 'QACH1S1L3' => '25',
      'QACH1S2L1' => '30', 'QACH1S2L2' => '31', 'QACH1S2L3' => '32',
      'QACH1S3L1' => '37', 'QACH1S3L2' => '38', 'QACH1S4L1' => '43',
      'QACH1S4L2' => '44', 'QACH1S4L3' => '45', 'QACH2S1L1' => '51',
      'QACH2S1L2' => '52', 'QACH2S1L3' => '53', 'QACH3S1L1' => '59',
      'QACH3S1L2' => '60', 'QACH3S1L3' => '61', 'QACH3S2L1' => '66',
      'QACH3S2L2' => '67', 'QACH3S2L3' => '68', 'QACH3S3L1' => '73',
      'QACH3S3L2' => '74', 'QACH3S3L3' => '75', 'QACH3S4L1' => '80',
      'QACH3S4L2' => '81', 'QACH3S4L3' => '82', 'QACH3S4L4' => '83',
      'QACH3S5L1' => '88', 'QACH3S5L2' => '89', 'QACH3S5L3' => '90',
      'QACH3S6L1' => '95', 'QACH3S6L2' => '96', 'QACH3S6L3' => '97',
      'QACH3S7L1' => '102', 'QACH3S7L2' => '103', 'QACH3S7L3' => '104',
      'QACH4S1L1' => '110', 'QACH4S1L2' => '111', 'QACH4S1L3' => '112',
      'QACH4S1L4' => '113'
    }

    def prepare!(xls)
      qa = xls.qa
      s  = qa.supplier_profile
      ad = qa.assessment_day_form

      cas = qa.all_levels

            # if qa.assess_type == 'CAP Close'
            #   qa.full_chapter_assessments
            # else
            #   qa.chapter_assessments
            # end

      xls.to_excel[2] = {
        'D2' => "#{s.code} #{s.name}",
        'D3' => qa.assess_date.strftime('%Y-%m-%d'),
        'D4' => qa.primary_assessor.assessor_profile.name,
        'D5' => ad.pls.map(&:name).join(', '),
        'D6' => ad.pl_present ? 'Yes' : 'No',
        'D7' => ad.observer_names.join(','),
        'D8' => ad.people_encountered,
        'D9' => ad.steps_assessed,
        'D10' => ad.steps_assessed,
        'D11' => qa.assess_type,
        'D12' => s.industrial_process,
        'D13' => s.status
      }

      xls.to_excel[3] = {
        'E11' => xls.result,
        'J2' => qa.result,
        'J3' => qa.cotation * 0.01
      }

      cas.each do |ca|
        r = EXCEL_MAP[ca.chapter_tag]
        score = 'Not audited' if ca.score == 'Not Assessed'
        xls.to_excel[2]["C#{r}"] = score || ca.score
        xls.to_excel[2]["D#{r}"] = ca.chapter_questions.map(&:strong_points).join("\n\n")
        xls.to_excel[2]["E#{r}"] = ca.chapter_questions.map(&:points_to_improve).join("\n\n")
      end
    end
  end
end
