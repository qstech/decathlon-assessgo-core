# frozen_string_literal: true

class XlsExport::Sse::V6
  class << self
    EXCEL_MAP = {
      'SSECH1S1L2' => '23', 'SSECH1S1L3' => '24', 'SSECH1S1L4' => '25',
      'SSECH2S1L1' => '31', 'SSECH2S1L2' => '32', 'SSECH2S1L3' => '33', 'SSECH2S1L4' => '34',
      'SSECH2S2L1' => '39', 'SSECH2S2L2' => '40', 'SSECH2S2L3' => '41', 'SSECH2S2L4' => '42',
      'SSECH2S3L1' => '47', 'SSECH2S3L2' => '48', 'SSECH2S3L3' => '49', 'SSECH2S3L4' => '50',
      'SSECH2S4L1' => '56', 'SSECH2S4L2' => '57', 'SSECH2S4L3' => '58', 'SSECH2S4L4' => '59',
      'SSECH2S5L1' => '63', 'SSECH2S5L2' => '64', 'SSECH2S5L3' => '65', 'SSECH2S5L4' => '66',
      'SSECH2S6L1' => '71', 'SSECH2S6L2' => '72', 'SSECH2S6L3' => '73', 'SSECH2S6L4' => '74',
      'SSECH2S7L2' => '79', 'SSECH2S7L3' => '80', 'SSECH2S7L4' => '81'
    }.freeze

    def prepare!(xls)
      qa = xls.qa
      s  = qa.supplier_profile
      ad = qa.assessment_day_form

      xls.to_excel[3] = {
        'F2' => "#{s.code} - #{s.name} (#{qa.site || 'Main Site'})",
        'F3' => qa.assess_date.strftime('%Y-%m-%d'),
        'F4' => qa.primary_assessor.assessor_profile.name,
        'F5' => ad.pls.map(&:name).join(', '),
        'F6' => ad.pl_present ? 'Yes' : 'No',
        'F7' => ad.observer_names.join(','),
        'F8' => ad.people_encountered,
        'F9' => ad.steps_assessed,
        'F10' => qa.assess_type,
        'F11' => qa.target,
        'F12' => s.status
      }

      xls.to_excel[4] = {
        'E11' => xls.result
      }

      qa.all_levels.each do |ca|
        r = EXCEL_MAP[ca.chapter_tag]
        score = 'Not audited' if ca.score == 'Not Assessed'
        score = 'Not applicable' if ca.score == 'Not Applicable'

        xls.to_excel[3]["E#{r}"] = score || ca.score
        xls.to_excel[3]["F#{r}"] = ca.chapter_questions.map(&:strong_points)
                                     .reject { |x| x.blank? || x == 'N/A' }.join("\n\n")
        xls.to_excel[3]["G#{r}"] = ca.chapter_questions.map(&:points_to_improve)
                                     .reject { |x| x.blank? || x == 'N/A' }.join("\n\n")
      end
    end
  end
end
