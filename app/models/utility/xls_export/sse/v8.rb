# frozen_string_literal: true

class XlsExport::Sse::V8
  class << self
    FILE = "#{Rails.root}/lib/grids/sse_v8"
    EXCEL_MAP = {
      'SSECH1S1L2' => '23', 'SSECH1S1L3' => '24', 'SSECH1S1L4' => '25',
      'SSECH2S1L2' => '31', 'SSECH2S1L3' => '32', 'SSECH2S1L4' => '33',
      'SSECH3S1L1' => '39', 'SSECH3S1L2' => '40', 'SSECH3S1L3' => '41', 'SSECH3S1L4' => '42',
      'SSECH3S2L1' => '47', 'SSECH3S2L2' => '48', 'SSECH3S2L3' => '49', 'SSECH3S2L4' => '50',
      'SSECH3S3L1' => '55', 'SSECH3S3L2' => '56', 'SSECH3S3L3' => '57', 'SSECH3S3L4' => '58',
      'SSECH3S4L1' => '63', 'SSECH3S4L2' => '64', 'SSECH3S4L3' => '65', 'SSECH3S4L4' => '66',
      'SSECH3S5L1' => '71', 'SSECH3S5L2' => '72', 'SSECH3S5L3' => '73', 'SSECH3S5L4' => '74',
      'SSECH3S6L1' => '79', 'SSECH3S6L2' => '80', 'SSECH3S6L3' => '81', 'SSECH3S6L4' => '82',
      'SSECH3S7L2' => '87', 'SSECH3S7L3' => '88', 'SSECH3S7L4' => '89'
    }.freeze

    def prepare!(xls)
      xls.filename = FILE
      qa = xls.qa
      s  = qa.supplier_profile
      ad = qa.assessment_day_form

      xls.to_excel[3] = {
        'F2' => "#{s.code} - #{s.name} (#{qa.site || 'Main Site'})",
        'F3' => qa.assess_date.strftime('%Y-%m-%d'),
        'F4' => qa.primary_assessor.assessor_profile.name,
        'F5' => ad.pls.map(&:name).join(', '),
        'F6' => ad.pl_present ? 'Yes' : 'No',
        'F7' => ad.observer_names.join(','),
        'F8' => ad.people_encountered,
        'F9' => ad.steps_assessed,
        'F10' => qa.assess_type,
        'F11' => qa.target,
        'F12' => s.status,
        'F13' => s.industrial_process
      }

      xls.to_excel[4] = {
        'E11' => xls.result
      }

      qa.all_levels.each do |ca|
        r = EXCEL_MAP[ca.chapter_tag]
        score = 'Not audited' if ca.score == 'Not Assessed'
        score = 'Not applicable' if ca.score == 'Not Applicable'

        xls.to_excel[3]["E#{r}"] = score || ca.score
        xls.to_excel[3]["F#{r}"] = ca.chapter_questions.map(&:strong_points)
                                     .reject { |x| x.blank? || x == 'N/A' }.join("\n\n")
        xls.to_excel[3]["G#{r}"] = ca.chapter_questions.map(&:points_to_improve)
                                     .reject { |x| x.blank? || x == 'N/A' }.join("\n\n")
      end
    end
  end
end
