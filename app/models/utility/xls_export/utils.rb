module XlsExport::Utils
  def cell_to_coordinates(cell_name)
    x = cell_name.match(/(\D)(\d+)/)
    i = ('A'..'Z').to_a.index(x[1])
    r = x[2].to_i - 1
    [r, i]
  end
end
