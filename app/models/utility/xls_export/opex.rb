class XlsExport::Opex
  class << self
    EXCEL_MAP = {
      'OPEXCH1S1' => { sheet: 2, row: '52', score_pos: 'C41' }, 'OPEXCH1S2' => { sheet: 2, row: '53', score_pos: 'C42' }, 'OPEXCH1S3' => { sheet: 2, row: '54', score_pos: 'C43' },
      'OPEXCH1S4' => { sheet: 2, row: '55', score_pos: 'C44' }, 'OPEXCH1S5' => { sheet: 2, row: '56', score_pos: 'C45' }, 'OPEXCH1S6' => { sheet: 2, row: '57', score_pos: 'C46' },

      'OPEXCH2S1' => { sheet: 3, row: '54', score_pos: 'C41' }, 'OPEXCH2S2' => { sheet: 3, row: '55', score_pos: 'C42' }, 'OPEXCH2S3' => { sheet: 3, row: '56', score_pos: 'C43' },
      'OPEXCH2S4' => { sheet: 3, row: '57', score_pos: 'C44' }, 'OPEXCH2S5' => { sheet: 3, row: '58', score_pos: 'C45' }, 'OPEXCH2S6' => { sheet: 3, row: '59', score_pos: 'C46' },
      'OPEXCH2S7' => { sheet: 3, row: '60', score_pos: 'C47' }, 'OPEXCH2S8' => { sheet: 3, row: '61', score_pos: 'C48' },

      'OPEXCH3S1' => { sheet: 4, row: '54', score_pos: 'C41' }, 'OPEXCH3S2' => { sheet: 4, row: '55', score_pos: 'C42' }, 'OPEXCH3S3' => { sheet: 4, row: '56', score_pos: 'C43' },
      'OPEXCH3S4' => { sheet: 4, row: '57', score_pos: 'C44' }, 'OPEXCH3S5' => { sheet: 4, row: '58', score_pos: 'C45' }, 'OPEXCH3S6' => { sheet: 4, row: '59', score_pos: 'C46' },
      'OPEXCH3S7' => { sheet: 4, row: '60', score_pos: 'C47' }, 'OPEXCH3S8' => { sheet: 4, row: '61', score_pos: 'C48' },

      'OPEXCH4S1' => { sheet: 5, row: '58', score_pos: 'C41' }, 'OPEXCH4S2' => { sheet: 5, row: '59', score_pos: 'C42' }, 'OPEXCH4S3' => { sheet: 5, row: '60', score_pos: 'C43' },
      'OPEXCH4S4' => { sheet: 5, row: '61', score_pos: 'C44' }, 'OPEXCH4S5' => { sheet: 5, row: '62', score_pos: 'C45' }, 'OPEXCH4S6' => { sheet: 5, row: '63', score_pos: 'C46' },
      'OPEXCH4S7' => { sheet: 5, row: '64', score_pos: 'C47' }, 'OPEXCH4S8' => { sheet: 5, row: '65', score_pos: 'C48' }, 'OPEXCH4S9' => { sheet: 5, row: '66', score_pos: 'C49' },
      'OPEXCH4S10' => { sheet: 5, row: '67', score_pos: 'C50' }, 'OPEXCH4S11' => { sheet: 5, row: '68', score_pos: 'C51' }, 'OPEXCH4S12' => { sheet: 5, row: '69', score_pos: 'C52' },

      'OPEXCH5S1' => { sheet: 6, row: '54', score_pos: 'C41' }, 'OPEXCH5S2' => { sheet: 6, row: '55', score_pos: 'C42' }, 'OPEXCH5S3' => { sheet: 6, row: '56', score_pos: 'C43' },
      'OPEXCH5S4' => { sheet: 6, row: '57', score_pos: 'C44' }, 'OPEXCH5S5' => { sheet: 6, row: '58', score_pos: 'C45' }, 'OPEXCH5S6' => { sheet: 6, row: '59', score_pos: 'C46' },
      'OPEXCH5S7' => { sheet: 6, row: '60', score_pos: 'C47' }, 'OPEXCH5S8' => { sheet: 6, row: '61', score_pos: 'C48' }
    }

    def prepare!(xls)
      qa = xls.qa
      s  = qa.supplier_profile
      ad = qa.assessment_day_form
      ss = qa.section_scores

      xls.to_excel[0] = {
        'B4' => qa.assess_date.strftime('%Y-%m-%d'),
        'H4' => "#{s.code} - #{s.name} (#{qa.site || 'Main Site'})",
        'L4' => ad.observer_names.join(',')
      }

      # OVERALL COMMENTS
      xls.to_excel[1] = {
        'B56' => "Strong Points: #{qa.strong_points} \n\nPoints To Improve: #{qa.points_to_improve}\n\nOverall Comments: #{qa.overall_comments}"
      }

      qa.all_levels.each do |ca|
        excel_pos = EXCEL_MAP[ca.section]
        next if excel_pos.blank?

        r = excel_pos[:row]
        sheet = excel_pos[:sheet]
        score_pos = excel_pos[:score_pos]
        xls.to_excel[sheet] = {} if xls.to_excel[sheet].nil?

        # SCORES
        score = ss[ca.section].nil? ? 'N/A' : ss[ca.section][:level]
        xls.to_excel[sheet][score_pos] = score

        # EVALUATION
        strong = ca.chapter_questions.map(&:strong_points).reject { |x| x.blank? || x == 'N/A' }.join("\n - ")
        improve = ca.chapter_questions.map(&:points_to_improve).reject { |x| x.blank? || x == 'N/A' }.join("\n - ")
        xls.to_excel[sheet]["B#{r}"] = '' if xls.to_excel[sheet]["B#{r}"].nil?

        xls.to_excel[sheet]["B#{r}"] += "\nStrong points (#{ca.level}):\n#{strong}\n" if strong.present?
        xls.to_excel[sheet]["B#{r}"] += "\nPoints to improve (#{ca.level}):\n#{improve}\n" if improve.present?
      end
    end
  end
end
