require 'prawn'
require 'mini_magick'
require 'open-uri'
require 'base64'
require_relative 'pdf_modules/pdf_radars'
require_relative 'pdf_modules/pdf_grid_summary'
require_relative 'pdf_modules/pdf_img_grid'
require_relative 'pdf_modules/pdf_layouts'
require_relative 'pdf_modules/pdf_results'
require_relative 'pdf_modules/pdf_basic_info'
require_relative 'pdf_modules/pdf_chapter_content_v2'
require_relative 'pdf_modules/pdf_control_plan_content'

class PdfGenerator
  attr_accessor :qa
  attr_reader :pdf

  # Vertical Rhythm
  RHYTHM = 10
  # margins
  BOX_MARGIN = 70
  INNER_MARGIN = 20
  PAGE_MARGIN = 20
  MARGIN_BOTTOM = 56

  # Adjustment for axis calculation
  ADJUSTMENT = MARGIN_BOTTOM - PAGE_MARGIN

  # Header
  HEADER_HEIGHT = BOX_MARGIN * 2 + RHYTHM * 2

  # Colors
  WHITE           = 'FFFFFF'
  BLACK           = '000000'
  LIGHT_GRAY      = 'FBF7F0'
  MED_LIGHT_GRAY  = 'C2C0C4'
  MED_GRAY        = 'EFEFEF'
  MED_DARK_GRAY   = 'B5B5B5'
  DARK_GRAY       = '4A4A4A'
  LIGHT_BLUE      = '39A3EA'
  BLUE            = '0198F1'
  RED             = 'F2735D'
  GREEN           = '568D77'
  BROWN           = 'A57E63'
  ORANGE          = 'FDB879'
  PURPLE          = 'A147C5'
  YELLOW          = 'FFF8CE'
  EP_YELLOW       = 'FFF899'
  DARK_YELLOW     = 'DFC629'
  LIGHT_GREEN     = '69A78E'
  OFFWHITE        = 'FAF7F0'

  LEVEL_COLORS = { 'E' => BLACK, 'EP' => EP_YELLOW, 'D' => RED, 'C' => ORANGE, 'B' => GREEN, 'A' => BLUE }

  def initialize(qa = nil, full = true)
    return true if qa.nil?

    if qa.instance_of?(QualityAssessment)
      @qa = qa
      @supplier = qa.supplier_profile
      @scores = full ? qa.score_calc_full : qa.score_calc
      @radars = qa.radars_for_score_set(@scores)
      @radars = qa.radar_set if qa.assess_type == 'CAP Review'
      # @pdf = render_pdf
    elsif qa.instance_of?(ControlPlan)
      @cp = qa
      @product_family = @cp.product_family
      @supplier = @product_family.supplier_profile
      @radars = @cp.radar_set
    end
  end
  # ============================================================================
  # ============================================================================
  #                   ROUTERS / SECTION LOGIC / UTILITY
  # ============================================================================
  # ============================================================================

  def render_pdf
    Prawn::Document.new(page_size: 'A4', margin: PAGE_MARGIN, bottom_margin: MARGIN_BOTTOM) do |pdf|
      base_flow(pdf)
    end.render
  end

  def base_flow(pdf)
    puts 'in prawn'
    set_attributes(pdf)
    puts 'attributes_set'

    if @qa.present?
      create_qa_report(pdf)
    elsif @cp.present?
      create_cp_report(pdf)
    end

    puts 'doc set!'
  end

  def set_attributes(pdf)
    @doc_height = pdf.bounds.absolute_top + PAGE_MARGIN
    @doc_width = pdf.bounds.absolute_left + pdf.bounds.absolute_right
  end

  def self.generate_samples!
    # PdfGenerator.new(QualityAssessment.first).generate!("test_qa_gemba.pdf")
    # PdfGenerator.new(QualityAssessment.find_by(completed: true, grid: "quality_assessment", assess_type: "Annual")).generate!("test_qa_annual.pdf")
    # PdfGenerator.new(ControlPlan.last).generate!("test_cp.pdf")
    PdfGenerator.new(QualityAssessment.last).generate!('test_qa_cap_review.pdf')

    # PdfGenerator.new(QualityAssessment.where(completed: true, grid: "hrp_assessment", assess_type: "Annual").first).generate!("test_hrp.pdf")
  end

  def generate!(filepath)
    @offline = true
    Prawn::Document.generate(filepath, page_size: 'A4', margin: PAGE_MARGIN, bottom_margin: MARGIN_BOTTOM) do |pdf|
      base_flow(pdf)
    end
  end

  def init_cn_font(pdf)
    pdf.font_families.update('Microhei' => {
                               normal: "#{Rails.root}/public/msyh.ttc",
                               bold: "#{Rails.root}/public/msyhbd.ttc"
                             })
    pdf.font 'Microhei'
  end

  def create_cp_report(pdf)
    init_cn_font(pdf)
    cover_page(pdf)

    pdf.start_new_page
    section(pdf, 'Control Plan Results')

    pdf.start_new_page
    section(pdf, 'Control Plan Monitoring')
    # # pdf.start_new_page
    # # section(pdf, "Go For Assessment")
    # pdf.start_new_page
    # section(pdf, "Chapter Assessments")

    footer(pdf)
  end

  def create_qa_report(pdf)
    init_cn_font(pdf)
    cover_page(pdf)

    if @qa.assess_type != 'CAP Review'
      qa_standard_report(pdf)
    else
      qa_cap_review(pdf)
    end

    footer(pdf)
  end

  def qa_cap_review(pdf)
    pdf.start_new_page
    section(pdf, 'Overall Results')

    pdf.start_new_page
    section(pdf, 'CAP Review')
  end

  def qa_standard_report(pdf)
    pdf.start_new_page
    section(pdf, 'Overall Results')

    pdf.start_new_page
    section(pdf, 'Assessment Preparation')
    # pdf.start_new_page
    # section(pdf, "Go For Assessment")
    pdf.start_new_page
    section(pdf, 'Chapter Assessments')
  end

  def route_content(pdf, title)
    case title
    when 'Assessment Preparation' then render_prep_questions(pdf)
    when 'Go For Assessment' then render_day_form(pdf)
    when 'Chapter Assessments' then render_chapter_assessments(pdf)
    when 'Grid Summary' then render_report_summary(pdf)
    when 'Overall Results' then render_overall_results(pdf)
    when 'Control Plan Results' then render_cp_results(pdf)
    when 'Control Plan Monitoring' then render_cp_monitoring(pdf)
    when 'CAP Review' then render_qa_cap_review(pdf)
    end
  end

  def calc_lines(text, max = 31)
    return 2 if text.blank?

    text = text.join("\n") if text.is_a? Array
    extra_lines = text.count("\n")

    return extra_lines + 1 if text.length < max

    result = 0
    lines = text.split("\n")
    lines.each do |line|
      new_lines = []
      arr = line.split(/(\s|-)/)
      char_count = 0
      arr.each do |word|
        char_count += word.length

        if char_count > max
          result += 1
          new_lines = [word]
          char_count = word.length
        else
          new_lines << word
        end
      end
      result += 1
    end
    result
  end
end
