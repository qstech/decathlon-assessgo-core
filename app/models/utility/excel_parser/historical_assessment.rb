require 'creek'
class ExcelParser::HistoricalAssessment
  def initialize(excel_file)
    @file = excel_file.original_filename
    @creek = Creek::Book.new excel_file.path, check_file_extension: false
  end

  def parse_excel
    analyze_excel
  end

  def analyze_excel
    sheet = @creek.sheets[0]

    sheet.rows_with_meta_data.each do |row|
      row['cells'] = row['cells'].reject { |k, _v| k > "F#{row['r']}" || k.size > (row['r'].to_i + 1) }

      content_flat = row['cells'].values.join(' ')
      next if content_flat.include?('HISTORICAL') || content_flat.include?('supplier_code')

      full_cells = row['cells'].reject { |_k, v| v.blank? }
      next if full_cells.blank?

      values = full_cells.values
      supplier = SupplierProfile.find_by_code(values[0].gsub('.0', ''))
      grid = QualityAssessment.lookup_grid(values[1])
      next if supplier.nil? || grid.nil?
      next unless values[2].in?(['Annual', 'CAP Close'])

      params = {
        supplier_profile_id: supplier.id,
        supplier_code: "#{supplier.code} #{supplier.name}",
        grid: grid,
        assess_type: values[2],
        assess_date: values[3],
        result: values[4],
        cotation: values[5],
        historical: true
      }

      QualityAssessment.create(params)
    end
  end
end
