class PagePrepper::ControlPlanPage
  attr_accessor :params, :current_user, :app

  OFFLINE_PAGES = %w[index review review_detail]

  def initialize(current_user, params, cp, response, app)
    @current_user = current_user
    @params = params
    @cp = cp
    @app = app
    @init_response = response.dup
  end

  def render(page)
    @response = @init_response.dup
    @response[:t] = I18n.t("api.v1.control_plan.#{page}")
    raise StandardError, 'PAGE NOT VALID' unless OFFLINE_PAGES.include?(page)

    public_send(page.to_sym)
  end

  def index
    control_plans = [@cp]
    index_hash = {
      header: {
        bgColor: 'purple',
        tableLogo: '/images/logos/cp_logo.jpg',
        tableDesc: 'Master your process, you will master your product',
        buttonText: 'Schedule CP ➜',
        toPage: 'newQa',
        url: '/members/control_plan/schedule'
      },
      controlPlans: control_plans.map { |x| PageComponent.index_cp_card(x) }
    }
    @response.merge!(index_hash)

    @response
  end

  def review
    control_plan = @cp
    product_family = control_plan.product_family
    edit_product_web_url = NativeUrl.generate('control_plan/new-product', current_user, product_id: product_family.id)
    supplier = control_plan.product_family.supplier_profile
    plan_items = control_plan.plan_items
    plan_items_array = plan_items.map { |x| x.process_step_id }
    product_processes = ProductProcess.where(product_family_id: product_family.id).includes(:process_steps)
    pp_array = []

    product_processes.each do |process|
      p_hash = process.standard_hash.merge({ steps: [] })

      process.process_steps_active.each do |step|
        next unless plan_items_array.include?(step.id)

        plan_item = plan_items.select { |x| x.process_step_id == step.id }[0]
        p_hash[:steps].push(step.standard_hash.merge({
                                                       plan_item_id: plan_item.id,
                                                       plan_item_completed: plan_item.completed,
                                                       plan_item_result: plan_item.result
                                                     }))

        p_hash[:steps].sort_by! { |x| x[:step_number] }
      end
      pp_array.push(p_hash) if p_hash[:steps].length > 0
    end

    active_steps = pp_array.map { |x| x[:steps].select { |s| !s[:plan_item_id].nil? } }.flatten
    active_steps.sort_by! { |x| x[:step_number] }

    review_hash = {
      header: {
        bgColor: 'blue'
      },
      controlPlan: control_plan.to_h,
      supplier: {
        name: supplier.name
      },
      productFamily: {
        id: product_family.id,
        name: product_family.name,
        web_url: edit_product_web_url
      },
      processCategories: ProductProcess::CATEGORY,
      processList: pp_array.group_by { |x| x[:category] },
      active_steps: active_steps
    }

    cp_floating_home
    @response.merge!(review_hash)
    @response
  end

  def review_detail
    plan_item = PlanItem.find(params[:plan_item_id])
    process_step = plan_item.process_step
    process_step_hash = process_step.standard_hash.merge!({ category: process_step.product_process.category })
    control_plan = plan_item.control_plan
    plan_items = control_plan.plan_items
    plan_items_array = plan_items.map { |x| x.process_step_id }
    product_family = control_plan.product_family
    supplier = product_family.supplier_profile
    product_processes = ProductProcess.where(product_family_id: product_family.id).includes(:process_steps)
    images = {}
    images[process_step.id] = {
      acceptable_image: process_step.acceptable_image_url,
      unacceptable_image: process_step.unacceptable_image_url
    }

    active_plan_items = []
    active_process_steps = []
    product_processes.each do |process|
      process.process_steps_active.each do |step|
        next unless plan_items_array.include?(step.id)

        s = plan_items.select { |x| x.process_step_id == step.id }[0]
        active_plan_items << s unless s.nil?
        active_process_steps << step
      end
    end

    sorted_ids = active_plan_items.map(&:id).sort
    current_index = sorted_ids.index(params[:plan_item_id].to_i) || 0
    next_id = sorted_ids[current_index + 1] || 0
    last_id = sorted_ids[current_index - 1]
    last_id = 0 if current_index == 0

    detail_hash = {
      header: {
        bgColor: 'blue'
      },
      supplier: {
        name: supplier.name
      },
      productFamily: {
        id: product_family.id,
        name: product_family.name,
        web_url: NativeUrl.generate('control_plan/new-product', current_user, product_id: product_family.id)
      },
      controlPlan: {
        id: control_plan.id
      },
      planItem: plan_item.to_h,
      processStep: process_step_hash,
      images: images,
      active_plan_items: active_plan_items.map { |x| x.to_h },
      active_process_steps: active_process_steps,
      next_id: next_id,
      last_id: last_id
    }
    @response[:t].merge!(I18n.t('common'))
    @response[:t].merge!(I18n.t('api.v1.control_plan.step_detail.step_detail_card'))
    @response.merge!(detail_hash)
    @response
  end

  private

  def cp_floating_home
    @response.merge!({ floating: { icon: 'home', bindtap: 'home',
                                   url: '/members/control_plan/index' } })
  end
end
