class PagePrepper::CapPage
  attr_accessor :params, :current_user, :app

  OFFLINE_PAGES = %w[cap_index cap_form cap_followup]

  def initialize(current_user, params, ref, response, app)
    @current_user = current_user
    @params = params
    @ref = ref
    @app = app
    @init_response = response.dup
  end

  def render(page)
    @response = @init_response.dup
    @response[:t] = I18n.t("api.v1.custom_data.#{page}")
    raise StandardError, 'PAGE NOT VALID' unless OFFLINE_PAGES.include?(page)

    public_send(page.to_sym)
  end

  def cap_form
    ref = @ref
    points = ref.cap_points.map { |x| x.standard_hash }
    i = (params[:index] || '0').to_i
    @response.merge!({ points: points, active_index: i, active_point: points[i], qa_id: params[:id] })
    @response
  end

  def cap_index
    ref = @ref
    web_url = NativeUrl.generate('cap/followup_invite', current_user, qa_id: params[:id])
    cap = ref.cap
    cap_points = cap.cap_points.includes(:cap_followup, :cap_resolutions).order(:id)
    points = cap_points.map do |x|
      h = x.serializable_hash
      h.symbolize_keys!
      h[:resolutions] = x.cap_resolutions
      h[:followup] = x.cap_followup
      h
    end
    can_schedule = false
    if params[:followup] == 'true'
      resolved = cap_points.select { |x| x.cap_followup.present? && x.cap_followup.resolved }
      perc = resolved.count * 100 / cap_points.length
    else
      resolved = cap_points.select { |x| x.cap_resolutions.present? }
      perc = resolved.count * 100 / cap_points.length
      can_schedule = true if perc == 100
    end

    @response.merge!({ points: points, cap: cap.basic_h, perc: perc, can_schedule: can_schedule, qa_id: params[:id],
                       web_url: web_url })
    @response
  end

  def cap_followup
    ref = @ref
    cap_followup = ref.cap_points[params[:index].to_i].cap_followup
    return {} if cap_followup.nil?

    cap_point = cap_followup.cap_point
    resolutions = cap_point.cap_resolutions

    date = cap_followup.followup_date.strftime('%Y-%m-%d')
    ass = cap_point.cap.assessment
    url = ass.mp_path(:review)
    point_url = "/members/assessment/historical-level?id=#{cap_point.ref_id}"

    hashy = {
      date: date,
      resolutions: resolutions.map(&:standard_hash),
      point: cap_point.to_h,
      followup: cap_followup.to_h,
      disable_editing: ref.primary_assessor != current_user
    }

    @response.merge!(hashy)
    @response.merge!({ point_url: point_url, cap_url: url, qa_id: params[:id] }) if ass.is_a?(QualityAssessment)
    @response[:t].merge!(I18n.t('api.v1.custom_data.cap_form'))

    @response
  end
end
