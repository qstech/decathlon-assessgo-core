# Pseudo Requests for Offline Rendering
class PagePrepper::AssessmentPage
  attr_accessor :params, :current_user, :qa, :app

  OFFLINE_PAGES = %w[index pre_confirmation assessment_prep go_for_assessment
                     chapter_assessment single_chapter overview].freeze

  def initialize(current_user, params, qa, response, app)
    @current_user = current_user
    @params = params
    @qa = qa
    @app = app
    @init_response = response.dup
  end

  def render(page)
    @response = @init_response.dup
    @response[:t] = I18n.t("api.v1.quality_assessments.#{page}")
    raise StandardError "PAGE NOT VALID: #{page}" unless OFFLINE_PAGES.include?(page)

    public_send(page.to_sym)
  end

  def index
    # using params ref model to pass assessment type (ex. QA, OPEX) because it's allowed
    grid = 'quality_assessment'
    grid = params[:ref_model] if params[:ref_model].present?
    puts "GRID IS: #{grid}"

    @user = params[:id].present? ? User.find(params[:id]) : current_user

    index_hash = {
      header: {
        bgColor: @qa.bg_color,
        tableLogo: @qa.logo,
        tableDesc: @qa.motto,
        buttonText: 'New Assessment ➜',
        toPage: 'newQa',
        url: '/members/assessment/new'
      },
      self: true,
      invited: false,
      invitation: {
        description: "You have been invited to help with a #{@qa.short_name} Assessment",
        # "You have an invitation!",
        buttonDesc: 'Go to invitations ➜',
        iconType: 'info',
        bindtap: 'goInvitations',
        url: "/members/assessment/invitations?grid=#{grid}"
      }
    }

    total_set = [PageComponent.future_qa_card(@qa)]
    index_hash[:gemba_qacards] = total_set.select do |x|
      !['Annual', 'Self Assessment Training', 'CAP Close'].include?(x[:qaType][:desc])
    end
    index_hash[:annual_qacards] = total_set.select do |x|
      ['Annual', 'Self Assessment Training', 'CAP Close'].include?(x[:qaType][:desc])
    end

    @response.merge!(index_hash)
    @response.merge!({ short_name: qa.short_name })

    @response
  end

  def pre_confirmation
    qa = @qa
    supplier = qa.supplier_profile
    assessor = qa.assessor
    ass_name = qa.assessor_name
    puts 'OH WOW'
    active_links = PageComponent.qa_active_links(qa, current_user, @review == true)
    assessor_header = 'OFFLINE MODE'
    result = qa.completed? ? qa.result : ''
    cotation = qa.completed? ? qa.cotation : ''
    days_left = (qa.assess_date.to_date - Time.now.to_date).to_i
    day_form_target = AssessmentDayForm.find_by(quality_assessment_id: qa.id).supplier_target
    trans = 'api.v1.quality_assessments.pre_confirmation'
    extra_supp_info = {
      completed: qa.completed?, day_form_target: day_form_target,
      target: day_form_target || supplier.target || 'N/A'
    }

    hashy = {
      completed: qa.completed?,
      result: result,
      resultColor: PageComponent::EXTRA_CLASSES[result] || 'black',
      cotation: cotation,
      prep_completed: qa.prep_completed,
      assessor_confirmed: qa.assessor_confirmed,
      assess_today: qa.assess_today?,
      annual_assessment: qa.assess_type == 'Annual',
      preloading: {
        type: 'Boolean',
        value: 'false'
      },
      supplier: supplier.to_h.merge(extra_supp_info),
      header: {
        supplier: supplier.name,
        assessor: assessor_header,
        type: qa.assess_type,
        color: PageComponent::EXTRA_CLASSES[qa.status],
        projectDate: {
          month: Date::ABBR_MONTHNAMES[qa.assess_date.month],
          day: qa.assess_date.day
        },
        status: qa.status
      },
      active_links: active_links,
      gradientNotice: {
        in: days_left,
        in_label: I18n.t("#{trans}.days_count", count: days_left),
        left: days_left - 7,
        left_label: I18n.t("#{trans}.days_count", count: days_left - 7)
      },
      qa: qa.to_h,
      generate_cap: qa.generate_cap?
    }

    hashy[:supplier][:qa_id] = qa.id

    # to overwrite supplier's to_h method
    last_qa = if qa.assess_type == 'CAP Close' || qa.assess_type == 'CAP Review'
                qa.cap.nil? ? nil : qa.cap.ref
              else
                supplier.quality_assessments.where(completed: true, assess_type: qa.assess_type,
                                                   grid: qa.grid).order(assess_date: :desc).first
              end

    if last_qa.nil?
      hashy[:supplier][:lastAssessed] = 'N/A'
      hashy[:supplier][:lastResult] = 'N/A'
    else
      hashy[:supplier][:lastAssessed] = (last_qa.assess_date.strftime('%Y-%m-%d') || 'N/A')
      hashy[:supplier][:lastResult] = (last_qa.result || 'N/A')
      hashy[:supplier][:resultColor] = (last_qa.level_color[last_qa.result] || 'black')
    end

    with_floating_home
    @response.merge!(hashy)
    @response
  end

  def go_for_assessment
    qa = @qa
    adf = qa.assessment_day_form
    disable_editing = qa.official_assessment? && current_user.assessor_profile.blank?
    adapterSource = qa.adapter_source
    supplier = qa.supplier_profile.to_h(grid: qa.grid)
    supplier[:day_form_target] = adf.supplier_target

    @response.merge!({
                       supplier: supplier,
                       qaType: qa.official_assessment?,
                       type: qa.assess_type,
                       grid: qa.grid,
                       day_form: adf.form,
                       adapterSource: adapterSource,
                       inputValue: '',
                       bindSource: [],
                       selection: AssessmentDayForm::PL_PRESENT_CHOICES,
                       targetOptions: AssessmentDayForm::TARGET_CHOICES,
                       disable_editing: disable_editing
                     })

    @response.merge!({ list: adf.responses })
    @response
  end

  def chapter_assessment
    qa = @qa
    chapters = PageComponent.qa_chapter_set(qa)
    supplier = qa.supplier_profile

    res = {
      short_name: qa.short_name,
      assess_type: qa.assess_type,
      color: qa.color,
      chapters: chapters,
      supplier: supplier.name,
      perc: qa.chapter_percent
    }

    res[:perc] += 1 if res[:perc] == 99
    @response.merge!(res)
    @response
  end

  def single_chapter
    puts 'START'
    @level = params[:level].nil? ? 1 : params[:level].to_i
    ch   = ChapterAssessment.find_by(id: params[:id])
    qa   = @qa
    chs  = ch.siblings.with_questions
    cqs  = ChapterQuestion.with_images.with_likes.where(chapter_assessment_id: chs.map(&:id))
    tags = cqs.map(&:chapter_tag)
    last_attempt = qa.last_attempt(tags)
    images = cqs.map { |cq| [cq.id, cq.img_h] }.to_h
    puts 'SET VARS'
    if qa.dpr?
      gl = DprGuideline.find_by(guideline_tag: ch.chapter_tag, grid: 'dpr_textile')
      cq = cqs.first
      gl_h = gl.to_h.merge(cq.single_chapter_form(current_user, with_comments: true))
      response_set = [gl_h]
      active_level = gl
    else
      gl = Guideline.with_grandchildren.find_by(grid_version_id: qa.grid_version_id, guideline_tag: ch.section)
      response_set = gl.children.sort_by(&:guideline_tag)
                       .select { |x| chs.map(&:chapter_tag).include?(x.guideline_tag) }
                       .map do |lev|
        vars = {
          chs: chs, cqs: cqs, gl: gl, qa: qa, last_attempt: last_attempt,
          user: current_user, native: true, china: false, tags: tags
        }
        lev.single_chapter_form(vars)
      end
      active_level = gl.children.find{|x| x.guideline_tag == ch.chapter_tag}
    end
    selection = active_level.response_set
    puts 'SINGLE CHAPTER FORM END'

    # if (qa.grid == "hrp_assessment") && (@level > 0)
    #   selection << {name: "NOK without risk", value: "NOK without risk"}
    #   selection.sort_by!{|x| x[:name]}.reverse!.uniq!
    # end

    disable_editing = (params[:review].present? || (qa.official_assessment? && !current_user.can_edit?(qa)))

    hashy = { disable_editing: disable_editing, grid: qa.grid,
              assess_type: qa.assess_type, levels: response_set, selection: selection,
              qa_id: ch.quality_assessment_id, images: images, completed: qa.completed,
              score: qa.section_score(guideline: gl.to_h(with_children: true), chapters: chs), can_bookmark: current_user.decathlon?,
              active_level: response_set[@level - 1], next_offline: '', last_offline: '' }
    puts 'HASH SETUP'
    unless qa.dpr?
      ai = response_set.index(hashy[:active_level]) || (response_set.length - 1)
      next_i = ai + 1
      hashy[:next_offline] = offline_link(response_set[next_i][:lev_obj]) if next_i < (response_set.length - 1)
      hashy[:last_offline] = offline_link(response_set[ai - 1][:lev_obj]) if ai > 0
    end
    puts 'RESOLVED'
    @response.merge!(hashy)
    clean_response!
    @response
  end

  def overview
    @response.merge!({ assessment: @qa.serializable_hash })
    @response
  end

  def assessment_prep
    respondant = params[:respondant] || 'assessor'
    fq = @qa.prep_questions_form(respondant)

    @response.merge!(fq)
    @response.merge!({ short_name: @qa.short_name, assess_type: @qa.assess_type })
    clean_response!

    @response
  end

  private

  def with_floating_home
    @response.merge!({ floating: { icon: 'home', bindtap: 'home',
                                   url: '/members/assessment/index' } })
  end

  def chapter_assessment_ref(chapters)
    ref = @qa.cap.ref
    ref_chapters = PageComponent.qa_chapter_set(ref)

    ref_chapters.each do |chapter|
      chapter[:children].each do |x|
        x[:disable_editing] = true
        x[:cap_id] = @qa.cap.id
        x[:cap_qa_id] = @qa.id
        x[:annual_chapter] = true
      end
    end

    chapters.each_with_index do |chapter, idx|
      # Re-assigning ref_chapter's chapter to CAP's chapter if present
      chapter[:children].each do |sec|
        ref_chapters[idx][:children].each_with_index do |x, i|
          ref_chapters[idx][:children][i] = sec if sec[:guideline_tag] == x[:guideline_tag]
        end
      end
    end
    ref_chapters
  end

  def clean_response!
    @response = clean(@response)
  end

  def clean(v)
    if v.nil?
      ''
    elsif v.is_a? Array
      v.map { |x| clean(x) }
    elsif v.is_a? Hash
      v.each { |k, y| v[k] = clean(y) }
      v
    else
      v
    end
  end

  def offline_link(obj)
    "members/assessment/single-chapter?id=#{obj.id}&section=#{params[:section]}&level=#{obj.level}"
  end
end
