class MailBag < ApplicationRecord
  has_rich_text :body
  # TODO: Implement Custom Audience.

  TRIGGERS = [
    ['No Trigger (Only Manual Send)', 'manual'],
    ['5 Days After User Created (w/ Login)', 'user_followup_5_login'],
    ['5 Days After User Created (w/ No Login)', 'user_followup_5_no_login'],
    ['After Login (Daily)', 'user_after_login'],
    ['Comment Posted (Goes)', 'comment_create_goes'], # DONE
    ['Comment Posted (Chapter Review)', 'comment_create_chapter'], # DONE
    ['Control Plan Scheduled', 'control_plan_create'], # DONE
    ['Control Plan Completed', 'control_plan_completed'], # DONE
    ['Invitation Sent', 'invitation_sent'], # DONE
    ['Invitation Accepted', 'invitation_accepted'],  # DONE
    ['Invitation Rejected', 'invitation_rejected'],  # DONE
    ['Assessment Created', 'assessment_create'], # DONE
    ['Assessment Completed', 'assessment_completed'], # DONE
    ['Assessment Scheduled Reminder', 'assessment_reminder'], # NEED SCHEDULE
    ['Assessment Deleted', 'assessment_destroyed'], # DONE
    ['Bug Report Created', 'bug_report_create'], # DONE
    ['Bug Report Resolved', 'bug_report_completed'], # DONE
    ['CAP Followup Scheduled', 'cap_followup_create'], # DONE
    ['CAP Followup Reminder', 'cap_followup_reminder'], # NEED SCHEDULE
    ['Custom KPI Recorded', 'kpi_record_create'], # DONE
    ['Product Family Created', 'product_family_create'], # DONE
    ['Supplier Email Changed', 'supplier_email_changed'],
    ['Supplier Edited', 'supplier_update'], # DONE
    ['Supplier Gemba Creation', 'supplier_gemba_creation'],
    ['Assessor Application Submitted', 'assessor_application_create'], # DONE
    ['Assessor Application Withdrawn', 'assessor_application_destroyed'], # DONE
    ['Assessor Application Updated', 'assessor_application_update'], # DONE
    ['Assessor Application Validated', 'assessor_application_completed'], # DONE
    ['Custom Developed Trigger', 'custom_trigger']
  ]

  AUDIENCES = {
    'assessment' => %i[assessor creator supplier supplier_pls
                       supplier_opms supplier_ptms candidate referent admins],
    'control_plan' => %i[creator supplier supplier_pls supplier_opms
                         supplier_ptms admins],
    'invitation' => %i[sender receiver supplier supplier_pls supplier_opms
                       supplier_ptms admins],
    'comment' => %i[sender receiver supplier admins],
    'bug_report' => %i[sender admins tech_team],
    'cap_followup' => %i[assessor creator supplier supplier_pls
                         supplier_opms supplier_ptms admins],
    'kpi' => %i[supplier supplier_pls supplier_opms supplier_ptms admins],
    'product_family' => %i[creator supplier supplier_pls supplier_opms
                           supplier_ptms admins],
    'supplier' => %i[contact_email secondary_users supplier_pls supplier_opms
                     supplier_ptms admins],
    'assessor_application' => %i[applicant opm team_leader validator admins],
    'user' => %i[user admins],
    'manual' => [:manual],
    'custom' => [:custom]
  }

  AUDIENCE_LABELS = {
    assessor: 'Assessor', creator: 'Creator', supplier: 'Supplier (primary contact)',
    supplier_pls: "Supplier PL's", supplier_opms: "Supplier OPM's",
    supplier_ptms: "Supplier PTM's", candidate: 'Candidate', referent: 'Referent',
    admins: 'Admins (kkang20, hyu50)', sender: 'Sender', receiver: 'Receiver',
    tech_team: 'Tech Team', contact_email: 'Supplier (primary contact)',
    secondary_users: 'Supplier (secondary users)', applicant: 'Assessor Applicant',
    opm: 'OPM', team_leader: 'Team Leader', validator: 'Validator (Referent)',
    user: 'User',
    manual: 'Manual Only (no automatic audience)',
    custom: 'Custom Logic. Cannot Edit.'
  }

  def audience_options
    return ['Must Select Trigger First'] if trigger.blank?

    chosen = AUDIENCES.find { |k, _v| trigger.include?(k) }
    chosen = chosen[1]
    chosen.map { |x| [AUDIENCE_LABELS[x], x] }
  end

  def trigger_name
    return 'NOT SET' if trigger.blank?

    TRIGGERS.find { |x| x[1] == trigger }[0]
  end

  # Models triggered:  comment, control plan, invitation, qa,
  # Needs to have triggers => Events that cause action
  # Needs to know who to send it to.
  # Needs to know what text to replace.

  def dispatch_for_audience(obj, replacements = {})
    obj.audience_for(audience).each do |user|
      replacements[:name] = user.name
      replacements[:email] = user.email
      dispatch_to_user(user, replacements)
    end
  end

  def dispatch_to_user(user, replacements = {})
    # 1) get the user(s) for the audience
    letter = Letter.create(to: user.id, mail_bag_id: id,
                           subject: personalize(subject, replacements),
                           body: personalize(body, replacements),
                           cc: cc, bcc: bcc)

    letter.deliver!
    # 3) each letter .deliver!
  end

  def personalize(content, replacements = {})
    new_body = content.to_s
    replacements.each do |k, v|
      v = v.strftime('%Y-%m-%d') if v.is_a? ActiveSupport::TimeWithZone
      new_body = new_body.gsub("{#{k}}".upcase, v.to_s)
    end
    new_body
  end

  # TRIGGERS
  # 1) Time/Action + <receiver/audience>
  #   - trigger = :cp_update
  #   - audience = supplier, creator, assessor,
  # What are our triggers?
  # OnCreate, On Complete, On Update(throttle: 1/day)

  class << self
    def trigger!(some_trigger, obj)
      # TODO: finish mailer seed before activating.
      MailBag.where(trigger: some_trigger).each do |mailbag|
        mailbag.dispatch_for_audience(obj)
      end
    end

    def trigger_by_tag!(tag, user, replacements = {})
      m = MailBag.find_by_tag(tag)
      m.dispatch_to_user(user, replacements) if m
      # rescue
      #   true
    end
  end
end
