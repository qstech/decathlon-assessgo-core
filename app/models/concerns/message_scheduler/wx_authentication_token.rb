module MessageScheduler
  module WxAuthenticationToken
    def notify_new_set
      ['login_notification']
    end

    # def notify_update_set
    #   ["logout_notification"]
    # end

    def login_notification
      recipients = User.where(id: [user_id]).where('login_notified_at < ?', 1.days.ago)

      recipients.map do |x|
        openid = x.oa_openid unless x.oa_openid.nil?
        next({}) if openid.nil?

        note_params = {
          openid: openid,
          username: x.username,
          timestamp: created_at.strftime('%c')
        }
        wx_params = WxNotifier.login_success(note_params)
        { email_params: {}, wx_params: wx_params }
      end
    end

    def logout_notification
      recipients = User.where(id: [user_id])

      recipients.map do |x|
        openid = x.oa_openid unless x.oa_openid.nil?
        next({}) if openid.nil?

        note_params = {
          openid: openid,
          username: x.username,
          timestamp: x.created_at.strftime('%c')
        }
        wx_params = WxNotifier.logout_success(note_params)
        { email_params: {}, wx_params: wx_params }
      end
    end
  end
end
