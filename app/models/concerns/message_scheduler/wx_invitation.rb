module MessageScheduler
  module WxInvitation
    def notify_update_set
      %w[invitation_accepted invitation_rejected]
    end

    def notify_new_set
      ['invitation_sent']
    end

    def mailbag_params
      ass = ref
      ass.mailbag_params
    end

    def invitation_accepted
      if accepted_changed? && accepted
        recipients = users_concerned
        assessment = ref
        assessor = recipient
        assessor_name = assessor.profile.name || assessor.username

        recipients.map do |x|
          MailBag.trigger_by_tag!('invitation_accepted', x, mailbag_params)
          openid = x.oa_openid unless x.oa_openid.nil?
          # next if openid.nil?

          note_params = {
            openid: openid,
            redirect: assessment.mp_path(:assess),
            assessor_name: assessor_name,
            accepted_at: updated_at.strftime('%Y-%m-%d')
          }

          # WxNotifier.invite_accepted(note_params)
          params = { email_params: note_params.merge({ user_id: x.id, email: x.email }) }
          params.merge!({ wx_params: WxNotifier.invite_accepted(note_params) }) if openid.present?

          params
        end
      end
    end

    def invitation_rejected
      if rejected_changed? && rejected
        recipients = users_concerned
        assessment = ref
        assessor = recipient
        assessor_name = assessor.profile.name || assessor.username

        recipients.map do |x|
          MailBag.trigger_by_tag!('invitation_rejected', x, mailbag_params)
          openid = x.oa_openid unless x.oa_openid.nil?
          # next if openid.nil?

          if x == assessor
            redirect = 'members/home/home'
            header = 'You have declined an invitation to assess.'
          else
            redirect = assessment.mp_path(:assess)
            header = 'Assessor is unable to assess. Try sending another request?'
          end

          note_params = {
            openid: openid,
            redirect: redirect, # where does this supposed to redirect to?
            assessor_name: assessor_name,
            rejected_at: updated_at.strftime('%Y-%m-%d'),
            reason: 'Assessor unable to assess at this time.'
          }

          # WxNotifier.invite_rejected(note_params)
          params = { email_params: note_params.merge({ user_id: x.id, email: x.email }) }
          params.merge!({ wx_params: WxNotifier.invite_rejected(note_params) }) if openid.present?

          params
        end
      end
    end

    def invitation_sent
      # TODO:  can be CP or QA, should send a link to the HOME PAGE of relavent
      # assessment type or control plan
      assessment = ref
      supp = assessment.supplier_profile
      recipients = users_concerned

      if ref_model == 'quality_assessment'
        title = "Invitation Received for a #{assessment.short_name} Assessment for #{supp.code} #{supp.name}"
        creator = assessment.user.profile.name
      elsif ref_model == 'control_plan'
        title = "Invitation Received for a Control Plan Montioring for #{supp.code} #{supp.name}"
        creator = assessment.assessor_profile.name
      end

      recipients.map do |x|
        MailBag.trigger_by_tag!('invitation_sent', x, mailbag_params)

        openid = x.oa_openid unless x.oa_openid.nil?
        # next if openid.nil?

        note_params = {
          openid: openid,
          redirect: "members/assessment/invitations?grid=#{assessment.grid}",
          header: 'Invitation Received!',
          title: title,
          creator: creator,
          assess_date: assessment.project_date
        }

        WxNotifier.scheduled_event(note_params)
        params = { email_params: note_params.merge({ user_id: x.id, email: x.email }) }
        params.merge!({ wx_params: WxNotifier.scheduled_event(note_params) }) if openid.present?

        params
      end
    end
  end
end
