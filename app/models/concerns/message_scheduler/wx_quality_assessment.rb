module MessageScheduler
  module WxQualityAssessment
    def notify_new_set
      %w[scheduled_event opmsd_notify]
    end

    def notify_update_set
      %w[assessment_result future_reminders] # add the timed notif
    end

    def mailbag_params
      { grid: short_name, assess_type: assess_type, assess_date: assess_date,
        supplier: supplier_profile.name, supplier_code: supplier_profile.code }
    end

    def future_reminders
      grid = self.grid
      assess_type = self.assess_type

      res = result
      puts "assessment result ===>>>> #{res}"

      supp = supplier_profile
      supp_pl_users = AssessorProfile.where(id: (supp.pl_set + supp.ptm_set + supp.opm_set).uniq).map(&:user_id)

      recipients = (users_concerned.map(&:id) + supp_pl_users).uniq

      supplier_type = if supp.partner
                        'partner'
                      elsif supp.kas
                        'kas'
                      else
                        'other'
                      end

      schedules = NextAssessmentSchedule.where(grid: grid, after_assess_type: assess_type,
                                               supplier_type: [supplier_type, 'all'], destroyed_at: nil)

      if completed_changed? && completed && schedules.present?
        params = []
        schedules.each do |sched|
          # check result_condition
          puts "sched result ====>>> #{sched.result_condition}"
          next if sched.result_condition.present? && !res.in?(sched.result_condition.split(','))
          next if supp.country_name.in?(sched.countries['except'])
          next unless sched.countries['only'].blank? || supp.country_name.in?(sched.countries['only'])

          # parse and set the reminder times
          next_assessment = assess_date + sched.next_assessment.send(sched.time_unit)

          reminder_times = sched.notify_in.reject { |x| x == 'endless-complete' }
                                .map { |x| x.split('-') }.map { |y| next_assessment - y[0].to_i.send(y[1]) }

          # put it in the log!!
          logs = reminder_times.map do |notify_time|
            na = NextAssessmentLog.create!(
              ref_id: id,
              ref_grid: grid,
              ref_assess_type: assess_type,
              next_assess_type: sched.assess_type,
              ref_assess_date: assess_date,
              next_assess_date: next_assessment,
              notify_time: notify_time,
              recipients: recipients
            )

            na
          end
          params << { timed_params: logs }
        end
        params.reject(&:nil?)
      end
    end

    def next_assessment_reminder(next_assess_type)
      # set logic which reminder to send
      # will hard code the `notify_what` part for now
      supp = supplier_profile
      puts "assess_type ==>> #{assess_type}"
      supp_future_qas = supplier_profile.quality_assessments
                                        .where(grid: grid, assess_type: next_assess_type, destroyed_at: nil)
                                        .where('assess_date > ?', assess_date)

      future_qa = supp_future_qas.first

      # check if assessment is created
      if future_qa.nil?
        # set note_params
        template = { template: 'assessment_not_scheduled' }

      # if created check the other 3 conditions
      else
        template = { future_qa_id: future_qa.id }

        # check if assessor is assigned
        if future_qa.completed
          template.merge!({ template: 'completed' })
        elsif future_qa.assessor_assignments.blank?
          template.merge!({ template: 'assessor_not_assigned' })
        elsif future_qa.prep_status
          template.merge!({ template: 'prep_questions_not_finished' })
        else
          template.merge!({ template: 'assessment_is_approaching' })
        end
      end

      puts "template inside next_assessment_reminder ==> #{template}"
      template
    end

    # who are we sending it to? Creator? Supplier?
    def assessment_result
      if completed_changed? # triggered at update_status ?
        title = "#{supplier_code} - #{short_name} #{assess_type}"
        ids = assessed_by + people_concerned
        recipients = users_concerned

        recipients.map do |x|
          MailBag.trigger_by_tag!('assessment_result', x, mailbag_params)

          openid = x.oa_openid unless x.oa_openid.nil?
          # next({}) if openid.nil?

          note_params = {
            openid: openid,
            redirect: mp_path(:review),
            title: title,
            assess_date: updated_at.strftime('%Y-%m-%d'),
            result: result
          }
          # WxNotifier.assessment_result(note_params)
          params = { email_params: {} }
          params.merge!({ wx_params: WxNotifier.assessment_result(note_params) }) if openid.present?

          params
        end
      end
    end

    def scheduled_event
      supp = supplier_profile
      title = "#{short_name} #{assess_type} Assessment Scheduled for #{supp.code} #{supp.name}"
      recipients = users_concerned

      recipients.map do |x|
        MailBag.trigger_by_tag!('scheduled_event', x, mailbag_params)
        openid = x.oa_openid unless x.oa_openid.nil?
        # next({}) if openid.nil?

        note_params = {
          openid: openid,
          header: 'New Assessment Scheduled!',
          redirect: mp_path(:assess),
          title: title,
          assess_date: assess_date.strftime('%Y-%m-%d'),
          creator: user.profile.name
        }
        # WxNotifier.scheduled_event(note_params)
        params = { email_params: {} }
        params.merge!({ wx_params: WxNotifier.scheduled_event(note_params) }) if openid.present?

        params
      end
    end

    def opmsd_notify
      return nil unless opmsd.present?

      supp = supplier_profile
      title = "#{short_name} #{assess_type} Assessment Scheduled for #{supp.code} #{supp.name}"
      recipients = User.includes(:assessor_profile).where(assessor_profiles: { id: opmsd })

      recipients.map do |x|
        MailBag.trigger_by_tag!('opmsd_notify', x, mailbag_params)
        openid = x.oa_openid unless x.oa_openid.nil?
        # next({}) if openid.nil?

        note_params = {
          openid: openid,
          header: 'ATTN: OPMSD, New Assessment Scheduled!',
          redirect: mp_path(:assess),
          title: title,
          assess_date: assess_date.strftime('%Y-%m-%d'),
          creator: user.profile.name
        }
        # WxNotifier.scheduled_event(note_params)
        params = { email_params: {} }
        params.merge!({ wx_params: WxNotifier.scheduled_event(note_params) }) if openid.present?

        params
      end
    end
  end
end
