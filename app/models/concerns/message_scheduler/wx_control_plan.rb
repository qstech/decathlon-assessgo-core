module MessageScheduler
  module WxControlPlan
    def notify_new_set
      ['scheduled_event']
    end

    def notify_update_set
      ['assessment_result']
    end

    # who are we sending it to? Creator? Supplier?
    def assessment_result
      if completed # triggered at update_status ?
        title = "#{product_family.name} - Control Plan"
        recipients = users_concerned

        recipients.map do |x|
          openid = x.oa_openid unless x.oa_openid.nil?
          # next if openid.nil?
          MailBag.trigger_by_tag!('control_plan_completed', x, {
                                    product_name: product_family.name,
                                    result: nonconformity_rate.to_s,
                                    qr: mp_path(:review)
                                  })

          note_params = {
            openid: openid,
            redirect: mp_path(:review),
            title: title,
            assess_date: updated_at.strftime('%Y-%m-%d'),
            result: nonconformity_rate
          }
          # WxNotifier.assessment_result(note_params)
          params = { email_params: {} }
          params.merge!({ wx_params: WxNotifier.assessment_result(note_params) }) if openid.present?

          params
        end
      end
    end

    def scheduled_event
      title = "Control Plan Monitoring Scheduled for #{product_family.name}"
      recipients = users_concerned

      recipients.map do |x|
        MailBag.trigger_by_tag!('control_plan_create', x, {
                                  product_name: product_family.name,
                                  qr: mp_path(:assess)
                                })
        openid = x.oa_openid unless x.oa_openid.nil?
        # next if openid.nil?

        note_params = {
          openid: openid,
          redirect: mp_path(:assess),
          header: 'New Control Plan Scheduled!',
          title: title,
          assess_date: assess_date.strftime('%Y-%m-%d'),
          creator: assessor_profile.name
        }

        wx_params = WxNotifier.scheduled_event(note_params)

        params = { email_params: note_params.merge({ user_id: x.id, email: x.email }) }
        params.merge!({ wx_params: wx_params }) if openid.present?

        params
      end
    end
  end
end
