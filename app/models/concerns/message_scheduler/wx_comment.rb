module MessageScheduler
  module WxComment
    def notify_update_set
      []
    end

    def notify_new_set
      ['comment_posted']
    end

    def comment_posted
      ref = self.ref
      recipients = users_concerned
      creator = user.profile.name || user.username

      if ref_model == 'goes_activity'
        # ref is @goes_activity
        # qa is ref.ref
        ass = ref.ref
        chapter_tag = ''
        title_tag = ref.ref_model == 'quality_assessment' ? "#{ass.short_name} (#{ass.assess_type})" : 'Control Plan'
        supp = ass.supplier_profile
        title = "New comment posted for #{title_tag} - #{supp.name} (#{supp.code})"

        redirect = "members/activity/show?id=#{ref.id}"
        trigger = 'comment_create_goes'

      elsif ref_model == 'chapter_assessment'
        ass = ref.quality_assessment
        title_tag = "#{ass.short_name} (#{ass.assess_type})"
        chapter_tag = "#{ref.chapter_tag}-#{ref.level}"
        supp = ass.supplier_profile
        title = "New comment posted for #{title_tag} - #{chapter_tag}  - #{supp.name} (#{supp.code})"

        redirect = "members/assessment/single-chapter-review?section=#{ref.section}&level=#{ref.level}&id=#{ref.id}"
        trigger = 'comment_create_goes'
      end
      puts recipients
      recipients.map do |x|
        puts "recipient usr id ====>>>> #{x.id}"
        openid = x.oa_openid unless x.oa_openid.nil?
        # next if openid.nil?
        MailBag.trigger_by_tag!(trigger, x, {
                                  grid: ass.short_name,
                                  assess_type: ass.assess_type,
                                  chapter_tag: chapter_tag,
                                  supplier: supp.name,
                                  supplier_code: supp.code,
                                  qr: redirect
                                })

        note_params = {
          openid: openid,
          redirect: redirect,
          header: 'New Comment Posted!',
          title: title,
          creator: creator,
          content: content,
          assessment: "#{title_tag} - #{ass.assess_date.strftime('%Y-%m-%d')} - #{supp.name} (#{supp.code})",
          comment_date: created_at.strftime('%Y-%m-%d')
        }

        params = { email_params: {} }
        params.merge!({ wx_params: WxNotifier.comment_posted(note_params) }) if openid.present?

        params
      end
    end
  end
end
