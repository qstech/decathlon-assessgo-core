module MessageScheduler
  extend ActiveSupport::Concern

  def self.included(base)
    super

    case base.name
    when 'ControlPlan'
      base.send :include, WxControlPlan
    when 'Invitation'
      base.send :include, WxInvitation
    when 'QualityAssessment'
      base.send :include, WxQualityAssessment
    when 'User'
      base.send :include, WxUser
    when 'AuthenticationToken'
      base.send :include, WxAuthenticationToken
    when 'Comment'
      base.send :include, WxComment
    end
    base.send :after_create, :notify_new
    base.send :before_update, :notify_update
  end

  # INSTANCE METHODS HERE
  def notify!(tag)
    # string of class name, e.g. user, quality_assessment
    class_name = model_name.singular

    # Fetch list of Notifications to prepare
    wx_notes = send("notify_#{tag}_set") # i.e.: ["comment_posted"]
    wx_notes.each do |x|
      # i.e: x = comment_posted
      recipients = send(x) || []
      recipients.each do |params|
        # puts "combined email + wx params =====>>>>> #{params}"

        obj_hash = { id: id, model: model_name.name }
        wx_params = params[:wx_params]
        email_params = params[:email_params]
        timed_params = params[:timed_params]

        # timed = wx_params.present? ? wx_params[:deliver_at] : nil
        timed = timed_params.present?

        if timed
          timed_params.each do |t|
            notify_time = t.notify_time
            notify_time = 5.days.from_now if notify_time < Time.now
            ScheduledNotifWorker.perform_at(notify_time, obj_hash, t.next_assess_type, t.id)
          end
        else
          WechatWorker.perform_async(obj_hash, params)
        end
      end
    end
    true
  end

  def notify_new
    notify!('new')
  end

  def notify_update
    notify!('update')
  end

  def notify_new_set
    # OVERWRITTEN BY SUB_CLASSES
    []
  end

  def notify_update_set
    # OVERWRITTEN BY SUB_CLASSES
    []
  end
end
