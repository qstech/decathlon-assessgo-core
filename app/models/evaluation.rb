class Evaluation < ApplicationRecord
  belongs_to :guideline
  LIMITS_ORDER = %w[A B C D E].freeze
  # When seeding:
  # - limit is the first number that will not pass the result
  #  i.e. limit_a: 80 means that the result must be > 80% in order to qualify for A
  # - leave limit nil if you  want to skip in calculation
  #  i.e if you want the following logic: "A: > 90; C: from 89 to 60; E: <= 59" limit_b and limit_d should be nil
  # lowest_limit is set when you don't want to use all the limits upto E
  # i.e. if you want to have the following logic
  def to_a
    arr = ["#{limits.first[0]}: > #{limits.first[1]}%"]
    final_limit = limits.first[1] - 1

    limits.each_with_index do |x, i|
      next if i.zero?

      upper_limit = limits[i - 1][1] - 1
      arr << "#{x[0]}: #{upper_limit} to #{x[1]}%"
      final_limit = x[1] - 1
    end
    arr << "#{lowest_limit}: <= #{final_limit}%"
  end

  def limits
    attributes.select { |k, v| k.first(5) == "limit" && v.present? }
    .sort_by { |k, _v| k }
    .map { |x| [x[0].gsub('limit_', '').capitalize, x[1]] }
  end
end
