class Form < ApplicationRecord
  has_many :form_sections
  has_many :form_questions, through: :form_sections
  has_paper_trail ignore: %i[created_at updated_at]

  def to_h
    h = serial_hash
    h[:sections] = form_sections.map { |x| x.to_h } if form_sections.present?
    { form: h }
  end
end
