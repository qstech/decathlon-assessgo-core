class FormQuestion < ApplicationRecord
  translates :label
  belongs_to :form_section
  has_many :form_options, dependent: :destroy
  validates :question_tag, uniqueness: { scope: [:grid_version_id] }
  has_paper_trail ignore: %i[created_at updated_at]

  def to_h
    h = serial_hash
    h[:options] = form_options.map { |x| x.to_h } if form_options.present?
    h
  end
end
