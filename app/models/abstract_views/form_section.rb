class FormSection < ApplicationRecord
  belongs_to :form
  has_many :form_questions
  has_paper_trail ignore: %i[created_at updated_at]

  def to_h
    h = serial_hash
    h[:questions] = form_questions.map { |x| x.to_h } if form_questions.present?
    h
  end
end
