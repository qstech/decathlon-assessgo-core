class FormOption < ApplicationRecord
  belongs_to :form_question
  has_paper_trail ignore: %i[created_at updated_at]

  def to_h
    serial_hash
  end
end
