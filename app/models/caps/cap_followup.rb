class CapFollowup < ApplicationRecord
  belongs_to :cap_point
  belongs_to :cap_resolution, optional: true
  has_many :invitations, -> { where(ref_model: 'cap_followup') }, foreign_key: 'ref_id'
  after_update :update_cap_point!
  before_update :set_resolved
  after_update :trigger_mail!

  def trigger_mail!
    # Schedule cap_followup_reminder for Followup Date
  end

  def to_h
    serial_hash
  end

  def supplier_profile
    cap_resolution.cap_point.cap.supplier_profile
  end

  def project_date
    followup_date.strftime('%Y-%m-%d')
  end

  def set_resolved
    self.resolved = result == 'OK'
  end

  def update_cap_point!
    if resolved
      cap_point.update(resolved_at: Time.now)
    else
      cap_point.update(resolved_at: nil)
    end
  end
end
