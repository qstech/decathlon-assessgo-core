class CapResolution < ApplicationRecord
  belongs_to :cap_point
  has_paper_trail ignore: %i[created_at updated_at]

  # TODO: Change types
  ACTION_TYPES = %w[TYPE1 TYPE2 TYPE3]

  # def to_h
  #   h = serial_hash
  #   # h[:deadline_date] = h[:deadline_date].strftime("%Y-%m-%d") if h[:deadline_date].present?
  #   h
  # end
end
