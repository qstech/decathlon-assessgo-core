class CapPoint < ApplicationRecord
  belongs_to :cap
  has_many :cap_resolutions, -> { where(destroyed_at: nil) }
  has_one :cap_followup
  accepts_nested_attributes_for :cap_resolutions, reject_if: :all_blank, allow_destroy: true
  has_paper_trail ignore: %i[created_at updated_at]
  scope :unresolved, -> { where(resolved_at: nil) }

  def to_h
    serial_hash.merge({ cap: cap.standard_hash, ref: ref, resolutions: cap_resolutions.map { |x| x.standard_hash } })
  end

  def calc_point
    return nil if ref_model != 'chapter_question'

    # POINT => 1.2 E-1
    # pt => guideline hash for POINT
    # cq => ChapterQuestion item for POINT

    cq = ref
    pt = Guideline.find_by(guideline_tag: cq.chapter_tag, grid_version_id: cq.quality_assessment.grid_version_id).to_h
    point = base_point(pt)
    point[:data] = cq.standard_hash
    point
  end

  def base_point(pt)
    {
      section_name: pt[:section_name], title: pt[:title], content: pt[:content],
      note_header: pt[:note_header], bullets: pt[:note_bullets]
    }
  end
end
