class Cap < ApplicationRecord
  belongs_to :supplier_profile
  has_one    :quality_assessment
  has_one    :control_plan
  has_many   :cap_points, dependent: :destroy
  has_many   :cap_followups, through: :cap_points
  has_many   :cap_resolutions, through: :cap_points
  # validates :ref_model, uniqueness: {scope: [:ref_id]}
  has_paper_trail ignore: %i[created_at updated_at]
  # default_scope ->{where(destroyed_at: nil)}

  after_create :update_qa_observers

  def self.related_for(qa)
    where("caps.id IN (#{qa_tree_sql_for(qa)})")
    .where.not(id: qa.cap_id).order('caps.id')
  end

  def self.cap_tree_for(qa)
    where("caps.id IN (#{qa_tree_sql_for(qa)})").order('caps.id')
  end

  def self.parent_tree_for(qa)
    where("caps.id IN (#{qa_parent_tree_sql_for(qa)})").order('caps.id')
  end

  def self.qa_parent_tree_sql_for(qa)
    tree_sql = <<-SQL
    WITH RECURSIVE parent_search_tree(id, ref_model, ref_id, path) AS (
      SELECT id, ref_model, ref_id, ARRAY[id]
      FROM caps
      WHERE id = #{qa.cap_id}
      UNION ALL
      SELECT caps.id, caps.ref_model, caps.ref_id, path || caps.id
      FROM parent_search_tree
      JOIN quality_assessments ON quality_assessments.id = parent_search_tree.ref_id
      JOIN caps ON caps.id = quality_assessments.cap_id
      WHERE caps.ref_model = 'quality_assessment'
      AND NOT caps.id = ANY(path)
    )
    SELECT distinct id
    FROM parent_search_tree
    SQL
  end

  def self.qa_tree_sql_for(qa)
    # recursive query all CAP parents & CAP children
    # Note for Aggy:  great for situations when parent has parent has parent..etc.
    # Basic Structure:
    # WITH RECURSIVE function_name(var1,...) AS(
    #   << non-recursive starting point >>
    #   UNIONALL
    #   << recursive query using function_name(xxx)
    # )
    # <<query of result>>

    tree_sql = <<-SQL
    WITH RECURSIVE parent_search_tree(id, ref_model, ref_id, path) AS (
      SELECT id, ref_model, ref_id, ARRAY[id]
      FROM caps
      WHERE id = #{qa.cap_id || 'NULL'}
      UNION ALL
      SELECT caps.id, caps.ref_model, caps.ref_id, path || caps.id
      FROM parent_search_tree
      JOIN quality_assessments ON quality_assessments.id = parent_search_tree.ref_id
      JOIN caps ON caps.id = quality_assessments.cap_id
      WHERE caps.ref_model = 'quality_assessment'
      AND NOT caps.id = ANY(path)
    ),
    child_search_tree(id, ref_model, ref_id, path) AS (
      SELECT id, ref_model, ref_id, ARRAY[id]
      FROM caps
      WHERE id = #{qa.cap_id || 'NULL'}
      UNION ALL
      SELECT caps.id, caps.ref_model, caps.ref_id, path || caps.id
      FROM child_search_tree
      JOIN quality_assessments ON quality_assessments.cap_id = child_search_tree.id
      JOIN caps ON caps.ref_id = quality_assessments.id
      WHERE caps.ref_model = 'quality_assessment'
      AND NOT caps.id = ANY(path)
    )
    SELECT distinct id
    FROM parent_search_tree
    UNION
    SELECT distinct id
    FROM child_search_tree
    SQL
  end

  def assessment
    quality_assessment || control_plan
  end

  def concerned_assessment
    ref_model.classify.constantize.find(ref_id)
  end

  def cap_close?
    cap_type == 'CAP Close'
  end

  def related_activity
    base = ref_model.classify.constantize.left_joins(:cap)
    base.where.not(caps: { id: id }).where(caps: { ref_model: ref_model, ref_id: ref_id })
    .where(destroyed_at: nil).or(base.where(id: ref_id, destroyed_at: nil)).order(:id)
  end

  def update_qa_observers
    if ref_model == 'quality_assessment'
      puts "Updating QA's day_form.observers \n\n\n"
      qa = QualityAssessment.find(ref_id)
      qa.assessment_day_form.update(observers: invited_pls)
      puts "QA's observers: #{qa.assessment_day_form.observers}"
    end
  end

  def basic_h
    reff = ref
    serial_hash.merge({ ref_name: ref_model.titleize,
                        ref_date: reff.assess_date.strftime('%Y-%m-%d'),
                        supplier: supplier_profile.serial_hash, grid_name: reff.short_name.downcase })
  end

  def to_h
    reff = ref
    serial_hash.merge({ ref: reff, ref_name: ref_model.titleize,
                        ref_date: reff.assess_date.strftime('%Y-%m-%d'),
                        supplier: supplier_profile.standard_hash, grid_name: reff.short_name.downcase })
  end

  def self.create_from_xls(qa)
    return false if qa.excel_cap_review.attachment.nil?

    model_params = {
      ref_model: 'quality_assessment',
      ref_id: qa.id,
      supplier_profile_id: qa.supplier_profile_id,
      cap_type: 'CAP Review',
      cap_date: 2.weeks.from_now,
      people_concerned: [qa.created_by]
    }

    pts = XlsExport::CapReview.new(qa.excel_cap_review.attachment.blob).extract!

    Cap.create_and_setup(model_params, pts, qa.created_by)
  end

  def self.create_and_setup(cap_params, pts, uid = nil)
    cap = Cap.new(cap_params)
    if cap.save!
      pts.each do |pt|
        capp = CapPoint.new(pt.except(:cap_resolutions))
        cap.cap_points << capp
        next unless pt[:cap_resolutions].present?

        pt[:cap_resolutions].each do |res|
          capp.cap_resolutions << CapResolution.new(res)
        end
      end
      if cap.ref_model == 'quality_assessment'
        qa = cap.setup_qa
        qa.update(created_by: uid) if uid.present?
      elsif cap.ref_model == 'control_plan'
        cp = cap.setup_cp
        cp.update(created_by: uid)
        sch = Schedule.create(
          ref_id: cp.id,
          ref_model: 'control_plan',
          sender_id: uid,
          # TODO: supplier profile sometimes doesn't have a user id attached, change in prod
          recipient_id: cp.product_family.supplier_profile_id,
          # schedule_date: Date.strptime(params[:schedule_date], "%Y-%m-%d"),
          supplier_schedule_date: cap.cap_date
        )
      end
    else
      p cap.errors
    end
  end

  def setup_qa
    last_qa = ref
    # LAST QA INFO:
    qa_params = { supplier_code: last_qa.supplier_code, supplier_profile_id: last_qa.supplier_profile_id }
    qa_params[:grid] = last_qa.grid.present? ? last_qa.grid : 'quality_assessment'
    qa_params[:created_by] = last_qa.created_by
    # CURRENT CAP INFO:
    qa_params[:assess_date] = cap_date
    qa_params[:assess_type] = cap_type || 'CAP Review'
    qa_params[:people_concerned] = people_concerned
    qa_params[:cap_id] = id
    ch_params = { chapter_set: cap_points.map { |x| x.section }.uniq }

    new_qa = QualityAssessment.create_and_setup(qa_params, ch_params)
  end

  def setup_cp
    last_cp = ref
    cp_params = { product_family_id: last_cp.product_family_id, assessor_profile_id: last_cp.assessor_profile_id }
    cp_params[:assess_date] = cap_date
    cp_params[:plan_type] = 'CAP Review'
    cp_params[:people_concerned] = people_concerned
    cp = ControlPlan.create(cp_params)

    items_ids = cap_points.map { |x| x.ref_id }
    items = PlanItem.where(id: items_ids)
    items.each do |x|
      pi = PlanItem.create(control_plan_id: cp.id, process_step_id: x.process_step_id)
    end
    cp
  end

  def mp_path
    @ref = assessment
    if @ref.completed
      @ref.mp_path(:review)
    else
      @ref.mp_path(:assess)
    end
  end
end
