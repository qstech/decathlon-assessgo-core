class AssessorApplication < ApplicationRecord
  belongs_to :user
  belongs_to :opm, class_name: 'AssessorProfile', foreign_key: 'opm_id', optional: true
  belongs_to :validator, class_name: 'AssessorProfile', foreign_key: 'validator_id', optional: true
  belongs_to :team_leader, class_name: 'AssessorProfile', foreign_key: 'team_leader_id', optional: true
  has_many :validations, dependent: :destroy

  has_one_attached :final_validation_report

  after_create :set_user_candidate
  after_update :trigger_mail!

  def trigger_mail!
    MailBag.trigger!('assessor_application_completed', self) if final_validation == 'Completed'
  end

  BASE_PATH = '/members/me/assessor_application'.freeze
  URL_BASE = '/native/assessor_application'.freeze

  LABELS = {
    grid: 'Which assessor would you like to become?',
    reason: 'Why do you want to be an assessor?',
    team_leader: 'Who is your team leader?',
    opm: 'Who is your opm?',
    validator: 'Who would you like to be your assessor referent/validator?',
    cica_at: 'Attended CICA Training On:',
    cica_status: 'CICA Skill Result:',
    trained_at: 'Attended Training On:',
    technical_eval: 'Technical Evaluation:',
    opm_confirmed: 'OPM Confirmed?',
    opm_reason: 'OPM Comments',
    team_leader_confirmed: 'Team Leader Confirmed?',
    team_leader_reason: 'Team Leader Comments',
    step_1: 'Submit Application',
    step_2: 'Team Leader/OPM Approval',
    step_2b: 'Attend CICA Training',
    step_3: 'Becoming Assessor Training',
    step_3b: 'Submit your 2 co-animated OPEX assessment',
    step_3c: 'Technical Evaluation',
    step_4: 'Submit Assessment for Validation',
    step_5: 'Final Validation Approval'
  }.freeze

  def set_user_candidate
    ass = AssessorProfile.find_by(user_id: user_id)
    ass.candidate_for << grid
    ass.save
  end

  def to_h(user = nil)
    h = serial_hash
    h[:percentage] = percentage
    h[:steps] = [1, 2, 3, 4, 5]
    h[:steps] = [1, 2, '2b', 3, '3b', '3c', 4, 5] if grid == 'OPEX'

    h[:steps].map! { |x| status_for(x, user) }
    h
  end

  def status_for(step, user = nil)
    send(:"step_#{step}_status", user)
  end

  def step_1_status(user)
    h = {
      description: label_for(:step_1),
      status: 'Completed',
      expanded: false,
      info: base_form(%i[grid reason team_leader opm validator])
    }
    if user.nil? || user.id == user_id
      h[:page] = "#{BASE_PATH}/application?id=#{id}"
      h[:url] = "#{URL_BASE}/application?id=#{id}"
    end
    h
  end

  def step_2_status(user)
    h = {
      description: label_for(:step_2),
      status: opm_status,
      expanded: false
    }
    if h[:status] != 'Waiting for OPM & Team Leader'
      h[:info] = base_form(%i[opm_confirmed opm_reason team_leader_confirmed team_leader_reason])
    end

    return h if user.nil? || user.id == user_id

    role = 'opm' if user.profile.id == opm_id
    role = 'team_leader' if user.profile.id == team_leader_id
    if role
      h[:page] = "#{BASE_PATH}/qualification?id=#{id}&role=#{role}"
      h[:url] = "#{URL_BASE}/qualification?id=#{id}&role=#{role}"
      h[:btn_text] = 'Submit'
    end
    h
  end

  def step_2b_status(_user)
    h = {
      description: label_for(:step_2b),
      status: cica_status,
      expanded: false
    }
    if h[:status] != 'Completed' && (opm_confirmed && team_leader_confirmed)
      h[:page] = "#{BASE_PATH}/training_time?id=#{id}&cica=true"
      h[:url] = "#{URL_BASE}/training_time?id=#{id}&cica=true"
      h[:btn_text] = 'Submit'
    elsif h[:status] == 'Completed'
      h[:info] = base_form(%i[cica_at cica_status])
    end
    h
  end

  def step_3_status(_user)
    h = {
      description: label_for(:step_3),
      status: training_status,
      expanded: false
    }
    if h[:status] != 'Completed' && (opm_confirmed && team_leader_confirmed)
      h[:page] = "#{BASE_PATH}/training_time?id=#{id}"
      h[:url] = "#{URL_BASE}/training_time?id=#{id}"
      h[:btn_text] = 'Submit'
    elsif h[:status] == 'Completed'
      h[:info] = base_form([:trained_at])
    end
    h
  end

  def step_3b_status(user)
    h = {
      description: label_for(:step_3b),
      status: co_animate_status,
      expanded: false
    }

    if validations.where(co_animated: true).present?
      h[:info] = validations.where(co_animated: true).map { |x| x.base_form(%i[created_at assessment_summary]) }.flatten
    end

    return h if user.nil? || user.id != user_id

    if h[:status] != 'Completed'
      h[:page] = "#{BASE_PATH}/application_selection?id=#{id}&co_animated=true"
      h[:url] = "#{URL_BASE}/application_selection?id=#{id}&co_animated=true"
      h[:btn_text] = 'Submit'
    end
    h
  end

  def step_3c_status(user)
    h = {
      description: label_for(:step_3c),
      status: technical_eval_status,
      expanded: false
    }

    h[:info] = base_form([:technical_eval]) if technical_eval.present?

    return h if user.nil? || user.id != user_id

    if h[:status] != 'Completed'
      h[:page] = "#{BASE_PATH}/technical_eval?id=#{id}"
      h[:url] = "#{URL_BASE}/technical_eval?id=#{id}"
      h[:btn_text] = 'Submit'
    end
    h
  end

  def step_4_status(user)
    h = {
      description: label_for(:step_4),
      status: assessment_submitted_status,
      expanded: false
    }

    if validations.where(co_animated: false).present?
      h[:info] = validations.where(co_animated: false).map do |x|
        x.base_form(%i[created_at assessment_summary])
      end.flatten
    end

    return h if user.nil? || user.id != user_id

    if h[:status] != 'Completed' && validations.where(co_animated: false, result: nil).blank?
      h[:page] = "#{BASE_PATH}/application_selection?id=#{id}"
      h[:url] = "#{URL_BASE}/application_selection?id=#{id}"
      h[:btn_text] = 'Submit'
    end
    h
  end

  def step_5_status(user)
    h = {
      description: label_for(:step_5),
      status: assessment_validation_status,
      expanded: false
    }

    if validations.where.not(co_animated: false, result: nil).present?
      h[:info] = validations(co_animated: false).map { |x| x.base_form(%i[result reason]) }.flatten
    end

    return h if user.nil? || user.id == user_id

    if (user.profile.id == validator_id) && validations.find_by(co_animated: false, result: nil).present?
      valid_id = validations.find_by(co_animated: false, result: nil).id
      h[:page] = "#{BASE_PATH}/validator?id=#{valid_id}"
      h[:url] = "#{URL_BASE}/validator?id=#{valid_id}"
      h[:btn_text] = 'Submit'
    end
    h
  end

  def percentage
    stats = ['Completed', opm_status, training_status, assessment_validation_status, final_validation]
    stats.count('Completed') * 20
  end

  # ======================================
  # STATUS FOR STEPS

  def opm_status
    if opm_confirmed.nil? && team_leader_confirmed.nil?
      'Waiting for OPM & Team Leader'
    elsif opm_confirmed && team_leader_confirmed
      'Completed'
    elsif (opm_confirmed == false) || (team_leader_confirmed == false)
      'Request Denied'
    elsif opm_confirmed
      'Waiting for Team Leader'
    elsif team_leader_confirmed
      'Waiting for OPM'
    end
  end

  def co_animate_status
    v = validations.where(co_animated: true)
    v.count == 2 ? 'Completed' : 'Must submit Co-Animated Assessment'
  end

  def technical_eval_status
    technical_eval.present? ? 'Completed' : 'Not yet recorded'
  end

  def cica_status
    cica_at.present? ? 'Completed' : 'Not yet recorded'
  end

  def training_status
    trained_at.present? ? 'Completed' : 'Not yet recorded'
  end

  def assessment_submitted_status
    v = validations.where(co_animated: false)
    if v.select { |x| x.result.nil? }.present?
      'Submitted. Awaiting evaluation.'
    elsif v.select { |x| x.validated }.present?
      'Completed'
    elsif v.blank?
      'Assessment Not Yet Submitted'
    else
      'In Progress'
    end
  end

  def assessment_validation_status
    v = validations.where(co_animated: false)
    if v.blank?
      'Not Yet Submitted'
    elsif v.select { |x| x.validated }.present?
      'Completed'
    elsif v.select { |x| x.result == 'Does Not Validate' }.present?
      'Not Validated'
    elsif v.select { |x| x.result.nil? }
      'In Progress'
    else
      'Partially Validated, Submit Another On-Site Validation'
    end
  end

  def final_validation
    if final_validation_report.attachment.present?
      'Completed'
    elsif validations.where(validated: true).blank?
      'Waiting for Validation'
    else
      'Waiting For Final Validation Report'
    end
  end
end
