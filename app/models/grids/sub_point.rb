class SubPoint < ApplicationRecord
  belongs_to :guideline
  has_paper_trail ignore: %i[created_at updated_at]
end
