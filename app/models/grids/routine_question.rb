# frozen_string_literal: true

# Routine Question carries dropdown info for SSE
class RoutineQuestion < ApplicationRecord
  belongs_to :guideline
  has_paper_trail ignore: %i[created_at updated_at]
  translates :questions, :how_to_check, :frequency, :guidance_definitions, :documents_to_check
  before_update :trim_content_fields

  CONTENT_FIELDS = %i[questions how_to_check guidance_definitions documents_to_check].freeze

  def to_h
    h = serial_hash
    CONTENT_FIELDS.each do |item|
      h[item] = [] if public_send(item).nil?
    end
    h
  end

  def trim_content_fields
    CONTENT_FIELDS.each do |x|
      value = public_send(x)
      public_send(:"#{x}=", value.reject(&:blank?)) if value.is_a? Array
    end
  end

  def self.translate_sync
    all.each do |x|
      htc = x.attributes['how_to_check']
      x.how_to_check = htc
      x.save
    end
  end
end
