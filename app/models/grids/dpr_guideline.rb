class DprGuideline < ApplicationRecord
  validates :guideline_tag, presence: true, uniqueness: true
  validates :code, presence: true, uniqueness: true

  has_paper_trail ignore: %i[created_at updated_at]
  has_many_attached :toolbox_images

  def to_h(_date = nil)
    h = serial_hash.except(:id)
    h[:grid_images] = grid_images_local
    h[:section_split] = section_split
    # TODO:  Copy organizational logic of guideline for frontend integration
    # # h[:sub_points] = sub_points.map{|x| x.serial_hash} if sub_points.present?
    # unorganized_tree = self.descendents.group_by{|x| x.parent_id}
    # h[:children] = organize_tree(id, unorganized_tree, date)
    h
  end

  def response_set
    response_choices.map { |x| { name: x, value: x } }
  end

  def ch_name
    "Chatper #{chapter_no}"
  end

  def ch_title
    chapter_name
  end

  def requirement
    "#{ch_title}\n #{lev_name} \n #{sec_title}: #{lev_title}"
  end

  def section_split
    code.split('.').map(&:to_i)
  end

  def sec_name
    "#{chapter_no}.#{section_no}"
  end

  def sec_title
    section_name
  end

  def lev_name
    "#{chapter_no}.#{step_no}"
  end

  def lev_title
    "#{step_name} (#{from_level}=>#{to_level})"
  end

  def ch_key
    "#{chapter_no}-#{chapter_name}"
  end

  def sec_key
    "#{section_no} - #{section_name}"
  end

  def lev_key
    "#{step_no} - #{step_name}"
  end

  def seed_images!
    filestem = "dpr_#{code}"
    filename = "#{filestem}_1.png"
    dir = "#{Rails.root}/public/dpr"
    files = []
    files = Dir.entries(dir).select { |f| f.include?(filestem) } if Dir.entries(dir).include?(filename)

    files.each do |f|
      toolbox_images.attach(io: File.open("#{dir}/#{f}"), filename: f)
    end
  end

  def grid_images_local
    filestem = "dpr_#{code}"
    filename = "#{filestem}_1.png"
    dir = "#{Rails.root}/public/dpr"
    pdf_set = false
    if Dir.entries(dir).include?(filename)
      pdf_set = Dir.entries(dir).select { |f| f.include?(filestem) }.map { |f| "#{ApplicationRecord::DOMAIN}/dpr/#{f}" }
    end
    pdf_set
  end

  def chapter_h
    { section_name: ch_name, title: ch_title, children: [] }
  end

  def section_h
    {
      section_name: sec_name, title: sec_title,
      from: from_level, to: to_level, section_split: section_split,
      children: []
    }
  end

  def level_h
    {
      guideline_tag: guideline_tag, section_name: lev_name,
      title: step_name, from: from_level, to: to_level,
      tag: "/images/dpr/#{from_level.downcase}_to_#{to_level.downcase}_v2.png",
      children: [to_h], response_choices: ["OK", "NOK", "Not Applicable", "Not Assessed"]
    }
  end

  def self.fetch_for(dpr_grid)
    guidelns = DprGuideline.where(grid: dpr_grid).order(guideline_tag: :asc)
    return [] if guidelns.blank?

    # CHAPTERS => ARRAY OF
    #    {guideline_tag, title, content, active, section_name, section_type, section_no, children, level}
    #    Sections:  children => same + cotation
    #    Levels:   children => same + color, chapter_id, result, cotation
    #
    # FRONTEND REQ: CH: section_name, title, children
    #               SEC:  section_name, title, children
    #               LEV:  chapter_id, section_name,

    guidelns.group_by(&:ch_key).values.map do |x|
      children = x.group_by(&:sec_key).values
                  .sort { |a, b| a.first.section_split <=> b.first.section_split }
                  .map do |y|
                    y_children = y.group_by(&:lev_key).values.map { |z| z.first.level_h }
                    y.first.section_h.merge({ children: y_children })
                  end

      x.first.chapter_h.merge({ children: children })
    end
  end
end
