class GridVersion < ApplicationRecord
  has_many :guidelines
  validates :grid, presence: true, uniqueness: { scope: [:version_number] }
  after_save :deactivate_others!, if: :active

  def deactivate_others!
    GridVersion.where.not(id: id).where(grid: grid).update_all(active: false)
  end
end
