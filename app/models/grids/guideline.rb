class Guideline < ApplicationRecord
  translates :title, :content, :note_header, :note_bullets, :toolbox_slides
  enum toolbox_mode: %i[attachments slides]
  # attr_accessor :parent

  belongs_to :parent, class_name: 'Guideline', foreign_key: 'parent_id', optional: true
  belongs_to :grid_version

  has_many :sub_points
  has_many :routine_questions
  has_many :children, class_name: 'Guideline', foreign_key: 'parent_id'
  has_one :evaluation, dependent: :destroy
  has_many_attached :toolbox_images
  accepts_nested_attributes_for :routine_questions, reject_if: :all_blank, allow_destroy: true

  validates :guideline_tag, presence: true, uniqueness: { scope: [:grid_version_id] }
  has_paper_trail ignore: %i[created_at updated_at]
  scope :top_level, -> { where(parent_id: nil) }
  scope :optimized, -> { includes([{ toolbox_images_attachments: :blob }, :routine_questions]) }
  scope :with_children, -> { includes(children: [{ toolbox_images_attachments: :blob }, :routine_questions]) }
  scope :with_grandchildren, -> {
    includes(children: [
               { children: [{ toolbox_images_attachments: :blob },
                            :routine_questions] }, { toolbox_images_attachments: :blob }, :routine_questions
    ])
  }
  scope :with_all_children, -> {
    includes(children: [
               { children: [{ children: [:routine_questions] }, { toolbox_images_attachments: :blob }] }
    ])
  }
  scope :active, -> { joins(:grid_version).where(grid_versions: { active: true }) }

  enum process: %i[textile footwear heavystitching hardware bikes] # used only for INDUS grid

  SLIDES_HOST = Rails.application.credentials.dig(:google, :api)
  SLIDES_PATH = 'api/v1/google/slides_thumbnail'
  RESPONSE_VALUES = ['OK', 'NOK', 'NOK without risk', 'Not Applicable', 'Not Assessed']

  def build_routine_question
    RoutineQuestion.create(guideline_id: id) if routine_questions.blank?
  end

  def includes_process?
    ref_model.in?(%w[indus_assessment])
  end

  def response_set
    response_choices.map { |x| { name: x, value: x } }
  end

  def requirement
    "#{content} \n#{note_header || ''} #{(note_bullets || []).join("\n") || []}"
  end

  def section_split
    section? ? section_name.split('.').map(&:to_i) : section
  end

  def chapter
    guideline_tag.gsub(/[A-Z]+(\d+).*/, '\1')
  end

  def section
    guideline_tag.gsub(/[A-Z]+(\d+)S(\d+).*/, '\2')
  end

  def lev_name
    "#{chapter}.#{section}"
  end

  def sort_id
    point? ? guideline_tag.split('-').last.to_i : guideline_tag
  end

  def merged_content
    arr1 = [content]
    arr1 << note_header if note_header.present?
    m_content = arr1.join('  ')
    m_content = [m_content, note_bullets].flatten.join('  -> ') if note_bullets.present?
    m_content
  end

  def slide_urls
    (toolbox_slides || []).map { |slide| "#{ApplicationRecord::DOMAIN}/gs?slide_url=#{CGI.escape(slide)}" }
  end

  def fetch_toolbox_slides(china: false)
    slide_urls.map { |slide| fetch_slide(slide, china: china) }.reject(&:nil?)
  end

  def fetch_slide(slide, china: false)
    google = google_service_url(slide)
    slide = JSON.parse(RestClient.get(google).body)
    china ? slide['cn_url'] : slide['url']
  rescue RestClient::InternalServerError
    nil
  end

  def google_service_url(slide)
    slide_url = CGI.unescape(slide.split('/gs?slide_url=')[1])
    parsed = slide_url.match(%r{/d/([^/]*)/.*slide=id\.(\w*)})
    pres = parsed[1]
    page = parsed[2]
    "#{SLIDES_HOST}/#{SLIDES_PATH}?id=0&presentation=#{pres}&page=#{page}"
  end

  def show_survey?
    guideline_tag == 'HRPCH2S5L2-3'
  end

  def to_h(date = nil, product = nil, with_children: true, with_routine: true, merged: false, skip_nutrition: true)
    h = serial_hash(except: [:toolbox_images], skip_attachments: !level?)
    # h[:sub_points] = sub_points.map{|x| x.serial_hash} if sub_points.present?

    h[:routine_questions] = with_routine && point? ? routine_questions.map(&:to_h) : []
    h[:toolbox_images] = slide_urls if slides?
    h[:content] = merged_content if merged
    h[:content_split] = h[:content].split("\n")
    h[:section_split] = section_split if section?
    h[:show_survey] = show_survey?

    if with_children && !point?
      h[:children] = children.select { |x| product.nil? ? true : x.relavent_products.include?(product) }
      h[:children].reject!(&:nutrition) if skip_nutrition
      h[:children] = h[:children].reject(&:destroyed_at).sort_by(&:sort_id).map { |x| x.to_h(date, product) }
    end
    h
  end

  def point?
    section_type == 'Point'
  end

  def chapter?
    section_type == 'Chapter'
  end

  def section?
    section_type == 'Section'
  end

  def level?
    section_type == 'Level'
  end

  def with_attributes(vars = {})
    vars.each do |k, v|
      instance_variable_set(:"@#{k}", v)
    end
  end

  def point_form(_cqs, i, vars)
    with_attributes(vars)
    cq = @cqs.find { |x| x.chapter_tag == guideline_tag }
    return nil if cq.id.in?([0, nil])

    pt_h = to_h(with_children: false, with_routine: true, merged: @native)
    cq_h = cq.single_chapter_form(@user, show_last: !@native, qa: @qa, last_attempt: @last_attempt, i: i)
    pt_h.merge(cq_h)
  end

  def format_points(vars)
    points = children.select { |pt| @tags.include?(pt.guideline_tag) }.sort_by(&:sort_id)
    points = points.map.with_index { |pt, i| pt.point_form(@cqs, i, vars) }
    points.reject { |pt| pt[:form_fields][:id] == 0 }
  end

  def toolbox_default(native: false)
    return [] if native

    false
  end

  def single_chapter_form(vars)
    with_attributes(vars)
    return nil if children.blank?

    lev = to_h(with_children: false, with_routine: false)
    points = format_points(vars)
    obj = @chs.first
    levobj = @chs.find { |x| x.chapter_tag == guideline_tag }

    return nil if levobj.nil?

    lev[:section_name] = @qa.assert_standard_level(lev[:section_name])
    lev[:children] = points
    # TOOLBOX FOR SECTION
    toolbox = lev[:toolbox_images]
    toolbox = toolbox_default(native: @native) if toolbox.blank?
    toolbox = fetch_toolbox_slides(china: @china) if slides?

    {
      section: @gl.serial_hash,
      color: PageComponent::EXTRA_CLASSES[section_name],
      level: lev,
      toolbox: toolbox,
      lev_obj: levobj,
      object: obj.serial_hash,
      points: points,
      images: [],
      comments: obj.comments.order(created_at: :desc).map { |x| PageComponent.comment_card(x, @user) }
    }
  end

  def organize_tree(pid, unorganized_tree, date = nil, product = nil)
    return [] if unorganized_tree.blank?

    child_set = if unorganized_tree[pid].present?
      unorganized_tree[pid].select do |x|
        product.nil? || x.relavent_products.include?(product)
      end.map do |x|
        x = x.version_at(date) if date.present?
        x.to_h(with_children: false)
      end
    else
      []
    end
    child_set.select! { |x| x[:destroyed_at].nil? }

    new_tree = unorganized_tree.except(pid)
    child_set.each do |child|
      child[:children] = organize_tree(child[:id], new_tree, date, product)
    end
    child_set
  end

  def descendents
    self_and_descendents - [self]
  end

  def self_and_descendents
    self.class.tree_for(self)
  end

  def self.tree_for(instance)
    with_attached_toolbox_images
    .includes(:routine_questions, :versions)
    .where("#{table_name}.id IN (#{tree_sql_for(instance)})")
    .where(destroyed_at: nil).order("#{table_name}.id")
  end

  def self.tree_sql_for(instance)
    tree_sql = <<-SQL
    WITH RECURSIVE search_tree(id, path) AS (
      SELECT id, ARRAY[id]
      FROM #{table_name}
      WHERE id = #{instance.id}
      UNION ALL
      SELECT #{table_name}.id, path || #{table_name}.id
      FROM search_tree
      JOIN #{table_name} ON #{table_name}.parent_id = search_tree.id
      WHERE NOT #{table_name}.id = ANY(path)
    )
    SELECT id FROM search_tree ORDER BY path
    SQL
  end

  def self.fetch_for(model_name)
    guidelns = Guideline.optimized.with_all_children.active.top_level
    .where(ref_model: model_name).order(guideline_tag: :asc)
    return [] if guidelns.blank?

    guidelns = guidelns.map(&:to_h)
  end

  def self.for_assessemt(grid, date = nil)
    gls = Guideline.active.includes(:versions, :routine_questions)
    .where(ref_model: grid, destroyed_at: nil).order(parent_id: :desc)
    parents = gls.reject(&:parent_id).sort_by(&:id)
    unorganized_tree = gls.select(&:parent_id).sort_by(&:id).group_by(&:parent_id)

    parents.map! do |gl|
      h = gl.serial_hash.except(:id)
      h[:routine_questions] = gl.routine_questions.map { |x| x.serial_hash }
      h[:children] = gl.organize_tree(gl.id, unorganized_tree, date)
      h
    end
  end

  def self.sections_for(model_name)
    Guideline.active.select(:section_name)
    .where(ref_model: model_name, section_type: 'Section')
    .order(:section_name).map(&:section_name)
  end

  def meta_data
    ch = guideline_tag.gsub(/[A-Z]+(\d+).*/, '\1')
    sec = guideline_tag.gsub(/[A-Z]+(\d+)S(\d+).*/, '\2')

    {
      chapter_tag: guideline_tag, chapter: ch, section: sec,
      level: section_name.split('-').first, point: section_name
    }
  end

  def merge_relavent_products!(prods)
    self.relavent_products = [] if relavent_products.nil?
    self.relavent_products += prods
    self.relavent_products.uniq!
    save
  end

  def self.find_by_hrp_tag(tag)
    raise StandardError unless tag.is_a? String
    raise StandardError unless tag.include?('.')

    ch_no, q_no = tag.split('.')
    gls = Guideline.active.where(ref_model: 'hrp_assessment', section_type: 'Section').order(:guideline_tag)
    gls[ch_no.to_i - 1].grandchildren[q_no.to_i - 1]
  end

  def self.find_by_env_tag(tag)
    raise StandardError unless tag.is_a? String
    raise StandardError unless tag.include?('.')

    ch_no, lev_no, q_no = tag.split('.')
    gls = Guideline.active.where(ref_model: 'env_assessment', section_type: 'Section').order(:guideline_tag)
    gls[ch_no.to_i].children[lev_no.to_i - 1].children[q_no.to_i - 1]
  end

  def grandchildren
    children.map(&:children).flatten.sort_by(&:sort_id)
  end
end
