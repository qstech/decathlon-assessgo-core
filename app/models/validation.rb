class Validation < ApplicationRecord
  belongs_to :quality_assessment, optional: true
  belongs_to :control_plan, optional: true
  belongs_to :assessor_application

  before_update :set_validated

  LABELS = {
    created_at: 'Validation Submission Date',
    assessment_summary: 'Assessment Submitted',
    date: 'Validation Date',
    result: 'Validation Result',
    reason: 'Validation Comments'
  }
  def assessment
    quality_assessment || control_plan
  end

  def date
    if quality_assessment.present?
      quality_assessment.assess_date
    elsif control_plan.present?
      control_plan.assess_date
    end
  end

  def assessment_summary
    if quality_assessment_id.present?
      ass = quality_assessment
      typ = ass.assess_type
    elsif control_plan_id.present?
      ass = control_plan
      typ = ass.plan_type
    end

    sp = ass.supplier_profile
    "#{ass.short_name} (#{typ}) on #{ass.assess_date}, for #{sp.name} (#{sp.code})"
  end

  def set_validated
    if result == 'Validates'
      self.validated = true
      assessor_application.update(completed: true, result: 'validated', status: 'Completed')
      ass = assessor_application.user.assessor_profile
      ass.candidate_for.delete(assessor_application.grid)
      ass.assessor_for << assessor_application.grid
      ass.save
    end
  end
end
