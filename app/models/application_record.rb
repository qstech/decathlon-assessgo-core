class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true
  VALID_MODELS = %w[bug_reports mail_bags letters action_text_rich_texts
                    assessment_day_forms assessor_assignments assessor_applications
                    assessor_profiles cap_resolutions cap_followups cap_points change_logs
                    caps comments control_plans chapter_assessments chapter_questions
                    cp_pending_frequencies dpr_guidelines form_sections form_questions forms
                    followers goes_activities favorites form_options likes guidelines
                    kpi_tracks invitations kpi_records next_assessment_logs
                    next_assessment_schedules quality_assessments product_families
                    prep_questions product_processes process_steps plan_items
                    sub_points schedules routine_questions users search_histories
                    validations supplier_profiles survey_questions].freeze

  # after_create :trigger_mailbag_create!
  # after_update :trigger_mailbag_update!
  # after_destroy :trigger_mailbag_destroy!

  # def trigger_mailbag_create!
  #   MailBag.trigger!("#{self.class.model_name.singular}_create", self)
  # end

  # def trigger_mailbag_update!
  #   MailBag.trigger!("#{self.class.model_name.singular}_update", self)
  # end

  # def trigger_mailbag_destroy!
  #   MailBag.trigger!("#{self.class.model_name.singular}_destroy", self)
  # end

  def with_vars(vars)
    @vars = vars
    vars.each do |k, v|
      instance_variable_set(:"@#{k}", v)
    end
    self
  end

  def self.valid_tables
    ActiveRecord::Base.connection.tables - %w[active_storage_attachments active_storage_blobs
                                              ar_internal_metadata schema_migrations versions authentication_tokens]
  end

  DOMAIN = if Rails.env == 'development'
             'http://127.0.0.1:3000'
           elsif Rails.env == 'production' && ENV['TESTING'].present?
             'https://assessgopp.decathlon.cn'
           elsif Rails.env == 'production'
             'https://assessgo.decathlon.cn'
           else
             'http://127.0.0.1:3000'
           end

  before_update :cascaded_soft_delete
  before_save :non_zero_id
  before_save :remove_empty_string_from_array

  def non_zero_id
    id = nil if id == 0
  end

  def remove_empty_string_from_array
    model = model_name.name.constantize
    h = serializable_hash
    h.each do |k, v|
      next if model.columns_hash[k.to_s].nil?

      datatype = model.columns_hash[k.to_s].type
      default = model.columns_hash[k.to_s].default

      v.delete('') if datatype == :text && default == '{}' && v.present?
    end
  end

  def cascaded_soft_delete
    return true unless self.class.column_names.include?('destroyed_at')

    if destroyed_at_changed? && destroyed_at.present?
      [goes_activities, comments,
       likes, favorites, invitations].each do |set|
        set.update_all(destroyed_at: Time.now) if set.present? && set.first.class.column_names.include?('destroyed_at')
      end
    end
  end

  def recover!
    return true unless self.class.column_names.include?('destroyed_at')

    [goes_activities, comments,
     likes, favorites, invitations].each do |set|
      if set.present? && set.first.class.column_names.include?('destroyed_at')
        set.unscoped.where(ref_model: model_name.singular,
                           ref_id: id).update_all(destroyed_at: nil)
      end
    end

    update!(destroyed_at: nil)
  end

  # before_destroy :soft_delete
  # scope :with_comments, -> { includes("#{name}_attachment": :blob) }

  # def soft_delete
  #   if self.class.column_names.include? ("destroyed_at")
  #     self.update(destroyed_at: Time.now)
  #     return false
  #   else
  #     return true
  #   end
  # end

  def ref
    ref_model.classify.constantize.find(ref_id) if ref_model.present? && ref_id.present?
  end

  def self.form
    f = Form.find_by(ref_model: model_name.singular)
    return false if f.blank?

    f.to_h
  end

  def self.form_questions
    fq = FormQuestion.includes(:form_section).includes(form_section: :form).where(forms: { ref_model: model_name.singular })
  end

  def standard_hash
    if methods.include?(:to_h)
      to_h
    else
      serial_hash
    end
  end

  def render_activity(user)
    goes_params = {
      user_id: user.id,
      ref_model: model_name.singular,
      ref_id: id
    }
    # TO SETUP GOES ACTIVITY IN MODEL BEGIN W/ SUPER
    # EX:  goes_params = super
    #      set status & content
    #
  end

  def serial_hash(except: [], only: [], skip_attachments: false)
    except.map!(&:to_s)
    only.map!(&:to_s)
    h = {}
    klass = self.class
    klass.columns.each do |col|
      nm = col.name
      next unless only.blank? || only.include?(nm)
      next if except.include?(nm)

      nm = nm[0..-14] if nm.include?('_translations')
      h[nm] = (public_send(nm) || default_for(col))
      h[nm] = h[nm].strftime('%Y-%m-%d') if h[nm].is_a? ActiveSupport::TimeWithZone
    end

    h = h.except('destroyed_at')
    h.symbolize_keys!

    h = serial_attachments(h, except: except, only: only) unless skip_attachments
    h
  end

  def default_for(column)
    nm = column.name
    nm = nm[0..-14] if nm.include?('_translations')

    if column.type == :integer
      0
    elsif column.type == :boolean
      false
    elsif column.type == :text && column.default == '{}'
      []
    else
      attributes[nm] || ''
    end
  end

  def blob_link(x)
    host = DOMAIN
    Rails.application.routes.url_helpers.rails_blob_url(x, host: host)
  rescue Module::DelegationError
    ''
  end

  def variant_link(x)
    variant = x.variant(resize: '160x140')
    host = DOMAIN
    Rails.application.routes.url_helpers.rails_representation_url(variant, host: host)
  rescue ActiveStorage::InvariableError
    blob_link(x)
  rescue Module::DelegationError
    ''
  end

  h = def serial_attachments(h, except: [], only: [])
        h
        attachment_types = self.class.methods.map do |x|
                             x.to_s
                           end.select { |x| x.include?('with_attached') }.map { |x| x[14..-1].to_sym }
        attachment_types.each do |att|
          next unless only.blank? || only.include?(att)
          next if except.include?(att)

          set = public_send(att)
          if set.instance_of?(ActiveStorage::Attached::Many)
            thumbs = set.map { |x| variant_link(x) }.reject(&:blank?)
            result = set.map { |x| blob_link(x) }.reject(&:blank?)
            bookmarks = set.map(&:bookmark)
          elsif set.instance_of?(ActiveStorage::Attached::One)
            thumbs = set.attached? ? variant_link(set) : ''
            result = set.attached? ? blob_link(set) : ''
            bookmarks = set.attached? ? set.bookmark : ''
          end
          h[att] = result
          h["#{att}_thumbs".to_sym] = thumbs
          h["#{att}_bookmarks".to_sym] = bookmarks
        end
        h
      end

  def self.guidelines
    guidelns = Guideline.active.where(ref_model: model_name.singular, parent_id: nil).order(guideline_tag: :asc)
    return [] if guidelns.blank?

    guidelns = guidelns.map { |x| x.to_h }
  end

  def self.fetch_guidelines(_params = {})
    { guidelines: guidelines }
  end

  def base_form(columns)
    columns.map do |column|
      base_input(column)
    end
  end

  def base_input(column)
    label = label_for(column)
    value = send(column)
    value = 'Not Available' if value.nil?
    value = 'YES' if value == true
    value = 'NO' if value == false
    value = value.name if value.is_a? AssessorProfile
    value = value.name if value.is_a? SupplierProfile
    value = value.strftime('%Y-%m-%d') if value.is_a? ActiveSupport::TimeWithZone
    value = blob_link(value) if value.is_a? ActiveStorage::Attached::One

    { label: label, value: value }
  end

  def label_for(column)
    self.class::LABELS[column]
  end

  def version_where_attribute_changed_to(attribute_name, attribute_value)
    versions.select do |version|
      if version.object_changes.blank?
        false
      else
        object_changes = YAML.load version.object_changes
        (object_changes[attribute_name.to_s] || [])[1] == attribute_value
      end
    end
  end

  def safe_send(sym)
    v = public_send(sym)

    if v.nil?
      ''
    elsif v.is_a?(Array)
      v.join(', ')
    elsif !v.is_a?(String)
      v.to_s
    else
      v
    end
  end

  def goes_activities
    GoesActivity.where(id: 0)
  end

  def comments
    Comment.where(id: 0)
  end

  def likes
    Like.where(id: 0)
  end

  def favorites
    Favorite.where(id: 0)
  end

  def invitations
    Invitation.where(id: 0)
  end

  def version_at(_time)
    self
    #   return self if time.nil?
    #   p time

    #   time = Time.parse(time) if time.is_a? String
    #   p time

    #   version = versions.select{|x| x.created_at < time }.last
    #   p version
    #   return self if version.nil?
    #   return self if version.created_at == created_at

    #   historical_object = version.reify
    #   historical_object.nil? ? self : historical_object
    # rescue NoMethodError
    #   return self
  end
end
