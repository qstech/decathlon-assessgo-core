require 'csv'
class BugReport < ApplicationRecord
  enum app: %i[unknown_app web wechat ios android multi_platform]
  enum crash_level: %i[unknown_crash server_error cannot_assess improve_assessment
                       save_time usability_request new_feature]
  enum feature: %i[QA CP OPEX HRP SSE ENV SCM PUR DPR CAP_REVIEW CAP_CLOSE TRANSLATIONS STATS OTHER IND]
  enum status: %i[unconfirmed pending debugged feature_request closed rejected decathlon_request]
  enum assignee: %i[empty sergio jason aggy serg haidee]
  enum priority: %i[not_set p0 p1 p2 p3]
  store_accessor :user_params, :user_id, :username, :email
  has_many_attached :support_images
  has_many_attached :support_files
  after_update :set_resolved_at
  after_create :update_related!

  attr_accessor :bug_counts

  STATUS_COLLECTION = BugReport.statuses.keys.map { |v| [v, v] }
  ASSIGNEE_COLLECTION = BugReport.assignees.keys.map { |x| [x.humanize, x] }
  PRIORITY_COLLECTION = BugReport.priorities.keys.map { |x| [x.humanize, x] }

  ACTIVESTORAGE_DUPLICATE_KEY = 'index_active_storage_attachments_uniqueness'.freeze
  ACTIVESTORAGE_INTEGRITY = 'ActiveStorage::IntegrityError'.freeze
  ACTIVESTORAGE_INVALID_BLOB = 'signed_id delegated to blob, but blob is nil'.freeze
  CONNECTION_RESET = 'Errno::ECONNRESET'.freeze

  class << self
    def for_user(user)
      if user.admin || user.technical?
        all.order(:id)
      else
        where("user_params->>'user_id' = '#{user.id}'").order(:id)
      end
    end

    def create_from_exception(exception)
      new(bug_type: exception.class.to_s, message: exception.message,
          backtrace: exception.backtrace, crash_level: :server_error)
    end

    def invalid_messages
      [ACTIVESTORAGE_DUPLICATE_KEY, ACTIVESTORAGE_INTEGRITY, ACTIVESTORAGE_INVALID_BLOB, CONNECTION_RESET]
    end
  end

  def set_resolved_at
    if saved_change_to_status && closed?
      self.update(resolved_at: DateTime.now)
    elsif saved_change_to_status
      self.update(resolved_at: nil)
    end
  end

  def with_count(count)
    self.bug_counts = count
    self
  end

  def group_tag
    backtrace.present? ? backtrace : id
  end

  def trigger_complete
    MailBag.trigger!('bug_report_completed', self) if status_changed? && closed
  end

  def supplier?
    username == email
  end

  def user_type
    return 'unknown' if username.nil?

    supplier? ? 'Supplier' : 'Decathlon'
  end

  def supplier
    SupplierProfile.find_by(code: supplier_code) || SupplierProfile.new
  end

  def update_related!
    related_bugs.update_all(status: status) if auto_report?
  end

  def related_bugs
    BugReport.where(backtrace: backtrace)
  end

  def impact_list
    User.where(id: related_bugs.map(&:user_id).uniq)
  end

  def bug_report_path
    Rails.application.routes.url_helpers.bug_report_path(id: id)
  end

  def auto_report?
    request_params.present?
  end

  def report_type
    auto_report? ? 'auto-report' : 'user filed'
  end

  def table_row
    [created_at.strftime('%Y-%m-%d'), report_type,
     supplier_code, crash_level.titleize, "#{username} - #{user_type}",
     status, priority.humanize, assignee.humanize, user_description,
     "#{ApplicationRecord::DOMAIN}#{bug_report_path}"]
  end
end
