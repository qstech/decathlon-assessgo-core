module AssessmentHelper::Cards
  extend ActiveSupport::Concern
  def invitation_card
    {
      description: "You have been invited to help with a #{short_name} Assessment",
      buttonDesc: 'Go to invitations ➜',
      iconType: 'info',
      bindtap: 'goInvitations',
      url: "/members/assessment/invitations?grid=#{grid}"
    }
  end

  def assessor_request_card
    {
      description: 'You have pending assessment requests!',
      buttonDesc: 'Go to requests ➜',
      iconType: 'info',
      bindtap: 'goInvitations',
      url: '/members/assessment/invitations?set_assessor=true'
    }
  end

  def index_header
    {
      bgColor: bg_color,
      tableLogo: logo,
      tableDesc: motto,
      buttonText: 'New Assessment ➜',
      toPage: 'newQa',
      url: '/members/assessment/new'
    }
  end

  def index_cards(user, is_self: true)
    total_set = user.relavent_assessments(grid)

    { header: index_header,
      self: true,
      invited: (is_self && user.pending_invitations?(grid)),
      invitation: invitation_card,
      short_name: short_name,
      set_assessor: assessor_request_card,
      must_set_assessor: (is_self && hrp? && user.must_set_assessor),
      gemba_qacards: map_qa_cards(total_set.reject(&:official_assessment?)),
      annual_qacards: map_qa_cards(total_set.select(&:official_assessment?)) }
  end

  def map_qa_cards(assessments)
    assessments.map { |x| PageComponent.future_qa_card(x) }.reject(&:nil?)
  end
end
