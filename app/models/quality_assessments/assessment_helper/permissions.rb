module AssessmentHelper::Permissions
  extend ActiveSupport::Concern

  def invalid_level?(level)
    return false unless level.is_a?(String)
    return false unless assess_type == 'Gemba E/D Levels'

    level < 'D'
  end

  def show_sections?
    return false if short_name == 'DPR'

    assess_type.in?(['Gemba', 'Self Assessment'])
  end
end
