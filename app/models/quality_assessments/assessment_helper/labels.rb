module AssessmentHelper::Labels
  extend ActiveSupport::Concern

  def assessor_header
    if assessor_assignments.blank? || !assessor.present?
      'Not Confirmed'
    elsif assessor.is_a?(AssessorProfile) && assessor.assessor_for.include?(short_name)
      "Assessor: #{assessor_name}"
    else
      "Gemba: #{assessor_name}"
    end
  end

  def cap_needed_label
    return 'UNDETERMINED RESULT' if result.blank?

    result > target ? 'NEED A CAP' : 'NO CAP NEEDED'
  end
end
