module AssessmentHelper::Constants
  extend ActiveSupport::Concern

  COLORS = {
    'quality_assessment' => '#0198F1', # BLUE
    'opex_assessment' => '#568D77', # GREEN
    'sse_assessment' => '#FECC45', # YELLOW
    'hrp_assessment' => '#54C4C0', # AQUA
    'env_assessment' => '#54C4C0', # AQUA
    'pur_assessment' => '#F2735D', # RED
    'scm_assessment' => '#F2735D', # RED
    'dpr_assessment' => '#333333', # BLACK
    'dpr_textile' => '#333333', # BLACK
    'hfwb_assessment' => '#A5D7D5', # BLACK
    'indus_assessment' => '#333333' # BLACK ASK_LATER
  }.freeze

  SCORES = ['OK', 'NOK', 'NOK without risk', 'Not Applicable', 'Not Assessed'].freeze

  GRIDS = %w[quality_assessment opex_assessment sse_assessment
             hrp_assessment env_assessment pur_assessment scm_assessment].freeze

  ACTIVE_GRIDS = %w[QA HRP SSE PUR ENV OPEX SCM DPR IND].freeze # ASK_LATER what does this do

  SHORT_NAMES = {
    'quality_assessment' => 'QA', 'opex_assessment' => 'OPEX',
    'sse_assessment' => 'SSE', 'hrp_assessment' => 'HRP', 'scm_assessment' => 'SCM',
    'env_assessment' => 'ENV', 'pur_assessment' => 'PUR', 'dpr_assessment' => 'DPR',
    'dpr_textile' => 'DPR TEXT', 'hfwb_assessment' => 'HFWB', 'indus_assessment' => 'IND'
  }.freeze

  SCORE_COLORS = {
    'OK' => 'green', 'NOK' => 'red', 'NOK without risk' => 'yellow',
    'Not Applicable' => 'yellow', 'Not Assessed' => 'yellow'
  }.freeze

  LEVEL_COLORS = {
    'E' => 'black', 'EP' => 'black', 'D' => 'red', 'C' => 'yellow',
    'B' => 'blue', 'A' => 'green'
  }.freeze

  STATUS_COLORS = {
    'Prep Required' => 'red',
    'Awaiting Assessor' => 'yellow',
    'Supplier Not Ready!' => 'red',
    'Assessment Ready!' => 'aqua',
    'Assessment In Progress' => 'green',
    'Chapters Completed' => 'purple',
    'Complete' => 'blue'
  }.freeze

  NEXT_LEVELS = { 'E' => 'D', 'D' => 'C', 'C' => 'B', 'B' => 'A', 'A' => 'A' }.freeze
  LAST_LEVELS = { 'E' => 'E', 'D' => 'E', 'C' => 'D', 'B' => 'C', 'A' => 'B' }.freeze
  SORT_INDEX = { 'A' => 1, 'B' => 2, 'C' => 3, 'D' => 4, 'EP' => 5, 'E' => 6 }.freeze

  BG_COLORS = {
    'quality_assessment' => 'blue', 'opex_assessment' => 'green',
    'sse_assessment' => 'yellow', 'hrp_assessment'  => 'aqua',
    'scm_assessment' => 'red', 'pur_assessment'     => 'red',
    'env_assessment' => 'aqua', 'dpr_assessment'    => 'blue',
    'dpr_textile' => 'blue',
    'hfwb_assessment' => 'blue', 'indus_assessment' => 'blue'
  }.freeze

  LOGOS = {
    'quality_assessment' => '/images/logos/qa_logo.jpg',
    'opex_assessment' => '/images/logos/opex_logo.jpg',
    'sse_assessment' => '/images/logos/sse_logo.jpg',
    'hrp_assessment' => '/images/logos/hrp_logo.jpg',
    'scm_assessment' => '/images/logos/scm_logo.jpg',
    'env_assessment' => '/images/logos/env_logo.jpg',
    'pur_assessment' => '/images/logos/pur_logo.jpg',
    'dpr_assessment' => '/images/logos/dpr_logo.jpg',
    'dpr_textile' => '/images/dpr/textile.png',
    'hfwb_assessment' => '/images/logos/hfwb_logo.jpg',
    'indus_assessment' => '/images/logos/indus_logo.jpg' # ASK_LATER
  }.freeze

  MOTTOS = {
    'quality_assessment' => 'A Systematic Routine to Drive Autonomy',
    'opex_assessment' => 'A Systematic Routine to Drive Autonomy',
    'sse_assessment' => 'Journey to Supply Excellence & Autonomy',
    'hrp_assessment' => 'A Systematic Routine to Drive Autonomy',
    'scm_assessment' => 'A Systematic Routine to Drive Autonomy',
    'env_assessment' => 'A Systematic Routine to Drive Autonomy',
    'pur_assessment' => 'A Systematic Routine to Drive Autonomy',
    'dpr_assessment' => 'A Systematic Routine to Drive Autonomy',
    'dpr_textile' => 'A Systematic Routine to Drive Autonomy',
    'hfwb_assessment' => 'A Systematic Routine to Drive Autonomy'
  }.freeze

  HFWB_PRODUCTS = ['trail_running_bag', 'bags', 'welded_bags', 'pfd_foam',
                   'pfd_welding', 'support', 'flexible_protection', 'boxing_gloves', 'boxing_bag',
                   'horse_riding_textile', 'umbrella', 'fg_net', 'kite_sail', # "webbing","horse_riding_leather",
                   'tents', 'sleeping_bag', 'stitched_balls', 'bladders',
                   'rubber_moulded_balls', 'pvc_rotomoulded_balls', 'laminated_balls', 'thb',
                   'feather_shuttle', 'gluing', 'leather', 'welding'].freeze

  HFWB_PRODUCT_NAMES = ['Trail running bag', 'Bags', 'Welded Bags', 'PFD Foam',
                        'PFD Welding', 'Support', 'Flexible protection', 'Boxing Gloves', 'Boxing bag',
                        'Horse Riding Textile', 'Umbrella', 'FG Net', 'Kite & Sail', # "Webbing", "Horse Riding Leather",
                        'Tents', 'Sleeping bag', 'Stitched balls', 'Bladders',
                        'Rubber Moulded Balls', 'PVC Rotomoulded balls', 'Laminated Balls', 'THB',
                        'Feather Shuttle', 'Gluing', 'Leather', 'Welding'].freeze

  HFWB = HFWB_PRODUCTS.zip(HFWB_PRODUCT_NAMES).to_h.freeze
  HFWB_LOGOS = HFWB_PRODUCTS.zip(HFWB_PRODUCTS.map { |x| "/images/logos/#{x}_logo.jpg" }).to_h.freeze

  HFWB_GRIDS = HFWB_PRODUCTS.map { |x| "HFWB:#{x}" }.freeze

  ASSESSOR_TYPES = {
    'quality_assessment' => ['Gemba E/D Levels', 'Self Assessment For Autonomy', 'Annual', 'CAP Close', 'CAP Review'],
    'opex_assessment' => ['Gemba E/D Levels', 'Self Assessment For Autonomy', 'Annual', 'CAP Close', 'CAP Review'],
    'sse_assessment' => ['Gemba E/D Levels', 'Self Assessment For Autonomy', 'Annual', 'CAP Close', 'CAP Review'],
    'hrp_assessment' => ['Gemba E/D Levels', 'Self Assessment For Autonomy', 'Annual', 'CAP Close', 'CAP Review'],
    'env_assessment' => ['Gemba E/D Levels', 'Self Assessment For Autonomy', 'Annual', 'CAP Close', 'CAP Review'],
    'scm_assessment' => ['Self Assessment', 'Self Assessment Training', 'CAP Close', 'CAP Review'],
    'pur_assessment' => ['Gemba E/D Levels', 'Self Assessment For Autonomy', 'Annual', 'CAP Close', 'CAP Review'],
    'dpr_assessment' => ['Gemba E/D Levels', 'Self Assessment For Autonomy', 'Annual', 'CAP Close', 'CAP Review'],
    'hfwb_assessment' => ['Gemba E/D Levels', 'Self Assessment For Autonomy', 'Annual', 'CAP Close', 'CAP Review'],
    'indus_assessment' => ['Gemba E/D Levels', 'Self Assessment For Autonomy', 'Annual', 'CAP Close', 'CAP Review']
  }.freeze

  # OLD:  ASSESSOR_TYPES = {
  #   'quality_assessment' => ['Gemba', 'Annual', 'CAP Close', 'CAP Review'],
  #   'opex_assessment' => ['Self Assessment', 'Annual', 'CAP Close', 'CAP Review'],
  #   'sse_assessment' => ['Self Assessment', 'Annual', 'CAP Close', 'CAP Review'],
  #   'hrp_assessment' => ['Gemba E/D Levels', 'Self Assessment For Autonomy', 'Annual', 'CAP Close', 'CAP Review'],
  #   'env_assessment' => ['Gemba E/D Levels', 'Self Assessment For Autonomy', 'Annual', 'CAP Close', 'CAP Review'],
  #   'scm_assessment' => ['Self Assessment', 'Self Assessment Training', 'CAP Close', 'CAP Review'],
  #   'pur_assessment' => ['Gemba', 'Annual', 'CAP Close', 'CAP Review'],
  #   'dpr_assessment' => ['Gemba', 'Annual', 'CAP Close', 'CAP Review'],
  #   'hfwb_assessment' => ['Gemba', 'Annual', 'CAP Close', 'CAP Review'],
  #   'indus_assessment' => ['Gemba', 'Annual', 'CAP Close', 'CAP Review']
  # }.freeze

  SUPPLIER_TYPES = {
    'quality_assessment' => ['Gemba', 'CAP Review'],
    'opex_assessment' => ['Self Assessment', 'CAP Review'],
    'sse_assessment' => ['Gemba', 'CAP Review'],
    'hrp_assessment' => ['Gemba E/D Levels', 'Self Assessment For Autonomy', 'CAP Review'],
    'env_assessment' => ['Gemba E/D Levels', 'Self Assessment For Autonomy', 'CAP Review'],
    'scm_assessment' => ['Self Assessment', 'CAP Review'],
    'pur_assessment' => ['Gemba', 'CAP Review'],
    'dpr_assessment' => ['Gemba', 'CAP Review'],
    'hfwb_assessment' => ['Gemba', 'CAP Review'],
    'indus_assessment' => ['Gemba', 'CAP Review']
  }.freeze

  DEFAULT_RESULT = {
    'quality_assessment' => 'Not Applicable',
    'opex_assessment' => 'Not Applicable',
    'sse_assessment' => 'Not Assessed',
    'hrp_assessment' => 'Not Assessed',
    'scm_assessment' => 'Not Applicable',
    'env_assessment' => 'Not Applicable',
    'pur_assessment' => 'Not Applicable',
    'dpr_assessment' => 'Not Applicable',
    'hfwb_assessment' => 'Not Applicable',
    'indus_assessment' => 'Not Applicable'
  }.freeze

  PROCESS_OPTIONS = {
    'quality_assessment' => [],
    'opex_assessment' => [],
    'sse_assessment' => [],
    'hrp_assessment' => [],
    'env_assessment' => [],
    'pur_assessment' => [],
    'scm_assessment' => [],
    'dpr_assessment' => [],
    'dpr_textile' => [],
    'hfwb_assessment' => [],
    'indus_assessment' => %w[textile footwear heavystitching hardware bikes]
  }.freeze
end
