module AssessmentHelper::ScoreBase
  extend ActiveSupport::Concern

  def base_point(pt)
    {
      section_name: pt[:section_name], title: pt[:title], content: pt[:content],
      note_header: pt[:note_header], bullets: pt[:note_bullets] || []
    }
  end

  def base_level(ca, cqs)
    {
      level: ca.level, score: ca.score, cotation: ca.cotation,
      color: score_color[ca.score], questions: {
        count: cqs.select { |x| x.result.in?(%w[OK NOK]) }.count,
        ok_count: cqs.select { |x| x.result == 'OK' }.count
      }, points: []
    }
  end

  def merge_levels(ca, last_ca)
    # MERGE Chapter Questions
    cqs  = ca.chapter_questions
    tags = cqs.map(&:chapter_tag)
    cqs  = cqs.select { |x| valid_result?(x.result, ca.level) }
    cqs += (last_ca || ChapterAssessment.new).chapter_questions
                                             .reject { |x| cqs.map(&:chapter_tag).include?(x.chapter_tag) }
                                             .select { |x| tags.include?(x.chapter_tag) }

    cqs.sort_by!(&:chapter_tag)

    # Create transient object
    merged = ChapterAssessment.new(level: ca.level, quality_assessment_id: ca.quality_assessment_id)

    # CALCULATING TOTALS
    res = merged.calculate_questions(cqs)
    merged.set_score!(res)
    merged.set_cotation!(res)

    # Return Level Hash
    base_level(merged, cqs)
  end

  def base_section(gl)
    {
      section_name: gl[:section_name], title: gl[:title],
      content: gl[:content], level: nil, cotation: nil,
      subscores: []
    }
  end

  def base_chapter(gl)
    {
      section_no: gl[:section_no], section_name: gl[:section_name],
      title: gl[:title], level: nil, cotation: nil, level_color: nil,
      sections: []
    }
  end

  def base_score
    {
      assess_date: assess_date.strftime('%Y-%m-%d'), level: nil,
      cotation: nil, level_color: nil, chapters: []
    }
  end
end
