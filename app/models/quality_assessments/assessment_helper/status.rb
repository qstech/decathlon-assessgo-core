module AssessmentHelper::Status
  extend ActiveSupport::Concern

  def valid_cap_close?
    @is_valid_cap_close ||= (cap_close? && cap.present?)
  end

  def cap_close?
    assess_type == 'CAP Close'
  end

  def cap_review?
    assess_type == 'CAP Review'
  end

  def cap?
    cap_close? || cap_review?
  end

  def annual?
    assess_type == 'Annual'
  end

  def gemba?
    assess_type == 'Gemba'
  end

  def self_assessment_training?
    assess_type == 'Self Assessment Training'
  end

  def official_assessment?
    annual? || cap_close? || self_assessment_training?
  end

  def hrp?
    grid == 'hrp_assessment'
  end

  def chapter_percent
    return 0 if chapter_questions.count.zero?

    incomplete = chapter_questions.where.not(result: nil).count
    (incomplete.to_f / chapter_questions.count * 100).ceil(0).to_i
  end

  def cap_review_status
    return nil unless cap_review?

    if user.supplier? && cap.cap_points.unresolved.blank?
      'Closed'
    elsif cap_cycle_closed?
      "Closed: #{related_cap_close.assessment.result}"
    elsif cap.cap_points.unresolved.blank?
      'Close in progress'
    else
      'In progress'
    end
  end

  def cap_cycle_closed?
    return false if related_cap_close.nil?

    related_cap_close.quality_assessment.completed?
  end

  def related_cap_close
    @related_cap_close ||= if cap_close?
                             cap
                           elsif cap_review?
                             Cap.find_by(cap_type: 'CAP Close', ref_model: cap.ref_model, ref_id: cap.ref_id)
                           else
                             Cap.find_by(cap_type: 'CAP Close', ref_model: 'quality_assessment', ref_id: id)
                           end
  end

  def verify_complete
    set_score! if completed
  end

  def verify_chapters
    self.chapters_completed = chapter_assessments.where(score: nil).blank?
    set_score! if chapters_completed
  end

  def verify_day_form
    self.day_form_completed = assessment_day_form.complete? if assessment_day_form.present?
  end

  def verify_prep_status
    self.prep_completed = prep_complete?
  end

  def verify_assessor
    self.assessor_confirmed = assessor_confirmed?
  end

  def assess_today?
    Time.now.to_date == assess_date.to_date
  end

  def assessor_confirmed?
    assessor_assignments.where(confirmed: true).present?
  end

  def assessor_unconfirmed?
    assessor_assignments.where(confirmed: true).blank?
  end

  def prep_incomplete?
    prep_questions.where(response: nil).present? && prep_status_all[:status] != '✓ Prep submitted'
  end

  def prep_complete?
    prep_questions.where(response: nil, respondant: 'assessor').blank? && prep_status_all[:status] == '✓ Prep submitted'
  end

  def day_form_complete?
    return false if assessment_day_form.nil?

    assessment_day_form.complete?
  end

  def chapters_complete?
    chapter_questions.where(result: nil).blank?
  end

  def ready_for_qa?
    # add prep_optional?
    (prep_completed || prep_optional?) && assessor_confirmed
  end

  def completed?
    completed
  end

  def incomplete?
    !completed
  end

  def cap_finished?
    cap_points.where(resolved_at: nil).blank?
  end

  def chapter_assessments_finished?
    chapter_questions.where(result: nil).blank?
  end

  def prep_status_all
    prep_status_for('assessor')
  end

  def prep_status_supplier
    prep_status_for('supplier')
  end

  def prep_status_for(respondant)
    return { status: '✓ No Questions Needed', color: 'green', icon: '✓', tag: 'no_questions' } if prep_questions.blank?

    question_set = prep_questions.map { |x| x.question_tag }
    required = FormQuestion.where(question_tag: question_set, required: true, destroyed_at: nil).map do |fq|
      fq.question_tag
    end
    arr = prep_questions.where(question_tag: required, respondant: respondant)
    if prep_questions.where(response: [nil, ''], respondant: respondant).present?
      { status: '✖︎ Incomplete', color: 'red', icon: '✖︎', tag: 'incomplete' }
    elsif arr.map { |x| x.response }.include?('NOK')
      { status: 'Prep critical points not fullfilled', color: 'red', icon: '✖︎', tag: 'critical_unfulfilled' }
    else
      { status: '✓ Prep submitted', color: 'green', icon: '✓', tag: 'submitted' }
    end
  end

  def basic_status
    assessor = primary_assessor
    {
      status: status,
      color: status_color[status],
      date: assess_date.strftime('%Y-%m-%d'),
      name: (assessor.present? ? assessor.email : 'Assessor Unconfirmed')
    }
  end
end
