module AssessmentHelper::Radars
  extend ActiveSupport::Concern
  def radar_set
    return {} unless completed

    if dpr?
      score_set = score_calc
      radars_for_dpr(score_set)
    elsif assess_type != 'CAP Review'
      score_set = score_calc
      radars_for_score_set(score_set)
    else
      radars_for_cap
    end
  end

  def radars_for_score_set(score_set)
    radars = { total: { labels: [], points: [], cotations: [] }, chapters: [] }

    # For showing diff of results from previous assessment.
    # Ended up ugly, commenting out for now.

    # if cap_id.present?
    #   last_score = cap.ref.score_calc
    #   radars[:total][:last_points] = []
    #   radars[:total][:last_cotations] = []
    # end

    score_set[:chapters].each do |ch|
      next if ch.blank?

      ch_radar = radars_for_chapter(ch)

      radars[:total][:labels] += ch_radar[:labels]
      radars[:total][:points] += ch_radar[:points]
      radars[:total][:cotations] += ch_radar[:cotations]

      # For showing diff of results from previous assessment.
      # Ended up ugly, commenting out for now.

      # if cap_id
      #   last_ch = last_score[:chapters].find{|x| x[:section_name] == ch[:section_name]}
      #   last_radar = radars_for_chapter(last_ch)

      #   radars[:total][:last_points] += last_radar[:points]
      #   radars[:total][:last_cotations] += last_radar[:cotations]
      # end

      radars[:chapters] << ch_radar
    end
    radars
  end

  def radars_for_dpr(_score_set)
    conversion = { 'E' => 0, 'EP' => 0, 'D' => 25, 'C' => 50, 'B' => 75, 'A' => 100, 'N/A' => 100 }
    radars = { total: { labels: [], points: [], cotations: [] }, chapters: [] }
    cqs = chapter_questions.order(:chapter_assessment_id)
    radars[:total][:labels] = cqs.map { |x| x.chapter_tag.gsub(/[A-Z]/, '.').gsub(/\D*(\d+\.)000\.(\d+)/, '\1\2') }
    gls = DprGuideline.where(guideline_tag: cqs.map(&:chapter_tag)).group_by { |x| x.guideline_tag }

    radars[:total][:points] = cqs.map do |x|
      gl = gls[x.chapter_tag].first
      next(conversion[gl.from_level]) if x.result == 'NOK'
      next(conversion[gl.to_level]) if x.result == 'OK'

      nil
    end

    radars[:total][:cotations] = chapter_assessments.order(:id).map { |x| x.cotation }

    radars[:total][:points].map! { |x| x || 0 }
    radars[:total][:cotations].map! { |x| x || 0 }

    radars[:chapters] = radars_for_dpr_chapters(radars)

    radars
  end

  def radars_for_dpr_chapters(radars)
    keys = {}
    radars[:total][:labels].each_with_index do |tag, i|
      keys[tag] = i
    end

    chapters = radars[:total][:labels].group_by { |x| x.split('.').first }.values
    chapters.map do |arr|
      res = { labels: [], points: [], cotations: [], name: "Ch #{arr.first.split('.').first}" }
      arr.each_with_index do |x, _i|
        j = keys[x]
        res[:labels] << radars[:total][:labels][j]
        res[:points] << radars[:total][:points][j]
        res[:cotations] << radars[:total][:cotations][j]
      end
      res
    end
  end

  def radars_for_chapter(ch)
    conversion = { 'E' => 0, 'EP' => 0, 'D' => 25, 'C' => 50, 'B' => 75, 'A' => 100, 'N/A' => 100 }
    ch_radar = { labels: [], points: [], cotations: [], name: "#{ch[:section_name]} - #{ch[:title]}" }
    ch[:sections].each do |sec|
      next if sec.blank?

      full_set = ('A'..'E').to_a + ['EP', 'N/A', nil]
      ch_radar[:labels] << sec[:section_name]

      if full_set.include?(sec[:level])
        puts 'something good'
        p ch_radar[:labels]

        ch_radar[:points] << conversion[sec[:level] || 'N/A']
      else
        i = sec[:level].split('.')[0].to_i - 1
        lev = ('A'..'E').to_a.reverse[i]
        ch_radar[:points] << conversion[lev]
      end
      sec[:cotation] = 100 if sec[:level] == 'N/A'
      ch_radar[:cotations] << sec[:cotation].to_i
    end
    ch_radar
  end

  def radars_for_cap
    radars = { total: { labels: [], points: [], cotations: [] }, chapters: [] }
    return radars if cap.blank?

    points_by_sec = cap.cap_points.group_by { |x| x.section }

    points_by_sec.each do |sec, pts|
      next if sec.blank?

      sum = pts.map { |x| convert_cap_point(x) }.sum
      res = pts.length == 0 ? 0 : sum / pts.length

      radars[:total][:labels] << sec
      radars[:total][:points] << res
    end
    radars
  end

  def convert_cap_point(pt)
    return 100 if pt.resolved_at.present?
    return 0 if pt.cap_resolutions.blank?

    50
  end
end
