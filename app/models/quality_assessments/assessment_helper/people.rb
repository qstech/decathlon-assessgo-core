module AssessmentHelper::People
  extend ActiveSupport::Concern
  def audience_for(audience)
    single = %i[assessor creator supplier referent candidate]
    multi  = %i[supplier_pls supplier_opms supplier_ptms]

    return [public_send(audience)].reject(&:nil?) if audience.in?(single)
    return public_send(audience) if audience.in?(multi)
    return User.where(username: %w[kkang20 wxu50]) if audience == :admins

    raise StandardError, "AUDIENCE NOT VALID: #{audience}"
  end

  def opmsd_user
    return nil if opmsd.in?([nil, 0])

    @opmsd_user ||= fetch_opmsd
  end

  def primary_assessor
    @primary_assessor ||= fetch_primary_assessor
  end

  def assessors
    @assessors ||= fetch_assessors
  end

  def assessor
    @assessor ||= fetch_assessor
  end

  def referent
    @referent ||= fetch_referent
  end

  def candidate
    @candidate ||= fetch_candidate
  end

  def users_concerned
    @users_concerned ||= fetch_users_concerned
  end

  def supplier
    @supplier ||= fetch_supplier
  end

  def supplier_pls
    @supplier_pls ||= fetch_assessor_users(supplier_profile.pl_set)
  end

  def supplier_opms
    @supplier_opms ||= fetch_assessor_users(supplier_profile.opm_set)
  end

  def supplier_ptms
    @supplier_ptms ||= fetch_assessor_users(supplier_profile.ptm_set)
  end

  def assessor_name
    return assessor.name if assessor.is_a? AssessorProfile
    return assessor.username if assessor.is_a? User
  end

  def assessor_name_noquery
    (fetch_assessor_noquery || AssessorProfile.new).name
  end

  def supplier_name
    "#{supplier_profile.name} (#{supplier_profile.code})"
  end

  def referent_name
    return nil if referent.nil?

    referent.name
  end

  def supplier_users
    return [] if supplier_profile.in?([0, nil])

    supplier_profile.all_users.map { |x| { id: "s#{x.id}", name: x.username, candidate: false, assessor: false } }
  end

  def adapter_source
    AssessorProfile.source_list(permitted_grid) + supplier_users
  end

  # ===========================END PUBLIC=======================================
  private

  def fetch_assessor_noquery
    ass = assessor_assignments.sort_by { |x| x.primary ? 0 : 1 }.map(&:user).first
    ass || user
  end

  def fetch_referent
    User.includes(:assessor_assignments, :assessor_profile)
        .where(assessor_assignments: { ref_model: model_name.singular,
                                       ref_id: id }).select { |x| x.referent_for?(short_name) }.first
  end

  def fetch_candidate
    User.includes(:assessor_assignments, :assessor_profile)
        .where(assessor_assignments: { ref_model: model_name.singular,
                                       ref_id: id }).select { |x| x.candidate_for?(short_name) }.first
  end

  def fetch_primary_assessor
    User.includes(:assessor_assignments)
        .find_by(assessor_assignments: { ref_model: model_name.singular,
                                         ref_id: id, primary: true }) || assessors.first
  end

  def fetch_assessor
    return nil if assessors.blank?

    ass = primary_assessor || assessors.first

    return user if ass.nil?
    return ass.assessor_profile if ass.assessor_profile.present?

    user
  end

  def fetch_assessors
    base = User.includes(:assessor_assignments, :supplier_profile, :assessor_profile)

    base.where(assessor_assignments: { ref_model: 'quality_assessment', ref_id: id })
        .or(base.where(id: created_by)).or(base.where(assessor_profiles: { id: people_concerned }))
  end

  def fetch_opmsd
    User.includes(:assessor_profile).find_by(assessor_profiles: { id: opmsd })
  end

  def fetch_users_concerned
    User.where(id: concerned_user_ids)
  end

  def fetch_assessor_users(set)
    User.includes(:assessor_profile).where(assessor_profiles: { id: set })
  end

  def fetch_supplier
    User.find_by(id: supplier_profile.user_id)
  end

  def concerned_user_ids
    assign_ids = assessor_assignments.map(&:user_id)
    peeps = AssessorProfile.where(id: people_concerned).map(&:user_id)
    user_set = [created_by, assign_ids, peeps, supplier_profile.user_id,
                supplier_profile.user_ids]
    user_set.flatten.reject(&:blank?)
  end
end
