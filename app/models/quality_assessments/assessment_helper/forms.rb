module AssessmentHelper::Forms
  extend ActiveSupport::Concern

  def edit_form(form_h)
    c_set = chapter_set
    p_set = dpr? ? process_set : []

    form_h[:buttonShow] = true
    form_h[:object] = serializable_hash
    form_h[:object]['assess_date'] = assess_date.strftime('%Y-%m-%d')
    form_h[:supplierInfo] = supplier_profile.serializable_hash
    form_h[:found] = true
    form_h[:chapter_set] = c_set
    form_h[:grid] = grid

    form_h[:chapters].each do |ch|
      ch[:subchapters].each do |sc|
        sc[:checked] = c_set.include?(sc[:chapter_id])
      end
    end

    if dpr?
      form_h[:processes].each do |pr|
        pr[:subchapters].each do |sc|
          sc[:checked] = p_set.include?(sc[:chapter_id])
        end
      end
    end
    form_h
  end

  def new_form(user, native: false)
    chapters = format_chapters(guidelines)
    source_list = adapter_source

    {
      assessorSource: source_list,
      referentSource: AssessorProfile.referent_list[permitted_grid],
      supplierSource: SupplierProfile.source_list(:qa, permitted_grid),
      supplierInfo: {},
      grid: grid,
      gridColor: bg_color,
      openCaps: QualityAssessment.all_open_caps(grid),
      chapters: chapters,
      processes: process_list(native: native),
      from_level: %w[E D C B A],
      to_level: %w[C B A],
      types: assess_types_for(user.role),
      show_nutrition: grid == 'quality_assessment',
      must_invite_assessor: user.must_invite_assessor?(permitted_grid),
      process_options: process_options
    }
  end

  def generate_cap_prep
    cqs = (all_nok_points + all_low_points).sort_by(&:sorting_chapter_tag) # REMINDER here need to change logic to take cqs with cq.result > target
    form = {
      ref_model: 'quality_assessment', ref_id: id, points: [], recheck: [],
      pl_id: assessment_day_form.pl_id,
      supplier_profile_id: supplier_profile_id,
      redirect: "/#{mp_path(:index)}"
    }
    form[:points] = cqs.map do |cq|
      dpr? ? cq.cap_point_h(:dpr) : cq.cap_point_h(:normal)
    end
    form
  end

  def prep_questions_form(respondant = 'assessor')
    @fq = { form: { active: true }, can_copy: true }
    pqs = prep_questions.with_attached_attachments
    question_set = pqs.map(&:question_tag)
    form_questions = FormQuestion.includes(:form_options)
    .where(question_tag: question_set, respondant: respondant, grid_version_id: grid_version_id).order(:priority)

    @fq[:form][:form_questions] = form_questions.reject { |x| x.label.blank? }.map do |x|
      pq = pqs.select { |pq| pq.question_tag == x.question_tag }.first
      format_question(x, pq)
    end

    missing_last = @fq[:form][:form_questions].map { |x| x[:lastPrep] }.include?(nil)

    @fq[:can_copy] = false if missing_last && !valid_cap_close?
    @fq
  end

  def survey_questions_form
    form_h = { form: { active: true } }
    pqs = survey_questions.with_attached_attachments
    question_set = pqs.map(&:question_tag)
    form_h
  end
  #     copy_available = false if last_pq.nil? && !valid_cap_close?

  def go_for_assessment_form
    fq = { form: { active: true, form_questions: [] } }
    go_for = assessment_day_form
    go_h = go_for.to_h
    go_for.labels.each do |k, v|
      next if go_h[k].blank?

      fq[:form][:form_questions] << { label: v, response: go_h[k] }
    end
    fq
  end

  def all_levels
    valid_result = "chapter_assessments.score IN ('OK', 'NOK', 'NOK without risk')"
    ChapterAssessment.includes(:chapter_questions)
                     .select('distinct on (chapter_tag) chapter_assessments.*')
                     .joins(:quality_assessment)
                     .where(quality_assessments: { id: parent_and_self_assessment_ids })
                     .order(:chapter_tag)
                     .order("(CASE WHEN #{valid_result} THEN 'A' ELSE 'B' END) ASC")
                     .order(id: :desc)
  end

  def all_points
    valid_result = "chapter_questions.result IN ('OK', 'NOK', 'NOK without risk')"
    ChapterQuestion.select('distinct on (chapter_tag) chapter_questions.*')
                   .joins(chapter_assessment: :quality_assessment)
                   .where(quality_assessments: { id: parent_and_self_assessment_ids })
                   .order(:chapter_tag)
                   .order("(CASE WHEN #{valid_result} THEN 'A' ELSE 'B' END) ASC")
                   .order(id: :desc)
  end

  def all_nok_points
    # POSTGRES ONLY: distinct on (column) ....
    ChapterQuestion.select('distinct on (chapter_tag) chapter_questions.*')
                   .joins(chapter_assessment: :quality_assessment)
                   .where(quality_assessments: { id: parent_and_self_assessment_ids })
                   .where(result: ChapterQuestion::NOK_TYPE)
                   .where('chapter_questions.level > ?', target)
                   .order(:chapter_tag, id: :desc)
  end

  def all_low_points
    ChapterQuestion.select('distinct on (chapter_tag) chapter_questions.*')
                   .joins(chapter_assessment: :quality_assessment)
                   .where(quality_assessments: { id: parent_and_self_assessment_ids })
                   .where(result: ("A"..."E").to_a)
                   .where('chapter_questions.result > ?', target)
                   .order(:chapter_tag, id: :desc)
  end

  private

  def format_chapters(gls)
    gls.map do |ch|
      # For some reason, can't use attribute calling, so I switched it to symbol.

      formatted = { title: "#{ch[:section_name]} #{ch[:title]}",
                    button: I18n.t('api.v1.quality_assessments.new.check_all'), buttonShow: true }
      formatted[:subchapters] = ch[:children].sort_by { |sc| sc[:section_name] }.map do |sc|
        h = { chapter_id: sc[:section_name], value: sc[:title], checked: false, disabled: false, process: sc[:process] == 0 ? 'all' : sc[:process] }
        h
      end
      formatted
    end
  end

  def format_question(question, pq)
    h = question.to_h
    h.merge!(pq.form)
    last_pq = PrepQuestion.last_prep_for(self, h)
    h[:lastPrep] = last_pq.standard_hash if last_pq.present?
    h
  end
end
