module AssessmentHelper::Score
  extend ActiveSupport::Concern
  include AssessmentHelper::ScoreBase

  SCORING = {
    'hrp_assessment' => { 'B' => 100, 'C' => :no_nok, 'D' => :no_nok, 'E' => :no_nok },
    'quality_assessment' => { 'B' => 100, 'C' => 100, 'D' => 100, 'E' => 100 },
    'opex_assessment' => { 'B' => 100, 'C' => 100, 'D' => 100, 'E' => 100 },
    'sse_assessment' => { 'B' => 100, 'C' => 100, 'D' => 100, 'E' => 100 },
    'env_assessment' => { 'B' => 100, 'C' => 100, 'D' => 100, 'E' => 100 },
    'scm_assessment' => { 'B' => 100, 'C' => 100, 'D' => 100, 'E' => 100 },
    'pur_assessment' => { 'B' => 100, 'C' => 100, 'D' => 100, 'E' => 100 },
    'dpr_assessment' => { 'B' => 80, 'C' => 80, 'D' => 100, 'E' => 100 },
    'hfwb_assessment' => { 'B' => 100, 'C' => 100, 'D' => 100, 'E' => 100 },
    'indus_assessment' => { 'B' => 100, 'C' => 100, 'D' => 100, 'E' => 100 }
  }

  ALGORITHMS = {
    'hrp_assessment' => :limit_level,
    'quality_assessment' => :limit_level,
    'opex_assessment' => :limit_level,
    'sse_assessment' => :limit_level,
    'env_assessment' => :limit_level,
    'scm_assessment' => :limit_level,
    'pur_assessment' => :cotation_level,
    'dpr_assessment' => :limit_level,
    'hfwb_assessment' => :limit_level
  }
  # Files Dependent on this Module:
  # ==> ./radars.rb
  # ==> ~/app/models/users/supplier_profile.rb #last_qa_result
  # ==> ~/app/models/utility/pdf_generator.rb

  # Routes Dependent on this Module:
  # get "members/assessment/report" => "quality_assessments#report" {For RADAR generation}

  # =======================ALGORITHM======================================
  # The following mapping shows the heirarchy of methods in the score calculation
  # The Base Hash for each level is included in ./score_base.rb
  #
  # score_calc (creates nested hash of ALL results, includes data in hash)
  # |--result_cotation
  # |--result_total
  # |--calc_total_chapters
  #    |--calc_chapter
  #       |--cotation_chapter
  #       |--result_chapter
  #       |--calc_chapter_sections
  #          |--calc_section
  #             |--cotation_section
  #             |--result_section
  #             |--calc_section_levels
  #                |--calc_level
  #                   |--calc_point

  # section_score (Quick calculation of section score, e.g. QACH1S2)
  # |--calc_section
  #    |--{same as above}

  # =======================NEXT STEPS=====================================

  # Separation of concerns, smaller methods responsible for smaller calculations
  # GOAL: separate into many sub-methods so that more minimalistic calculations
  # can be done for modules that don't need such comprehensive information.
  # --->  Radar Generation only needs the OK/NOK, Question Count Data, etc.
  # --->  Can calculation of total score be optimized so that it doesn't take so
  #       long for larger assessments?
  # --->  Can the following Model/Controller Actions incorporate the same methods if optimized?
  #       - CONTROLLER: api/v1/quality_assessments#single_chapter
  #       - MODEL: utility/page_component#self.qa_chapter_set(qa)
  # ===========================================================================
  # ===========================================================================
  # ===========================================================================

  # ===========CALC FOR ASSESSMENT=========
  def scoring_rules
    product_grid.nil? ? SCORING[grid] : SCORING[product_grid]
  end

  def score_summary!(h = {})
    h = score_calc if h.blank?
    clean_score(h)
  end

  def clean_score(h = {}, reqs = %i[level result cotation score count ok_count])
    h = h.reject { |k, v| ![Hash, Array].include?(v.class) && (!reqs.include? k) }
    h.each do |k, v|
      h[k] = clean_score(v) if v.is_a? Hash
      if v.is_a? Array
        v.select! { |x| x.is_a? Hash }
        v.map! { |x| clean_score(x) }
      end
    end
    h.reject { |_k, v| v.blank? }
  end

  def print_cotations(h = {})
    puts "TOTAL:  #{h[:cotation]}"
    puts '========================'
    puts '     CHAPTERS'
    h[:chapters].each_with_index do |ch, i|
      puts "CHAPTER #{i + 1}: #{ch[:cotation]} #{ch[:level]}"
      ch[:sections].each_with_index do |sec, j|
        puts "---> #{i + 1}.#{j + 1}: #{sec[:cotation]} #{ch[:level]}"
        sec[:subscores].each_with_index do |lev, _k|
          puts "/////// #{lev[:level]}: #{lev[:cotation]}"
        end
      end
      puts '==================='
    end
  end

  def score_calc_full
    @full_score_needed = true
    score_calc(true)
  end

  def score_calc(fresh_query = true)
    # SETUP BASE VARIABLES
    result = base_score
    chapters = calc_total_chapters(fresh_query)

    # ONLY KEEP CHAPTERS w/ SECTIONS PRESENT
    result[:chapters] = chapters.select { |x| x[:sections].present? }
    result[:cotation] = result_cotation(result)

    algorithm = ALGORITHMS[grid] || :limit_level
    result[:level] = result_total(result, algorithm)
    result[:level_color] = level_color[result[:level]]

    result
  end

  def calc_total_chapters(fresh_query = true)
    cas = if fresh_query
      chapter_assessments.where(destroyed_at: nil).includes(:chapter_questions)
      .order(level: :desc).order(section: :asc)
    else
      chapter_assessments.reject(&:destroyed_at).sort_by(&:level).reverse.sort_by(&:section)
    end

    set_last_cas if cap_close? && cap_id.present? && @full_score_needed
    @more_tags = @last_cas ? @last_cas.group_by(&:section).keys : []
    @sec_tags = cas.group_by(&:section).keys
    chapters = guidelines
    # CALC SCORE FOR CHAPTERS
    chapters.map! do |gl|
      gl[:children] = if dpr?
        filter_sections(gl[:children], :title)
      else
        filter_sections(gl[:children], :guideline_tag)
      end
      next({}) if gl[:children].blank?

      calc_chapter(gl, cas)
    end
  end

  def result_cotation(result)
    countable = valid_chapters(result)
    return nil unless countable.count > 0

    cot_sum = countable.map { |x| x[:cotation].to_f }.sum
    (cot_sum / countable.count).ceil(0)
  end

  def result_total(result, algorithm = :limit_level)
    if algorithm == :limit_level
      countable = valid_chapters(result)
      return nil unless countable.count > 0

      countable.map! { |x| x[:level] }
      countable.sort_by! { |x| QualityAssessment::SORT_INDEX[x] }
      countable.last
    elsif algorithm == :cotation_level
      return nil if result[:cotation].nil?

      c = result[:cotation]

      if    c > 90 then 'A'
      elsif c > 70 then 'B'
      elsif c > 40 then 'C'
      elsif c > 20 then 'D'
      else              'E'
      end

    end
  end

  # ============CALC FOR CHAPTER===========
  def calc_chapter(gl, cas)
    ch = base_chapter(gl)

    # GET SECTIONS
    ch[:sections] = calc_chapter_sections(gl, cas)

    # CALC COTATION
    ch[:cotation] = cotation_chapter(ch)

    # CALC LEVEL
    ch[:level] = result_chapter(ch)

    ch
  end

  def calc_chapter_sections(gl, cas)
    gl[:children].map { |sec| calc_section(sec, cas) }
  end

  def cotation_chapter(ch)
    # ch => Detailed Guideline Hash w/ Section Hashes
    countable = ch[:sections].select { |x| x[:level] != 'N/A' }.reject { |x| x[:cotation].nil? }
    return nil if countable.count == 0

    result = (countable.map { |x| x[:cotation].to_f }.sum / countable.count) if countable.count > 0
    result.ceil(0)
  end

  def result_chapter(ch)
    # ch => Detailed Guideline Hash w/ Section Hashes & w/ Chapter Cotation
    # MUST USE ONLY AFTER COTATION CALCULATED
    sections_present = present_sections(ch)

    if sections_present.blank?
      'N/A'
    else
      sections_present.max_by { |x| QualityAssessment::SORT_INDEX[x] }
    end
  end

  # ===========CALC FOR SECTION============

  def calc_section(gl, cas)
    # SECTION => 1.2
    # gl => Guideline hash
    # cas => ChapterAssessment array

    # Setup Section Before Calculation

    section = base_section(gl)
    section[:subscores] = calc_section_levels(gl, cas)

    # Choose Algorithm for Section Score
    algorithm = :next_level
    algorithm = :current_level if ['pur_assessment'].include?(grid)

    # Calculation Section Score & Cotation
    section[:level] = result_section(section, algorithm)
    section[:cotation] = cotation_section(section)

    section
  end

  def calc_section_levels(gl, cas)
    subscores = []

    if dpr?
      cs = cas.select { |x| x.section == gl[:title] }

      if @last_cas.present?
        cs += @last_cas.select do |x|
          (x.section == gl[:title]) && !cs.map(&:chapter_tag).include?(x.chapter_tag)
        end
        cs = cs.sort_by(&:level).reverse
      end
    else
      cs = cas.select { |x| x.section == gl[:guideline_tag] }

      if @last_cas.present?

        cass = @last_cas.select do |x|
          condition1 = (x.section == gl[:guideline_tag])
          condition2 = !cs.map(&:chapter_tag).include?(x.chapter_tag)

          condition1 && condition2
        end
        h = {}
        cass.each do |x|
          if x1 = h[x.chapter_tag]
            x_is_newer = x.id > x1.id
            x_is_valid = x.score != 'Not Applicable'

            h[x.chapter_tag] = x if x_is_newer && x_is_valid
          elsif h[x.chapter_tag].blank?
            h[x.chapter_tag] = x
          end
        end
        cs += h.values
        cs = cs.sort_by(&:level).reverse

        unless @full_score_needed
          cs.map! do |x|
            if x.score.in?(['OK', 'NOK', 'NOK without risk'])
              x
            else
              @last_cas.select { |y| x.chapter_tag == y.chapter_tag }
              .select { |y| y.score != 'Not Applicable' }.last || x
            end
          end
        end
      end
    end
    cs.each do |ca|
      lvl_gl = gl[:children].find { |x| x[:guideline_tag] == ca.chapter_tag }
      next if lvl_gl.nil?

      sub_score = calc_level(lvl_gl, ca)
      if dpr?
        sub_score[:from] = lvl_gl[:from]
        sub_score[:to]   = lvl_gl[:to]
      end
      subscores << sub_score
    end

    subscores
  end

  def cotation_section(section)
    # sec => Detailed section Hash w/ level Hashes
    ad = assessment_day_form
    countable = section[:subscores].reject do |x|
      x[:cotation].nil? || x[:score].in?([nil, 'Not Applicable', 'N/A',
                                          'Not Assessed']) || ((ad.supplier_target || 'A') > x[:level])
    end

    if countable.count == 0
      nil
    else
      result = (countable.map { |x| x[:cotation].to_f }.sum / countable.count) if countable.count > 0
      result.ceil(0)
    end
  end

  def result_section(section, algorithm = :next_level)
    countable = section[:subscores].sort_by { |x| x[:level] }.reverse

    break_tot = false
    level = 'N/A'
    countable.each_with_index do |sub, i|
      next if na_result?(sub[:score], sub[:level])
      next if break_tot

      next_lev = countable[i + 1]
      break_tot = true if stop_result?(sub[:score], sub[:level])
      level = result_section_algorithm(sub[:score], sub[:level], next_lev, algorithm)
    end
    level
  end

  def valid_result?(score, level)
    if scoring_rules[level] == :no_nok
      score.in?(['OK', 'NOK', 'NOK without risk', 'NOK (EP)', 'Not Applicable', 'Not Assessed'])
    else
      score.in?(['OK', 'NOK', 'NOK without risk', 'NOK (EP)'])
    end
  end

  def na_result?(score, level)
    if scoring_rules[level] == :no_nok
      score.in?([nil])
    else
      score.in?([nil, 'Not Applicable', 'N/A', 'Not Assessed'])
    end
  end

  def stop_result?(score, level)
    if scoring_rules[level] == :no_nok
      score.in?(['NOK', 'NOK (EP)'])
    else
      score.in?(['NOK', 'Not Audited', 'Not Assessed', 'NOK (EP)', 'Not Applicable'])
    end
  end

  def good_result?(score, level)
    if scoring_rules[level] == :no_nok
      score.in?(['OK', 'NOK without risk', 'Not Applicable', 'Not Assessed'])
    else
      score.in?(['OK', 'NOK without risk'])
    end
  end

  def result_section_algorithm(score, level, next_lev, algorithm = :next_level)
    if algorithm == :next_level && good_result?(score, level)
      next_lev.nil? ? 'A' : next_lev[:level]
    elsif algorithm == :current_level && stop_result?(score, level)
      last_level(level)
    elsif ['NOK (EP)'].include?(score)
      'EP'
    else # i.e. next_level NOK, current_level OK
      level
    end
  end

  # ===========CALC FOR LEVEL============

  def calc_level(lvl_gl, ca)
    # LEVEL => 1.2 E
    # lvl_gl => guidline hash for LEVEL
    # ca => ChapterAssessment item for LEVEL

    # Including images in calc section, as it speeds up PDF generation
    cqs = ca.chapter_questions
    tags = cqs.map(&:chapter_tag)
    last_cas = (@last_cas || []).select { |x| x.chapter_tag == ca.chapter_tag }
    last_ca = last_cas.last
    sub_score = base_level(ca, cqs)

    if last_ca
      cqs = cqs.select(&:countable?)
      if @full_score_needed
        last_cas.each do |c|
          cqs += c.chapter_questions.select(&:countable?).select { |x| tags.include?(x.chapter_tag) }
          cqs.uniq!
        end
      else
        last_ca = last_cas.select(&:countable?).last

        cqs += (last_ca || ChapterAssessment.new).chapter_questions
        .select { |x| tags.include?(x.chapter_tag) }
        .reject { |x| cqs.map(&:chapter_tag).include?(x.chapter_tag) }
      end
      cqs.sort_by!(&:chapter_tag)

      sub_score = merge_levels(ca, last_ca)
    end

    if dpr?
      cq = cqs.first

      point = calc_point(lvl_gl, cq)
      result = ca.result

      sub_score[:points] << point
      sub_score[:level] = 'N/A'
      sub_score[:level] = lvl_gl[:from] if result == 'NOK'
      sub_score[:level] = lvl_gl[:to] if result == 'OK'
    else
      lvl_gl[:children].each do |pt|
        cq = cqs.select { |x| x.chapter_tag == pt[:guideline_tag] }.sort_by(&:id)

        next if cq.blank?

        cq.each do |c|
          point = calc_point(pt, c)
          point[:last_assessment] = true if c.chapter_assessment_id != ca.id
          point[:last_assessment] = true if @last_cas.present? && @last_cas.include?(ca)
          sub_score[:points] << point
        end
      end
    end
    sub_score[:level] = sub_score[:score] if sub_score[:level].in? %w[Y X]
    sub_score[:level] = assert_standard_level(sub_score[:level]) unless  sub_score[:level].nil?
    sub_score
  end

  # ===========CALC FOR POINT============

  def calc_point(pt, cq)
    # POINT => 1.2 E-1
    # pt => guideline hash for POINT
    # cq => ChapterQuestion item for POINT
    point = base_point(pt)
    point[:data] = cq.standard_hash
    point
  end

  # ===========HELPER METHODS============

  def section_scores
    cas = chapter_assessments.where(destroyed_at: nil).group_by(&:section)
    cas.map do |section_name, levels|
      gl = Guideline.find_by(guideline_tag: section_name, grid_version_id: grid_version_id).to_h
      score = calc_section(gl, levels)
      reqs = %i[level result cotation score strong_points points_to_improve]
      [section_name, clean_score(score, reqs)]
    end.to_h
  end

  def section_score(section_name: nil, guideline: nil, chapters: nil)
    gl = guideline || Guideline.find_by(guideline_tag: section_name, grid_version_id: grid_version_id).to_h
    cas = chapters || chapter_assessments.where(destroyed_at: nil).where(section: section_name)
    return '' if gl.blank? || cas.blank?

    calc_section(gl, cas)
  end

  def present_sections(ch)
    # For SCM Compatibility.
    # Returns list of converted levels to standard A-E Format

    # ch => Detailed Guideline Hash w/ Section Hashes

    ch[:sections].reject { |x| x[:level] == 'N/A' }.map { |x| assert_standard_level(x[:level]) }
  end

  def set_last_cas
    cap_refs = parent_assessment_ids

    @last_cas = ChapterAssessment.includes(:quality_assessment, :chapter_questions)
    .where(destroyed_at: nil, quality_assessments: { id: cap_refs })
    .order(:id, level: :desc, section: :asc)
  end

  def filter_sections(sections, key)
    sections.select { |x| @sec_tags.include?(x[key]) || @more_tags.include?(x[key]) }
  end

  def valid_chapters(result)
    result[:chapters].reject { |x| ['N/A', nil].include?(x[:level]) }
  end
end
