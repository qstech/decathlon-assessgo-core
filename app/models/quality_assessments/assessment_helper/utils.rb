module AssessmentHelper::Utils
  extend ActiveSupport::Concern

  def default_result
    AssessmentHelper::Constants::DEFAULT_RESULT[grid]
  end

  def feature_name
    return 'DPR' if product.present?

    short_name
  end

  def grid_short_name
    if product.nil?
      AssessmentHelper::Constants::SHORT_NAMES[grid] || 'QA'
    else
      'HFWB'
    end
  end

  def short_name
    if product.nil?
      AssessmentHelper::Constants::SHORT_NAMES[grid] || 'QA'
    else
      AssessmentHelper::Constants::HFWB[product]
    end
  end

  def score_color
    AssessmentHelper::Constants::SCORE_COLORS
  end

  def level_color
    AssessmentHelper::Constants::LEVEL_COLORS
  end

  # IMMEDIATE ACTION: YES/NO
  # NOK w/out risk => NEXT LEVEL, but YELLOW
  def status_color
    AssessmentHelper::Constants::STATUS_COLORS
  end

  def next_level(letter)
    AssessmentHelper::Constants::NEXT_LEVELS[letter]
  end

  def last_level(letter)
    AssessmentHelper::Constants::LAST_LEVELS[letter]
  end

  def color
    if grid.blank?
      AssessmentHelper::Constants::COLORS['quality_assessment']
    else
      AssessmentHelper::Constants::COLORS[product_grid || grid]
    end
  end

  def bg_color
    if grid.blank?
      AssessmentHelper::Constants::BG_COLORS['quality_assessment']
    else
      AssessmentHelper::Constants::BG_COLORS[product_grid || grid]
    end
  end

  def logo
    if product.nil?
      AssessmentHelper::Constants::LOGOS[grid]
    else
      AssessmentHelper::Constants::HFWB_LOGOS[product]
    end
  end

  def motto
    m = AssessmentHelper::Constants::MOTTOS
    m[grid] || m['quality_assessment']
  end

  def permitted_grid
    return 'DPR' if grid.include?('hfwb')

    short_name
  end

  def assess_types_for(role)
    if role == :supplier
      AssessmentHelper::Constants::SUPPLIER_TYPES[product_grid || grid]
    elsif role == :internal_assessor
      AssessmentHelper::Constants::ASSESSOR_TYPES[product_grid || grid]
    elsif role == :assessor
      AssessmentHelper::Constants::ASSESSOR_TYPES[product_grid || grid]
    end
  end

  def assert_standard_level(level)
    # If level is not standard A->E format, converts to A->E format [For SCM Compatibility.]

    if !QualityAssessment::SORT_INDEX.keys.include?(level)
      i = level.split('.')[0].to_i - 1
      ('A'..'E').to_a.reverse[i]
    else
      level
    end
  end

  def process_options
    AssessmentHelper::Constants::PROCESS_OPTIONS[product_grid || grid]
  end
end
