module AssessmentHelper::Setup
  extend ActiveSupport::Concern

  class_methods do
    def make_one!(user, grid = 'quality_assessment')
      raise StandardError, 'NOT A SUPPLIER!' if user.assessor?

      supp = user.profile

      qa_params = {
        supplier_code: "#{supp.code} #{supp.name}",
        assess_type: 'Gemba',
        assess_date: Time.now + 7.days,
        supplier_profile_id: supp.id,
        created_by: user.id,
        grid: grid,
        site: supp.sites.sample
      }

      ch_params = { chapter_set: Guideline.sections_for(grid).sample(rand(4)) }

      create_and_setup(qa_params, ch_params)
    end

    def create_and_setup(qa_params, ch_params)
      qa = QualityAssessment.new(qa_params)
      # qa.status = qa_params[:assess_type] == "Gemba" ? "Awaiting Assessor" : "Prep Required"
      qa.status = 'Prep Required'
      qa.assign_grid_version
      qa.generate_slug
      if qa.save
        qa.setup_qa_wave(ch_params)
      else
        10.times { puts 'CREATE QA FAILED:' }
        p qa.errors
      end
      qa
    end
  end

  def setup_qa_wave(params)
    setup_assessment_day
    if assess_type == 'CAP Review'
      self.chapters_completed = true
      self.prep_completed = true
      save
    else
      setup_chapters(params[:chapter_set], params[:processes])

      setup_prep_questions(params[:chapter_set])
      SurveyQuestion.seed_for_assessment(self)
    end
    self
  end

  def setup_prep_questions(chapter_set)
    # OPEX assessments don't have prep questions, others do
    if %w[hrp_assessment scm_assessment env_assessment].include?(grid)
      ch = chapter_set.map { |x| x.split('.').first + '.0' }.uniq + chapter_set
      ch.sort_by! { |x| x.to_f }

      fqs = PrepQuestion.form_questions.where(grid_version_id: grid_version_id, grid: grid,
                                              respondant: 'assessor', destroyed_at: nil).order(priority: :asc).to_a
      fqs.select { |x| fqs.find { |y| y.label == x.label } == x }.each do |fq|
        PrepQuestion.create(quality_assessment_id: id, question_tag: fq.question_tag, respondant: 'assessor')
      end

      fqs = PrepQuestion.form_questions.where(grid_version_id: grid_version_id, grid: grid, prep_for: ch,
                                              destroyed_at: nil).order(priority: :asc).to_a
      fqs.select { |x| fqs.find { |y| y.label == x.label } == x }.each do |fq|
        PrepQuestion.create(quality_assessment_id: id, question_tag: fq.question_tag, respondant: 'supplier')
      end
    else

      fqs = PrepQuestion.form_questions.where(grid_version_id: grid_version_id,
                                              grid: (product_grid || grid), destroyed_at: nil).order(priority: :asc).to_a
      fqs.select { |x| fqs.find { |y| y.label == x.label } == x }.each do |fq|
        PrepQuestion.create(quality_assessment_id: id, question_tag: fq.question_tag, respondant: 'assessor')
      end
    end
  end

  def setup_assessment_day
    self.assessment_day_form = AssessmentDayForm.create(quality_assessment_id: id)
  end

  def setup_chapters(chapter_set, processes = [])
    chapter_set = Guideline.sections_for(grid) if chapter_set.nil?

    chapter_set.each do |ch|
      if dpr?
        setup_dpr_chapter(ch, processes)
      else
        setup_basic_chapter(ch)
      end
    end
  end

  def setup_dpr_chapter(ch, processes)
    ch_split = ch.split('.')
    ch_split[0] = ('A'..'Z').to_a.index(ch_split[0]) + 1 if ('A'..'Z').include?(ch_split[0])
    tag = 'DPR_TEXT'
    tag += "CH#{ch_split[0]}"
    tag += "S#{ch_split[1][0]}00"
    tag += "L#{ch_split[1]}" if ch_split[1][1..-1] != '00'

    guideline = DprGuideline.where('guideline_tag ILIKE ?', "#{tag}%")

    guideline = guideline.where(process_name: processes) if processes.present?

    if from_level && to_level
      levels = (to_level..from_level).to_a
      guideline = guideline.where(from_level: levels, to_level: levels)
    end
    guideline.each do |lev|
      ch_params = {
        quality_assessment_id: id,
        chapter_tag: lev.guideline_tag,
        level: lev.to_level,
        section: lev.section_name
      }
      ChapterAssessment.create(ch_params)
    end
  end

  def setup_basic_chapter(ch)
    ch_split = ch.split('.')
    ch_split[0] = ('A'..'Z').to_a.index(ch_split[0]) + 1 if ('A'..'Z').include?(ch_split[0])
    tag = 'QA'
    tag = 'OPEX' if grid == 'opex_assessment'
    tag = 'SSE' if grid == 'sse_assessment'
    tag = 'HRP' if grid == 'hrp_assessment'
    tag = 'ENV' if grid == 'env_assessment'
    tag = 'SCM' if grid == 'scm_assessment'
    tag = 'PUR' if grid == 'pur_assessment'
    tag = 'HFWB' if grid.include?('hfwb')
    tag = 'IND' if grid == 'indus_assessment'
    tag += "CH#{ch_split[0]}S#{ch_split[1]}"

    # debugger

    guideline = Guideline.find_by(section_name: ch, destroyed_at: nil, grid_version_id: grid_version_id)
    if guideline.nil?
      guideline = Guideline.find_by(guideline_tag: tag, destroyed_at: nil, grid_version_id: grid_version_id)
    end

    levels = guideline.nil? ? [] : guideline.children
    levels.each do |lev|
      next if invalid_level?(lev.section_name)
      next if lev.children.blank?

      ch_params = {
        quality_assessment_id: id,
        chapter_tag: lev.guideline_tag,
        level: lev.section_name,
        section: guideline.guideline_tag
      }
      ChapterAssessment.find_or_create_by(ch_params).update(destroyed_at: nil)
    end
  end

  def setup_redo_chapter(tag)
    guideline = Guideline.find_by(guideline_tag: tag, grid_version_id: grid_version_id)
    chapters = []
    guideline.children.each do |lev|
      ch_params = {
        quality_assessment_id: id,
        chapter_tag: lev.guideline_tag,
        level: lev.section_name,
        section: tag
      }
      chapters << ChapterAssessment.create(ch_params)
    end
    chapters
  end
end
