module AssessmentHelper::Dpr
  extend ActiveSupport::Concern

  PROCESSES = ['General Process', 'Die cutting', 'Thermo mould/comp', 'Heat Transfer', 'Embroidery',
               'Print', 'Sublimation', 'Down', 'Welding & Bonding', 'Taping', 'Neoprene', 'Gloves'].freeze
  DPR_GRIDS = ['dpr_assessment'].freeze

  def dpr_processes
    [{
      title: 'PROCESS',
      button: 'Check All',
      buttonShow: true,
      subchapters: PROCESSES.map { |x| { chapter_id: x, value: x, checked: false, disabled: false } }
    }]
  end

  def product_grid
    return nil unless grid.include?(':')

    stem = grid.split(':')[0]
    "#{stem}_assessment"
  end

  def product
    grid.split(':')[1] if grid.include?(':')
  end

  def dpr?
    grid.in?(DPR_GRIDS)
  end

  def process_list(native: false)
    if dpr?
      dpr_processes
    elsif native
      []
    end
  end
end
