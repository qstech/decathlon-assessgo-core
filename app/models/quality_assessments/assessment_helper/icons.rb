module AssessmentHelper::Icons
  extend ActiveSupport::Concern

  class_methods do
    def icon_for(grid, path = :index)
      url = "/members/assessment/index?ref_model=#{grid}" if path == :index
      url = "/members/home/home?dpr=true&grid=#{grid}" if path == :home

      {
        image: AssessmentHelper::Constants::LOGOS[grid],
        borderColor: AssessmentHelper::Constants::BG_COLORS[grid],
        bindtap: 'toPage',
        url: url
      }
    end
  end
end
