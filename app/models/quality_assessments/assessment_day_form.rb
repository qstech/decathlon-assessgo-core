class AssessmentDayForm < ApplicationRecord
  belongs_to :quality_assessment
  after_commit :update_qa
  has_paper_trail ignore: %i[created_at updated_at]

  PL_PRESENT_CHOICES = %w[Yes No N/A].map { |x| { name: x, value: x.downcase } }
  TARGET_CHOICES = ('A'..'E').map { |x| { text: x, value: x } }

  def to_h
    h = serial_hash
    pl = pl_id.nil? ? nil : AssessorProfile.find_by(id: pl_id)
    h[:pl_id] = pl.nil? ? 'No Production Leader Set' : pl.name
    h[:pl_present] = %w[false 0].include?(pl_present) ? 'No' : 'Yes'
    h[:observers] = observer_names
    h[:pl_set] = pl_names unless pl_set.blank?
    h
  end

  def update_qa
    quality_assessment.update_status
  end

  def observer_names
    return [] if observers.blank?

    AssessorProfile.where(id: observers.map(&:to_i)).map(&:name)
  end

  def pl_names
    pls.map(&:name)
  end

  def complete?
    if ['Annual', 'CAP Close'].include? quality_assessment.assess_type
      (pl_id.present? || pl_set.present?) && people_encountered.present? && steps_assessed.present?
    else
      people_encountered.present? && steps_assessed.present?
    end
  end

  def go_for_assessment_status
    qa = quality_assessment
    if ['Annual', 'CAP Close'].include? qa.assess_type
      (pl_id.present? || pl_set.present?) && people_encountered.present? && steps_assessed.present?
    else
      people_encountered.present? && steps_assessed.present?
    end
  end

  def labels
    {
      pl_id: 'Production Leader / Team Leader / OPM',
      pl_set: 'Production Leader / Team Leader / OPM',
      pl_present: 'Did this person attend the assessment?',
      observers: 'Observers',
      people_encountered: 'Main People Met',
      steps_assessed: 'Process Step(s) Assessed'
    }
  end

  def pls
    return [] if pl_set.blank? && pl_id.blank?

    set = pl_set
    set << pl_id if pl_id.present?
    @pls ||= AssessorProfile.where(id: set)
  end

  def responses
    t = I18n.t('api.v1.quality_assessments.go_for_assessment')
    res = []
    if quality_assessment.assess_type.in?(['Annual', 'CAP Close'])
      res << {
        label: "#{t[:production_leader]} / #{t[:team_leader]} / #{t[:opm]}",
        value: pl_names.join(', ')
      }
      res << { label: t[:person_attend], value: pl_present }
      res << { label: t[:target], value: supplier_target }
    end
    res << { label: t[:main_people], value: people_encountered }
    res << { label: t[:process_steps_assessed], value: steps_assessed }
    res << { label: t[:observers], value: observer_names.join(', ') }
    res
  end

  def form
    day_form = serializable_hash
    day_form[:pl_name] = AssessorProfile.find_by(id: pl_id).name if pl_id.present?
    # adding supplier's employees to observer list
    suppliers_arr = observers.select { |x| x.first === 's' }.map { |x| x[1..-1].to_i }
    supplier_staff = quality_assessment.supplier_profile.users.where(id: suppliers_arr)

    day_form[:observer_names] = observer_names + supplier_staff.map(&:username)
    day_form[:pl_names] = pl_names
    day_form
  end
end
