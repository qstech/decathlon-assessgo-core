class QualityAssessment < ApplicationRecord
  include AssessmentHelper::Cards
  include AssessmentHelper::Constants
  include AssessmentHelper::Dpr
  include AssessmentHelper::Icons
  include AssessmentHelper::Labels
  include AssessmentHelper::Forms
  include AssessmentHelper::Radars
  include AssessmentHelper::Score
  include AssessmentHelper::Setup
  include AssessmentHelper::Status
  include AssessmentHelper::Utils
  include AssessmentHelper::People
  include AssessmentHelper::Permissions

  belongs_to :supplier_profile
  belongs_to :user, class_name: 'User', foreign_key: 'created_by', optional: true
  belongs_to :cap, optional: true
  belongs_to :grid_version

  has_one :assessment_day_form, dependent: :destroy
  has_many :prep_questions, dependent: :destroy
  has_many :survey_questions, dependent: :destroy
  has_many :chapter_assessments, dependent: :destroy
  has_many :chapter_questions, through: :chapter_assessments
  has_many :goes_activities, -> { where(ref_model: 'quality_assessment') }, foreign_key: 'ref_id'
  has_many :assessor_assignments, -> { where(ref_model: 'quality_assessment') }, foreign_key: 'ref_id'
  has_many :likes, -> { where(ref_model: 'quality_assessment') }, foreign_key: 'ref_id'
  has_many :favorites, -> { where(ref_model: 'quality_assessment') }, foreign_key: 'ref_id'
  has_many :invitations, -> { where(ref_model: 'quality_assessment') }, foreign_key: 'ref_id'
  has_many :related_chapter_questions, class_name: "ChapterQuestion", foreign_key: 'related_assessment_id'

  has_one_attached :report
  has_one_attached :excel_report
  has_one_attached :excel_cap_review
  has_one_attached :excel_cap_review_empty
  has_paper_trail ignore: %i[created_at updated_at]

  scope :active, -> { where(destroyed_at: nil) }
  scope :destroyed, -> { where.not(destroyed_at: nil) }
  scope :complete, -> { where(completed: true) }
  scope :incomplete, -> { where(completed: [false, nil]) }
  scope :incomplete, -> { where(completed: [false, nil]) }
  scope :formal, -> { where(assess_type: ['Annual', 'CAP Close']) }
  scope :valid, -> { where(destroyed_at: nil, historical: false) }
  scope :join_caps, -> { joins('INNER JOIN "caps" ON "quality_assessments"."id" = "caps"."ref_id"') }
  scope :with_supplier, -> { includes(:supplier_profile) }
  scope :with_user_profiles, -> {
    includes(user: %i[assessor_profile supplier_profile supplier],
             assessor_assignments: { user: %i[assessor_profile supplier_profile supplier] })
  }
  enum process: %i[textile footwear heavystitching hardware bikes] #used only for INDUS grid

  validates_presence_of :assess_date, :assess_type, :supplier_code
  validates_uniqueness_of :slug
  before_create :assign_grid_version
  before_create :generate_slug
  before_create :load_previous_score, unless: :historical
  before_update :recalculate_related_cqs # ASK_LATER not triggered
  before_update :complete_hook, if: proc { |cq| !cq.skip_callback }
  before_update :merge_chapter_dates!, if: :chapter_dates_changed?
  before_save :cleanup

  # before_update :verify_complete
  mattr_accessor :guideline_set
  alias creator user
  include MessageScheduler

  def cleanup
    self.process = nil if process == 'Your Selection:'
    self.cap_id = nil if cap_id&.zero?
  end

  def merge_chapter_dates!
    puts 'merge_chapter_dates!'
    puts chapter_dates
    android = chapter_dates.keys.include?('section_name')
    dates = android ? { chapter_dates['section_name'] => chapter_dates['date'] } : chapter_dates

    puts dates
    self.chapter_dates = changed_attributes['chapter_dates'].merge(dates)
  end

  # def self.guideline_set
  #   if @@guideline_set
  #     return @@guideline_set
  #   else
  #     res = {}
  #     self::GRIDS.each do |grid|
  #       res[grid] = Guideline.for_assessemt(grid, assess_date)
  #     end
  #     return @@guideline_set = res
  #   end
  # end

  # def trigger_complete
  #   if completed_changed? && self.completed
  #     MailBag.trigger!("assessment_completed", self)
  #   end
  # end

  # def trigger_mailbag_create!
  #   MailBag.trigger!("assessment_create", self)
  # end

  # def trigger_mailbag_update!
  #   MailBag.trigger!("assessment_update", self)
  # end

  # def trigger_mailbag_destroy!
  #   MailBag.trigger!("assessment_destroy", self)
  # end

  def self.find_by_info(code, date)
    QualityAssessment.joins(:supplier_profile)
    .find_by(assess_date: Date.parse(date), destroyed_at: nil, supplier_profiles: { code: code })
  end

  def sections
    chapter_assessments.map(&:section_name).uniq.join(', ')
  end

  def parent_assessment_ids
    cap_id.present? ? Cap.parent_tree_for(self).map(&:ref_id) : []
  end

  def parent_and_self_assessment_ids
    return [id] if cap_id.blank?
    parent_assessment_ids + [id]
  end

  def parent_chapter_assessments
    return ChapterAssessment.where(id: nil) if cap_id.blank?

    cap_refs = parent_assessment_ids

    ChapterAssessment.includes(:quality_assessment, :chapter_questions)
    .where(destroyed_at: nil, quality_assessments: { id: cap_refs })
    .order(:id, level: :desc, section: :asc)
  end

  def has_related?
    assess_type.in? ['CAP Review', 'CAP Close']
  end

  def skip_callback(value = false)
    @skip_callback ||= value
  end

  def recalculate_questions!
    5.times { puts '' }
    puts "Recalculating CQ's"
    chapter_questions.each do |cq|
      cq.recalculate!
    end
  end

  def recalculate_chapters!
    5.times { puts '' }
    puts "Recalculating CA's"
    chapter_assessments.each do |ca|
      ca.recalculate!
    end
  end

  def recalculate_self!
    5.times { puts '' }
    puts 'Recalculating QA'
    skip_callback(true)
    set_score!(false)
    save
  end

  def recalculate!
    qa = QualityAssessment.includes(chapter_assessments: { chapter_questions: {
                                                             strong_points_images_attachments: :blob,
                                                             points_to_improve_images_attachments: :blob
    } }).find(id)
    qa.recalculate_questions!
    qa.recalculate_chapters!
    qa.recalculate_self!
  end

  def self.recalculate_all!(completed: true)
    start = Time.now
    cqs = ChapterQuestion.joins(chapter_assessment: :quality_assessment)
    .where(quality_assessments: { completed: completed })
    5.times { puts '' }
    puts "Recalculating CQ's"
    cqs.each(&:recalculate!)

    cas = ChapterAssessment.includes(:chapter_questions, :quality_assessment)
    .where(quality_assessments: { completed: completed })
    5.times { puts '' }
    puts "Recalculating CA's"
    cas.each(&:recalculate!)

    qas = QualityAssessment.includes(chapter_assessments: :chapter_questions)
    .where(completed: completed)
    qas.each(&:recalculate_self!)
    finished = Time.now
    "Finished in #{finished - start} sec"
  end

  def csv_row_for(parameter: :cotation)
    case parameter
    when :cotation
      tot = chapter_questions.select { |x| %w[OK NOK].include?(x.result) }.count
      ok_tot = chapter_questions.select { |x| x.result == 'OK' }.count.to_f
      cq_rate = 0 if tot == 0
      cq_rate = ok_tot * 100 / tot if ok_tot > 0

      "#{id},#{result},#{cotation},#{cq_rate},#{cq_rate.to_i - cotation.to_i}"
    end
  end

  def cascaded_soft_delete
    super

    if destroyed_at_changed? && destroyed_at.present?
      if cap_id.present?
        puts 'SOFT DELETING..... CAP'
        Cap.find(cap_id).update(destroyed_at: Time.now)
      else
        Cap.where(ref_model: 'quality_assessment', ref_id: id).update_all(destroyed_at: Time.now)
      end
    end
  end

  def target
    assessment_day_form.supplier_target || to_level || supplier_profile.target || 'A'
  end

  def generate_cap?
    return false if assess_type == 'CAP Review'

    sup_target = assessment_day_form.supplier_target || supplier_profile.target || 'A'
    cqs = chapter_questions.where(result: (['NOK', 'Not Audited', 'Not Assessed'] + ChapterQuestion::LETTER_RESULTS)).where(
      'chapter_questions.level > ?', sup_target
    )

    return false if cqs.blank?

    types = if user.present? && user.supplier?
      ['CAP Close', 'CAP Review']
    else
      ['CAP Close']
    end

    Cap.where(ref_model: 'quality_assessment', ref_id: id, cap_type: types,
              destroyed_at: nil).blank?
  end

  def prep_optional?
    assess_type == 'Gemba' || assess_type == 'CAP Review'
  end

  def can_edit?(uid)
    # return false if assessor.blank?
    # assessor.id == uid
    return false if assessors.blank?

    can_edit_ids.include?(uid)
  end

  def can_edit_ids
    day_form = assessment_day_form
    edit_set = assessors.map(&:id)
    edit_set += AssessorProfile.where(id: day_form.observers + day_form.pl_set).map(&:user_id)
    edit_set
  end

  def complete_hook
    cap_id = nil if cap_id_changed? && cap_id == 0

    if completed_changed?
      self.completed_at = Time.now
      cap.update(completed: true) if cap.present?
      chapter_questions.where(result: nil).update_all(result: default_result)
      set_score!
    end
  end

  def cap_points
    if cap_id.present?
      cap.cap_points.order(:id)
    else
      sections = chapter_questions.map(&:section)
      CapPoint.includes(:cap)
      .joins('INNER JOIN "chapter_questions"
        ON "chapter_questions"."id" = "cap_points"."ref_id"')
      .where(cap_points: { ref_model: 'chapter_question' },
             chapter_questions: { section: sections })
      .where(caps: { completed: false })
    end
  end

  def last_assessment
    if cap? && cap.present?
      cap.ref
    else
      QualityAssessment.where(supplier_profile_id: supplier_profile_id,
                              assess_type: 'Annual', completed: true, site: site, grid: grid)
      .order(assess_date: :desc).first
    end
  end

  def last_assess_date
    ls = last_assessment
    return '' if ls.blank?

    last_assessment.assess_date.strftime('%Y-%m-%d')
  end

  def load_previous_score
    last_one = last_assessment
    if last_one.nil?
      previous_score = 'N/A'
      true
    else
      previous_score = last_one.result
      previous_date  = last_one.assess_date
      true
    end
  end

  def update_status
    verify_prep_status
    verify_assessor
    verify_day_form
    verify_chapters
    verify_complete
    if prep_questions.present?
      self.status = 'Prep Required' if prep_incomplete?
      self.status = 'Supplier Not Ready!' if prep_status_all[:status] == 'Prep critical points not fullfilled'
    end

    # Add prep_optional?
    self.status = 'Awaiting Assessor' if (prep_completed || prep_optional?) && assessor_unconfirmed?
    self.status = 'Assessment Ready!' if ready_for_qa?
    self.status = 'Assessment In Progress' if day_form_complete?

    self.status = 'Chapters Completed' if chapter_assessments.present? && chapters_complete?

    self.status = 'Complete' if completed?
    # puts self.status

    save
    true
  end

  def render_activity(user)
    profile = user.profile
    goes_params = super
    goes_params[:ref_type] = assess_type
    # ONLY ON CREATE AND COMPLETE

    if completed?
      goes_params[:ref_status] = "#{short_name} Completed!"
      goes_params[:content] =
        "#{(profile.name || user.username).split(' ').first.capitalize} has finished a #{short_name} assessment! Take a look and offer some helpful advice"
      GoesActivity.find_or_create_by(goes_params)
    elsif prep_incomplete?
      goes_params[:ref_status] = "New #{short_name} Scheduled!"
      goes_params[:content] =
        "#{(profile.name || user.username).split(' ').first.capitalize} has scheduled a new #{short_name}!"
      GoesActivity.find_or_create_by(goes_params)
    end
  end

  def activity_type
    assess_type
  end

  # def primary_assessor
  #   assessors.where(assessor_assignments: {primary: true}).first
  # end

  def completed!
    update(completed: true)
  end

  def set_score!(fresh_query = true)
    hashy = score_calc(fresh_query)
    full_set = ('A'..'E').to_a + ['EP']

    if full_set.include?(hashy[:level]) || hashy[:level].nil?
      self.result = hashy[:level]
    else
      i = hashy[:level].split('.')[0].to_i - 1
      self.result = ('A'..'E').to_a.reverse[i]
    end
    self.cotation = hashy[:cotation]
  end

  def project_date
    assess_date.strftime('%Y-%m-%d')
  end

  def prep_status
    !prep_questions.map { |x| x.status }.uniq.include?(false)
  end

  def chapter_set
    section_set = chapter_assessments.map { |x| x.chapter_tag.gsub(/^[A-Z_]+(\d+)[A-Z]+(\d+)[A-Z]+.*$/, '\1.\2') }.uniq

    section_set += section_set.map do |sec|
      sec = sec.split('.')
      i = sec.first.to_i - 1
      sec[0] = ('A'..'Z').to_a[i]
      sec.join('.')
    end
    section_set
  end

  def process_set
    section_set = chapter_assessments.map(&:chapter_tag)
    DprGuideline.where(guideline_tag: section_set).map(&:process_name).uniq
  end

  def prep_finished_at
    prep_questions.order(updated_at: :desc).last.updated_at.strftime('%Y-%m-%d')
  end

  def open_caps
    CapPoint.includes(:cap).where(caps: { ref_model: 'quality_assessment', ref_id: id }).where(resolved_at: nil)
  end

  def caps
    Cap.where(ref_model: 'quality_assessment', ref_id: id, destroyed_at: nil)
  end

  def related_caps
    return Cap.related_for(self)
    # return Cap.where.not(id: cap_id).where(ref_model: "quality_assessment", ref_id: [cap.ref_id, id], destroyed_at: nil) if cap_id.present?
    caps
  end

  def cap_closed?
    return true if (assess_type == 'CAP Close') && completed
    return true if related_caps.select(&:cap_close?).select(&:completed).present?
    return true if caps.select(&:cap_close?).select(&:completed).present?

    false
  end

  def self.all_open_caps(grid = 'quality_assessment')
    caps = CapPoint.includes(:cap).where(caps: { ref_model: 'quality_assessment',
                                                 destroyed_at: nil }).where(resolved_at: nil)
    cap_qa_ids = caps.group_by { |x| x.cap.ref_id }
    # puts cap_qa_ids
    qas = QualityAssessment.where(id: cap_qa_ids.keys, grid: grid)
    # puts "QAS #{qas.count}"
    result = {}
    qas.each do |qa|
      result[qa.supplier_profile_id] = {} if result[qa.supplier_profile_id].blank?
      cap_qa_ids[qa.id].each do |x|
        result[qa.supplier_profile_id][x.section] = true
      end
    end
    result
  end

  def to_h
    h = serial_hash.transform_keys!(&:to_sym)
    h[:assess_date] = assess_date.strftime('%Y-%m-%d')
    h
  end

  def standard_hash
    grid_name = {
      'quality_assessment' => 'Quality Assessment',
      'opex_assessment' => 'OPEX Assessment',
      'sse_assessment' => 'SSE Assessment',
      'hrp_assessment' => 'HRP Assessment',
      'pur_assessment' => 'PUR Assessment',
      'env_assessment' => 'ENV Assessment',
      'scm_assessment' => 'SCM Assessment'
    }
    h = serializable_hash.transform_keys!(&:to_sym)
    h[:projectDate] = {
      month: Date::ABBR_MONTHNAMES[assess_date.month],
      day: assess_date.day,
      grid_name: grid_name[grid]
    }
    h
  end

  def generate_slug
    self.slug = SecureRandom.urlsafe_base64(12)
    generate_slug unless valid? || !errors.messages.keys.include?(:slug)
  end

  def assign_grid_version
    self.grid_version_id = if valid_cap_close?
      cap.ref.grid_version_id
    else
      GridVersion.find_by(grid: product_grid || grid, active: true).id
    end
  end

  def fake_assess!
    last_cqs = cap.ref.chapter_questions if cap_id

    chapter_questions.each do |cq|
      cq.skip_callback(true)
      last_cq = cap_id ? last_cqs.find { |x| x.chapter_tag == cq.chapter_tag } : nil

      if cap_id.nil? || (last_cq && last_cq.result == 'NOK')
        cq.result = rand(10) > 3 ? 'OK' : 'NOK'
        cq.strong_points = 'Some content for faked strong points'
        cq.points_to_improve = 'Some content for faked points to improve'
      else
        cq.result = 'Not Applicable'
        cq.strong_points = nil
        cq.points_to_improve = nil
      end
      cq.save
      cq.recalculate!
    end
    recalculate_chapters!
    recalculate_self!
  end

  # def guidelines
  #   gls = Guideline.includes(:versions, :routine_questions).where(ref_model: self.grid, parent_id: nil).order(guideline_tag: :asc) if self.grid.present?
  #   gls = Guideline.includes(:versions, :routine_questions).where(ref_model: "chapter_assessment", parent_id: nil).order(guideline_tag: :asc) if gls.blank?
  #   gls = Guideline.includes(:versions, :routine_questions).where(ref_model: "quality_assessment", parent_id: nil).order(guideline_tag: :asc) if gls.blank?
  #   guidelines = gls.map{|x| x.version_at(self.assess_date)}.map{|x| p x; x.to_h(self.assess_date)}
  # end

  def guidelines
    if grid.present? && dpr?
      gls = DprGuideline.fetch_for('dpr_textile')
    else
      grd = grid
      base = Guideline.with_all_children.with_attached_toolbox_images.includes(:versions, :routine_questions)
      base = id.nil? ? base.active : base.where(grid_version_id: grid_version_id)
      if grd.include?(':')
        gls = base.where(ref_model: product_grid, parent_id: nil, destroyed_at: nil)
        .where("'#{product}' = ANY (guidelines.relavent_products)")
      elsif grid.present?
        gls = base.where(ref_model: grd, parent_id: nil, destroyed_at: nil)
      end
      guidelines = gls.order(guideline_tag: :asc).map { |x| x.to_h(assess_date, product, skip_nutrition: !nutrition) }
    end
  end

  def last_attempt(tags = nil)
    # For use in sending information about the LAST ATTEMPT for questions on a page.

    tags = chapter_questions.map(&:chapter_tag) if tags.nil?
    if cap_id.present?
      la = cap.ref.chapter_questions
    else
      la = ChapterQuestion.with_images.joins(chapter_assessment: :quality_assessment)
      .where(quality_assessments: { supplier_profile_id: supplier_profile_id, site: site })
      .where.not(quality_assessments: { id: id })
      .where(chapter_questions: { chapter_tag: tags })
      .order(updated_at: :desc, chapter_tag: :asc)

      la = la.select { |x| la.find { |y| y.chapter_tag == x.chapter_tag } == x }
    end

    la.map { |x| [x.chapter_tag, x.standard_hash] }.to_h
  end

  def mp_path(path_for = :assess)
    case path_for
    when :assess
      "members/assessment/pre-confirmation?id=#{id}"
    when :review
      "members/assessment/review-assessment?id=#{id}"
    when :report
      "members/assessment/report?id=#{id}"
    when :index
      "members/assessment/index?ref_model=#{grid}"
    end
  end

  def generate_report(full = true)
    pdf = PdfGenerator.new(self, full)
    StringIO.new(pdf.render_pdf)
  end

  def generate_excel_report(cap_review: false)
    # ........
    if XlsExport::ACTIVE_GRIDS.include?(grid_short_name)
      xls = XlsExport.new(self, cap_review)
      xls.cap_review if cap_review
      StringIO.new(xls.export!)
    end
  end

  def filename_base
    "#{assess_date.strftime('%Y-%m-%d')}-#{supplier_profile.code}-#{supplier_profile.name}-#{grid}"
  end

  def attach_report(file, report_type)
    case report_type
    when 'pdf'
      filename = "#{filename_base}.pdf"
      content_type = 'application/pdf'
      attachment = report
    when 'xlsx'
      filename = "#{filename_base}.xlsx"
      content_type = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
      attachment = excel_report
    when 'xls'
      filename = "#{filename_base}.xls"
      content_type = 'application/vnd.ms-excel'
      attachment = excel_report
    when 'cap'
      filename = "#{filename_base}-(CAP Review).xlsx"
      content_type = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
      attachment = excel_cap_review_empty
    end

    attachment.attach(
      io: file,
      filename: filename,
      content_type: content_type
    )
  end

  def update_report!(full = true)
    updated = updated_at
    self.report_state = 'PROCESSING'
    save(touch: false)
    file = generate_report(full)
    attach_report(file, 'pdf')
    self.report_state = nil
    self.report_date = Time.now
    save(touch: false)
    update(updated_at: updated)
  rescue Exception => e
    puts 'ERROR ENCOUNTERED!!! Resetting report state.'
    self.report_state = nil
    save(touch: false)
    raise e
  end

  def update_excel_report!
    updated = updated_at
    puts 'Inside update_report! for excel'
    self.excel_state = 'PROCESSING'
    save(touch: false)
    file = generate_excel_report
    attach_report(file, report_extension)
    self.excel_state = nil
    self.excel_date = Time.now
    save(touch: false)
    update(updated_at: updated)
  rescue Exception => e
    puts 'ERROR ENCOUNTERED!!! Resetting Excel state.'
    self.excel_state = nil
    save(touch: false)
    raise e
  end

  def initialize_cap_review!
    self.record_timestamps = false
    puts 'Inside update_report! for excel'
    file = generate_excel_report(cap_review: true)
    attach_report(file, 'cap')
    self.record_timestamps = true
  rescue Exception => e
    puts 'ERROR ENCOUNTERED!!! Resetting Excel state.'
    raise e
  end

  def self.last_report!
    qa = last
    qa.update_report!
    qa.report.service_url
  end

  def reset_update_before(time)
    last_update = versions.map(&:reify).reject(&:nil?).reject { |x| x.updated_at > time }.last
    self.updated_at = last_update.updated_at
    save
  end

  def basic_info
    cols = %i[supplier_name site grid assess_type assess_date assessor_name
              status result cotation target created_at updated_at created_by
              prep_completed assessor_confirmed day_form_completed chapters_completed
              slug overall_comments strong_points points_to_improve report_date
              completed_at]

    data = cols.map { |x| [x, safe_send(x)] }.to_h
    data[:grid] = short_name
    data[:created_by] = user.username
    data
  end

  def report_extension
    return 'xls' if short_name.in?(['HRP'])

    'xlsx'
  end

  def self.lookup_grid(short)
    if short.include?('HFWB')
      short.downcase
    else
      AssessmentHelper::Constants::SHORT_NAMES.key(short)
    end
  end

  def recalculate_related_cqs
    related_chapter_questions.update_all(result: result) if result_changed?
  end
end
