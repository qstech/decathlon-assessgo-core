class ChapterAssessment < ApplicationRecord
  belongs_to :quality_assessment
  has_many :chapter_questions, -> { with_images }, dependent: :destroy
  has_many :comments, -> { where(ref_model: 'chapter_assessment') }, foreign_key: 'ref_id'
  has_paper_trail ignore: %i[created_at updated_at]
  after_create :seed_questions
  after_update :update_qa, if: proc { |ca| !ca.skip_callback }

  default_scope -> { where(destroyed_at: nil) }
  scope :with_questions, -> {
    includes(chapter_questions: [{ strong_points_images_attachments: :blob }, { points_to_improve_images_attachments: :blob }])
  }

  def recalculate!
    skip_callback(true)
    score_calc
    update_column(:score, score) if score_changed?
    update_column(:cotation, cotation) if cotation_changed?
  end

  def update_qa
    quality_assessment.update_status
  end

  def update_status
    score_calc
    update(attributes)
  end

  def skip_callback(value = false)
    @skip_callback ||= value
  end

  def evaluation
    guideline&.evaluation if guideline.instance_of?(Guideline)
  end

  def guideline
    if quality_assessment.dpr?
      DprGuideline.find_by(guideline_tag: chapter_tag, grid: 'dpr_textile')
    else
      Guideline.find_by(guideline_tag: chapter_tag, grid_version_id: quality_assessment.grid_version_id)
    end
  end

  def self.fetch_guidelines(attr = { chapter: 0, section: 0, level: 0, point: 0 })
    ch  = attr[:chapter]
    s   = attr[:section]
    l   = attr[:level]
    pt  = attr[:point]

    res = guidelines
    return { 'guidelines' => res } if ch.nil? || ch < 1

    res = guidelines[ch - 1]
    return { 'guidelines' => res } if s.blank?

    res = res['children'][s - 1]
    return { 'guidelines' => res } if l.blank?

    res = res['children'][l - 1]
    return { 'guidelines' => res } if pt.blank?

    res = res['children'][pt - 1]
    { 'guidelines' => res }
  end

  def seed_questions
    if quality_assessment.dpr?
      q = DprGuideline.find_by(guideline_tag: chapter_tag)
      ch = q.guideline_tag.gsub(/[A-Z]+(\d+).*/, '\1')
      sec = q.guideline_tag.gsub(/[A-Z]+(\d+)S(\d+).*/, '\2')
      lev = q.guideline_tag.gsub(/[A-Z]+(\d+)S(\d+)L(\d+).*/, '\3')
      params = {
        chapter_assessment_id: id,
        chapter_tag: q.guideline_tag,
        chapter: ch,
        section: sec,
        level: lev,
        point: lev
      }
      ChapterQuestion.find_or_create_by(params)
    else
      qs = Guideline.joins(:grid_version)
      .find_by(guideline_tag: chapter_tag, grid_versions: { id: quality_assessment.grid_version_id }).children.where(destroyed_at: nil)

      qs.each do |q|
        next if !quality_assessment.nutrition && q.nutrition

        ch = q.guideline_tag.gsub(/[A-Z]+(\d+).*/, '\1')
        sec = q.guideline_tag.gsub(/[A-Z]+(\d+)S(\d+).*/, '\2')
        lev = q.section_name.split('-').first

        params = {
          chapter_assessment_id: id,
          chapter_tag: q.guideline_tag,
          chapter: ch,
          section: sec,
          level: lev,
          point: q.section_name
        }
        ChapterQuestion.find_or_create_by(params)
      end
    end
  end

  def section_name
    section.gsub(/.*CH(\d+)S(\d+)/, '\1.\2')
  end

  def result
    cqs = relevant_cqs
    res = cqs.map(&:result).reject(&:nil?).uniq
    tot = cqs.map(&:score).reject(&:nil?).reduce(:+)

    if tot.nil?
      nil
    elsif (res.length == 1) && (res.first == 'Not Applicable')
      'Not Applicable'
    elsif (res.length == 1) && (res.first == 'Not Assessed')
      'Not Assessed'
    elsif tot == 0
      'OK'
    elsif tot > 0
      'NOK'
    end
  end

  def calculate!
    cqs = chapter_questions.sort_by { |cq| cq.chapter_tag }
    calculate_questions(cqs)
  end

  def calculate_questions(cqs)
    res = { total: 0, points: 0, level: nil, cqs: cqs }
    if level == 'Y'
      res[:level] = cqs.reject { |x| x.result.nil? }.max_by(&:result)&.result || nil
    else
      cqs.each do |cq|
        next if cq.result == 'Not Applicable' # && quality_assessment.assess_type != "CAP Close"
        next if cq.result.nil? && level != 'X'

        res[:total] += 1

        # +1 if cq is OK
        res[:points] += 1 if cq.score == 0
      end
    end
    res
  end

  def score_calc
    res = calculate!
    s = score
    set_score!(res)
    s2 = score
    puts "#{s} => #{s2}" if s != s2

    c = cotation
    set_cotation!(res)
    c2 = cotation
    puts "#{c} => #{c2}" if c != c2
  end

  def check_ep?
    ep_grids = ['hrp_assessment']
    grid = quality_assessment.grid

    (ep_grids.include?(grid) && level == 'E')
  end

  def set_score!(res = {})
    res = calculate! if res.blank?
    if level == "Y"
      self.score = res[:level]
      return
    end
    cqs = res[:cqs]


    return set_score_by_evaluation!(res) if level == "X"


    self.score = if res[:total] == 0
      'Not Applicable'
    elsif res[:points] == res[:total]
      'OK'
    elsif all_in?(cqs, :result, ['OK', 'Not Applicable', 'NOK without risk'])
      'NOK without risk'
    elsif check_ep? && all_in?(cqs, :action_taken, [true, nil])
      'NOK (EP)'
    elsif all_in?(cqs, :result, ['Not Assessed', 'Not Applicable'])
      'Not Assessed'
    else
      'NOK'
    end
  end

  def set_cotation!(res = {})
    res = calculate! if res.blank?

    self.cotation = if res[:total] == 0
      nil
    else
      (res[:points].to_f / res[:total] * 100).to_i
    end
  end

  def not_assessed!
    self.cotation = nil
    self.score    = 'Not Assessed'
    chapter_questions.each do |cq|
      cq.update(result: 'Not Assessed')
    end
    true
  end

  def all_in?(set, column, results)
    set.select { |x| results.include?(x.public_send(column)) } == set
  end

  def finished?
    chapter_questions.where(result: nil).blank?
  end

  def section_score
    # Make helper method for section_score
  end

  def last_ca
    return nil unless quality_assessment.assess_type == 'CAP Close' && quality_assessment.cap.present?

    @last_ca ||= ChapterAssessment.includes(:chapter_questions).find_by(
      quality_assessment_id: quality_assessment.cap.ref_id, chapter_tag: chapter_tag
    )
  end

  def merged_cqs
    return chapter_questions if last_ca.nil?

    @merged_cqs ||= (last_ca.chapter_questions.select { |x| x.result == 'NOK' } + chapter_questions).sort_by(&:id)
  end

  def relevant_cqs
    cqs = merged_cqs
    cqs.group_by(&:chapter_tag).map do |_ct, cq_arr|
      next(cq_arr.first) if cq_arr.length == 1

      cq_arr.select { |x| x.result.in?(%w[OK NOK]) }.max_by(&:id)
    end
  end

  def countable?
    score != 'Not Applicable'
  end

  def siblings
    if quality_assessment.dpr?
      ChapterAssessment.where(id: id)
    else
      ChapterAssessment.where(section: section, quality_assessment_id: quality_assessment_id)
    end
  end

  def border_color
    if score.in?(['NOK', 'Not Assessed'])
      'red'
    elsif score == 'OK'
      'green'
    end
  end

  def action_taken?
    actions_count = chapter_questions.where(action_taken: [true, nil]).count
    (actions_count == chapter_questions.count) && chapter_questions.where(result: 'NOK').present?
  end

  def set_score_by_evaluation!(res)
    return if res[:total].zero?

    cot = (res[:points].to_f / res[:total] * 100).to_i
    limits = evaluation.limits
    self.score = evaluation.lowest_limit
    limits.each do |x|
      if cot > x[1]
        self.score = x[0]
        break
      end
    end
  end
end
