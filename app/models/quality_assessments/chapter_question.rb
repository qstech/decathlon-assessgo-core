class ChapterQuestion < ApplicationRecord
  belongs_to :chapter_assessment
  belongs_to :related_assessment, class_name: 'QualityAssessment', foreign_key: 'related_assessment_id', optional: true
  has_many :cap_points, -> { where(ref_model: 'chapter_question') }, foreign_key: 'ref_id'
  has_many :likes, -> { where(ref_model: 'chapter_question') }, foreign_key: 'ref_id'
  has_many_attached :strong_points_images
  has_many_attached :points_to_improve_images
  has_paper_trail ignore: %i[created_at updated_at]
  before_update :score_calc, if: proc { |cq| !cq.skip_callback }
  after_update :update_chapter, if: proc { |cq| !cq.skip_callback }
  before_save :check_zeros

  validates :chapter_tag, uniqueness: { scope: :chapter_assessment_id }
  scope :with_images, -> { with_attached_strong_points_images.with_attached_points_to_improve_images }
  scope :with_likes, -> { includes(:likes) }
  scope :with_qa, -> { includes(chapter_assessment: :quality_assessment) }

  RESULTS = ['OK', 'NOK', 'Not Applicable', 'Not Assessed'].freeze
  RESULTS_CHOICES = RESULTS.map { |x| { name: x, value: x } }
  NOK_TYPE = ['NOK', 'Not Assessed'].freeze
  GRIDS_WITH_RELATED_ASSESSMENTS = %w[indus_assessment].freeze
  LETTER_RESULTS = ('A'...'E').to_a.freeze


  def nok_type?
    NOK_TYPE.include?(result)
  end

  def check_zeros
    self.related_assessment_id = nil if related_assessment_id == 0
  end

  def show_related_assessments?
    quality_assessment.grid.in?(GRIDS_WITH_RELATED_ASSESSMENTS) && guideline.related_assessments_grid.present?
  end

  def related_assessments
    QualityAssessment.where(grid: guideline.related_assessments_grid, completed: true, supplier_code: quality_assessment.supplier_code)
  end

  def related_assessments_cards
    related_assessments.map { |x| PageComponent.completed_qa_card(x) }
  end

  def single_chapter_form(user, with_comments: false, show_last: true, qa: nil, last_attempt: nil, i: 0)
    qa ||= quality_assessment
    pt = {
      form_fields: standard_hash,
      likes: likes_h(user, i: i),
      last_attempt: false # ==============BOOKMARK
    }

    pt[:comments] = comments.map { |x| PageComponent.comment_card(x, user) } if with_comments
    pt[:form_fields][:confidential] = false if qa.users_concerned.include?(user)

    if qa.cap_close? && show_last
      pt[:last_attempt] = last_attempt[chapter_tag]
      pt[:last_attempt] = false if pt[:last_attempt].blank? || pt[:last_attempt][:id].nil?
    end
    gl = qa.grid.in?(GRIDS_WITH_RELATED_ASSESSMENTS) ? guideline : nil
    pt[:related_assessments] = []
    pt[:show_related_assessments] = false
    if (gl.present? && gl.instance_of?(Guideline)) && gl.related_assessments_grid.present?
      pt[:related_assessments] = related_assessments_cards
      pt[:show_related_assessments] = true
      pt[:schedule_url] = "/members/assessment/new?ref_model=#{gl.related_assessments_grid}"
    end
    pt
  end

  def img_h
    {
      strong_points_images: strong_points_images_urls,
      strong_points_images_thumbs: strong_points_images_thumbs_urls,
      strong_points_images_bookmarks: strong_points_images_bookmarks,
      points_to_improve_images: points_to_improve_images_urls,
      points_to_improve_images_thumbs: points_to_improve_images_thumbs_urls,
      points_to_improve_images_bookmarks: points_to_improve_images_bookmarks
    }
  end

  def to_h
    h = serial_hash
    h[:similar_question] = similar_question
    h[:similar_grid] = similar_grid
    h
  end

  def likes_h(user, i: 0)
    {
      strong_points: like_h(user, 'strong_points', i: i),
      points_to_improve: like_h(user, 'points_to_improve', i: i)
    }
  end

  def like_h(user, like_type, i: 0)
    like = user.liked?(self, like_type)
    likeobj = user.like_for(self, like_type)

    {
      index: i, ref_model: 'chapter_question', ref_id: id, ref_type: like_type,
      liked: like, like_id: likeobj.present? ? likeobj.id : '',
      count: likes.select { |x| x.ref_type == like_type }.length
    }
  end

  def cap_point_h(grid = nil)
    grid = grid_type if grid.nil?
    h = {
      ref_model: 'chapter_question',
      ref_id: id,
      result: result || 'N/A',
      nonconformity: points_to_improve || '',
      level: point
    }

    if grid == :dpr
      gl = DprGuideline.find_by(guideline_tag: chapter_tag)
      h[:level] = gl.step_name
    else
      gl = Guideline.find_by(guideline_tag: chapter_tag, grid_version_id: grid_version_id)
    end

    h[:requirement] = gl.requirement
    h[:section] = "#{chapter}.#{section}"
    h
  end

  def grid_version_id
    quality_assessment.grid_version_id
  end

  def grid_type
    quality_assessment.dpr? ? :dpr : :normal
  end

  def countable?
    result.in?(['OK', 'NOK', 'NOK without risk'])
  end

  def recalculate!
    skip_callback(true)
    set_score!
    update_column(:score, score) if score_changed?
  end

  def update_chapter
    chapter_assessment.update_status
  end

  def skip_callback(value = false)
    @skip_callback ||= value
  end

  def render_activity(user)
    quality_assessment.render_activity(user) if quality_assessment.chapters_complete?
  end

  def strong_points_images_urls
    strong_points_images.map { |x| blob_link(x) }
  end

  def points_to_improve_images_urls
    points_to_improve_images.map { |x| blob_link(x) }
  end

  def strong_points_images_thumbs_urls
    strong_points_images.map { |x| variant_link(x) }
  end

  def points_to_improve_images_thumbs_urls
    points_to_improve_images.map { |x| variant_link(x) }
  end

  def strong_points_images_bookmarks
    strong_points_images.map { |x| x.bookmark }
  end

  def points_to_improve_images_bookmarks
    points_to_improve_images.map { |x| x.bookmark }
  end

  def comments
    @comments ||= Comment.where(ref_model: 'chapter_assessment',
                                ref_id: chapter_assessment_id).order(created_at: :desc)
  end

  def quality_assessment
    @quality_assessment ||= chapter_assessment.quality_assessment
  end

  def set_score!
    scores = { 'OK' => 0, 'NOK' => 1, 'Not Assessed' => 1, 'Not Applicable' => nil }
    self.score = scores[result]
  end

  def score_calc
    set_result_by_related_assessment! if related_assessment_id_changed?
    set_score! if result_changed?
  end

  def similar_grid
    tag = similar_tag

    return similar_tag.gsub(/CH.+/, '') if tag

    ''
  end

  def similar_question
    tag = similar_tag

    if tag && chapter_assessment.present?
      suppid = quality_assessment.supplier_profile_id
      cq = ChapterQuestion.joins(chapter_assessment: :quality_assessment).find_by(
        quality_assessments: { supplier_profile_id: suppid }, chapter_questions: {
          chapter_tag: tag
        }
      )
      url = '/members/assessment/historical-level'
      url += cq.present? ? "?id=#{cq.id}" : "?tag=#{tag}"
      return url
    end
    ''
  end

  def similar_tag
    similars = [
      %w[SCMCH3S3L1-1 HRPCH2S2L1-2], %w[SCMCH6S1L1-1 ENVCH1S2L3-6],
      %w[SCMCH7S1L1-1 ENVCH1S1L3-2], %w[SCMCH7S1L2-2 ENVCH1S1L3-1],
      %w[SCMCH7S1L3-1 ENVCH1S1L3-3], %w[SCMCH7S1L4-1 ENVCH1S1L4-3],
      %w[SCMCH7S2L1-1 ENVCH1S2L1-2], %w[SCMCH7S2L1-2 ENVCH1S2L1-3],
      %w[SCMCH7S2L2-1 ENVCH1S2L2-2], %w[SCMCH7S2L2-2 ENVCH1S2L2-3],
      %w[SCMCH7S2L3-1 ENVCH1S2L3-5], %w[SCMCH7S3L1-2 ENVCH1S3L2-2],
      %w[SCMCH7S3L2-1 ENVCH1S3L2-1], %w[SCMCH7S3L2-2 ENVCH1S3L3-3],
      %w[SCMCH7S3L3-1 ENVCH1S3L3-4], %w[SCMCH7S3L3-2 ENVCH1S3L3-6],
      %w[SCMCH7S3L4-1 ENVCH1S3L4-2]
    ]

    similars.each do |tag_set|
      return tag_set.find { |x| x != chapter_tag } if tag_set.include?(chapter_tag)
    end
    false
  end

  def finished?
    result.present?
  end

  def set_result_by_related_assessment!
    self.result = related_assessment.present? ? related_assessment.result : nil
  end

  def guideline
    if quality_assessment.dpr?
      DprGuideline.find_by(guideline_tag: chapter_tag, grid: 'dpr_textile')
    else
      Guideline.find_by(guideline_tag: chapter_tag, grid_version_id: quality_assessment.grid_version_id)
    end
  end

  def sorting_chapter_tag
    tag_arr = chapter_tag.split('-')
    return chapter_tag if tag_arr.length == 1
    tag_arr[1] = "0#{tag_arr[1]}" if tag_arr[1].length == 1
    tag_arr.join('-')
  end

end
