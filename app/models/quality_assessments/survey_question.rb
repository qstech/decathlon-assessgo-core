# frozen_string_literal: true

# Survey questions for HRP Dormitory List (2.5 D-3)
class SurveyQuestion < ApplicationRecord
  belongs_to :quality_assessment
  belongs_to :form_question
  enum result: %i[no_result OK NOK not_applicable not_assessed]
  after_update :update_cq!
  before_update :input_result!
  has_many_attached :attachments

  OK_TYPES = %i[OK not_applicable].freeze
  REQUIRED_OPTIONS = %w[OK NOK not_applicable].freeze
  STANDARD_OPTIONS = %w[OK NOK not_applicable not_assessed].freeze

  def label
    form_question.label
  end

  def required?
    form_question.required
  end

  def with_remarks?
    form_question.with_remarks
  end

  def require_attachment?
    form_question.require_attachment
  end

  def component
    form_question.component
  end

  def options
    required? ? REQUIRED_OPTIONS : STANDARD_OPTIONS
  end

  def radio?
    component == 'radio'
  end

  def input?
    component == 'input'
  end

  def form_section
    @form_section ||= form_question.form_section
  end

  def to_h
    { id: id, required: required?, label: label, component: component, options: options,
      with_remarks: with_remarks?, with_attachment: require_attachment? }
  end

  def required_siblings
    SurveyQuestion.joins(:form_question)
                  .where(form_questions: { required: true })
                  .where(quality_assessment_id: quality_assessment_id)
  end

  def siblings
    SurveyQuestion.where(quality_assessment_id: quality_assessment_id)
  end

  def critical_points_ok?
    required_siblings.where(result: :OK).count == required_siblings.count
  end

  def input_result!
    self.result = :OK if input? && response.present?
  end

  def update_cq!
    cq = quality_assessment.chapter_questions.find_by_chapter_tag('HRPCH2S5L2-3')
    result = critical_points_ok? ? 'OK' : 'NOK'

    cq.update(result: result)
  end

  class << self
    def seed_for_assessment(assessment)
      return true unless [assessment.grid, assessment.grid_version.version_number] == ['hrp_assessment', 2]

      FormQuestion.where('question_tag ILIKE ?', 'HRPDORM%').each do |question|
        params = { quality_assessment_id: assessment.id, form_question_id: question.id,
                   question_tag: question.question_tag }
        SurveyQuestion.find_or_create_by(params)
      end
    end
  end
end
