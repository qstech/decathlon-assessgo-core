class PrepQuestion < ApplicationRecord
  belongs_to :quality_assessment
  after_commit :update_qa
  has_paper_trail ignore: %i[created_at updated_at]

  has_many_attached :attachments

  def can_edit?(uid)
    if respondant == 'assessor'
      quality_assessment.can_edit?(uid)
    else
      supp = quality_assessment.supplier_profile
      uids = supp.user_ids
      uids << supp.user_id
      uids.include?(uid) || quality_assessment.can_edit?(uid)
    end
  end

  def update_qa
    quality_assessment.update_status
  end

  def status
    !response.nil?
  end

  def prep_list
    {
      chapter: question_tag,
      status: response,
      statusColor: response.nil? ? 'red' : 'green'
    }
  end

  def attachment_thumbs
    attachments.select { |x| ['image/jpeg', 'image/png'].include?(x.blob.content_type) }
               .map { |x| variant_link(x) }
  end

  def form
    h = {
      response: response,
      attachments: attachments.map { |x| blob_link(x) },
      attachments_thumbs: attachment_thumbs,
      pq_id: id
    }
    h[:required] = true if %w[QAPREP1 QAPREP6].include? question_tag
    h[:na_ok] = true if %w[QAPREP2 QAPREP9 QAPREP20].include? question_tag
    h
  end

  class << self
    def last_prep_for(qa, question)
      if qa.valid_cap_close?
        qa.cap.ref.prep_questions.find_by(question_tag: question[:question_tag])
      else
        PrepQuestion.includes(:quality_assessment)
                    .where(quality_assessments: { supplier_profile_id: qa.supplier_profile_id,
                                                  grid: qa.grid, assess_type: ['Annual', 'CAP Close'] })
                    .where(question_tag: question[:question_tag])
                    .where.not(id: question[:pq_id], response: [nil, ''])
                    .order(:id).last
      end
    end
  end
end
