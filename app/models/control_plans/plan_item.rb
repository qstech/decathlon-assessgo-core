class PlanItem < ApplicationRecord
  belongs_to :control_plan
  belongs_to :process_step
  has_many :cap_points, -> { where(ref_model: 'plan_item') }, foreign_key: 'ref_id'
  has_paper_trail ignore: %i[created_at updated_at]

  has_many_attached :findings_images
  has_many_attached :findings_process_images

  before_update :unique_orders, on: %i[create update]
  before_update :check_complete

  RESULTS = %w[OK NOK DER]

  def to_h
    date = cap_date.nil? ? '' : cap_date.strftime('%Y-%m-%d')
    formatted_orders = orders.select do |x|
                         x.split('-').first != '' && x.split('-').last != ''
                       end.map do |x|
      { order_number: x.split('-').first,
        order_qty: x.split('-').last }
    end
    serial_hash.merge({ cap_date: date, formated_orders: formatted_orders,
                        images: { findings_images: findings_images_urls, findings_process_images: findings_process_images_urls } })
  end

  def check_complete
    self.completed = result != 'false'
    if completed
      ac = process_step.active_freq
      ac.present? ? ac.complete! : true
    end
  end

  def unique_orders
    self.orders = orders.select { |x| x.split('-').first != '' && x.split('-').last != '' }
  end

  def plan_item_assessor
    control_plan.assessor
  end

  def findings_images_urls
    host = DOMAIN
    findings_images.map { |x| Rails.application.routes.url_helpers.rails_blob_url(x, host: host) }
  end

  def findings_process_images_urls
    host = DOMAIN
    findings_process_images.map { |x| Rails.application.routes.url_helpers.rails_blob_url(x, host: host) }
  end

  def result_color
    return 'red' if nok?
    return 'green' if ok?
    return 'yellow' if der?

    'black'
  end

  def nok?
    result == 'NOK'
  end

  def ok?
    result == 'OK'
  end

  def der?
    result == 'DER'
  end
end
