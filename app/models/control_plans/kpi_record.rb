class KpiRecord < ApplicationRecord
  belongs_to :kpi_track
  validates :value, presence: true
  has_paper_trail ignore: %i[created_at updated_at]
end
