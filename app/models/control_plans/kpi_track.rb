class KpiTrack < ApplicationRecord
  belongs_to :product_family
  has_many :kpi_records
  validates :name, presence: true, uniqueness: { scope: [:product_family_id] }
  validates :frequency, presence: true
  has_paper_trail ignore: %i[created_at updated_at]
  OPTIONS = ['Right First Time', 'Line Reject Rate', 'Rework Rate', 'RPM', 'NQC', 'CR', 'SCRAPT RATE']
end
