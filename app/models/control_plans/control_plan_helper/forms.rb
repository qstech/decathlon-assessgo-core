module ControlPlanHelper::Forms
  extend ActiveSupport::Concern

  def generate_cap_prep
    pis = plan_items.where(result: 'NOK')
    h = {}
    h[:ref_model] = 'control_plan'
    h[:ref_id] = id
    # h[:cap_date] ASKED IN FORM
    # h[:cap_type] ASKED IN FORM
    h[:supplier_profile_id] = product_family.supplier_profile_id
    # h[:pl_id] ASK DURING CP?
    h[:points] = []
    h[:recheck] = []
    pis.each do |pi|
      process_step = pi.process_step
      new_h = {}
      new_h[:ref_model] = 'plan_item'
      new_h[:ref_id]    = pi.id
      new_h[:section]   = process_step.product_process.name
      new_h[:level]     = process_step.step_number
      new_h[:requirement] = "#{process_step.requirement}\n Frequency: #{process_step.decathlon_control_freq}"
      new_h[:result]    = pi.result || 'N/A'
      new_h[:bullets]   = ["Findings: #{pi.findings}", "Immediate Actions/Protections: #{pi.actions}"]

      if pi.block
        h[:recheck] << new_h
      else
        h[:points] << new_h
      end
    end
    h
  end
end
