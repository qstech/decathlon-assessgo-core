module ControlPlanHelper::Calculations
  extend ActiveSupport::Concern

  def stats
    [
      { label: 'Nonconformity Rate', value: nonconformity_rate },
      { label: 'Frequency Respect Rate', value: freq_respect_rate },
      { label: 'CAP Close Rate', value: cap_close_rate }
    ]
  end

  def nonconformity_rate
    return 0 if plan_items.blank?

    pi = plan_items
    (pi.select { |x| x.result == 'NOK' }.count.to_f / pi.length * 100).round(2)
  end

  def freq_respect_rate(freq_for = 'Decathlon', fresh_query = true)
    return 100 if plan_items.blank?

    ps = plan_items.map(&:process_step).flatten.map { |x| x.freq_respected?(freq_for, fresh_query) }.flatten
    return 100 if ps.blank?

    (ps.select { |x| x == true }.count.to_f * 100 / ps.count).round(0)
  end

  def cap_close_rate
    return 100 if plan_items.blank?

    caps = plan_items.map(&:cap_points).flatten.map { |x| x.resolved_at.present? }
    # TODO: getting an error here that prevents some execution, not sure if this is the right way of handling it or not
    return 0 if caps.blank?

    (caps.select { |x| x == true }.count.to_f * 100 / caps.count).round(0)
  end

  def cap_finished?
    cap_points.where(resolved_at: nil).blank?
  end
end
