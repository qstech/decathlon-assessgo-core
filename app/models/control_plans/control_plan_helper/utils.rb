module ControlPlanHelper::Utils
  extend ActiveSupport::Concern

  def product_name
    product_family.name
  rescue StandardError
    'UNAVAILABLE'
  end

  def plan_date
    assess_date
  end

  def plan_status
    completed ? 'Completed' : 'Open'
  end

  def status
    completed ? 'Finished' : 'N/A'
  end

  def level_color
    { 'OK' => 'green', 'NOK' => 'red', nil => 'black', 'DER' => 'yellow' }
  end

  def status_color
    { 'finished' => 'blue', 'N/A' => 'yellow' }
  end

  def result
    return 'N/A' unless completed
    return 'NOK' if plan_items.select(&:nok?).present?
    return 'DER' if plan_items.select(&:der?).present?
    return 'OK'  if plan_items.select(&:ok?).present?

    'N/A'
  end

  def assess_type
    plan_type
  end

  def basic_info
    cols = %i[supplier_name product_name plan_type plan_date assessor_name
              plan_status nonconformity_rate freq_respect_rate cap_close_rate
              created_at updated_at slug created_by]

    data = cols.map { |x| [x, safe_send(x)] }.to_h
    data[:created_by] = user.username
    data
  end

  def mp_path(path_for = :assess)
    case path_for
    when :assess
      "members/control_plan/review?cp_id=#{id}"
    when :review
      "members/control_plan/findings?id=#{id}"
    when :report
      "members/control_plan/report?id=#{id}"
    when :index
      'members/control_plan/index'
    end
  end

  def generate_slug
    self.slug = SecureRandom.urlsafe_base64(12)
    generate_slug unless valid?
  end

  def short_name
    'CP'
  end

  def icon
    {
      image: '/images/logos/cp_logo.jpg',
      borderColor: 'purple',
      bindtap: 'toPage',
      url: "/#{mp_path(:index)}"
    }
  end
end
