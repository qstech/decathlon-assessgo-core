module ControlPlanHelper::People
  extend ActiveSupport::Concern

  def audience_for(audience)
    single = %i[creator supplier]
    multi  = %i[supplier_pls supplier_opms supplier_ptms]

    return [public_send(audience)].reject(&:nil?) if audience.in?(single)
    return public_send(audience) if audience.in?(multi)
    return User.where(username: %w[kkang20 wxu50]) if audience == :admins

    raise StandardError, "AUDIENCE NOT VALID: #{audience}"
  end

  def users_concerned
    @users_concerned ||= fetch_users_concerned
  end

  def supplier
    @supplier ||= fetch_supplier
  end

  def supplier_profile
    @supplier_profile ||= product_family.supplier_profile
  end

  def supplier_pls
    @supplier_pls ||= fetch_assessor_users(supplier.pl_set)
  end

  def supplier_opms
    @supplier_opms ||= fetch_assessor_users(supplier.opm_set)
  end

  def supplier_ptms
    @supplier_ptms ||= fetch_assessor_users(supplier.ptm_set)
  end

  def assessor_profile
    @assessor_profile ||= assessor_type == 'assessor' ? super : SupplierProfile.find(assessor_profile_id)
  end

  def assessor
    assessor_profile
  end

  def assessor_name
    (assessor_profile && assessor_profile.name) || user.username
  end

  def supplier_name
    product_family.supplier_profile.name
  rescue StandardError
    'UNAVAILABLE'
  end

  def username
    if assessor_type == 'assessor'
      assessor_profile.name
    else
      user.username
    end
  end

  # ===========================END PUBLIC=======================================
  private

  def fetch_assessors
    base = User.includes(:assessor_assignments, :supplier_profile, :assessor_profile)

    base.where(assessor_assignments: { ref_model: 'quality_assessment', ref_id: id })
        .or(base.where(id: created_by)).or(base.where(assessor_profiles: { id: people_concerned }))
  end

  def fetch_users_concerned
    User.where(id: concerned_user_ids)
  end

  def fetch_assessor_users(set)
    User.includes(:assessor_profile).where(assessor_profiles: { id: set })
  end

  def fetch_supplier
    User.find_by(id: supplier_profile.user_id)
  rescue StandardError
    nil
  end

  def concerned_user_ids
    peeps = AssessorProfile.where(id: people_concerned).map(&:user_id)
    user_set = [supplier_profile.user_id, supplier_profile.user_ids, peeps]
    user_set << assessor_profile.user_id if assessor_profile.present? && (assessor_type == 'assessor')
    user_set.flatten.reject(&:blank?).uniq
  end
end
