module ControlPlanHelper::Reports
  extend ActiveSupport::Concern

  def generate_report
    pdf = PdfGenerator.new(self)
    StringIO.new(pdf.render_pdf)
  end

  def attach_report(file)
    filename = "#{assess_date.strftime('%Y-%m-%d')}-#{supplier_profile.code}-#{supplier_profile.name}-#{grid}.pdf"

    report.attach(
      io: file,
      filename: filename,
      content_type: 'application/pdf'
    )
  end

  def update_report!(_full = true)
    self.report_state = 'PROCESSING'
    save(touch: false)
    file = generate_report
    attach_report(file)
    self.report_state = nil
    self.report_date = Time.now
    save(touch: false)
  rescue StandardError
    puts 'ERROR ENCOUNTERED!!! Resetting report state.'
    self.report_state = nil
    save(touch: false)
  end
end
