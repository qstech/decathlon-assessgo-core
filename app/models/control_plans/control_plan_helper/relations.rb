module ControlPlanHelper::Relations
  extend ActiveSupport::Concern
  def schedule
    Schedule.find_by(ref_id: id)
  end

  def process_steps
    ProcessStep.includes(:plan_items).includes(plan_items: :control_plan)
               .where(control_plans: { id: id })
  end

  def steps_available
    plan_items.select { |x| x.active }
  end

  def steps_reviewed
    steps_available.select { |x| x.completed }
  end

  def steps_pending
    steps_available.reject { |x| x.completed }
  end

  def cap_points
    process_step_ids = process_steps.map { |x| x.id }
    CapPoint.includes(:cap).joins('INNER JOIN "plan_items" ON "plan_items"."id" = "cap_points"."ref_id"')
            .where(cap_points: { ref_model: 'plan_item' },
                   plan_items: { process_step_id: process_step_ids })
            .where(caps: { completed: false })
  end

  def related_caps
    return Cap.where(ref_model: 'control_plan', ref_id: cap.ref_id) if cap_id.present?

    []
  end

  def caps
    Cap.where(ref_model: 'control_plan', ref_id: id)
  end
end
