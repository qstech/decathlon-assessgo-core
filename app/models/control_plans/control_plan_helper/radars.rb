module ControlPlanHelper::Radars
  extend ActiveSupport::Concern

  def details_for(radar)
    radar.map do |k, arr|
      h = { title: k, values: [] }
      h[:values] << { name: 'NCR', value: (arr.select { |x| x.result == 'NOK' }.count.to_f / arr.count * 100).round(2) }
      h[:values] << { name: 'OK Count', value: arr.select(&:ok?).count }
      h[:values] << { name: 'DER Count', value: arr.select(&:der?).count }
      h[:values] << { name: 'NOK Count', value: arr.select(&:nok?).count }
      h[:values] << { name: 'Total Count', value: arr.count }
      h
    end
  end

  def radar_set
    return {} unless completed

    pi = plan_items.includes(:process_step).includes(process_step: :product_process)

    radars = []
    nok = pi.where(result: 'NOK')
    tot = pi.count

    by_process = pi.group_by { |x| x.process_step.product_process.name }
    by_control_type = pi.group_by { |x| x.process_step.control_type }
    by_criticity = pi.group_by { |x| x.process_step.criticity }
    by_category = pi.group_by { |x| x.process_step.product_process.category }

    radars << {
      name: 'process',
      title: 'Nonconformity Rate by Process',
      labels: by_process.keys.map { |x| x || '' },
      details: details_for(by_process),
      data: by_process.values.map { |arr| (arr.select { |x| x.result == 'NOK' }.count.to_f / arr.count * 100).round(2) }
    }

    radars << {
      name: 'type',
      title: 'Nonconformity Rate by Control Type',
      labels: by_control_type.keys.map { |x| x || '' },
      details: details_for(by_control_type),
      data: by_control_type.values.map do |arr|
              (arr.select do |x|
                 x.result == 'NOK'
               end.count.to_f / arr.count * 100).round(2)
            end
    }

    radars << {
      name: 'category',
      title: 'Nonconformity Rate by Category',
      labels: by_category.keys.map { |x| x || '' },
      details: details_for(by_category),
      data: by_category.values.map do |arr|
              (arr.select do |x|
                 x.result == 'NOK'
               end.count.to_f / arr.count * 100).round(2)
            end
    }

    radars << {
      name: 'type',
      title: 'Nonconformity Rate by Criticity',
      labels: by_criticity.keys.map { |x| x || '' },
      details: details_for(by_criticity),
      data: by_criticity.values.map do |arr|
              (arr.select do |x|
                 x.result == 'NOK'
               end.count.to_f / arr.count * 100).round(2)
            end
    }

    radars
  end
end
