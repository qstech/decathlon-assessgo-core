class CpPendingFrequency < ApplicationRecord
  belongs_to :process_step
  has_paper_trail ignore: %i[created_at updated_at]

  def complete!
    update(resolved_at: Time.now)
    x = eval(process_step.decathlon_control_freq) || 2.weeks
    CpPendingFrequency.create(process_step_id: process_step_id, frequency_for: frequency_for, expires_at: Time.now + x)
  end
end
