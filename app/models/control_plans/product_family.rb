class ProductFamily < ApplicationRecord
  belongs_to :supplier_profile
  has_many :control_plans, dependent: :destroy
  has_many :plan_items, through: :control_plans
  has_many :product_processes, dependent: :destroy
  has_many :process_steps, through: :product_processes
  has_many :kpi_tracks
  has_many :kpi_records, through: :kpi_tracks

  has_paper_trail ignore: %i[created_at updated_at]
  has_many_attached :process_flow_images

  # default_scope ->{where(destroyed_at: nil)}

  def other_kpis
    kpi_tracks.includes(:kpi_records).where(active: true).map do |kpi|
      vals = kpi.kpi_records.map(&:value).reject(&:nil?)
      avg = vals.length > 0 ? (vals.sum / vals.length.to_f) : 0
      { name: kpi.name, value: avg.round }
    end
  end

  def web_views(current_user)
    {
      pf_edit_url: NativeUrl.generate('control_plan/new-product', current_user, product_id: id),
      history_url: NativeUrl.generate('control_plan/history', current_user, product_id: id),
      cp_history_url: NativeUrl.generate('control_plan/cphistory', current_user, product_family_id: id),
      steps_url: NativeUrl.generate('control_plan/list', current_user, product_id: id),
      schedule_url: NativeUrl.generate('control_plan/schedule', current_user, product_id: id),
      kpi_url: NativeUrl.generate('control_plan/kpi', current_user, product_id: id)
    }
  end

  def kpi_details
    details = {}
    # details['Frequency Respect Rate'] = kpi_for_freq_respect
    # details['Nonconformity Rate'] = kpi_for_nonconformity
    # details['CAP Close Rate'] = kpi_for_cap_close
    kpis = kpi_tracks.includes(:kpi_records)
    kpis.each do |track|
      details[track.name] = kpi_for(track)
    end

    { kpi_details: details, kpi_tracks: kpi_tracks.group_by(&:name) }
  end

  def kpi_for(track)
    track.kpi_records.select do |x|
      x.destroyed_at.nil?
    end.map { |x| { col1: x.created_at.strftime('%Y-%m-%d'), col2: x.value, id: x.id } }
  end

  def kpi_for_freq_respect
    ps = process_steps.includes(:plan_items, :cp_pending_frequencies)
    new_ps = ps.group_by { |step| step.step_number }
    result = []

    new_ps.each do |k, v|
      result << { col1: k, col2: v.map { |x| x.freq_respect_rate }.sum.fdiv(v.size).round(0) }
    end

    result
  end

  def kpi_for_nonconformity
    ps = process_steps.includes(:plan_items)
    new_ps = ps.group_by { |step| step.step_number }
    result = []

    new_ps.each do |k, v|
      result << { col1: k, col2: v.map { |x| x.nonconformity_rate }.sum.fdiv(v.size).round(0) }
    end
    result
  end

  def kpi_for_cap_close
    ps = process_steps.includes(:plan_items)
    new_ps = ps.group_by { |step| step.step_number }
    result = []

    new_ps.each do |k, v|
      result << { col1: k, col2: v.map { |x| x.cap_close_rate }.sum.fdiv(v.size).round(0) }
    end
    result
  end

  def nonconformity_rate
    # # old algo
    # ps = process_steps.includes(:plan_items)
    # new_ps = ps.group_by{|step| step.step_number}
    # new_ps.each do |k,v|
    # result << v.map{|x| x.nonconformity_rate}.sum.fdiv(v.size).round(0)
    # end
    # tot = process_steps.count

    cps = control_plans.includes(:plan_items)
    result = []

    result = cps.map(&:nonconformity_rate)

    tot = control_plans.count

    return 0 if tot.blank? || tot == 0

    result.sum.fdiv(tot).round(0)
  end

  def freq_respect_rate(freq_for = 'Decathlon', fresh_query = true)
    ps = if fresh_query
           process_steps.includes(:plan_items, :cp_pending_frequencies)
                        .map { |x| x.freq_respected?(freq_for) }.flatten
         else
           process_steps.map { |x| x.freq_respected?(freq_for, fresh_query) }.flatten
         end
    return 100 if ps.blank?

    (ps.select { |x| x == true }.count.to_f * 100 / ps.count).round(0)
  end

  def cap_close_rate
    cps = control_plans.map { |x| x.id }
    return 100 if cps.blank?

    caps = CapPoint.includes(:cap).where(caps: { ref_model: 'control_plan', ref_id: cps }).map do |x|
      x.resolved_at.present?
    end
    # TODO: getting an error here that prevents some execution, not sure if this is the right way of handling it or not
    return 0 if caps.blank?

    (caps.select { |x| x == true }.count.to_f * 100 / caps.count).round(0)
  end

  def recent_changes(list_length)
    process_steps.order(updated_at: :desc)[0..(list_length - 1)].map do |step|
      s = step.versions.last
      if s.event == 'create'
        {
          id: step.id,
          abbr_name: "#{step.step_number} #{step.control_item}",
          step_number: step.step_number,
          control_item: step.control_item,
          date: s.created_at.strftime('%F'),
          whodunit: s.whodunnit,
          event: 'Created'
        }
      elsif s.event == 'update'

        changes = s.object_changes.present? ? YAML.load(s.object_changes) : []

        {
          id: step.id,
          abbr_name: "#{step.step_number} #{step.control_item}",
          step_number: step.step_number,
          control_item: step.control_item,
          date: s.created_at.strftime('%F'),
          whodunit: s.whodunnit,
          event: 'Updated',
          changes: changes.map { |x| { column: x[0], diff: x[1][1] } }
        }
      end
    end
  end

  def to_h
    serializable_hash.reject do |k, _|
      %w[created_at
         updated_at].include?(k)
    end.transform_keys!(&:to_sym).merge!({ process_flow_images: process_flow_images_urls })
  end

  def process_flow_images_urls
    host = DOMAIN
    process_flow_images.map { |x| Rails.application.routes.url_helpers.rails_blob_url(x, host: host) }
  end

  def available_count
    available_steps.count
  end

  def available_steps
    process_steps.where(active: true, destroyed_at: nil).order(step_number: :asc)
  end

  def reviewed_count
    reviewed_steps.count
  end

  def reviewed_steps
    # INCLUDE ALL REVIEWED
    process_steps.includes(:plan_items).where(active: true, destroyed_at: nil)
                 .where(plan_items: { completed: true })
  end

  def pending_count
    available_count - reviewed_count
  end

  def pending_steps
    base = process_steps.includes(:cp_pending_frequencies, :plan_items)
                        .where(active: true, destroyed_at: nil)

    unresolved = base.where(cp_pending_frequencies: { frequency_for: 'Decathlon',
                                                      resolved_at: nil, expires_at: (1.years.ago..1.weeks.from_now) })
                     .or(base.where.not(cp_pending_frequencies: { id: nil }))

    noks = base.where.not(plan_items: { id: nil })
               .select { |x| x.plan_items.order(:id).last.result == 'NOK' }

    (unresolved + noks).uniq.sort_by(&:step_number)
  end

  def next_review
    cps = control_plans.where(completed: false).order(assess_date: :asc)
    return nil if cps.blank?

    cps.first.assess_date.strftime('%Y-%m-%d')
  end

  def standard_hash
    serializable_hash.transform_keys do |x|
      x.to_sym
    end.reject! do |k, _v|
      %i[created_at
         updated_at].include?(k)
    end.merge!({ stepCount: process_steps.count, process_flow_images: process_flow_images_urls })
  end

  def last_cp
    return nil if control_plans.where(completed: true).empty?

    control_plans.where(completed: true).min_by { |x| x.schedule.supplier_schedule_date }
  end

  def process_map
    mapped_steps = process_steps.with_attachments.includes(:cp_pending_frequencies, :product_process)
                                .where(destroyed_at: nil).group_by { |x| x.product_process }.map do |k, v|
      z = v.sort_by { |x| x.step_number.to_i }.map do |y|
        y.standard_hash.merge({ checked: false, needs_cap: false })
      end

      { id: k.id, name: k.name, category: k.category, button: 'Check all', steps: z }
    end

    mapped_steps.sort_by { |h| h[:steps].first[:step_number].to_i }.group_by { |x| x[:category] }
  end
end
