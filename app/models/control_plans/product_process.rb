class ProductProcess < ApplicationRecord
  belongs_to :product_family
  has_many :process_steps, dependent: :destroy
  has_many :cp_pending_frequencies, through: :processes_steps
  has_paper_trail ignore: %i[created_at updated_at]

  validates :name, uniqueness: { scope: %i[product_family_id category],
                                 message: 'Cannot have repeated processes.' }

  CATEGORY = ['Incoming control', 'Online control', 'Final control', 'Lab test']

  def process_steps_active
    ProcessStep.where(product_process_id: id, destroyed_at: nil).order(step_number: :asc)
  end

  def standard_hash_with_steps
    serializable_hash.merge!({ steps: process_steps })
  end
end
