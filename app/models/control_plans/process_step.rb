class ProcessStep < ApplicationRecord
  has_one_attached :acceptable_image
  has_one_attached :unacceptable_image
  has_many_attached :requirement_images

  belongs_to :product_process
  has_paper_trail ignore: %i[created_at updated_at]
  has_many :plan_items, dependent: :destroy
  has_many :cp_pending_frequencies, dependent: :destroy
  has_many :cap_points, -> { where(ref_model: 'process_step') }, foreign_key: 'ref_id'
  after_save :init_cp_pending_frequency
  scope :with_attachments, -> {
                             with_attached_acceptable_image.with_attached_unacceptable_image.with_attached_requirement_images
                           }

  CONTROL_TYPES = ['Records & Method Control', 'Process Control', 'Product Control', 'Visual/Cosmetic Control',
                   'Lab Control']
  CONTROL_TYPES_SHORT = {
    'Records & Method Control' => 'R&M',
    'Process Control' => 'Process',
    'Product Control' => 'Product',
    'Visual/Cosmetic Control' => 'V/C',
    'Lab Control' => 'Lab'
  }

  def init_cp_pending_frequency
    if cp_pending_frequencies.where(frequency_for: 'Decathlon').blank? && decathlon_control_freq.present?
      return CpPendingFrequency.create(process_step_id: id, frequency_for: 'Decathlon',
                                       expires_at: Time.now + eval(decathlon_control_freq))
    end

    if cp_pending_frequencies.where(frequency_for: 'supplier').blank? && supplier_control_freq.present?
      CpPendingFrequency.create(process_step_id: id, frequency_for: 'supplier',
                                expires_at: Time.now + eval(supplier_control_freq))
    end
  end

  def to_h
    h = serial_hash
    decath = cp_pending_frequencies.sort_by(&:id).select do |x|
      (x.frequency_for == 'Decathlon') && x.resolved_at.nil?
    end.last
    supp = cp_pending_frequencies.sort_by(&:id).select do |x|
      (x.frequency_for == 'supplier') && x.resolved_at.nil?
    end.last
    h[:decathlon_freq_ddl] = decath.present? ? decath.expires_at.strftime('%Y-%m-%d') : '9999-12-31'
    h[:supplier_freq_ddl] = supp.present? ? supp.expires_at.strftime('%Y-%m-%d') : '9999-12-31'
    h[:decathlon_freq_respected] = Date.today < h[:decathlon_freq_ddl].to_date
    h[:supplier_freq_respected] = Date.today < h[:supplier_freq_ddl].to_date
    h
  end

  def active_freq
    set = cp_pending_frequencies.where(resolved_at: nil)
    if set.blank?
      init_cp_pending_frequency
    else
      set.first
    end
  end

  def freq_respected?(freq_for = 'Decathlon', _fresh_query = true)
    cp_pending_frequencies.select { |x| (x.frequency_for == freq_for) && !x.expires_at.nil? }
                          .map { |x| x.resolved_at.present? ? x.resolved_at < x.expires_at : Time.now < x.expires_at }
  end

  def freq_respect_rate(freq_for = 'Decathlon')
    ps = freq_respected?(freq_for)
    return 100 if ps.blank?

    (ps.select { |x| x == true }.count.to_f * 100 / ps.count).round(0)
  end

  def nonconformity_rate
    pl = plan_items
    nok = pl.where(result: 'NOK').count
    tot = pl.count

    return 0 if tot.blank? || tot == 0

    (nok * 100 / tot.to_f).round(0)
  end

  def cap_close_rate
    pls = plan_items.map { |x| x.id }
    return 100 if pls.blank?

    caps = CapPoint.where(ref_model: 'plan_item', ref_id: pls).map { |x| x.resolved_at.present? }
    return 100 if caps.blank?

    (caps.select { |x| x == true }.count.to_f * 100 / caps.count).round(0)
  end

  def acceptable_image_url
    host = DOMAIN
    acceptable_image.attached? ? Rails.application.routes.url_helpers.rails_blob_url(acceptable_image, host: host) : ''
  end

  def unacceptable_image_url
    host = DOMAIN
    if unacceptable_image.attached?
      Rails.application.routes.url_helpers.rails_blob_url(unacceptable_image,
                                                          host: host)
    else
      ''
    end
  end

  def requirement_images_urls
    host = DOMAIN
    requirement_images.map { |x| Rails.application.routes.url_helpers.rails_blob_url(x, host: host) }
  end

  def plan_items_finished?
    items = plan_items
    return false if items.blank?

    res = items.map { |x| x.completed }.uniq!
    if res.nil?
      false
    else
      res.include?(false) ? false : true
    end
  end

  def reviewed?
    plan_items.where(completed: true).present?

    # # MORE COMPLEX METHOD -- SAVE FOR LATER IF NEEDED
    # return false if cp_pending_frequencies.blank?

    # cp_pending_frequencies.where(frequency_for: "Decathlon", resolved_at: nil,
    #   expires_at: (Time.new(0)..5.days.from_now)).blank?
  end

  def last_review
    last = plan_items.where(completed: true).order(updated_at: :asc).last
    if last.nil?
      { assessor: 'N/A', reviewed_at: 'N/A', result: 'N/A' }
    else
      { assessor: last.plan_item_assessor.name, reviewed_at: last.updated_at.strftime('%Y-%m-%d'),
        result: last.result }
    end
  end
end
