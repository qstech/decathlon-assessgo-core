class ControlPlan < ApplicationRecord
  attr_reader :show_sections, :sections, :color, :cap_closed, :short_name,
              :grid, :previous_score, :official_assessment

  belongs_to :product_family
  belongs_to :assessor_profile, optional: true
  belongs_to :cap, optional: true
  belongs_to :user, class_name: 'User', foreign_key: 'created_by', optional: true

  has_many :plan_items, dependent: :destroy
  has_many :goes_activities, -> { where(ref_model: 'control_plan') }, foreign_key: 'ref_id'
  has_many :likes, -> { where(ref_model: 'control_plan') }, foreign_key: 'ref_id'
  has_many :favorites, -> { where(ref_model: 'control_plan') }, foreign_key: 'ref_id'

  has_one :schedule, foreign_key: 'ref_id'
  has_one_attached :report
  has_paper_trail ignore: %i[created_at updated_at]
  scope :active, -> { where(destroyed_at: nil) }
  scope :destroyed, -> { where.not(destroyed_at: nil) }
  scope :complete, -> { where(completed: true) }
  scope :include_supplier, -> { includes(product_family: :supplier_profile) }
  scope :include_assessor, -> { includes(:assessor_profile).where(assessor_type: 'assessor') }

  # ASSESS_TYPE = ['Fundamental', 'Advanced', 'CAP Review']
  ASSESS_TYPE = ['New Monitor', 'CAP Review', 'Implementation Order']
  # CONTROL_TYPES = ["Records & Method Control", "Process Control", "Product Control", "Visual/Cosmetic Control", "Lab Control"]
  include MessageScheduler
  include ControlPlanHelper::People
  include ControlPlanHelper::Relations
  include ControlPlanHelper::Radars
  include ControlPlanHelper::Utils
  include ControlPlanHelper::Calculations
  include ControlPlanHelper::Forms
  include ControlPlanHelper::Reports

  validates_uniqueness_of :slug
  before_create :generate_slug
  after_initialize :set_static_vars
  alias cap_closed? cap_closed
  alias cap_close? cap_closed
  alias official_assessment? official_assessment
  alias show_sections? show_sections
  alias creator user

  def supplier_users
    supplier_profile.users.map { |x| { id: "s#{x.id}", name: x.username } }
  end

  def set_static_vars
    @official_assessment = true
    @show_sections = false
    @sections = ''
    @color = '#A147C5'
    @cap_closed = false
    @short_name = 'CP'
    @grid = 'control_plan'
    @previous_score = nil
  end

  def can_edit?(_uid)
    true
  end

  def render_activity(user)
    goes_params = super
    goes_params[:ref_type] = plan_type
    # ONLY ON CREATE AND COMPLETE

    if completed?
      goes_params[:ref_status] = 'CP Monitor Completed!'
      goes_params[:content] =
        "#{user.name.split(' ').first.capitalize} has finished a Control Plan Monitor! Take a look and offer some helpful advice"
      GoesActivity.create(goes_params)
    elsif updated_at == created_at
      goes_params[:ref_status] = "CP #{plan_type} Scheduled!"
      goes_params[:content] = "#{user.name.split(' ').first.capitalize} has scheduled a new CP!"
      GoesActivity.create(goes_params)
    end
  end

  def to_h
    h = serializable_hash.transform_keys!(&:to_sym)
    h[:assess_date] = assess_date.strftime('%Y-%m-%d')
    h
  end
end
