class User < Profile
  # Include default devise modules. Others available are:
  # :confirmable, :timeoutable, :omniauthable, :validatable, :lockable,
  # attr_accessor :login
  devise :database_authenticatable, :registerable, :lockable,
         :recoverable, :rememberable, :trackable, :token_authenticatable

  validates :username, uniqueness: true
  belongs_to :supplier, optional: true, class_name: 'SupplierProfile', foreign_key: 'supplier_id'

  has_many :assessments, class_name: 'QualityAssessment', foreign_key: 'created_by'
  has_many :cps, class_name: 'ControlPlan', foreign_key: 'created_by'
  has_many :authentication_tokens, dependent: :destroy
  has_many :assessor_assignments
  has_many :goes_activities
  has_many :followers
  has_many :invitations, foreign_key: 'sender_id'
  has_many :comments
  has_many :favorites
  has_many :likes
  has_many :search_histories
  has_many :assessor_applications
  has_many :schedules, foreign_key: 'sender_id'

  has_one  :assessor_profile
  has_one  :supplier_profile
  accepts_nested_attributes_for :assessor_profile, reject_if: :all_blank, allow_destroy: true
  has_one_attached :avatar
  after_create :store_email
  after_create :seed_assessment!, unless: :assessor?
  has_paper_trail ignore: %i[created_at updated_at]

  mattr_accessor :next_sync

  include MessageScheduler

  EMAIL_ASSESSOR_TEMPLATES = {
    'oa_invite' => 'Official account invitation',
    'thanks_followup' => 'Follow up thank you'
  }

  EMAIL_SUPPLIER_TEMPLATES = {
    'welcome' => 'Welcome',
    'email_changed' => 'Email has changed',
    '5_no_login' => '5 Days w/out logging in',
    '5_login' => '5 Days follow up',
    '10_to_50' => '10 To 50 days follow up',
    'reset_password' => 'Reset password',
    'oa_invite' => 'Official account invitation',
    'thanks_followup' => 'Follow up thank you'
  }

  def self.destroy_duplicates!
    User.group(:username).count.select { |_, v| v > 1 }.each { |k, _| User.where(username: k)[1..].each(&:destroy) }
  end

  def seed_assessment!
    QualityAssessment.make_one!(self) unless profile.nil?
  end

  def active?(date = nil)
    if date.nil?
      assessments.present? || cps.present?
    else
      d = Time.parse(date)
      assessments.select { |x| x.created_at < d }.present? || cps.select { |x| x.created_at < d }.present?
    end
  end

  def technical?
    can_manage?('technical') || username.in?(%w[testuser srivas09 kkang20 hyu50])
  end

  def authorized_developer?
    username.in?(%w[z09sriva])
  end

  def can_edit?(object)
    case object.class.name
    when 'QualityAssessment'
      object.can_edit?(id)
    when 'AssessmentDayForm'
      object.quality_assessment.can_edit?(id)
    when 'ChapterAssessment'
      object.quality_assessment.can_edit?(id)
    when 'PrepQuestion'
      object.can_edit?(id)
    when 'ChapterQuestion'
      object.chapter_assessment.quality_assessment.can_edit?(id)
    when 'PlanItem'
      object.control_plan.can_edit?(id)
    when 'SupplierProfile'
      decathlon?
    else
      true
    end
  end

  def change_password?
    (valid_password?('Decathlon') || valid_password?('Decathl0n')) && username.include?('@')
  end

  def name
    @name ||= profile.present? ? profile.name : username
    @name || username
  end

  def must_invite_assessor?(ref_model)
    return true unless assessor?
    return false if ref_model == 'hrp_assessment'
    return true if assessor_profile.assessor_for.blank?

    short_name = QualityAssessment::SHORT_NAMES[ref_model]
    !assessor_profile.assessor_for.include?(short_name)
  end

  def latest_activity_for_favorites
    return [] if favorites.blank?

    query = ''
    favorites.each do |fav|
      query += "(ref_model LIKE '#{fav.ref_model}' AND ref_id = #{fav.ref_id}) OR "
    end
    fav_cards = GoesActivity.where(query[0..-4]).order(updated_at: :desc)
                            .group_by { |x| "#{x.ref_model}:#{x.ref_id}" }.values.map { |x| x.first }
  end

  def favorited?(obj)
    favorites.where(ref_model: obj.class.model_name.singular, ref_id: obj.id).present?
  end

  def fav_for(obj)
    favorites.where(ref_model: obj.class.model_name.singular, ref_id: obj.id).first
  end

  def liked?(obj, *type)
    like_for(obj, type).present?
  end

  def like_for(obj, *type)
    like_objs = obj.likes.select { |like| like.user_id == id }
    like_objs = like_objs.select { |like| like.ref_type == type } if type.present?
    like_objs.first
  end

  def must_set_assessor
    # if current_user is an OPMSD of any HRP && any of these HRP has no pending invite NOR assessor assignment
    hrps = QualityAssessment.where(opmsd: id).map { |x| x.id }
    # invite_present = true if hrps.blank?
    need_invite = []
    hrps.each do |x|
      invite = Invitation.where(ref_model: 'quality_assessment', ref_id: x).first
      if invite.nil?
        need_invite << true
      elsif invite.present? && invite.rejected
        need_invite << true
      end
    end

    hrps.present? && need_invite.present?
  end

  def received_invitations
    @received_invitations ||= Invitation.where(recipient_id: id, accepted: false, rejected: false, destroyed_at: nil)
  end

  def pending_invitations?(grid)
    return false if received_invitations.blank?

    QualityAssessment.where(id: received_invitations.map(&:ref_id), grid: grid).present?
  end

  def sent_invitations
    Invitation.where(sender_id: id, accepted: false, rejected: false, destroyed_at: nil)
  end

  def quality_assessments
    prof = assessor_profile
    base = QualityAssessment.joins(:assessment_day_form, :supplier_profile).valid
    assign_ids = assessor_assignments.where(ref_model: 'quality_assessment').map(&:ref_id)

    if prof.present?
      puts 'is assessor'
      puts 'Loaded assignments'
      # NOTE: FOR JOINS to work with OR, the SAME JOINS must be used in ALL conditions!
      base.where(created_by: id).or(base.where(id: assign_ids))
          .or(base.where("'#{id}' = ANY (prepped_by) OR '#{id}' = ANY (assessed_by) OR '#{assessor_profile.id}' = ANY (people_concerned)"))
          .or(base.where(assessment_day_forms: { pl_id: prof.id }))
          .or(base.where("'#{prof.id}' = ANY (assessment_day_forms.observers)"))
          .or(base.where("'#{prof.id}' = ANY (assessment_day_forms.pl_set)"))
          .or(base.where("'#{prof.id}' = ANY (supplier_profiles.pl_set)"))
    elsif supplier_id.present?
      base.where(created_by: id).or(base.where(id: assign_ids))
    elsif supplier_profile.present?
      base.where(created_by: id).or(base.where(id: assign_ids))
    else
      QualityAssessment.where(id: nil)
    end
  end

  def relavent_assessments(grid)
    base         = QualityAssessment.joins(:assessment_day_form, :supplier_profile).valid
    invit_base   = QualityAssessment.joins(:invitations)
    sent_invites = invit_base.where(invitations: { sender_id: id, accepted: false })
                             .where(completed: false, destroyed_at: nil, grid: grid)
    accepted_qa  = invit_base.where(invitations: { recipient_id: id, accepted: true })
                             .where(completed: false, destroyed_at: nil, grid: grid)
    qas = quality_assessments.or(base.where('? = ANY(assessment_day_forms.observers)', id.to_s))
                             .or(base.where(opmsd: id))
                             .where(completed: false, destroyed_at: nil, grid: grid)

    (qas + sent_invites + accepted_qa).uniq
  end

  def control_plans
    base = ControlPlan.joins(product_family: :supplier_profile)

    if assessor?
      base.where(assessor_profile_id: assessor_profile.id, destroyed_at: nil)
          .or(base.where("'#{assessor_profile.id}' = ANY (people_concerned)").where(destroyed_at: nil))
          .or(base.where("'#{assessor_profile.id}' = ANY (supplier_profiles.pl_set)").where(destroyed_at: nil))
    else
      # ControlPlan.include_supplier.where(supplier_profiles: {user_id: id}, control_plans: {destroyed_at: nil})
      ControlPlan.joins(:schedule).where(schedules: { sender_id: id }, control_plans: { destroyed_at: nil })
    end
  end

  def cp_counts
    control_plans.where(completed: false).count
  end

  def suppliers
    if assessor?
      qa_supp_ids = quality_assessments.where(destroyed_at: nil).map { |x| x.supplier_profile_id }

      cp_supp_ids = ProductFamily.includes(:control_plans).select(:supplier_profile_id)
                                 .where(control_plans: { assessor_profile_id: assessor_profile.id, destroyed_at: nil })
                                 .map { |x| x.supplier_profile_id }

      SupplierProfile.where(id: qa_supp_ids + cp_supp_ids).uniq
    else
      []
    end
  end

  def product_families
    if assessor?
      sids = suppliers.map { |x| x.id }
      ProductFamily.where(supplier_profile_id: sids)
    elsif supplier_id.present?
      ProductFamily.where(supplier_profile_id: supplier_id)
    else
      ProductFamily.where(supplier_profile_id: supplier_profile.id)
    end
  end

  def frequency_respect_rate
    freqs = product_families.map { |x| x.freq_respect_rate }
    return freqs.sum / freqs.count if freqs.count > 0

    0
  end

  def cap_close_rate
    return 0
    cap_rates = product_families.map { |x| x.cap_close_rate }.uniq.reject { |x| x.blank? }
    return 0 if cap_rates.blank?

    cap_rates.sum / cap_rates.count
  end

  def nonconformity_rate
    pls = PlanItem.includes(:control_plan).where(control_plans: { assessor_profile_id: assessor_profile.id,
                                                                  assessor_type: 'assessor', completed: true })
    puts "TOTAL: #{pls.count}"
    puts "TOTAL NOK: #{pls.where(result: 'NOK').count}"
    puts "TOTAL OK: #{pls.where(result: 'OK').count}"
    return 0 if pls.count == 0

    (pls.where(result: 'NOK').count.to_f * 100 / pls.where(result: %w[OK NOK]).count).round(2)
  end

  def assessors
    if supplier?
      assessments = quality_assessments
      assessors = assessments.prepped_by + assessments.assessed_by
      assessors.uniq
      User.where('id = ?', assessors)
    else
      []
    end
  end

  def invited?
    invites = Invitation.where(recipient_id: id, destroyed_at: nil)
    if invites.present?
      invites.map { |x| x.accepted == false && x.rejected == false }.include?(true)
    else
      false
    end
  end

  def invitation_cards(grid)
    ids = QualityAssessment.where(id: received_invitations.map(&:ref_id), grid: grid).map(&:id)

    received_invitations.select { |x| x.ref_id.in?(ids) }.map { |x| PageComponent.invitation_card(x) }
  end

  def invitation_cards_to_send(_grid = 'hrp_assessment')
    return [] unless must_set_assessor

    hrps = QualityAssessment.where(opmsd: id).select { |x| (x.invitations.blank? || x.invitations.last.rejected) }
    # need_invite = []
    # hrps.each { |x|
    #   invites = Invitation.where(ref_model: 'quality_assessment', ref_id: x.id)
    #   if invites.blank?
    #     need_invite << x
    #   elsif invites.last.rejected
    #     need_invite << x
    #   end
    # }
    hrps.map { |x| PageComponent.future_qa_card(x) }
  end

  def role
    if internal_assessor?
      :internal_assessor
    elsif supplier?
      :supplier
    else
      :assessor
    end
  end

  def internal_assessor?
    return false if assessor?

    supplier? && profile&.internal_assessors&.include?(username)
  end

  def assessor?
    assessor_profile.present?
  end

  def supplier?
    supplier_id.present? || supplier_profile.present?
  end

  def decathlon?
    supplier_id.nil? && !username.include?('@') && assessor_profile.present?
  end

  def candidate?
    assessor? && assessor_profile.candidate_for.present?
  end

  def candidate_for?(grid)
    assessor? && assessor_profile.candidate_for.include?(grid)
  end

  def referent?
    assessor? && assessor_profile.referent_for.present?
  end

  def referent_for?(grid)
    assessor? && assessor_profile.referent_for.include?(grid)
  end

  def profile
    @profile ||= (assessor_profile || supplier || supplier_profile)
  end

  def following_ids
    User.joins(assessor_profile: :followers).where(followers: { user_id: id }).map(&:id)
  end

  def following
    Follower.where(user_id: id)
  end

  def following_hash
    Follower.where(user_id: id).map { |_x| { id: f.id, avatar: f.ref.avatar || PageComponent::AVATAR[:user] } }
  end

  def followers
    prof = profile
    ref_model = prof.model_name.singular
    ref_id = prof.id
    Follower.where(ref_model: ref_model, ref_id: ref_id)
  end

  def follower_hash
    prof = profile
    ref_model = prof.model_name.singular
    ref_id = prof.id
    Follower.where(ref_model: ref_model, ref_id: ref_id).map do |f|
      { id: f.id, avatar: f.follower.wx_avatar || PageComponent::AVATAR[:user] }
    end || []
  end

  def store_email
    update(email: username) if username.include?('@')
  end

  def user_hash
    assessor_for = assessor? ? assessor_profile.assessor_for : []
    {
      id: id,
      username: username,
      name: profile.name,
      profile: profile.serial_hash,
      last_popup: last_popup,
      avatar: wx_avatar.nil? ? '' : wx_avatar,
      openid: wx_openid.nil? ? '' : wx_openid,
      unionid: unionid.nil? ? false : unionid,
      locale: locale,
      assessor: assessor?,
      decathlon: decathlon?,
      candidate: candidate?,
      assessor_for: assessor_for
    }
  end

  def following?(obj)
    Follower.where(ref_model: obj.model_name.singular, ref_id: obj.id, user_id: id).present?
  end

  def qa_counts
    qa_counts = Hash.new(0)
    qas = quality_assessments.where(completed: false, destroyed_at: nil) || []
    qas.each do |qa|
      grid = qa.grid == '' ? 'quality_assessments' : qa.grid # for backwards compat
      qa_counts[grid] += 1
    end

    invites = Invitation.where(recipient_id: id, accepted: false, rejected: false, destroyed_at: nil,
                               ref_model: 'quality_assessment') || []
    inv_set = invites.map { |x| x.ref_id }
    cap_invites = Invitation.where(recipient_id: id, accepted: false, rejected: false, destroyed_at: nil,
                                   ref_model: 'cap_followup') || []
    p cap_invites
    cap_invites = cap_invites.map do |x|
                    x.ref.cap_resolution.cap_point.cap
                  end.select { |cap| cap.ref_model == 'quality_assessment' }
    p cap_invites
    cap_invites.reject!(&:nil?)
    cap_invites.map!(&:ref_id)
    p cap_invites

    ids = inv_set + cap_invites
    p ids

    qa2s = QualityAssessment.where(id: ids)
    qa2s.each do |qa|
      grid = qa.grid == '' ? 'quality_assessment' : qa.grid # for backwards compat
      qa_counts[grid] += 1
    end
    qa_counts
  end

  def all_comments
    (comments.includes(:reply_user) + received_comments).uniq
  end

  def received_comments
    ids = quality_assessments.map { |x| x.id }
    set1 = Comment.with_users.join_chapters
                  .where(chapter_assessments: { quality_assessment_id: ids }, comments: { ref_model: 'chapter_assessment',
                                                                                          destroyed_at: nil })
    set2 = Comment.with_users.join_activities.where(goes_activities: { ref_id: ids, ref_model: 'quality_assessment' },
                                                    comments: { ref_model: 'goes_activity', destroyed_at: nil })
                  .or(Comment.with_users.join_activities.where(goes_activities: { user_id: id },
                                                               comments: { ref_model: 'goes_activity',
                                                                           destroyed_at: nil }))
    set3 = Comment.with_users.where(comments: { reply_to: id })
    set1 + set2 + set3
  end

  def received_comments_count
    received_comments.count
  end

  def received_unread_comments_count
    received_comments.select { |x| !x.receiver_read }.count
  end

  def received_comments_mark_read
    received_comments.select { |x| !x.receiver_read }.each { |x| x.update(receiver_read: true) }
  end

  def mailbag_params
    { name: name, email: email }
  end

  def send_mail!(type)
    case type
    when 'thanks'
      return true if username.include?('@')

      if last_thanks.nil? || last_thanks < (Time.now - 3.days)
        MailBag.trigger_by_tag!('thanks_followup', self, mailbag_params)
        update(last_thanks: Time.now)
      end
    when 'welcome'
      MailBag.trigger_by_tag!('welcome', self, mailbag_params)
    when 'email_changed'
      MailBag.trigger_by_tag!('email_changed', self, mailbag_params)
    when '5_no_login'
      MailBag.trigger_by_tag!('followup_5_no_login', self, mailbag_params)
    when '5_login'
      MailBag.trigger_by_tag!('followup_5_login', self, mailbag_params)
    when '10_to_50'
      MailBag.trigger_by_tag!('followup_10_to_50', self, mailbag_params)
    when 'reset_password'
      MailBag.trigger_by_tag!('reset_password', self, mailbag_params)
    when 'oa_invite'
      MailBag.trigger_by_tag!('oa_invite', self, mailbag_params)
    end
  end

  # LARGE USAGE STATS
  def self.daily_logins(i = 1)
    User.includes(:authentication_tokens).where(authentication_tokens: { last_used_at: (i.days.ago..Time.now) }).count
  end

  def self.country_count
    AssessorProfile.includes(:user,
                             user: :authentication_tokens).where.not(user_id: nil).where(authentication_tokens: { created_at: (1.years.ago..Time.now) }).group(:country).count
  end

  def can_manage?(level)
    access_levels.include?(level) || admin
  end

  def avatar_url
    return nil if avatar.attachment.blank?

    host = DOMAIN
    Rails.application.routes.url_helpers.rails_blob_url(avatar, host: host)
  end

  def pending_applications
    prof = assessor_profile
    return [] if prof.blank?

    base = AssessorApplication.includes(:validations)
    base.where(opm_id: prof.id, opm_confirmed: nil)
        .or(base.where(team_leader_id: prof.id, team_leader_confirmed: nil))
        .or(base.where(assessor_applications: {
                         validator_id: prof.id, opm_confirmed: true, team_leader_confirmed: true
                       }, validations: { co_animated: false, result: nil }))
  end

  def pending_application
    pending_applications.first
  end

  def validations
    return [] if assessor_profile.blank?

    base = AssessorApplication.includes(user: :assessor_profile)

    base.where(validator_id: assessor_profile.id)
        .or(base.where(opm_id: assessor_profile.id))
        .or(base.where(team_leader_id: assessor_profile.id))
        .map do |x|
      {
        avatar: x.user.avatar_url || PageComponent::AVATAR[:user],
        id: x.user.assessor_profile.id,
        name: (x.user.assessor_profile.name || x.user.assessor_profile.handle).truncate(10),
        application: x.standard_hash,
        url: "/members/show/show?id=#{x.user.assessor_profile.id}"
      }
    end
  end

  def self.sync_oa!
    self.next_sync = Time.now if next_sync.nil?
    return true if next_sync > Time.now

    self.next_sync = 1.days.from_now
    users = User.where.not(unionid: nil)
    subscribers = WxNotifier.fetch_users['data']['openid']
    i = 0
    puts "-----------#{subscribers.count} users subscribed"
    subscribers.each do |openid|
      next if users.select { |u| u.oa_openid == openid }.present?

      unionid = WxNotifier.fetch_unionid(openid)
      user = users.find { |u| u.unionid == unionid }
      if user.present? && user.update(oa_openid: openid)
        i += 1
        puts "SYNCED #{i} USERS"
      end
    end
    puts "------------OA sync done: synced #{i} accounts"
    i
  end

  def self.invite_oa!
    sync_oa!
    users = User.where(unionid: nil)
    users.each do |u|
      u.send_mail!('oa_invite')
      u.update(last_oa_invite: Time.now)
    end
    puts "---------OA Invites sent to #{users.count} accounts"
    users.count
  end

  def test!(obj)
    obj
  end

  def next_assessment_notify!(params, next_assess_type, qa, log)
    supp = "#{qa.supplier_profile.code} #{qa.supplier_profile.name}"
    openid = oa_openid

    template = params[:template]
    puts "current template ===>>> #{template}"
    future_qa = QualityAssessment.find(params[:future_qa_id].to_i) if params[:future_qa_id].present?
    return true if future_qa.present? && future_qa.completed

    redirect = future_qa.present? ? future_qa.mp_path(:assess) : 'members/home/home'

    case template
    when 'completed'
      return true
    when 'assessment_not_scheduled'
      redirect = "members/assessment/index?ref_model=#{qa.grid}"
      title = "Please schedule a new #{qa.short_name} #{next_assess_type} assessment for #{supp}"
      header = 'Schedule an assessment!'
    when 'assessor_not_assigned'
      title = "Please assign an assessor for #{qa.short_name} #{next_assess_type} assessment for #{supp}"
      header = 'Assign an assessor!'
    when 'prep_questions_not_finished'
      title = "Please complete prep questions for #{qa.short_name} #{next_assess_type} assessment for #{supp}"
      header = 'Complete prep questions for upcoming assessment'
    when 'assessment_is_approaching'
      title = "#{qa.short_name} #{next_assess_type} assessment for #{supp} is coming up soon!"
      header = 'Assessment is coming soon'
    end

    note_params = {
      'openid' => openid,
      'redirect' => redirect,
      'title' => title,
      'header' => header,
      'assess_date' => log.next_assess_date.strftime('%Y-%m-%d'),
      'user_id' => id
    }
    # run WxNotifier
    if openid.present?
      wx_params = WxNotifier.send(template.to_sym, note_params)
      puts "wx_params ==>> #{wx_params}"
      WxNotifier.notify!(wx_params)
    end
    # run Mailer
    UserMailer.with(object: qa, email_params: note_params).notification.deliver_later
  end
end
