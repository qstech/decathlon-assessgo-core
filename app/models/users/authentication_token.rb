class AuthenticationToken < ApplicationRecord
  belongs_to :user
  has_paper_trail ignore: %i[created_at updated_at]

  include MessageScheduler
end
