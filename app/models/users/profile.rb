require 'iso_country_codes'
class Profile < ApplicationRecord
  # COMMON METHODS FOR ASSESSOR & SUPPLIER PROFILE
  self.abstract_class = true

  def self.included(base)
    super
    base.extend self
  end

  # INSTANCE METHODS
  def country_name
    set = { 'VN' => 'Vietnam' }
    return nil unless methods.include?(:country)
    return 'Not Set' if ['', nil].include? country

    set[country] || IsoCountryCodes.find(country).name
  rescue IsoCountryCodes::UnknownCodeError
    country
  end

  def country_index
    return nil unless methods.include?(:country)
    return 0 if ['', nil].include? country

    i = User.country_values.index(country)
    i = User.country_names.index(country) if i.nil?
    puts "COUNTRY INDEX: #{i}"
    i
  rescue IsoCountryCodes::UnknownCodeError
    nil
  end

  def clean_country
    str = country.strip
    self.country = self.class.country_search(str)
  end

  def last_login
    return nil if self.class != User && user_id.nil?

    user = instance_of?(User) ? self : self.user
    tokens = user.authentication_tokens.sort_by(&:created_at)
    return nil if tokens.blank?

    tokens.last.last_used_at
  end

  # CLASS METHODS
  class << self
    def country_names
      return IsoCountryCodes.all.map { |x| x.name } unless new.methods.include?(:country)

      country_values.map { |x| IsoCountryCodes.find(x).name }
    end

    def country_values
      return IsoCountryCodes.all.map { |x| x.alpha2 } unless new.methods.include?(:country)

      full_set = IsoCountryCodes.all.map { |x| x.alpha2 }

      group(:country).count.keys.select { |x| full_set.include?(x) }.sort
    end

    def all_countries
      return IsoCountryCodes.all.map { |x| { label: x.name, value: x.alpha2 } } unless new.methods.include?(:country)

      country_values.map { |x| IsoCountryCodes.find(x) }.map { |x| { label: x.name, value: x.alpha2 } }
    end

    def country_search(str)
      return nil if ['', nil].include? str

      str.strip!
      h = IsoCountryCodes.all.map { |x| [x.name, x.alpha2] }.to_h
      j = IsoCountryCodes.all.map { |x| [x.alpha2, x.alpha2] }.to_h
      k = (1..5).to_a.map { |i| ["C#{i}", 'CN'] }.to_h
      h.merge!(j)
      h.merge!(k)
      h[str] || str
    end
  end
end

# ============================================================================
# Alternative Method for inheritance of instance & class methods
#           => Good for long list of class methods
# =========================
# def self.included base
#   base.send :include, InstanceMethods
#   base.extend ClassMethods
# end

# module InstanceMethods
#   def bar1
#     'bar1'
#   end
# end

# module ClassMethods
#   def bar2
#     'bar2'
#   end
# end
