class AssessorProfile < Profile
  has_one_attached :avatar
  validates_uniqueness_of :handle, allow_blank: true, allow_nil: true

  has_many :control_plans
  has_many :followers, -> { where(ref_model: 'assessor_profile') }, foreign_key: 'ref_id'
  belongs_to :user, optional: true
  has_paper_trail ignore: %i[created_at updated_at]

  before_save :cleanup_names
  before_create :clean_country, if: :country

  def username
    handle
  end

  def cc_list
    [AssessGoApi::PO, AssessGoApi::IT]
  end

  def cleanup_names
    @name = name.titleize if name.present?
    @handle = handle.downcase if handle.present?
  end

  def product_families
    return ProductFamily.where(id: nil) if user_id.blank?

    sids = user.suppliers.map(&:id)
    ProductFamily.where(supplier_profile_id: sids)
  end

  def frequency_respect_rate
    return 0 if user_id.blank?

    sids = user.suppliers.map(&:id)
    pfs = ProductFamily.includes(:process_steps,
                                 process_steps: :cp_pending_frequencies).where(product_families: { supplier_profile_id: sids })
    cp_pending_frequencies = pfs.map { |x| x.process_steps.map { |y| y.cp_pending_frequencies } }.flatten

    freqs = cp_pending_frequencies.reject do |x|
              x.expires_at.nil?
            end.map { |x| x.resolved_at.present? ? x.resolved_at < x.expires_at : Time.now < x.expires_at }

    return 100 if freqs.blank?

    (freqs.select { |x| x == true }.count.to_f * 100 / freqs.count).round(0)
  end

  def cap_close_rate
    return 0 if user_id.blank?

    cap_rates = product_families.includes(:control_plans)
                                .map { |x| x.cap_close_rate }.uniq.reject { |x| x.blank? }
    return 0 if cap_rates.blank?

    (cap_rates.sum / cap_rates.count).to_i
  end

  def nonconformity_rate
    return 0 if user_id.blank?

    pls = PlanItem.includes(:control_plan).where(control_plans: { assessor_profile_id: id, completed: true })

    return 0 if pls.where(result: %w[OK NOK]).count == 0

    (pls.where(result: 'NOK').count.to_f * 100 / pls.where(result: %w[OK NOK]).count).to_i
  end

  def display_name
    return name if user.nil?

    if user.supplier || user.supplier_profile
      supp = user.supplier || user.supplier_profile
      dname = name
      dname += " (#{supp.short_name})" if supp.short_name.present?
      dname
    else
      name
    end
  end

  # CLASS METHODS FOR CALCULATING MACRO STATS / RANKINGS
  class << self
    def source_list(grid = nil)
      AssessorProfile.includes(user: %i[supplier supplier_profile])
                     .where.not(user_id: nil, name: nil)
                     .map do |x|
        {
          id: x.id.to_s,
          name: x.display_name,
          candidate: x.candidate_for.include?(grid),
          assessor: x.assessor_for.include?(grid)
        }
      end
    end

    def referent_list
      h = {}
      AssessorProfile.includes(user: %i[supplier supplier_profile])
                     .where.not(referent_for: [], user_id: nil, name: nil).each do |ass|
        ass.referent_for.each do |grid|
          h[grid] = [] if h[grid].blank?
          h[grid] << { id: ass.id.to_s, name: ass.display_name, candidate: false, assessor: true }
        end
      end
      h
    end

    def most_assessments_completed(from_date = 1.months.ago, to_date = Time.now, n = 10)
      QualityAssessment.joins(assessor_assignments: { user: :assessor_profile })
                       .where(assessor_assignments: { primary: true, ref_model: 'quality_assessment' },
                              quality_assessments: { completed: true, assess_date: from_date..to_date })
                       .group('assessor_profiles.name').count.sort_by { |x| x[1] }.reverse
                       .reject do |x|
        (['Rivas Sergio Alberto', 'Kang Kerry',
          'Haidee Yu'].include?(x[0]) || x[0].blank?)
      end [0..(n - 1)].to_h
    end

    def most_assessments_planned(from_date = 1.months.ago, to_date = Time.now, n = 10)
      QualityAssessment.joins(user: :assessor_profile)
                       .where(quality_assessments: { created_at: from_date..to_date })
                       .group('assessor_profiles.name').count.sort_by { |x| x[1] }.reverse
                       .reject do |x|
        (['Rivas Sergio Alberto', 'Kang Kerry',
          'Haidee Yu'].include?(x[0]) || x[0].blank?)
      end [0..(n - 1)].to_h
    end

    def most_active(from_date = 1.months.ago, to_date = Time.now, n = 10)
      AuthenticationToken.left_outer_joins(user: %i[assessor_profile supplier_profile])
                         .where(authentication_tokens: { created_at: from_date..to_date })
                         .group('CASE WHEN assessor_profiles.name IS NOT NULL THEN assessor_profiles.name ELSE users.username END').count.sort_by { |x| x[1] }.reverse
                         .reject do |x|
        (['Rivas Sergio Alberto', 'Kang Kerry',
          'Haidee Yu'].include?(x[0]) || x[0].blank?)
      end [0..(n - 1)].to_h
    end
  end
end
