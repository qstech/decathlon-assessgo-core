require 'csv'
class SupplierProfile < Profile
  belongs_to :user, optional: true
  has_many :users, foreign_key: 'supplier_id'
  has_many :followers, -> { where(ref_model: 'supplier_profile') }, foreign_key: 'ref_id'

  has_many :quality_assessments
  has_many :product_families, -> { where(destroyed_at: nil) }
  has_many :control_plans, through: :product_families
  has_many :cps, -> { where(assessor_type: 'supplier') }, class_name: 'ControlPlan', foreign_key: 'assessor_profile_id'
  has_one_attached :avatar
  has_paper_trail ignore: %i[created_at updated_at]

  validates :code, uniqueness: true
  validates :contact_email, uniqueness: true, format: { with: /\A[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,64}\z/,
                                                        message: 'not acceptable email format' }, allow_blank: true, allow_nil: true
  before_create :generate_code
  before_create :set_status
  before_create :clean_country, if: :country
  before_save :attach_user
  after_save :seed_assessment!
  before_validation :clear_blanks
  before_save :update_grids_available
  before_save :check_closed
  after_save :lock_users, if: :closed
  scope :with_plan_items, -> { includes(product_families: { plan_items: [:control_plan] }) }

  INDUSTRIAL_PROCESSES = ['Not Set', 'TENT & SDC', 'ELECTRONICS PROCESS', 'FOAM', 'WOVEN',
                          'PROTEK', 'SYNTHETIC KNIT', 'METAL', 'HEADWEAR & SWEATERS', 'BIKES', 'BAGS',
                          'OPTICS', 'PLASTIC & COMPOSITE', 'ACCESSORIES BU', 'NATURAL KNIT & FLEECE',
                          'PACE MIGRATION', 'COMPOSITES WEDZE', 'WATER THERMAL INSULATION',
                          'FOOTWEAR INDUSTRIAL PROCESS', 'WELDING', 'SOCKS', 'NUTRITION', 'BALLS',
                          'PACKAGING', 'ROBOOST STITCHING', 'LIGHTING PROCESS', 'GLOVES', 'CHEMISTRY & CARE',
                          'HELMET', 'BOARDS', 'SAFETY STITCHING', 'NATURAL YARN PROCESS',
                          'SYNTHETIC YARN PROCESS', 'FLOTATION DEVICE', 'STORE FITTING', 'SEAMLESS',
                          'RFID BUSINESS UNIT', 'PYROTECHNY & ACCESSORIES', 'SIMOND']

  STATUS_CHOICES = ['Opening Supplier', 'Active Supplier', 'Closed SDB Code'].map { |x| { text: x, value: x } }
  RANK_CHOICES = ['Unranked', 'Rank 1', 'Rank 2'].map { |x| { text: x, value: x } }
  TARGET_CHOICES = ('A'..'E').to_a.push('N/A').map { |x| { text: x, value: x } }

  def all_users
    @all_users ||= user_id.nil? ? users : User.where(supplier_id: id).or(User.where(id: user_id))
  end

  def full_address
    "#{address}, #{city}, #{country}"
  end

  def pl_names
    pl_set.blank? ? [] : AssessorProfile.where(id: pl_set).map(&:name)
  end

  def opm_names
    opm_set.blank? ? [] : AssessorProfile.where(id: opm_set).map(&:name)
  end

  def ptm_names
    ptm_set.blank? ? [] : AssessorProfile.where(id: ptm_set).map(&:name)
  end

  def username
    code
  end

  def decathlon_users_complete?
    opm_set.present? && pl_set.present? && ptm_set.present?
  end

  def self.filter_by_grid_and_date(grid, date)
    includes(:quality_assessments)
      .where(quality_assessments: { grid: grid })
      .where('quality_assessments.assess_date >= ?', date)
      .references(:quality_assessments)
  end

  def check_closed
    self.closed = true if status == 'Closed SDB Code'
  end

  def lock_users
    user.update(locked_at: Time.now) if user_id.present?
    users.update_all(locked_at: Time.now)
  end

  def update_grids_available
    if grids_available_changed?
      users.update_all(grids_available: grids_available)
      user.update(grids_available: grids_available) if user_id.present?
    end
  end

  def seed_assessment!
    user.seed_assessment! if user.present? && user.quality_assessments.blank? && !assessor_provider
  end

  def users_concerned
    all_ids = [pl_set, opm_set, ptm_set].flatten.uniq
    return [] if all_ids.blank?

    User.includes(:assessor_profile).where(assessor_profiles: { id: all_ids })
  end

  def send_ib_email!(*opts)
    UserMailer.with(supplier: self).sdb_email_update.deliver_later(*opts) if ib_emails.present?
  end

  def cc_list
    cc = [AssessGoApi::PO, AssessGoApi::IT]
    rel = users_concerned
    cc += secondary_users if secondary_users.present?
    cc << contact_email if contact_email.present?

    cc += rel.map(&:email)
    cc
  end

  def clear_blanks
    self.contact_name = nil if contact_name.blank? || contact_name == 'Not Set'
    self.contact_email = nil if contact_email.blank? || contact_email == 'Not Set' || !contact_email.include?('@')
    self.industrial_process = nil if industrial_process.blank? || industrial_process == 'Not Set'
  end

  def supplier_type
    if finished_goods_supplier
      'Finished Goods Rank 1'
    elsif component_supplier
      'Component Rank 1'
    else
      'Strategic Rank 2'
    end
  end

  def supplier_status
    active ? 'Active' : 'In Sourcing'
  end

  def self.batch_ib_emails!
    batches = SupplierProfile.where.not(ib_emails: []).order(:id).in_groups_of(400)
    batches.each_with_index do |batch, i|
      batch.delete(nil)
      puts "BATCH #{i + 1}"
      puts batch.map { |x| "#{x.code} #{x.name}" }
      batch.each_with_index do |supp, j|
        time = Time.now + i.days + (j + 1).minutes
        Rails.cache.fetch("spl_email/#{supp.code}") do
          supp.send_ib_email!(wait_until: time)
          'SCHEDULED'
        end
      end
    end
  end

  def self.setup_assessors
    SupplierProfile.where(assessor_provider: true).each { |x| x.create_assessors! }
  end

  def self.start_mail_worker!
    # NOTE: ONLY DO THIS ONCE. Most likely it's already been done. So don't do it.
    # Really. Probably a bad idea to try this out. You might start sending emails to
    # REAL users by accident. Not a good idea. Bad PR. Really. Don't do this.

    # Start the MailScheduleWorker.
    # DANGER:  Worker is recursive and reschedules itself for tomorrow 7AM
    # So not a good idea to do this more than one time on a server.
    next_time = 1.days.from_now.in_time_zone('Beijing').change(hour: 7)
    MailScheduleWorker.perform_at(next_time)
  end

  def self.batch_emails
    base = includes(user: :authentication_tokens).where.not(user_id: nil)
    needs_follow_up = base.where.not(invite_sent_at: nil).where(emails_sent: [])
    needs_invite = base.where(invite_sent_at: nil, emails_sent: [])
    others = base.where.not(invite_sent_at: nil, emails_sent: []).where(invite_sent_at: 60.days.ago..Time.now)

    needs_invite.each do |supp|
      puts "Sending Invite for #{supp.name}..."
      supp.update(invite_sent_at: Time.now)
      supp.message_user!('welcome')
    end

    needs_follow_up.each do |supp|
      next if Time.now < (supp.invite_sent_at + 5.days)
      next if supp.emails_sent.join.include?('5_')

      puts "Sending followup for #{supp.name}..."
      if supp.user.authentication_tokens.present?
        supp.emails_sent << "5_login (#{Time.now.strftime('%Y-%m-%d')})"
        supp.save
        supp.message_user!('5_login')
      else
        supp.emails_sent << "5_no_login (#{Time.now.strftime('%Y-%m-%d')})"
        supp.save
        supp.message_user!('5_no_login')
      end
    end

    others.each do |supp|
      next if supp.emails_sent.count > 5

      last_time_string = supp.emails_sent.last.split('(').last[0..-2]
      last_time = Time.parse(last_time_string)

      if supp.emails_sent.count == 1
        next if Time.now < (supp.invite_sent_at + 10.days)

        supp.message_user!('10_to_50')
      else
        next if Time.now < (last_time + 10.days)

        supp.message_user!('10_to_50')
      end
    end
  end

  def self.attach_users
    includes(:user).where(user_id: nil).where.not(contact_email: [nil, ''])
                   .each { |x| x.save }
  end

  def self.supplier_list(col_no)
    # WIP:  Method to quickly export columns to copy/paste to google sheets
    codes = File.readlines("#{Rails.root}/lib/supplier_list.csv").map { |x| x.strip }
    @@supplier_list = codes.map { |x| x.nil? ? nil : SupplierProfile.find_by_code(x) }
    case col_no
    when 0 || 'Series No.'
    end
  end

  def self.source_list(qtype = :cp, grid = nil)
    if qtype == :cp
      SupplierProfile.includes(:product_families).where.not(code: nil, name: nil)
                     .where(closed: false, destroyed_at: nil)
                     .where(product_families: { destroyed_at: nil }).map do |x|
        pf = x.product_families.reject(&:destroyed_at).first || ProductFamily.new(id: 0)
        { code: x.code, id: x.id, name: x.name, product_id: pf.id }
      end
    elsif qtype == :qa
      SupplierProfile.where.not(code: nil, name: nil)
                     .where(closed: false, destroyed_at: nil).map do |x|
        grid_available = x.grids_available.blank? || x.grids_available == ['']
        grid_available ||= x.grids_available.include?(grid) unless grid.nil?

        { code: x.code, id: x.id, name: x.name, product_id: 0,
          grid_available: grid_available }
      end
    end
  end

  def create_assessors!
    if assessor_provider
      users.each { |u| attach_assessor(u) if u.assessor_profile.blank? }
    end
  end

  def email_list
    res = []
    res << 1 if invite_sent_at
    emails_sent.each_with_index do |_x, i|
      res << i + 2
    end
    res
  end

  def default_user_params(email = nil)
    email = contact_email if email.nil?
    h = { email: email.downcase, username: email.downcase, password: 'Decathlon', access_levels: ['my-reports'],
          grids_available: grids_available }
    h[:supplier_id] = id if email != contact_email
    h[:locale] = 'zh-CN' if country == 'CN'
    h
  end

  def default_user_create(email)
    u = User.find_by_username(email.downcase)
    u = User.create(default_user_params(email)) if u.blank?
    attach_assessor(u) if assessor_provider
    u.send_mail!('welcome')
    u
  end

  def attach_user
    attach_primary_user if contact_email_changed?

    attach_secondary_users if secondary_users_changed?
  end

  def attach_assessor(u)
    if u.assessor_profile.blank?
      a_params = { handle: u.username, name: "NOT SET (#{name})", user_id: u.id,
                   city: city, country: country }
      AssessorProfile.create(a_params)
    end
  end

  def attach_primary_user
    return true unless email_valid?

    if user_id.blank?
      u = default_user_create(contact_email)
      self.user_id = u.id
      self.invite_sent_at = Time.now
      message_user!('welcome')
    elsif contact_email.blank?
      u = user
      self.user_id = nil
      u.update(locked_at: Time.now, destroyed_at: Time.now)
    else
      u = user
      u.update(default_user_params)
    end
    true
  end

  def attach_secondary_users
    self.secondary_users = [] if secondary_users.nil?
    secondary_users.reject!(&:nil?)
    secondary_users.map!(&:downcase)

    u_set = users.to_a

    u_set.each do |u|
      u.update(locked_at: Time.now, destroyed_at: Time.now) unless secondary_users.include?(u.username)
      u_set.delete(u) unless secondary_users.include?(u.username)
    end

    secondary_users.each do |email|
      default_user_create(email) if u_set.select { |x| x.username == email.downcase }.blank?
    end
    true
  end

  def message_user!(mail_type)
    user.send_mail!(mail_type) if user.present?
  end

  def email_valid?
    self.contact_email = nil if contact_email == ''
    self.contact_email = contact_email.downcase.gsub(/(\s)/, '') if contact_email.present?
    contact_email.present? && contact_email.match?(/\w+.*@\w+.*\.\w+/)
  end

  def self.industrial_processes
    SupplierProfile.group(:industrial_process).count.reject { |x| x.blank? }.keys
  end

  def process_name
    return 'Not Selected' if industrial_process.in?(['', nil])

    industrial_process
  end

  def set_status
    if status.blank? && code[0].to_i > 0
      status = 'Active Supplier'
    elsif status.blank?
      status = 'Opening Supplier'
    end
  end

  def goes_activities
    GoesActivity.where(ref_model: 'quality_assessment', ref_id: quality_assessment_ids)
  end

  def location
    [city, country].select { |x| !x.nil? }.join(', ')
  end

  def assessors
    assessments = quality_assessments
    qa_ids = quality_assessment_ids
    assessors = assessments.map { |x| x.prepped_by + x.assessed_by }.flatten.uniq
    set_a = User.where(id: assessors)
    set_b = User.includes(:assessor_assignments)
                .where(assessor_assignments: { ref_model: 'quality_assessment', ref_id: qa_ids })
    set_a + set_b
  end

  def frequency_respect_rate
    freqs = product_families.where(destroyed_at: nil).map { |x| x.freq_respect_rate }
    return (freqs.sum / freqs.count).to_i if freqs.count > 0

    0
  end

  def cap_close_rate
    return 0
    cap_rates = product_families.where(destroyed_at: nil).map { |x| x.cap_close_rate }.uniq.reject { |x| x.blank? }
    return 0 if cap_rates.blank?

    (cap_rates.sum / cap_rates.count).to_i
  end

  def nonconformity_rate
    pls = PlanItem.includes(control_plan: :product_family)
                  .where(product_families: { supplier_profile_id: id, destroyed_at: nil })
                  .where(control_plans: { completed: true })
    puts "TOTAL: #{pls.count}"
    puts "TOTAL NOK: #{pls.where(result: 'NOK').count}"
    puts "TOTAL OK: #{pls.where(result: 'OK').count}"
    tot = pls.where(result: %w[OK NOK]).count
    cnt = pls.where(result: 'NOK').count.to_f * 100
    return 0 if pls.count == 0
    return 0 if tot == 0

    r = (cnt / tot).to_i
  end

  def generate_code
    self.code = if code.blank?
                  p_code
                else
                  code.strip
                end
  end

  def to_h(grid: nil)
    h = serial_hash
    query = quality_assessments.active.formal
    query = query.where(grid: grid) unless grid.nil?
    last_one = {}

    if query.present?
      last_one = last_qa_result(grid: grid)
      next_one = next_qa(grid: grid) # could be nil
      h[:nextQAStatus] = next_one.basic_status if next_one.present?
    end

    h[:users] = users.map { |x| { id: "s#{x.id}", name: x.username } }
    h[:target] = 'N/A' if h[:target].nil?
    h[:contact_email] = 'Not Set' if h[:contact_email].blank?
    h[:contact_name]  = 'Not Set' if h[:contact_name].blank?
    h[:targetColor] = (QualityAssessment.new.level_color[h[:target]] || 'black')
    h[:lastAssessed] = (last_one[:assess_date] || 'N/A')
    h[:lastResult] = (last_one[:level] || 'N/A')
    h[:resultColor] = (last_one[:level_color] || 'black')
    h[:borderColor] = 'blue'
    h.merge!(decathlon_status)
    h
  end

  def decathlon_status
    if decathlon_users_complete?
      {
        edit_subtext: 'Assess Go PL/OPM/PTM information current',
        edit_subtext_color: 'green'
      }
    else
      {
        edit_subtext: 'Assess Go PL/OPM/PTM information missing, please help edit this supplier',
        edit_subtext_color: 'red'
      }
    end
  end

  def p_code
    rcode = "P#{rand(1000..9999)}"
    rcode = p_code if SupplierProfile.find_by(code: rcode)
    rcode
  end

  def last_qa_result(grid: nil)
    qa = last_qa(grid)
    qa.present? ? qa.score_calc : {}
  end

  def next_qa(grid: nil)
    query = quality_assessments.incomplete
    query = query.where(grid: grid) unless grid.nil?
    query.order(assess_date: :asc).first
  end

  def last_qa(grid = nil)
    query = quality_assessments.active.complete.formal
    query = query.where(grid: grid) unless grid.nil?
    query.order(assess_date: :desc).first
  end

  def setup_new_qa(params)
    qa_params = {
      supplier_profile_id: id,
      supplier_code: code,
      assess_type: params[:assess_type],
      created_by: params[:created_by],
      assess_date: Time.parse(params[:assess_date]),
      status: 'Prep Required'
    }
    qa = QualityAssessment.create(qa_params)
    qa.setup_qa_wave(params)
  end

  def concerned_assessment_grids
    grids_available.blank? ? 'ALL' : grids_available.join(', ')
  end

  def pre_confirmation_card
    last = quality_assessments.last
    {
      code: code,
      name: name,
      location: address,
      lastAssessed: last.assess_date.to_s.split(' ').first,
      lastResult: last.result.nil? ? 'NA' : last.result,
      contactName: contact_name,
      contactEmail: contact_email
    }
  end

  def go_for_assessment_card
    last = quality_assessments.last
    {
      code: code,
      name: name,
      location: address,
      lastAssessed: last.assess_date.to_s.split(' ').first,
      lastResult: last.result.nil? ? 'NA' : last.result,
      resultColor: last.level_color[last.result] || 'black',
      contactName: contact_name,
      contactEmail: contact_email,
      report: last.result.nil? ? false : true,
      buttonText: 'Assess Report Preview ➜'
    }
  end

  def basic_info
    cols = %i[code name address contact_name contact_email status
              updated_at industrial_process finished_goods_supplier component_supplier
              country city partner sites secondary_users concerned_assessment_grids
              rank kas]

    data = cols.map { |x| [x, safe_send(x)] }.to_h
    # User.includes(:assessor_profile).where(assessor_profiles: {id: all_ids})
    %i[pl_set opm_set ptm_set].each do |col|
      set = public_send(col)
      data[col] = set.blank? ? 'NOT SET' : AssessorProfile.where(id: set).map(&:name).join(', ')
    end
    data
  end
end
