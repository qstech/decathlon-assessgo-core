class Letter < ApplicationRecord
  belongs_to :mail_bag
  # Letter is 1 to receiver.
  has_rich_text :body

  def deliver!
    u = User.find_by(id: to)
    UserMailer.send_out_letter(u, self).deliver_later!
    update(delivered_at: Time.now)
  end

  def read?
    opened_at.present?
  end

  def recipients
    case to
    when 'decathlon'
      User.includes(:assessor_profile).where.not(assessor_profiles: { id: nil })
    when 'suppliers'
      User.where('username ILIKE ?', '%@%')
    when 'assessors'
      User.includes(:assessor_profile).where.not(assessor_profiles: { assessor_for: [] })
    when 'grid_leaders'
      User.includes(:assessor_profile).where.not(assessor_profiles: { referent_for: [] })
    when 'all'
      User.where(destroyed_at: nil)
    else
      User.find(to)
    end
  end
end
