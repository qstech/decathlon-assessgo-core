class Schedule < ApplicationRecord
  belongs_to :sender, class_name: 'User', foreign_key: 'sender_id'
  belongs_to :recipient, class_name: 'SupplierProfile', foreign_key: 'recipient_id'
  belongs_to :control_plan, class_name: 'ControlPlan', foreign_key: 'ref_id', optional: true
  has_paper_trail ignore: %i[created_at updated_at]
end
