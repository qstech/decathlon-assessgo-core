class Comment < ApplicationRecord
  belongs_to :user
  belongs_to :reply_user, class_name: 'User', foreign_key: 'reply_to', optional: true
  has_paper_trail ignore: %i[created_at updated_at]
  include MessageScheduler

  # belongs_to :chapter_question, -> {select('chapter_questions.id').where('comments.ref_model = "chapter_question"')}, class_name: 'ChapterQuestion', foreign_key: 'ref_id', optional: true
  # belongs_to :goes_activity, -> {select('goes_activities.id').where('comments.ref_model = "goes_activity"')}, class_name: 'GoesActivity', foreign_key: 'ref_id'
  scope :with_users, -> { includes(:user, :reply_user) }
  scope :join_chapters, -> {
                          joins('INNER JOIN "chapter_assessments" ON "chapter_assessments"."id" = "comments"."ref_id"').where(ref_model: 'chapter_assessment')
                        }
  scope :join_activities, -> {
                            joins('INNER JOIN "goes_activities" ON "goes_activities"."id" = "comments"."ref_id"').where(ref_model: 'goes_activity')
                          }
  default_scope -> { where(destroyed_at: nil) }

  def users_concerned
    if ref_model == 'goes_activity'
      users = ref.ref.users_concerned || []
    elsif ref_model == 'chapter_assessment'
      users = ref.quality_assessment.assessors || []
    end

    users = users.to_a
    users << User.find_by(id: reply_to) unless reply_to.nil?
    users.flatten.uniq
  end

  def ref
    ref_model.classify.constantize.find(ref_id)
  end

  def profile
    user.profile
  end

  def page_type
    case ref_model
    when 'goes_activity' then 'goes'
    when 'chapter_assessment'
      return 'dpr-chapter' if ref.quality_assessment.dpr?

      'single-chapter'
    end
  end

  def link
    case ref_model
    when 'goes_activity' then "/members/activity/show?id=#{ref_id}"
    when 'chapter_assessment' then "/members/assessment/single-chapter-review?id=#{ref_id}"
    end
  end

  def self.weekly_count
    # today = Date.today.wday # 0 is Sunday, 1 is Monday
    now = Time.now
    start = Time.now - 7.days
    start_date = Date.new(start.strftime('%Y').to_i, start.strftime('%m').to_i, start.strftime('%d').to_i).to_time
    Comment.where(created_at: start_date..now).count
  end
end
