class GoesActivity < ApplicationRecord
  belongs_to :user
  has_many :comments, -> { where(ref_model: 'goes_activity') }, foreign_key: 'ref_id'

  has_paper_trail ignore: %i[created_at updated_at]
  validates :ref_status, uniqueness: { scope: %i[ref_model ref_id] }
  default_scope -> { where(destroyed_at: nil) }

  scope :with_qa, -> { joins('LEFT JOIN quality_assessments qa ON qa.id = goes_activities.ref_id') }
  scope :with_qa_supplier, -> {
                             with_qa.joins('LEFT JOIN supplier_profiles qa_supp ON qa.supplier_profile_id = qa_supp.id')
                           }
  scope :with_cp, -> { joins('LEFT JOIN control_plans cp ON cp.id = goes_activities.ref_id') }
  scope :with_pf, -> { with_cp.joins('LEFT JOIN product_families pf ON cp.product_family_id = pf.id') }
  scope :with_cp_supplier, -> {
                             with_pf.joins('LEFT JOIN supplier_profiles cp_supp ON pf.supplier_profile_id = cp_supp.id')
                           }
  scope :only_qa_partners, -> { where(qa_supp: { partner: true }) }
  scope :only_cp_partners, -> { where(cp_supp: { partner: true }) }
  scope :no_hrp, -> { where(ref_model: 'quality_assessment').where("qa.grid != 'hrp_assessment'") }
  scope :only_cp, -> { where(ref_model: 'control_plan') }
  scope :for_suppliers, -> {
    with_qa_supplier.with_cp_supplier.only_qa_partners.no_hrp
                    .or(GoesActivity.with_qa_supplier.with_cp_supplier.only_cp_partners.only_cp)
  }

  def to_h
    h = serial_hash
    h['linked'] = { model: ref_model, data: ref.serial_hash }
    h
  end

  def grid_assess_type(grid)
    "#{grid} #{ref_type}"
  end

  def ref
    ref_model.classify.constantize.find(ref_id)
  end

  def supplier_profile
    ref.supplier_profile
  end

  def assessor_profile
    ref.assessor
  end

  def self.activities_count(user)
    user.goes_activities.select { |x| !x.ref.completed }.count
  end
end
