class ChangeLog < ApplicationRecord
  before_create :clean_change_list

  def clean_change_list
    self.change_list = change_list.reject { |x| x.blank? }
  end
end
