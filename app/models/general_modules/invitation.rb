class Invitation < ApplicationRecord
  belongs_to :sender, class_name: 'User', foreign_key: 'sender_id'
  belongs_to :recipient, class_name: 'User', foreign_key: 'recipient_id'

  has_paper_trail ignore: %i[created_at updated_at]
  after_update :assign_user
  default_scope -> { where(destroyed_at: nil) }
  # after_update :trigger_mail!
  include MessageScheduler

  # def audience_for(audience)
  #   set = []
  #   audience.each do |x|
  #     set << fetch_audience(x.to_sym)
  #   end
  #   set.flatten
  # end

  # def fetch_audience(audience)
  #   single = [:sender, :recipient, :supplier]
  #   multi = []
  #   # multi  = [:supplier_pls, :supplier_opms, :supplier_ptms]

  #   return [public_send(audience)].reject(&:nil?) if audience.in?(single)
  #   return public_send(audience) if audience.in?(multi)
  #   return User.where(username: ["kkang20","wxu50"]).to_a if audience == :admins
  #   raise StandardError.new("AUDIENCE NOT VALID: #{audience}")
  # end

  # def trigger_mail!
  #   if self.accepted
  #     MailBag.trigger!("invitation_accepted", self)
  #   elsif self.rejected
  #     MailBag.trigger!("invitation_rejected", self)
  #   end
  # end

  def render_activity(user)
    if accepted
      goes_params = {}
      goes_params[:user_id]   = user.id
      goes_params[:ref_model] = ref_model
      goes_params[:ref_id]    = ref_id
      goes_params[:ref_type]  = ref.assess_type
      goes_params[:ref_status] = 'Assessor Confirmed!'
      goes_params[:content] = "#{user.profile.name.split(' ').first.capitalize} has confirmed to assess this QA!"
      GoesActivity.create(goes_params)
    end
  end

  def ref
    @ref ||= ref_model.classify.constantize.find(ref_id)
  end

  def invited_date
    created_at.strftime('%Y-%m-%d')
  end

  def assign_user
    if accepted
      grid = ref.short_name
      i_count = ref.invitations.count
      primary = (i_count == 1) || !recipient.referent_for?(grid)

      ass = AssessorAssignment.new(ref_model: ref_model, ref_id: ref_id,
                                   user_id: recipient_id, confirmed: true)

      ref.assessor_assignments.each do |assign|
        assign.update(primary: false) if primary
      end

      ass.primary = primary
      ass.save

      # Deleting all other invitations
      ref.invitations.where(accepted: false).each do |inv|
        rec = inv.recipient
        inv.destroy unless rec.referent_for?(grid) || rec.candidate_for?(grid)
      end
    end
  end

  def users_concerned
    User.where(id: [sender_id, recipient_id])
  end
end
