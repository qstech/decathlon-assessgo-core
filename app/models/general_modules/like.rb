class Like < ApplicationRecord
  belongs_to :user
  has_paper_trail ignore: %i[created_at updated_at]
  default_scope -> { where(destroyed_at: nil) }
end
