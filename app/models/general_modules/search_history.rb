class SearchHistory < ApplicationRecord
  belongs_to :user
  has_paper_trail ignore: %i[created_at updated_at]
  scope :recent, -> { order(created_at: :desc).limit(6) }
  scope :queries, -> { select(:query).map(&:query) }
end
