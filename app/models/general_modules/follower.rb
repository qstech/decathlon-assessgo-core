class Follower < ApplicationRecord
  belongs_to :follower, class_name: 'User', foreign_key: 'user_id'
  has_paper_trail ignore: %i[created_at updated_at]
  validates :ref_model, presence: true
  validates :ref_id, presence: true
  # validates :follower, uniqueness: { scope: :user,
  # message: "You can't follow the same person twice."}
  # validate :user_and_follower_unique

  def ref
    ref_model.classify.constantize.find(ref_id)
  end

  def user
    ref.user
  end

  def to_h
    h = serial_hash
    h[:profile] = ref.serial_hash

    if h[:profile][:name].nil?
      h[:profile][:name] = h[:profile][:handle]
    elsif h[:profile][:name].length > 10
      h[:profile][:name] = h[:profile][:name][0..5] + '...'
    end

    h[:profile][:type] = ref_model
    avatar = user.wx_avatar if user.present?
    avatar = PageComponent::AVATAR[:user] if avatar.nil? && ref_model == 'assessor_profile'
    avatar = PageComponent::AVATAR[:supplier] if avatar.nil? && ref_model == 'supplier_profile'
    h[:avatar] = avatar
    id_type = (ref_model == 'supplier_profile' ? 'supplier' : '')
    h[:url] = "/members/show/show?#{id_type}id=#{ref_id}"
    h
  end

  def user_and_follower_unique
    errors.add(:user, "You can't follow yourself.") if ref == follower
  end
end
