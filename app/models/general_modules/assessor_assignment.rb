class AssessorAssignment < ApplicationRecord
  belongs_to :user
  belongs_to :quality_assessment, class_name: 'QualityAssessment', foreign_key: 'ref_id', optional: true
  # belongs_to :chapter_question, -> {select('chapter_questions.id').where('comments.ref_model = "chapter_question"')}, class_name: 'ChapterQuestion', foreign_key: 'ref_id'
  has_paper_trail ignore: %i[created_at updated_at]

  after_save :update_parent

  def parent_form
    ref_model.classify.constantize.find(ref_id)
  end

  def update_parent
    parent_form.update(assessor_confirmed: true) if confirmed
  end
end
