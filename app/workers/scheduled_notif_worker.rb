class ScheduledNotifWorker
  include Sidekiq::Worker

  def perform(obj_hash, next_assess_type, log_id)
    return true if ENV['staging'] == 'true'

    # update object w/ most recent database updates
    unless obj_hash.instance_of?(String)
      obj_class = obj_hash['model'].constantize
      obj = obj_class.find_by(id: obj_hash['id'])
      p obj_hash
      # return true if obj.blank?
    end

    if obj_hash['model'] == 'QualityAssessment'
      # Check which "message" template to send
      params = obj.next_assessment_reminder(next_assess_type)

      log = NextAssessmentLog.find_by(id: log_id.to_i)
      recipients = log.recipients

      recipients.each do |user_id|
        user = User.find(user_id)

        user.next_assessment_notify!(params, next_assess_type, obj, log)
      end

      log.update!(sent: true)
    end
  end
end
