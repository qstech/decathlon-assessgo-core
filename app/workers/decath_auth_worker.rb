require 'rest-client'
require 'jwt'
require 'mechanize'

class DecathAuthWorker
  include Sidekiq::Worker
  sidekiq_options retry: false, queue: :login

  def perform(enc_username, enc_password, *openid)
    username = Encryptor.decrypt(enc_username)
    password = Encryptor.decrypt(enc_password)
    openid = clean_openid(openid)
    DecathAuthentication.oauth_login(username, password, openid)
  end

  def clean_openid(openid)
    if openid.is_a? Array
      openid.flatten.first
    elsif openid.is_a? String
      res = JSON.parse(openid)
      return res if res.is_a? Hash
      return clean_openid(res) if res.is_a? Array

      res
    end
  end
end
