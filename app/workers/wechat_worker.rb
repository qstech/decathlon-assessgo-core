class WechatWorker
  include Sidekiq::Worker

  def perform(obj_hash, params = {})
    return true if ENV['staging'] == 'true'

    # update object w/ most recent database updates
    unless obj_hash.instance_of?(String)
      obj_class = obj_hash['model'].constantize
      obj = obj_class.find_by(id: obj_hash['id'])
      p obj_hash
      # return true if obj.blank?
    end
    # check for condition
    # if wx_params["condition"].present? && obj_hash.class != String
    #   p "TESTING WORKER W/ CONDITION: #{wx_params["condition"]}"
    #   condition = obj.send(wx_params["condition"])
    # else
    #   condition = true
    # end

    wx_params = params['wx_params']
    email_params = params['email_params'] || {}
    if wx_params.present?
      puts 'Activating Notifier'
      WxNotifier.notify!(wx_params)
    end

    if email_params['email'].present?
      puts 'Activating Email Notification!'
      UserMailer.with(object: obj, email_params: email_params).notification.deliver_later
    end
  end
end
