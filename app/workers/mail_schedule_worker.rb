class MailScheduleWorker
  include Sidekiq::Worker

  def perform(*_args)
    SupplierProfile.batch_emails

    # Reschedule self Tomorrow 7AM CN time
    next_time = 1.days.from_now.in_time_zone('Beijing').change(hour: 7)

    MailScheduleWorker.perform_at(next_time)
  end
end
