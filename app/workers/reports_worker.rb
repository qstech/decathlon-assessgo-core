require 'rest-client'
class ReportsWorker
  include Sidekiq::Worker
  sidekiq_options retry: false, queue: :reports
  HOST = if Rails.env == 'development'
           'http://localhost:3000'
         elsif ENV['STAGING']
           'https://assessgopp.decathlon.cn'
         else
           'https://assessgo.decathlon.cn'
         end

  def perform(id, model_name, report_type, full = true)
    model = model_name.constantize
    @obj = model.find(id.to_i)
    puts "GENERATING #{report_type} REPORT FOR #{model_name}: #{id}"

    if report_type == 'pdf'
      p full
      @obj.update_report!(full)
      RestClient.get("#{HOST}/api/v1/reports/#{@obj.slug}/pdf_finished?new_url=#{rails_blob_path(@obj.report)}")
    elsif report_type == 'xls'
      @obj.update_excel_report!
      RestClient.get("#{HOST}/api/v1/reports/#{@obj.slug}/pdf_finished?new_url=#{rails_blob_path(@obj.excel_report)}")
    elsif report_type == 'cap'
      @obj.initialize_cap_review!
      RestClient.get("#{HOST}/api/v1/reports/#{@obj.slug}/pdf_finished?new_url=#{rails_blob_path(@obj.excel_cap_review_empty)}")
    end
  end

  def rails_blob_path(item)
    Rails.application.routes.url_helpers.rails_blob_path(item, host: HOST, disposition: 'attachment')
  end
end
