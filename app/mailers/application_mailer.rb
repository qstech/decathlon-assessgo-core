class ApplicationMailer < ActionMailer::Base
  default from: 'assessgo_decathlon@163.com'
  layout 'mailer'
end
