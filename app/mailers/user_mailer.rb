class UserMailer < ApplicationMailer
  default from: Rails.application.credentials.dig(:vipmail, :email),
          cc: [AssessGoApi::PO, AssessGoApi::IT]
  layout 'mailer_temp', only: [:notification]

  def reset_password
    @user = params[:user]
    @new_pass = 'Decathlon' # SecureRandom.urlsafe_base64(4).upcase
    @user.password = @new_pass
    @user.save

    mail(to: @user.email, subject: 'Your New Password')
  end

  def welcome
    @user = params[:user]
    @supplier = @user.supplier || @user.supplier_profile
    mail_params = { to: @user.email, subject: 'Your New Password' }
    if @supplier.present?
      mail_params[:cc] = @supplier.cc_list
      mail(mail_params)
    end
  end

  def oa_invite
    @user = params[:user]
    @supplier = @user.supplier || @user.supplier_profile
    mail_params = { to: @user.email, subject: 'Invitation to AssessGo Official Account Notifications' }
    mail_params[:cc] = @supplier.cc_list if @supplier.present?

    mail(mail_params)
  end

  def thanks_followup
    @user = params[:user]
    mail_params = { to: @user.email, subject: 'Thanks for Using Assess Go!' }
    mail_params[:cc] = [AssessGoApi::PO, AssessGoApi::IT]
    mail(mail_params) if @user.assessor_profile.present?
  end

  def followup_5_no_login
    @user = params[:user]
    @profile = @user.supplier || @user.supplier_profile
    mail_params = { to: @user.email, subject: 'Need help using Assess Go?' }

    mail_params[:cc] = @profile.cc_list if @profile.present?

    mail(mail_params) if @profile.present?
  end

  def followup_5_login
    @user = params[:user]
    @profile = @user.supplier || @user.supplier_profile
    mail_params = { to: @user.email, subject: 'Thanks for using Assess Go!' }

    mail_params[:cc] = @profile.cc_list if @profile.present?

    mail(mail_params) if @profile.present?
  end

  def followup_10_to_50
    @user = params[:user]
    @profile = @user.supplier || @user.supplier_profile

    @qa_count      = QualityAssessment.where(created_by: @user).count
    @search_count  = SearchHistory.where(user_id: @user.id).count
    @follow_count  = Follower.where(user_id: @user.id).count
    @comment_count = Comment.where(user_id: @user.id).count

    @set = [@qa_count, @search_count, @follow_count, @comment_count]
    mail_params = { to: @user.email, subject: 'Thanks for using Assess Go!' }

    @active = if @qa_count == 0 && !@profile.emails_sent.join.include?('no_qa')
                'no_qa'
              elsif @qa_count == 0 && !@profile.emails_sent.join.include?('no_search')
                'no_search'
              elsif @qa_count == 0 && !@profile.emails_sent.join.include?('no_follow')
                'no_follow'
              elsif @qa_count == 0 && !@profile.emails_sent.join.include?('no_comment')
                'no_comment'
              else
                'other'
              end
    days = @profile.emails_sent.count * 10
    @profile.emails_sent << "#{days}_#{@active} (#{Time.now.strftime('%Y-%m-%d')})"
    @profile.save!

    mail_params[:cc] = @profile.cc_list if @profile.present?

    mail(mail_params) if @profile.present?
  end

  def email_changed
    @user = params[:user]
    @supplier = @user.supplier || @user.supplier_profile

    mail(to: @user.email, subject: 'Your New Password', cc: @supplier.cc_list) if @supplier.present?
  end

  def notification
    puts 'inside UserMailer..'
    @object = params[:object]
    @email_params = params[:email_params]
    p @email_params
    if @email_params['redirect'].present?
      mp_url = @email_params['redirect'].split('?')
      qr_params = { page: mp_url[0] }
      qr_params.merge!({ scene: mp_url[1] }) unless mp_url[1].nil?
      @qr = WxAuthentication.mp_qrcode(qr_params)
      image_data = Base64.decode64(@qr['data:image/png;base64,'.length..-1])
      attachments.inline['qr.png'] = image_data
    end

    @user = User.find(@email_params[:user_id] || @email_params['user_id'])
    @profile = @user.profile

    mail(to: @user.email, subject: "Decathlon AssessGo - #{@email_params[:header] || @email_params['header']}",
         cc: [AssessGoApi::PO, AssessGoApi::IT])
  end

  def sdb_email_update
    @supplier = params[:supplier]
    mail(to: @supplier.ib_emails, subject: "Decathlon AssessGo - Supplier Info Needed (#{@supplier.code})",
         cc: @supplier.cc_list)
  end

  def auto_gemba_creation
    @user = params[:user]
    mail(to: @user.email, subject: 'Assess Go has created a Gemba assessment for you')
  end

  def send_out_letter(user, letter)
    @user = user
    @letter = letter
    mail(to: user.email, subject: letter.subject)
  end

  def test(user)
    mail(to: user.email, subject: 'TEST')
  end
end
