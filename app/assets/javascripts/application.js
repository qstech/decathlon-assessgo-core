// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require rails-ujs
//= require jquery3
//= require frappe
//= require autocomplete
//= require activestorage
//= require cable
//= require turbolinks
//= require lightgallery
//= require sweetalert2
//= require vanillatoast
//= require highcharts/highcharts
//= require highcharts/highcharts-more
//= require highcharts/map
//= require xlsx
//= require choices.min
//= require table_filter
//= require_tree .



function getQrcode(id, name) {
  var URL = window.location.pathname;
  AjaxRequest.get(
    {
      url: `${URL}?id=${id}`,
      onSuccess: function(req){
        img = req.responseText
        swal({
          title: name,
          text: "Scan to check",
          imageUrl: img
        });
      }
    }
  )
}

function postCapPointData(model, value, column, id){
  console.log(model, value, column, id);
  var path = `/api/v1/${model}/${id}`
  var param = {}
  if (column == 'root_cause_nd') {
    param = {root_cause_nd: value}
  } else if (column == 'root_cause_nc') {
    param = {root_cause_nc: value}
  } else if ((column == 'resolved') && (value == true)) {
    param = {resolved_at: new Date()}
  } else if ((column == 'resolved') && (value == false)) {
    param = {resolved_at: null}
  } else if (model == 'caps') {
    param = {completed: true}
  }

  var body = model == 'caps' ? {cap: param} : { cap_point: param }
  console.log(body)

  fetch(path, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'X-CSRF-Token': Rails.csrfToken()
    },
    body: JSON.stringify(body)
  }).then(response => {
      if (!response.ok) {
        throw new Error(response.statusText)
      }
      return response.json()
    })
}

function testLogin(){
  Swal.fire({
    title: "Test Supplier Account",
    html: '<input id="email" class="swal2-input" placeholder="Supplier Email...">' +
    '<input id="password" class="swal2-input" placeholder="Password..">',
    inputAttributes: {
      autocapitalize: 'off'
    },
    showCancelButton: true,
    confirmButtonText: 'Confirm',
    showLoaderOnConfirm: true,
    preConfirm: () => {
      var email = document.getElementById('email').value;
      var password = document.getElementById('password').value;
      var path = `/users/test_login?email=${email}&password=${password}`
      console.log(path)

      console.log(email, password)
      return fetch(path)
        .then(response => {
          if (!response.ok) {
            throw new Error(response.statusText)
          }
          console.log('response', response)
          return response.json()
        })
        .catch(error => {
          Swal.showValidationMessage(`Request failed: ${error}`)
        })
    },
    allowOutsideClick: () => !Swal.isLoading()
  }).then((result) => {
    if (result.value) {
      Swal.fire({
        html: result.value.msg
      }).then(res=>{
        location.reload(true);
      })
    }
  })
}

function verifyUsername(username, action){
  if (action == 'lock'){
    var title = "Lock User";
    var path  = `/users/lock?username=${username}`;
  } else if (action == 'reset'){
    var title = "Reset Password for User";
    var path  = `/users/reset?username=${username}`;
  } else if (action == 'unlock'){
    var title = "Unlock User";
    var path = `/users/unlock?username=${username}`;
  } else if (action == 'invite'){
    var title = "Invite User";
    var path = `/users/invite?username=${username}`;
  }

  Swal.fire({
    title: title,
    html: `Please type <span style="color:red">${username}</span> to confirm.`,
    input: 'text',
    inputAttributes: {
      autocapitalize: 'off'
    },
    showCancelButton: true,
    confirmButtonText: 'Confirm',
    showLoaderOnConfirm: true,
    preConfirm: (login) => {
      console.log('login:', login);
      if (username == login){
        return fetch(path)
          .then(response => {
            if (!response.ok) {
              throw new Error(response.statusText)
            }
            return response.json()
          })
          .catch(error => {
            Swal.showValidationMessage(`Request failed: ${error}`)
          })
      } else {
        Swal.showValidationMessage(`Request failed: Must type the '${username}' exactly to confirm.`)
      }
    },
    allowOutsideClick: () => !Swal.isLoading()
  }).then((result) => {
    if (result.value) {
      Swal.fire({
        html: result.value.msg
      }).then(res=>{
        location.reload(true);
      })
    }
  })
}

function notificationSub(channel, id) {
  console.log('sub notification');
  if (!App.notification || App.notification.consumer.connection.disconnected) {
    App.notification = App.cable.subscriptions.create({ channel: channel, id: id}, {
      received: function(data) {
        // reloadWithTurbolinks();
      }
    })
  }
}

function PdfNotificationSub(id) {
  console.log('sub Pdf');
  if (!App.pdf_notification || App.pdf_notification.consumer.connection.disconnected) {
    App.pdf_notification = App.cable.subscriptions.create({ channel: 'PdfChannel', id: id}, {
      received: function(data) {
        // reloadWithTurbolinks();
        App.pdf_notification.unsubscribe();

      }
    })
  }
}

function setScroll(event){
  console.log('before render: ', scrollPosition)
  console.log(event.data)
  if (!scrollPosition){
    scrollPosition = [window.scrollX, window.scrollY];
    console.log('after render: ', scrollPosition)
  }

}

function loadScroll(event){
  console.log('before load: ', scrollPosition)
  if (scrollPosition) {
    window.scrollTo.apply(window, scrollPosition);
    console.log("SCROLLED:", scrollPosition)
    scrollPosition = null
  }
}

function checkScroll(event){
  console.log(event.type, [window.scrollX, window.scrollY])
}

function loading(){
  Swal.fire({
    title: 'Uploading...',
    html: '<strong></strong>',
    onBeforeOpen: () => {
      Swal.showLoading()
    },
  })
}

function deleting(){
  Swal.fire({
    title: 'Deleting...',
    html: '<strong></strong>',
    onBeforeOpen: () => {
      Swal.showLoading()
    },
  })
}

function autoClickForm(form){
  console.log('clicked', form)
  for (var i = 0; i < form.length; i++) {
    if (form[i].name == "commit"){
      form[i].click()
    }
  }
}

function deleteImage(e, model, name, id, i){
    e.parentElement.remove();
    document.querySelectorAll(`.id-${id}`).forEach(function(a){
      if (parseInt(a.dataset.index) > i){
        a.dataset.index = i-1
      }
    })

    var data = {
      type: "POST",
      url: "/api/v1/"+ model+ "/" + id + "/" + i,
      data: "option="+name,
      dataType: 'json',
      success: (res)=>{
        Swal.close();
      }
    }
    deleting();
    Rails.ajax(data)
  }

function bookMark(model, name, id, index, boolean) {
    var data = {
      type: "POST",
      url: "/api/v1/" + model + "/" + id + "/" + "bookmark" + "/" + index,
      data: "option=" + name + "&bookmark=" + boolean,
      dataType: 'json',
      success: (res)=>{
        document.removeEventListener('turbolinks:visit', setScroll);
        document.removeEventListener('turbolinks:render', loadScroll);
        document.addEventListener('turbolinks:visit', setScroll);
        document.addEventListener('turbolinks:render', loadScroll);
        setTimeout(function () {
          Turbolinks.visit(window.location);
        }, 1200)
      }
    }
    Rails.ajax(data);
}


function reloadWithTurbolinks(){
  console.log("Reloading w/ turbolinks")
  var form_name = document.activeElement.form.name;
  var el = document.activeElement.name;

  document.removeEventListener('turbolinks:visit', setScroll);
  document.removeEventListener('turbolinks:render', loadScroll);
  document.addEventListener('turbolinks:visit', setScroll);
  document.addEventListener('turbolinks:render', loadScroll);

  function reload() {
    console.log("RELOADING")
    Turbolinks.visit(location.toString(), {action: 'replace'})
  }

  reload();
  console.log(form_name, el)
  setTimeout(function(){
    var field = document.forms[form_name].elements[el]
    var val = field.value
    field.focus();
    field.value = '';
    field.value = val;
  }, 100)
}

// function copyPrevChapterResult(chapter_id, last_result) {
//   console.log(chapter_id, last_result)
//   console.log(last_result["points_to_improve"])
// }

function copyPrevChapterResult(chapter_id, last_result){
  console.log(chapter_id, last_result);
  var path = `/api/v1/chapter_questions/${chapter_id}`
  var body = {chapter_question: {result: last_result["result"], strong_points: last_result["strong_points"], points_to_improve: last_result["points_to_improve"]}}

  console.log("body ==>", body)

  fetch(path, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'X-CSRF-Token': Rails.csrfToken()
    },
    body: JSON.stringify(body)
  }).then(response => {
      if (!response.ok) {
        throw new Error(response.statusText)
      }
    // Swal.close();
    document.removeEventListener('turbolinks:visit', setScroll);
    document.removeEventListener('turbolinks:render', loadScroll);
    document.addEventListener('turbolinks:visit', setScroll);
    document.addEventListener('turbolinks:render', loadScroll);
    setTimeout(function(){
      Turbolinks.visit(location.toString(), {action: 'replace'})
    }, 500)
      return response.json()
    })
}

function initForms(){
  document.querySelectorAll('form').forEach(function(form){
    form.addEventListener('ajax:success', function(){
      VanillaToasts.create({
        title: 'Assess Go',
        text: 'Information Saved',
        type: 'success', // success, info, warning, error   / optional parameter
        icon: '/ag_spinner.png', // optional parameter
        timeout: 1800 // hide after 5000ms, // optional paremter
        // callback: function() { ... } // executed when toast is clicked / optional parameter
      });
    });
  })
}

function exportTableToExcel(tableID, filename = ''){
  var table = document.getElementById(tableID);
  generateExcel(table, filename)
}

function downloadExcel(table, filename = ''){
  var downloadLink;
  var dataType = 'application/vnd.ms-excel';
  var tableHTML = table.outerHTML.replace(/ /g, '%20');

  // Specify file name
  filename = filename?filename+'.xls':'excel_data.xls';

  // Create download link element
  downloadLink = document.createElement("a");

  document.body.appendChild(downloadLink);

  if(navigator.msSaveOrOpenBlob){
    var blob = new Blob(['\ufeff', tableHTML], {
        type: dataType
    });
    navigator.msSaveOrOpenBlob( blob, filename);
  }else{
    // Create a link to the file
    downloadLink.href = 'data:' + dataType + ', ' + tableHTML;

    // Setting the file name
    downloadLink.download = filename;

    //triggering the function
    downloadLink.click();
  }
}


function generateExcel(table, filename= ''){

  // Specify file name
  filename = filename?filename+'.xls':'excel_data.xls';

  /* Sheet Name */
  var ws_name = "DataSheet";

  if (typeof console !== 'undefined') console.log(new Date());
  var wb = XLSX.utils.book_new(),

      ws = XLSX.utils.table_to_sheet(table);

  /* Add worksheet to workbook */
  XLSX.utils.book_append_sheet(wb, ws, ws_name);

  /* Write workbook and Download */
  if (typeof console !== 'undefined') console.log(new Date());
  XLSX.writeFile(wb, filename);
  if (typeof console !== 'undefined') console.log(new Date());
}

function exportJSONToTable(obj){
  var table = document.createElement("table");
  var thead = document.createElement("thead");
  var headers = document.createElement("tr");
  obj.headers.forEach(function(hdr){
    var header = document.createElement("th");
    header.innerText = hdr;
    headers.appendChild(header);
  })
  thead.appendChild(headers);
  table.appendChild(thead);

  var tbody = document.createElement("tbody");
  obj.data.forEach(function(row){
    var tr = document.createElement("tr");
    obj.headers.forEach(function(key){
      var td = document.createElement("td");
      td.innerText = row[key];
      tr.appendChild(td)
    })
    tbody.appendChild(tr)
  })

  table.appendChild(tbody)
  return table
}

function exportJSONToExcel(obj, filename = ''){
  var table = exportJSONToTable(obj)
  generateExcel(table, filename)
}

// Stats filters using Choices
function statsFilterChoices() {
  console.log('stats filter loaded')
  var c = document.getElementById('countries_stats_filter');
  var g = document.getElementById('grids_stats_filter')
  var a = document.getElementById('assess_types_stats_filter')
  var p = document.getElementById('plan_types_stats_filter')
  var r = document.getElementById('results_stats_filter')
  var s = document.getElementById('supplier_status_stats_filter')
  var ea = document.getElementById('email_assessors')
  var ed = document.getElementById('email_decathlonians')
  var eg = document.getElementById('email_grid_leaders')
  var es = document.getElementById('email_suppliers')
  var ass = document.getElementById('assessors_stats_filter')
  var proc = document.getElementById('processes_stats_filter')
  var ap = document.getElementById('assess_plan_types_stats_filter')
  var users = document.getElementById('users_stats_filter');

  var elements = [p, c, g, a, r, s, ass, ea, ed, eg, es, proc, ap, users].filter(element => element != null)

  console.log('elements ==>> ', elements)

  elements.forEach(element => {
    console.log('element ==>>', element)
    return new Choices(element, {
    removeItemButton: true,
    classNames: {
      inputCloned: 'no-shadow',
      placeholder: true
    }
  })
  })

}

window.addEventListener("turbolinks:load", makeAllSortable);

window.addEventListener('load', function() {
  addEventListener("direct-upload:initialize", event => {
    const { target, detail } = event
    const { id, file } = detail
    target.insertAdjacentHTML("beforebegin", `
      <div id="direct-upload-${id}" class="direct-upload direct-upload--pending">
        <div id="direct-upload-progress-${id}" class="direct-upload__progress" style="width: 0%"></div>
        <span class="direct-upload__filename">${file.name}</span>
      </div>
    `)
  })

  addEventListener("direct-upload:start", event => {
    const { id } = event.detail
    const element = document.getElementById(`direct-upload-${id}`)
    element.classList.remove("direct-upload--pending")
  })

  addEventListener("direct-upload:progress", event => {
    const { id, progress } = event.detail
    const progressElement = document.getElementById(`direct-upload-progress-${id}`)
    progressElement.style.width = `${progress}%`
  })

  addEventListener("direct-upload:error", event => {
    event.preventDefault()
    const { id, error } = event.detail
    const element = document.getElementById(`direct-upload-${id}`)
    element.classList.add("direct-upload--error")
    element.setAttribute("title", error)
  })

  addEventListener("direct-upload:end", event => {
    const { id } = event.detail
    const element = document.getElementById(`direct-upload-${id}`)
    element.classList.add("direct-upload--complete")
  })

  addEventListener("direct-uploads:start", event=>{
    loading();
  })

  addEventListener("direct-uploads:end", event=>{
    autoClickForm(event)
    Swal.close();
    document.removeEventListener('turbolinks:visit', setScroll);
    document.removeEventListener('turbolinks:render', loadScroll);
    document.addEventListener('turbolinks:visit', setScroll);
    document.addEventListener('turbolinks:render', loadScroll);
    setTimeout(function () {
      Turbolinks.visit(window.location);
    }, 1200)
  })

  // initForms()
})











