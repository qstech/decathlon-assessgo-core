
function sortTable(table, col, reverse) {
  var tb = table.tBodies[0], // use `<tbody>` to ignore `<thead>` and `<tfoot>` rows
      tr = Array.prototype.slice.call(tb.rows, 0), // put rows into array
      i;
  reverse = -((+reverse) || -1);
  tr = tr.sort(function (a, b) { // sort rows
      return reverse // `-1 *` if want opposite order
          * (smartSort(a,b,col));
  });
  for(i = 0; i < tr.length; ++i) tb.appendChild(tr[i]); // append each row in order
}

function makeSortable(table) {
  var th = table.tHead, i;
  th && (th = th.rows[0]) && (th = th.cells);
  if (th) i = th.length;
  else return; // if no `<thead>` then do nothing
  while (--i >= 0) (function (i) {
      var dir = 1;
      th[i].addEventListener('mouseover', function (e) {
        e.target.style.cursor = "pointer"
      });
      th[i].addEventListener('click', function () {
        sortTable(table, i, (dir = 1 - dir))
      });
  }(i));
}

function isInteger(str){
  return parseInt(str).toString() == str
}

function smartSort(a,b,col){
  var v1 = a.cells[col].textContent.trim();
  var v2 = b.cells[col].textContent.trim();
  if (isInteger(v1) && isInteger(v2)){
    return v1 - v2
  } else {
    return v1.localeCompare(v2)
  }
}

function makeAllSortable() {
  var t = document.getElementsByTagName('table'), i = t.length;
  while (--i >= 0) makeSortable(t[i]);
}
