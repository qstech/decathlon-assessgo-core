module Helpers

  def create_assessment(grid)

    def chapter_set(grid)
      chap1_id = Guideline.find_by(parent_id: nil, ref_model: grid).id
      sections = Guideline.where(parent_id: chap1_id)
      sections.map(&:section_name)
    end

    data = {
      supplier_code: SupplierProfile.first.code,
      assess_type: 'Gemba',
      assess_date: Date.today,
      supplier_profile_id: SupplierProfile.first.id,
      created_by: AssessorProfile.first.id,
      grid: grid,
    }

    post '/api/v1/quality_assessments', params: {
      quality_assessment: data,
      chapter_set: chapter_set(grid),
    }
  end

end