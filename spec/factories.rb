# frozen_string_literal: true

FactoryBot.define do
  factory :evaluation do
    limit_a { 1 }
    limit_b { 1 }
    limit_c { 1 }
    limit_d { 1 }
    description { "MyString" }
    guideline { nil }
  end
  
  factory :survey_question do
    belongs_to { "" }
    belongs_to { "" }
    question_tag { "MyString" }
    response { "MyString" }
    result { 1 }
    remarks { "MyText" }
    destroyed_at { "2021-05-13 19:22:14" }
    respondant { "MyString" }
  end

  factory :grid_version do
    grid { "MyString" }
    version_number { 1 }
    version_name { "MyString" }
    active { false }
  end

  factory :letter do
    to { "MyString" }
    mail_bag { nil }
    subject { "MyString" }
    cc { "MyString" }
    bcc { "MyString" }
    body { "MyText" }
    opened_at { "2020-11-11 13:24:16" }
    delivered_at { "2020-11-11 13:24:16" }
  end

  factory :mail_bag do
    cc { "MyString" }
    bcc { "MyString" }
    subject { "MyString" }
    body { "MyText" }
    trigger { "MyString" }
    tag { "MyString" }
  end

  factory :bug_report do
    bug_type { "MyString" }
    message { "MyString" }
    backtrace { "MyText" }
    user_params { "" }
    app { 1 }
    crash_level { 1 }
    feature { 1 }
    supplier_code { "MyString" }
    user_description { "MyText" }
  end

  factory :dpr_guideline do
    guideline_tag { "MyString" }
    title { "MyString" }
    content { "MyText" }
    process_name { "MyString" }
    code { "MyString" }
    chapter_no { 1 }
    section_no { 1 }
    grid { "MyString" }
    from_level { "MyString" }
    to_level { "MyString" }
  end

  factory :validation do
    quality_assessment { nil }
    control_plan { nil }
    result { "MyString" }
    validated { false }
    comments { "MyText" }
  end

  factory :assessor_application do
    grid { "MyString" }
    reason { "MyText" }
    opm { nil }
    validator { nil }
    opm_confirmed { false }
    opm_reason { "MyText" }
    trained_at { "2019-11-07 15:09:43" }
    status { "MyString" }
    completed { false }
    result { "MyString" }
  end

  factory :cap_followup do
    cap_resolution { nil }
    result { "MyString" }
    followup_date { "2019-09-11 17:31:36" }
    comments { "MyText" }
    resolved { false }
  end

  factory :change_log do
    version { "MyString" }
    changes { "MyText" }
    viewed_by { "MyText" }
  end

  factory :routine_question do
    guideline { nil }
    points_to_discuss { "MyText" }
    how_to_check { "MyText" }
    comments { "MyText" }
  end

  factory :search_history do
    query { "MyString" }
    options { "MyText" }
    user { nil }
  end

  factory :user do
    username { 'bloggins@testerino.com' }
    password { 'blahblahblah' }
  end

  factory :assessor, class: AssessorProfile do
    name { 'Bloggins' }
    user_id { create(:user).id }
  end

  factory :guideline do
    trait :qa do
      ref_model { 'chapter_assessment' }
      guideline_tag { 'QACH1' }
      title { 'ch qa title' }
    end
    trait :opex do
      ref_model { 'opex_assessment' }
      guideline_tag { 'OPEXCH1' }
      title { 'opex qa title' }
    end
    parent_id { nil }
    content { nil }
    active { true }
    section_name { 'Chapter 1' }
    section_type { 'Chapter' }
    section_no { 1 }
    note_header { nil }
    note_bullets { [] }
  end

  factory :supplier, class: SupplierProfile do
    name { 'Tyrell Corporation' }
    code { '12345' }
    active { true }
    user_id { create(:user, username: 'tyrell@tyrellcorp.com').id }
  end

  factory :assessment, class: QualityAssessment do
    supplier_code { SupplierProfile.first.code }
    assess_type { 'Gemba' }
    assess_date { Date.today }
    supplier_profile_id { SupplierProfile.first.id }
    created_by { AssessorProfile.first.id }

    trait :qa do
      grid { 'quality_assessment' }
    end

    trait :opex do
      grid { 'opex_assessment' }
    end
  end
end
