# frozen_string_literal: true

require 'rails_helper'
require './spec/support/helpers'

RSpec.configure do |c|
  c.include Helpers
end

describe 'Quality Assessments V1 API', type: :request do
  before(:all) do
    @assessor = create(:assessor)
    @supplier = create(:supplier)
    sign_in @assessor.user
    create_assessment('quality_assessment')
    create_assessment('opex_assessment')
  end

  describe 'GET /api/v1/members/assessment/index' do
    before(:all) do

      get '/api/v1/members/assessment/index'
    end

    it 'returns status code 200' do
      expect(response).to have_http_status(:success)
    end

    it 'returns qa assessment' do
      json_response = JSON.parse(response.body)
      expect(json_response['gemba_qacards'].length).to eq(1)
      expect(json_response['gemba_qacards'][0]['grid']).to eq('quality_assessment')

    end
  end

  describe 'GET /api/v1/members/assessment/index?ref_model=opex_assessment' do
    before(:all) do
      get '/api/v1/members/assessment/index?ref_model=opex_assessment'
    end

    it 'returns status code 200' do
      expect(response).to have_http_status(:success)
    end

    it 'returns qa assessment' do
      json_response = JSON.parse(response.body)
      expect(json_response['gemba_qacards'].length).to eq(1)
      expect(json_response['gemba_qacards'][0]['grid']).to eq('opex_assessment')
    end
  end

  describe 'GET /api/v1/members/assessment/new' do
    before(:all) do
      get '/api/v1/members/assessment/new'
    end

    it 'returns status code 200' do
      expect(response).to have_http_status(:success)
    end

    it 'returns qa chapters' do
      qa_ch = Guideline.find_by(guideline_tag: 'QACH1')
      json_response = JSON.parse(response.body)

      expect(json_response['chapters'][0]['title']).to eq("#{qa_ch.section_name} #{qa_ch.title}")
    end
  end

  describe 'GET /api/v1/members/assessment/new?ref_model=opex_assessment' do
    before(:all) do
      get '/api/v1/members/assessment/new?ref_model=opex_assessment'
    end

    it 'returns status code 200' do
      expect(response).to have_http_status(:success)
    end

    it 'returns opex chapters' do
      opex_ch = Guideline.find_by(guideline_tag: 'OPEXCH1')
      json_response = JSON.parse(response.body)

      expect(json_response['chapters'][0]['title']).to eq("#{opex_ch.section_name} #{opex_ch.title}")
    end
  end

  describe 'POST /api/v1/quality_assessments for qa_assessments' do
    before(:all) do

      def chapter_set
        chap1_id = Guideline.find_by(guideline_tag: 'QACH1').id
        sections = Guideline.where(parent_id: chap1_id)
        sections.map(&:section_name)
      end

      data = {
        assess_date: Date.today.strftime('%F'),
        assess_type: 'Gemba',
        created_by: @assessor.id,
        supplier_code: @supplier.code,
        supplier_profile_id: @supplier.id,
        grid: 'quality_assessment'
      }

      post '/api/v1/quality_assessments', params: {
        quality_assessment: data,
        chapter_set: chapter_set
      }
    end

    it 'returns status code 200' do
      expect(response).to have_http_status(:ok)
    end

    it 'returns prep complete false' do
      json_response = JSON.parse(response.body)
      expect(json_response['object']['prep_completed']).to eq(false)
    end
  end

  describe 'POST /api/v1/quality_assessments for opex_assessments' do
    before(:all) do

      def chapter_set
        chap1_id = Guideline.find_by(guideline_tag: 'OPEXCH1').id
        sections = Guideline.where(parent_id: chap1_id)
        sections.map(&:section_name)
      end

      data = {
        assess_date: Date.today.strftime('%F'),
        assess_type: 'Gemba',
        created_by: @assessor.id,
        supplier_code: @supplier.code,
        supplier_profile_id: @supplier.id,
        grid: 'opex_assessment'
      }

      post '/api/v1/quality_assessments', params: {
        quality_assessment: data,
        chapter_set: chapter_set,
      }
    end

    it 'returns status code 200' do
      expect(response).to have_http_status(:ok)
    end

    it 'returns prep complete true' do
      json_response = JSON.parse(response.body)
      expect(json_response['object']['prep_completed']).to eq(true)
    end
  end
end
