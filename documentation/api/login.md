# API 介绍

## API Hosts & VERSION

有两个域名：
**APP的代码里面必需有一个variable可以来换访问的API是测试服务器还是真实服务器。**

```js
// 测试服务器
"https://assessgopp.decathlon.cn/api/v1/..."

// 真实服务器
"https://assessgo.decathlon.cn/api/v1/..."
```

也需要保存一下API的Version:  `VERSION='3.0.4'`
登录的访问需要提供的。


---

## HEADERS

除了登录相关的页面以外，所有的页面必需发headers

```js
{"X-USER-USERNAME": username, "X-USER-TOKEN": token}
````
- username是用户登录的帐户。
- token是登录的时候收到的token。
- 24小时内可以用同一个token。

---

## LOGIN

1. 每次load APP需要先查询用户登录的状态怎样：
  POST `${API_PATH}/pages/app/launch`
  Headers: 没有
  Body: {openid: 'someopenid', current: VERSION}
  - *openid: 如果APP的cache里面有，可以发一下*
  - *如果没有openid，登录后API会创建一个，发给用户*
  - *请注意：测试服务器会自动登录TESTUSER，不需要走用户名密码的流程*

  Response (success):
  ```js
    {
      "code":200,
      "login":true,
      "launch":"debug",
      "object":{
        "id":239,
        "username":"testuser",
        "name":"TestUser",
        "profile":{
          "id":307,
          "user_id":239,
          "handle":"testuser",
          "assessor_for":[],
          "candidate_for":[],
          "created_at":"2019-04-12T14:11:58.490Z",
          "updated_at":"2019-06-25T06:15:48.004Z",
          "name":"TestUser",
          "referent_for":[],
          "ref_candidate_for":[],
          "city":"Not Set",
          "title":"opmsd",
          "country":"CN",
          "avatar":null,
          "avatar_thumbs":null
        },
        "last_popup":null,
        "avatar":"",
        "openid":"",
        "unionid":false,
        "locale":"en",
        "assessor":true,
        "decathlon":true,
        "candidate":false,
        "assessor_for":[]
      },
      "user":{
        "username":"testuser",
        "token":"j27urQtecyj8i5TMoJ_X"
      },
      "headers":{
        "X-USER-USERNAME":"testuser",
        "X-USER-TOKEN":"j27urQtecyj8i5TMoJ_X"
      },
      "hide_preload":true
    }
  ```

  Response (fail):

  ```js
  {
    "code": 300, //300 or 401
    "launch": "production",
    "msg": "no user associated with this wechat account" //or other message
  }
  ```

2. 如果做第（1）的时候用户没有成功，让用户填用户名密码然后访问：
  POST `${API_PATH}/pages/sign-in/sign-in`
  Headers: 没有
  Body: { data: {username: username, password: password}, openid: openid, current: VERSION }
  - *username: 用户填表提供的*
  - *password: 用户填表提供的*
  - *openid: 如果APP的cache里面有，可以发一下*

  Response (success):
  访问到欧洲需要一点时间，所以先respond一下的信息。收到了后需要没两秒访问第（3）的API步
  ```js
    {
      "code":200,
      "login":false,
      "login_status":"Intializing connection..."
    }
  ```

3. 查看第（2）步的用户名密码有没有输入对的：
  POST `${API_PATH}/login-status`
  Headers: 没有
  Body: { data: {username: username, password: password}, openid: openid, current: VERSION }
  - *username: 用户填表提供的*
  - *password: 用户填表提供的*
  - *openid: 如果APP的cache里面有，可以发一下*

  Response (in progress):
  如果收到这个，用户需要看在loading的状态。
  ```js
    {
      "code":888,
      "login":false,
      "login_status":"Connecting to Decathlon Login System..." // or other message.
    }
  ```

  Response (fail):
  如果用户收到这个，用户需要回到sign-in的页面
  ```js
    {
      "code": 402,
      "login": false,
      "msg": "Invalid Username or Password!"
    }
  ```

  Response (success):
  如果用户收到这个，用户可以继续到主页
  ```js
    {
      "code":200,
      "login":true,
      "object":{
        "id":239,
        "username":"testuser",
        "name":"TestUser",
        "profile":{
          "id":307,
          "user_id":239,
          "handle":"testuser",
          "assessor_for":[],
          "candidate_for":[],
          "created_at":"2019-04-12T14:11:58.490Z",
          "updated_at":"2019-06-25T06:15:48.004Z",
          "name":"TestUser",
          "referent_for":[],
          "ref_candidate_for":[],
          "city":"Not Set",
          "title":"opmsd",
          "country":"CN",
          "avatar":null,
          "avatar_thumbs":null
        },
        "last_popup":null,
        "avatar":"",
        "openid":"",
        "unionid":false,
        "locale":"en",
        "assessor":true,
        "decathlon":true,
        "candidate":false,
        "assessor_for":[]
      },
      "headers":{
        "X-USER-USERNAME":"testuser",
        "X-USER-TOKEN":"JHaQGuZFYvuPS8qUYgjo"
      },
      "user":{
        "username":"testuser",
        "token":"JHaQGuZFYvuPS8qUYgjo"
      },
      "hide_preload":true
    }
  ```
---

