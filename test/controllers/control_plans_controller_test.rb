require 'test_helper'

class ControlPlansControllerTest < ActionDispatch::IntegrationTest
  test "should get product_families" do
    get control_plans_product_families_url
    assert_response :success
  end

  test "should get update_product_family" do
    get control_plans_update_product_family_url
    assert_response :success
  end

  test "should get process_steps" do
    get control_plans_process_steps_url
    assert_response :success
  end

  test "should get update_process_step" do
    get control_plans_update_process_step_url
    assert_response :success
  end

end
