require 'test_helper'

class Api::V1::UserSessionsControllerTest < ActionDispatch::IntegrationTest
  test "should get force_login" do
    get api_v1_user_sessions_force_login_url
    assert_response :success
  end

  test "should get logout" do
    get api_v1_user_sessions_logout_url
    assert_response :success
  end

  test "should get login" do
    get api_v1_user_sessions_login_url
    assert_response :success
  end

  test "should get login_status" do
    get api_v1_user_sessions_login_status_url
    assert_response :success
  end

  test "should get set_mode" do
    get api_v1_user_sessions_set_mode_url
    assert_response :success
  end

  test "should get change_password" do
    get api_v1_user_sessions_change_password_url
    assert_response :success
  end

end
