require 'test_helper'

class Api::V1::CustomDataControllerTest < ActionDispatch::IntegrationTest
  test "should get cap_form" do
    get api_v1_custom_data_cap_form_url
    assert_response :success
  end

  test "should get cap_index" do
    get api_v1_custom_data_cap_index_url
    assert_response :success
  end

  test "should get caps" do
    get api_v1_custom_data_caps_url
    assert_response :success
  end

  test "should get cap_confirmation" do
    get api_v1_custom_data_cap_confirmation_url
    assert_response :success
  end

  test "should get cap_followup_create" do
    get api_v1_custom_data_cap_followup_create_url
    assert_response :success
  end

  test "should get cap_followup" do
    get api_v1_custom_data_cap_followup_url
    assert_response :success
  end

  test "should get cap_followup_invite" do
    get api_v1_custom_data_cap_followup_invite_url
    assert_response :success
  end

  test "should get cap_related" do
    get api_v1_custom_data_cap_related_url
    assert_response :success
  end

  test "should get pdf_finished" do
    get api_v1_custom_data_pdf_finished_url
    assert_response :success
  end

  test "should get quality_assessments" do
    get api_v1_custom_data_quality_assessments_url
    assert_response :success
  end

  test "should get kpi_create" do
    get api_v1_custom_data_kpi_create_url
    assert_response :success
  end

  test "should get get_oa_token" do
    get api_v1_custom_data_get_oa_token_url
    assert_response :success
  end

  test "should get get_wx_token" do
    get api_v1_custom_data_get_wx_token_url
    assert_response :success
  end

  test "should get section_na" do
    get api_v1_custom_data_section_na_url
    assert_response :success
  end

  test "should get invitation" do
    get api_v1_custom_data_invitation_url
    assert_response :success
  end

end
