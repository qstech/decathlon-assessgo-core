require 'test_helper'

class Api::V1::ControlPlanControllerTest < ActionDispatch::IntegrationTest
  test "should get search" do
    get api_v1_control_plan_search_url
    assert_response :success
  end

  test "should get new_product" do
    get api_v1_control_plan_new_product_url
    assert_response :success
  end

  test "should get report" do
    get api_v1_control_plan_report_url
    assert_response :success
  end

  test "should get product_list" do
    get api_v1_control_plan_product_list_url
    assert_response :success
  end

  test "should get review" do
    get api_v1_control_plan_review_url
    assert_response :success
  end

  test "should get index" do
    get api_v1_control_plan_index_url
    assert_response :success
  end

  test "should get product" do
    get api_v1_control_plan_product_url
    assert_response :success
  end

  test "should get kpi" do
    get api_v1_control_plan_kpi_url
    assert_response :success
  end

  test "should get history" do
    get api_v1_control_plan_history_url
    assert_response :success
  end

  test "should get schedule_create" do
    get api_v1_control_plan_schedule_create_url
    assert_response :success
  end

  test "should get review_detail" do
    get api_v1_control_plan_review_detail_url
    assert_response :success
  end

  test "should get cap_confirmation" do
    get api_v1_control_plan_cap_confirmation_url
    assert_response :success
  end

  test "should get findings" do
    get api_v1_control_plan_findings_url
    assert_response :success
  end

  test "should get step_detail" do
    get api_v1_control_plan_step_detail_url
    assert_response :success
  end

  test "should get step_edit" do
    get api_v1_control_plan_step_edit_url
    assert_response :success
  end

  test "should get new_step" do
    get api_v1_control_plan_new_step_url
    assert_response :success
  end

  test "should get list" do
    get api_v1_control_plan_list_url
    assert_response :success
  end

  test "should get schedule" do
    get api_v1_control_plan_schedule_url
    assert_response :success
  end

  test "should get cp_history" do
    get api_v1_control_plan_cp_history_url
    assert_response :success
  end

end
