require 'test_helper'

class Api::V1::AssessorApplicationControllerTest < ActionDispatch::IntegrationTest
  test "should get validator" do
    get api_v1_assessor_application_validator_url
    assert_response :success
  end

  test "should get qualification" do
    get api_v1_assessor_application_qualification_url
    assert_response :success
  end

  test "should get application" do
    get api_v1_assessor_application_application_url
    assert_response :success
  end

  test "should get application_selection" do
    get api_v1_assessor_application_application_selection_url
    assert_response :success
  end

  test "should get training_time" do
    get api_v1_assessor_application_training_time_url
    assert_response :success
  end

  test "should get application_status" do
    get api_v1_assessor_application_application_status_url
    assert_response :success
  end

end
