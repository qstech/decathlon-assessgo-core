require 'test_helper'

class Api::V1::DataControllerTest < ActionDispatch::IntegrationTest
  test "should get destroy" do
    get api_v1_data_destroy_url
    assert_response :success
  end

  test "should get index" do
    get api_v1_data_index_url
    assert_response :success
  end

  test "should get delete_image" do
    get api_v1_data_delete_image_url
    assert_response :success
  end

  test "should get read_comments" do
    get api_v1_data_read_comments_url
    assert_response :success
  end

  test "should get show" do
    get api_v1_data_show_url
    assert_response :success
  end

  test "should get create" do
    get api_v1_data_create_url
    assert_response :success
  end

  test "should get update" do
    get api_v1_data_update_url
    assert_response :success
  end

end
