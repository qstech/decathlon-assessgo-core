require 'test_helper'

class Api::V1::QualityAssessmentsControllerTest < ActionDispatch::IntegrationTest
  test "should get supplier_info" do
    get api_v1_quality_assessments_supplier_info_url
    assert_response :success
  end

  test "should get index" do
    get api_v1_quality_assessments_index_url
    assert_response :success
  end

  test "should get cap_index" do
    get api_v1_quality_assessments_cap_index_url
    assert_response :success
  end

  test "should get go_for_assessment_review" do
    get api_v1_quality_assessments_go_for_assessment_review_url
    assert_response :success
  end

  test "should get go_for_assessment" do
    get api_v1_quality_assessments_go_for_assessment_url
    assert_response :success
  end

  test "should get set_qa_chapters" do
    get api_v1_quality_assessments_set_qa_chapters_url
    assert_response :success
  end

  test "should get new" do
    get api_v1_quality_assessments_new_url
    assert_response :success
  end

  test "should get assessment_prep_review" do
    get api_v1_quality_assessments_assessment_prep_review_url
    assert_response :success
  end

  test "should get historical_level" do
    get api_v1_quality_assessments_historical_level_url
    assert_response :success
  end

  test "should get invitations" do
    get api_v1_quality_assessments_invitations_url
    assert_response :success
  end

  test "should get single_chapter" do
    get api_v1_quality_assessments_single_chapter_url
    assert_response :success
  end

  test "should get chapter_assessment_review" do
    get api_v1_quality_assessments_chapter_assessment_review_url
    assert_response :success
  end

  test "should get single_chapter_review" do
    get api_v1_quality_assessments_single_chapter_review_url
    assert_response :success
  end

  test "should get review_assessment" do
    get api_v1_quality_assessments_review_assessment_url
    assert_response :success
  end

  test "should get pre_confirmation" do
    get api_v1_quality_assessments_pre_confirmation_url
    assert_response :success
  end

  test "should get chapter_assessment" do
    get api_v1_quality_assessments_chapter_assessment_url
    assert_response :success
  end

  test "should get overview" do
    get api_v1_quality_assessments_overview_url
    assert_response :success
  end

  test "should get assessment_prep" do
    get api_v1_quality_assessments_assessment_prep_url
    assert_response :success
  end

  test "should get report" do
    get api_v1_quality_assessments_report_url
    assert_response :success
  end

end
