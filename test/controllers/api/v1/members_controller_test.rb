require 'test_helper'

class Api::V1::MembersControllerTest < ActionDispatch::IntegrationTest
  test "should get manual" do
    get api_v1_members_manual_url
    assert_response :success
  end

  test "should get members_show_history" do
    get api_v1_members_members_show_history_url
    assert_response :success
  end

  test "should get activity_more" do
    get api_v1_members_activity_more_url
    assert_response :success
  end

  test "should get members_detail" do
    get api_v1_members_members_detail_url
    assert_response :success
  end

  test "should get activity_show" do
    get api_v1_members_activity_show_url
    assert_response :success
  end

  test "should get account_settings" do
    get api_v1_members_account_settings_url
    assert_response :success
  end

  test "should get search_form" do
    get api_v1_members_search_form_url
    assert_response :success
  end

  test "should get members_show" do
    get api_v1_members_members_show_url
    assert_response :success
  end

  test "should get me" do
    get api_v1_members_me_url
    assert_response :success
  end

  test "should get home" do
    get api_v1_members_home_url
    assert_response :success
  end

  test "should get search_results" do
    get api_v1_members_search_results_url
    assert_response :success
  end

  test "should get activity" do
    get api_v1_members_activity_url
    assert_response :success
  end

end
