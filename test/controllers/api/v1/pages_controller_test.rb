require 'test_helper'

class Api::V1::PagesControllerTest < ActionDispatch::IntegrationTest
  test "should get supplier_info" do
    get api_v1_pages_supplier_info_url
    assert_response :success
  end

  test "should get guidelines" do
    get api_v1_pages_guidelines_url
    assert_response :success
  end

  test "should get components" do
    get api_v1_pages_components_url
    assert_response :success
  end

  test "should get form" do
    get api_v1_pages_form_url
    assert_response :success
  end

  test "should get forget_password" do
    get api_v1_pages_forget_password_url
    assert_response :success
  end

  test "should get logs" do
    get api_v1_pages_logs_url
    assert_response :success
  end

  test "should get loading" do
    get api_v1_pages_loading_url
    assert_response :success
  end

  test "should get coming_soon" do
    get api_v1_pages_coming_soon_url
    assert_response :success
  end

end
