# source 'https://rubygems.org'
source 'https://gems.ruby-china.com'
# git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.6.3'

# CORE GEMS ======================================
gem 'rails', '~> 5.2.1'
gem 'pg', '>= 0.18', '< 2.0'
gem 'puma', '~> 3.11'
gem 'redis', '~> 4.0'
gem "sidekiq"
gem 'coffee-rails', '~> 4.2'

# Authentication =================================
gem 'devise'
gem 'tiddle' # For Devise-Token Authentication (API-DEVISE)
gem 'encryption' # gem for Enrypting/decrypting used in authentication system
gem 'rest-client', '~> 2.0.2' # for backend API communication
gem 'jwt' # Used to decrypt OAUTH2 package
gem 'mechanize' # Virtual browser used in authentication for OAUTH2

# Assets =========================================
gem 'actiontext', github: 'rails/actiontext', branch: 'archive', require: 'action_text'
gem 'webpacker', '~> 5.1.1'
gem 'image_processing'
gem 'sassc-rails'
gem 'uglifier', '>= 1.3.0'
gem 'turbolinks', '~> 5'
gem "azure-storage", require: false # Active Storage Adapter
gem 'mini_magick', '~> 4.8' # Use ActiveStorage variant in Rails 5.2
gem 'jquery-rails', '~> 4.4.0'
gem "letter_opener", group: :development
gem 'pagy', '~> 3.5'

# DOCUMENT INTERACTION ===========================
gem 'creek' # for Excel Parsing
gem 'rubyXL' # for Excel Editing
gem 'spreadsheet' # for Excel 2003 and older
gem 'ttfunk', '~> 1.5.1'
gem 'prawn' # for PDF Generation
# Docsplit for splitting PDF's has MANY dependencies:
# https://documentcloud.github.io/docsplit/
gem 'docsplit'

# Utility ========================================
gem 'json_translate' # Tranlsation of ActiveRecord column data
# gem 'whenever' # for basic CRON scheduling (used well w/ sidekiq for recurring work jobs)
gem 'paper_trail' # Allow historical tracking of column data & changes made
gem 'iso_country_codes' # for converting DKN suppier countries
gem 'jbuilder', '~> 2.5'
gem 'ip2location_ruby' # For IP to Country mapping

# data & graphs ==================================
gem 'daru-view', '~> 0.2.5'
gem "daru", '~> 0.2.2'
gem 'daru-data_tables', '~> 0.3.5'
# gem 'chartkick'
gem 'groupdate'

# Devops/Deployment ==============================
gem 'bootsnap', '>= 1.1.0', require: false # Reduces boot times through caching; required in config/boot.rb
gem 'rb-readline' # Fix libread dependency issues in production


group :development, :test do
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
  gem 'rspec-rails', '~> 3.6'
  gem 'factory_bot_rails'
  gem 'database_cleaner'
  gem 'faker'
end

group :development do
  gem 'rubocop'
  gem 'web-console', '>= 3.3.0'
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
  gem 'guard-livereload', '~> 2.5', require: false
end


gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]


