require 'json'
arr = JSON.parse(File.read("#{Rails.root}/lib/spl_list.json"))
SupplierProfile.update_all(ib_emails: [])
arr.each do |x|
  supp = SupplierProfile.find_by_code(x["code"])
  next if supp.blank?
  u = User.find_by(username: x["username"].downcase)
  if u.blank?
    puts "PL NOT A USER:  #{x["username"]}"
  else
    supp.pl_set << u.assessor_profile.id
    supp.pl_set.uniq!
  end

  supp.ib_emails << x["email"]
  supp.save
end
