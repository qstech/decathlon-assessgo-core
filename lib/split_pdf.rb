require 'docsplit'
files = "/Users/sergiorivas/Downloads/2. Storage/0. General (Storage)"
OUTPUT = "#{Rails.root}/public/toolbox"
splittable = Dir.glob("#{files}/*.pdf")

puts "BEGIN SPLITTING"
start = Time.now

splittable.each do |file|
  Docsplit.extract_images(file, format: :png, output: OUTPUT)
end
puts "this took #{Time.now - start} seconds"

filenames = Dir.glob("#{OUTPUT}/2.0*.png")
file_counts = {}

filenames.each_with_index do |file, i|
  num = file.split('/').last.gsub(/(2\.0\.\d+)\s?([AB]).*/, '\1\2')
  file_counts[num] = 0 if file_counts[num].nil?
  file_counts[num] +=1
  new_filename = "dpr_sleeping_#{num}-ALL#{file_counts[num]}.png"
  File.rename(file, "#{OUTPUT}/#{new_filename}")
end
