def recursive_merge(h1, h2)
  res = {}
  h1.each do |k,v|
    if h2[k].nil?
      res[k.to_s] = h1[k]
    elsif v.is_a? Hash
      res[k.to_s] = recursive_merge(h1[k],h2[k])
    else
      res[k.to_s] = h2[k] || h1[k]
    end
  end
  res
end
