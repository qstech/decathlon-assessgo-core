User.sync_oa!
# # 7. The preparation list on QA in Assess Go: 1~7,10 allow pictures, 8,9, 11, 12, 16, 17 no need pictures. 13, 14, 18 ,20allow pictures. 15, 19, 21, 22 need to fill in info.(Mini App only allows pictures, other format go through web input)

# require_imgs = (1..7).to_a + [10,13,14,18,20]
# require_input = [15,19,21,22]

# require_imgs.map!{|x| "QAPREP#{x}"}
# require_input.map!{|x| "QAPREP#{x}"}

# FormQuestion.where(question_tag: require_imgs).update_all(require_attachment: true)
# FormQuestion.where(question_tag: require_input).update_all(component: "input")

# # ProductFamily.all.each do |pf|
# #   pf.product_processes
# # end

# gl = Guideline.find_by(guideline_tag: "SSECH1S1L2-1")
# RoutineQuestion.find_or_create_by({
#   guideline_id: gl.id,
#   points_to_discuss: "岗位技能说明需要包含的技能类型",
#   how_to_check: "查看供应链每个部门的岗位说明书，作业指导书和岗位技能需求表。并查询历史更新记录，如没有记录，询问工厂是否有文件规定以何频率或在哪种情况下需要更新",
#   comments: "Check with Angela"
#   })

# gl = Guideline.find_by(guideline_tag: "SSECH1S1L2-2")
# RoutineQuestion.find_or_create_by({
#   guideline_id: gl.id,
#   points_to_discuss: "工作交接文件内容需详细到何种程度",
#   how_to_check: "查看供应链各岗位的工作交接文件和流程是否齐全，是否合理。",
#   comments: "Get more BP from B/C"
#   })

# gl = Guideline.find_by(guideline_tag: "SSECH1S1L2-3")
# RoutineQuestion.find_or_create_by({
#   guideline_id: gl.id,
#   how_to_check: "确认工厂是否有相应师傅来培训新员工，并确认各岗位在休假/请假期间的替代人员是否完备"
#   })

# cl = ChangeLog.find_or_create_by(version: "1.8.0")

# cl.change_list = [
#   "Fixed various bugs in user Performance Page",
#   "Added support for sharing personal performance page",
#   "QR Scanning page load bugs resolved",
#   "Added OPEX & SSE to Goes Feed",
#   "Fixed bugs in Comparable Search",
#   "Added Supplier Frequency Rate to CP KPI overview box",
#   "Add support for adding/removing Process Flow Images in CP Product Page",
#   "Improved sorting logic for CP process steps"
# ]
# cl.save

# cl = ChangeLog.find_or_create_by(version: "1.8.5")

# cl.change_list = [
#   "Added HRP",
#   "HRP Toolbox Complete",
#   "Added support for HRP supplier preparation questions",
#   "Added All to Comparative Search for Countries & Industrial Process",
#   "Added Partner Suppliers to Comparative Search",
#   "Assessments can now link directly to specific Levels (D,C,B, etc.)",
#   "Fixed Edit Permissions for historical assessments",
#   "Added Explainer Videos for each main feature"
# ]
# cl.save

# cl = ChangeLog.find_or_create_by(version: "1.9.5")

# cl.change_list = [
#   "Fixed Problems w/ CP Process Step Edit Forms",
#   "Fixed explainer video bugs for iOS devices",
#   "Added Multiple Scores support to Comparative Search",
#   "Cleaned up design for web-based PDF Generation",
#   "Added CP History to Product Page",
#   "Fixed bugs in webform autosave",
#   "Added Edit Prep Questions to Web Guidelines",
#   "Allow blank submission for N/A in CP Monitoring",
#   "Updated Review count to disregard frequency",
#   "Updated SSE Toolbox",
#   "Fixed bugs in CAP planning for assessments",
#   "Added historical result to assessments"
# ]
# cl.save
# # print `rails runner db/seeds.rb base`

# emails = ["james.song@wheeltop.com",
# "estero@igm-grafiche.it",
# "15165421301@163.com",
# "zhoutong@163.com",
# "shen@163.com",
# "davidsongomes80@yahoo.com",
# "misschen123@163.com",
# "jackal.chiu@groupfu.com.tw",
# "rock.zhang@arcanapower.com",
# "deepal.jayasinghe@sl.crystal-martin.com",
# "thuquyen@tng.vn",
# "carnie.ng@ap.averydennison.com",
# "evan@asiglovemail.com",
# "jayesh@omaxcotspin.com",
# "dafin.versan@smw.ro",
# "jorge.salgado@rte.pt",
# "chihyao.chuang@duro.co.th",
# "ym@nttms.com",
# "nicolai@welldyeing.com",
# "hoanghongnhung1603@gmail.com",
# "sl06@sunnywheel.com.tw",
# "hogan.hou@sab-cn.com",
# "lujg@helmets.gd.cn",
# "jagan.arul@sakthiinfra.com",
# "mosharraf.hossain@lizfashion.com",
# "nora.hsu@groupfu.com.tw",
# "shengchan@ah-huayang.com",
# "terry@sbszipper.com",
# "zzxj@kolmar.co.jp",
# "277049370@qq.com",
# "liutao@csigroup.com.cn",
# "Henry.cui@juntai-tech.com",
# "354420719@qq.com",
# "mudan.luo@rfx-care.com",
# "13913296628@163.com",
# "yanzhou@glanbia.com",
# "qiaohongzhuang@qq.com",
# "catherine.li@chaucerfoodsqd.com",
# "cuiyunshan@andros.com.cn",
# "jiangdongrui@toyopackch.com",
# "vittorio@olagnero.it",
# "pincha.t.otis@atgsports.com.cn",
# "lily@sunfreda.com.cn",
# "guyamei@shdongxia.cn",
# "gu123@163.com",
# "prosenjit@mafshoes.com",
# "yf151@tektro.com.cn",
# "liub@zjsunrising.com",
# "fabio@marcweb.it",
# "sshahadat@youngonectg.com",
# "lee@dejuntextile.com",
# "abbydai@yong-qi.com",
# "mario.lencioni@dssmith.com",
# "swift_tsai@rtfiber.co.th",
# "Rita@sion-eagle.com",
# "stefan.vitale@ipaper.it",
# "joyce@yong-qi.com",
# "mnnathan@fgmc.in",
# "ibrahim@knitasia.com",
# "nazmul@nassagroup.org",
# "ddk@ddkbike.com",
# "rina@korrun.com",
# "liangjinshan@fs-mzfz.com",
# "rita@jdco.com.tw",
# "15170897581@163.com",
# "dc@vpcomponents.com",
# "anita.rao@winson.com.hk",
# "15165421301@163.com",
# "st06@snowtex.org",
# "janet.zhang@mainetti.com",
# "philip.tang@utxgroup.com",
# "danny@vpcomponents.com",
# "tiger@vpcomponents.com",
# "ruomei.qiu@chinajifa.com",
# "thrzxs@textaihua.com",
# "harry.fan@sunfreda.com.cn",
# "anna.yan@hengfeng-china.com",
# "tom.liangjiajie@fenda.com",
# "qc@ezonwatch.com",
# "jane@betrueco.com",
# "karasawa_jaime@china-karasawa.com",
# "solar.xu@chinahulong.com",
# "cz@qiaoertt.com",
# "lan@jsgoose.com"]

# supps = SupplierProfile.includes(:user).where.not(contact_email: emails).where.not(user_id: nil)
# supps.each do |x|
#   x.update(invite_sent_at: Time.now)
#   x.message_user!("welcome")
# end

# supps = SupplierProfile.includes(:user).where(contact_email: emails)
# supps.each do |x|
#   next if x.invite_sent_at.present? && x.invite_sent_at > 6.days.ago
#   x.emails_sent = [] if x.emails_sent.nil?
#   if x.user.authentication_tokens.blank?
#     x.emails_sent << "5_no_login (#{Time.now.strftime('%Y-%m-%d')})"
#     x.message_user!("5_no_login")
#     x.save
#   else
#     x.emails_sent << "5_login (#{Time.now.strftime('%Y-%m-%d')})"
#     x.message_user!("5_login")
#     x.save
#   end
# end

cl = ChangeLog.find_or_create_by(version: "3.0.5")

cl.change_list = [
  "New Connections between related Assessments & CAP's",
  "Updated Generate CAP Process"
]
cl.save


