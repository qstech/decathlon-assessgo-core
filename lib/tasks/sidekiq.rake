require 'sidekiq/api'
namespace :sidekiq do
  task clear: :environment do
    Sidekiq::Queue.all.each(&:clear)
    Sidekiq::RetrySet.new.clear
    Sidekiq::ScheduledSet.new.clear
    Sidekiq::DeadSet.new.clear
  end

  task run: :environment do
    `bundle exec sidekiq -q login -q default -q mailers -q reports`
  end
end
