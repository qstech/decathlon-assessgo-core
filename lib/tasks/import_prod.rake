namespace :db do
    task import_prod: :environment do
        Rake::Task["db:drop"].execute
        puts "dropped current local db"
        Rake::Task["db:create"].execute
        puts "created new local db"
        puts "importing production data ..."
        `sh lib/get_db.sh`
        puts "imported production data"
        Rake::Task["db:migrate"].execute
    end
end
