def seed_product(content, code)
  supp = SupplierProfile.find_by(code: code)
  if supp.blank?
    puts "SUPPLIER NOT FOUND! #{code}"
    return false
  end

  puts "SEEDING: #{code}"

  pf_hash = content["product_family"]

  pf = ProductFamily.find_or_create_by(supplier_profile_id: supp.id, name: pf_hash["name"].strip)
  pf_hash["processes"].each do |process|
    process["name"] = process["category"] if process["name"].blank?
    pr = ProductProcess.find_or_create_by(product_family_id: pf.id, name: process["name"], category: process["category"])
    process["steps"].each do |step|
      ps = ProcessStep.find_or_create_by(step.merge({product_process_id: pr.id}))
    end
  end
end

folder = "#{Rails.root}/db/data/product_families"
files  = Dir.entries(folder).select{|x| x.include? "json"}

files.each do |file|
  code = file.split("-").first
  content = JSON.parse(File.read("#{folder}/#{file}"))
  seed_product(content, code)
end


