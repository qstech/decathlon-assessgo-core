def fsection(str)
  section,point = str.split(" ")
  Guideline.where(section_name: section).first
end

def flevel(str)
  section,point = str.split(" ")
  lev, = point.split("-")
  Guideline.joins(:parent).where(parents_guidelines: {section_name: section}).where(section_name: lev).first
end

def fpoint(str)
  section,point = str.split(" ")
  Guideline.joins(parent: :parent).where(parents_guidelines_2: {section_name: section}).where(section_name: point).first
end

def section_add(str)
  chapter = str.split(".").first
  section,point = str.split(" ")

  ch = Guideline.find_by(section_type: "Chapter", section_no: chapter.to_i,
    ref_model: "hfwb_assessment")
  attrs = { parent_id: ch.id, ref_model: "hfwb_assessment", guideline_tag: "#{ch.guideline_tag}S#{ch.children.count + 1}", active: true, section_name: section, section_type: "Section", section_no: ch.section_no, relavent_products: [], nutrition: false, grid_version_id: ch.grid_version_id}
  Guideline.create(attrs)
end

def level_add(str)
  section,point = str.split(" ")
  level,pt_no = point.split("-")
  sec = Guideline.find_by(section_name: section)

  letters = ("A".."F").to_a.reverse
  lev_attrs = attrs = {parent_id: sec.id, ref_model: sec.ref_model,
    guideline_tag: "#{sec.guideline_tag}L#{letters.index(level)}",
    section_name: level, section_type: "Level", section_no: letters.index(level),
    title_en: "Level #{level}", title_zhcn: "Level #{level}",
    relavent_products: sec.relavent_products, grid_version_id: sec.grid_version_id
  }
  Guideline.create(lev_attrs)
end

def point_add(str)
  section,point = str.split(" ")
  level,pt_no = point.split("-")

  sec = Guideline.find_by(section_name: section)
  sec = section_add(str) if sec.nil?

  lev = sec.children.find{|x| x.section_name == level}
  lev = level_add(str) if lev.nil?
  attrs = {parent_id: lev.id, ref_model: lev.ref_model,
    guideline_tag: "#{lev.guideline_tag}-#{pt_no}",
    section_name: point, section_type: "Point", section_no: pt_no.to_i,
    title_en: point, title_zhcn: point, content_en: "NEEDS UPDATE",
    relavent_products: lev.relavent_products, grid_version_id: lev.grid_version_id
  }
  Guideline.create(attrs)
end

def add_product(gl, product)
  gl.relavent_products << product
  gl.save
end

def enforce_product(product, chapter)
  point_add(chapter) if fpoint(chapter).nil?

  add_product(fsection(chapter), product) unless fsection(chapter).nil?
  add_product(flevel(chapter), product) unless flevel(chapter).nil?
  add_product(fpoint(chapter), product) unless fpoint(chapter).nil?
end
# set.each do |x|
#   enforce_product("stitched_balls", x)
# end

def rm_product(product, chapter)
  fsection(chapter).self_and_descendents.each do |x|
    x.relavent_products.delete(product)
    x.save
  end
end

def clean_dup_ca_all!
  problems = ChapterAssessment.all.group_by { |x| "#{x.quality_assessment_id} #{x.chapter_tag}" }.select { |_, v| v.length > 1 }

  problems.each_value do |all_cas|
    vs = all_cas.select { |v| v.cotation.nil? }
    vs = vs[1..] if vs.length == all_cas.length
    vs.each(&:destroy)
  end
end


def clean_dup_ca(assessment)
  dups = assessment.chapter_assessments.group(:chapter_tag).count.select { |_, v| v > 1 }.keys
  problems = assessment.chapter_assessments.where(chapter_tag: dups).order(:id).group_by(&:chapter_tag)
  problems.each_value { |vs| vs.select { |v| v.cotation.nil? }.last.destroy }
  problems.each_value do |vs|
    if vs.map(&:cotation).include?(nil) && vs.length > 1
      vs.select { |v| v.cotation.nil? }.last.destroy
    end
  end
end
