#!/bin/bash

ssh_attrs=""
# your_host can be an ip or domain
ssh_host="srivas@assessgo.decathlon.cn"
ssh="ssh $ssh_host $ssh_attrs"
db_dev="assess-go-api_development"
db_to_import="assessProduction"

$ssh "dokku postgres:export $db_to_import > /tmp/$db_to_import.dump"
rsync -avz -e "ssh $ssh_attrs" $ssh_host:/tmp/$db_to_import.dump /tmp/$db_to_import.dump
pg_restore -O -d $db_dev /tmp/$db_to_import.dump

# then we download the uploads folder, which we’ve mounted as persistent storage in our dokku app, see http://glennjon.es/2016/10/24/adding-persistent-storage-to-dokku.html for more information

# prod_uploads_dir should be the dir that you've mounted the persistent storage to, use `dokku storage:list name_of_your_dokku_app` on your server to see that

prod_uploads_dir="/var/lib/dokku/data/storage/assessgo/uploads/"

ls -d public/uploads
if [ $? -eq 0 ]
then
    rsync -avz -e "ssh $ssh_attrs" $ssh_host:$prod_uploads_dir public/uploads
fi
