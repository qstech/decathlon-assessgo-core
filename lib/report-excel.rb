require 'rubyXL'
require 'rubyXL/convenience_methods'
qa = QualityAssessment.where(grid: "quality_assessment", assess_type: "Annual", completed: true).first
pls = AssessorProfile.where(id: qa.assessment_day_form.pl_set.push(qa.assessment_day_form.pl_id).reject(&:nil?))
result = qa.result > qa.target ? "NEED A CAP" : "NO CAP NEEDED"
workbook = RubyXL::Parser.parse("#{Rails.root}/lib/grids/#{qa.grid}.xlsx")

# SHEET 3
TO_EXCEL = {3=>{
  "E4"  => qa.supplier_profile.code,
  "E5"  => qa.assess_date.strftime("%Y-%m-%d"),
  "E6"  => qa.primary_assessor.assessor_profile.name,
  "E7"  => pls.map(&:name).join(","),
  "E8"  => "Internal Audit (done by Quality Auditor)",
  "E9"  => qa.target,
  "E10" => qa.supplier_profile.status,
  "E11" => result,
  "J2"  => qa.result,
  "J3"  => qa.cotation * 0.01
}}

TAG_TO_CELL = {
  "QACH1S1" => "16",
  "QACH1S2" => "17",
  "QACH1S3" => "18",
  "QACH1S4" => "19",
  "QACH2S1" => "20",
  "QACH3S1" => "21",
  "QACH3S2" => "22",
  "QACH3S3" => "23",
  "QACH3S4" => "24",
  "QACH3S5" => "25",
  "QACH3S6" => "26",
  "QACH3S7" => "27",
  "QACH4S1" => "28"
}



TAG_TO_CELL.keys.each do |sec|
  score = qa.section_score(sec)
  r = TAG_TO_CELL[sec]
  TO_EXCEL[3]["G#{r}"] = score[:cotation] * 0.01
  TO_EXCEL[3]["H#{r}"] = "#{score[:level]}"
  TO_EXCEL[3]["C#{r}"] = score[:subscores].find{|x| x[:level] == "E"}[:score] if score[:subscores].find{|x| x[:level] == "E"}.present?
  TO_EXCEL[3]["D#{r}"] = score[:subscores].find{|x| x[:level] == "D"}[:score] if score[:subscores].find{|x| x[:level] == "D"}.present?
  TO_EXCEL[3]["E#{r}"] = score[:subscores].find{|x| x[:level] == "C"}[:score] if score[:subscores].find{|x| x[:level] == "C"}.present?
  TO_EXCEL[3]["F#{r}"] = score[:subscores].find{|x| x[:level] == "B"}[:score] if score[:subscores].find{|x| x[:level] == "B"}.present?
end

def cell_to_coordinates(cell_name)
  x = cell_name.match(/(\D)(\d+)/)
  i = ("A".."Z").to_a.index(x[1])
  r = x[2].to_i - 1
  [r, i]
end


TO_EXCEL.each do |i,set|
  sheet = workbook.worksheets[i]
  set.each do |cell_name,value|
    coor = cell_to_coordinates(cell_name)
    cell = sheet[coor[0]][coor[1]]
    value = "Not audited" if value == "Not Assessed"
    if cell.formula.present?
      ref_sheet = cell.formula.expression.split("!").first[1..-2]
      ref_cell = cell.formula.expression.split("!").last
      sht = workbook[ref_sheet]
      ref_coor = cell_to_coordinates(ref_cell)
      cel = sht[ref_coor[0]][ref_coor[1]]
      cel.change_contents(value, nil)
    end

    cell.change_contents(value, nil)
  end
end

workbook.write("#{Rails.root}/lib/test-#{qa.grid}.xlsx")
