require 'docsplit'
# require 'RMagick'

PATH="/Users/sergiorivas/Downloads/dpr_update2"

# OUTPUT="#{Rails.root}/public/dpr"
OUTPUT="/Users/sergiorivas/Downloads/dpr_update2/OUT"

Dir.foreach(OUTPUT) do |f|
  fn = File.join(OUTPUT, f)
  File.delete(fn) if !f.in?(['.','..'])
end

# all_files = Dir["#{PATH}/**/*.pdf"].reject{|x| x.include?("DPR_TOOLS/")}
all_files = Dir["#{PATH}/*.pdf"]
dpr_codes = DprGuideline.all.map(&:code)
res = {}
dpr_codes.each do |code|
  files = all_files.select{|x| x.include?(code)}
  res[code] = files
end

puts "BEGIN SPLITTING"
start = Time.now
res.each do |code, files|
  print "."
  next if files.count.zero?

  file = files.sort_by{|x| File.mtime(x)}.last

  images = Docsplit.extract_images(file, format: :png, output: OUTPUT)
end
puts "this took #{Time.now - start} seconds"


filenames = Dir.glob("#{OUTPUT}/*.png")

file_set = {}
dpr_codes.each do |code|
  files = filenames.select{|x| x.include?(code)}
  file_set[code] = files
end

file_set.each do |code, set|
  set.each_with_index do |img, i|
    new_filename = "dpr_#{code}_#{i+1}.png"
    full_path = img.split("/")
    full_path[-1] = new_filename
    File.rename(img, full_path.join("/"))
  end
end
