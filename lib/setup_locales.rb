require 'csv'
LOCALE = {}
EXCEPTIONS = []
LOCALE_FILE = "locales-2020-03.csv"

def recursive_replace(h={})
  h.each do |k,v|
    if v.is_a? String
      h[k] = LOCALE[v] || "MISSING VALUE"
      EXCEPTIONS.push(v) if h[k] == "MISSING VALUE"
    elsif v.is_a? Hash
      recursive_replace(h[k])
    end
  end
  return h
end

def recursive_dump(h={})
  h.each do |k,v|
    if v.is_a? String
      LOCALE[k] = v
    elsif v.is_a? Hash
      recursive_dump(v)
    end
  end
  return h
end

def collect_values(h={})
  h.each do |k,v|
    if h[k].is_a? String
      h[k] = LOCALE[v] || "MISSING VALUE"
    elsif h[k].is_a? Hash
      recursive_replace(h[k])
    end
  end
  return h
end

def dump_to_csv!(locale: "en", prefix: nil)
  set = [prefix, locale]
  set.delete(nil)
  filename = set.join(".")

  h = YAML.load_file("#{Rails.root}/config/locales/#{file}.yml")
  h = recursive_dump(h)
  path = "#{Rails.root}/lib/locale_trans_uniq.csv"
  CSV.open(path, 'wb') do |csv|
    csv << ["en", "zh-CN"]
    LOCALE.values.uniq.each{|x| csv << [x]}
  end
end

def prep_trans(locale: "en")
  path = "#{Rails.root}/lib/#{LOCALE_FILE}"
  CSV.foreach(path, {headers: true}) do |row|
    LOCALE[row["en"]] = row[locale]
  end
end

def dump_to_locales!(locale:"en", prefix:nil)
  en_set = [prefix, 'en']
  en_set.delete(nil)
  en_file = en_set.join(".")

  set = [prefix, locale]
  set.delete(nil)
  filename = set.join(".")

  prep_trans(locale: locale)
  h = YAML.load_file("#{Rails.root}/config/locales/#{en_file}.yml")
  h = recursive_replace(h)

  File.write("#{Rails.root}/config/locales/#{filename}.yml", h.to_yaml.gsub("en:", "#{locale}:"))
end


# dump_to_csv!

# dump_to_locales!(locale: "fr")
# dump_to_locales!(locale: "fr", prefix: "assessgo")
# dump_to_locales!(locale: "it")
# dump_to_locales!(locale: "it", prefix: "assessgo")
# dump_to_locales!(locale: "vi")
# dump_to_locales!(locale: "vi", prefix: "assessgo")
puts EXCEPTIONS.uniq
