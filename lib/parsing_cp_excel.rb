require 'fileutils'
require 'creek'

# folder  = "#{Rails.root}/db/data"
folder = "/Users/sergiorivas/Desktop/cp_files"
files   = Dir.entries(folder).select{|x| x.include?("xls")}
i = 0
puts "PARSING #{files.count} FILES"
files.each do |file|
  puts "Analyzing #{file}"
  code = file.match(/\d{3}\d+/).to_s

  # @supplier = SupplierProfile.find_by(code: file.split("--").first)
  @supplier = SupplierProfile.find_or_create_by(code: code)
  if @supplier.blank?
    puts "SUPPLIER NOT FOUND for #{file}"
    FileUtils.mv("#{folder}/#{file}", "#{folder}/not-found/#{file}")
    next
  end
  puts "SUPPLIER FOUND!"
  creek  = Creek::Book.new("#{folder}/#{file}")

  puts "PARSING EXCEL"
  creek.sheets.each do |sheet|
    test_set = sheet.rows_with_meta_data.find{|x| x["cells"].values.join.include?("WHAT IS THE STANDARD")}
    next if test_set.blank?

    i+=1
    # INITIALIZE
    @results = {}
    @tmp_process = {}
    @category = ""
    @key = ""
    @process = ProductProcess.new
    @step = ProcessStep.new
    @step_params = {}

    puts "SCRAPING FOR SUPPLIER: #{@supplier.code}"
    sheet.rows_with_meta_data.each do |row|
      row["cells"] = row["cells"].reject{|k,v| k > "O#{row['r']}" || k.size > (row['r'].to_i + 1)}
      full_cells = row["cells"].reject{|k,v| v.nil?}
      next if full_cells.blank?
      puts "ROW: #{row["r"]}"
      p row
      merged = full_cells.values.join.gsub(/(\W+\s*)/,' ').strip.capitalize
      merged = "Incoming control" if merged == "In coming control"

      if full_cells.values.join.include?("PRODUCT designation") && @key.blank?
        @key = full_cells.reject{|k,v| !v.include?("PRODUCT designation")}.keys.first
        @key = "#{@key[0]}#{@key[1].to_i + 1}"
      end

      if full_cells.keys.include?(@key)
        pf_name = full_cells[@key].gsub(/(\W+\s*)/,' ').strip
        @pf = ProductFamily.find_or_create_by(name: pf_name, supplier_profile_id: @supplier.id)
        15.times{puts ""}
        puts "SUPPLIER: #{@supplier.code}"
        puts "PRODUCT FAMILY: #{@pf.name}"


        @results[@supplier.code] = {} if @results[@supplier.code].nil?
        @results[@supplier.code]["product_families"] = {} if @results[@supplier.code]["product_families"].nil?
        @results[@supplier.code]["product_families"][@pf.name] = {"processes"=> []}
      end

      if ProductProcess::CATEGORY.include? merged
        @category = merged
        puts "CATEGORY: #{@category}"
        @process_name = @category
      elsif full_cells.keys.count == 1
        p "PROCESS"
        @process_name = full_cells.values.join.gsub(/(\W+\s*)/,' ').strip.capitalize
        @process_name = full_cells.values.join.strip if @process_name.blank?
        @step_params[:step_number] = "000"
      else
        next if @category.blank?
        unless @process.name == @process_name
          # p @pf
          @results[@supplier.code]["product_families"][@pf.name]["processes"] << @tmp_process if @tmp_process.present?
          @process = ProductProcess.find_or_create_by(product_family_id: @pf.id, name: @process_name, category: @category)
        # p "HERE"
          puts "PROCESS: #{@process.name}"
          @tmp_process = {name: @process.name, category: @process.category, steps: []}
        end
        set = row["cells"].values.map{|x| x.to_s.strip if x.present?}

        @step_params[:product_process_id] = @process.id
        @step_params[:step_number] = set[0] if set[0].present?
        @step_params[:control_item] = set[1] if set[1].present?
        if set[0].present?
          puts "#{row['r']} STEP: #{set[0]} - #{set[1]}"
        end
        criticity = "Critical" if set[6].present?
        criticity = "Major" if set[7].present?
        criticity = "Minor" if set[8].present?
        # p criticity
        # p@step_params
        @item = {
          control_method: set[2],
          requirement: set[3],
          criticity: criticity,
          control_type: set[9],
          supplier_control_freq_note: set[10],
          supplier_controller: set[11],
          decathlon_control_freq_note: set[12],
          active: true
        }
        p @item

        ps = ProcessStep.find_or_create_by(@step_params.merge(@item))
        puts "ITEM: #{ps.control_method}"
        @tmp_process[:steps] << @step_params.merge(@item).except(:product_process_id)
      end
    end
    filepath = "#{Rails.root}/db/data/product_families"
    @results[@supplier.code]["product_families"][@pf.name]["processes"] << @tmp_process


    @supp_json = {"filename"=> file, "count"=> @results[@supplier.code]["product_families"][@pf.name]["processes"].count}
    @supp_json.merge!({"product_family"=> {"name"=> @pf.name, "processes"=> @results[@supplier.code]["product_families"][@pf.name]["processes"]}})
    File.open("#{filepath}/#{@supplier.code}-#{i}.json", "wb") do |f|
      f.write(@supp_json.to_json)
    end
  end
    @results[@supplier.code]["product_families"] = {}

  FileUtils.mv("#{folder}/#{file}", "#{folder}/completed/#{file}")

  rescue NoMethodError
    FileUtils.mv("#{folder}/#{file}", "#{folder}/000FAIL/#{file}")
  rescue Zip::Error
    FileUtils.mv("#{folder}/#{file}", "#{folder}/000FAIL/empty/#{file}")
  rescue RuntimeError
    FileUtils.mv("#{folder}/#{file}", "#{folder}/formatFail/#{file}")
end
