# Assess Go Server API + Web

---

In order to run this repository in development environment:

1. Git Pull into your computer
2. Make sure you have `ruby 2.6.3`
3. Do the following:

```
bundle install
LIMVERSION=4.2.8 rails s
```

---

In order to push to production:

1. Add production server's git receiver

```
git remote add prod dokku@139.217.233.160:assessgo
```

2. In the Production Server, add your ssh-key.

```
dokku ssh-keys:add yourname path/to/your/ssh/pub
```

3. Push!

```
git push prod master
```
