require 'sidekiq/web'
require_relative '../app/models/application_record'

Rails.application.routes.draw do
  get '/check.txt', to: proc { [200, {}, ['simple_check']] } # DOKKU DEPLOYMENT CHECK

  devise_for :users, controllers: { sessions: 'sessions' }

  root to: 'pages#dashboard'
  # patch '/' => 'pages#dashboard'

  # Static Pages
  get 'help' => 'pages#help', as: 'help'
  get 'gs' => 'pages#google_slide'
  get 'gs/:slide_url' => 'pages#google_slide'

  # Mobile native for Control Plan
  get '/native/survey-questions' => 'native#survey_questions', as: 'survey_questions'
  get '/native/new-supplier' => 'native#new_supplier', as: 'new_supplier'
  get '/native/request_access' => 'native#request_access', as: 'request_access'
  get '/native/control_plan/index' => 'native#cp_index', as: 'cp_index'
  get '/native/control_plan/search' => 'native#cp_search', as: 'cp_search'
  get '/native/control_plan/product-list' => 'native#cp_product_list', as: 'cp_product_list'
  get '/native/control_plan/product' => 'native#cp_product', as: 'cp_product'
  get '/native/control_plan/product-design' => 'native#cp_product_design', as: 'cp_product_design'
  get '/native/control_plan/new-product' => 'native#cp_new_product', as: 'cp_new_product'
  get '/native/control_plan/schedule' => 'native#cp_schedule', as: 'cp_schedule'
  get '/native/control_plan/review' => 'native#cp_review', as: 'cp_review'
  get '/native/control_plan/review-detail' => 'native#cp_review_detail', as: 'cp_review_detail'
  get '/native/control_plan/findings' => 'native#cp_findings', as: 'cp_findings'
  get '/native/control_plan/history' => 'native#cp_history', as: 'cp_history'
  get '/native/control_plan/cphistory' => 'native#cp_cphistory', as: 'cp_cphistory'
  get '/native/control_plan/kpi' => 'native#cp_kpi', as: 'cp_kpi'
  get '/native/control_plan/list' => 'native#cp_list', as: 'cp_list'
  get '/native/control_plan/new-step' => 'native#cp_new_step', as: 'cp_new_step'
  get '/native/control_plan/report' => 'native#cp_report', as: 'cp_report'
  get '/native/cap/confirmation' => 'native#cap_confirmation', as: 'cap_confirmation'
  get '/native/cap/index' => 'native#cap_index', as: 'cap_index'
  get '/native/cap/form' => 'native#cap_form', as: 'cap_form'
  get '/native/cap/followup' => 'native#cap_followup', as: 'cap_followup'
  get '/native/cap/followup_invite' => 'native#cap_followup_invite', as: 'cap_followup_invite'
  get '/native/done' => 'native#done', as: 'done'

  #  Mobile native for assessor application

  get '/native/assessor_application/application' => 'native#aa_application', as: 'aa_application'
  get '/native/assessor_application/application_status' => 'native#aa_application_status', as: 'aa_application_status'
  get '/native/assessor_application/qualification' => 'native#aa_qualification', as: 'aa_qualification'
  get '/native/assessor_application/training_time' => 'native#aa_training_time', as: 'aa_training_time'
  get '/native/assessor_application/application_selection' => 'native#aa_application_selection', as: 'aa_application_selection'
  get '/native/assessor_application/validator' => 'native#aa_validator', as: 'aa_validator'
  get '/native/assessor_application/technical_eval' => 'native#aa_technical_eval', as: 'aa_technical_eval'

  # Mobile native for bug reporter
  get '/native/bug-tracker/form' => 'bugs#bug_tracker_form', as: 'bug_tracker_form'
  get '/native/bug-tracker/submitted' => 'bugs#bug_submitted', as: 'bug_submitted'
  # USERS CONTROLLER
  get 'users' => 'users#users', as: 'users'
  get 'users/staff' => 'users#staff', as: 'staff'
  get 'users/email_user' => 'users#email_user', as: 'email_user'
  post 'users/send_email' => 'users#send_email'
  get 'users/new' => 'users#users_new', as: 'users_new'
  get 'users/reset' => 'users#reset'
  get 'users/lock'  => 'users#lock'
  get 'users/unlock'  => 'users#unlock'
  get 'users/invite'  => 'users#invite'
  get 'users/permissions' => 'users#permissions'
  post 'users/permissions' => 'users#post_permissions'
  patch 'users/permissions' => 'users#patch_permissions'
  get 'users/permissions/all' => 'users#permissions_all'
  get 'users/test_login' => 'users#test_login'

  get 'suppliers'          => 'suppliers#suppliers', as: 'suppliers'
  get 'suppliers/:id/edit' => 'suppliers#suppliers_edit', as: 'suppliers_edit'
  get 'suppliers/new'      => 'suppliers#suppliers_new', as: 'suppliers_new'
  post 'add_supplier'      => 'suppliers#supplier_create'

  get 'locked' => 'users#locked', as: 'locked'
  # =====================================================
  # =====================================================

  # REPORTS
  get 'reports' => 'reports#reports', as: 'reports'
  get 'reports/my-reports' => 'reports#my_reports', as: 'my_reports'
  get 'reports/my-reports/:slug' => 'reports#update_report', as: 'update_report'
  get 'reports/my-reports/:slug/ap' => 'reports#update_ap', as: 'update_ap'
  get 'reports/my-reports/:slug/go' => 'reports#update_go', as: 'update_go'
  get 'reports/my-reports/:slug/ca' => 'reports#update_ca', as: 'update_ca'
  get 'reports/my-reports/:slug/cap' => 'reports#update_cap', as: 'update_cap'
  get 'reports/my-reports/:slug/finish' => 'reports#finish_qa', as: 'finish_qa'
  get 'reports/my-reports/:slug/comments' => 'reports#overall_comments', as: 'overall_comments'
  get 'reports/my-reports/:slug/ca/:chapter' => 'reports#update_chapter', as: 'update_chapter'
  get 'reports/my-reports/:slug/dormitory' => 'reports#update_dormitory', as: 'update_dormitory'
  get 'reports/my-reports/:slug/download' => 'reports#download', as: 'download_report'
  get 'reports/my-reports/:slug/download-excel' => 'reports#download_excel', as: 'download_excel'
  get 'reports/my-reports/:slug/download-cap-review' => 'reports#download_cap_review', as: 'download_cap_review'
  get 'reports/my-reports/:slug/upload-cap-revew' => 'reports#upload_cap_review', as: 'upload_cap_review'
  get 'reports/my-reports/:slug/generate-cap-revew' => 'reports#generate_cap_review', as: 'generate_cap_review'
  get 'reports/inprogress' => 'reports#inprogress', as: 'inprogress'
  get 'reports/completed' => 'reports#completed', as: 'completed'
  get 'reports/my-cp-reports/:slug' => 'reports#update_cp_report', as: 'update_cp_report'
  get 'reports/my-cp-reports/:slug/plan-item/:pi_id' => 'reports#update_plan_item', as: 'update_plan_item'
  get 'reports/my-cp-reports/:slug/download' => 'reports#download_cp', as: 'download_cp'

  # CONTROL PLAN
  get 'control-plan' => 'pages#control_plan', as: 'control_plan'
  get 'control-plan/:supplier_code/product-families' => 'control_plans#product_families', as: 'product_families'
  get 'control-plan/:supplier_code/product-families/:pf_id' => 'control_plans#update_product_family', as: 'update_product_family'
  get 'control-plan/:supplier_code/product-families/:pf_id/process-steps' => 'control_plans#process_steps', as: 'process_steps'
  get 'control-plan/:supplier_code/product-families/:pf_id/process-steps/:ps_id' => 'control_plans#update_process_step', as: 'update_process_step'

  # STATS
  get 'statistics' => 'statistics#statistics', as: 'stats'
  get 'statistics/excel-download' => 'statistics#excel_download', as: 'excel_download'
  get 'statistics/excel/supplier' => 'statistics#excel_by_code', as: 'excel_by_code'
  get 'statistics/excel/global' => 'statistics#excel_global', as: 'excel_global'
  get 'statistics/xuwei' => 'statistics#xuwei_stats'
  get 'statistics/country' => 'statistics#country', as: 'country_stats'
  get 'statistics/process' => 'statistics#industrial_process', as: 'process_stats'
  get 'statistics/result' => 'statistics#result', as: 'result_stats'
  get 'statistics/rank' => 'statistics#rank', as: 'rank_stats'
  get 'statistics/comment' => 'statistics#comment', as: 'comment_stats'
  get 'statistics/supplier' => 'statistics#supplier', as: 'supplier_stats'
  get 'statistics/supplier-full' => 'statistics#supplier_full', as: 'supplier_full_stats'
  get 'statistics/supplier/search' => 'statistics#supplier_search', as: 'supplier_stats_search'
  get 'statistics/supplier/history' => 'statistics#supplier_history', as: 'supplier_history_stats'
  get 'statistics/supplier/:supplier_code' => 'statistics#supplier_qa', as: 'supplier_qa_stats'
  get 'statistics/multiple-stats' => 'statistics#multiple_stats', as: 'multiple_stats'
  get 'statistics/assessment' => 'statistics#assessment', as: 'assessment_stats'

  get 'goes' => 'pages#goes', as: 'goes'

  # GUIDELINES
  get 'guidelines/show' => 'guidelines#show', as: 'guideline'
  get 'guidelines/prep' => 'guidelines#prep', as: 'guideline_prep'
  get 'guidelines/toolbox' => 'guidelines#toolbox', as: 'guideline_toolbox'
  get 'guidelines/frequency' => 'guidelines#frequency', as: 'guideline_frequency'
  post 'guidelines/frequency' => 'guidelines#frequency_update', as: 'create_guideline_frequency'
  post 'guidelines/frequency/:id' => 'guidelines#frequency_update', as: 'update_guideline_frequency'
  resources :guidelines, only: [:index, :edit, :update]

  # CP UPLOADER
  get  'upload-cp' => 'pages#upload_cp', as: 'upload_cp'
  post 'upload-cp' => 'pages#upload_cp', as: 'upload_cp_post'
  put  'upload-cp' => 'pages#upload_cp', as: 'upload_cp_put'

  get  'upload-historical-assessments' => 'pages#upload_historical_assessments', as: 'upload_historical_assessments_path'
  post 'upload-historical-assessments' => 'pages#upload_historical_assessments'
  # # ADMIN
  get 'new-change-log' => 'pages#new_change_log', as: 'change_log'

  get 'login' => 'pages#login', as: 'login'

  # EMAIL SYSTEM
  get 'email-system' => 'emails#email_system', as: 'email_system'
  get 'email-system/send-out' => 'emails#email_send_out', as: 'email_send_out'
  get 'email-system/send-out/new' => 'emails#letter_send_out_new', as: 'letter_send_out_new'
  post 'email-system/send-out/create' => 'emails#letter_send_out_create', as: 'letter_send_out_create'
  get 'email-system/send-out/show/:id' => 'emails#email_send_out_show', as: 'email_send_out_show'
  get 'email-system/manage-templates' => 'emails#manage_templates', as: 'manage_templates'
  get 'email-system/manage-template/new' => 'emails#mail_bag_new', as: 'mail_bag_new'
  post 'email-system/manage-template/create' => 'emails#mail_bag_create', as: 'mail_bag_create'
  get 'email-system/manage-template/edit/:id' => 'emails#mail_bag_edit', as: 'mail_bag_edit'
  post 'email-system/manage-template/update/:id' => 'emails#mail_bag_update', as: 'mail_bag_update'
  get 'email-system/view-email' => 'emails#view_email', as: 'view_email'
  get 'email-system/statistics' => 'emails#statistics', as: 'statistics'
  get 'email-system/statistics/spy/:id' => 'emails#statistics_spy', as: 'statistics_spy'

  # BOOKMARKS
  get 'bookmarks' => 'bookmarks#bookmarks', as: 'bookmarks'
  get 'bookmarks/strong-points' => 'bookmarks#strong_points', as: 'strong_points'
  get 'bookmarks/points-to-improve' => 'bookmarks#points_to_improve', as: 'points_to_improve'

  # BUG REPORTER
  get  'bug-reports' => 'bugs#bug_reports', as: 'bug_reports'
  get  'bug-reports/report/:id' => 'bugs#bug_report', as: 'bug_report'
  post 'bug-reports/report/:id/update-status' => 'bugs#update_bug_status', as: 'update_bug_status'
  get  'bug-reports/form' => "bugs#bug_tracker_form", as: 'web_bug_tracker_form'
  get  'bug-reports/generate_excel_bug_report' =>"bugs#generate_excel_bug_report", as: 'generate_excel_bug_report'

  get 'oauth2/callback' => 'oauth#callback'
  get 'oauth2/callbackweb' => 'oauth#callback_web'

  namespace :api, defaults: { format: :json } do
    namespace :v1 do
      get 'check_version' =>'base#check_version'
      get 'offline_data'  =>'custom_data#offline_data'
      # =====i18n FINISHED IN MP & API or a POST =====
      get  'members/home/home'                             => 'members#home'
      get  'members/me/me'                                 => 'members#me'
      get  'members/show/show'                             => 'members#members_show'
      get  'members/activity/activity'                     => 'members#activity'
      get  'members/activity/show'                         => 'members#activity_show'
      get  'members/activity/more'                         => 'members#activity_more'
      get  'members/search/form'                           => 'members#search_form'
      get  'members/search/results'                        => 'members#search_results'
      post 'members/search/results'                        => 'members#search_results'

      get  'members/assessment/index'                      => 'quality_assessments#index'
      get  'members/assessment/new'                        => 'quality_assessments#new'
      get  'members/assessment/pre-confirmation'           => 'quality_assessments#pre_confirmation'
      get  'members/assessment/review-assessment'          => 'quality_assessments#review_assessment'
      get  'members/assessment/overview'                   => 'quality_assessments#overview'
      get  'members/assessment/report'                     => 'quality_assessments#report'
      get  'members/assessment/assessment-prep'            => 'quality_assessments#assessment_prep'
      get  'members/assessment/assessment-prep-review'     => 'quality_assessments#assessment_prep_review'
      get  'members/assessment/go-for-assessment'          => 'quality_assessments#go_for_assessment'
      get  'members/assessment/go-for-assessment/:id/copy'     => 'quality_assessments#copy_last_go_for_assessment'
      get  'members/assessment/go-for-assessment-review'   => 'quality_assessments#go_for_assessment_review'
      get  'members/assessment/chapter-assessment'         => 'quality_assessments#chapter_assessment'
      get  'members/assessment/single-chapter'             => 'quality_assessments#single_chapter'
      get  'members/assessment/historical-level'           => 'quality_assessments#historical_level'
      get  'members/assessment/invitations'                => 'quality_assessments#invitations'
      get  'members/assessment/chapter-assessment-review'  => 'quality_assessments#chapter_assessment_review'
      get  'members/assessment/single-chapter-review'      => 'quality_assessments#single_chapter_review'
      get  'members/assessment/cap-index'                  => 'quality_assessments#cap_index'

      # get  'members/assessment/overall-comments'           => 'quality_assessments#overall_comments'

      post 'members/control_plan/schedule'                 => 'control_plan#schedule_create'
      get  'members/cap/index'                             => 'custom_data#cap_index'
      get  'members/cap/confirmation'                      => 'custom_data#cap_confirmation'
      get  'members/cap/followup'                          => 'custom_data#cap_followup'
      get  'members/cap/followup_invite'                   => 'custom_data#cap_followup_invite'
      get  'members/cap/related'                           => 'custom_data#cap_related'

      get  'members/me/assessor_application/application'           => 'assessor_application#application'
      get  'members/me/assessor_application/qualification'         => 'assessor_application#qualification'
      get  'members/me/assessor_application/training_time'         => 'assessor_application#training_time'
      get  'members/me/assessor_application/application_selection' => 'assessor_application#application_selection'
      get  'members/me/assessor_application/application_status'    => 'assessor_application#application_status'
      get  'members/me/assessor_application/validator'             => 'assessor_application#validator'

      # =====i18n done in MP & missing tl added to YML ====
      get  'members/supplier/edit-info'                    => 'quality_assessments#supplier_info'
      get  'members/control_plan/index'                    => 'control_plan#index'
      get  'members/control_plan/list'                     => 'control_plan#list'
      get  'members/control_plan/search'                   => 'control_plan#search'
      get  'members/control_plan/history'                  => 'control_plan#history'
      get  'members/control_plan/schedule'                 => 'control_plan#schedule'
      get  'members/control_plan/new-step'                 => 'control_plan#new_step'
      get  'members/control_plan/step-detail'              => 'control_plan#step_detail'
      get  'members/control_plan/step-edit'                => 'control_plan#step_edit'
      get  'members/control_plan/product-list'             => 'control_plan#product_list'
      get  'members/control_plan/product'                  => 'control_plan#product'
      get  'members/control_plan/new-product'              => 'control_plan#new_product'
      get  'members/control_plan/review'                   => 'control_plan#review'
      get  'members/control_plan/review-detail'            => 'control_plan#review_detail'
      get  'members/control_plan/kpi'                      => 'control_plan#kpi'
      get  'members/control_plan/findings'                 => 'control_plan#findings'
      get  'members/control_plan/cphistory'                => 'control_plan#cp_history'
      get  'members/control_plan/report'                   => 'control_plan#report'

      get  'members/cap/form'                              => 'custom_data#cap_form'
      get  'members/manual/index'                          => 'members#manual'
      get  'members/show/detail'                           => 'members#members_detail'
      get 'redo-chapter'                                   => 'quality_assessments#redo_chapter'
      get  'assessors/blackouts/:id'                       => 'custom_data#blackouts'
      # =====i18n missing?====

      # ===== Are we using these?
      # get  'members/assessment/checklist-submission'       => 'quality_assessments#checklist_submission'
      # get  'members/assessment/assessment'                 => 'quality_assessments#assessment' # not used
      # get  'members/assessment/inviting-user'              => 'quality_assessments#inviting_user'
      get  'suppliers/more-info/more-info'                 => 'pages#supplier_info'
      get  'members/account-settings/account-settings'     => 'members#account_settings'
      get  'pages/logs/logs'                               => 'pages#logs'
      get  'pages/forget-password/forget-password'         => 'pages#forget_password'
      get  'pages/components'                              => 'pages#components'
      get  'pages/coming-soon'                             => 'pages#coming_soon'
      get  'pages/loading'                                 => 'pages#loading'
      # =====

      devise_scope :user do
        post 'pages/app/launch'      => 'user_sessions#set_mode' # sign-in page, set mode based on server mode
        post 'sign-in'               => 'user_sessions#login' # app.js sign in, find user by wx_openid and issue Token
        post 'sign-out'              => 'user_sessions#logout' # app.js sign in, find user by wx_openid and issue Token
        post 'login-status'          => 'user_sessions#login_status'
        post 'pages/sign-in/sign-in' => 'user_sessions#create' # production login via username and password
        post 'change-password'       => 'user_sessions#change_password'
      end

      # ========= DATA INTERACTIONS =========
      get  "/wx_token"                          => 'custom_data#get_wx_token'
      get  "/oa_token"                          => 'custom_data#get_oa_token'
      get '/reports/:slug/pdf_finished'         => 'custom_data#pdf_finished'
      post "/caps"                              => 'custom_data#caps'
      post "/cap_followups"                     => "custom_data#cap_followup_create"
      post "/quality_assessments"               => 'custom_data#quality_assessments'
      post "/update_invitaion"                  => 'custom_data#invitation'
      get "/reset_invitation/:id"               => 'custom_data#destroy_assessor_assignments'
      post "/section-na/:id"                    => 'custom_data#section_na'
      post "/kpis"                              => 'custom_data#kpi_create'
      post "pages/sign-in/sign-in/avatar"       => "base#set_avatar"
      get  "/duplicate_assessment/:id"          => "custom_data#duplicate_assessment"
      post "/link_assessment_to_question/:id"   => "custom_data#link_assessment_to_question"



      # Standard CRUD's
      post  '/mark/comments/as/read' => 'data#read_comments', as: 'read_comments'

      scope ':name', name: /#{ApplicationRecord::VALID_MODELS.join('|')}/ do
        get    '/form'                  => 'pages#form',          as: 'api_form'
        get    '/guidelines'            => 'pages#guidelines',    as: 'api_guidelines'
        get    '/'                      => 'data#index',          as: 'data_index'
        get    '/:id'                   => 'data#show',           as: 'data_show'
        post   '/'                      => 'data#create',         as: 'data_create'
        post   '/:id'                   => 'data#update',         as: 'data_update'
        patch  '/:id'                   => 'data#update',         as: 'data_update_patch'
        post   '/:id/rotate'            => 'data#rotate_image',   as: 'rotate_image'
        post   '/:id/bookmark/:index'   => 'data#bookmark_image', as: 'bookmark_image'
        post   '/:id/:index'            => 'data#delete_image',   as: 'delete_image'
        delete '/:id'                   => 'data#destroy',        as: 'data_destroy'
      end
    end
  end
  mount ActionCable.server => '/cable'
  mount Sidekiq::Web => '/sidekiq'

  # match '*unmatched', to: proc {[404, {}, ['PAGE NOT FOUND']]}, via: :all,
  #   constraints: lambda{ |req| req.path.exclude?('active_storage') && req.path.exclude?('users') }
end
